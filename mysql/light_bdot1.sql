use cRIO;

-- Create the light_bdot1 table.
create table light_bdot1 (
    shot_num int primary key,
    insertion float,
    clocking float,
    latitude float,
    longitude float,
    alpha float,
    beta float,
    gamma float,
    port_radius float
);

-- Insert into the table the port information for a select number of shots.
insert into light_bdot1 (shot_num, insertion, clocking, latitude, longitude, alpha, beta, gamma, port_radius)
    select TREX_data.shot_num, 1.52847 - 0.01 * TREX_data.ax_z, 180, 0, 37.5, 0, 0, 0, 1.52847 
    from TREX_data
    where (TREX_data.shot_num >= 58842 and TREX_data.shot_num <= 60428)
;
