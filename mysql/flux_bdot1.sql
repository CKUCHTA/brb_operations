use cRIO;

-- Create the flux_bdot1 table.
create table flux_bdot1 (
    shot_num int primary key,
    insertion float,
    clocking float,
    latitude float,
    longitude float,
    alpha float,
    beta float,
    gamma float,
    port_radius float
);

-- Insert into the table the port information for a select number of shots. This is the position of point along the insertion axis that the flange is closest to.
insert into flux_bdot1 (shot_num, insertion, clocking, latitude, longitude, alpha, beta, gamma, port_radius)
    select TREX_data.shot_num, 0.2108, 0, 80, 0, 10, 0, 0, 1.5
    from TREX_data
    where (TREX_data.shot_num >= 63511 and TREX_data.shot_num <= 63800)
;

update flux_bdot1 set insertion = 0.2108 where (shot_num >= 62389 and shot_num <= 63510);
