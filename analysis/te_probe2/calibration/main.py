import logging
from modules.shot_loader import get_remote_shot_tree
import numpy as np
from te_probe2.calibration.optimization import get_te2_calibrations, load_optimized_values, save_optimized_values
from te_probe2.calibration.plot_data import plot_corrected_data, plot_optimized_values

# TODO: Get more shots since calibration bad for some of these shots.
# esat_shots = list(range(53385, 53388))
esat_shots = [53386]
# isat_shots = list(range(53364, 53369))
isat_shots = [53365]
save_file_path = './analysis/te_probe2/calibration/data/areas_and_resistances.mat'
logging.basicConfig(level=logging.DEBUG)

areas, resistances = get_te2_calibrations(isat_shots, esat_shots)
print("Areas: {}".format(areas))
print("Resistances: {}".format(resistances))
# save_optimized_values(areas, resistances, save_file_path)

# areas, resistances = load_optimized_values(save_file_path)
# plot_optimized_values(areas, resistances)

for i, shot in enumerate(esat_shots):
    tree = get_remote_shot_tree(shot)
    plot_corrected_data(tree, areas[i], resistances[i], 15, True)

for i, shot in enumerate(isat_shots):
    tree = get_remote_shot_tree(shot)
    plot_corrected_data(tree, areas[i], resistances[i], 0, True)
