import numpy as np
from scipy.signal import savgol_filter
import logging


def to_vsense(raw_data):
    # Remove baseline.
    vmeas = raw_data - np.average(raw_data[:1000])
    # Reverse voltage divider.
    resistor_1 = 56000 # ohms
    resistor_2 = 510 # ohms
    resistor_dig = 500 # ohms
    resistor_2_dig = (1 / resistor_2 + 1 / resistor_dig)**-1
    vsense = vmeas * (resistor_1 + resistor_2_dig) / resistor_2_dig
    return vsense

def to_iprobe(raw_data, raw_rsense, noise_data, noise_rsense):
    # Remove noise from the raw signal and change it to vsense.
    vsense = to_vsense(remove_noise(raw_data, raw_rsense, noise_data, noise_rsense))
    return vsense / raw_rsense

def correct_iprobe(iprobe, relative_area):
    # Change the iprobe values to the corrected values by adjusting by the relative area.
    # We multiply instead of divide because of an issue in the mdsplus equation.
    # TODO: Change mdsplus equation and then this for calibration.
    return iprobe * relative_area

def to_vprobe(iprobe, raw_rsense, vbias, rwire):
    return vbias + iprobe * (raw_rsense + rwire)

def remove_noise(raw_data, raw_rsense, noise_data, noise_rsense):
    # Subtract the raw and noise data after factoring in the sense resistance.
    return (raw_data / raw_rsense - noise_data / noise_rsense) * raw_rsense

def smooth_data(data):
    # These numbers are chosen so that the curve looks smoother but does not lose the overall shape of the curve.
    return savgol_filter(data, 41, 10)

def remove_value_spikes(data, max_diff, bad_domain_radius=20):
    """
    Remove value spikes by finding where spikes are and returning an index range to ignore in future analysis.

    Parameters
    ----------
    data : np.array
        Data to find problem spikes from.
    max_diff : float
        Maximum magnitude of change between moving avg and data to allow. Usually something around mean(all_data) / ____ works.
    bad_domain_radius : int
        Distance from bad points to also treat as bad.

    Returns
    -------
    usable_points : np.array[bool]
        Boolean mask of points that are not invalid.
    """
    # We follow a moving average of n points.
    num_avg_points = 300
    # max_diff = 0.01

    # Store the problem indeces.
    problem_indeces = []

    averages = np.zeros(data.shape[0])
    curr_avg = np.mean(data[:num_avg_points])
    avg_start_index = 0
    problem_curr_index = 0
    # Iterate through all points, moving the average and comparing the data to the average.
    for i in range(num_avg_points, data.shape[0]):
        averages[i] = curr_avg

        # If this is a problem point then add the index to the list but don't update the moving average with the new point.
        if abs(data[i] - curr_avg) > max_diff:
            problem_indeces.append(i)
        else:
            curr_avg += data[i] / num_avg_points
            curr_avg -= data[avg_start_index] / num_avg_points

            # Increase the average start index but skip past the problem points.
            avg_start_index += 1
            while problem_curr_index < len(problem_indeces) and problem_indeces[problem_curr_index] <= avg_start_index:
                if problem_indeces[problem_curr_index] == avg_start_index:
                    avg_start_index += 1
                problem_curr_index += 1

    # Create a boolean mask that holds the valid analysis points as ones.
    usable_points = np.ones(data.shape, dtype=bool)

    for i in problem_indeces:
        # Set all booleans around the problem point to 0 (i - domain_size : i + domain_size)
        usable_points[max(0, i - bad_domain_radius): min(data.shape[0], i + bad_domain_radius)] = 0

    logging.debug("{}% of points were bad because of spiky data.".format(100 * (1 - np.count_nonzero(usable_points) / usable_points.size)))
    return usable_points

def get_usable_mask(data_list, index_range, val_bounds=None):
    """
    Get a mask of the usable data for comparing between curves by removing bad points and only selecting in the index range.
    
    Parameters
    ----------
    data_list : list[np.array]
        List of data arrays.
    index_range : tuple[int]
        Tuple of 2 elements that are the start and end indeces.
    val_bounds : tuple[float], default=None
        (Minimum, maximum) bounds that the data must fall between or it is unusable. This is useful 
        for digitizer data that hits the top/bottom voltage rails. By default do
        not limit value.

    Returns
    -------
    all_usable_points : np.array[bool]
        A boolean array that stores the usability of each index.
    """
    logging.debug("Getting usable points.")
    # Initialize the mask and set only points within the range to 1.
    usable_point_mask = np.zeros(data_list[0].shape)
    usable_point_mask[index_range[0]:index_range[1]] = 1

    max_diff = max([abs(np.mean(data)) for data in data_list]) / 2.5
    for data in data_list:
        nonspiky_point_mask = remove_value_spikes(data, max_diff)

        if val_bounds is None:
            logging.debug("{}% of points will be used.".format(100 * np.count_nonzero(nonspiky_point_mask) / nonspiky_point_mask.size))
            usable_point_mask = np.logical_and(usable_point_mask, nonspiky_point_mask)
        else:
            bounded_point_mask = np.logical_and(val_bounds[0] <= data, data <= val_bounds[1])
            logging.debug("{}% of points were bad because of value bounds.".format(100 * (1 - np.count_nonzero(bounded_point_mask) / bounded_point_mask.size)))

            curr_usable_points = np.logical_and(nonspiky_point_mask, bounded_point_mask)
            logging.debug("{}% of points will be used.".format(100 * np.count_nonzero(curr_usable_points) / curr_usable_points.size))
            usable_point_mask = np.logical_and(usable_point_mask, curr_usable_points)

    return usable_point_mask

# TODO: Move this stuff into the modules.langmuir area.
def get_usable_points(data_list, index_range=(10000, 50000), val_bounds=None):
    """
    Get only points that we want to compare out of a list of arrays.
    
    Parameters
    ----------
    data_list : list[np.array]
        List of 1D data arrays all of the same size.
    index_range : tuple[int], default=(10000, 50000)
        The minimum and maximum indeces to use.
    val_bounds : tuple[float], default=None
        (Minimum, maximum) bounds that the data must fall between or it is unusable. This is useful 
        for digitizer data that hits the top/bottom voltage rails. By default do
        not limit value.
    
    Returns
    -------
    usable_data_list : list[np.array]
        List of data arrays where the only remaining points are those to compare.
    """
    usable_point_mask = get_usable_mask(data_list, index_range, val_bounds)
    return [data[np.where(usable_point_mask)] for data in data_list]
