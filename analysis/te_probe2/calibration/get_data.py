import logging
import numpy as np
from te_probe2.calibration import constants
from te_probe2.calibration import change_data


def get_node_data(tree, tag):
    """Get the data of some node."""
    logging.debug("Getting data for node {tag}.".format(tag=tag))
    node = tree.get(tag)
    node_data = node.data()
    logging.debug("Got node {tag} data from tree.".format(tag=tag))
    return node_data

def get_node_time_data(tree, tag):
    """Get the time data of some node."""
    logging.debug("Getting time data for node {tag}.".format(tag=tag))
    time_node = tree.get('dim_of({tag})'.format(tag=tag))
    time_data = time_node.data()
    logging.debug("Got node {tag} time data from tree.".format(tag=tag))
    return time_data

def to_tag(card, channel, cell=3):
    return "\\c{cell}_{card}_{channel}".format(cell=cell, card=card, channel=str(channel).zfill(2))

def get_tip_data(tree, get_times=True):
    """Get the tip data for this probe."""
    tip_tags = [to_tag(1, 1), to_tag(1, 3), to_tag(1, 6), to_tag(1, 7), to_tag(1, 9), to_tag(1, 11), to_tag(1, 13), to_tag(1, 15),
                to_tag(3, 1), to_tag(3, 3), to_tag(3, 6), to_tag(3, 7), to_tag(3, 9), to_tag(3, 11), to_tag(3, 13), to_tag(3, 15)]

    # Get the data for each node and change it to a more precise data type.
    tip_data = [get_node_data(tree, tag).astype(np.float64) for tag in tip_tags]
    if get_times:
        tip_time_data = [get_node_time_data(tree, tag) for tag in tip_tags]
    else:
        tip_time_data = [None for _ in tip_tags]

    logging.debug("Got info for probe tips.")
    return {"raw": tip_data, "time": tip_time_data}

def get_noise_data(tree, get_times=True):
    """Get the tip data for this probe."""
    noise_tags = [to_tag(1, 2), to_tag(1, 4), to_tag(1, 5), to_tag(1, 8), to_tag(1, 10), to_tag(1, 12), to_tag(1, 14), to_tag(1, 16),
                to_tag(3, 2), to_tag(3, 4), to_tag(3, 5), to_tag(3, 8), to_tag(3, 10), to_tag(3, 12), to_tag(3, 14), to_tag(3, 16)]

    # Get the data for each node and change it to a more precise data type.
    noise_data = [get_node_data(tree, tag).astype(np.float64) for tag in noise_tags]
    if get_times:
        noise_time_data = [get_node_time_data(tree, tag) for tag in noise_tags]
    else:
        noise_time_data = [None for _ in noise_tags]
    logging.debug("Got info for probe noise.")
    return {"noise": noise_data, "time": noise_time_data}

def get_bias_data(tree):
    """Get the tip biases."""
    bias_tags = [to_tag(1, chan, cell=1) for chan in range(9, 17)] + [to_tag(2, chan, cell=1) for chan in range(9, 17)]
    bias_div_tags = ['\\te1_{}_vbias_div'.format(str(tip_num).zfill(2)) for tip_num in range(1, 17)]
    bias_offset_tags = ['\\te1_{}_vbias_off'.format(str(tip_num).zfill(2)) for tip_num in range(1, 17)]

    # bias_data = [(get_node_data(tree, tag).astype(np.float64) for tag in bias_tags]
    bias_data = []
    for i in range(len(bias_tags)):
        raw_data = get_node_data(tree, bias_tags[i]).astype(np.float64)
        offset = get_node_data(tree, bias_offset_tags[i]).astype(np.float64)
        divider = get_node_data(tree, bias_div_tags[i]).astype(np.float64)

        bias_data.append((raw_data - offset) / divider)
    logging.debug("Got data for biases.")
    return bias_data

def get_bias(tree, index):
    """Get specific bias of certain capacitor indexed from 0."""
    full_bias_data = get_bias_data(tree)
    # Take the average of the first 2000 points to get an estimate of the bias during the entire run.
    # Even though during esat the bias voltages drops over time, since we are comparing the data pointwise this doesn't matter.
    # In fact we could totally ignore the bias voltage when comparing but we include it to be consistent with the math.
    avg_bias = [np.mean(data[:2000]) for data in full_bias_data]

    return avg_bias[index]

def get_iprobe_data(tree):
    """
    Get the iprobe data for the probe.
    """
    # Get data for tips.
    tip_data_dict = get_tip_data(tree, get_times=False)
    noise_data_dict = get_noise_data(tree, get_times=False)

    # Change the data into iprobe values.
    iprobe_data_list = [change_data.to_iprobe(tip_data_dict["raw"][i], constants.RSENSE_RAW[i], noise_data_dict["noise"][i], constants.RSENSE_NOISE[i]) for i in range(16)]
    return iprobe_data_list

def get_usable_iprobe_data(tree):
    """
    Get iprobe data within some range that can be compared by ignoring spots where peaks exist.
    
    returns:
    usable_data = List of iprobe data arrays with only data we want to compare.
    """
    # Smooth the iprobe data.
    iprobe_data_list = get_iprobe_data(tree)
    iprobe_data_list = [change_data.smooth_data(data) for data in iprobe_data_list]

    # Create a mask of the points that are not peaks and within the specified range to compare.
    usable_data = change_data.get_usable_points(iprobe_data_list)

    return usable_data
