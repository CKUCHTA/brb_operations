import matplotlib.pyplot as plt
import logging
from te_probe2.calibration import get_data, change_data, constants
import numpy as np


def plot_raw_data(tree):
    """Plot the cell data used for te probe #1."""
    fig = plt.figure()
    gs = fig.add_gridspec(1, 2, wspace=0)
    axs = gs.subplots(sharey=True)
    
    fig.suptitle("Shot {}".format(tree.shot_number))
    for i, card in enumerate([1, 3]):
        logging.info("Plotting raw data from card {}.".format(card))
        raw_node_tags = ["\\c{cell}_{card}_{channel}".format(cell=3, card=card, channel=str(chan).zfill(2)) for chan in range(1, 17)]

        axs[i].set_title("Card {}".format(card))
        axs[i].set_yticks(range(1, 17))
        axs[i].set_ylim(0, 17)
        for j, tag in enumerate(raw_node_tags):
            raw_data = get_data.get_node_data(tree, tag)

            axs[i].plot(2 * raw_data + j + 1, lw=0.5)

        axs[i].grid(axis='y')

    for ax in axs.flat:
        ax.set(ylabel='Channel')

    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()

    plt.show()

def plot_tip_data(tree, scale_factor=1):
    """Plot the tip data used for te probe #1."""
    fig = plt.figure()
    ax = plt.axes()
    
    fig.suptitle("Shot {}: Tip Data".format(tree.shot_number))
    ax.set_yticks(range(1, 17))
    ax.set_ylim(0, 17)

    tip_data = get_data.get_tip_data(tree)
    noise_data = get_data.get_noise_data(tree)
    time_data_list = tip_data['time']
    raw_data_list = [tip_data['raw'][i] * 40 for i in range(16)]
    noise_data_list = [noise_data['noise'][i] * 40 for i in range(16)]
    for tip_number in range(16):
        logging.debug("Plotting tip {}.".format(tip_number + 1))
        # ax.plot(time_data_list[tip_number], raw_data_list[tip_number] + tip_number + 1, lw=0.5, color='k')
        # ax.plot(time_data_list[tip_number], noise_data_list[tip_number] + tip_number + 1, lw=0.5, color='r')
        # ax.plot(time_data_list[tip_number], raw_data_list[tip_number] - noise_data_list[tip_number] + tip_number + 1, lw=1, alpha=0.5, color='g')
        ax.plot((raw_data_list[tip_number] - noise_data_list[tip_number]) * scale_factor + tip_number + 1, lw=1, alpha=0.5, color='g')

    ax.grid(axis='y')

    plt.show()

def plot_usable_data(shot, data_list, usable_data_func, plot_time=False, data_factor=1, bool_factor=1, separation=0):
    """
    Plot the data from the `data_list` along with which data is usable.
    
    Parameters
    ----------
    shot : int
        Shot number for the data
    data_list : list[np.array]
        List of data arrays.
    usable_data_func : func(np.array) -> np.array[bool]
        Function that takes the data and returns a mask of which data is usable.
    plot_time : bool, default=False
    data_factor : float, default=1.0
        How much to multiply each data array by when plotting.
    bool_factor : float, default=1.0
        How much to multiply each boolean mask array by when plotting.
    separation : float, default=0.0
        How much to separate each data array curve by. If the separation = 1 then we offset 
        all arrays up by 1 so that they are associated with the currect tip number.
    """
    all_usable_points = np.ones(data_list[0].shape[0], dtype=bool)
    logging.debug("Plotting data and usable values.")
    for i, data in enumerate(data_list):
        usable_points = usable_data_func(data)
        all_usable_points &= usable_points

        if plot_time:
            # Change the digitizer data indeces to microsecond values.
            x_data = np.arange(data.shape[0]) / 5
        else:
            x_data = np.arange(data.shape[0])
        
        if separation == 1:
            curr_sep = i + 1
        else:
            curr_sep = i * separation
        plt.plot(x_data, data * data_factor + curr_sep, alpha=0.2, lw=0.5, color=(1, 0, 0))
        plt.plot(x_data, usable_points.astype(np.float) * bool_factor + curr_sep, color=(0, 1, 0))
    
    if plot_time:
        plt.plot(np.arange(all_usable_points.shape[0]) / 5, all_usable_points.astype(np.float) * bool_factor, label="Combined usable points")
    else:
        plt.plot(np.arange(all_usable_points.shape[0]), all_usable_points.astype(np.float) * bool_factor, label="Combined usable points", color=(0, 0, 0))

    plt.legend(["Data", "Usable points"], loc="upper right")
    plt.title("Shot #{}".format(shot))

    if plot_time:
        plt.xlabel("Time (microsecond)")
    else:
        plt.xlabel("Index")

    plt.show()

def plot_bias_data(tree):
    """Plot the capacitor voltages over time."""
    bias_data_list = get_data.get_bias_data(tree)
    mean_biases = []
    for i, bias_data in enumerate(bias_data_list):
        plt.plot(bias_data, label=str(i + 1))
        curr_mean_bias = np.mean(bias_data[:2000])
        mean_biases.append(curr_mean_bias)
        logging.info("({}) Vbias average: {}".format(str(i + 1).zfill(2), curr_mean_bias))
    
    plt.title("Bias Voltage vs. Time Index for shot #{}".format(tree.shot_number))
    plt.yticks(mean_biases, list("{} ({} V)".format(str(i + 1), int(mean_biases[i])) for i in range(len(mean_biases))))
    plt.legend()
    plt.show()

def plot_corrected_data(tree, relative_areas, wire_resistances, bias_data_index=None, compare_correction=False):
    """Plot the iprobe and vprobe data after optimizing the relative area and wire resistance."""
    tip_data_dict = get_data.get_tip_data(tree)
    noise_data_dict = get_data.get_noise_data(tree)
    bias_data_list = get_data.get_bias_data(tree)

    iprobe_data_list = [change_data.to_iprobe(tip_data_dict["raw"][i], constants.RSENSE_RAW[i], noise_data_dict["noise"][i], constants.RSENSE_NOISE[i]) for i in range(16)]
    corrected_iprobe_data_list = [change_data.correct_iprobe(iprobe_data_list[i], relative_areas[i]) for i in range(16)]
    
    if bias_data_index is None:
        vprobe_data_list = [change_data.to_vprobe(iprobe_data_list[i], constants.RSENSE_RAW[i], bias_data_list[i], 0) for i in range(16)]
        corrected_vprobe_data_list = [change_data.to_vprobe(iprobe_data_list[i], constants.RSENSE_RAW[i], bias_data_list[i], wire_resistances[i]) for i in range(16)]
    elif isinstance(bias_data_index, int):
        vprobe_data_list = [change_data.to_vprobe(iprobe_data_list[i], constants.RSENSE_RAW[i], bias_data_list[bias_data_index], 0) for i in range(16)]
        corrected_vprobe_data_list = [change_data.to_vprobe(iprobe_data_list[i], constants.RSENSE_RAW[i], bias_data_list[bias_data_index], wire_resistances[i]) for i in range(16)]

    if not compare_correction:
        for iprobe in corrected_iprobe_data_list:
            plt.plot(iprobe)
        plt.title("Shot #{}: Iprobe".format(tree.shot_number))
        plt.show()

        for vprobe in corrected_vprobe_data_list:
            plt.plot(vprobe)
        plt.title("Shot #{}: Vprobe".format(tree.shot_number))
        plt.show()
    else:
        a = 0.5
        # Iprobe plot.
        for i, iprobe in enumerate(iprobe_data_list):
            if i == 0:
                plt.plot(iprobe, label='Uncorrected', color='r', alpha=a)
            else:
                plt.plot(iprobe, color='r', alpha=a)
        for i, iprobe in enumerate(corrected_iprobe_data_list):
            if i == 0:
                plt.plot(iprobe, label='Corrected', color='b', alpha=a)
            else:
                plt.plot(iprobe, color='b', alpha=a)
        plt.legend()
        plt.title("Shot #{}: Iprobe".format(tree.shot_number))
        plt.show()
        
        # Vprobe plot.
        for i, vprobe in enumerate(vprobe_data_list):
            if i == 0:
                plt.plot(vprobe, label='Uncorrected', color='r', alpha=a)
            else:
                plt.plot(vprobe, color='r', alpha=a)
        for i, vprobe in enumerate(corrected_vprobe_data_list):
            if i == 0:
                plt.plot(vprobe, label='Corrected', color='b', alpha=a)
            else:
                plt.plot(vprobe, color='b', alpha=a)
        plt.legend()
        plt.title("Shot #{}: Vprobe".format(tree.shot_number))
        plt.show()

def plot_optimized_values(relative_areas, wire_resistances):
    """
    Plot the optimized values.
    
    params:
    relative_areas      = 3D array where i is a shot, j is a loop, and k is a tip index. This array has elements which are the areas.
    wire_resistances    = 3D array where i is a shot, j is a loop, and k is a tip index. This array has elements which are the resistances.
    """
    color_map = plt.get_cmap('brg', relative_areas.shape[0])

    divided_relative_areas = relative_areas
    for i in range(relative_areas.shape[0]):
        for j in range(relative_areas.shape[1]):
            divided_relative_areas[i, j] /= np.max(relative_areas[i, j])
            plt.plot(np.arange(1, 17), divided_relative_areas[i, j], color=color_map(i / (relative_areas.shape[0])), lw=0.5, alpha=0.2)
    mean_areas = np.mean(divided_relative_areas, axis=(0, 1))

    print("Mean areas:")
    rounded_areas = np.around(mean_areas, decimals=5)
    for i, mean_area in enumerate(rounded_areas):
        print("{}: {}".format(str(i + 1).zfill(2), mean_area))

    plt.scatter(np.arange(1, 17), mean_areas, color='black', zorder=100)
    plt.title("Relative Area divided my max area")
    plt.show()

    color_map = plt.get_cmap('brg', wire_resistances.shape[0])
    for i in range(wire_resistances.shape[0]):
        for j in range(wire_resistances.shape[1]):
            plt.plot(np.arange(1, 17), wire_resistances[i, j], color=color_map(i / (wire_resistances.shape[0] - 1)), lw=0.5, alpha=0.2)
    mean_resistances = np.mean(wire_resistances, axis=(0, 1))

    print("Mean resistances:")
    rounded_resistances = np.around(mean_resistances, decimals=5)
    for i, mean_resistance in enumerate(rounded_resistances):
        print("{}: {}".format(str(i + 1).zfill(2), mean_resistance))

    plt.scatter(np.arange(1, 17), mean_resistances, color='black', zorder=100)
    plt.title("Wire Resistances")
    plt.show()
