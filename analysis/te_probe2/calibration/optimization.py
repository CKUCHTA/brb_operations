import logging
from te_probe2.calibration import constants, get_data, change_data
import matplotlib.pyplot as plt
from scipy.optimize import minimize, Bounds, differential_evolution
import numpy as np
from scipy.io import savemat, loadmat
from te_probe1.get_data import Te_Probe1_Data
import tqdm
from modules.shot_loader import get_remote_shot_tree


SAVE_PATH = lambda te_number: "./analysis/te_probe{num}/calibration/data/".format(num=te_number)

def compare_vectors(coefficients, vector_list):
    """
    Calculate the total difference between all vectors in a list of vectors that are all linearly multiplied by some coefficient.
    """
    total_norm = 0
    for i in range(len(vector_list) - 1):
        for j in range(i + 1, len(vector_list)):
            total_norm += np.linalg.norm(vector_list[i] - vector_list[j], ord=2) / min(coefficients[i], coefficients[j])

    return total_norm

def iprobe_curve_similarity(relative_areas, iprobe_magnitudes_sq, iprobe_dot_products):
    """
    Calculate the similarity between various iprobe curves.

    params:
    relative_areas          = Coefficients to multiply the first 15 curves by.
    iprobe_magnitudes_sq    = The magnitude of each iprobe curve.
    iprobe_dot_products     = The dot product between each of the iprobe curves.
    """
    # We calculate the difference between the curves by finding |a_i x_i - a_j x_j|^2 / min(a_i, a_j).
    # We then expand this out to make the calculation fast.
    total = 0
    for i in range(relative_areas.size):
        for j in range(i + 1, relative_areas.size):
            total += (relative_areas[i]**2 * iprobe_magnitudes_sq[i] + relative_areas[j]**2 * iprobe_magnitudes_sq[j]
                      - 2 * relative_areas[i] * relative_areas[j] * iprobe_dot_products[i, j]) / min(relative_areas[i], relative_areas[j])**2
    return total

def compare_rsense(tree):
    """
    Compare various combinations of rsense for each probe tip by comparing the curve similarity and plotting each curve.
    """
    tip_data_dict = get_data.get_tip_data(tree)
    noise_data_dict = get_data.get_noise_data(tree)
    # TODO: Figure out which rsense go where.

    rsense_raw_options = [constants.RSENSE_RAW, constants.RSENSE_RAW[:8] + constants.RSENSE_NOISE[8:], 
                          constants.RSENSE_NOISE[:8] + constants.RSENSE_RAW[8:], constants.RSENSE_NOISE]
    rsense_noise_options = list(reversed(rsense_raw_options))

    colors = ['r', 'g', 'b', 'k']
    for index in range(len(rsense_raw_options)):
        iprobe_data_list = [
            change_data.smooth_data(change_data.to_iprobe(tip_data_dict["raw"][i], rsense_raw_options[index][i], noise_data_dict["noise"][i], rsense_noise_options[index][i])) for i in range(16)
        ]
        for data in iprobe_data_list:
            plt.plot(data + index, alpha=0.5, color=colors[index])
    
    plt.show()

def optimize_arel(usable_iprobe_data):
    """
    Optimize the relative area of each probe by comparing iprobe / Arel. When the probe is in Isat, these curves should be the same.

    params:
    usable_iprobe_data  = List of arrays containing the iprobe curves to compare.

    returns:

    """
    logging.debug("Optimizing curve similarity using relative areas.")
    num_curves = len(usable_iprobe_data)
    # Calculate the magnitudes squared and dot product between each curve.
    iprobe_magnitudes_sq = np.array([np.dot(iprobe, iprobe) for iprobe in usable_iprobe_data])
    iprobe_dot_products = np.zeros((num_curves, num_curves))
    for i in range(num_curves - 1):
        for j in range(i + 1, num_curves):
            iprobe_dot_products[i, j] = np.dot(usable_iprobe_data[i], usable_iprobe_data[j])

    # # Initialize relative areas with some random distribution.
    # initial_areas = np.random.normal(1, 0.2, len(usable_iprobe_data))
    # logging.debug("Relative areas initialized as {}.".format(initial_areas))

    # Set bounds on what the area could be.
    area_bounds = Bounds(0.1 * np.ones(num_curves), 25 * np.ones(num_curves))
    # Minimize the difference between each curve.
    function_args = tuple([iprobe_magnitudes_sq, iprobe_dot_products])
    optimized_result = differential_evolution(iprobe_curve_similarity, bounds=area_bounds, args=function_args)
    relative_areas = optimized_result.x
    logging.info("Optimized relative areas: {}".format(relative_areas))
    
    return relative_areas

def vprobe_curve_similarity(rwire, initial_rwire, raw_rsense, iprobe_magnitudes_sq, iprobe_dot_products):
    """
    Compare all vprobe curves and weight the difference so that there is less difference at rwire=initial guess.

    params:
    rwire                   = The wire resistances for each probe.
    initial_rwire           = The estimated wire resistance that we weight around.
    raw_rsense              = The sense resistor values.
    iprobe_magnitudes_sq    = The squared magnitudes of the iprobe vectors.
    iprobe_dot_products     = The dot product between each iprobe vector.
    """
    coefficients = np.array([rwire[i] + raw_rsense[i] for i in range(len(rwire))])
    # weight_function = lambda rwire1, rwire2, initial_rwire1, initial_rwire2: 1 + 0.0003 * ((rwire1 - initial_rwire1)**2 + (rwire2 - initial_rwire2)**2)

    total = 0
    for i in range(len(rwire) - 1):
        for j in range(i + 1, len(rwire)):
            # total += (coefficients[i]**2 * iprobe_magnitudes_sq[i] + coefficients[j]**2 * iprobe_magnitudes_sq[j]
            #           - 2 * coefficients[i] * coefficients[j] * iprobe_dot_products[i, j]) * \
            #           weight_function(rwire[i], rwire[j], initial_rwire[i], initial_rwire[j])
            total += (coefficients[i]**2 * iprobe_magnitudes_sq[i] + coefficients[j]**2 * iprobe_magnitudes_sq[j]
                      - 2 * coefficients[i] * coefficients[j] * iprobe_dot_products[i, j])

    return total

def optimize_rwire(usable_iprobe_data, probe_bias, rsense):
    """
    Optimize the resistance of the wire to the probes by comparing the probe voltage. When the probe is in Esat then these curves should be the same.

    params:
    usable_iprobe_data  = (list) All data that should be compared between probe tips.
    probe_bias          = Bias of each probe. This doesn't actually matter in optimization.
    rsense              = Sense resistor values.

    returns:
    rwire               = Optimized rwire values.
    """
    logging.debug("Optimizing curve similarity using wire resistance.")
    # Initialize the value of rwire and bound the possible resistance of the wires. The wires start with some random distribution around 25.
    initial_rwire = np.random.normal(25, 2, len(usable_iprobe_data))
    logging.debug("Wire resistances initialized as {}.".format(initial_rwire))

    num_curves = len(usable_iprobe_data)
    # Calculate the magnitudes squared and dot product between each curve.
    iprobe_magnitudes_sq = np.array([np.dot(iprobe, iprobe) for iprobe in usable_iprobe_data])
    iprobe_dot_products = np.zeros((num_curves, num_curves))
    for i in range(num_curves - 1):
        for j in range(i + 1, num_curves):
            iprobe_dot_products[i, j] = np.dot(usable_iprobe_data[i], usable_iprobe_data[j])

    rwire_bounds = Bounds(1 * np.ones(num_curves), 100 * np.ones(num_curves))
    function_args = (initial_rwire, rsense, iprobe_magnitudes_sq, iprobe_dot_products)
    optimized_result = differential_evolution(vprobe_curve_similarity, bounds=rwire_bounds, args=function_args)

    rwire_optimized = optimized_result.x
    logging.info("Optimized wire resistance: {}".format(rwire_optimized))

    return rwire_optimized

def save_optimized_values(relative_areas, wire_resistances, file_name):
    savemat(file_name, {'relative_areas': relative_areas, 'wire_resistances': wire_resistances})

def load_optimized_values(file_name):
    mat_file = loadmat(file_name)
    return mat_file['relative_areas'], mat_file['wire_resistances']

def get_te1_calibrations(isat_shots, esat_shots, num_loops):
    """
    Optimize the area and resistance for te probe #1.
    
    params:
    isat_shots          = Shot numbers when this probe is biased to isat.
    esat_shots          = Shot numbers when this probe is biased to esat.
    num_loops           = Number of times to repeat the optimizations with randomized initial areas/resistances.
    
    returns:
    relative_areas      = Array of optimized areas where the array has size (num_loops x num_probe_tips).
    wire_resistances    = Array of optimized resistances where the array has size (num_loops x num_probe_tips).
    """
    relative_areas = [[] for _ in isat_shots]
    wire_resistances = [[] for _ in esat_shots]

    te1_rsense = [131, 134.6, 135.7, 136.5, 135.2, 132.8, 133.7, 134.2, 371.8, 401.1, 387.8, 380.4, 385.7, 391.9, 376.3, 397.6]
    comparable_index_range = (16000, 50000)
        
    logging.debug("Looping through isat shots to optimize relative area for te probe 1.")
    for i, shot_number in enumerate(isat_shots):
        logging.info("Optimizing relative area for shot {}.".format(shot_number))
        tree = get_remote_shot_tree(shot_number)
        te_data = Te_Probe1_Data(tree, iprobe=True, vprobe=False, working=True, time=False)
        iprobe_data = te_data.iprobe[np.nonzero(te_data.working)]
        smoothed_iprobe_data = change_data.smooth_data(iprobe_data)
        usable_iprobe_data = change_data.get_usable_points(smoothed_iprobe_data, comparable_index_range)
        
        for _ in tqdm.tqdm(range(num_loops)):
            relative_area = optimize_arel(usable_iprobe_data)
            # The relative area array may have size less than the total number of tips so set the relative area for non-working tips to 1.
            all_relative_area = np.ones(16)
            all_relative_area[np.nonzero(te_data.working)] = relative_area
            relative_areas[i].append(all_relative_area)

    logging.debug("Looping through esat shots to optimize wire resistance for te probe 1.")
    for i, shot_number in enumerate(esat_shots):
        logging.info("Optimizing wire resistance for shot {}.".format(shot_number))
        tree = get_remote_shot_tree(shot_number)
        te_data = Te_Probe1_Data(tree, iprobe=True, vprobe=False, working=True, time=False)
        iprobe_data = te_data.iprobe[np.nonzero(te_data.working)]
        smoothed_iprobe_data = change_data.smooth_data(iprobe_data)
        usable_iprobe_data = change_data.get_usable_points(smoothed_iprobe_data, comparable_index_range)

        probe_bias = get_data.get_bias(tree, 15)
        for _ in tqdm.tqdm(range(num_loops)):
            wire_resistance = optimize_rwire(usable_iprobe_data, probe_bias, te1_rsense)
            # The wire resistance may have size less than the total number of tips so set the wire resistance for non-working tips to 0.
            all_wire_resistance = np.zeros(16)
            all_wire_resistance[np.nonzero(te_data.working)] = wire_resistance
            wire_resistances[i].append(all_wire_resistance)

    return relative_areas, wire_resistances

def get_te2_calibrations(isat_shots, esat_shots):
    """
    Optimize the area and resistance for te probe #2.
    
    params:
    isat_shots          = Shot numbers when this probe is biased to isat.
    esat_shots          = Shot numbers when this probe is biased to esat.
    
    returns:
    relative_areas      = Array of optimized areas where the array has size (num_loops x num_probe_tips).
    wire_resistances    = Array of optimized resistances where the array has size (num_loops x num_probe_tips).
    """
    relative_areas = [[] for _ in isat_shots]
    wire_resistances = [[] for _ in esat_shots]
        
    logging.debug("Looping through isat shots to optimize relative area for te probe 2.")
    for i, shot_number in enumerate(isat_shots):
        logging.info("Optimizing relative area for shot {}.".format(shot_number))
        tree = get_remote_shot_tree(shot_number)
        usable_iprobe_data = get_data.get_usable_iprobe_data(tree)
        relative_areas[i] = optimize_arel(usable_iprobe_data)

    logging.debug("Looping through esat shots to optimize wire resistance for te probe 2.")
    for i, shot_number in enumerate(esat_shots):
        logging.info("Optimizing wire resistance for shot {}.".format(shot_number))
        tree = get_remote_shot_tree(shot_number)
        usable_iprobe_data = get_data.get_usable_iprobe_data(tree)

        probe_bias = get_data.get_bias(tree, 15)
        wire_resistances[i] = optimize_rwire(usable_iprobe_data, probe_bias, constants.RSENSE_RAW)

    return np.array(relative_areas), np.array(wire_resistances)
