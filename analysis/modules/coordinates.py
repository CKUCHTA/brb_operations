"""This contains an object for holding a Position and a Direction. It also allows us to transform easily between coordinate systems."""
import numpy as np
from modules.generic_get_data import Port


class Position:
    def __init__(self, cartesian_position=None, cylindrical_position=None) -> None:
        """
        Contains the position of a point that can easily change between coordinate systems.

        Attributes
        ----------
        cartesian : np.array[float]
            `[x, y, z]` position in meters. See notes.
        cylindrical : np.array[float]
            `[rho, phi, z]` position in meters and radians. See notes.
        spherical : np.array[float]
            `[r, theta, phi]` position in meters and radians. See notes.

        Notes
        -----
        This can handle either individual points or multiple points. If this is 
        for a single point then pass 1D arrays of shape `(3,)` for any of the 
        attributes. If this is for `NUM_POINTS` points then pass 2D arrays of 
        shape `(3, NUM_POINTS)`.

        Examples
        --------
        >>> coord = Position()
        >>> coord.cartesian = np.array([3, 4, 2])
        >>> coord.cartesian
        array([3, 4, 2])
        >>> coord.cylindrical
        array([5., 0.92729522, 2.])

        You can also just initiate the position by passing the cartesian coordinates.
        >>> coord = Position([3, 4, 2])
        """
        self._x = None
        self._y = None
        self._z = None

        try:
            if cartesian_position is not None:
                self.cartesian = cartesian_position
            elif cylindrical_position is not None:
                self.cylindrical = cylindrical_position
        except IndexError:
            raise ValueError("Could not transform `{}` into a coordinate.".format(cartesian_position))
            
    def __repr__(self) -> str:
        return "Position(cartesian={})".format(self.cartesian)
    
    def from_port_object(self, port : Port, x=None, y=None, z=None, clocking=None):
        """
        Take the port object and convert it to be used by this object.

        Parameters
        ----------
        port : Port
            Port object to convert to a position.
        x, y, z : float (m), default=None
            Longitudanally, latitudinally, and radially inward position of
            probe in relation to port. (North, East, Down). If not given then
            the `port.insert` is used for `z` and `x = y = 0`.
        clocking : float (radians), default=None
            Counter-clockwise angle of the probe measured from when the x
            direction is pointed north. If not given then `port.clocking_rad`
            is used.

        Notes
        -----
        There is no inverse to get from cartesian to port coordinates as the coorinate is not unique.
        """
        self.from_port(
            port.rport, port.long_rad, port.lat_rad, port.alpha_rad, port.beta_rad, port.gamma_rad,
            0 if x is None else x, 0 if y is None else y,
            port.insert if z is None else z, port.clocking_rad if clocking is None else clocking
        )
    
    def from_port(self, port_r, port_phi, port_theta, alpha, beta, gamma, x_port, y_port, z_port, clocking):
        """
        Take the port position, tilt angles, and probe position in relation to the port and convert it to be used by this object.

        Parameters
        ----------
        port_r : float (m)
            Radial port position.
        port_phi : float (radians)
            Longitude of port.
        port_theta : float (radians)
            Latitude of port.
        alpha, beta, gamma : float (radians)
            1st, 2nd, and 3rd Euler angle of port.
        x_port : float (radians)
            Longitudanally directed position of probe in relation to port. (North when clocking=0)
        y_port : float (radians)
            Latitudinally directed position of probe in relation to port. (East when clocking=0)
        z_port : float (radians)
            Inward position of probe in relation to port. (Down)
        clocking : float (radians)
            Counter-clockwise angle of the probe measured from when the x direction is pointed north.

        Notes
        -----
        There is no inverse to get from cartesian to port coordinates as the coorinate is not unique.
        """
        port_cartesian = np.array((
            port_r * np.cos(port_theta) * np.cos(port_phi),
            port_r * np.cos(port_theta) * np.sin(port_phi),
            port_r * np.sin(port_theta)
        ))

        probe_coords = np.array((x_port, y_port, z_port))
        probe_cartesian = np.dot(
            self._get_transformation_matrix(
                port_phi, port_theta, alpha, beta, gamma, clocking
            ), probe_coords
        ) + port_cartesian
        self.cartesian = probe_cartesian

    def _get_transformation_matrix(self, port_phi, port_theta, alpha, beta, gamma, clocking):
        """For internal use only.
        Get the matrix that transforms local probe coordinates into machine cartesian coordinates.

        Parameters
        ----------
        port_phi : float (radians)
            Longitude of port.
        port_theta : float (radians)
            Latitude of port.
        alpha, beta, gamma : float (radians)
            1st, 2nd, and 3rd Euler angle of port.
        clocking : float (radians)
            Counter-clockwise angle of the probe measured from when the x direction is pointed north.

        Returns
        -------
        transformation_matrix : np.array[float]
            3 x 3 matrix to multiply an (x, y, z) vector by.
        """
        clocking_rotation = np.array((
            (   np.cos(-clocking), -np.sin(-clocking),    0),
            (   np.sin(-clocking), np.cos(-clocking),     0),
            (   0,                 0,                     1)
        ))

        rotation_x = np.array((
            (   1,          0,             0           ),
            (   0,          np.cos(beta),  -np.sin(beta)  ),
            (   0,          np.sin(beta),  np.cos(beta)   )
        ))
        rotation_y = np.array((
            (   np.cos(alpha), 0,          -np.sin(alpha) ),
            (   0,             1,          0           ),
            (   np.sin(alpha), 0,          np.cos(alpha)  )
        ))
        rotation_z = np.array((
            (   np.cos(gamma), -np.sin(gamma),0           ),
            (   np.sin(gamma), np.cos(gamma), 0           ),
            (   0,             0,             1           )
        ))

        # The rotation matrix is ordered as Ry * Rx * Rz for intrinsic rotations (i.e. rotating about the local axes).
        rotation_matrix = np.matmul(np.matmul(rotation_y, rotation_x), rotation_z)

        ned_to_cartesian = np.array((
            (   -np.sin(port_theta) * np.cos(port_phi),   -np.sin(port_phi),     -np.cos(port_theta) * np.cos(port_phi)),
            (   -np.sin(port_theta) * np.sin(port_phi),   np.cos(port_phi),      -np.cos(port_theta) * np.sin(port_phi)),
            (   np.cos(port_theta),                       0,                     -np.sin(port_theta))
        ))

        return np.matmul(np.matmul(ned_to_cartesian, rotation_matrix), clocking_rotation)

    @property
    def cartesian(self):
        """
        Returns
        -------
        nd.array[float]
            [x, y, z] position in meters.

        Notes
        -----
        `x` increases towards the top of the BRB.
        `y` increases towards the interior of Sterling (`-y` points towards the outside).
        `z` increases towards the north pole which is towards the alpha power supplies.
        """
        return np.array([self._x, self._y, self._z])
    
    @cartesian.setter
    def cartesian(self, coordinate):
        """
        Parameters
        ----------
        coordinate : np.array[float]
            `[x, y, z]` Position of coordinate in meters.
        """
        self._x = coordinate[0]
        self._y = coordinate[1]
        self._z = coordinate[2]

    @property
    def cylindrical(self):
        """
        Returns
        -------
        np.array[float]
            [rho, phi, z] position in meters and radians.

        Notes
        -----
        `rho` increases outward from the `z` axis.
        `phi` starts at `0` on the half circle going through the poles and the top of the sphere. This is the longitude.
        `z` is directed along the axis through the poles and increases towards the alpha power supplies.
        """
        return np.array([
            (self._x**2 + self._y**2)**0.5, # rho(x, y, z)
            np.arctan2(self._y, self._x), # phi(x, y, z)
            self._z # z(x, y, z)
        ])

    @cylindrical.setter
    def cylindrical(self, coordinate):
        """
        Parameters
        ----------
        coordinate : np.array[float]
            `[rho, phi, z]` Position of coordinate in meters and radians.
        """
        self._x = coordinate[0] * np.cos(coordinate[1])
        self._y = coordinate[0] * np.sin(coordinate[1])
        self._z = coordinate[2]

    @property
    def spherical(self):
        """
        Returns
        -------
        np.array[float]
            [r, theta, phi] position in meters and radians.

        Notes
        -----
        `r` is the distance from the center of the BRB.
        `theta` starts at 0 on the `xy`-plane and increases towards the north pole. This is the latitude.
        `phi` starts at `0` on the half circle going through the poles and the top of the sphere (`+x`). This is the longitude.
        """
        r = (self._x**2 + self._y**2 + self._z**2)**0.5
        return np.array([
            r, # r(x, y, z)
            np.where(r == 0, 0, np.arcsin(self._z / r)), # theta(x, y, z)
            np.arctan2(self._y, self._x) # phi(x, y, z)
        ])
    
    @spherical.setter
    def spherical(self, coordinate):
        """
        Parameters
        ----------
        coordinate : np.array[float]
            `[r, theta, phi]` Position of coordinate in meters and radians.
        """
        self._x = coordinate[0] * np.cos(coordinate[1]) * np.cos(coordinate[2])
        self._y = coordinate[0] * np.cos(coordinate[1]) * np.sin(coordinate[2])
        self._z = coordinate[0] * np.sin(coordinate[1])

    def __add__(self, otherPosition):
        if isinstance(otherPosition, Position):
            return Position(self.cartesian + otherPosition.cartesian)
        else:
            return Position(self.cartesian + otherPosition)
        
    def to_tf_centered_coordinates(self, inverse=False):
        """
        Adjust the position by setting the z-axis to be along the TF coil where 
        the TF coil has been lowered for the insert. This moves the origin 2.2 
        cm in the -x direction.

        Parameters
        ----------
        inverse : bool, default=False
            Whether to actually do the inverse and raise the TF coil and return 
            the coordinates to the ball centered coordinates.
        """
        try:
            self.cartesian -= np.array([-0.022, 0, 0]) * (-1 if inverse else +1)
        except ValueError:
            self.cartesian -= np.array([-0.022, 0, 0])[:, np.newaxis] * (-1 if inverse else +1)

    def add_insert(self, hemisphere):
        """
        Adjust the position by moving the coordinates depending on whether the attached probe is on the north or south hemisphere.
        
        Parameters
        ----------
        hemisphere : str ('north' or 'n', 'south' or 's')
            The hemisphere from which this probe coordinate is coming from. Case independent.

        Notes
        -----
        This is useful when you have a coordinate such as a probe position and the insert 
        is attached. Then if the probe is coming from the northern or southern hemisphere 
        the coordinate has to be adjusted to the new coordinate system.
        """
        if hemisphere.lower() in ('north', 'n'):
            self.cartesian += np.array([0, 0, 0.0498])
        elif hemisphere.lower() in ('south', 's'):
            self.cartesian -= np.array([0, 0, 0.0498])
        else:
            raise ValueError("Invalid hemisphere value passed of '{}'. Valid options are ('north' or 'n', 'south' or 's').".format(hemisphere))
        
    @staticmethod
    def cylindrical_to_cartesian(coordinate):
        """
        Parameters
        ----------
        coordinate : np.array[float]
            `[rho, phi, z]` Position of coordinate in meters.
        """
        return np.array((
            coordinate[0] * np.cos(coordinate[1]),
            coordinate[0] * np.sin(coordinate[1]),
            coordinate[2]
        ))

    @staticmethod
    def cartesian_to_cylindrical(coordinate):
        """
        Parameters
        ----------
        coordinate : np.array[float]
            `[x, y, z]` Position of coordinate in meters.
        """
        return np.array((
            (coordinate[0]**2 + coordinate[1]**2)**0.5,
            np.arctan2(coordinate[1], coordinate[0]),
            coordinate[2]
        ))


class Vector:
    def __init__(self, position, cartesian_vector=None):
        """
        Contains the position and component magnitudes for a vector in space. Can easily transfer between coordinate systems.

        Attributes
        ----------
        position : Position
            Position of vector.
        cartesian : np.array[float]
            `[x, y, z]` magnitudes of vector. Independent of vector position.
        cylindrical : np.array[float]
            `[rho, phi, z]` magnitudes of vector. Dependent on vector position.
        spherical : np.array[float]
            `[r, theta, phi]` magnitudes of vector. Dependent on vector position.

        Notes
        -----
        This can handle either individual vectors or multiple vectors. If this 
        is for a single vector then the `position` object should be for a 
        single point and the vector coordinate should be passed as a 1D array 
        of shape `(3,)` for any of the attributes. If this is for `NUM_VECTORS` 
        vectors then the `position` object should represent `NUM_VECTORS` points 
        and the vector coordinates shoulbe be then passed as a 2D array of shape 
        `(3, NUM_VECTORS)`.

        Examples
        --------
        >>> vec = Vector(Position([2, 2, 2]))
        >>> 
        """
        if isinstance(position, Position):
            self.position = position
        else:
            self.position = Position(position)

        if cartesian_vector is None:
            self._x = None
            self._y = None
            self._z = None
        else:
            self.cartesian = cartesian_vector

    def __repr__(self) -> str:
        return "Vector(position={}, cartesian={})".format(self.position, self.cartesian)

    def from_port_object(self, port : Port, x_direction, y_direction, z_direction, clocking=None):
        """
        Take the port object and convert it to be used by this object.

        Parameters
        ----------
        port : Port
            Port object to convert to a position.
        x_direction, y_direction, z_direction : float
            Longitudanal, latitudinal, and radial component of vector. (North, East, Down).
        clocking : float (radians), default=None
            Counter-clockwise angle of the probe measured from when the x
            direction is pointed north. If not given then `port.clocking_rad`
            is used.

        Notes
        -----
        There is no inverse to get from cartesian to port coordinates as the coorinate is not unique.
        """
        self.from_port(
            port.long_rad, port.lat_rad, port.alpha_rad, port.beta_rad, port.gamma_rad,
            x_direction, y_direction, z_direction, port.clocking_rad if clocking is None else clocking
        )

    def from_port(self, port_phi, port_theta, alpha, beta, gamma, x_direction, y_direction, z_direction, clocking):
        """
        Take the port position, tilt angles, direction, and clocking and convert it to be used by this object.
        The clocking is defined such that the probe x direction is pointed to the north.

        Parameters
        ----------
        port_phi : float (radians)
            Longitude of port.
        port_theta : float (radians)
            Latitude of port.
        alpha, beta, gamma : float (radians)
            1st, 2nd, and 3rd Euler angle of port.
        x_direction : float
            Longitudanal component of vector. (North when clocking=0)
        y_direction : float
            Latitudinal component of vector. (East)
        z_direction : float
            Radially inward component of vector. (Down)
        clocking : float (radians)
            Counter-clockwise angle of the probe measured from when the x direction is pointed north.

        Notes
        -----
        There is no inverse to get from cartesian to port coordinates as the coorinate is not unique.
        """
        direction_coords = np.array((x_direction, y_direction, z_direction))
        direction_cart = np.dot(self.position._get_transformation_matrix(port_phi, port_theta, alpha, beta, gamma, clocking), direction_coords)
        self.cartesian = direction_cart

    @property
    def cartesian(self):
        """
        Returns
        -------
        np.array[float]
            `[x, y, z]` magnitudes of vector. Independent of vector position.
        """
        return np.array([
            self._x,
            self._y,
            self._z
        ])
    
    @cartesian.setter
    def cartesian(self, vector):
        """
        Parameters
        ----------
        vector : np.array[float]
            `[x, y, z]` magnitudes of vector.
        """
        self._x = vector[0]
        self._y = vector[1]
        self._z = vector[2]

    @property
    def cylindrical(self):
        """
        Returns
        -------
        np.array[float]
            `[rho, phi, z]` magnitudes of vector. Dependent on vector position.
        """
        phi_vec = self.position.cylindrical[1]
        return np.array([
            np.cos(phi_vec) * self._x + np.sin(phi_vec) * self._y, # rho(position, x, y, z)
            -np.sin(phi_vec) * self._x + np.cos(phi_vec) * self._y, # phi(position, x, y, z)
            self._z # z(position, x, y, z)
        ])
    
    @cylindrical.setter
    def cylindrical(self, vector):
        """
        Parameters
        ----------
        vector : np.array[float]
            `[rho, phi, z]` magnitudes of vector.
        """
        phi_vec = self.position.cylindrical[1]
        self._x = np.cos(phi_vec) * vector[0] - np.sin(phi_vec) * vector[1]
        self._y = np.sin(phi_vec) * vector[0] + np.cos(phi_vec) * vector[1]
        self._z = vector[2]

    @property
    def spherical(self):
        """
        Returns
        -------
        np.array[float]
            `[r, theta, phi]` magnitudes of vector. Dependent on vector position.
        """
        phi_vec = self.position.spherical[2]
        theta_vec = np.pi / 2 - self.position.spherical[1]
        return np.array([
            [np.sin(theta_vec) * np.cos(phi_vec), np.sin(theta_vec) * np.sin(phi_vec), np.cos(theta_vec)],
            [np.cos(theta_vec) * np.cos(phi_vec), np.cos(theta_vec) * np.sin(phi_vec), -np.sin(theta_vec)],
            [-np.sin(phi_vec), np.cos(phi_vec), 0]
        ]) @ np.array([self._x, self._y, self._z])
    
    @spherical.setter
    def spherical(self, vector):
        """
        Parameters
        ----------
        vector : np.array[float]
            `[r, theta, phi]` magnitudes of vector.
        """
        phi_vec = self.position.spherical[2]
        theta_vec = np.pi / 2 - self.position.spherical[1]
        self._x = np.sin(theta_vec) * np.cos(phi_vec) * vector[0] + np.cos(theta_vec) * np.cos(phi_vec) * vector[1] - np.sin(theta_vec) * vector[2]
        self._y = np.sin(theta_vec) * np.sin(phi_vec) * vector[0] + np.cos(theta_vec) * np.sin(phi_vec) * vector[1] + np.cos(theta_vec) * vector[2]
        self._z = np.cos(phi_vec) * vector[0] - np.sin(phi_vec) * vector[1]

    @staticmethod
    def cylindrical_to_cartesian(vector, position):
        """
        Parameters
        ----------
        vector : np.array[float]
            `[rho, phi, z]` magnitudes of vector.
        position : np.array[float]
            `[rho, phi, z]` positions of vector origin.
        """
        phi = position[1]
        return np.array([
            np.cos(phi) * vector[0] - np.sin(phi) * vector[1], # rho(position, x, y, z)
            np.sin(phi) * vector[0] + np.cos(phi) * vector[1], # phi(position, x, y, z)
            vector[2] # z(position, x, y, z)
        ])

    @staticmethod
    def cartesian_to_cylindrical(vector, position):
        """
        Parameters
        ----------
        vector : np.array[float]
            `[x, y, z]` magnitudes of vector.
        position : np.array[float]
            `[x, y, z]` positions of vector origin.

        Returns
        -------
        np.array[float]
            `[rho, phi, z]` magnitudes of vector.
        """
        phi = np.arctan2(position[1], position[0])
        return np.array((
            np.cos(phi) * vector[0] + np.sin(phi) * vector[1],
            -np.sin(phi) * vector[0] + np.cos(phi) * vector[1],
            vector[2]
        ))
