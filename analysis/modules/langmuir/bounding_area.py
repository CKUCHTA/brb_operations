"""
Create the area around each IV point and draw lines that bound the upper and lower regions.
"""
import logging
import numpy as np
import sympy as sp
from modules.langmuir.geometry import Line, Ellipse


# TODO: Study the convergence rate for a variety of input paramaters.
def create_bounding_lines(v_values: np.array, i_values: np.array, *error_vectors: np.array):
    """
    Create the upper and lower piecewise linear curves.
    
    Parameters
    ----------
    v_values : np.array
        Probe voltages
    i_values : np.array
        Probe currents
    *error_vectors : (2,) np.array or (# of points, 2) np.array of floats
        Standard deviations in directions given by each 2D vector. We assume there is a normal distribution in all error_vector directions. There can be any number of error_vectors.

    Returns
    -------
    upper_points, lower_points : (# of points, 2) np.array of floats
        Points that describe the upper and lower curves.
    """
    if v_values.shape != i_values.shape:
        raise ValueError("I and V arrays are different shape ({} and {}).".format(v_values.shape, i_values.shape))

    # Tile the vectors for each data point if we are given a single error vector for all points.
    num_points = v_values.size
    for i in range(len(error_vectors)):
        if error_vectors[i].shape == (2,):
            error_vectors[i] = np.tile(error_vectors[i], (num_points, 1))
            
    upper_points = np.zeros((num_points, 2))
    lower_points = np.zeros_like(upper_points)

    ellipses = [construct_error_ellipse(v_values[i], i_values[i], *(err_vec[i] for err_vec in error_vectors)) for i in range(num_points)]
    upper_coeffs = []
    lower_coeffs = []
    for i in range(num_points - 1):
        all_coeffs, all_points = approximate_ellipse_tangents(ellipses[i], ellipses[i + 1], return_points=True)
        # We use the tangent points for the points on the first and last ellipse (there aren't two lines that could intersect there).
        if i == 0:
            upper_points[0] = all_points[1][0]
            lower_points[0] = all_points[0][0]
        elif i == num_points - 2:
            upper_points[-1] = all_points[1][1]
            lower_points[-1] = all_points[0][1]
            
        # Also store the points in case we have issues in the future getting the line intersection.
        upper_points[i] = all_points[1][0]
        lower_points[i] = all_points[0][0]

        upper_coeffs.append(all_coeffs[1])
        lower_coeffs.append(all_coeffs[0])

    for coeff_list, points in zip((upper_coeffs, lower_coeffs), (upper_points, lower_points)):
        for i in range(len(coeff_list) - 1):
            try:
                point = Line.coef_intersection(coeff_list[i], coeff_list[i + 1])
            except ValueError:
                logging.info("Lines are parallel so using tangent point of ellipse.")
            else:
                points[i + 1] = point

    return upper_points, lower_points


def construct_error_ellipse(x0: float, y0: float, *error_vectors: np.array):
    # Construct the covariance matrices.
    cov_matrices = [get_covariance_matrix(vec) for vec in error_vectors]

    # Construct the full covariance matrix that is equal to p_errors + q_errors
    full_cov_matrix = sum(cov_matrices)

    a = full_cov_matrix[0, 0]
    # The covariance matrix is symmetric so we need only store this.
    b = full_cov_matrix[0, 1]
    c = full_cov_matrix[1, 1]
    # Get the variances along the ellipse axis which are the eigenvalues. We need to eventually take the sqrt to get the stdevs.
    ellipse_a2 = 0.5 * (a + c + (a**2 - 2 * a * c + c**2 + 4 * b**2)**0.5)
    ellipse_b2 = 0.5 * (a + c - (a**2 - 2 * a * c + c**2 + 4 * b**2)**0.5)

    alpha = np.arctan2(ellipse_a2 - a, b)

    return Ellipse(np.rad2deg(alpha), ellipse_a2**0.5, ellipse_b2**0.5, x0, y0)

def get_covariance_matrix(vec):
    return np.array((
        (vec[0]**2, vec[0] * vec[1]),
        (vec[0] * vec[1], vec[1]**2)
    ))

def exact_ellipse_tangents(e1: Ellipse, e2: Ellipse):
    """Get all lines that are exactly tangent to both ellipses.
    
    Paramaters
    ----------
    e1, e2 : Ellipse
        Ellipses to get tangent lines of.
    
    Returns
    -------
    list of tuples
        Each tuple is ordered as `(c1, c2, c3)`.

    Notes
    -----
    This is a symbolic solver that takes a while to get the solution but returns exact values.
    """
    # Tangent of two ellipses code taken from https://www.mathworks.com/matlabcentral/answers/635464-how-to-find-common-tangents-between-2-ellipses#answer_584222
    # Create the coefficient variables. This gives a line of the form `c1*x + c2*y + c3 = 0`
    c1, c2, c3 = sp.symbols('c1, c2, c3')
    # Create the equations
    equations = [
        sp.Matrix([c1, c2, c3]).T * e1.get_equation_matrix(True)**-1 * sp.Matrix([c1, c2, c3]),
        sp.Matrix([c1, c2, c3]).T * e2.get_equation_matrix(True)**-1 * sp.Matrix([c1, c2, c3]),
        c1**2 + c2**2 + c3**2 - 1
    ]

    # Solve the system of equations
    solution = sp.solve(equations, [c1, c2, c3])

    return solution

def approximate_ellipse_tangents(e1: Ellipse, e2: Ellipse, num_iterations=1, return_points=False):
    """Get some lines that are approximately tangent to both ellipses.
    
    Paramaters
    ----------
    e1, e2 : Ellipse
        Ellipses to get tangent lines of.
    num_iterations : int, default=1
    return_points : bool, default=False
        Return the points that the line goes through as well as the coefficients.
        Returned as a tuple of `(tangent_coeffs, tangent_points)` if `return_points` is `True`. 
    
    Returns
    -------
    tangent_coeffs : list of tuples
        Each tuple is ordered as `(c1, c2, c3)`. This coefficient vector is normalized.
    tangent_points : list of tuples of np.array
        Ordered as `[(np.array((cw_x1, cw_y1)), np.array((cw_x2, cw_y2))), (np.array((ccw_x1, ccw_y1)), np.array((ccw_x2, ccw_y2)))]`.
        Each tuple is for a single line. Each tuple has two points and these points are the points on the ellipses.

    Notes
    -----
    This is an iterative solver that approximates the exact solution. It also only gets the lines that don't 
    cross a line between ellipse centers. The returned values are in the order `('cw', 'ccw')`.
    """
    if num_iterations == 1:
        tangent_line = e2.center - e1.center
        # Get the points that are tangent to the new tangent line.
        e1_points = e1.get_tangent_points(tangent_line)
        e2_points = e2.get_tangent_points(tangent_line)

        final_lines = (Line(intersection_points=(e1_points[i], e2_points[i])) for i in range(len(e1_points)))
        tangent_coeffs = [final_line.coefficients for final_line in final_lines]
        tangent_points = [(e1_points[i], e2_points[i]) for i in range(len(e1_points))]
    else:
        tangent_coeffs = []
        tangent_points = []

        # We do two loops. One for which the points are above the center line and the other below.
        for side in ('cw', 'ccw'):
            tan_points = (e1.center, e2.center)
            for _ in range(num_iterations):
                tangent_line = tan_points[1] - tan_points[0]
                # Get the points that are tangent to the new tangent line.
                e1_point = e1.get_tangent_points(tangent_line, point_side=side)
                e2_point = e2.get_tangent_points(tangent_line, point_side=side)

                tan_points = (e1_point, e2_point)
            
            final_line = Line(intersection_points=(e1_point, e2_point))
            tangent_coeffs.append(final_line.coefficients)
            tangent_points.append(tan_points)

    if return_points:
        return (tangent_coeffs, tangent_points)
    else:
        return tangent_coeffs


if __name__ == "__main__":
    from random import random, gauss, uniform
    import matplotlib.pyplot as plt
    logging.basicConfig(level=logging.INFO)

    for _ in range(10):
        alpha = uniform(0, 2 * np.pi)
        error_vecs = [uniform(1, 2) * np.array((np.cos(alpha), np.sin(alpha))), uniform(1, 2) * np.array((-np.sin(alpha), np.cos(alpha)))]
        center = np.array((uniform(-1, 1), uniform(-1, 1)))
        e1 = construct_error_ellipse(center[0], center[1], *error_vecs)

        def similar_ellipse(e: Ellipse):
            # We want e2 to be similar to e1.
            dalpha = 30
            alpha = gauss(e.alpha_deg, dalpha)
            da = 0.25
            a = gauss(e.a, da)
            db = 0.25
            b = gauss(e.b, db)
            distance = uniform(5, 10)
            distance += (a**2 + b**2)**0.5
            direction = gauss(0, np.pi / 6)
            center = np.array((e.x0 + distance * np.cos(direction), e.y0 + distance * np.sin(direction)))
            return Ellipse(alpha, a, b, *center)

        num_ellipses = 5
        ellipses = [e1]
        for _ in range(num_ellipses - 1):
            ellipses.append(similar_ellipse(ellipses[-1]))

        t = np.linspace(0, 1, num=1000)
        fig = plt.figure()
        ax = fig.add_subplot()
        for e in ellipses:
            e_param = e.parametrize(include_axis=True)
            ax.plot(*e_param(t))

        x0_vals = np.zeros(len(ellipses))
        y0_vals = np.zeros_like(x0_vals)
        a_vecs = np.zeros((len(ellipses), 2))
        b_vecs = np.zeros_like(a_vecs)
        for i, e in enumerate(ellipses):
            x0_vals[i] = e.x0
            y0_vals[i] = e.y0
            a_vecs[i] = e.a_vector
            b_vecs[i] = e.b_vector

        # Plot just the lines that are tangent to each ellipse.
        for i in range(num_ellipses - 1):
            lines, point_pairs = approximate_ellipse_tangents(ellipses[i], ellipses[i + 1], return_points=True)
            for line, point_pair in zip(lines, point_pairs):
                l = Line(line)
                l_param = l.parameterize((point_pair[0][0], point_pair[1][0]), (point_pair[0][1], point_pair[1][1]))
                plt.plot(*l_param(t), color='grey', linestyle='dashed')

        upper_points, lower_points = create_bounding_lines(x0_vals, y0_vals, a_vecs, b_vecs)
        line_kwords = {'linewidth': 2, 'alpha': 0.5}
        upper_line = plt.plot(upper_points[:, 0], upper_points[:, 1], **line_kwords)
        plt.scatter(upper_points[:, 0], upper_points[:, 1], color=upper_line[0].get_color())
        lower_line = plt.plot(lower_points[:, 0], lower_points[:, 1], **line_kwords)
        plt.scatter(lower_points[:, 0], lower_points[:, 1], color=lower_line[0].get_color())

        ax.set_aspect('equal')

        plt.show()
