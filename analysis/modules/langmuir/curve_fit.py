import numpy as np
from scipy import odr
from scipy.optimize import root
import matplotlib.pyplot as plt
import tqdm
from numbers import Number
import logging
import random
from modules.langmuir.Chen_Laframboise_parametrized_IV import total_current

from te_probe1.get_data import Te_Probe1_Data
from te_probe1.raw_data_functions import get_V_probe_function, get_I_probe_function
from te_probe3.get_data import Te_Probe3_Data


# Constants
M_ELECTRON = 9.109e-31 # kg
M_PROTON = 1.673e-27 # kg
Q_ELECTRON = 1.602e-19 # C

def to_n_conversion(n_converted):
    """
    Convert the value of `n` from the ODR output to the actual value.
    """
    return 10**n_converted

def to_n_conversion_deriv(n_converted):
    """Get the derivative of the conversion function."""
    return np.log(10) * 10**n_converted

def from_n_conversion(n):
    """
    Convert the value of `n` to the value that ODR uses.
    """
    return np.log10(n)

T_e_min = 0.05 # eV
T_e_max = 100 # eV
def to_T_e_conversion(T_e_converted):
    """
    Convert the value of `T_e` from the ODR output to the actual value.
    """
    # return T_e_min + 10**T_e_converted
    return T_e_min + (0.5 + np.arctan(T_e_converted) / np.pi) * (T_e_max - T_e_min)

def to_T_e_conversion_deriv(T_e_converted):
    """Get the derivative of the conversion function."""
    # return np.log(10) * 10**T_e_converted
    return (T_e_max - T_e_min) / (np.pi * (T_e_converted**2 + 1))

def from_T_e_conversion(T_e):
    """
    Convert the value of `T_e` to the value that ODR uses.
    """
    # return np.log10(T_e - T_e_min)
    return -np.tan(np.pi * (T_e_min + T_e_max - 2 * T_e) / (2 * (T_e_max - T_e_min)))

def to_val_conversion(odr_output : odr.Output):
    """Take an output from ODR and convert the relevant terms."""
    unconverted_n, unconverted_T_e, V_plasma = odr_output.beta
    unconverted_n_sd, unconverted_T_e_sd, V_plasma_sd = odr_output.sd_beta

    odr_output.beta = np.array([to_n_conversion(unconverted_n), to_T_e_conversion(unconverted_T_e), V_plasma])
    odr_output.sd_beta = np.array([
        to_n_conversion_deriv(unconverted_n) * unconverted_n_sd,
        to_T_e_conversion_deriv(unconverted_T_e) * unconverted_T_e_sd,
        V_plasma_sd
    ])
    odr_output.sd_beta_bounds = np.array([
        [to_n_conversion(unconverted_n - unconverted_n_sd), to_n_conversion(unconverted_n + unconverted_n_sd)],
        [to_T_e_conversion(unconverted_T_e - unconverted_T_e_sd), to_T_e_conversion(unconverted_T_e + unconverted_T_e_sd)],
        [V_plasma - V_plasma_sd, V_plasma + V_plasma_sd]
    ])
    # TODO: Also convert the covariance matrix.

    return odr_output

def weighting_function(T_e, V_plasma, V_p, normalize=True):
    """
    Weight the probe current based on how far into the electron saturation 
    region the measurement is.

    Parameters
    ----------
    T_e : float
        Electron temperature in eV.
    V_plasma : float
        Plasma potential in V.
    V_p : np.array[float]
        1D array of probe voltages.
    normalize : bool, default=True
        If `True` then the weights are normalized.
    """
    # TODO: Figure out a good weighting scheme.
    unnormalized_weights = np.ones_like(V_p)
    if normalize:
        return unnormalized_weights / np.sum(unnormalized_weights)
    else:
        return unnormalized_weights

def implicit_model_function(B, x, A_p, V_p_func, I_p_func, tip_numbers, tip_radius, model_name):
    """
    The implicit model to use ODR on.
    
    ODR operates by minimizing the sum of squares of the residuals. This 
    function is the residuals of the model and the data.

    Parameters
    ----------
    B : tuple
        Tuple of the form `(n_converted, T_e_converted, V_plasma)`.
    x : np.array[float]
        2D array of the form `np.array((v_s, v_n, V_b))`.
    A_p : np.array[float]
        Actual areas of each tip.
    V_p_func : func(v_s, v_n, V_b, tip_number) -> np.array[float]
        Function that takes a value of `v_s`, `v_n`, `V_b`, and `tip_number` 
        and changes it into an array of `V_probe` values.
    I_p_func : func(v_s, v_n, tip_number) -> np.array[float]
        Function that takes a value of `v_s`, `v_n`, and `tip_number` and 
        changes it into an array of `I_probe` values.
    tip_numbers : np.array[int]
        1D array of shape `(# of tips,)` containing the number for each tip.
    tip_radius : float
        Radius of probe tip.
    model_name : str
        IV curve model to use.

    Returns
    -------
    np.array[float]
        1D array of differences between theoretical and actual current density.
    """
    # print(to_n_conversion(B[0]), to_T_e_conversion(B[1]), B[2])
    # print(model_I_p(V_p_func(x[0], x[1], x[2]), to_n_conversion(B[0]), to_T_e_conversion(B[1]), B[2], A_p, tip_radius=tip_radius))
    V_p_values = V_p_func(x[0], x[1], x[2], tip_numbers)
    J_p_difference = I_p_func(x[0], x[1], tip_numbers) / A_p - model_J_p(V_p_values, to_n_conversion(B[0]), to_T_e_conversion(B[1]), B[2], A_p, tip_radius=tip_radius, model_name=model_name)
    weights = weighting_function(to_T_e_conversion(B[1]), B[2], V_p_values)
    return J_p_difference * weights

def model_J_p(V_p, n, T_e, V_plasma, A_p, tip_radius=Te_Probe1_Data.TIP_RADIUS, model_name='chen_approx'):
    """
    The theoretical probe current density.

    There are many different theories for how probe current relies on plasma 
    parameters and probe voltage. Here we have implemented three options:    

    `'Chen'`: This is valid for all probe voltages and is the most accurate. 
    For `V_p < V_plasma`, the ion current is a parametrized version of the 
    Bernstein-Rabinowitz-Laframboise model by Chen. The electron current is 
    just an exponential. For `V_p > V_plasma`, the ion current is 0 (since the 
    ions are repelled and very cold) and the electron current uses the planar 
    probe approximation corrected for cylindrical geometry. The planar probe 
    approximation requires an integral to calculate the distance from the probe 
    to the edge of the sheath. This integral is done numerically.

    `'Chen_approx'`: This is the same as `'Chen'` but the integral for the
    planar probe approximation is approximated using a polynomial fit. This is
    much faster but less accurate.

    `'Hutchinson'`: This is the simplest model and is valid for `V_p < V_plasma`.
    It is a simple exponential for the electron current and the ion current is 
    assumed to be constant.

    Parameters
    ----------
    V_p : np.array[float]
        1D array of probe voltages.
    n, T_e, V_plasma : float
        Plasma density, electron temperature, and plasma potential.
    A_p : np.array[float]
        Actual areas of each tip.
    tip_radius : float, default=Te_Probe1_Data.TIP_RADIUS
        Radius of the probe tip.
    model_name : str, default='chen_approx'
        IV curve model to use. Should one of `'chen'`, `'chen_approx'`, or `'hutchinson'`.

    Returns
    -------
    J_p : np.array[float]
        1D array of probe current densities.
    """
    tip_length = (A_p - np.pi * tip_radius**2) / (2 * np.pi * tip_radius)
    if model_name.lower() == 'chen':
        return total_current(tip_radius, tip_length, V_p, V_plasma, n, T_e, approximation=False) / A_p
    elif model_name.lower() == 'chen_approx':
        return total_current(tip_radius, tip_length, V_p, V_plasma, n, T_e, approximation=True) / A_p
    elif model_name.lower() == 'hutchinson':
        # This is equation 3.2.33 from Hutchinson (2002).
        # We assume that we are using a hydrogen plasma and thus m_i = m_p.
        # Since the debye length is much smaller than the probe we'll assume the sheath area = probe area.
        return n * -Q_ELECTRON * A_p * (Q_ELECTRON * T_e / M_PROTON)**0.5 * (
            0.5 * ((2 * M_PROTON) / (np.pi * M_ELECTRON))**0.5 * np.exp((V_p - V_plasma) / T_e) - np.exp(-0.5)
        ) / A_p
    else:
        raise ValueError("IV model called '{}' is not available. Valid options are 'Chen', 'Chen_approx', or 'Hutchinson'.".format(model_name))

def non_railed_measurements_indeces(v_s, v_n):
    """
    Identify tips signals that are outside of the digitizer's measurement range.

    Identify which tip indeces are too large and thus the digitizer doesn't 
    correctly measure them. Then return the indeces that ARE valid.

    Parameters
    ----------
    v_s, v_n : np.array[float]
        1D array of values for the probe measurements at a single time point of shape `(# of tips,)`. 
        These should be raw data as smoothed data may return valid indeces that should be invalid.

    Returns
    -------
    indeces : np.array[int]
        1D array of indeces that have valid measurements at this time point.
    """
    return np.where(np.logical_and(np.abs(v_s) < 1, np.abs(v_n) < 1))[0]

def try_odr(data, model, initial_guess, print_trials=False):
    """
    Try fitting the model to the data and raise an error if fitting fails.
    """
    odr_object = odr.ODR(data, model, beta0=initial_guess)
    output = odr_object.run()

    if print_trials:
        output.pprint()

    # From ODRPACK, if `info >= 5` then we have a questionable solution.
    if output.info >= 5:
        raise odr.OdrError("Could not successfully fit the given data using the initial conditions. Try different initial conditions? Stop reason was:\n{}".format(output.stopreason))

    return output

def solve_odr(
        v_s, v_n, V_b, A_p, V_p_func, I_p_func, initial_guess, 
        working_tips=np.full(Te_Probe1_Data.NUM_TIPS, True), s_v_s=0.02, s_v_n=0.02, s_V_b=2, 
        random_seed=None, check_fit=False, 
        tip_numbers=np.arange(1, Te_Probe1_Data.NUM_TIPS + 1), tip_radius=Te_Probe1_Data.TIP_RADIUS,
        model_name='chen_approx'
    ):
    """
    Solve for the plasma parameters using Orthogonal Distance Regression.

    This function takes the probe measurements and fits a curve to them 
    accounting for errors in the raw signal measurements
    
    Parameters
    ----------
    v_s : np.array[float]
        1D array of shape `(# of tips,)` of the raw signal measurements.
    v_n : np.array[float]
        1D array of shape `(# of tips,)` of the raw noise measurements.
    V_b : np.array[float]
        1D array of shape `(# of tips,)` of the bias potential measurements.
    A_p : np.array[float]
        1D array of shpae `(# of tips,)` of actual areas of each tip in m^2.
    V_p_func : func(v_s, v_n, V_b, tip_number) -> np.array[float]
        Function that takes a value of `v_s`, `v_n`, `V_b`, and `tip_number` 
        and changes it into an array of `V_probe` values.
    I_p_func : func(v_s, v_n, tip_number) -> np.array[float]
        Function that takes a value of `v_s`, `v_n`, and `tip_number` and 
        changes it into an array of `I_probe` values.
    initial_guess : np.array[float]
        1D array of 3 floats for an estimate of `(n, T_e, V_plasma)`. 
    working_tips : np.array[bool], default=np.full(Te_Probe1_Data.NUM_TIPS, True)
        1D array of shape `(# of tips,)` of which tips are working. By default, 
        all tips are assumed to be working.
    s_v_s, s_v_n, s_V_b : float or np.array
        Standard deviation of measurements.
    random_seed : None or int, default=None
        Seed for random initial guess sampler to use.
    check_fit : bool, default=False
        Plot the IV points and the fit curve.
    tip_numbers : np.array[int], default=np.arange(1, 17)
        1D array of shape `(# of tips,)` containing the number for each tip.
    tip_radius : float, default=Te_Probe1_Data.TIP_RADIUS
        Radius of the tip in meters. The default is the tip radius for the Te probe.
    model_name : str, default='chen_approx'
        IV curve model to use as described in `model_J_p`. Should be one of 
        `'chen'`, `'chen_approx'`, or `'hutchinson'`.

    Returns
    -------
    output : odr.Output
        Output from ODR for the best fit using a variety of initial guesses. The 
        returned result is of the form `(n, T_e, V_plasma)`.
    usable_point_indeces : np.array[int]
        Indeces of which points were used.
    """
    num_tips = v_s.size
    if num_tips != v_s.size or num_tips != v_n.size or num_tips != V_b.size or num_tips != A_p.size or num_tips != working_tips.size or num_tips != tip_numbers.size:
        raise ValueError("`v_s`, `v_n`, `V_b`, `A_p`, `working_tips`, and `tip_numbers` must all have the same size.")

    data_points = np.array((v_s, v_n, V_b))

    # Change the standard deviations into arrays if they aren't already.
    standard_devs = np.ones((3, num_tips))
    for i, standard_dev in enumerate((s_v_s, s_v_n, s_V_b)):
        if isinstance(standard_dev, Number):
            standard_devs[i] = np.full(num_tips, standard_dev)
        else:
            standard_devs[i] = standard_dev

    # Reduce the number of used points to only non railed measurements and working tips.
    non_railed_indeces = non_railed_measurements_indeces(v_s, v_n)
    usable_point_indeces = np.where(working_tips[non_railed_indeces])[0]
    data_points = data_points[:, usable_point_indeces]
    standard_devs = standard_devs[:, usable_point_indeces]
    A_p_usable = A_p[usable_point_indeces]
    tip_numbers_usable = tip_numbers[usable_point_indeces]
    logging.debug(f"Found {usable_point_indeces.size} / {num_tips} usable points.")

    # Create the RealData and Model objects for use during ODR.
    data = odr.RealData(
        data_points, 1, standard_devs,
    )
    model = odr.Model(
        implicit_model_function, 
        extra_args=(A_p_usable, V_p_func, I_p_func, tip_numbers_usable, tip_radius, model_name), 
        implicit=True
    )

    initial_guess = initial_guess.astype(float)
    converted_guess = np.array([
        from_n_conversion(initial_guess[0]), 
        from_T_e_conversion(initial_guess[1]), 
        initial_guess[2]
    ])
    # Do multiple runs for a variety of initial guesses.
    density_range = (initial_guess[0] * 10**-2, initial_guess[0] * 10**2)
    temperature_range = (
        max(initial_guess[1] * 10**-1, T_e_min), 
        min(initial_guess[1] * 10**1, T_e_max)
    )
    plasma_potential_range = (initial_guess[2] - 50, initial_guess[2] + 50)

    outputs = []
    num_tries = 10

    rng = np.random.default_rng(seed=random_seed)
    density_guesses = 10**(np.log10(density_range[1] / density_range[0]) * rng.random(num_tries - 1) + np.log10(density_range[0]))
    temp_guesses = 10**((np.log10(temperature_range[1]) - np.log10(temperature_range[0])) * rng.random(num_tries - 1) + np.log10(temperature_range[0]))
    potential_guesses = (plasma_potential_range[1] - plasma_potential_range[0]) * rng.random(num_tries - 1) + plasma_potential_range[0]
    all_guesses = [converted_guess] + [
        (from_n_conversion(density_guesses[i]), from_T_e_conversion(temp_guesses[i]), potential_guesses[i]) for i in range(num_tries - 1)
    ]
    for guess in all_guesses:
        try:
            logging.debug("Trying to find a curve fit starting at `(n, Te, Vplasma) = {}`.".format(np.array([to_n_conversion(guess[0]), to_T_e_conversion(guess[1]), guess[2]])))
            odr_output_converted = try_odr(data, model, guess)
            odr_output = to_val_conversion(odr_output_converted)
        except Exception as e:
            logging.debug("Using guess `(n, Te, Vplasma) = {}` for fitting the curve fit did not work. Error was:\n{}".format(
                np.array([to_n_conversion(guess[0]), to_T_e_conversion(guess[1]), guess[2]]), e
            ))
        else:
            logging.debug("Successfully found `(n, Te, Vplasma) = {}`.".format(odr_output.beta))
            outputs.append(odr_output)

    logging.info("Found {} good results when curve fitting using {} guesses.".format(len(outputs), num_tries))

    if len(outputs) != 0:
        # Choose the result with the lowest sum_square error.
        best_output = min(outputs, key=lambda out: out.sum_square)
        logging.debug("Result with lowest error was `(n, Te, Vplasma) = {}`.".format(best_output.beta))
    else:
        best_output = None
        logging.debug("No results found.")

    if check_fit:
        v_probe = V_p_func(v_s, v_n, V_b, tip_numbers)
        j_probe = I_p_func(v_s, v_n, tip_numbers) / A_p

        fig = plt.figure()
        gs = fig.add_gridspec(
            2, 1, height_ratios=[5, 1],
        )
        iv_ax = fig.add_subplot(gs[0])
        iv_ax.axhline(0, color='grey')
        v_p_continuous = np.linspace(np.min(v_probe), np.max(v_probe), num=200)

        iv_ax.plot(v_p_continuous, model_J_p(v_p_continuous, *initial_guess, 1, tip_radius), color='green', alpha=0.2, label='Initial Guess')

        for output in outputs:
            if output == best_output:
                continue

            iv_ax.plot(v_p_continuous, model_J_p(v_p_continuous, *output.beta, 1, tip_radius), color='blue', alpha=0.2, linestyle='--')

        if best_output is not None:
            for (data_point, point_on_curve, tip_number) in zip(data_points.T, best_output.xplus.T, tip_numbers_usable):
                iv_ax.plot(
                    [V_p_func(*data_point, tip_number), V_p_func(*point_on_curve, tip_number)],
                    [
                        I_p_func(*data_point[:2], tip_number) / A_p[tip_number - 1], 
                        I_p_func(*point_on_curve[:2], tip_number) / A_p[tip_number - 1]
                    ],
                    color='red'
                )

            iv_ax.plot(v_p_continuous, model_J_p(v_p_continuous, *best_output.beta, 1, tip_radius))
            
            text_str = ''
            names = ['n', 'T_e', 'V_p']
            for i in range(len(names)):
                text_str += r'${{{}}} = {{{:.2g}}} \pm {{{:.1g}}}$'.format(
                    names[i], best_output.beta[i], best_output.sd_beta[i]
                )
                if i < len(names) - 1:
                    text_str += '\n'
                
            iv_ax.text(np.min(v_probe), np.min(j_probe), text_str)

        iv_ax.scatter(v_probe[usable_point_indeces], j_probe[usable_point_indeces])
        iv_ax.scatter(np.delete(v_probe, usable_point_indeces), np.delete(j_probe, usable_point_indeces), color='red', marker='x')

        try:
            for v_p, j_p, tip_number in zip(v_probe, j_probe, tip_numbers):
                iv_ax.annotate(tip_number, (v_p, j_p))
        except TypeError:
            logging.info("Couldn't annotate tips.")

        j_p_diff = np.max(j_probe) - np.min(j_probe)
        # plt.ylim(np.min(j_probe) - 0.1 * j_p_diff, np.max(j_probe) + 0.1 * j_p_diff)

        weights_ax = fig.add_subplot(gs[1], sharex=iv_ax)
        unnormalized_weights = weighting_function(to_T_e_conversion(best_output.beta[1]), best_output.beta[2], v_probe, normalize=False)
        normalized_weights = unnormalized_weights / np.sum(unnormalized_weights)
        continuous_weights = weighting_function(to_T_e_conversion(best_output.beta[1]), best_output.beta[2], v_p_continuous, normalize=False) / np.sum(unnormalized_weights)
        weights_ax.plot(v_p_continuous, continuous_weights)
        weights_ax.scatter(v_probe[usable_point_indeces], normalized_weights[usable_point_indeces])
        weights_ax.set_xlabel('V_probe [V]')
        weights_ax.set_ylabel('Weight')

        # fig.suptitle("IV Curve for shot #{} at time index {}".format(te_data.shot_number, time_index))

        # iv_ax.set_xlabel('V_probe [V]')
        iv_ax.set_ylabel('J_probe [A / m^2]')

        # plt.show()

    if len(outputs) == 0:
        raise ValueError("Unable to get a good curve fit for the passed voltage measurements.")
    else:
        return best_output, usable_point_indeces
    

def get_plasma_parameters(te_data, time_index, initial_guess, **kwargs):
    """
    Get the plasma parameters for a specific time index.

    Parameters
    ----------
    te_data : Te_Probe1_Data or Te_Probe3_Data
    time_index : int
        Index of the time to get the plasma parameters at.
    initial_guess : np.array[float]
        1D array of 3 floats for an estimate of `(n, T_e, V_plasma)`.
    kwargs : dict
        Additional keyword arguments to pass to `solve_odr`.
    
    Returns
    -------
    output : odr.Output
        Output from ODR for the best fit using a variety of initial guesses. The 
        returned result is of the form `(n, T_e, V_plasma)`.
    usable_point_indeces : np.array[int]
        Indeces of which points were used.

    Notes
    -----
    To access the fit parameters of the `output` object, use `output.beta` for
    the parameters and `output.sd_beta` for the standard deviations of the
    parameters. To get the parameter fits one standard deviation away, use
    `odr_output.sd_beta_bounds`. This is more accurate than using the standard
    deviations directly.

    To check the quality of the fit, set `check_fit=True` when calling this
    function. This will plot the IV curve and the fit curve. The points that
    were used in the fit are colored blue and the points that were not used are
    colored red. The weights for each point are also plotted. The fit parameters
    are displayed on the plot.

    Example
    -------
    To get the fit for a single time point, your code might look something like this:
    ```python
    shot = 65451
    te_data = Te_Probe1_Data(shot)
    time_index = 33010
    initial_guess = np.array([10**17, 3, 0])

    get_plasma_parameters(te_data, time_index, initial_guess, check_fit=True)
    ```

    If you want to get the fit for multiple time points, you might do something like this:
    ```python
    shot = 65451
    te_data = Te_Probe1_Data(shot)
    time_indeces = range(33000, 33010)
    initial_guess = np.array([10**17, 3, 0])
    solutions = []

    for time_index in time_indeces:
        try:
            current_solution, used_point_indeces = get_plasma_parameters(
                te_data, time_index, initial_guess, check_fit=True
            )
        except ValueError as e:
            print(e)
        else:
            solutions.append(current_solution)
            # Update the guess using the current fit.
            initial_guess = current_solution.beta
    ```
    Now you can check that the fits are good. The `solutions` list will contain
    the `odr.Output` objects for each time point.
    """
    if not isinstance(te_data, Te_Probe1_Data) and not isinstance(te_data, Te_Probe3_Data):
        raise ValueError("The passed data must be from a Te probe.")
    
    if te_data.to_raw_index(time_index) < 0:
        raise ValueError("The passed time index is less than the start index of the `te_data.time_index_range[0]`. To fix this, either increase the `time_index` or decrease the `te_data.time_index_range[0]` by passing a different value of `time_index_range` when constructing the `Te_Probe` object.")
    elif te_data.to_raw_index(time_index) >= te_data.v_s.shape[1]:
        raise ValueError("The passed time index is greater than the end index of the `te_data.time_index_range[1]`. To fix this, either decrease the `time_index` or increase the `te_data.time_index_range[1]` by passing a different value of `time_index_range` when constructing the `Te_Probe` object.")
    
    return solve_odr(
        te_data.v_s[:, te_data.to_raw_index(time_index)], 
        te_data.v_n[:, te_data.to_raw_index(time_index)], 
        te_data.vbias,
        te_data.relative_area * te_data.normalization_area,
        get_V_probe_function(te_data),
        get_I_probe_function(te_data),
        initial_guess,
        working_tips=te_data.working,
        **kwargs
    )


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('matplotlib').setLevel(logging.WARNING)

    shot = 61095
    shot = 57974
    te_data = Te_Probe3_Data(shot)
    shot = 65451
    # shot = 65120
    te_data = Te_Probe1_Data(shot)
    # te_data.working
    # bad_tips = np.array([4, 14, 15])
    # bad_tips = np.array([3, 5, 7])
    # te_data._working[bad_tips - 1] = False

    time_index = 33000
    initial_guess = np.array([10**17, 3, 0])

    get_plasma_parameters(te_data, time_index, initial_guess, check_fit=True)

    # Plot the IV data from MDSplus.
    # plt.scatter(te_data.vprobe[:, time_index], te_data.iprobe[:, time_index] / te_data.normalization_area, alpha=0.5)

    plt.show()
