"""
This method parametrizes Laframboise's IV theory for cylindrical probes using Chen's parametrization.

Notes
-----
Theory validity:
    - Collisionless sheath (i.e. mean free path >> debye length).
    - Probe radius << probe length.
    - Quasineutral plasma consisting of electrons and singly-ionized ions.
    - Bulk plasma flow only parallel to probe axis (no change to theory).
    - Both species annihilated when touching the probe.
    - No magnetic fields.
    - The probe potential is less than the plasma potential.
    - Cylindrical probe.
    - `T_i << T_e`.

This follows F. F. Chen, “Langmuir probe analysis for high density plasmas,” Physics of Plasmas, vol. 8, no. 6, pp. 3029-3041, Jun. 2001, doi: 10.1063/1.1368874.
"""
import numpy as np
from modules.langmuir.laframboise.normalizations import *
from modules.langmuir.laframboise.planar_probe import collected_current


def normalized_probe_radius(R_p, T_e, n_e):
    """
    Gives the `xi` parameter found in the paper.

    Parameters
    ----------
    R_p : float
        Probe radius in units of [m].
    T_e : float
        Electron temperature in units of [eV].
    n_e : float
        Electron number density in units of [m^-3].
    """
    return R_p / debye_length(n_e, T_e) # Equation (5).

def normalized_probe_potential(V_p, V_s, T_e):
    """
    Gives the `eta` parameter found in the paper.

    Parameters
    ----------
    V_p, V_s : float
        Probe and space (plasma) potential.
    T_e : float
        Electron temperature in units of [eV].

    Notes
    -----
    Equation (6).
    """
    return -(V_p - V_s) / T_e

def large_xi_parameters(xi):
    """
    Get ABCD for the ion collected current if `xi >> 3`.
    
    Parameters
    ----------
    xi : float

    Returns
    -------
    ABCD_arr : np.array[float]

    Notes
    -----
    Equation (9) and Table (2).
    """
    abcdfg_params = np.array([
        [1.142, 19.027, 3.0, 1.433, 4.164, 0.252],
        [0.53, 0.97, 3.0, 1.11, 2.12, 0.35],
        [0.0, 1.0, 3.0, 1.95, 1.27, 0.035],
        [0.0, 2.65, 2.96, 0.376, 1.94, 0.234]
    ]) # Shape `(ABCD, abcdfg)`

    ABD_func = lambda xi, params: params[0] + params[1] * (xi - params[2])**params[3] * np.exp(-params[4] * (xi - params[2]**params[5]))
    C_func = lambda xi, params: params[0] + params[1] * np.exp(-params[2] * np.log(xi - params[3])) + params[4] * (1 - params[5] * np.log(xi))

    ABCD_arr = np.array([ABD_func(xi, abcdfg_params[0]), ABD_func(xi, abcdfg_params[1]), C_func(xi, abcdfg_params[2]), ABD_func(xi, abcdfg_params[3])])
    return ABCD_arr

def small_xi_parameters(xi):
    """
    Get ABCD for the ion collected current if `xi ~ 3`.
    
    Parameters
    ----------
    xi : float

    Returns
    -------
    ABCD_arr : np.array[float]

    Notes
    -----
    Equation (10) and Table (4).
    """
    abcdf_params = np.array([
        [1.12, .00034, 6.87, 0.145, 110.0],
        [0.5, 0.008, 1.5, 0.18, 0.8],
        [1.07, 0.95, 1.01, 0, 0],
        [0.05, 1.54, 0.3, 1.135, 0.37]
    ]) # Shape `(ABCD, abcdf)`

    A_func = lambda xi, params: params[0] + 1 / (1 / (params[1] * xi**params[2]) - 1 / (params[3] * np.log(xi / params[4])))
    BD_func = lambda xi, params: params[0] + params[1] * xi**params[2] * np.exp(-params[3] * xi**params[4])
    C_func = lambda xi, params: params[0] + params[1] * xi**-params[2]

    ABCD_arr = np.array([A_func(xi, abcdf_params[0]), BD_func(xi, abcdf_params[1]), C_func(xi, abcdf_params[2]), BD_func(xi, abcdf_params[3])])
    return ABCD_arr

def normalized_ion_current(xi, eta, xi_limit=3.5):
    """
    The ion current to the probe normalized by the ion current when the probe is at the plasma potential and the ions have `T_i = T_e`.

    Parameters
    ----------
    xi : float
    eta : np.array[float]
    xi_limit : float, default=3.5
        Limit at which the calculation for the parameters changes from the large to small xi parameters.
    
    Notes
    -----
    Equation (8).
    """
    ABCD = large_xi_parameters(xi) if xi >= xi_limit else small_xi_parameters(xi)
    return np.where(eta <= 0, 0, (1 / (ABCD[0] * eta**ABCD[1])**4 + 1 / (ABCD[2] * eta**ABCD[3])**4)**-0.25)

def ion_current(R_p, L, V_p, V_s, N_inf, T_e, m_i=m_p, Z=1):
    """
    The ion current to the probe.

    In the electron saturation region there is no ion current.
    
    Parameters
    ----------
    R_p : float
        Probe radius. [m]
    L : np.array[float] or float
        Probe tip lengths. [m]
    V_p : np.array[float]
        The probe potential. [V]
    V_s : float
        The plasma potential. [V]
    N_inf : float
        Particle number density at infinite radius. [m^-3]
    T_e : float
        Electron temperature. [eV]
    m_i : float, default=m_p
        Ion particle mass. [kg]
    Z : float
        Number of electronic charges on the ion.

    Notes
    -----
    This normalization is slightly different than equation (6). Instead we use 
    the normalization of Laframboise.
    """
    ion_attracted_indeces = np.nonzero(V_p <= V_s)

    coeff = collected_current_at_plasma_potential(R_p, N_inf, T_e, m_i, Z)
    eta = normalized_probe_potential(V_p[ion_attracted_indeces], V_s, T_e)
    xi = normalized_probe_radius(R_p, T_e, N_inf)
    current_per_length = np.zeros_like(V_p)
    current_per_length[ion_attracted_indeces] = coeff * normalized_ion_current(xi, eta)

    # We change the length to account for the end cap.
    L_corrected = L + R_p / 2
    current = L_corrected * current_per_length
    return current

def electron_current(R_p, L, V_p, V_s, N_inf, T_e, approximation=True):
    """
    The electron current to the probe.

    In the electron attracted regime we use the planar probe approximation.
    
    Parameters
    ----------
    R_p : float
        Probe radius. [m]
    L : np.array[float] or float
        Probe tip lengths. If an array, should be 1D and of shape `(num_points,)`. [m]
    V_p : np.array[float]
        1D array of shape `(num_points,)` containing the probe potential. [V]
    V_s : float
        The plasma potential. [V]
    N_inf : float
        Particle number density at infinite radius. [m^-3]
    T_e : float
        Electron temperature. [eV]
    approximation : bool, default=True
        Whether to approximate the electron attracted current using a faster method.

    Notes
    -----
    Equation (15).
    """
    ion_attracted_indeces = np.nonzero(V_p <= V_s)
    current = np.zeros_like(V_p)
    
    coeff = collected_current_at_plasma_potential(R_p, N_inf, T_e, m_e, 1)
    eta = normalized_probe_potential(V_p[ion_attracted_indeces], V_s, T_e)
    current_per_length = coeff * np.exp(-eta)

    # We change the length to account for the end cap.
    L_corrected = L + R_p / 2
    if isinstance(L, np.ndarray):
        current[ion_attracted_indeces] = L_corrected[ion_attracted_indeces] * current_per_length
        electron_attracted_indeces = np.nonzero(V_p > V_s)
        current[electron_attracted_indeces] = collected_current(
            R_p, L[electron_attracted_indeces], V_p[electron_attracted_indeces], V_s, N_inf, T_e, approximation=approximation
        )
    else:
        current[ion_attracted_indeces] = L_corrected * current_per_length
        electron_attracted_indeces = np.nonzero(V_p > V_s)
        current[electron_attracted_indeces] = collected_current(
            R_p, L, V_p[electron_attracted_indeces], V_s, N_inf, T_e, approximation=approximation
        )

    return current

def total_current(R_p, L, V_p, V_s, N_inf, T_e, m_i=m_p, Z=1, approximation=True):
    """
    The sum of the electron and ion current to the probe.
    
    Parameters
    ----------
    R_p : float
        Probe radius. [m]
    L : np.array[float] or float
        Probe tip lengths. [m]
    V_p : np.array[float]
        The probe potential. [V]
    V_s : float
        The plasma potential. [V]
    N_inf : float
        Particle number density at infinite radius. [m^-3]
    T_e : float
        Electron temperature. [eV]
    m_i : float, default=m_p
        Ion particle mass. [kg]
    Z : float
        Number of electronic charges on the particle.
    approximation : bool, default=True
        Whether to approximate the electron attracted current using a faster method.
    """
    return ion_current(R_p, L, V_p, V_s, N_inf, T_e, m_i, Z) - electron_current(R_p, L, V_p, V_s, N_inf, T_e, approximation)


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from modules.langmuir.curve_fit import model_J_p
    import cProfile

    R_p = 4.953 * 10**-4 # Te probe.
    R_p = 0.0002921 # PA core.
    # R_p = 0.0008255 # PA shell.
    A = 10**-8
    L = (A - np.pi * R_p**2) / (2 * np.pi * R_p)
    print('L = ', L)
    # L = 2 * 10**-3
    V_s = 0
    V_p_vals = V_s + np.linspace(-100, 100, num=1000)
    N_inf = 1 * 10**18
    plt.axhline(0, color='grey', alpha=0.2)
    plt.axvline(V_s, color='grey', alpha=0.2)
    for T in [10]:
        I = total_current(R_p, L, V_p_vals, V_s, N_inf, T)
        I_approx = total_current(R_p, L, V_p_vals, V_s, N_inf, T, approximation=True)

        line_color = plt.plot(V_p_vals, I_approx, label='{}, {}, Approximation'.format(int(R_p / debye_length(N_inf, T)), T))[0].get_color()
        # plt.plot(V_p_vals, I, label='{}, {}'.format(int(R_p / debye_length(N_inf, T)), T), color=line_color)
        plt.plot(V_p_vals, ion_current(R_p, L, V_p_vals, V_s, N_inf, T), color=line_color, linestyle='dashed', label=r'$I_p$')
        plt.plot(V_p_vals, -electron_current(R_p, L, V_p_vals, V_s, N_inf, T), color=line_color, linestyle='dotted', label=r'$I_e$')

        # line_color = plt.plot(V_p_vals, full_IV(R_p, L, V_p_vals, V_s, N_inf, T), label="Chen: {}, {}".format(int(R_p / debye_length(N_inf, T)), T))[0].get_color()
        # plt.plot(V_p_vals[np.nonzero(V_p_vals <= V_s)], model_I_p(V_p_vals[np.nonzero(V_p_vals <= V_s)], N_inf, T, V_s, np.pi * R_p**2 + 2 * np.pi * R_p * L, model='hutchinson'), color=line_color, linestyle='dotted', label='Hutchinson')
    plt.legend(title=r'$R_p / \lambda_D$, $T_e$')
    plt.xlabel(r"$V$")
    plt.ylabel(r"$I$")
    plt.title(r"$N_\inf = {} \times 10^{{{}}}$".format(str(N_inf)[0], int(np.log10(N_inf))))
    plt.show()

