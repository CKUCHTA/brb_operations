"""
Contain various geometric objects.
"""
import numpy as np
import sympy as sp


class IncreasingPiecewiseLinear:
    """
    Holds info for a monotonically increasing continuous piecewise linear function defined over some finite domain.
    
    Parameters
    ----------
    domain_edge_points : np.array
        Independent variable values at which the linear domains change. The first and last elements define the start and end of the domain.
    edge_values : np.array
        Left and right dependent variable values at which the linear domains change.
    """
    def __init__(self, edge_points, edge_values) -> None:
        if len(edge_values) != len(edge_points):
            raise ValueError("Number of edge values ({}) must have the same number of elements as edge points ({}).".format(len(edge_values), len(edge_points)))

        self.edge_points = edge_points
        self.edge_values = edge_values
        self.num_domains = len(edge_values)

    def moving_intersect(self, x_diff, new_y_bounds):
        """
        Construct a new piecewise linear function that is the intersect between this and a straight line.
        
        Parameters
        ----------
        x_diff : float
            Difference between :math:`x_i` and :math:`x_{i+1}`.
        new_y_bounds : tuple of two float
            `(lower, upper)` bounds for :math:`y_{i+1}`.

        Returns
        -------
        IncreasingPiecewiseLinear

        Notes
        -----
        This algorithm finds a function of :math:`y_{i+1}` for the leftmost intersect of this function and the function

        .. math:: \frac{-1}{x_{i + 1} - x_i} y_i + \frac{y_{i + 1}}{x_{i + 1} - x_i}
        """
        moving_line = lambda y, new_y: (new_y - y) / x_diff

        new_edge_points = []
        new_edge_values = []
        # Add the starting point/values.
        starting_line_edge_vals = moving_line(self.edge_points, new_y_bounds[0]) # Value of the line for the minimum new_y at all edges of the function.
        if starting_line_edge_vals[0] < self.edge_values[0]:
            # This means that the leftmost intersection is along the vertical boundary of this function at the leftmost domain edge.
            new_edge_points.append(new_y_bounds[0])
            new_edge_values.append(starting_line_edge_vals[0])

            edge_index = -1
        else:
            # Find in which domain the line first intersects. The domain is [self.edge_points[edge_index], self.edge_points[edge_index + 1]].
            edge_index = np.argmax(starting_line_edge_vals < self.edge_values) - 1

            # If the index where the max is == 0 then that means the starting line was always greater than the function.
            if edge_index == -1:
                return IncreasingPiecewiseLinear()
            else:
                # Find the value along the domain where the line intersects.
                intersection_point = Line.point_intersection(
                    (np.array((self.edge_points[0], starting_line_edge_vals[0])), np.array((self.edge_points[1], starting_line_edge_vals[1]))),
                    (np.array((self.edge_points[edge_index], self.edge_values[edge_index])), np.array((self.edge_points[edge_index + 1], self.edge_values[edge_index + 1])))
                )
                new_edge_points.append(new_y_bounds[0])
                new_edge_values.append(intersection_point[1])

        # Subtract the edge values and the starting_line. This gives the value of `(y_new - y_new_min) / x_diff` at each edge.
        difference = self.edge_values - starting_line_edge_vals
        y_new_points = difference * x_diff + new_y_bounds[0]

        # Now we know what values of y_new are at edges of the new IncreasingPiecewiseLinear. Now we only want to select the y_new values that fall within the bounds.
        valid_indeces = np.where(np.logical_and(new_y_bounds[0] < y_new_points, y_new_points <= new_y_bounds[1]))
        valid_y_new_points = y_new_points[valid_indeces]
        valid_y_new_edge_vals = self.edge_values[valid_indeces]

        new_edge_points += list(valid_y_new_points)
        new_edge_values += list(valid_y_new_edge_vals)

        # Now deal with the end point. If the largest y_new (for the final edge) is less than the y_new bounds then we are done (i.e. we can't get to that y_new value).
        if y_new_points[-1] > new_y_bounds[1] and (valid_y_new_points.size == 0 or valid_y_new_points[-1] < new_y_bounds[1]):
            new_edge_points.append(new_y_bounds[1])
            # If the moving line traverses an edge or the domain it intersects is not the leftmost vertical then find the intersect and add the point.
            if valid_y_new_points.size != 0 or edge_index != -1:
                if valid_y_new_points.size == 0:
                    last_edge_index = edge_index
                else:
                    last_edge_index = valid_indeces[0][-1]

                final_line_edge_vals = moving_line(self.edge_points[last_edge_index:last_edge_index + 2], new_y_bounds[1])
                # If the largest y_new intersection falls in the middle of a domain, find that intersection point.
                intersection_point = Line.point_intersection(
                    (np.array((self.edge_points[last_edge_index], self.edge_values[last_edge_index])), np.array((self.edge_points[last_edge_index + 1], self.edge_values[last_edge_index + 1]))),
                    (np.array((self.edge_points[last_edge_index], final_line_edge_vals[0])), np.array((self.edge_points[last_edge_index + 1], final_line_edge_vals[1])))
                )

                new_edge_values.append(intersection_point[1])
            else:
                # If we are still on the left vertical, just add the value of the final line at the left point.
                new_edge_values.append(moving_line(self.edge_points[0], new_y_bounds[1]))

        return IncreasingPiecewiseLinear(new_edge_points, new_edge_values)


class Ellipse:
    """
    Create an Ellipse object.
    
    Parameters
    ----------
    alpha : float
        Angle of axis with magnitude `a` in degrees.
    a : float
    b : float
    x0 : float
    y0 : float
    """
    def __init__(self, alpha, a, b, x0, y0):
        self.alpha_deg = alpha
        self.alpha_rad = np.deg2rad(alpha)

        self.a = a
        self.b = b
        self.a_vector = self.a * np.array((np.cos(self.alpha_rad), np.sin(self.alpha_rad)))
        self.b_vector = self.b * np.array((-np.sin(self.alpha_rad), np.cos(self.alpha_rad)))

        self.x0 = x0
        self.y0 = y0
        self.center = np.array((x0, y0))

    def __str__(self) -> str:
        return "Ellipse(alpha={}, a={}, b={}, center=({}, {}))".format(self.alpha_deg, self.a, self.b, self.x0, self.y0)

    def __repr__(self) -> str:
        return str(self)

    def get_tangent_points(self, tangent_vector, point_side=None):
        """
        Get the points at which the ellipse is tangent to some vector.
        
        Parameters
        ----------
        tangent_vector : np.array of 2 floats
        point_side : [None, 'cw', 'ccw'], default=None
            Which point to get that is either clockwise or counterclockwise of the tangent_vector. None means get both points.

        Returns
        -------
        tuple of np.array
            Points on ellipse that are tangent to `tangent_vector` in the order ('cw', 'ccw')
        """
        if point_side not in [None, 'cw', 'ccw']:
            raise ValueError("Passed `point_side` is invalid (passed value of '{}'). Must be one of [None, 'cw', 'ccw'].".format(point_side))

        # First, transform the ellipse and tangent vector to a coordinate system where the ellipse is a circle.
        rotated_vector = self._rotation_matrix(-self.alpha_rad) @ tangent_vector
        scaled_vector = np.array((1 / self.a, 1 / self.b)) * rotated_vector

        normalized_vector = scaled_vector / np.linalg.norm(scaled_vector)
        # The perpendicular vectors are equal and opposite so just store one. When we translate them, we separate them if needed.
        perp_vector = np.array((normalized_vector[1], -normalized_vector[0]))
        if point_side == 'ccw':
            perp_vector *= -1

        # Invert the transformations.
        unrotated_perp = np.array((self.a, self.b)) * perp_vector
        untranslated_perp = self._rotation_matrix(self.alpha_rad) @ unrotated_perp

        translation_vector = np.array((self.x0, self.y0))
        if point_side is None:
            return (untranslated_perp + translation_vector, -untranslated_perp + translation_vector)
        else:
            return untranslated_perp + translation_vector

    @staticmethod
    def _rotation_matrix(angle):
        """
        Get the 2D rotation matrix for some angle.

        Parameters
        ----------
        angle : float
            Angle in radians

        Returns
        -------
        np.array
        """
        return np.array((
            (np.cos(angle), -np.sin(angle)),
            (np.sin(angle), np.cos(angle))
        ))

    def get_equation_matrix(self, symbolic):
        if not symbolic:
            if hasattr(self, 'equation_matrix_numeric'):
                return self.equation_matrix_numeric

            translation_matrix = np.eye(3)
            translation_matrix[:2, -1] = (-self.x0, -self.y0)

            elliptical_matrix = np.diag([1 / self.a**2, 1 / self.b**2, -1])

            rotation_matrix = np.array((
                (np.cos(self.alpha_rad), -np.sin(self.alpha_rad), 0),
                (np.sin(self.alpha_rad), np.cos(self.alpha_rad), 0),
                (0, 0, 1)
            ))

            return translation_matrix.T @ rotation_matrix.T @ elliptical_matrix @ rotation_matrix @ translation_matrix
        else:
            if hasattr(self, 'equation_matrix_symbolic'):
                return self.equation_matrix_symbolic

            x0 = sp.S(self.x0)
            y0 = sp.S(self.y0)
            a = sp.S(self.a)
            b = sp.S(self.b)
            alpha = sp.S(self.alpha_deg)

            translation_matrix = sp.eye(3)
            translation_matrix[:2, -1] = (-x0, -y0)

            elliptical_matrix = sp.diag(1 / a**2, 1 / b**2, -1)

            alpha_rad = alpha * sp.pi / 180
            rotation_matrix = sp.Matrix((
                (sp.cos(alpha_rad), -sp.sin(self.alpha_rad), 0),
                (sp.sin(alpha_rad), sp.cos(self.alpha_rad), 0),
                (0, 0, 1)
            ))

            return translation_matrix.T * rotation_matrix.T * elliptical_matrix * rotation_matrix * translation_matrix

    def parametrize(self, include_axis=False):
        """
        Paramaterize the ellipse so that t in [0, 1] goes all the way around.
        
        Paramaters
        ----------
        include_axis : bool, default=False
            Whether to also draw the `a` axis.

        Returns
        -------
        func(t : float)

        Notes
        -----
        We use the ellipse equation form given by https://www.maa.org/external_archive/joma/Volume8/Kalman/General.html
        """
        def ellipse_curve(t):
            unrotated_x = self.a * np.cos(2 * np.pi * t)
            unrotated_y = self.b * np.sin(2 * np.pi * t)
            return (np.cos(self.alpha_rad) * unrotated_x - np.sin(self.alpha_rad) * unrotated_y + self.x0, 
                    np.sin(self.alpha_rad) * unrotated_x + np.cos(self.alpha_rad) * unrotated_y + self.y0)

        def axis_line(t):
            center = np.array((self.x0, self.y0))
            ellipse_start = np.array(ellipse_curve(0))
            line = Line(intersection_points=(center, ellipse_start))
            return line.parameterize([center[0], ellipse_start[0]], [center[1], ellipse_start[1]])(t)

        if not include_axis:
            return ellipse_curve
        else:
            return lambda t: np.where(t <= 0.5, axis_line(2 * t), ellipse_curve(2 * t - 1))


class Line:
    """
    Construct a line object.
    
    This only works if `line_coeffs` is passed alone OR `intersection_point` and `tangent_vector` are both passed OR `intersection_points` is passed.
    
    Parameters
    ----------
    line_coeffs : iterable of three floats
        Coefficients to plug into a line equation of the form `c1 * x + c2 * y + c3 == 0`
    intersection_point : np.array of two floats
        Point that line intersects.
    tangent_vector : np.array of two floats
        Vector that is tangent to line.
    intersection_points : tuple of two np.arrays of two floats
        Points that line intersects.
    """
    def __init__(self, line_coeffs=None, intersection_point=None, tangent_vector=None, intersection_points=None):
        # Make sure that we are only passing the correct number of parameters.
        are_not_none = [line_coeffs is not None, intersection_point is not None or tangent_vector is not None, intersection_points is not None]
        if are_not_none.count(True) != 1:
            raise ValueError("Too many paramaters were passed to define the line.")

        if line_coeffs is not None:
            self.c1 = line_coeffs[0]
            self.c2 = line_coeffs[1]
            self.c3 = line_coeffs[2]
        elif intersection_point is not None and tangent_vector is not None:
            self.c1, self.c2, self.c3 = self._point_and_vec_to_coeffs(intersection_point, tangent_vector)
        elif intersection_points is not None:
            self.c1, self.c2, self.c3 = self._points_to_coeffs(intersection_points)
        else:
            raise ValueError("Not enough paramaters were passed to define the line.")

        # Normalize the coefficients.
        norm = (self.c1**2 + self.c2**2 + self.c3**2)**0.5
        self.c1 /= norm
        self.c2 /= norm
        self.c3 /= norm

    @staticmethod
    def _point_and_vec_to_coeffs(point, vec):
        return (vec[1], -vec[0], vec[0] * point[1] - vec[1] * point[0])

    @staticmethod
    def _points_to_coeffs(points):
        return Line._point_and_vec_to_coeffs(points[0], (points[1][0] - points[0][0], points[1][1] - points[0][1]))

    @property
    def coefficients(self):
        return (self.c1, self.c2, self.c3)

    def parameterize(self, x_bounds, y_bounds):
        """
        Return a function that goes from bound to bound by taking an input in [0, 1].
        
        Parameters
        ----------
        x_bounds : tuple of two floats
            Holds (start, end) bounds on x.
        y_bounds : tuple of two floats
            Holds (start, end) bounds on y.

        Returns
        -------
        parametrized_line : func(t : float)
        """
        if self.c2 == 0:
            def vertical_line(t):
                # Add 0 * t so that if t is array like we get an array like return value.
                x_position = -self.c3 / self.c1 + 0 * t
                y_position = y_bounds[0] + (y_bounds[1] - y_bounds[0]) * t
                return x_position, y_position
            return vertical_line
        else:
            def non_vertical_line(t):
                x_position = x_bounds[0] + (x_bounds[1] - x_bounds[0]) * t
                y_position = -(self.c1 * x_position + self.c3) / self.c2
                return x_position, y_position
            return non_vertical_line

    @staticmethod
    def coef_intersection(line1_coeffs, line2_coeffs):
        """
        Get the intersection point for two lines defined by (c1, c2, c3) as in the `Line` class.

        Parameters
        ----------
        line1_coeffs, line2_coeffs : tuple of 3 float
            Normalized line coefficient vector.

        Returns
        -------
        2 element tuple
            Returns the (x, y) coordinates of where they intersect.
        """
        if np.allclose(line1_coeffs, line2_coeffs):
            raise ValueError("While trying to find line intersection, both lines described the same line and thus intersect at an infinite number of points.")
        # Check if the lines are parallel. We know that the lines are not the same as not all coeffs are the same.
        elif np.isclose(line1_coeffs[0] * line2_coeffs[1], line2_coeffs[0] * line1_coeffs[1]): # Cross product
            raise ValueError("While trying to find line intersection, lines were parallel but did not intersect.")

        y_intersection = (line1_coeffs[2] * line2_coeffs[0] - line1_coeffs[0] * line2_coeffs[2]) / (line1_coeffs[0] * line2_coeffs[1] - line1_coeffs[1] * line2_coeffs[0])
        if line1_coeffs[0] != 0:
            x_intersection = -1 * (line1_coeffs[1] * y_intersection + line1_coeffs[2]) / line1_coeffs[0]
        else:
            x_intersection = -1 * (line2_coeffs[1] * y_intersection + line2_coeffs[2]) / line2_coeffs[0]

        return (x_intersection, y_intersection)

    @staticmethod
    def point_intersection(line1_points, line2_points):
        """
        Get the intersection point for two lines defined by their respective intersection points.
        
        Parameters
        ----------
        line1_points, line2_points : tuple of two np.arrays of two floats
            Points that lines intersects.

        Returns
        -------
        2 element tuple
            Returns the (x, y) coordinates of where they intersect.
        """
        return Line.coef_intersection(Line._points_to_coeffs(line1_points), Line._points_to_coeffs(line2_points))
