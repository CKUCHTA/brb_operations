import numpy as np
from scipy.special import lambertw, erf, erfinv
from scipy.integrate import solve_ivp
from scipy.optimize import minimize, root_scalar, NonlinearConstraint, root
from scipy.io import loadmat, savemat
import os
import matplotlib.pyplot as plt
import tqdm
import warnings
import logging
import itertools
import time
from scipy.odr import ODR, Model, Data
import cProfile
# The following equations use those from Guittienne et. al. PoP 2018


Q = 1.60217733 * 10**-19
EPS_0 = 8.85418781 * 10**-12

def get_equation_limited_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0):
    """
    Get the upper and lower bounds on `V_s` from the paper.

    Returns
    -------
    lower, upper : float
        Lower and upper bounds on `V_s` for the specified `R_s`.
    """
    # Equation B8 & B9.
    # Temperature in units of Volts.
    th = T_e * T_i / (T_e + T_i)
    if V_a < V_0:
        return V_0 - T_i / 2, np.real(V_0 - th / 2 - T_i / 2 * lambertw(-th * R_p**2 / (T_i * R_s**2) * np.exp(2 * (V_a - V_0) / T_e - th / T_i)))
    elif V_a > V_0:
        return np.real(V_0 + th / 2 + T_e / 2 * lambertw(-th * R_p**2 / (T_e * R_s**2) * np.exp(-2 * (V_a - V_0) / T_i - th / T_e))), V_0 + T_e / 2
    else:
        return V_0 - T_i / 2, V_0 + T_e / 2

def lambw(val, k=0):
    """
    Special lambertw function that makes any value less than -1/e be set to -1/e and evaluated. Only works for `k=0` or `k=-1`.
    """
    return np.where(val == -1 / np.e, -1, np.real(lambertw(val, k)))

def valid_bounds(x, y):
    """
    Test if both values have opposite signs so that root finding can occur.
    """
    if x * y <= 0:
        return True
    else:
        return False
    
def get_densities(r, V, R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0):
    """
    Get the ion and electron density at a point.

    Returns
    -------
    n_i, n_e : float
    """
    if V_a <= V_0:
        if V >= V_s:
            n_i = n_0 * np.exp(lambw(-R_s**2 / r**2 * np.exp(-2 * (V_s - V) / T_i - 1)) / 2 - (V - V_0) / T_i)
            n_e = n_0 * np.exp(
                lambw(-R_p**2 / r**2 * np.exp(2 * (V_a - V) / T_e - 1)) / 2 + (V - V_0) / T_e
            )
        else:
            # We alter the form of k = -1 branch to remove large errors that can crop up as W_-1 -> -np.inf
            # n_i = n_0 * np.exp(lambw(-R_s**2 / r**2 * np.exp(-2 * (V_s - V) / T_i - 1), k=-1) / 2 - (V - V_0) / T_i)
            n_i = n_0 * R_s / r * np.exp((V_0 - V_s) / T_i - 0.5) / (-lambw(-R_s**2 / r**2 * np.exp(-2 * (V_s - V) / T_i - 1), k=-1))**0.5
            n_e = n_0 * np.exp(
                lambw(-R_p**2 / r**2 * np.exp(2 * (V_a - V) / T_e - 1)) / 2 + (V - V_0) / T_e
            )
    else:
        if V <= V_s:
            n_e = n_0 * np.exp(lambw(-R_s**2 / r**2 * np.exp(2 * (V_s - V) / T_e - 1)) / 2 + (V - V_0) / T_e)
            n_i = n_0 * np.exp(
                lambw(-R_p**2 / r**2 * np.exp(-2 * (V_a - V) / T_i - 1)) / 2 - (V - V_0) / T_i
            )
        else:
            # n_e = n_0 * np.exp(lambw(-R_s**2 / r**2 * np.exp(2 * (V_s - V) / T_e - 1), k=-1) / 2 + (V - V_0) / T_e)
            n_e = n_0 * R_s / r * np.exp((V_s - V_0) / T_e - 0.5) / (-lambw(-R_s**2 / r**2 * np.exp(2 * (V_s - V) / T_e - 1), k=-1))**0.5
            n_i = n_0 * np.exp(
                lambw(-R_p**2 / r**2 * np.exp(-2 * (V_a - V) / T_i - 1)) / 2 - (V - V_0) / T_i
            )

    return n_i, n_e

def ode_function(r, V_vec, R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0):
    """
    Evaluate Poisson's equation.
    """
    # print('Evaluating at r = {}'.format(r))
    # Equations (30), (37), and (39).
    n_i, n_e = get_densities(r, V_vec[0], R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0)
    result = np.array((V_vec[1], -V_vec[1] / r - Q / EPS_0 * (n_i - n_e)))

    return result

def ode_function_jac(r, V_vec, R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0):
    jac_array = np.zeros((2, 2))
    
    jac_array[0, 1] = 1
    jac_array[1, 1] = -1 / r
    
    V = V_vec[0]
    if V == V_s and r == R_s:
        # Since the jacobian is undefined for this pair of values and the solver can't handle infinity then do this.
        jac_array[1, 0] = 0
    elif V_a <= V_0:
        jac_array[1, 0] = n_0 * Q / EPS_0 * (
            # Electrons
            np.exp((V - V_0) / T_e + 0.5 * lambw(-R_p**2 / r**2 * np.exp(2 * (V_a - V) / T_e - 1))) /
            (T_e * (1 + lambw(-R_p**2 / r**2 * np.exp(2 * (V_a - V) / T_e - 1)))) +
            # Ions
            np.where(V <= V_s,
                np.exp((V_0 - V) / T_i + 0.5 * lambw(-R_s**2 / r**2 * np.exp(2 * (V - V_s) / T_i - 1), k=-1)) /
                (T_i * (1 + lambw(-R_s**2 / r**2 * np.exp(2 * (V - V_s) / T_i - 1), k=-1))),
                np.exp((V_0 - V) / T_i + 0.5 * lambw(-R_s**2 / r**2 * np.exp(2 * (V - V_s) / T_i - 1))) /
                (T_i * (1 + lambw(-R_s**2 / r**2 * np.exp(2 * (V - V_s) / T_i - 1))))
            )
        )
    else:
        jac_array[1, 0] = n_0 * Q / EPS_0 * (
            # Ions
            np.exp((V_0 - V) / T_i + 0.5 * lambw(-R_p**2 / r**2 * np.exp(2 * (V - V_a) / T_i - 1))) /
            (T_i * (1 + lambw(-R_p**2 / r**2 * np.exp(2 * (V - V_a) / T_i - 1)))) +
            # Electrons
            np.where(V >= V_s,
                np.exp((V - V_0) / T_e + 0.5 * lambw(-R_s**2 / r**2 * np.exp(2 * (V_s - V) / T_e - 1), k=-1)) /
                (T_e * (1 + lambw(-R_s**2 / r**2 * np.exp(2 * (V_s - V) / T_e - 1), k=-1))),
                np.exp((V - V_0) / T_e + 0.5 * lambw(-R_s**2 / r**2 * np.exp(2 * (V_s - V) / T_e - 1))) /
                (T_e * (1 + lambw(-R_s**2 / r**2 * np.exp(2 * (V_s - V) / T_e - 1)))),
            )
        )
    return jac_array

def initial_V_vec(R_s, T_e, T_i, V_a, V_0, V_s):
    """
    Get `V` and the derivative of `V` at the sonic radius.
    """
    if V_a <= V_0:
        return np.array([V_s, T_i / R_s])
    else:
        return np.array([V_s, -T_e / R_s])

# TODO: Implement the jacobian for the ode function.
def find_V_a(V_s, R_s, R_p, T_e, T_i, V_a, V_0, n_0):
    """
    Solve the IVP going inwards to the probe surface to find V_a.

    Returns
    -------
    float
        The final `V` value found.
    """
    r_span = (R_s, R_p)
    V_vec_0 = initial_V_vec(R_s, T_e, T_i, V_a, V_0, V_s)

    eps = 10**-6
    v_s_bound = (V_s - np.sign(V_a - V_0) * eps)
    v_a_bound = (V_a - 0.1 * (V_s - V_a))
    # v_a_bound = (V_a + np.sign(V_a - V_0) * eps)
    # We don't bound on V_a because having more events significantly slows evaluation.
    events = [
        lambda _, V_vec, *args: V_vec[0] - v_s_bound,
        lambda _, V_vec, *args: V_vec[0] - v_a_bound,
    ]
    for e in events:
        e.terminal = True

    # result = solve_ivp(ode_function, r_span, V_vec_0, args=(R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0), events=events)
    result = solve_ivp(ode_function, r_span, V_vec_0, args=(R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0), method='RK45')
    if not result.success:
        logging.info("Could not solve the IVP to get V_a, {}, starting at R_s = {} and V_s = {} because of the following:\n\t{}".format(V_a, R_s, V_s, result.message))

    return result.y[0, -1]

    if result.t_events[0].size != 0:
        return result.y_events[0][0, 0]
    elif result.t_events[1].size != 0:
        return result.y_events[1][0, 0]
    else:
        return result.y[0, -1]

def divergence_distance(V_s, R_s, R_p, T_e, T_i, V_a, V_0, n_0, plot_result=False):
    """
    Solve the IVP going outward and return the distance that the solution got before reaching a bound.

    Returns
    -------
    divergence_distance : float
        Positive if solution diverged to V_s. Negative if solution diverged to V_0.
    """
    # Create a list of events that will terminate the integration. We do this in case the voltage goes outside of the (V_a, V_0) bounds.
    events = [
        lambda _, V_vec, *args: V_vec[0] - (V_s - np.sign(V_0 - V_s) * 10**-8), # V_s bound.
        lambda _, V_vec, *args: V_vec[0] - V_0, # V_0 bound.
    ]
    n_i_initial, n_e_initial = get_densities(R_s, V_s, R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0)

    for e in events:
        e.terminal = True
    V_vec_0 = initial_V_vec(R_s, T_e, T_i, V_a, V_0, V_s)

    r_span = (R_s, R_s + 5 * R_p)
    # result = solve_ivp(ode_function, r_span, V_vec_0, events=events, args=(R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0), jac=ode_function_jac, method='RK23')
    result = solve_ivp(ode_function, r_span, V_vec_0, events=events, args=(R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0), method='RK45')

    if plot_result:
        # Plot the densities and voltages round while solving the IVP.
        found_densities = np.zeros((2, result.t.size))
        for i, (r, V) in enumerate(zip(result.t, result.y[0])):
            found_densities[:, i] = get_densities(r, V, R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0)

        fig, (density_ax, v_ax) = plt.subplots(2, sharex=True, figsize=(6, 8))

        density_ax.plot(result.t, found_densities[0], label=r'$n_i$', color='red')
        density_ax.scatter(result.t, found_densities[0], color='red')
        density_ax.plot(result.t, found_densities[1], label=r'$n_e$', color='blue')
        density_ax.scatter(result.t, found_densities[1], color='blue')

        v_ax.plot(result.t, result.y[0], color='green')
        v_ax.scatter(result.t, result.y[0], color='green')

        density_ax.axhline(n_0, color='black', linestyle='dashed', label=r'$n_0$')
        density_ax.axvline(R_s, color='grey', alpha=0.2)
        if V_a < V_0:
            density_ax.axhline(n_i_initial, color='red', linestyle='dashed', label=r'$n_{i0}$')
        else:
            density_ax.axhline(n_e_initial, color='blue', linestyle='dashed', label=r'$n_{e0}$')
        
        v_ax.axhline(V_s, color='orange', linestyle='dashed', label=r'$V_s$')
        v_ax.axhline(V_0, color='yellow', linestyle='dashed', label=r'$V_0$')
        v_ax.axvline(R_s, color='grey', alpha=0.2)

        # Choose whether to be dealing with n_i or n_e.    
        index_of_interest = 0 if V_a < V_0 else 1

        if result.t_events[0].size != 0:
            density_ax.axvline(result.t_events[0][0], color='black', linestyle='dotted')
            event_density = get_densities(result.t_events[0][0], result.y_events[0][0, 0], R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0)[index_of_interest]
            density_ax.scatter(result.t_events[0][0], event_density, color='red', marker='x')
            v_ax.scatter(result.t_events[0][0], result.y_events[0][0, 0], color='red', marker='x')
            fig.suptitle(r"$V_a = {:.3f}$: $R_s = {:.6f}$, $V_s = {:.6f}$    Hit $V_s$".format(V_a, R_s, V_s))

        elif result.t_events[1].size != 0:
            density_ax.axvline(result.t_events[1][0], color='black', linestyle='dotted')
            event_density = get_densities(result.t_events[1][0], result.y_events[1][0, 0], R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0)[index_of_interest]
            density_ax.scatter(result.t_events[1][0], event_density, color='red', marker='x')
            v_ax.scatter(result.t_events[1][0], result.y_events[1][0, 0], color='red', marker='x')
            fig.suptitle(r"$V_a = {:.3f}$: $R_s = {:.6f}$, $V_s = {:.6f}$    Hit $V_0$".format(V_a, R_s, V_s))

        max_y_diff = np.max(found_densities[index_of_interest]) - np.min(found_densities[index_of_interest])
        density_ax.set_ylim(np.min(found_densities[index_of_interest]) - 0.1 * max_y_diff, np.max(found_densities[index_of_interest]) + 0.1 * max_y_diff)
        density_ax.set_xlabel(r"$r$")
        density_ax.set_ylabel(r"$n$")
        density_ax.legend()
        
        max_y_diff = np.max(result.y[0]) - np.min(result.y[0])
        v_ax.set_ylim(np.min(result.y[0]) - 0.1 * max_y_diff, np.max(result.y[0]) + 0.1 * max_y_diff)
        v_ax.set_xlabel(r"$r$")
        v_ax.set_ylabel(r"$V$")
        v_ax.legend()
        plt.show()

    if result.t_events[0].size != 0:
        return ((r_span[1] - result.t_events[0][0]) / R_p)**5
    elif result.t_events[1].size != 0:
        return -((r_span[1] - result.t_events[1][0]) / R_p)**5
    else:
        return 0
    
def select_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0, V_s_bounds):
    """
    Choose a lower and upper bound for V_s using the passed bounds and the equation limited bounds.
    """
    equation_V_s_bounds = get_equation_limited_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0)
    V_s_lower_bound = equation_V_s_bounds[0] if V_s_bounds[0] is None else max(equation_V_s_bounds[0], V_s_bounds[0])
    V_s_upper_bound = equation_V_s_bounds[1] if V_s_bounds[1] is None else min(equation_V_s_bounds[1], V_s_bounds[1])

    return V_s_lower_bound, V_s_upper_bound

def get_optimal_V_s(R_s, R_p, T_e, T_i, V_a, V_0, n_0, V_s_estimate=None, V_s_bounds=(None, None), get_bound_distances=False):
    """
    Given `R_s` and the plasma parameters, find the `V_s` that doesn't diverge to +/- infinity.

    Returns
    -------
    V_s : float or None
        Returns `None` if both the lower and upper bound had the same sign.
    lower_bound_distance, upper_bound_distance : float
        Divergence distance for the upper and lower bounds.
    """
    V_s_lower_bound, V_s_upper_bound = select_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0, V_s_bounds)

    lower_bound_distance = divergence_distance(V_s_lower_bound, R_s, R_p, T_e, T_i, V_a, V_0, n_0)
    upper_bound_distance = divergence_distance(V_s_upper_bound, R_s, R_p, T_e, T_i, V_a, V_0, n_0)
    if not valid_bounds(lower_bound_distance, upper_bound_distance) or get_bound_distances:
        return None, lower_bound_distance, upper_bound_distance

    # Use the estimated V_s, if there is one, to further reduce bounds on V_s.
    if V_s_estimate is not None and V_s_lower_bound < V_s_estimate and V_s_estimate < V_s_upper_bound:
        # Calculate the divergence distance for the estimated V_s.
        estimate_distance = divergence_distance(V_s_estimate, R_s, R_p, T_e, T_i, V_a, V_0, n_0)
        if estimate_distance == 0:
            return V_s_estimate, lower_bound_distance, upper_bound_distance
        if np.sign(upper_bound_distance) == np.sign(estimate_distance):
            V_s_upper_bound = V_s_estimate
        else:
            V_s_lower_bound = V_s_estimate

    # Find the V_s value for which the distance goes to 0, i.e. the solution did not diverge.
    root_solution = root_scalar(
        divergence_distance, args=(R_s, R_p, T_e, T_i, V_a, V_0, n_0), 
        method='brentq', bracket=(V_s_lower_bound, V_s_upper_bound), 
        xtol=10**-8,
    )
    if not root_solution.converged:
        raise ValueError("There was an error while trying to find `V_s` for `R_s = {}`. The error was '{}'.".format(R_s, root_solution.flag))
    return root_solution.root, lower_bound_distance, upper_bound_distance

def alternate_get_R_s_and_V_s(R_p, T_e, T_i, V_a, V_0, n_0, R_s_estimate, R_s_bounds, V_s_estimate, V_s_bounds, known_valid_R_s):
    """
    Get the sonic radius and voltage by using 2D root finding.

    Returns
    -------
    R_s, V_s : float or None
        If `None` then there is no sonic radius and use equation (36).
    """
    if V_a == V_0:
        return None, None

    # Set the lower R_s bound to R_p or greater.
    R_s_bounds = list(R_s_bounds)
    R_s_bounds[0] = R_p if R_s_bounds[0] is None else max(R_p, R_s_bounds[0])

    if not known_valid_R_s:
        # Check if the sonic radius is well defined or if `R_s` should be `R_p`.
        V_s_at_R_p, lower_bound_distance, upper_bound_distance = get_optimal_V_s(R_p, R_p, T_e, T_i, V_a, V_0, n_0)

        if V_s_at_R_p is not None:
            if (V_a < V_0 and V_s_at_R_p < V_a) or (V_a > V_0 and V_s_at_R_p > V_a):
                logging.debug("Found `R_s = R_p`, `V_s = V_a` for `V_a = {:.6f}`".format(V_a))
                return None, None
            else:
                logging.info("Since `-|V_a| < V_s(R_p) < |V_a|` then `R_s` is greater than `R_p`.")
        else:
            logging.info("R_s(V_a) > R_p (i.e. the sonic radius for this applied voltage is greater than the probe radius). This is because the bounds on `V_s` were too strict.")

    # Set a valid estimate for R_s.
    R_s_estimate = max(R_s_estimate, R_p * (1 + 10**-8)) if R_s_estimate is not None else ((R_s_bounds[1] - R_s_bounds[0]) / 2 + R_s_bounds[0] if R_s_bounds[1] is not None else R_p * 2)
    # Set a valid estimate for V_s.
    V_s_bounds_at_R_s = get_equation_limited_V_s_bounds(R_s_estimate, R_p, T_e, T_i, V_a, V_0)
    V_s_bound_diff = V_s_bounds_at_R_s[1] - V_s_bounds_at_R_s[0]
    if V_s_estimate is not None:
        # Don't change the estimate if it fits in the equation bounds.
        if V_s_bounds_at_R_s[0] < V_s_estimate < V_s_bounds_at_R_s[1]:
            V_s_estimate = V_s_estimate
        # Otherwise set it to very close to the bound.
        elif V_s_estimate <= V_s_bounds_at_R_s[0]:
            V_s_estimate = V_s_bounds_at_R_s[0] + 10**-8 * V_s_bound_diff
        else:
            V_s_estimate = V_s_bounds_at_R_s[1] - 10**-8 * V_s_bound_diff
    else:
        V_s_estimate = V_s_bounds_at_R_s[0] + 0.5 * V_s_bound_diff

    # Now that we have the starting bounds we are going to create bounds on V_s and R_s by converting them to V_free and R_free where the new variables have no bounds.
    # TODO: Check that these conversion functions don't have lots of numerical error near bounds.
    to_R_s_func = lambda R_free: 0.5 * (2 * R_p + R_free + (4 * R_p**2 + R_free**2)**0.5)
    to_R_free_func = lambda R_s: R_p * ((R_s / R_p - 1) - 1 / (R_s / R_p - 1))
    def to_V_s_func(V_free, R_s):
        V_s_bounds_at_R_s = get_equation_limited_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0)
        V_s_bound_diff = V_s_bounds_at_R_s[1] - V_s_bounds_at_R_s[0]

        return V_s_bound_diff / (1 + np.exp(V_free / V_s_bound_diff)) + V_s_bounds_at_R_s[0]
    def to_V_free_func(V_s, R_s):
        V_s_bounds_at_R_s = get_equation_limited_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0)
        V_s_bound_diff = V_s_bounds_at_R_s[1] - V_s_bounds_at_R_s[0]

        return V_s_bound_diff * np.log(V_s_bound_diff / (V_s - V_s_bounds_at_R_s[0]) - 1)
    
    # Get the estimates in terms of R_free and V_free.
    R_free_estimate = to_R_free_func(R_s_estimate)
    V_free_estimate = to_V_free_func(V_s_estimate, R_s_estimate)

    def optimization_function(free_vec):
        """
        Parameters
        ----------
        free_vec : np.array[float]
            2 element vector of R_free and V_free.
        
        Returns
        -------
        result_vec : np.array[float]
            2 element vector of `found_V_a - actual_V_a` and the V_s `divergence_distance`.
        """
        # Convert the free variables to actual R_s and V_s.
        R_s = to_R_s_func(free_vec[0])
        V_s = to_V_s_func(free_vec[1], R_s)
        # print(R_s, V_s)

        # Solve the IVP going outward and inward.
        found_V_a = find_V_a(V_s, R_s, R_p, T_e, T_i, V_a, V_0, n_0)
        div_dist = divergence_distance(V_s, R_s, R_p, T_e, T_i, V_a, V_0, n_0)
        # print(found_V_a - V_a, div_dist)

        return np.array([found_V_a - V_a, div_dist])
    
    result = root(optimization_function, np.array([R_free_estimate, V_free_estimate]), options={'eps': 10**-5})
    final_R_s = to_R_s_func(result.x[0])
    final_V_s = to_V_s_func(result.x[1], final_R_s)

    return final_R_s, final_V_s

def get_R_s_and_V_s(R_p, T_e, T_i, V_a, V_0, n_0, R_s_estimate, R_s_bounds, V_s_estimate, V_s_bounds, known_valid_R_s): # lower_R_s_bound=None, upper_R_s_bound=None): 
    """
    Get the sonic radius and voltage.

    To do this, we first find if the passed `V_a` will give `R_s > R_p`. If not, 
    return `None`. Otherwise, iteratively find the upper bound on `R_s` if there 
    is not one and then iteratively find `R_s` and `V_s`.

    Parameters
    ----------
    R_p : float
        Probe radius (m)
    T_e, T_i : float
        Electron and ion temperatures (eV)
    V_a : float
        Applied probe voltage (V).
    V_0 : float
        Plasma potential (V)
    n_0 : float
        Particle density (m**-3)
    known_valid_R_s : bool
        Whether we know this V_a value will give an R_s > R_p.

    Returns
    -------
    R_s, V_s : float or None
        If `None` then there is no sonic radius and use equation (36).
    """
    return alternate_get_R_s_and_V_s(R_p, T_e, T_i, V_a, V_0, n_0, R_s_estimate, R_s_bounds, V_s_estimate, V_s_bounds, known_valid_R_s)

    if V_a == V_0:
        return None, None

    # Set the lower bound to R_p or greater.
    lower_R_s_bound = R_p if R_s_bounds[0] is None else max(R_p, R_s_bounds[0])
    upper_R_s_bound = R_s_bounds[1]

    if not known_valid_R_s:
        # Check if the sonic radius is well defined or if `R_s` should be `R_p`.
        V_s_at_R_p, lower_bound_distance, upper_bound_distance = get_optimal_V_s(R_p, R_p, T_e, T_i, V_a, V_0, n_0)

        if V_s_at_R_p is not None:
            if (V_a < V_0 and V_s_at_R_p < V_a) or (V_a > V_0 and V_s_at_R_p > V_a):
                logging.debug("Found `R_s = R_p`, `V_s = V_a` for `V_a = {:.6f}`".format(V_a))
                return None, None
            else:
                logging.info("Since `-|V_a| < V_s(R_p) < |V_a|` then `R_s` is greater than `R_p`.")
        else:
            logging.info("R_s(V_a) > R_p (i.e. the sonic radius for this applied voltage is greater than the probe radius). This is because the bounds on `V_s` were too strict.")

    if upper_R_s_bound is None:
        # Use the estimated R_s as a starting point for finding the upper R_s bound.
        if R_s_estimate is not None:
            curr_R_s = max(R_p, R_s_estimate)
        else:
            curr_R_s = lower_R_s_bound + R_p

        _, lower_bound_distance, upper_bound_distance = get_optimal_V_s(curr_R_s, R_p, T_e, T_i, V_a, V_0, n_0, V_s_estimate, V_s_bounds, True)

        if not valid_bounds(lower_bound_distance, upper_bound_distance):
            # If both distances have the same sign then we need to look for when there is at least one distance of the opposite sign at the bounds.
            prior_sign = np.sign(lower_bound_distance)
            lower_R_s_bound = curr_R_s
            curr_R_s *= 1.5
        else:
            # Otherwise we need to search for either when both bounds have the same sign or V_a is too small/large.
            prior_sign = None

        while upper_R_s_bound is None:
            curr_V_s, lower_bound_distance, upper_bound_distance = get_optimal_V_s(curr_R_s, R_p, T_e, T_i, V_a, V_0, n_0) # , V_s_estimate, V_s_bounds)

            if curr_V_s is None:
                # If we've gone too far and now both bounds give a divergence distance of the other sign then this is an upper bound.
                if np.sign(lower_bound_distance) != prior_sign:
                    upper_R_s_bound = curr_R_s
                else:
                    lower_R_s_bound = curr_R_s
                    curr_R_s *= 1.5
            else:
                # Check if `found_V_a < V_a < V_0` or `found_V_a > V_a > V_0`. If so then we've found an upper bound.
                curr_V_a_found = find_V_a(curr_V_s, curr_R_s, R_p, T_e, T_i, V_a, V_0, n_0)

                if (V_a < V_0 and curr_V_a_found > V_a) or (V_a > V_0 and curr_V_a_found < V_a):
                    lower_R_s_bound = curr_R_s
                    curr_R_s *= 1.5
                else:
                    upper_R_s_bound = curr_R_s

    # Iteratively search for the `(R_s, V_s)` combination by finding the best `V_s` at some location and then checking if the inward IVP gives `V_a`.
    if R_s_estimate is None:
        R_s_estimate = (lower_R_s_bound + upper_R_s_bound) / 2
    elif R_s_estimate < lower_R_s_bound:
        lower_R_s_bound = max(R_p, R_s_estimate)
        R_s_estimate = (lower_R_s_bound + upper_R_s_bound) / 2
    elif R_s_estimate > upper_R_s_bound:
        upper_R_s_bound = R_s_estimate
        R_s_estimate = (lower_R_s_bound + upper_R_s_bound) / 2

    if V_s_estimate is None:
        V_s_estimate = sum(select_V_s_bounds(R_s_estimate, R_p, T_e, T_i, V_a, V_0, V_s_bounds)) / 2

    # TODO: Implement use of the V_s_estimate and V_s_bounds.
    # Get the value of V_s and V_a at both bounds.
    upper_R_s_V_s, upper_R_s_V_s_distance, _ = get_optimal_V_s(upper_R_s_bound, R_p, T_e, T_i, V_a, V_0, n_0) #, V_s_estimate, V_s_bounds)
    lower_R_s_V_s, lower_R_s_V_s_distance, _ = get_optimal_V_s(lower_R_s_bound, R_p, T_e, T_i, V_a, V_0, n_0) #, V_s_estimate, V_s_bounds)
    while upper_R_s_V_s is None: # and not valid_bounds(upper_R_s_V_s_distance, lower_R_s_V_s_distance):
        # Keep increasing the R_s upper bound until we get to a point where there is a valid V_s.
        upper_R_s_bound *= 1.5
        upper_R_s_V_s, upper_R_s_V_s_distance, _ = get_optimal_V_s(upper_R_s_bound, R_p, T_e, T_i, V_a, V_0, n_0) #, V_s_estimate, V_s_bounds)
    upper_R_s_V_a = find_V_a(upper_R_s_V_s, upper_R_s_bound, R_p, T_e, T_i, V_a, V_0, n_0)
    lower_R_s_V_a = None if lower_R_s_V_s is None else find_V_a(lower_R_s_V_s, lower_R_s_bound, R_p, T_e, T_i, V_a, V_0, n_0)

    # If either the upper or lower R_s gave a result close to V_a then return the bound.
    if np.isclose(upper_R_s_V_a, V_a):
        return upper_R_s_bound, upper_R_s_V_s
    elif lower_R_s_V_a is not None:
        if np.isclose(lower_R_s_V_a, V_a):
            return lower_R_s_bound, lower_R_s_V_s
        if np.sign(upper_R_s_V_a - V_a) == np.sign(lower_R_s_V_a - V_a):
            raise ValueError("Passed bounds on `R_s` caused the found `V_a` at the limits to both be {} than the actual `V_a`.".format('upper' if np.sign(upper_R_s_V_a - V_a) > 0 else 'lower'))

    R_s_tolerance = 10**-8
    curr_R_s = R_s_estimate

    num_iter = 0
    # Do the bisection method to find the correct R_s by repeatedly finding the optimal V_s.
    while num_iter < 1 or upper_R_s_bound - lower_R_s_bound > R_s_tolerance:
        num_iter += 1

        if lower_R_s_V_s is None:
            V_s_estimate = upper_R_s_V_s
            V_s_bounds = (None, None)
        else:
            V_s_estimate = (upper_R_s_V_s + lower_R_s_V_s) / 2
            V_s_bounds = (min(upper_R_s_V_s, lower_R_s_V_s), max(upper_R_s_V_s, lower_R_s_V_s))

        curr_R_s_V_s = get_optimal_V_s(curr_R_s, R_p, T_e, T_i, V_a, V_0, n_0, V_s_estimate, V_s_bounds)[0]
        if curr_R_s_V_s is None:
            # Update the lower R_s bound.
            lower_R_s_bound = curr_R_s
            lower_R_s_V_s = curr_R_s_V_s
            # We can't update the lower bounds V_a as it has not yet been defined yet.
        else:
            curr_R_s_V_a = find_V_a(curr_R_s_V_s, curr_R_s, R_p, T_e, T_i, V_a, V_0, n_0)

            if np.isclose(curr_R_s_V_a, V_a):
                break
            elif (curr_R_s_V_a > V_a and upper_R_s_V_a > V_a) or (curr_R_s_V_a < V_a and upper_R_s_V_a < V_a):
                # Update the upper R_s bound.
                upper_R_s_bound = curr_R_s
                upper_R_s_V_s = curr_R_s_V_s
                upper_R_s_V_a = curr_R_s_V_a
            else:
                # Update the lower R_s bound.
                lower_R_s_bound = curr_R_s
                lower_R_s_V_s = curr_R_s_V_s
                lower_R_s_V_a = curr_R_s_V_a

        curr_R_s = (lower_R_s_bound + upper_R_s_bound) / 2

    if curr_R_s_V_s is None:
        raise ValueError("Bounds on `R_s`, `{}`, and `V_s`, `{}`, made it impossible to find the correct `(R_s, V_s)` for `V_a = {:.6f}`.".format(R_s_bounds, V_s_bounds, V_a))

    logging.debug("Found `R_s = {:.6f}`, `V_s = {:.6f}` for `V_a = {:.6f}`".format(curr_R_s, curr_R_s_V_s, V_a))
    return curr_R_s, curr_R_s_V_s

def calculate_current_density(V_s, R_s, R_p, T_e, T_i, V_a, V_0, n_0, split_contributions=False):
    """
    Get the current to the probe.

    Parameters
    ----------
    V_s, R_s : float
        Sonic voltage and radius (V and m).
    R_p : float
        Probe radius (m)
    T_e, T_i : float
        Electron and ion temperatures (eV)
    V_a : float
        Applied probe voltage (V).
    V_0 : float
        Plasma potential (V)
    n_0 : float
        Particle density (m**-3)
    split_contributions : bool, default=False
        If `False` then return the total current density. If `True` then return 
        the electron and ion contributions separately.

    Returns
    -------
    total_current_density or (electron_current_density, ion_current_density) : float
        Current density (A / m**2).
    
    """
    u_the = (Q * T_e / 9.109383e-31)**0.5
    u_thi = (Q * T_i / 1.672621e-27)**0.5

    # If R_s and V_s are None then we are within the V limits where there is no R_s.
    if R_s is None and V_s is None:
        R_s = R_p

        U_e = 1
        n_e = n_0 * np.exp(-0.5) * np.exp((V_a - V_0) / T_e)
        e_current = -Q * n_e * u_the * U_e**0.5

        U_i = 1
        n_i = n_0 * np.exp(-0.5) * np.exp(-(V_a - V_0) / T_i)
        i_current = Q * n_i * u_thi * U_i**0.5
    elif V_a < V_0:
        U_e = 1
        n_e = n_0 * np.exp(-0.5) * np.exp((V_a - V_0) / T_e)
        e_current = -Q * n_e * u_the * U_e**0.5

        # We can simplify this old calculation and reduce numerical errors by 
        # substituting `U_i` into `u_i` and `n_i` and simplifying.
        # U_i = -lambw(-R_s**2 / R_p**2 * np.exp(-2 * (V_s - V_a) / T_i - 1), k=-1)
        # n_i = n_0 * np.exp(-U_i / 2 - (V_a - V_0) / T_i)
        i_current = Q * u_thi * n_0 * R_s / R_p * np.exp((V_0 - V_s) / T_i - 0.5)
    elif V_a > V_0:
        # U_e = -lambw(-R_s**2 / R_p**2 * np.exp(2 * (V_s - V_a) / T_e - 1), k=-1)
        # n_e = n_0 * np.exp(-U_e / 2 + (V_a - V_0) / T_e)
        e_current = -Q * u_the * n_0 * R_s / R_p * np.exp((V_s - V_0) / T_e - 0.5)

        U_i = 1
        n_i = n_0 * np.exp(-0.5) * np.exp(-(V_a - V_0) / T_i)
        i_current = Q * n_i * u_thi * U_i**0.5
    else:
        raise ValueError("The given combination of R_s ({}), V_s ({}), V_a ({}), and V_0 ({}) is not allowable.".format(R_s, V_s, V_a, V_0))

    if split_contributions:
        return e_current, i_current
    else:
        return e_current + i_current
    
def fit_R_s(V_a_values, R_s_values, R_p, V_0, num_std):
    """
    Fit the found R_s values to predict the next R_s.

    Parameters
    ----------
    V_a_values, R_s_values : np.array[float]
        1D-arrays of values sorted such that `|V_a - V_0|` increases and 
        `V_a - V_0` is all the same sign.
    num_std : float
        Multiplier of the standard deviation.

    Returns
    -------
    fit_fcn, (min_fit_fcn, max_fit_fcn) : func[float] -> float
        Functions that take a `V_a` value and give an expected `R_s` value. The 
        `fit_fcn` gives an accurate value and the `max_fit_fcn` and `min_fit_fcn` 
        give bounds on that value within `num_std` standard deviations.
    """
    if np.allclose(R_s_values, R_p):
        # This means we have no good way to estimate the next point so just return functions that give R_p.
        return lambda x_vals: R_p, (lambda x_vals: None, lambda x_vals: None)

    fitting_data = Data(V_a_values, R_s_values)

    # Wherever the R_s_values == R_p, we want our fit to be zero.
    zero_boundary_index = np.argmax(R_s_values > R_p)
    
    # Decide whether to include an additional term in the fit function. Only do 
    # this if we have at least 3 points where R_s > R_p.
    estimate_R_s_lim = True if R_s_values.size - zero_boundary_index >= 3 else False
    last_invalid_V_a = (sum(V_a_values[zero_boundary_index - 1:zero_boundary_index]) / 2 if zero_boundary_index > 0 else V_0)
    side = np.sign(V_a_values[-1] - V_a_values[0])
    
    # Guess the initial beta values.
    # We'll assume the power is 0.5 and compute what the multiplier should be.
    if estimate_R_s_lim:
        beta0 = np.array((
            (R_s_values[zero_boundary_index] - R_p) / abs(V_a_values[zero_boundary_index] - last_invalid_V_a)**0.5,
            0.5, last_invalid_V_a
        ))
        fit_fcn = lambda beta_vals, x_val: R_p if side * (x_val - beta_vals[2]) < 0 else beta_vals[0] * (side * (x_val - beta_vals[2]))**beta_vals[1] + R_p
    else:
        beta0 = np.array((
            (R_s_values[zero_boundary_index] - R_p) / abs(V_a_values[zero_boundary_index] - last_invalid_V_a)**0.5,
            0.5
        ))
        fit_fcn = lambda beta_vals, x_val: R_p if side * (x_val - last_invalid_V_a) < 0 else beta_vals[0] * (side * (x_val - last_invalid_V_a))**beta_vals[1] + R_p
    partial_fit_fcn = lambda beta_vals, x_vals: np.array([fit_fcn(beta_vals, x) for x in ([x_vals] if isinstance(x_vals, float) else x_vals)])
    full_fit_fcn = lambda beta_vals, x_vals: partial_fit_fcn(beta_vals, x_vals)[0] if isinstance(x_vals, float) else partial_fit_fcn(beta_vals, x_vals)
    
    R_s_model = Model(full_fit_fcn)

    odr = ODR(fitting_data, R_s_model, beta0=beta0)
    result = odr.run()
    beta = result.beta
    std_beta = result.sd_beta

    min_beta = beta.copy()
    min_beta[:2] = min_beta[:2] - num_std * std_beta[:2]
    max_beta = beta.copy()
    max_beta[:2] = max_beta[:2] + num_std * std_beta[:2]

    if estimate_R_s_lim:
        min_beta[2] += side * num_std * std_beta[2]
        max_beta[2] -= side * num_std * std_beta[2]

    # If the standard deviation of beta is 0 then we can't really give a good upper and lower bound.
    if np.allclose(std_beta, 0):
        return lambda x_vals: full_fit_fcn(beta, x_vals), (lambda x_vals: None, lambda x_vals: None)
    else:
        return lambda x_vals: full_fit_fcn(beta, x_vals), (lambda x_vals: full_fit_fcn(min_beta, x_vals), lambda x_vals: full_fit_fcn(max_beta, x_vals))

def fit_V_s(V_a_values, V_s_values, V_0, num_std, T_e, T_i):
    """
    Fit the found V_s values to predict the next V_s.

    Parameters
    ----------
    V_a_values, V_s_values : np.array[float]
        1D-arrays of values sorted such that `|V_a - V_0|` increases and 
        `V_a - V_0` is all the same sign.
    num_std : float
        Multiplier of the standard deviation.

    Returns
    -------
    fit_fcn, (min_fit_fcn, max_fit_fcn) : func[float] -> float
        Functions that take a `V_a` value and give an expected `V_s` value. The 
        `fit_fcn` gives an accurate value and the `max_fit_fcn` and `min_fit_fcn` 
        give bounds on that value within `num_std` standard deviations.
    """
    if np.allclose(V_a_values, V_s_values):
        # This means that all the past V_a points had R_s = R_p and thus V_s = V_a.
        return lambda x_vals: x_vals, (lambda x_vals: None, lambda x_vals: None)
    
    fitting_data = Data(V_a_values, V_s_values)

    large_V_a_fcn = lambda beta, x: beta[0] * x**-beta[1] + beta[2]
    small_V_a_fcn = lambda beta, x: (1 - np.exp(-beta[3] * x)) * beta[4]
    fit_fcn = lambda beta, x: (large_V_a_fcn(beta, x) * small_V_a_fcn(beta, x)) / (large_V_a_fcn(beta, x) + small_V_a_fcn(beta, x))

    side = np.sign(V_a_values[-1] - V_a_values[0])
    V_s_limit = V_0 + side * (T_e * T_i) / (T_e + T_i)
    beta0 = np.array([10, -1, 2 * V_s_limit, 1 / (2 * V_s_limit)])


    
def estimate_R_s_and_V_s(prior_R_s_values, prior_V_s_values, V_a_values, R_p, V_0, new_V_a):
    """
    Create a list of estimates and bounds for R_s and V_s.

    Parameters
    ----------
    prior_R_s_values, prior_V_s_values, V_a_values : np.array[float]
        1D-array of values for `R_s`, `V_s`, and the associated `V_a`. This 
        should only contain points for `V_a` > `V_0` or `V_a` < `V_0`. This
        should also be sorted from oldest to newest which is the same as
        increasing `|V_a - V_0|`.

    Returns
    -------
    all_R_s_estimates : list[float or None]
    all_R_s_bounds : list[tuple[float or None]]
        List of tuples of 2 floats. These represent various guesses for bounds 
        on `R_s` from best to worst.
    all_V_s_estimates : list[float or None]
    all_V_s_bounds : list[tuple[float or None]]
        Same as `all_R_s_bounds` but for `V_s`. If `None` is passed then we use 
        the bounds given by equations (B8) & (B9).

    Notes
    -----
    If a returned value is `None` then we don't have a guess for it.
    """
    NUM_PRIOR = V_a_values.size
    all_R_s_estimates = []
    all_R_s_bounds = []
    all_V_s_estimates = []
    all_V_s_bounds = []

    side = -1 if new_V_a < V_0 else +1

    if NUM_PRIOR >= 2:
        # R_s
        estimate_fcn, (lower_bound_fcn, upper_bound_fcn) = fit_R_s(V_a_values, prior_R_s_values, R_p, V_0, 2)
        all_R_s_estimates.append(estimate_fcn(new_V_a))
        if lower_bound_fcn(new_V_a) is not None:
            lower_R_s_bound = lower_bound_fcn(new_V_a)
            upper_R_s_bound = upper_bound_fcn(new_V_a)
        else:
            lower_R_s_bound = R_p
            upper_R_s_bound = None
        
        # We know R_s must increase and must be concave down so check this is the case with the bounds.
        lower_R_s_bound = max(prior_R_s_values[-1], lower_R_s_bound)
        if prior_R_s_values[-2] != R_p:
            alt_upper_R_s_bound = (prior_R_s_values[-1] - prior_R_s_values[-2]) / (V_a_values[-1] - V_a_values[-2]) * (new_V_a - V_a_values[-1]) + prior_R_s_values[-1]
            
            if upper_R_s_bound is None:
                upper_R_s_bound = alt_upper_R_s_bound
            else:
                upper_R_s_bound = min(upper_R_s_bound, alt_upper_R_s_bound)

        R_s_bounds = (lower_R_s_bound, upper_R_s_bound)
        all_R_s_bounds.append(R_s_bounds)

        # V_s
        if NUM_PRIOR >= 3:
            V_s_fit_fcn = lambda beta, x: beta[1] * (side * (x - V_0))**beta[0] + beta[2]
            V_s_model = Model(V_s_fit_fcn)
            # We only want to use points away from the R_s = R_p limit.
            data_indeces = np.nonzero(prior_R_s_values > R_p)
            V_s_fitting_data = Data(V_a_values[data_indeces], prior_V_s_values[data_indeces])
            V_s_odr = ODR(V_s_fitting_data, V_s_model, beta0=np.array((-1, prior_V_s_values[0] - prior_V_s_values[-1], prior_V_s_values[-1])))
            result = V_s_odr.run()
            V_s_beta = result.beta
            V_s_std_beta = result.sd_beta

            # V_s can be estimated by fitting the corresponding function.
            all_V_s_estimates.append(
                V_s_fit_fcn(V_s_beta, new_V_a)
            )
            if NUM_PRIOR >= 4:
                # Use the standard deviation of beta to get good bounds.
                std_multiple = 2
                V_s_std_beta *= std_multiple

                min_beta = np.array((V_s_beta[0] - side * V_s_std_beta[0], V_s_beta[1] - V_s_std_beta[1], V_s_beta[2] - V_s_std_beta[2]))
                max_beta = np.array((V_s_beta[0] + side * V_s_std_beta[0], V_s_beta[1] + V_s_std_beta[1], V_s_beta[2] + V_s_std_beta[2]))

                all_V_s_bounds.append((V_s_fit_fcn(min_beta, new_V_a), V_s_fit_fcn(max_beta, new_V_a)))
            elif NUM_PRIOR == 3:
                if prior_V_s_values[-1] > prior_V_s_values[-2]:
                    all_V_s_bounds.append((
                        prior_V_s_values[-1],
                        (prior_V_s_values[-1] - prior_V_s_values[-2]) / (V_a_values[-1] - V_a_values[-2]) * (new_V_a - V_a_values[-1]) + prior_V_s_values[-1],
                    ))
                else:
                    all_V_s_bounds.append((
                        (prior_V_s_values[-1] - prior_V_s_values[-2]) / (V_a_values[-1] - V_a_values[-2]) * (new_V_a - V_a_values[-1]) + prior_V_s_values[-1],
                        prior_V_s_values[-1],
                    ))
        elif NUM_PRIOR == 2:
            # We can't make a good estimate of V_s.
            all_V_s_estimates.append(None)
            # Now that we have two points we can use that to bound V_s.
            if prior_V_s_values[-1] > prior_V_s_values[-2]:
                all_V_s_bounds.append((
                    prior_V_s_values[-1],
                    (prior_V_s_values[-1] - prior_V_s_values[-2]) / (V_a_values[-1] - V_a_values[-2]) * (new_V_a - V_a_values[-1]) + prior_V_s_values[-1],
                ))
            else:
                all_V_s_bounds.append((
                    (prior_V_s_values[-1] - prior_V_s_values[-2]) / (V_a_values[-1] - V_a_values[-2]) * (new_V_a - V_a_values[-1]) + prior_V_s_values[-1],
                    prior_V_s_values[-1],
                ))

    if NUM_PRIOR >= 1:
        # This is slightly better than no guess at all as it just uses the previous values as a starting point.
        all_R_s_estimates.append(None)
        all_R_s_bounds.append((prior_R_s_values[-1], None))
        all_V_s_estimates.append(prior_V_s_values[-1])
        all_V_s_bounds.append((None, None))
    
    # The most general guess is just giving no bounds nor estimates.
    all_R_s_estimates.append(None)
    all_R_s_bounds.append((None, None))
    all_V_s_estimates.append(None)
    all_V_s_bounds.append((None, None))

    return all_R_s_estimates, all_R_s_bounds, all_V_s_estimates, all_V_s_bounds

def get_current_densities(R_p, T_e, T_i, V_a_array, V_0, n_0, plot_R_s=False):
    """
    Calculate the current to the probe for an array of `V_a` values.

    Parameters
    ----------
    R_p : float
        Probe radius (m)
    T_e, T_i : float
        Electron and ion temperatures (eV)
    V_a_array : np.array[float]
        1D-array of applied probe voltages (V). This should be sorted.
    V_0 : float
        Plasma potential (V)
    n_0 : float
        Particle density (m**-3)

    Returns
    -------
    current_densities : np.array[float]
        Current density (A / m**2).
    """
    V_a_array = V_a_array.astype(float)
    if not np.all(np.sort(V_a_array) == V_a_array):
        raise ValueError('The V_a array should be sorted.')
    
    current_densities = np.zeros_like(V_a_array)
    e_current_densities = np.zeros_like(V_a_array)
    i_current_densities = np.zeros_like(V_a_array)
    R_s_values = np.zeros_like(V_a_array)
    V_s_values = np.zeros_like(V_a_array)
    V_0_boundary_index = np.argmax(V_a_array >= V_0)

    # The code works fastest when |V_A - V_0| increases because of how we can get bounds on R_p.
    indeces = np.concatenate((np.arange(V_0_boundary_index, V_a_array.size), np.arange(V_0_boundary_index - 1, -1, -1)))
    
    # for i in tqdm.tqdm(indeces):
    for i in indeces:
        if i >= V_0_boundary_index:
            R_s_estimates, R_s_bounds, V_s_estimates, V_s_bounds = estimate_R_s_and_V_s(
                R_s_values[V_0_boundary_index:i], V_s_values[V_0_boundary_index:i], V_a_array[V_0_boundary_index:i], R_p, V_0, V_a_array[i]
            )
        else:
            R_s_estimates, R_s_bounds, V_s_estimates, V_s_bounds = estimate_R_s_and_V_s(
                R_s_values[V_0_boundary_index - 1:i:-1], V_s_values[V_0_boundary_index - 1:i:-1], V_a_array[V_0_boundary_index - 1:i:-1], R_p, V_0, V_a_array[i]
            )

        if i == V_0_boundary_index or i == V_0_boundary_index - 1:
            known_valid_R_s = False
        elif (i > V_0_boundary_index and R_s_values[i - 1] == R_p) or (i < V_0_boundary_index - 1 and R_s_values[i + 1] == R_p):
            known_valid_R_s = False
        else:
            known_valid_R_s = True

        for j in range(len(R_s_estimates)):
            try:
                R_s, V_s = get_R_s_and_V_s(R_p, T_e, T_i, V_a_array[i], V_0, n_0, R_s_estimates[j], R_s_bounds[j], V_s_estimates[j], V_s_bounds[j], known_valid_R_s)
            except ValueError as e:
                logging.exception("Couldn't get R_s and V_s using `R_s_bounds = {}` and `V_s_bounds = {}` so trying with different bounds. Error was:\n{}".format(R_s_bounds[j], V_s_bounds[j], e))
            else:
                break
        else:
            # raise ValueError("Couldn't get `R_s` and `V_s` using any bounds for index {}.".format(i))
            logging.warning("Couldn't get `R_s` and `V_s` using any bounds for index {}. Using estimated `R_s` and `V_s`.".format(i))
            for j in range(len(R_s_estimates)):
                R_s = None
                V_s = None

                if R_s_estimates[j] is not None:
                    R_s = R_s_estimates[j]
                elif R_s_bounds[j] != (None, None):
                    if R_s_bounds[j][0] is not None and R_s_bounds[j][1] is not None:
                        R_s = sum(R_s_bounds[j]) / 2
                    elif R_s_bounds[j][0] is not None:
                        R_s = R_s_bounds[j][0]
                    else:
                        R_s = R_s_bounds[j][0]
                
                if V_s_estimates[j] is not None:
                    V_s = V_s_estimates[j]
                elif V_s_bounds[j] != (None, None):
                    if V_s_bounds[j][0] is not None and V_s_bounds[j][1] is not None:
                        V_s = sum(V_s_bounds[j]) / 2
                    elif V_s_bounds[j][0] is not None:
                        V_s = V_s_bounds[j][0]
                    else:
                        V_s = V_s_bounds[j][0]
                
                if R_s is not None and V_s is not None:
                    break
            else:
                raise ValueError("Couldn't get `R_s` and `V_s` using any bounds for index {} and couldn't guess them either.".format(i))

        e_current, i_current = calculate_current_density(V_s, R_s, R_p, T_e, T_i, V_a_array[i], V_0, n_0, split_contributions=True)
        current_densities[i] = e_current + i_current
        e_current_densities[i] = e_current
        i_current_densities[i] = i_current

        R_s_values[i] = R_s if R_s is not None else R_p
        V_s_values[i] = V_s if V_s is not None else V_a_array[i]

    if plot_R_s:
        # Plot R_s vs V_a and V_s vs V_a and fit curves to both.
        fig, axs = plt.subplots(3, sharex=True)

        axs[0].axhline(R_p, color='black', linestyle='dashed', label=r"$R_p$")
        axs[0].axvline(V_0, color='black', linestyle='dashed', label=r"$V_0$")

        point_step = 1
        # Fit the R_s(V_a) data for V_a < V_0 and V_a > V_0.
        all_num_points = list(range(3, min(V_0_boundary_index, V_a_array.size - V_0_boundary_index), point_step))
        for i, num_points in enumerate(all_num_points):
            for j, side in enumerate([-1, +1]):
                start_index = V_0_boundary_index if side == +1 else V_0_boundary_index - 1
                indeces_slice = slice(start_index, start_index + side * num_points, side)
                sliced_V_a = V_a_array[indeces_slice]
                sliced_R_s = R_s_values[indeces_slice]

                fit_fcn, (min_fit_fcn, max_fit_fcn) = fit_R_s(sliced_V_a, sliced_R_s, R_p, V_0, 2)

                index_start = start_index + side * (num_points - 1)
                index_end = start_index + side * (num_points + point_step)
                extended_indeces_slice = slice(index_start, index_end if index_end >= 0 else None, side)
                extended_V_a = V_a_array[extended_indeces_slice]
                extended_V_a = np.linspace(extended_V_a[0], extended_V_a[-1], num=point_step * 10)
                
                if j == 0:
                    prior_color = axs[0].plot(extended_V_a, fit_fcn(extended_V_a), alpha=1)[0].get_color()
                else:
                    axs[0].plot(extended_V_a, fit_fcn(extended_V_a), color=prior_color, alpha=1)
                
                if min_fit_fcn(extended_V_a[0]) is not None:
                    axs[0].fill_between(extended_V_a, min_fit_fcn(extended_V_a), max_fit_fcn(extended_V_a), alpha=0.2, color=prior_color)
                axs[0].plot(extended_V_a, fit_fcn(extended_V_a), color=prior_color, alpha=1)

        axs[0].scatter(V_a_array, R_s_values, color='black')
        axs[0].set_xlabel(r"$V_a$")
        axs[0].set_ylabel(r"$R_s$")
        axs[0].set_ylim(0, 1.1 * max(R_s_values))

        # Try fitting the V_s data. Do this for V_a > V_0 and for V_a < V_0.
        all_num_points = list(range(3, min(V_0_boundary_index, V_a_array.size - V_0_boundary_index), point_step))
        for i, num_points in enumerate(all_num_points):
            for j, side in enumerate([-1, +1]):
                fit_fcn = lambda beta, x: beta[1] * (side * (x - V_0))**beta[0] + beta[2]
                exp_model = Model(fit_fcn)
                
                start_index = V_0_boundary_index if side == +1 else V_0_boundary_index - 1
                indeces_slice = slice(start_index, start_index + side * num_points, side)
                fit_indeces = np.nonzero((R_s_values[indeces_slice] > R_p) & (side * V_a_array[indeces_slice] > V_0))

                sorted_indeces = np.argsort(side * V_a_array[indeces_slice][fit_indeces])
                sorted_V_a = V_a_array[indeces_slice][fit_indeces][sorted_indeces]
                sorted_V_s = V_s_values[indeces_slice][fit_indeces][sorted_indeces]

                if sorted_V_a.size == 0:
                    continue

                fitting_data = Data(sorted_V_a, sorted_V_s)
                odr = ODR(fitting_data, exp_model, beta0=np.array((-1, sorted_V_s[0] - sorted_V_s[-1], sorted_V_s[-1])))
                result = odr.run()
                beta = result.beta
                std_beta = result.sd_beta

                index_start = start_index + side * (num_points - 1)
                index_end = start_index + side * (num_points + point_step)
                extended_indeces_slice = slice(index_start, index_end if index_end >= 0 else None, side)
                extended_V_a = V_a_array[extended_indeces_slice]
                extended_V_a = np.linspace(extended_V_a[0], extended_V_a[-1], num=point_step * 10)

                min_beta = np.array((beta[0] - side * std_beta[0], beta[1] - std_beta[1], beta[2] - std_beta[2]))
                max_beta = np.array((beta[0] + side * std_beta[0], beta[1] + std_beta[1], beta[2] + std_beta[2]))
                if j == 0:
                    prior_color = axs[1].fill_between(extended_V_a, fit_fcn(min_beta, extended_V_a), fit_fcn(max_beta, extended_V_a), alpha=0.2).get_facecolor()
                else:
                    axs[1].fill_between(extended_V_a, fit_fcn(min_beta, extended_V_a), fit_fcn(max_beta, extended_V_a), alpha=0.2, color=prior_color)

                axs[1].plot(extended_V_a, fit_fcn(beta, extended_V_a), color=prior_color, alpha=1)

        V_s_lower = np.zeros_like(V_a_array)
        V_s_upper = np.zeros_like(V_a_array)
        for i in range(V_a_array.size):
            V_s_lower[i], V_s_upper[i] = get_equation_limited_V_s_bounds(R_s_values[i], R_p, T_e, T_i, V_a_array[i], V_0)
        line_color = axs[1].plot(V_a_array, V_s_lower, alpha=0.2, linestyle='dashed', color='black')[0].get_color()
        axs[1].plot(V_a_array, V_s_upper, alpha=0.2, color=line_color, linestyle='dashed')
        axs[1].scatter(V_a_array, V_s_values, color=line_color, alpha=1)
        axs[1].set_xlabel(r"$V_a$")
        axs[1].set_ylabel(r"$V_s$")
        axs[1].set_ylim(min(V_s_values) - 0.1 * (max(V_s_values) - min(V_s_values)), max(V_s_values) + 0.1 * (max(V_s_values) - min(V_s_values)))

        axs[2].axhline(0, color='black', linestyle='dashed')
        axs[2].axvline(V_0, color='black', linestyle='dashed')

        axs[2].plot(V_a_array, e_current_densities, color='blue', label='e')
        axs[2].scatter(V_a_array, e_current_densities, color='blue')
        axs[2].plot(V_a_array, i_current_densities, color='red', label='i')
        axs[2].scatter(V_a_array, i_current_densities, color='red')
        axs[2].plot(V_a_array, current_densities, color='black', label='total')

        axs[2].set_xlabel(r"$V_a$")
        axs[2].set_ylabel(r"$J$")
        axs[2].legend()

        fig.suptitle(r'$T_e = {:.2f} \mathrm{{ eV}}, \, T_i = {:.2f} \mathrm{{ eV}}, \, n_0 = {:.4E} \mathrm{{ m}}^{{-3}}$'.format(T_e, T_i, n_0))
        plt.show()
    return current_densities

def save_iv_curves(tip_radius, save_filepath):
    """
    Parameters
    ----------
    tip_radius : float
        Radius of tip (m).
    save_filepath : str
        Path to save to. Should end in `.mat`. If a file already exists at this 
        location then instead restart computations using the values found in 
        this file.
    """
    if os.path.isfile(save_filepath):
        loaded_dict = loadmat(save_filepath)

        density_values = loaded_dict['density'][0]
        Te_values = loaded_dict['Te'][0]
        Ti_values = loaded_dict['Ti'][0]
        potential_values = loaded_dict['V'][0]
        current_density_array = loaded_dict['current_density']
        last_saved_index = loaded_dict['last_saved_index'][0, 0]
        logging.info("Loading from `{}` and restarting computations from last saved index (#{}).".format(save_filepath, last_saved_index))
    else:
        logging.info("Starting from beginning for computations.")
        # The following array sizes should create a `current_density_array` of size 256 MB.
        density_values = 10**np.linspace(15, 19, num=64)
        Te_values = np.linspace(0.1**0.5, 125**0.5, num=128)**2
        Ti_values = np.linspace(0.01**0.5, 50**0.5, num=32)**2
        potential_values = np.linspace(-(50**(1 / 3)), 50**(1 / 3), num=32)**3

        density_values = np.array([10**17])
        Te_values = np.array([3])
        Ti_values = np.array([0.2])
        potential_values = np.linspace(-(3**(1 / 3)), 3**(1 / 3), num=50)**3
        tip_radius = .00015

        # Initialize an empty array for holding the current densities.
        current_density_array = np.empty((density_values.size, Te_values.size, Ti_values.size, potential_values.size))
        last_saved_index = -1

    save_dict = {
        "density" : density_values,
        "Te" : Te_values,
        "Ti" : Ti_values,
        "V" : potential_values,
        "current_density" : current_density_array,
        "last_saved_index" : last_saved_index
    }
    logging.debug("Saving top level values.")
    # savemat(save_filepath, save_dict, do_compression=True)

    save_period = 1
    start_time = time.time()
    # for i, (n_index, Te_index, Ti_index) in enumerate(tqdm.tqdm(itertools.product(np.arange(density_values.size), np.arange(Te_values.size), np.arange(Ti_values.size)), total=density_values.size * Te_values.size * Ti_values.size)):
    for i, (n_index, Te_index, Ti_index) in enumerate(itertools.product(np.arange(density_values.size), np.arange(Te_values.size), np.arange(Ti_values.size))):
        if i <= last_saved_index:
            continue

        n = density_values[n_index]
        Te = Te_values[Te_index]
        Ti = Ti_values[Ti_index]

        # a = (Ti + Te) / (Ti * Te) * 10**-1
        # potential_values = 1 / a * erfinv(np.linspace(erf(a * -10), erf(a * +10), num=128))
        if potential_values[0] == -np.inf or potential_values[0] == np.nan:
            continue

        if Te < Ti:
            # When fitting we will make sure Ti < Te.
            continue
        
        # potential_values = np.arange(11, 100, 5)

        try:
            current_density_array[n_index, Te_index, Ti_index] = get_current_densities(tip_radius, Te, Ti, potential_values, 0, n, plot_R_s=False)
            
            if (i + 1) % save_period == 0:
                logging.debug("Saving values for iteration #{}.".format(i))
                save_dict['current_density'] = current_density_array
                save_dict['last_saved_index'] = i
                # savemat(save_filepath, save_dict, do_compression=True)
        except Exception:
            logging.exception("Exception occured on iteration #{}. Most recent save was iteration #{}. Quitting...".format(i, save_dict['last_saved_index']))
            raise
    
    logging.info("Successfully saved all values in {} seconds.".format(time.time() - start_time))


# *************** Everything below this is just for testing ***********************
def V_p_found_changes(V_a=15):
    R_p = 0.25 * 10**-3
    T_e = 3
    T_i = 0.2
    V_0 = 20
    n_0 = 10**17

    num_R_s = 50
    num_V_s = 100
    if V_a < V_0:
        # R_s_values = np.linspace(0.8 * 10**-3, 1.2 * 10**-3, num=num_R_s)
        R_s_values = np.linspace(R_p, 1.2 * 10**-3, num=num_R_s)
        largest_v_s_value = max(get_equation_limited_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0)[1] for R_s in R_s_values)
        V_s_values = np.linspace(get_equation_limited_V_s_bounds(R_s_values[0], R_p, T_e, T_i, V_a, V_0)[0], largest_v_s_value, num=num_V_s)
    else:
        # R_s_values = np.linspace(0.28 * 10**-3, 0.40 * 10**-3, num=num_R_s)
        R_s_values = np.linspace(R_p, 0.40 * 10**-3, num=num_R_s)
        smallest_v_s_value = min(get_equation_limited_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0)[0] for R_s in R_s_values)
        V_s_values = np.linspace(smallest_v_s_value, get_equation_limited_V_s_bounds(R_s_values[0], R_p, T_e, T_i, V_a, V_0)[1], num=num_V_s)

    V_a_found = np.empty((R_s_values.size, V_s_values.size))

    optimal_V_s_values = np.empty_like(R_s_values)
    for i, R_s in enumerate(tqdm.tqdm(R_s_values)):
        lower_V_s, upper_V_s = get_equation_limited_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0)
        optimal_V_s_values[i] = get_optimal_V_s(R_s, R_p, T_e, T_i, V_a, V_0, n_0)[0]
        for j, V_s in enumerate(V_s_values):
            if V_s < lower_V_s or V_s > upper_V_s:
                V_a_found[i, j] = np.nan
                continue

            r_span = (R_s, R_p)
            V_vec_0 = initial_V_vec(R_s, T_e, T_i, V_a, V_0, V_s)

            result = solve_ivp(ode_function, r_span, V_vec_0, args=(R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0))

            V_a_found[i, j] = result.y[0][-1]

    V_a_masked = np.ma.masked_invalid(V_a_found)
    R_s_grid, V_s_grid = np.meshgrid(R_s_values, V_s_values)

    plt.figure()
    plt.pcolormesh(R_s_grid, V_s_grid, V_a_masked.T, vmin=V_a - abs(V_0 - V_a), vmax=V_a + abs(V_0 - V_a), cmap='PiYG')
    # plt.contour(R_s_grid, V_s_grid, V_a_masked.T, levels=np.linspace(V_a - abs(V_0 - V_a), V_a + abs(V_0 - V_a)))
    plt.plot(R_s_values, optimal_V_s_values, color='black')
    plt.xlabel(r'$R_s$')
    plt.ylabel(r'$V_s$')
    plt.colorbar(label=r'$V_a^{{\mathrm{found}}}$')
    plt.title(r'$V_a ({}) {} V_0 ({})$'.format(V_a, '<' if V_a < V_0 else '>', V_0))
    plt.show(block=False)

def V_s_divergence(V_a=15, plot_raw=False):
    R_p = 0.25 * 10**-3
    T_e = 3
    T_i = 0.2
    V_0 = 20
    n_0 = 10**17

    num_R_s = 50
    num_V_s = 100
    if V_a < V_0:
        R_s_values = np.linspace(R_p, 3 * R_p, num=num_R_s)
        largest_v_s_value = max(get_equation_limited_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0)[1] for R_s in R_s_values)
        V_s_values = np.linspace(get_equation_limited_V_s_bounds(R_s_values[0], R_p, T_e, T_i, V_a, V_0)[0], largest_v_s_value, num=num_V_s)
    elif V_a > V_0:
        R_s_values = np.linspace(R_p, 3 * R_p, num=num_R_s)
        smallest_v_s_value = min(get_equation_limited_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0)[0] for R_s in R_s_values)
        V_s_values = np.linspace(smallest_v_s_value, get_equation_limited_V_s_bounds(R_s_values[0], R_p, T_e, T_i, V_a, V_0)[1], num=num_V_s)
    elif V_a == V_0:
        R_s_values = np.linspace(R_p, 2 * R_p, num=num_R_s)
        V_s_values = np.linspace(*get_equation_limited_V_s_bounds(R_p, R_p, T_e, T_i, V_a, V_0), num=num_V_s)
    
    inv_dist_to_divergence = np.empty((R_s_values.size, V_s_values.size))

    optimal_V_s_values = np.empty_like(R_s_values)
    for i, R_s in enumerate(tqdm.tqdm(R_s_values)):
        lower_V_s, upper_V_s = get_equation_limited_V_s_bounds(R_s, R_p, T_e, T_i, V_a, V_0)
        optimal_V_s_values[i] = get_optimal_V_s(R_s, R_p, T_e, T_i, V_a, V_0, n_0)[0]
        for j, V_s in enumerate(V_s_values):
            if V_s < lower_V_s or V_s > upper_V_s:
                inv_dist_to_divergence[i, j] = np.nan
                continue

            # Create a list of events that will terminate the integration. We do this in case the voltage goes outside of the (V_a, V_0) bounds.
            events = [
                lambda _, V_vec, *args: V_vec[0] - (V_s - np.sign(V_0 - V_s) * 10**-4), # V_s bound.
                lambda _, V_vec, *args: V_vec[0] - V_0, # V_0 bound.
            ]
            for e in events:
                e.terminal = True

            r_span = (R_s, 2 * R_s)
            V_vec_0 = initial_V_vec(R_s, T_e, T_i, V_a, V_0, V_s)

            result = solve_ivp(ode_function, r_span, V_vec_0, events=events, args=(R_s, R_p, T_e, T_i, V_a, V_0, V_s, n_0))
            if plot_raw:
                plt.plot(result.t, result.y[0], color='blue', alpha=0.3 + 0.7 * j / V_s_values.size)
            
            if result.status == -1:
                print(result.message)

            if result.t_events[0].size != 0:
                inv_dist_to_divergence[i, j] = -np.sign(V_0 - V_s) * (r_span[1] - result.t_events[0][0]) / R_s
            elif result.t_events[1].size != 0:
                inv_dist_to_divergence[i, j] = np.sign(V_0 - V_s) * (r_span[1] - result.t_events[1][0]) / R_s
            else:
                inv_dist_to_divergence[i, j] = 0
        
        if plot_raw:
            plt.xlabel('r')
            plt.ylabel('V')
            plt.axhline(V_0, color='black', linestyle='dashed')
            plt.show()

    inv_dist_masked = np.ma.masked_invalid(inv_dist_to_divergence)
    R_s_grid, V_s_grid = np.meshgrid(R_s_values, V_s_values)

    plt.figure()
    plt.pcolormesh(R_s_grid, V_s_grid, inv_dist_masked.T, vmin=-np.max(abs(inv_dist_masked)), vmax=np.max(abs(inv_dist_masked)), cmap='PiYG')
    # plt.contour(R_s_grid, V_s_grid, inv_dist_masked.T, levels=np.linspace(-np.max(abs(inv_dist_masked)), np.max(abs(inv_dist_masked))))
    plt.plot(R_s_values, optimal_V_s_values, color='black')
    plt.xlabel(r'$R_s$')
    plt.ylabel(r'$V_s$')
    plt.colorbar(label='Inverse distance')
    plt.title(r'$V_a ({}) {} V_0 ({})$'.format(V_a, '<' if V_a < V_0 else '>', V_0))
    plt.show(block=False)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    plt.set_loglevel(level='warning')
    # T_e probe tip diameter = 0.039" = 0.0009906 m
    te_tip_radius = 0.0004953 # m

    # PA tip shell diameter = 0.058" = 0.0014732 m
    pa_shell_radius = 0.0007366 # m
    # PA tip core diameter = 24 AWG = 0.0201" = 0.00051054 m
    pa_core_radius = 0.00025527 # m

    te_filepath = "./analysis/modules/langmuir/te_probe_iv_curves.mat"
    # save_iv_curves(te_tip_radius, '')
    cProfile.run("save_iv_curves(te_tip_radius, '')") # , './notebooks/iv_curve_run_stats')
    exit()

    plot_R_V_dependence = False
    plot_current_density = False

    if plot_R_V_dependence:
        V_a = 15
        V_s_divergence(V_a, plot_raw=False)
        V_p_found_changes(V_a)
        plt.show()

    R_p = 4.953 * 10**-4
    T_e = 0
    T_i = 0.2
    V_a = -5
    V_0 = 20
    n_0 = 10**17

    if plot_current_density:
        V_a = np.linspace(-50, 50, num=40)

        vary_T_e = True
        vary_T_i = False
        vary_n_0 = False

        if vary_T_e:
            T_e_vals = np.array([2, 10, 30])
            T_i_vals = np.array([0.2, 0.2, 0.2])
            n_0_vals = np.array([3, 3, 3]) * 10**17
            curve_label = lambda n_0, T_e, T_i: r'$T_e = {} \mathrm{{eV}}$'.format(T_e)
            save_filepath = './notebooks/codes/IV_curve/saved_plots/varying_T_e.png'
        elif vary_T_i:
            T_e_vals = np.array([10, 10, 10])
            T_i_vals = np.array([0.2, 1, 5])
            n_0_vals = np.array([3, 3, 3]) * 10**17
            curve_label = lambda n_0, T_e, T_i: r'$T_i = {} \mathrm{{eV}}$'.format(T_i)
            save_filepath = './notebooks/codes/IV_curve/saved_plots/varying_T_i.png'
        elif vary_n_0:
            T_e_vals = np.array([10, 10, 10])
            T_i_vals = np.array([0.2, 0.2, 0.2])
            n_0_vals = np.array([1, 3, 8]) * 10**17
            curve_label = lambda n_0, T_e, T_i: r'$n_0 = {:.0e} \mathrm{{m}}^{{-3}}$'.format(n_0)
            save_filepath = './notebooks/codes/IV_curve/saved_plots/varying_n_0.png'

        all_current_densities = np.empty((T_e_vals.size, V_a.size))
        for i, (T_e, T_i, n_0) in enumerate(zip(T_e_vals, T_i_vals, n_0_vals)):
            start = time.time()
            all_current_densities[i] = get_current_densities(R_p, T_e, T_i, V_a, V_0, n_0)
            print("Total time: {}".format(time.time() - start))
        
        plt.figure()
        for i in range(len(T_e_vals)):
            T_e = T_e_vals[i]
            T_i = T_i_vals[i]
            n_0 = n_0_vals[i]
            current_densities = all_current_densities[i]

            plt.plot(V_a, current_densities / 1000, label=curve_label(n_0, T_e, T_i))

            # plt.plot(V_a, current_densities / 1000, label=r'$T_e = {} \mathrm{{eV}}, \, T_i = {} \mathrm{{eV}}, \, n_0 = {} \mathrm{{m}}^{{-3}}$'.format(T_e, T_i, n_0))
            # plt.plot(V_a, current_densities / (1000 * n_0), label=r'$T_e = {} \mathrm{{eV}}, \, T_i = {} \mathrm{{eV}}, \, n_0 = {:.1E} \mathrm{{m}}^{{-3}}$'.format(T_e, T_i, n_0))
        plt.axvline(V_0, label=r'$V_0$', color='black', linestyle='dashed')

        plt.xlabel(r'$V_a \, [\mathrm{V}]$')
        plt.ylabel(r'$J_p \, [\mathrm{kA/m}^2]$')
        plt.legend(loc='lower left')
        plt.tight_layout()
        
        # plt.savefig(save_filepath, dpi=300)
        plt.show()
