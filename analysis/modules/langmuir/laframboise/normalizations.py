"""
This file defines a variety of values for normalization purposes.

For notation purposes we do the following replacements.
    `+` as a subscript is now `i`.
    `-` as a subscrips is now `e`.
"""
import numpy as np

# CONSTANTS
e = 1.60217646e-19 # C. Absolute electron charge.
eps_0 = 8.8541878128e-12 # F * m^−1. Permittivity of free space.
m_p = 1.67262158e-27 # kg. Mass of proton.
m_e = 9.10938188e-31 # kg. Mass of electron.


def debye_length(number_density, temperature, charge=1):
    """
    Parameters
    ----------
    number_density : float
        Number density. [m^-3]
    temperature : float
        Temperature. [eV]
    charge : int, default=1
        Number of electron charges on the species.

    Notes
    -----
    The normalization refers to the ion temperature if `T_ion > 0` otherwise it 
    is the electron temperature.
    """
    return (eps_0 * temperature / (e * charge**2 * number_density))**0.5 # Equation (5).

def collected_current_at_plasma_potential(R_p, N_inf, T, m, charge=1, sphere=False):
    """
    Get the collected current to the probe from a species due to random motion when the probe is at the plasma potential.

    Parameters
    ----------
    R_p : float
        Probe radius. [m]
    N_inf : float
        Particle number density at infinite radius. [m^-3]
    T : float
        Temperature. [eV]
    m : float
        Particle mass. [kg]
    charge : float
        Number of electronic charges on the particle.
    sphere : bool
        Whether to return the collected current for a spherical probe or a cylindrical probe.

    Notes
    -----
    `(I_+)_0` from equation (3.2).
    """
    if sphere:
        return charge * e * N_inf * R_p**2 * (8 * np.pi * e * T / m)**0.5
    else:
        return charge * e * N_inf * R_p * (2 * np.pi * e * T / m)**0.5

def temperature_ratio(ion_temperature, electron_temperature):
    """
    Parameters
    ----------
    ion_temperature, electron_temperature : float
        Temperatures of positively and negatively charged species respectively.

    Notes
    -----
    This is `pi_1` as seen in equation (3.2).
    """
    return ion_temperature / electron_temperature

def mass_ratio(ion_mass, electron_mass):
    """
    Parameters
    ----------
    ion_mass, electron_mass : float
        Mass of positively and negatively charged species respectively.

    Notes
    -----
    This is `pi_2` as seen in equation (3.2).
    """
    return ion_mass / electron_mass

def dimensionless_probe_potential(probe_potential, ion_temperature, ion_charge=1):
    """
    Parameters
    ----------
    probe_potential : float
        Potential of probe relative to plasma. [V]
    ion_temperature : float
        Temperature of the positively charged species. [eV]
    ion_charge : int, default=1
        Number of electronic charges on the positively charged species.

    Notes
    -----
    This is `pi_3` or `chi_p_+` as seen in equation (3.2).
    """
    return ion_charge * probe_potential / ion_temperature

def dimensionless_probe_radius(probe_radius, number_density, ion_temperature, ion_charge=1):
    """
    Parameters
    ----------
    probe_radius : float
        Radius of the probe. [m]
    number_density : float
        Number density. [m^-3]
    ion_temperature : float
        Temperature of the positively charged species. [eV]
    ion_charge : int, default=1
        Number of electron charges on the positively charged species.

    Notes
    -----
    This is `pi_4` or `(gamma_+)**0.5` as seen in equation (3.2).
    """
    return probe_radius / debye_length(number_density, ion_temperature, ion_charge)

def charge_ratio(ion_charge=1, electron_charge=-1):
    """
    Parameters
    ----------
    ion_charge, electron_charge : int, default=1
        Number of electronic charges on the positively and negatively charged 
        species.

    Notes
    -----
    This is `pi_5` as seen in equation (3.2).
    """
    return ion_charge / electron_charge

def inverse_normalized_ion_number(number_density, ion_temperature, ion_charge=1):
    """
    Parameters
    ----------
    number_density : float
        Number density. [m^-3]
    ion_temperature : float
        Temperature of the positively charged species. [eV]
    ion_charge : int, default=1
        Number of electron charges on the positively charged species.

    Notes
    -----
    This is `g_+` as seen in equation (3.2).
    """
    return number_density**-1 * debye_length(number_density, ion_temperature, ion_charge)**-3

