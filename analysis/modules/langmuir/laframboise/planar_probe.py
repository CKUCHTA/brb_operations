"""
This gives the current to the probe if it has large radius, repelled particles are at zero temperature, and attracted particles are Maxwellian.

This follows the equations given in appendix F. This approximation is useful when `lambda_D << R_p`, `V_p > V_s`, and the ions are cold.
"""
import numpy as np
from scipy.special import erf
from scipy.integrate import quad
from modules.langmuir.laframboise.normalizations import *
import logging


def sigma_1(y):
    """
    Parameters
    ----------
    y : np.array[float]

    Returns
    -------
    np.array[float]
        Array of same shape as `y` containing the sigma_1 values.

    Notes
    -----
    Instead of using the approximation of (F.9) we just plug in the exact values 
    as it's faster and more accurate for large y.
    """
    return 2 / np.pi**0.5 * (np.pi**0.5 / 2 * np.exp(y) * (1 - erf(y**0.5)) - (np.pi**0.5 / 2 - y**0.5))

def sigma_2(z_2, eps=10**-14):
    """
    Parameters
    ----------
    z_2 : np.array[float] or float
        `z**2`.
    eps : float, default=10**-7
        Smallest term of sum to keep.

    Returns
    -------
    np.array[float]
        Array of same shape as `z_2` containing the sigma_2 values.

    Notes
    -----
    Equation (F.16).
    """
    z = z_2**0.5

    # First and second term in the (F.16) approximation.
    first_term = 1
    second_term = 2 * z / np.pi**0.5 * 2 / 3
    sig_2 = first_term - second_term

    n = 1
    while np.max(abs(first_term / sig_2)) > eps or np.max(abs(second_term / sig_2)) > eps or np.max((first_term - second_term) / sig_2) > eps:
        n += 1

        first_term *= z_2 / n
        second_term *= z_2 * 2 / (2 * n + 1)

        sig_2 += first_term - second_term

    return sig_2

def s(y, y_1=0, approximation=False):
    """
    Position as a function of potential.
    
    Parameters
    ----------
    y : np.array[float]
    y_1 : float, default=0.5
        Point at which the integral changes from one over `sigma_2` to `sigma_1`.
    approximation : bool, default=False
        Whether to approximate the integral using a power series.

    Notes
    -----
    Equation (F.15).

    We do a series approximation of `log(s(e**x))` around `x = 0`.
    """
    if approximation:
        # # Do a Taylor approximation of the electron current and then do a small correction.
        # taylor_approx = 2 * y**0.5 + 2 / (3 * np.pi**0.5) * y - (-8 + 3 * np.pi) / (18 * np.pi) * y**1.5 - (-200 + 63 * np.pi) / (540 * np.pi**1.5) * y**2
        # const = 0.0013179220205994887
        # correction = 1 + const * y
        # if np.max(y) > 280:
        #     pass
            # logging.warning("Error in approximation for electron current is greater than 10%.")
        # return taylor_approx * correction
        zero_indeces = np.nonzero(y == 0)
        x = np.log(y)
        approx = np.exp(0.855372 + 0.570156 * x + 0.0129142 * x**2 + 0.000977147 * x**3 - 0.0000617862 * x**4 - 0.0000194997 * x**5 - 4.93901*10**-7 * x**6 + 3.43632*10**-7 * x**7)
        # approx = np.exp(0.855372 + 0.570156 * x + 0.0129142 * x**2 + 0.000977147 * x**3 - 0.0000617862 * x**4)
        approx[zero_indeces] = 0
        return approx

    # Sort the y values so that we can integrate from 0 upwards, pausing at `y = y_1`.
    sorted_y_indeces = np.argsort(np.append(y, y_1))

    total = np.zeros_like(y)
    prior_y = 0
    curr_total = 0
    use_sigma_2 = True
    for sorted_index in sorted_y_indeces:
        if sorted_index == y.size:
            # If we are at the `y = y_1` point then we need to do the integral up to `y_1` and start using `sigma_1` from now on.
            curr_total += 2 * quad(lambda z: 1 / sigma_2(z**2)**0.5, prior_y**0.5, y_1**0.5)[0]
            prior_y = y_1
            use_sigma_2 = False
            continue
        elif use_sigma_2:
            curr_total += 2 * quad(lambda z: 1 / sigma_2(z**2)**0.5, prior_y**0.5, y[sorted_index]**0.5)[0]
        else:
            curr_total += quad(lambda yp: 1 / sigma_1(yp)**0.5, prior_y, y[sorted_index])[0]

        prior_y = y[sorted_index]
        total[sorted_index] = curr_total

    return total

def collected_current_normalized(chi_p, lambda_D, R_p, sphere=False, approximation=False):
    """
    The normalized absolute current to the probe from the attracted species.
    
    The current to the probe is normalized to the attracted species current at 
    `chi_p = 0`. The repelled species contributes no current as it has `T=0` 
    and thus no energy to overcome the potential barrier.

    Parameters
    ----------
    chi_p : np.array[float]
        Normalized probe potential.
    lambda_D : float
        Debye length. [m]
    R_p : float
        Probe radius. [m]
    sphere : bool, default=False
        If the probe is spherical. If `False`, probe is cylindrical.
    approximation : bool, default=False
        Whether to approximate the current using a faster method.
    """
    if sphere:
        return (1 + lambda_D / R_p * s(chi_p, approximation=approximation))**2
    else:
        return 1 + lambda_D / R_p * s(chi_p, approximation=approximation)
    
def collected_current(R_p, L, V_p, V_s, N_inf, T, m=m_e, Z=1, sphere=False, approximation=False):
    """
    The absolute current to the probe from the attracted species.
    
    To dimensionalize the normalized current we multiply by the current from 
    the attracted species assuming `V_p = V_s`.

    Parameters
    ----------
    R_p : float
        Probe radius. [m]
    L : np.array[float] or float
        Probe tip lengths. If an array, should be 1D and of shape `(num_points,)`. [m]
    V_p : np.array[float]
        1D array of shape `(num_points,)` containing the probe potential. [V]
    V_s : float
        The plasma potential. [V]
    N_inf : float
        Particle number density at infinite radius. [m^-3]
    T : float
        Temperature. [eV]
    m : float, default=m_e
        Particle mass. [kg]
    Z : float
        Number of electronic charges on the particle.
    sphere : bool
        Whether to return the collected current for a spherical probe or a cylindrical probe.
    approximation : bool, default=False
        Whether to approximate the current using a faster method.
    """
    coeff = collected_current_at_plasma_potential(R_p, N_inf, T, m, Z, sphere)
    chi_p = Z * (V_p - V_s) / T
    current_per_length = coeff * collected_current_normalized(chi_p, debye_length(N_inf, T), R_p, sphere, approximation=approximation)

    # We change the length to account for the end cap.
    L_corrected = L + R_p / 2
    return L_corrected * current_per_length

if __name__ == "__main__":
    import matplotlib.pyplot as plt

    R_p = 4.953 * 10**-4
    L = 2 * 10**-3
    V_s = 10
    V_p_vals = V_s + np.linspace(0, 30, num=10000)
    N_inf = 10**17
    for T in [3, 10, 30]:
        I = collected_current(R_p, L, V_p_vals, V_s, N_inf, T)

        c = plt.plot(V_p_vals, I, label=int(R_p / debye_length(N_inf, T)))[0].get_color()
        I_approx = collected_current(R_p, L, V_p_vals, V_s, N_inf, T, approximation=True)

        plt.plot(V_p_vals, I_approx, label=int(R_p / debye_length(N_inf, T)), linestyle='dashed', color=c)
    plt.legend(title=r'$R_p / \lambda_D$')
    plt.xlabel(r"$V$")
    plt.ylabel(r"$I$")
    plt.title(r"$N_\inf = 10^{17}$")
    plt.show()
