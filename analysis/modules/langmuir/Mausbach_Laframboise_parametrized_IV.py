"""
This method parametrizes Laframboise's IV theory for cylindrical probes using Mausbach's parametrization.

Notes
-----
Theory validity:
    - Collisionless sheath (i.e. mean free path >> debye length).
    - Probe radius << probe length.
    - Quasineutral plasma consisting of electrons and singly-ionized ions.
    - Bulk plasma flow only parallel to probe axis (no change to theory).
    - Both species annihilated when touching the probe.
    - No magnetic fields.

This parametrization has some major issues as you don't get smooth transitions 
across V_a = V_p and some of the values don't match with Mausbach's. Use with CAUTION!

This follows M. Mausbach, “Parametrization of the Laframboise theory for cylindrical Langmuir probe analysis,” Journal of Vacuum Science & Technology A: Vacuum, Surfaces, and Films, vol. 15, no. 6, pp. 2923-2929, Nov. 1997, doi: 10.1116/1.580886.
"""
import numpy as np
from scipy.interpolate import LinearNDInterpolator


def current_coefficients(n_e, n_i, T_e, T_i, A):
    """
    Get the coefficients multiplying f(xi).

    Parameters
    ----------
    n_e, n_i : float
        Electron and ion number density in units of [m^-3].
    T_e, T_i : float
        Electron and ion temperature in units of [eV].
    A : float
        Probe area in units of [m^2].

    Returns
    -------
    I_e0, I_i0 : float
        Coefficients to multiply the normalized saturation functions by.

    Notes
    -----
    This gives the coefficients used in equations (5) and (6) by using equations 
    (7), (8a), and (8b).
    """
    e = 1.60217646e-19 # C
    m_e = 9.10938188e-31 # kg
    m_i = 1.67262158e-27 # kg. Proton mass.

    I_e0 = -e * n_e * A * (T_e * e / (2 * np.pi * m_e))**0.5
    I_i0 = e * n_i * A * ((T_e if T_i == 0 else T_i) * e / (2 * np.pi * m_i))**0.5

    return I_e0, I_i0

def debye_length(T_e, n_e):
    e = 1.60217646e-19 # C
    eps_0 = 8.8541878128e-12 # F * m^−1. Permittivity of free space.

    return (eps_0 * T_e / (e * n_e))**0.5 # Equation (1). We've used the electron density instead of the plasma density.

def func_parameters(r, n_e, T_e, T_i):
    """
    Get the parameters to put into the normalized saturation function.

    Parameters
    ----------
    r : float
        Probe radius in units of [m].
    n_e : float
        Electron number density in units of [m^-3].
    T_e, T_i : float
        Electron and ion temperature in units of [eV].
    
    Returns
    -------
    electron_parameters, ion_parameters : np.array[float]
        3 element arrays containing `[a, b, c]` for the electron and ion saturation functions.

    Notes
    -----
    This takes the data from Table I and interpolates it to the given parameters.
    """
    # TODO: Include a way to estimate the error between the parametrized function and the Laframboise solution.

    # Interpolate the parameters from table 1.
    electron_interpolator = LinearNDInterpolator(
        # Points at which we have parameters ordered as `(T_i / T_e, r / debye_length)`.
        np.array([
            [0, 0], [0, 1], [0, 2], [0, 3], [0, 5], [0, 10], [0, 20], # Points for `T_i / T_e == 0`.
            [1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 10], [1, 20], [1, 50], [1, 100], # Points for `T_i / T_e == 1`.
        ]),
        # Values of `(a_e, b_e, c_e)` at each point.
        np.array([
            [1.2, 0.7, 0.48], [1.16, 0.72, 0.45], [1.02, 1.02, 0.45], [0.97, 1.16, 0.42], [0.9, 1.45, 0.38], [0.85, 1.88, 0.29], [0.83, 2.6, 0.21],
            [1.2, 0.7, 0.48], [1.2, 0.7, 0.48], [1.23, 0.62, 0.46], [1.25, 0.61, 0.44], [1.3, 0.55, 0.41], [1.36, 0.42, 0.37], [1.43, 0.24, 0.26], [1.34, 0.33, 0.2], [1.46, 0, 0.09], [1.42, 0, 0.06],
        ])
    )
    ion_interpolator = LinearNDInterpolator(
        np.array([
            [0, 0], [0, 2], [0, 3], [0, 4], [0, 5], [0, 10], [0, 20], [0, 50], [0, 100], # Points for `T_i / T_e == 0`.
            [1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 10], [1, 20], [1, 50], [1, 100], # Points for `T_i / T_e == 1`.
        ]),
        np.array([
            # [1.13, 0, 0.5], [1.13, 0, 0.5], [1.22, -0.11, 0.45], [1.22, -0.1, 0.4], [1.22, -0.16, 0.36], [1.18, -0.31, 0.25], [1.19, -0.30, 0.20], [1.06, -0.48, 0.1], [1.05, -0.85, 0.06],
            # [1.13, 0, 0.5], [1.13, 0, 0.5], [1.22, 0.11, 0.45], [1.22, 0.1, 0.4], [1.22, 0.16, 0.36], [1.18, 0.31, 0.25], [1.19, 0.30, 0.20], [1.06, 0.48, 0.1], [1.05, 0.85, 0.06],
            [1.13, 0, 0.5], [1.13, 0, 0.5], [1.22, 0, 0.45], [1.22, 0, 0.4], [1.22, 0, 0.36], [1.18, 0, 0.25], [1.19, 0, 0.20], [1.06, 0, 0.1], [1.05, 0, 0.06],
            [1.2, 0.7, 0.48], [1.2, 0.7, 0.48], [1.23, 0.62, 0.46], [1.25, 0.61, 0.44], [1.3, 0.55, 0.41], [1.36, 0.42, 0.37], [1.43, 0.24, 0.26], [1.34, 0.33, 0.2], [1.46, 0, 0.09], [1.42, 0, 0.06],
        ])
    )

    electron_parameters = electron_interpolator(T_i / T_e, r / debye_length(T_e, n_e))
    ion_parameters = ion_interpolator(T_i / T_e, r / debye_length(T_e, n_e))

    return electron_parameters, ion_parameters

def probe_characteristic(V_a, V_p, r, n_e, n_i, T_e, T_i, A):
    """
    Get the current to the probe given the plasma parameters.

    Parameters
    ----------
    V_a : np.array[float]
        Array of probe potentials in units of [V].
    V_p : float
        Plasma potential in units of [V].
    r : float
        Probe radius in units of [m].
    n_e, n_i : float
        Electron and ion number density in units of [m^-3].
    T_e, T_i : float
        Electron and ion temperature in units of [eV].
    A : float
        Probe area in units of [m^2].

    Returns
    -------
    I : np.array[float]
        Current to the probe.
    """
    xi = (V_a - V_p) / T_e # Equation (2).
    e_params, i_params = func_parameters(r, n_e, T_e, T_i)
    I_e0, I_i0 = current_coefficients(n_e, n_i, T_e, T_i, A)
    # f = lambda xi_vals, params: params[0] * (params[1] + xi_vals)**params[2] # Equation (9)
    f = lambda xi_vals, params: np.where(xi_vals + params[1] >= 0, params[0] * (params[1] + xi_vals)**params[2], 0)

    return np.where(xi < 0, I_i0 * f(-xi, i_params) + I_e0 * np.exp(xi), I_e0 * f(xi, e_params))


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    # T_e = 50
    # T_i = 0
    # n_e = 10**17
    # V_a_vals = np.linspace(0, 25, num=1000) * T_e
    # r_vals = debye_length(T_e, n_e) * np.array([0, 3, 4, 5, 10, 20, 50, 100])

    # fig, axs = plt.subplots(2, sharex=True, sharey=True)
    # axs[0].set_title(r'electrons, $ T_i / T_e = 0$')
    # axs[1].set_title(r'ions, $ T_i / T_e = 0$')
    # for i in range(1, 6):
    #     axs[0].axhline(i, color='grey', alpha=0.2)
    #     axs[1].axhline(i, color='grey', alpha=0.2)

    # for i, r in enumerate(r_vals):
    #     e_params, i_params = func_parameters(r, n_e, T_e, T_i)

    #     xi_vals = V_a_vals / T_e

    #     f = lambda xi, params: np.where(xi >= -params[1], params[0] * (params[1] + xi)**params[2], 0)

    #     axs[0].plot(xi_vals, f(xi_vals, e_params), label=r"$r / \lambda_D = {}$".format(int(r / debye_length(T_e, n_e))))
    #     axs[1].plot(xi_vals, f(xi_vals, i_params), label=r"$r / \lambda_D = {}$".format(int(r / debye_length(T_e, n_e))))
        
    # axs[0].legend()
    # axs[1].legend()
    # axs[0].set_xlim(0, 25)
    # axs[0].set_ylim(0, 6)
    # plt.show()
    # exit()

    T_i = 0
    T_e_vals = np.linspace(1, 10, num=10)
    V_p = 0
    V_a = np.linspace(-10, 10, num=3001)
    r = 0.001
    A = 0.01**2
    n_e = 10**17
    n_i = 10**17

    plt.axhline(0, color='grey', alpha=0.2)
    for T_e in T_e_vals:
        plt.plot(V_a, probe_characteristic(V_a, V_p, r, n_e, n_i, T_e, T_i, A), label=r'$T_e = {}$ eV, $ r / \lambda_D = {:.1f}$'.format(int(T_e), r / debye_length(T_e, n_e)))
    plt.legend()
    plt.show()

