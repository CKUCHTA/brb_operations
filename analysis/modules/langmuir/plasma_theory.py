import numpy as np
from astropy import units as u
from astropy import constants as c
import plasmapy


def quantity_return(func):
    def wrapped_func(self, *args, **kwargs):
        if self.return_quantities:
            return func(self, *args, **kwargs).cgs
        else:
            return func(self, *args, **kwargs).cgs.value
    return wrapped_func


class PlasmaDistribution:
    def __init__(self, return_quantities, proton_number) -> None:
        self.m_e = 1 * u.me
        self.m_i = proton_number * u.mp
        self.return_quantities = return_quantities


class Maxwellian(PlasmaDistribution):
    def __init__(self, electron_temperature, plasma_density, plasma_potential, return_quantities=False, proton_number=1) -> None:
        self.T_e = electron_temperature * u.eV
        self.n_e = plasma_density * u.cm**-3
        self.V_p = plasma_potential * u.V
        super().__init__(return_quantities, proton_number)

    def distribution_function(self):
        """
        Get the distribution function as a function of velocity magnitude.

        Returns
        -------
        func(float) -> float
            Returned in cgs units.
        """
        # TODO: Figure out what this should be and the units.
        # return lambda v_mag: self.n_e * (self.m_i / (2 * np.pi * self.T_e))**3/2 * np.exp(-self.m_i * v_mag**2 * )
        pass

    @quantity_return
    def plasma_potential(self):
        return self.Vp

    @quantity_return
    def debye_length(self):
        return (c.eps0 * self.T_e / (c.e**2 * self.n_e))**0.5

    @quantity_return
    def ion_saturation_current(self, probe_area):
        """
        Get the theoretical current when the probe is very negatively biased.
        
        Parameters
        ----------
        probe_area : float
            Probe area in units of m**2.
        """
        probe_area *= u.m**2
        return np.exp(-0.5) * probe_area * self.n_e * (c.e * self.T_e / self.m_i)**0.5

    @quantity_return
    def expected_current(self, probe_area, probe_potential):
        """
        Get the theoretical current for any bias as long as we are not in the electron saturation regime.
        
        Parameters
        ----------
        probe_area : float
            Probe area in units of m**2.
        probe_potential : float
            Probe potential in units of V.
        """
        probe_area *= u.m**2
        probe_potential *= u.V
        return self.n_e * c.e * probe_area * (c.e * self.T_e / self.m_i)**0.5 * (
            0.5 * (2 * self.m_i / (np.pi * self.m_e))**0.5 * np.exp((probe_potential - self.Vp) / self.T_e) - np.exp(-0.5)
        )
