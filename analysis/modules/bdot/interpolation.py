import numpy as np
from scipy.interpolate import interp1d
from modules.coordinates import Position, Vector
import logging


def get_db_machine_coords(db_values : list[np.array], positions : list[np.array], orientations : list[np.array], interpolation_positions=None, interpolation_kind='linear'):
    """
    1D interpolation of measured dB / dt to get machine oriented dB / dt vectors.

    Parameters
    ----------
    db_values : list[np.array]
        List of length 3 containing arrays of measured dB / dt where each array 
        is for a different coil set and each array is of shape `(NUM_COILS, NUM_TIMES)`.
    positions : list[np.array]
        List of length 3 containing arrays of positions of each coil set in machine 
        coordinates, `(r, phi, z)`. Each array is for a different coil set. The 
        arrays are of shape `(NUM_COILS, 3)`.
    orientations : list[np.array]
        List of length 3 containing arrays of orientations of each coil in 
        machine coordinates, `(r, phi, z)`. Each array is for a different coil 
        set. The orientation is defined as the unit normal vector of the coil. 
        The arrays are of shape `(NUM_COILS, 3)`.
    interpolation_positions : np.array[float] or None
        If `None` then the returned dB / dt values will be those found at the
        `positions`. If an `np.array`, of shape `(NUM_POINTS, 3)`, then return
        the dB / dt values at the given cylindrical positions. See notes.
    interpolation_kind : str, default='linear'
        How we interpolate the magnitudes between coils.

    Returns
    -------
    interpolation_positions : np.array[float]
        2D-array of cylindrical positions,`(r, phi, z)`, where `phi` is in 
        radians. Shape of `(NUM_POSITIONS, 3)`. If the `interpolation_positions`
        were passed then this is the positions sorted in the direction of the
        probe.
    interpolation_db_values : np.array[float]
        3D-array of interpolated dB/dt components in cylindrical coordinates, 
        `(dB_r / dt, dB_phi / dt, dB_z / dt)`. Shape of `(NUM_POSITIONS, 3, NUM_TIMES)`.
    
    Notes
    -----
    This operates by taking three independent vectors and finding the magnitude 
    in the machine directions. Each coil is at a specific location and to get 3 
    independent vectors we must use a linearly independent set of orientations. 
    We interpolate between coils in each unique direction by interpolating in 
    angle and magnitude.

    Sets of coils should all be in a similar direction such as the 
    `speed_bdot1` probe where there are three coil types (1, 2, and 3). Also,
    there should be exactly 3 sets of coils as that is what is needed to 
    uniquely find dB / dt in machine cylindrical coordinates.

    `NUM_COILS` is the number of coils of that type and may be different for
    each coil type. `NUM_TIMES` is the number of time points to use. This
    should be the same for all coils.

    When interpolating at different positions we use the 3 'nearest' coils each
    from a different coil set. We choose 'nearest' by calculating distance
    along the direction of the probe (which should be 1D).
    """
    logging.debug("Interpolating db values.")

    if len(db_values) != 3 or len(positions) != 3 or len(orientations) != 3:
        raise ValueError('The length of `db_values`, `positions`, and `orientation` must all be 3.')
    
    NUM_COILS = []
    NUM_TIMES = 0
    for i, (db_value_arr, position_arr, orientation_arr) in enumerate(zip(db_values, positions, orientations)):
        if i == 0:
            NUM_TIMES = db_value_arr.shape[1]

        NUM_COILS.append(db_value_arr.shape[0])

        if db_value_arr.shape[1] != NUM_TIMES:
            raise ValueError("All `db_value` arrays must have the same number of time points.")
        
        if position_arr.shape != (NUM_COILS[-1], 3):
            raise ValueError("All `position` arrays must be of shape `(NUM_COILS, 3)`.")
        if orientation_arr.shape != (NUM_COILS[-1], 3):
            raise ValueError("All `orientation` arrays must be of shape `(NUM_COILS, 3)`.")
        
    # It's best to do everything in cartesian coordinates for calculations and
    # then convert back to cylindrical coordinates at the end.
    positions_cart = []
    orientations_cart = []
    orientations_cyl = []
    for position, orientation in zip(positions, orientations):
        position_obj = Position()
        position_obj.cylindrical = position.T

        orientation_obj = Vector(position_obj)
        orientation_obj.cylindrical = orientation.T

        positions_cart.append(position_obj.cartesian.T)
        orientations_cart.append(orientation_obj.cartesian.T)
        orientations_cyl.append(orientation_obj.cylindrical.T)

    # For each set of coils we want them all oriented in approximately the same
    # direction so we'll use the first coil as the preferred orientation and
    # make all other coils in the set be pointed in that direction.
    for dir_values, dir_orientations_cart, dir_orientations_cyl in zip(db_values, orientations_cart, orientations_cyl):
        preferred_orientation = dir_orientations_cart[0]

        cos_angles = np.dot(dir_orientations_cart, preferred_orientation)
        # Flip the orientation and value if the angle is greater than 90 degrees.
        flip_indeces = np.nonzero(cos_angles < 0)

        dir_orientations_cart[flip_indeces] *= -1
        dir_orientations_cyl[flip_indeces] *= -1
        dir_values[flip_indeces] *= -1

    # Get the probe direction by doing an SVD of the mean-centered data.
    _, _, vectors = np.linalg.svd(np.concatenate(positions_cart, axis=0) - np.mean(np.concatenate(positions_cart, axis=0), axis=0)[None, :])
    probe_direction = vectors[0]

    # Now sort the coils along the probe direction.
    for i in range(len(db_values)):
        sorted_indeces = np.argsort(np.dot(positions_cart[i], probe_direction))

        db_values[i] = db_values[i][sorted_indeces]
        positions_cart[i] = positions_cart[i][sorted_indeces]
        positions[i] = positions[i][sorted_indeces]
        orientations_cart[i] = orientations_cart[i][sorted_indeces]
        orientations_cyl[i] = orientations_cyl[i][sorted_indeces]

    # The index points to the first coil in the set with a positive 
    # `dot(coil_position - interp_position, probe_direction)`. The index always 
    # increases as the interp positions are sorted in increasing distance along 
    # the probe_direction.
    indeces = [0, 0, 0]

    if interpolation_positions is None:
        interpolation_positions_cart = np.concatenate(positions_cart, axis=0)
        interpolation_positions = np.concatenate(positions)
    else:
        # Convert the interpolation positions to cartesian.
        interp_positions_obj = Position()
        interp_positions_obj.cylindrical = interpolation_positions.T
        interpolation_positions_cart = interp_positions_obj.cartesian.T

    # Now sort all the positions along the probe direction.
    sorted_interp_positions_indeces = np.argsort(np.dot(interpolation_positions_cart, probe_direction))
    interpolation_positions_cart = interpolation_positions_cart[sorted_interp_positions_indeces]
    interpolation_positions = interpolation_positions[sorted_interp_positions_indeces]

    if interpolation_kind == 'linear':
        # Use the fast interpolation if we are using linear interpolation.
        db_values = fast_db_machine_coords(db_values, positions_cart, [orientations[:, :, np.newaxis] for orientations in orientations_cart], interpolation_positions_cart)
        return  interpolation_positions, db_values[:, :, :, 0]

    interpolation_db_values = np.zeros((*interpolation_positions_cart.shape, NUM_TIMES))
    db_interpolaters = [
        interp1d(np.dot(positions_cart[i], probe_direction), db_values[i], kind=interpolation_kind, axis=0, fill_value=(db_values[i][0], db_values[i][-1]), bounds_error=False) for i in range(len(db_values))
    ]
    coil_positions_along_probe = [np.dot(positions_cart[i], probe_direction) for i in range(3)]
    for interp_index, interp_position_cart in enumerate(interpolation_positions_cart):
        # We need to solve an inverse problem of A x = b where A are the coil 
        # orientations, x are the db magnitudes in the machine directions, and
        # b are the measured magnitudes from each coil. We need to do this for
        # each time point but the A matrix is the same for all times.
        A_matrix = np.zeros((3, 3))
        b_vector = np.zeros((3, NUM_TIMES))

        interp_position_along_probe = np.dot(interp_position_cart, probe_direction)
        for i in range(len(indeces)):
            b_vector[i] = db_interpolaters[i](interp_position_along_probe)

            index = indeces[i] + np.argmax(np.dot(positions_cart[i][indeces[i]:], probe_direction) - interp_position_along_probe >= 0)
            # Get the distance along the probe between the indexed coil and the interp position.
            relative_position = np.dot(positions_cart[i][index], probe_direction) - interp_position_along_probe

            # If the position we are interpolating isn't between two probe 
            # positions then just use the orientation of the first/last probe.
            if interp_position_along_probe < coil_positions_along_probe[i][0]:
                A_matrix[i] = orientations_cyl[i][0]
            elif interp_position_along_probe > coil_positions_along_probe[i][-1]:
                A_matrix[i] = orientations_cyl[i][-1]
                indeces[i] = NUM_COILS[i] - 1
            # If the position is nearly on top of a coil then just use that
            # coils orientation.
            elif np.isclose(relative_position, 0):
                A_matrix[i] = orientations_cyl[i][index]
            # If the cartesian orientations of the two coils on either side are 
            # nearly the same then use the cylindrical orientation at the 
            # interpolation position projected onto the probe.
            elif np.allclose(orientations_cart[i][index], orientations_cart[i][index - 1]):
                # The ratio is 1 if the interp_position is at coil[index] and 0 if at coil[index - 1]
                ratio_between_coils = (interp_position_along_probe - coil_positions_along_probe[i][index - 1]) / \
                    (coil_positions_along_probe[i][index] - coil_positions_along_probe[i][index - 1])
                interp_position_on_probe_cart = ratio_between_coils * positions_cart[i][index] + (1 - ratio_between_coils) * positions_cart[i][index - 1]
                new_orientation_obj = Vector(Position(interp_position_on_probe_cart), orientations_cart[i][index])
                A_matrix[i] = new_orientation_obj.cylindrical

            # Since the orientations were different we need to
            # interpolate the orientation. We do this by doing
            # a linear interpolation along a great circle thru
            # the two orientations. We'll use the method from:
            # https://math.stackexchange.com/questions/1783746/equation-of-a-great-circle-passing-through-two-points
            else:
                orientation_1 = orientations_cart[i][index - 1]
                orientation_2 = orientations_cart[i][index]
                # We want to interpolate from 1 -> 2.
                d = np.dot(orientation_1, orientation_2)
                beta = (1 / ((1 + d) * np.dot(orientation_1, orientation_1 - orientation_2)))**0.5
                alpha = -d * beta

                # Take the value `t` from the site as a percent of
                # the angle between the orientations.
                ratio_between_coils = (interp_position_along_probe - coil_positions_along_probe[i][index - 1]) / \
                    (coil_positions_along_probe[i][index] - coil_positions_along_probe[i][index - 1])
                interpolation_angle = np.arccos(d) * ratio_between_coils

                new_orientation_cart = np.cos(interpolation_angle) * orientation_1 + np.sin(interpolation_angle) * (alpha * orientation_1 + beta * orientation_2)
                new_orientation_cart /= np.linalg.norm(new_orientation_cart)
                
                # When finding the new orientation, if the 2 orientations 
                # are very close we can get somewhat large numerical errors 
                # so we only want to check against very large differences.
                if not np.isclose(np.arccos(np.dot(orientation_1, orientation_2)), np.arccos(np.dot(new_orientation_cart, orientation_1)) + np.arccos(np.dot(new_orientation_cart, orientation_2)), rtol=10**-4, atol=10**-2):
                    raise Exception("The interpolated coil orientation is incorrect for direction {}. The total angle from coil (1 -> 2) is {} and the angle from (1 -> new orientation) + (new -> 2) is {}. They should be the same.".format(
                        i + 1, np.arccos(np.dot(orientation_1, orientation_2)), np.arccos(np.dot(new_orientation_cart, orientation_1)) + np.arccos(np.dot(new_orientation_cart, orientation_2))
                    ))
                
                # Now convert the cartesian orientation to a cylindrical one.
                interp_position_on_probe_cart = ratio_between_coils * positions_cart[i][index] + (1 - ratio_between_coils) * positions_cart[i][index - 1]
                new_orientation_obj = Vector(Position(interp_position_on_probe_cart), new_orientation_cart)
                A_matrix[i] = new_orientation_obj.cylindrical
                
        # Now we can solve for the x_vector which is the combonents of dB/dt in `(r, phi, z)` coordinates.
        x_vector = np.dot(np.linalg.inv(A_matrix), b_vector)
        interpolation_db_values[interp_index] = x_vector

    return interpolation_positions, interpolation_db_values

def fast_db_machine_coords(db_values : list[np.array], positions : list[np.array], orientations : list[np.array], interpolation_positions : np.array):
    """
    A simplified and vectorized `get_db_machine_coords` with restrictions on parameters.

    Parameters
    ----------
    db_values : list[np.array]
        List of length 3 containing arrays of measured dB / dt where each array 
        is for a different coil set and each array is of shape `(NUM_WORKING_COILS, NUM_TIMES)`.
    positions : list[np.array[float]]
        List of 2D-arrays of coil positions in 
        cartesian coordinates of the ball, `(x, y, z)`. Each 
        array should have shape `(NUM_WORKING_COILS, 3)`.
    orientations : list[np.array[float]]
        List of 3D-arrays of coil positions and orientations in 
        cartesian coordinates of the ball, `(x, y, z)`. Each 
        array should have shape `(NUM_WORKING_COILS, 3, S)`.
    interpolation_positions : np.array[float]
        2D-array of shape `(NUM_POSITIONS, 3)` to interpolate the db field at in cartesian coordinates.

    Returns
    -------
    interpolation_db_values : np.array[float]
        4D-array of interpolated dB/dt components in cylindrical coordinates, 
        `(dB_r / dt, dB_phi / dt, dB_z / dt)`. Shape of `(NUM_POSITIONS, 3, NUM_TIMES, S)`.

    Notes
    -----
    It is recommended to use `get_db_machine_coords` unless you need a very fast 
    method. See `get_db_machine_coords` for a description of the method. We use 
    linear interpolation.

    We assume that all the coil attributes have already been sorted in increasing 
    order along the probe direction.
    """
    # Remake the db_values to be a list of 3D-arrays of shape `(NUM_COILS, NUM_TIMES, S)`.
    new_db_values = [np.repeat(db_vals[:, :, np.newaxis], orientations[0].shape[2], axis=-1) for db_vals in db_values]
    new_orientations = [orientation.copy() for orientation in orientations]

    # For each set of coils we want them all oriented in approximately the same
    # direction so we'll use the first coil as the preferred orientation and
    # make all other coils in the set be pointed in that direction.
    for dir_values, dir_orientations in zip(new_db_values, new_orientations):
        # TODO: Make sure this is actually flipping values in the lists.
        preferred_orientation = dir_orientations[0]

        cos_angles = np.einsum(dir_orientations, [0, 1, 2], preferred_orientation, [1, 2], [0, 2])
        
        # Flip the orientation and value if the angle is greater than 90 degrees.
        flip_indeces = np.nonzero(cos_angles < 0)

        dir_orientations[flip_indeces[0], :, flip_indeces[1]] *= -1
        dir_values[flip_indeces[0], :, flip_indeces[1]] *= -1

    # Get the approximate probe position by using the coils of coil set 0 and comparing the first and last.
    probe_direction = positions[0][-1] - positions[0][0]
    probe_direction /= np.linalg.norm(probe_direction)

    all_coil_positions_along_probe = [np.dot(pos, probe_direction) for pos in positions] # List of 1D-arrays of shape `(NUM_WORKING_COILS,)`
    interpolation_positions_along_probe = np.dot(interpolation_positions, probe_direction) # 1D-array of shape `(NUM_POSITIONS,)`
    
    # List of 1D-arrays of shape `(NUM_POSITIONS,)` where the value is the index 
    # of the coil after the position. The value is -1 if there is no coil after.
    interpolation_coil_indeces = []
    for coil_positions in all_coil_positions_along_probe:
        curr_coil_index = 0
        curr_pos_index = 0
        
        coil_indeces = np.zeros_like(interpolation_positions_along_probe, dtype=int)
        while curr_coil_index < coil_positions.size and curr_pos_index < coil_indeces.size:
            if coil_positions[curr_coil_index] < interpolation_positions_along_probe[curr_pos_index]:
                curr_coil_index += 1
            else:
                coil_indeces[curr_pos_index] = curr_coil_index
                curr_pos_index += 1
        
        # If we have gotten to the last coil index but there are still more 
        # interpolation positions then fill the remaining spots with -1.
        if curr_pos_index < coil_indeces.size:
            coil_indeces[curr_pos_index:] = -1

        interpolation_coil_indeces.append(coil_indeces)

    # This is a list of 3D-arrays of shape `(NUM_POSITIONS, NUM_TIMES, S)` of the coil measured db values
    # interpolated to the interpolation positions.
    interp_coil_db_values = [
        np.where(
            coil_indeces[:, np.newaxis, np.newaxis] == 0, db_vals[np.newaxis, 0], 
            np.where(
                coil_indeces[:, np.newaxis, np.newaxis] == -1, db_vals[np.newaxis, -1],
                (((interpolation_positions_along_probe - coil_positions_along_probe[coil_indeces - 1]) / 
                    (coil_positions_along_probe[coil_indeces] - coil_positions_along_probe[coil_indeces - 1]))[:, np.newaxis, np.newaxis] * db_vals[coil_indeces] +
                ((coil_positions_along_probe[coil_indeces] - interpolation_positions_along_probe) / 
                    (coil_positions_along_probe[coil_indeces] - coil_positions_along_probe[coil_indeces - 1]))[:, np.newaxis, np.newaxis] * db_vals[coil_indeces - 1])
            )
        ) for coil_indeces, db_vals, coil_positions_along_probe in zip(interpolation_coil_indeces, new_db_values, all_coil_positions_along_probe)
    ]

    # This is a list of 3D-arrays of shape `(NUM_POSITIONS, 3, S)`. Interpolated orientations are in cylindrical coordinates.
    interp_coil_orientations = [
        np.where(
            coil_indeces[:, np.newaxis, np.newaxis] == 0, 
            Vector.cartesian_to_cylindrical(coil_orientations[0], coil_positions[0])[np.newaxis],
            np.where(
                coil_indeces[:, np.newaxis, np.newaxis] == -1, 
                Vector.cartesian_to_cylindrical(coil_orientations[-1], coil_positions[-1])[np.newaxis],
                _interpolate_orientations_vectorized(
                    coil_orientations[coil_indeces - 1], coil_orientations[coil_indeces],
                    coil_positions_along_probe[coil_indeces - 1], coil_positions_along_probe[coil_indeces],
                    coil_positions[coil_indeces - 1], coil_positions[coil_indeces],
                    interpolation_positions_along_probe
                )
            )
        ) for coil_indeces, coil_orientations, coil_positions_along_probe, coil_positions in zip(interpolation_coil_indeces, new_orientations, all_coil_positions_along_probe, positions)
    ]

    # Construct the `A_matrix` of shape `(NUM_POSITIONS, S, 3, 3)`.
    A_matrix = np.transpose(np.stack([np.transpose(orientation, [0, 2, 1]) for orientation in interp_coil_orientations], axis=-1), axes=(0, 1, 3, 2))

    # Construct the `b_vector` of shape `(NUM_POSITIONS, NUM_TIMES, S, 3)`
    b_vector = np.stack(interp_coil_db_values, axis=-1)

    # Calculate the new db values.
    interpolation_db_values = np.einsum(np.linalg.inv(A_matrix), [0, 1, 2, 3], b_vector, [0, 4, 1, 3], [0, 2, 4, 1])

    return interpolation_db_values


def _interpolate_orientations_vectorized(orientation_1, orientation_2, position_1, position_2, position_1_cart, position_2_cart, interp_position):
    """
    Parameters
    ----------
    orientation_1, orientation_2 : np.array[float]
        3D-arrays of shape `(NUM_POSITIONS, 3, S)` of the coil orientations in cartesian.
    position_1, position_2 : np.array[float]
        1D-arrays of shape `(NUM_POSITIONS,)` of coil positions along probe.
    position_1_cart, position_2_cart : np.array[float]
        2D-arrays of shape `(NUM_POSITIONS, 3)` of coil cartesian positions.
    interp_position : np.array[float]
        1D-array of shape `(NUM_POSITIONS,)` of interpolation positions along probe.

    Returns
    -------
    interp_orientation : np.array[float]
        3D-array of shape `(NUM_POSITIONS, 3, S)` of the interpolated coil orientations in cylindrical.
    """
    d = np.einsum(orientation_1, [0, 1, 2], orientation_2, [0, 1, 2], [0, 2])
    beta = (1 / ((1 + d) * np.einsum(orientation_1, [0, 1, 2], orientation_1 - orientation_2, [0, 1, 2], [0, 2])))**0.5
    alpha = -d * beta

    ratio_between_coils = (interp_position - position_1) / (position_2 - position_1)
    interp_angle = ratio_between_coils[:, np.newaxis] * np.arccos(d)
    # interp_angle = np.where(np.logical_and(d >= -1, d <= 1), ratio_between_coils[:, np.newaxis] * np.arccos(d), np.nan)

    # If the two orientations are very close then just use one of them. Otherwise interpolate.
    orientations_close = np.logical_or(np.all(np.isclose(orientation_1, orientation_2, atol=10**-4), axis=1), np.isnan(interp_angle))
    interp_orientation = np.where(
        orientations_close[:, np.newaxis, :],
        orientation_1,
        np.cos(interp_angle)[:, np.newaxis, :] * orientation_1 + np.sin(interp_angle)[:, np.newaxis, :] * (alpha[:, np.newaxis, :] * orientation_1 + beta[:, np.newaxis, :] * orientation_2)
    )
    interp_orientation /= np.linalg.norm(interp_orientation, axis=1)[:, np.newaxis]

    interp_position_on_probe_cart = (1 - ratio_between_coils)[:, np.newaxis] * position_1_cart + ratio_between_coils[:, np.newaxis] * position_2_cart
    interp_orientation_cyl = np.transpose(Vector.cartesian_to_cylindrical(np.transpose(interp_orientation, axes=(1, 0, 2)), np.transpose(interp_position_on_probe_cart[:, :, np.newaxis], axes=(1, 0, 2))), axes=(1, 0, 2))

    return interp_orientation_cyl
