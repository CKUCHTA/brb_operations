"""Get various derived quantities from the `linear_bdot1` probe."""
from linear_bdot1.get_data import Linear_Bdot1_Data
from helmholtz.get_data import Helmholtz_Data
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from modules.coordinates import Position, Vector
from modules.bdot.interpolation import get_db_machine_coords, fast_db_machine_coords
import logging
from scipy.integrate import simpson, cumulative_trapezoid, romb
from scipy.interpolate import UnivariateSpline


def get_db(linear_data : Linear_Bdot1_Data, insert_used=False, tf_coil_lowered=False, interpolation_kind='linear', baseline_time_indeces=slice(10)):
    """
    Get the `r`, `phi`, and `z` components for the Bdot data.

    Parameters
    ----------
    linear_data : Linear_Bdot1_Data
    insert_used : bool, default=False
        Whether the insert was on the machine. If so, the positions need to be
        adjusted if the probe is not at 0 latitude.
    tf_coil_lowered : bool, default=False
        Whether the TF coil was lowered and the central axis of the machine is
        different than usual which requires corrections for position and 
        direction.
    interpolation_kind : str, default='linear'
        How we interpolate the magnitudes between coils.
    baseline_time_indeces : slice, default=slice(10)
        Slice of indeces along the time axis where we can remove the digitizer
        baseline offset and nothing should be happening.

    Returns
    -------
    interpolation_positions : np.array[float]
        2D-array of cylindrical positions,`(r, phi, z)`, where `phi` is in 
        radians. Shape of `(NUM_POSITIONS, 3)`.
    interpolation_db_values : np.array[float]
        3D-array of interpolated dB/dt components in cylindrical coordinates, 
        `(dB_r / dt, dB_phi / dt, dB_z / dt)`. Shape of `(NUM_POSITIONS, 3, NUM_TIMES)`.
    
    Notes
    -----
    This operates by taking three independent vectors and finding the magnitude 
    in the machine directions. Each coil is at a specific location and to get 3 
    independent vectors we must use a coil in the 1, 2, and 3 directions. We 
    interpolate between coils in each unique direction by interpolating in 
    angle and magnitude.
    """
    NUM_COILS = linear_data.db_1_working.size
    # Get the data from the working coils for each coil direction.

    # The orientations are written in probe coordinates where coordinate 2 is along the probe axis.
    # The gains and orientations were taken from the lightsaber values as the linear and lightsaber have the same PCBs.
    # ~r cylindrical coordinates.
    db1_gain = 1 / (0.00011405763008579561 * 33238)
    db1_values = linear_data.db_1[np.nonzero(linear_data.db_1_working)] * db1_gain
    db1_positions = linear_data.db_1_position[np.nonzero(linear_data.db_1_working)]
    db1_orientations = np.tile([
        0.9976353449693043, 0.06437111511201171, -0.02408480863975193,
        # 1, 0, 0
    ], (NUM_COILS, 1))[np.nonzero(linear_data.db_1_working)].astype(float)

    # ~phi cylindrical coordinates.
    db2_gain = 1 / (0.00010335002132104189 * 23544)
    db2_values = linear_data.db_2[np.nonzero(linear_data.db_2_working)] * db2_gain
    db2_positions = linear_data.db_2_position[np.nonzero(linear_data.db_2_working)]
    db2_orientations = np.tile([
        0.08366467033387577, -0.9958623097358973, 0.03547510374061758,
        # 0, 1, 0
    ], (NUM_COILS, 1))[np.nonzero(linear_data.db_2_working)].astype(float)

    # ~z cylindrical coordinates.
    db3a_gain = 1 / (9.180375948045916e-05 * 32500)
    db3a_values = linear_data.db_3a[np.nonzero(linear_data.db_3a_working)] * db3a_gain
    db3a_positions = linear_data.db_3a_position[np.nonzero(linear_data.db_3a_working)]
    db3a_orientations = np.tile([
        0.09690377136456345, -0.018328967482767557, 0.995124971069634,
        # 0, 0, 1
    ], (NUM_COILS, 1))[np.nonzero(linear_data.db_3a_working)].astype(float)
    db3b_gain = 1 / (9.324622304675157e-05 * 32500)
    db3b_values = linear_data.db_3b[np.nonzero(linear_data.db_3b_working)] * db3b_gain
    db3b_positions = linear_data.db_3b_position[np.nonzero(linear_data.db_3b_working)]
    db3b_orientations = np.tile([
        -0.09070268745374756, -0.0447742203636293, 0.9948709924806819,
        # 0, 0, 1
    ], (NUM_COILS, 1))[np.nonzero(linear_data.db_3b_working)].astype(float)

    # Now combine the 3 direction.
    db3_values = np.concatenate((db3a_values, db3b_values), axis=0)
    db3_positions = np.concatenate((db3a_positions, db3b_positions), axis=0)
    db3_orientations = np.concatenate((db3a_orientations, db3b_orientations), axis=0)

    all_values = [db1_values, db2_values, db3_values]
    all_positions = [db1_positions, db2_positions, db3_positions]
    all_orientations = [db1_orientations, db2_orientations, db3_orientations]

    # Rotate the probes around the 2 coordinate.
    probe_rotation_angle = np.deg2rad(15)
    rotation_matrix = np.array([
        [np.cos(probe_rotation_angle), -np.sin(probe_rotation_angle), 0],
        [np.sin(probe_rotation_angle), np.cos(probe_rotation_angle), 0],
        [0, 0, 1]
    ])

    # For each set of coils we need to remove the baseline.
    for i, (dir_values, dir_orientations) in enumerate(zip(all_values, all_orientations)):
        all_values[i] = dir_values - np.mean(dir_values[:, baseline_time_indeces], axis=1)[:, None]
        all_orientations[i] = (rotation_matrix @ dir_orientations.T).T

    # Let's change the orientation of each coil to be in the machine cylindrical coordinates.
    # We also change the position depending on how the BRB is setup.
    for dir_positions, dir_orientations in zip(all_positions, all_orientations):
        for i in range(dir_orientations.shape[0]):
            position = Position()
            position.cylindrical = dir_positions[i]

            orientation = Vector(position)
            orientation.from_port(
                linear_data.port.long_rad, linear_data.port.lat_rad, linear_data.port.alpha_rad, linear_data.port.beta_rad, linear_data.port.gamma_rad,
                dir_orientations[i, 0], dir_orientations[i, 1], dir_orientations[i, 2], linear_data.port.clocking_rad
            )

            # We only need to adjust for the insert position if the probe is not on the equator.
            if insert_used and not np.isclose(linear_data.port.lat_rad, 0):
                hemisphere = 'n' if linear_data.port.lat_rad > 0 else 's'
                orientation.position.add_insert(hemisphere)
            
            # We then adjust for the lowered TF coil
            if tf_coil_lowered:
                orientation.position.to_tf_centered_coordinates()

            # Rewrite the position and orientation in the new cylindrical coordinates.
            dir_positions[i] = orientation.position.cylindrical
            dir_orientations[i] = orientation.cylindrical

            # Also renormalize the orientations. They can become slightly off.
            dir_orientations[i] /= np.linalg.norm(dir_orientations[i])

    return get_db_machine_coords(all_values, all_positions, all_orientations, interpolation_kind=interpolation_kind)

def plot_db_data(linear_data : Linear_Bdot1_Data, helm_data : Helmholtz_Data):
    logging.warning("Assuming usage of insert and lowered TF coil.")
    positions, db_vals = get_db(linear_data, insert_used=True, tf_coil_lowered=True)
    db_vals *= 1.2
    
    interp_position_obj = Position()
    interp_position_obj.cylindrical = positions.T
    interp_position_obj.to_tf_centered_coordinates(inverse=True)
    background_field = helm_data.analytic_b_h(interp_position_obj.cylindrical[0], interp_position_obj.cylindrical[2], insert=True)
    background_field_vector = Vector(interp_position_obj)
    background_field_vector.cylindrical = np.array([
        background_field[0],
        np.zeros_like(background_field[0]),
        background_field[1]
    ])
    background_field_vector.position.to_tf_centered_coordinates()

    # db_val_splines = [UnivariateSpline()]
    integrated_b_vals = np.zeros_like(db_vals)
    for i in range(1, linear_data.time.size):
        integrated_b_vals[:, :, i] = simpson(db_vals[:, :, :i + 1], linear_data.time[:i + 1], axis=-1, even='first')
    # integrated_b_vals = cumulative_trapezoid(db_vals, linear_data.time, axis=2, initial=0)
    integrated_b_vals += background_field_vector.cylindrical.T[:, :, np.newaxis]

    fig = plt.figure()
    gs = fig.add_gridspec(2, 3, hspace=0, wspace=0)
    axs = gs.subplots(sharex=True, sharey=True)

    names = [r'$\partial_t B_{{{}}}$', r'$B_{{{}}}$']
    directions = ['r', r'\phi', 'z']
    for name, axs_type, vals_type in zip(names, axs, [db_vals, integrated_b_vals]):
        for i in range(3):
            ax = axs_type[i]
            vals = vals_type[:, i]

            vlim = np.max(abs(vals))
            cmesh = ax.pcolormesh(positions[:, 2], linear_data.time, vals.T, vmin=-vlim, vmax=vlim, cmap='seismic')
            ax.contour(positions[:, 2], linear_data.time, integrated_b_vals[:, 2].T, levels=[0], colors='green')
            # ax.contour(positions[:, 2], linear_data.time, integrated_b_vals[:, 2].T, colors='green')
            ax.text(0.05, 0.05, name.format(directions[i]), bbox={'facecolor': 'white', 'pad': 5, 'alpha': 0.5}, transform=ax.transAxes)
            ax.set_xlabel(r'$z$ (m)')
            ax.set_ylabel(r'$t$ (s)')
            ax.label_outer()

            fig.colorbar(cmesh, ax=ax, pad=0)
            ax.set_xlim(-0.8091999888420105, np.max(positions[:, 2]))

    fig.suptitle("Shot #{}: Linear Data\n".format(linear_data.shot_number) + r"$r = {:.3f}$ m".format(background_field_vector.position.cylindrical[0, 0]))
    return fig, axs
    

if __name__ == "__main__":
    # logging.basicConfig(level=logging.DEBUG)
    vac_shots = [60099, 60106, 60163, 60179, 60186, 60195, 60202, 60209, 60218, 60225]
    vac_shots = [60099]

    a116_gain = 1.4730317965232678
    a128_gain = 0.9948673500294152

    shots = range(60366, 60367)
    plasma_shots = [60095, 60104, 60167, 60176, 60182, 60189, 60197, 60205, 60213, 60219]
    shots = [60165]
    from pa_probe1.analyze_iv_curve import shot_information
    filepath = "./notebooks/data/{}/{}.mat"
    # for shot in shot_information.helmholtz_100_drive_6_middle_True_snubber_True_tf_50_z_43_clocking_45:
    for shot in [59944]:
        linear_data = Linear_Bdot1_Data(shot, time_index_range=(65000, 65170), load_filepath=filepath.format('linear_bdot1', shot))
        helm_data = Helmholtz_Data(shot, load_filepath=filepath.format('helmholtz', shot))

        new_db_1 = linear_data.db_1
        new_db_1[::2] *= a116_gain
        new_db_1[1::2] *= a128_gain
        linear_data.db_1 = new_db_1

        new_db_2 = linear_data.db_2
        new_db_2[::2] *= a116_gain
        new_db_2[1::2] *= a128_gain
        linear_data.db_2 = new_db_2

        new_db_3a = linear_data.db_3a
        new_db_3a[::2] *= a116_gain
        new_db_3a[1::2] *= a128_gain
        linear_data.db_3a = new_db_3a

        new_db_3b = linear_data.db_3b
        new_db_3b[:, 1:] = new_db_3b[:, :-1]
        new_db_3b[:, 0] = 0
        linear_data.db_3b = new_db_3b

        plot_db_data(linear_data, helm_data)
        linear_data.save(filepath.format('linear_bdot1', shot))
        helm_data.save(filepath.format('helmholtz', shot))
    plt.show()

