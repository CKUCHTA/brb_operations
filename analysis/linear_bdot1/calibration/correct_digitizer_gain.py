import numpy as np
from linear_bdot1.get_data import Linear_Bdot1_Data
from scipy.interpolate import interp1d
from scipy.optimize import differential_evolution


# To calibrate the gain factors of the digitizers, we are going to take a320 (db3b) as the ground truth.
# Then we need to change the gain on digitizers a116 and a128 (db3a) such that we get a smooth B_z field.

def apply_gains(db3a_vals, db1_vals, odd_db3a_coil_indeces, odd_db1_coil_indeces, a116_gain, a128_gain):
    """
    Parameters
    ----------
    db3a_vals, db1_vals : np.array[float]
        3D-array of dB / dt values of the 3a and 1 coils. Of shape `(NUM_SHOTS, NUM_COILS, NUM_TIMES)`.
    odd_db3a_coil_indeces, odd_db1_coil_indeces : np.array[int]
        1D-array of integers that point to which rows of the `db3a_vals` and 
        `db1_vals` array are for the odd numbered coils. As we remove some 
        coils when selecting the working coils we need to keep track. Of shape 
        `(NUM_COILS,)`.
    a116_gain, a128_gain : np.array[float]
        1D-array of gains to multiply values by of shape `(S,)`. This is an 
        array so that we can vectorize our optimizer. `1` is the current gain 
        of the digitizers.

    Returns
    -------
    new_db3a_vals, new_db1_vals : np.array[float]
        4D-array of dB / dt values of the 3a and 1 coils with gains applied. Of 
        shape `(NUM_SHOTS, NUM_COILS, NUM_TIMES, S)`.
    """
    # a116 is for odd probes, a128 for the even.
    try:
        new_db3a_vals = db3a_vals[:, :, :, np.newaxis] * a128_gain[np.newaxis, np.newaxis, np.newaxis, :]
        new_db3a_vals[:, odd_db3a_coil_indeces, :, :] *= a116_gain[np.newaxis, np.newaxis, np.newaxis, :] / a128_gain[np.newaxis, np.newaxis, np.newaxis, :]

        new_db1_vals = db1_vals[:, :, :, np.newaxis] * a128_gain[np.newaxis, np.newaxis, np.newaxis, :]
        new_db1_vals[:, odd_db1_coil_indeces, :, :] *= a116_gain[np.newaxis, np.newaxis, np.newaxis, :] / a128_gain[np.newaxis, np.newaxis, np.newaxis, :]
    except IndexError:
        new_db3a_vals = db3a_vals * a128_gain
        new_db3a_vals[:, odd_db3a_coil_indeces, :] *= a116_gain / a128_gain

        new_db1_vals = db1_vals * a128_gain
        new_db1_vals[:, odd_db1_coil_indeces, :] *= a116_gain / a128_gain

    return new_db3a_vals, new_db1_vals

def cost_function(all_db3b_interpolated_values, all_db3a_values_w_gain, all_db1_values_w_gain):
    """
    The cost increases if the 3b and 3a coil values are different or the 1 coil values don't monotonically increase.

    Parameters
    ----------
    all_db3b_interpolated_values : np.array[float]
        3D-array of dB / dt values of the 3b coils interpolated onto the 3a 
        positions in space. Of shape `(NUM_SHOTS, NUM_COILS, NUM_TIMES)`.
    all_db3a_values_w_gain, all_db1_values_w_gain : np.array[float]
        4D-array of dB / dt values of the 3a and 1 coils with gains applied. Of 
        shape `(NUM_SHOTS, NUM_COILS, NUM_TIMES, S)`.

    Returns
    -------
    np.array[float]
        1D-array of shape `(S,)` containing costs for various gains. 
    """
    db1_differences = all_db1_values_w_gain[:, 1:] - all_db1_values_w_gain[:, :-1]
    if len(all_db3a_values_w_gain.shape) == 4:
        return np.sum(np.abs(all_db3b_interpolated_values[:, :, :, np.newaxis] - all_db3a_values_w_gain), axis=(0, 1, 2)) + \
            np.sum(np.where(db1_differences > 0, db1_differences, 0), axis=(0, 1, 2))
    else:
        return np.sum(np.abs(all_db3b_interpolated_values - all_db3a_values_w_gain), axis=(0, 1, 2)) + \
            np.sum(np.where(db1_differences > 0, db1_differences, 0), axis=(0, 1, 2))

def optimization_function(dig_gains, all_db3b_interpolated_values, all_db3a_values, all_db1_values, odd_db3a_coil_indeces, odd_db1_coil_indeces):
    """
    Parameters
    ----------
    dig_gains : np.array[float]
        2D-array of gains for `a116` and `a128` of shape `(2, S)`.
    all_db3b_interpolated_values : np.array[float]
        3D-array of dB / dt values of the 3b coils interpolated onto the 3a 
        positions in space. Of shape `(NUM_SHOTS, NUM_COILS, NUM_TIMES)`.
    all_db3a_values : np.array[float]
        3D-array of dB / dt values of the 3a coils. Of shape 
        `(NUM_SHOTS, NUM_COILS, NUM_TIMES)`.
    all_db1_values : np.array[float]
        3D-array of dB / dt values of the 1 coils. Of shape 
        `(NUM_SHOTS, NUM_COILS, NUM_TIMES)`.
    odd_db3a_coil_indeces : np.array[int]
        1D-array of integers that point to which rows of the `db3a_vals` array 
        are for the odd numbered coils. As we remove some coils when selecting 
        the working coils we need to keep track. Of shape `(NUM_COILS,)`.
    odd_db1_coil_indeces : np.array[int]
        1D-array of integers that point to which rows of the `db1_vals` array 
        are for the odd numbered coils. As we remove some coils when selecting 
        the working coils we need to keep track. Of shape `(NUM_COILS,)`.

    Returns
    -------
    np.array[float]
        1D-array of summed absolute difference between the 3a and 3b coils of 
        shape `(S,)`.
    """
    new_db3a_vals, new_db1_vals = apply_gains(all_db3a_values, all_db1_values, odd_db3a_coil_indeces, odd_db1_coil_indeces, dig_gains[0], dig_gains[1])
    cost = cost_function(all_db3b_interpolated_values, new_db3a_vals, new_db1_vals)
    print("Tried gains {}: \t\tCost = {}".format(dig_gains, cost))
    return cost

def get_args(linear_data_objects : list[Linear_Bdot1_Data], baseline_time_indeces=slice(10)):
    """
    Parameters
    ----------
    linear_data_objects : list[Linear_Bdot1_Data]
        List of length `NUM_SHOTS` for each vacuum shot.
    baseline_time_indeces : slice, default=slice(10)
        Data indeces where we can remove the digitizer baseline.

    Returns
    -------
    all_db3b_interpolated_values : np.array[float]
        3D-array of dB / dt values of the 3b coils interpolated onto the 3a 
        positions in space. Of shape `(NUM_SHOTS, NUM_COILS, NUM_TIMES)`.
    all_db3a_values : np.array[float]
        3D-array of dB / dt values of the 3a coils. Of shape 
        `(NUM_SHOTS, NUM_COILS, NUM_TIMES)`.
    all_db1_values : np.array[float]
        3D-array of dB / dt values of the 1 coils. Of shape 
        `(NUM_SHOTS, NUM_COILS, NUM_TIMES)`.
    odd_db3a_coil_indeces : np.array[int]
        1D-array of integers that point to which rows of the `db3a_vals` array 
        are for the odd numbered coils. As we remove some coils when selecting 
        the working coils we need to keep track. Of shape `(NUM_COILS,)`.
    odd_db1_coil_indeces : np.array[int]
        1D-array of integers that point to which rows of the `db1_vals` array 
        are for the odd numbered coils. As we remove some coils when selecting 
        the working coils we need to keep track. Of shape `(NUM_COILS,)`.
    """
    all_db3b_interpolated_values = []
    all_db3a_values = []
    all_db1_values = []

    for linear_data in linear_data_objects:
        db_3b_values = linear_data.db_3b[np.nonzero(linear_data.db_3b_working)]
        db_3b_values -= np.mean(db_3b_values[:, baseline_time_indeces], axis=1)[:, np.newaxis]
        db_3b_z_positions = linear_data.db_3b_position[:, 2][np.nonzero(linear_data.db_3b_working)]
        db3b_interpolator = interp1d(
            db_3b_z_positions, db_3b_values, axis=0, bounds_error=False, fill_value=(db_3b_values[0], db_3b_values[-1])
        )
        db_3a_z_positions = linear_data.db_3a_position[:, 2][np.nonzero(linear_data.db_3a_working)]
        all_db3b_interpolated_values.append(db3b_interpolator(db_3a_z_positions))

        db_3a_values = linear_data.db_3a[np.nonzero(linear_data.db_3a_working)]
        db_3a_values -= np.mean(db_3a_values[:, baseline_time_indeces], axis=1)[:, np.newaxis]
        all_db3a_values.append(db_3a_values)

        db_1_values = linear_data.db_1[np.nonzero(linear_data.db_1_working)]
        db_1_values -= np.mean(db_1_values[:, baseline_time_indeces], axis=1)[:, np.newaxis]
        all_db1_values.append(db_1_values)

    odd_db3a_coil_indeces = np.nonzero(np.tile(np.array([True, False]), linear_data.db_3a_working.size // 2)[np.nonzero(linear_data.db_3a_working)])
    odd_db1_coil_indeces = np.nonzero(np.tile(np.array([True, False]), linear_data.db_1_working.size // 2)[np.nonzero(linear_data.db_1_working)])

    return np.array(all_db3b_interpolated_values), np.array(all_db3a_values), np.array(all_db1_values), odd_db3a_coil_indeces, odd_db1_coil_indeces

def optimize_gains(vacuum_shots):
    """
    Parameters
    ----------
    vacuum_shots : list[int]
        List of shot numbers that are vacuum shots and the lightsaber probe is 
        coming in along the insert.
    """
    generic_filepath = './notebooks/data/linear_bdot1/{}.mat'
    linear_data_objects = [Linear_Bdot1_Data(shot, load_filepath=generic_filepath.format(shot), time_index_range=(65000, 65150)) for shot in vacuum_shots]

    gain_bounds = [(0.5, 3), (0.5, 3)]
    function_args = get_args(linear_data_objects)

    for linear_data in linear_data_objects:
        linear_data.save(generic_filepath.format(linear_data.shot_number))

    result = differential_evolution(optimization_function, gain_bounds, args=function_args, x0=np.ones(2), vectorized=True)
    
    # Print out the optimized orientations.
    if not result.success:
        print("Because of '{}' the gains couldn't be optimized. Here they are anyhow!".format(result.message))

    for dig, gain in zip(['a116', 'a128'], result.x):
        print(dig, gain)


if __name__ == "__main__":
    vac_shots = [60099, 60106, 60163, 60179, 60186, 60195, 60202, 60209, 60218, 60225]
    optimize_gains(vac_shots)
