"""Get data from the linear probe from the MDSplus tree."""
from modules.generic_get_data import Get, Data, lazy_get, Port
import logging
import numpy as np


class Linear_Bdot1_Data(Data):
    """
    Hold all the data for the linear probe in one class for easier access and reduced network connections.
    
    To get data, call the data without the underscore treating it as an attribute.
    """
    def __init__(self, tree, *args, **kwargs):
        """
        Load the data for some nodes from the tree.
        """
        super().__init__(tree, [], [], *args, **kwargs)

    @lazy_get
    def db_phi(self):
        return self.get(Get('\\linear_bdot1_dbphi'))

    @lazy_get
    def db_r(self):
        return self.get(Get('\\linear_bdot1_dbr'))

    @lazy_get
    def db_z(self):
        return self.get(Get('\\linear_bdot1_dbz'))

    def _is_bad_mdsplus_position(self):
        """
        Returns whether MDSplus has a valid position for this shot.
        """
        return (56376 <= self.shot_number <= 60428)

    @lazy_get
    def position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each probe. Of shape `(NUM_PROBES, 3)`.
        """
        if self._is_bad_mdsplus_position():
            logging.warning("Since the linear position in MDSplus is incorrect, I'm manually returning the probe position.")
            r = self.port.rport * np.cos(self.port.lat_rad)
            phi_rad = self.port.long_rad
            first_probe_z_location = 0.333
            z = first_probe_z_location - 2 * 0.0254 * np.arange(24)

            return np.array([r * np.ones_like(z), phi_rad * np.ones_like(z), z]).T
        else:
            return self.get(
                Get('TRANSPOSE( [' + ', '.join('DATA(\\linear_bdot1_{direction})'.format(direction=dir) for dir in ['r', 'phi', 'z']) + '] )', name='linear_bdot1_position', signal=False)
            )

    @lazy_get
    def db_1(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 1 coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\linear_bdot1_db1'))
    
    @lazy_get
    def db_1_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 1 coil. Of shape `(NUM_COILS, 3)`.
        """
        if self._is_bad_mdsplus_position():
            return self.position - np.array([0, 0, .034])[np.newaxis, :]
        else:
            return self.get(
                Get('TRANSPOSE( [' + ', '.join('DATA(\\linear_bdot1_db1_{direction})'.format(direction=dir) for dir in ['r', 'phi', 'z']) + '] )', name='linear_bdot1_db1_position', signal=False)
            )
    
    @lazy_get
    def db_1_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 1 coil. Of shape `(NUM_COILS,)`.
        """
        # As the linear_bdot1 probe does not yet have a working attribute we manually set it.
        logging.warning("Using hard-coded `Linear_Bdot1_Data.db_1_working` attribute.")
        return np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1], dtype=bool)

    @lazy_get
    def db_2(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 2 coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\linear_bdot1_db2'))
    
    @lazy_get
    def db_2_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 2 coil. Of shape `(NUM_COILS, 3)`.
        """
        if self._is_bad_mdsplus_position():
            return self.position - np.array([0, 0, .01])[np.newaxis, :]
        else:
            return self.get(
                Get('TRANSPOSE( [' + ', '.join('DATA(\\linear_bdot1_db2_{direction})'.format(direction=dir) for dir in ['r', 'phi', 'z']) + '] )', name='linear_bdot1_db2_position', signal=False)
            )
    
    @lazy_get
    def db_2_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 2 coil. Of shape `(NUM_COILS,)`.
        """
        logging.warning("Using hard-coded `Linear_Bdot1_Data.db_2_working` attribute.")
        return np.array([1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1], dtype=bool)

    @lazy_get
    def db_3a(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 3a coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\linear_bdot1_db3a'))
    
    @lazy_get
    def db_3a_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 3a coil. Of shape `(NUM_COILS, 3)`.
        """
        if self._is_bad_mdsplus_position():
            return self.position
        else:
            return self.get(
                Get('TRANSPOSE( [' + ', '.join('DATA(\\linear_bdot1_db3a_{direction})'.format(direction=dir) for dir in ['r', 'phi', 'z']) + '] )', name='linear_bdot1_db3a_position', signal=False)
            )
    
    @lazy_get
    def db_3a_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 3a coil. Of shape `(NUM_COILS,)`.
        """
        logging.warning("Using hard-coded `Linear_Bdot1_Data.db_3a_working` attribute.")
        # TODO: The 5th coil (index 4) should be good but it's gain is off.
        return np.array([1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=bool)

    @lazy_get
    def db_3b(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 3b coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\linear_bdot1_db3b'))
    
    @lazy_get
    def db_3b_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 3b coil. Of shape `(NUM_COILS, 3)`.
        """
        if self._is_bad_mdsplus_position():
            return self.position - np.array([0, 0, .025])[np.newaxis, :]
        else:
            return self.get(
                Get('TRANSPOSE( [' + ', '.join('DATA(\\linear_bdot1_db3b_{direction})'.format(direction=dir) for dir in ['r', 'phi', 'z']) + '] )', name='linear_bdot1_db3b_position', signal=False)
            )
    
    @lazy_get
    def db_3b_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 3b coil. Of shape `(NUM_COILS,)`.
        """
        logging.warning("Using hard-coded `Linear_Bdot1_Data.db_3b_working` attribute.")
        return np.array([1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1], dtype=bool)

    @lazy_get
    def time(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of time values. Of shape `(NUM_TIMES,)`.
        """
        server_time_value = self.get(Get('dim_of(\\linear_bdot1_db1)', name='linear_bdot1_time'))
        if server_time_value.size == 0:
            logging.warning("Since the `linear_bdot1` time data doesn't work at the moment, I'm manually returning the timing.")
            return 10**-7 * np.arange(self.db_phi.shape[1])
        else:
            return server_time_value

    @lazy_get
    def port(self) -> Port:
        """
        Returns
        -------
        Port
            Object that represents the port position of this probe.
        """
        return Port(self, 'linear_bdot1_')

