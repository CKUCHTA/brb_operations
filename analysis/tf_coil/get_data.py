import logging
from modules.generic_get_data import Get, Data, lazy_get
import numpy as np


class TF_Coil_Data(Data):
    def __init__(self, tree, i_1=False, i_2=False, i_tot=False, v_1=False, v_2=False, time=False, *args, **kwargs):
        variable_booleans = [i_1, i_2, i_tot, v_1, v_2, time]
        self.get_calls = [
            Get('\\tf_coil_i_1'), Get('\\tf_coil_i_2'), Get('\\tf_coil_i_tot'), 
            Get('\\tf_coil_v_1'), Get('\\tf_coil_v_2'), Get('dim_of(\\tf_coil_i_1)', name='tf_coil_time')
        ]

        self._i_1, self._i_2, self._i_tot, self._v_1, self._v_2, self._time = super().__init__(tree, variable_booleans, self.get_calls, *args, **kwargs)

    @lazy_get
    def i_1(self):
        return self.get(self.get_calls[0])

    @lazy_get
    def i_2(self):
        return self.get(self.get_calls[1])

    @lazy_get
    def i_tot(self):
        if 59645 <= self.shot_number <= 59801:
            logging.warning("Using current from Gun 13 for RF coil current.")
            return self.get(Get('\\gun_13_i_arc')) * 7
        else:
            return self.get(self.get_calls[2])

    @lazy_get
    def v_1(self):
        return self.get(self.get_calls[3])

    @lazy_get
    def v_2(self):
        return self.get(self.get_calls[4])

    @lazy_get
    def time(self):
        return self.get(self.get_calls[5])
    
    def b_phi(self, r):
        # Take the mean of the current from index 2000 to 2500 as this is before the reconnection shot.
        tf_current = np.mean(self.i_tot[2000:2500])
        return 4 * np.pi * 10**-7 * tf_current / (2 * np.pi * r)


if __name__=="__main__":
    # Get average for 100% TF (shots 53999-54001)
    from modules.shot_loader import get_remote_shot_tree
    import matplotlib.pyplot as plt
    import numpy as np
    logging.basicConfig(level=0)

    avg_tf_current = 0
    shot_numbers = [59774]
    for shot_num in shot_numbers:
        tree = get_remote_shot_tree(shot_num)
        data = TF_Coil_Data(tree)

        curr_avg_current = np.mean(data.i_1[3000:3500])
        avg_tf_current += curr_avg_current
        plt.plot(data.i_1)
        plt.hlines(curr_avg_current, 0, 5000)
        plt.title('Shot #{}: Current'.format(shot_num))
        plt.show()
        plt.plot(data.v_1)
        plt.title('Shot #{}: Voltage'.format(shot_num))
        plt.show()

    avg_tf_current /= len(shot_numbers)
    print("Average 100% TF Current: {}".format(avg_tf_current))
