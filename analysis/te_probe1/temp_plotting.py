from te_probe1.get_data import Te_Probe1_Data
from te_probe1.analyze_iv_curve.fit_curve import expected_current, get_all_fits
from modules.shot_loader import get_remote_shot_tree
import matplotlib.pyplot as plt
import logging
import numpy as np
logging.basicConfig(level=logging.DEBUG)

te_data = Te_Probe1_Data(55246, time_range=(0.01801, 0.01807))
params, stdevs, _, errors_during_optimization = get_all_fits(te_data, num_processes=8)
colors = ['red', 'green', 'blue']

for val_curve, stdev_curve, name, color in zip(params, stdevs, ['n', 'Te', 'Vplasma'], colors):
    normalized_value = val_curve / np.mean(val_curve)
    normalized_stdev = stdev_curve / np.mean(val_curve)
    plt.fill_between(te_data.time, normalized_value - normalized_stdev, normalized_value + normalized_stdev, color=color, alpha=0.2)
    plt.plot(te_data.time, normalized_value, label=name, color=color)

    plt.scatter(te_data.time[np.nonzero(errors_during_optimization)], normalized_value[np.nonzero(errors_during_optimization)], color='red', marker='x')

# plt.plot(te_data.time, params[0] / np.mean(params[0]), label='n')
# plt.plot(te_data.time, params[1] / np.mean(params[1]), label='Te')
# plt.plot(te_data.time, params[2] / np.mean(params[2]), label='Vplasma')

# plt.axhline(1 / np.mean(params[1]), color='grey')

plt.legend()
plt.show()
exit()

esat_shots = list(range(54270, 54286))
isat_shots = list(range(54286, 54302))
te_mode_shots = list(range(53391, 53394))


logging.basicConfig(level=logging.DEBUG)
for shot_num in [55246]:
    tree = get_remote_shot_tree(shot_num)
    te_data = Te_Probe1_Data(tree)

    for i, v in enumerate(te_data.vprobe):
        if te_data.working[i]:
            plt.plot(te_data.time, v, label=i + 1)

    plt.title('Shot #{}: Vprobe'.format(shot_num))
    plt.legend()
    plt.show()

    for i, current in enumerate(te_data.iprobe):
        if te_data.working[i]:
            plt.plot(te_data.time, current, label=i + 1)

    plt.title('Shot #{}: Iprobe'.format(shot_num))
    plt.legend()
    plt.show()
