from te_probe2.calibration.optimization import get_te1_calibrations, save_optimized_values, load_optimized_values
from te_probe2.calibration.plot_data import plot_optimized_values
from modules.shot_loader import get_remote_shot_tree
from te_probe1.get_data import Te_Probe1_Data
import logging


logging.basicConfig(level=logging.INFO)
isat_shots = list(range(54286, 54303))
esat_shots = list(range(54270, 54286))
vac_shots = [54273, 54277, 54281, 54285, 54290, 54294, 54298, 54302]
save_file_path = './analysis/te_probe1/calibration/data/areas_and_resistances.mat'

for shot in vac_shots:
    if shot in isat_shots:
        isat_shots.remove(shot)
    else:
        esat_shots.remove(shot)

# for shot in isat_shots:
#     tree = get_remote_shot_tree(shot)
#     te_data = Te_Probe1_Data(tree, vprobe=False, working=False, time=False)
#     plot_usable_data(tree, te_data.iprobe, te_data.working, comparable_index_range=(16000, 50000))
# exit()

# areas, resistances = get_te1_calibrations(isat_shots, esat_shots, 50)
# print("Areas: {}".format(areas))
# print("Resistances: {}".format(resistances))
# save_optimized_values(areas, resistances, save_file_path)

areas, resistances = load_optimized_values(save_file_path)
plot_optimized_values(areas, resistances)