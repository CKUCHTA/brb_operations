# Te1 Calibration

To calibrate the area and wire resistace, please refer to the calibration scripts in `te_probe2`. To run the calibration, use the `get_te1_calibrations` function in `optimization.py`.
