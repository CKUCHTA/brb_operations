"""Calculate V_probe and I_probe from the raw data measured by the digitizer for a Te probe."""


def get_V_probe_function(te_data):
    """
    Get the function to calculate the probe voltage from the raw data.

    Returns
    -------
    V_probe : function[float, float, float, int] -> float
        Function that takes the raw signal measurement, raw noise measurement, 
        bias potential, and tip number and returns the probe potential.
    """
    def V_probe(v_s, v_n, v_bias, tip_number):
        tip_index = tip_number - 1
        i_ds = v_s / te_data.r_diggnd[tip_index]
        i_dn = v_n / te_data.r_diggnd[tip_index]

        r_div = te_data.r_div[tip_index]
        r_sense = te_data.r_sense[tip_index]
        r_wire = te_data.r_wire[tip_index]
        return (
            (r_div * r_sense + r_div * r_wire + r_sense * r_wire) * i_ds - 
            (r_div * r_sense + r_div * r_wire + r_sense * r_sense + r_sense * r_wire) * i_dn
        ) / r_sense + v_bias

    return V_probe


def get_I_probe_function(te_data):
    """
    Get the function to calculate the probe current from the raw data.
    
    Returns
    -------
    I_probe : function[float, float, int] -> float
        Function that takes the raw signal measurement, raw noise measurement, 
        and tip number and returns the probe current.
    """
    def I_probe(v_s, v_n, tip_number):
        tip_index = tip_number - 1
        i_ds = v_s / te_data.r_diggnd[tip_index]
        i_dn = v_n / te_data.r_diggnd[tip_index]

        r_div = te_data.r_div[tip_index]
        r_sense = te_data.r_sense[tip_index]
        return ((r_div + r_sense) * i_ds - (r_div + r_sense) * i_dn) / r_sense

    return I_probe
