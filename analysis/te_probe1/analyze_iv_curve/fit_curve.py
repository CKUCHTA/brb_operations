import numpy as np
from scipy.optimize import curve_fit, differential_evolution
from scipy.odr import OdrError
import logging
from multiprocessing import Pool
import warnings
from modules.generic_get_data import Data
from te_probe3.calibration import manual_get_data
import tqdm
from modules.langmuir.curve_fit import solve_odr


# Constants
M_ELECTRON = 9.109e-31 # kg
M_PROTON = 1.673e-27 # kg
Q_ELECTRON = 1.602e-19 # C

def expected_current(v_probe, number_density, electron_temperature, v_plasma, probe_area):
    # This is equation 3.2.33 from Hutchinson (2002).
    # We assume that we are using a hydrogen plasma and thus m_i = m_p.
    # Since the debye length is much smaller than the probe we'll assume the sheath area = probe area.
    return number_density * -Q_ELECTRON * probe_area * (Q_ELECTRON * electron_temperature / M_PROTON)**0.5 * (
        0.5 * ((2 * M_PROTON) / (np.pi * M_ELECTRON))**0.5 * np.exp((v_probe - v_plasma) / electron_temperature) - np.exp(-0.5)
    )

def get_bounds(v_probe_data_snapshot, i_probe_data_snapshot, prior_solution=None):
    """
    Get upper and lower bounds for optimization.
    
    Parameters
    ----------
    v_probe_data_snapshot   = V_probe values at a single point in time for all probes.
    i_probe_data_snapshot   = I_probe values at a single point in time for all probes.
    prior_solution          = (3) element array of (n, Te, Vp) from previous solution.

    Returns
    -------
    differential_evolution_bounds   = Array that contains bounds for differential_evolution.
    curve_fit_bounds                = Same array in different format for curve_fit.
    """
    if prior_solution is None:
        # Use a large range of bounds to get initial solution.
        n_bounds = (10**14, 10**22)
        Te_bounds = (0.1, 50) 
        Vp_bounds = get_Vp_bounds(v_probe_data_snapshot, i_probe_data_snapshot)
    else:
        # Use the prior solution parameters with a smaller range of bounds.
        n_bounds = (prior_solution[0] * 10**-3, prior_solution[0] * 10**3)
        Te_bounds = (max(0.1, prior_solution[1] - 30), prior_solution[1] + 30)
        Vp_bounds = (prior_solution[2] - 70, prior_solution[2] + 70)
    
    # The bounds format is different for each optimizer.
    differential_evolution_bounds = np.array([n_bounds, Te_bounds, Vp_bounds], dtype='float')
    curve_fit_bounds = differential_evolution_bounds.T

    return differential_evolution_bounds, curve_fit_bounds

def get_Vp_bounds(v_probe_data_snapshot, i_probe_data_snapshot):
    """
    Get approximate upper and lower bounds to the plasma potential.

    Parameters
    ----------
    v_probe_data_snapshot   = V_probe values at a single point in time for all probes.
    i_probe_data_snapshot   = I_probe values at a single point in time for all probes.

    Returns
    -------
    lower_bound = The lowest value the plasma potential could be assuming it is greater than the lowest probe potential.
    upper_bound = The highest value the potential could reasonably be. We use a constant value as there is no good estimator.
    """
    # Since the plasma potential is always greater then the floating potential we will find where the floating potential is.
    # The floating potential is where the current changes from positive to negative.
    # If the leftmost current (lowest V) is not positive then there is probably a good bit of noise.
    # We'll assume that the lower bound is the lowest V in this case.
    v_min_index = v_probe_data_snapshot.argmin()
    if i_probe_data_snapshot[v_min_index] < 0:
        lower_bound = v_probe_data_snapshot[v_min_index]
    else:
        # Find the point at which the current changes from negative to positive.
        for i in range(v_min_index + 1, len(v_probe_data_snapshot)):
            if i_probe_data_snapshot[i] < 0:
                # Find point at which linear interpolation crosses 0.
                prev_v = v_probe_data_snapshot[i - 1]
                prev_i = i_probe_data_snapshot[i - 1]
                next_v = v_probe_data_snapshot[i]
                next_i = i_probe_data_snapshot[i]

                lower_bound = (prev_v - next_v) * prev_i / (next_i - prev_i) + prev_v
                break
        else:
            lower_bound = v_probe_data_snapshot[v_min_index]

    # We choose the upper bound to be a constant as we don't have a simple way to estimate it.
    upper_bound = 150

    return lower_bound, upper_bound

def get_plasma_fit(v_probe_data, i_probe_data, probe_area):
    """
    Get the plasma parameters for a set of time points.

    Parameters
    ----------
    v_probe_data, i_probe_data : np.array[float]
        2D-array of shape `(NUM_TIPS, NUM_TIMES)` of Vprobe and Iprobe values.
    probe_area : float
        Actual area of the probe (m^2).

    Returns
    -------
    optimized_parameters, std_dev_errors : np.array[float]
        2D-arrays of shape `(NUM_TIMES, 3)` of `(n, Te, Vplasma)` and their 
        standard deviations.
    sigma_vals : np.array[float]
        2D-array of shape `(NUM_TIMES, NUM_TIPS)` containing the relative error 
        set to each iprobe value when curve fitting.
    """
    # First set a weighting scheme of the points so that we trust points that have lower iprobe magnitude.
    # If the iprobe value is positive we trust the point and we set sigma to 1. As we get more negative I we also increase sigma.
    def sigma_func(i_probe):
        # Return a cubic that is normalized by the average i magnitude if i is negative.
        return np.where(i_probe >= 0, 1, 1 + 0.05 * (-i_probe / np.mean(np.abs(i_probe), axis=0))**3)

    num_time_points = v_probe_data.shape[1]
    sigma_vals = sigma_func(i_probe_data)

    optimization_function = lambda v_probe, n, T, v_plasma: expected_current(v_probe, n, T, v_plasma, probe_area)
    def differential_evolution_function(x, time_index=0):
        residuals = i_probe_data[:, time_index] - expected_current(v_probe_data[:, time_index], x[0], x[1], x[2], probe_area)
        with warnings.catch_warnings(record=True) as caught_warnings:
            chisq = sum((residuals / sigma_vals[:, time_index])**2)
            if len(caught_warnings) != 0:
                logging.debug("Overflow occured while getting chisq value.\nResiduals:\t{}\nSigma_vals:\t{}\nChi_sq:\t{}".format(residuals, sigma_vals[:, time_index], chisq))
        return chisq

    diff_bounds, _ = get_bounds(v_probe_data[:, 0], i_probe_data[:, 0])
    # For the first point we will use a global optimizer to find the solution and then get a better solution with the local. 
    # The optimization at later points will use the simpler local optimizer using the previous point solution.
    initial_optimization_result = differential_evolution(differential_evolution_function, diff_bounds, polish=False, seed=0)
    prior_solution = initial_optimization_result.x

    optimized_parameters = np.zeros((3, num_time_points))
    std_dev_errors = np.zeros((3, num_time_points))
    error_during_optimization = np.zeros(num_time_points)
    for time_index in range(num_time_points):
        # TODO: Change format to print out which thread is progressing.
        logging.debug("{} / {}".format(time_index, num_time_points))
        # Update the bounds at each time point.
        diff_bounds, curve_bounds = get_bounds(v_probe_data[:, time_index], i_probe_data[:, time_index], prior_solution)

        # Start with a global convergence method.
        optimization_result = differential_evolution(differential_evolution_function, diff_bounds, args=tuple([time_index]), polish=False, seed=0)

        # Now use the local curve_fit method to finish it off and get the covariances.
        try:
            optimized_parameters[:, time_index], covariances = curve_fit(optimization_function, v_probe_data[:, time_index], i_probe_data[:, time_index], 
                                                                        optimization_result.x, sigma_vals[:, time_index],
                                                                        bounds=curve_bounds)
        except RuntimeError:
            # logging.exception("Runtime exception occured while trying to solve for plasma parameters.\n" + 
            #     "Likely due to bad curve from Te probe. Setting (n = 0 +/- 0, Te = 1 +/- 0, Vp = 0 +/- 0).")
            optimized_parameters[:, time_index] = np.array((0, 1, 0))
            std_dev_errors[:, time_index] = np.zeros(3)
            error_during_optimization[time_index] = True
        else:
            # Update prior solution if we have found a good solution.
            prior_solution = optimized_parameters[:, time_index]
            std_dev_errors[:, time_index] = np.sqrt(np.diag(covariances))

    return optimized_parameters, std_dev_errors, sigma_vals, error_during_optimization

def fitting_function(v_probe_slice, i_probe_slice, probe_area):
    fitted_vals = []
    fitted_val_stdevs = []
    
    error_during_optimization = np.zeros(v_probe_slice.shape[1], dtype=bool)
    initial_guess = np.array([10**17, 3, 0])
    for index, (v_probe, i_probe) in enumerate(zip(v_probe_slice.T, i_probe_slice.T)):
        try:
            result = solve_odr(
                v_probe, i_probe, np.zeros_like(v_probe), probe_area, 
                lambda V_p, unused_arg1, unused_arg2: V_p, lambda unused_arg, I_p: I_p, initial_guess
            )[0]
            fitted_vals.append(result.beta)
            fitted_val_stdevs.append(result.sd_beta)
            # Only update the guess if we didn't error.
            initial_guess = result.beta
        except ValueError as e:
            logging.exception("Error occured while optimizing parameters.")
            fitted_vals.append(np.array([0, 1, 0]))
            fitted_val_stdevs.append(np.zeros(3))
            error_during_optimization[index] = True
            
    fitted_vals = np.array(fitted_vals).T
    fitted_val_stdevs = np.array(fitted_val_stdevs).T
    return fitted_vals, fitted_val_stdevs, np.ones_like(v_probe_slice), error_during_optimization


def get_all_fits(v_probe_data_or_te_data, i_probe_data=None, probe_area=None, analysis_time_range=None, time_index=None, num_processes=8):
    """
    Get all the plasma parameters along with errors and sigma values using multiple threads.
    
    Parameters
    ----------
    v_probe_data_or_te_data : np.array[float] or Data
        Array of shape `(NUM_TIPS, NUM_TIMES)` that contains v_probe values OR Te_Probe data object.
    i_probe_data : np.array[float], default=None
        Array of shape `(NUM_TIPS, NUM_TIMES)` that contains i_probe values. Required if Te data object not given.
    probe_area : float, default=None
        Float of probe area in (m^2). Required if Te data object not given.
    analysis_time_range : tuple[float], default=None
        Start and end time to do analysis for. If `None` then use all times.
    time_index : int, default=None
        If `analysis_time_range` is `None` and `time_index` is not `None` then 
        get the fit for a specific time index.
    num_processes : int, default=8
        Number of processes to split the curve fitting amongst.

    Returns
    -------
    all_optimized_parameters : np.array[float]
        Array of shape `(3, NUM_TIMES)` containing `(n, Te, Vplasma)` for each time slice.
    all_std_dev_errors : np.array[float]
        Array of shape `(3, NUM_TIMES)` containing `(n, Te, Vplasma)` standard deviations for each time slice.
    all_sigma_values : np.array[float]
        Array of shape `(3, NUM_TIMES)` containing relative errors on each iprobe value.
    error_during_optimization : np.array[bool]
        Array of shape `(NUM_TIMES,)` containing a boolean of weather an error occured during optimization. If an error occured then the params are invalid.
    """
    if isinstance(v_probe_data_or_te_data, Data):
        if analysis_time_range is not None:
            # Indeces of arrays from the te object that are in the passed time range.
            time_indeces = np.nonzero(np.logical_and(v_probe_data_or_te_data.time >= analysis_time_range[0], v_probe_data_or_te_data.time <= analysis_time_range[1]))[0]
        elif time_index is not None:
            time_indeces = np.array([time_index])
        else:
            # Use all points.
            time_indeces = np.nonzero(np.ones_like(v_probe_data_or_te_data.time))[0]

        te_data = v_probe_data_or_te_data

        # r_diggnd, r_div, r_sense, r_wire, a_rel, alternate_vp, parameter_tips = manual_get_data.load_parameters(all_tips=True)
        # tips_to_use = []
        # for tip in parameter_tips:
        #     if te_data.working[tip - 1]:
        #         tips_to_use.append(tip)
        # tips_to_use = np.array(tips_to_use)
        # r_diggnd = r_diggnd[tips_to_use - 1]
        # r_div = r_div[tips_to_use - 1]
        # r_sense = r_sense[tips_to_use - 1]
        # r_wire = r_wire[tips_to_use - 1]
        # a_rel = a_rel[tips_to_use - 1]

        # v_bias = te_data.vbias[tips_to_use - 1]
        # v_s_data = np.array([manual_get_data.v_s(te_data, tip) for tip in tips_to_use])
        # v_n_data = np.array([manual_get_data.v_n(te_data, tip) for tip in tips_to_use])
        # A_p = te_data.area

        # num_time_points = time_indeces.size
        # all_optimized_parameters = np.zeros((3, num_time_points))
        # all_optimized_parameters[1] = 1
        # all_std_dev_errors = np.zeros_like(all_optimized_parameters)
        # all_sigma_vals = np.ones_like(v_s_data)
        # error_during_optimization = np.zeros(num_time_points, dtype=bool)

        # for i, time_index in enumerate(tqdm.tqdm(time_indeces)):
        #     try:
        #         odr_output, usable_points = solve_odr(
        #             v_s_data[:, time_index], v_n_data[:, time_index], v_bias, A_p, 
        #             lambda v_s, v_n, V_b: manual_get_data.v_p_func(v_s, v_n, r_diggnd, r_div, r_sense, r_wire, V_b, alternate_vp),
        #             lambda v_s, v_n: manual_get_data.i_p_func(v_s, v_n, r_diggnd, r_div, r_sense, a_rel),
        #             np.array((10**17, 1, 15)), check_fit=False
        #         )
        #         all_optimized_parameters[:, i] = odr_output.beta
        #         all_std_dev_errors[:, i] = odr_output.sd_beta
        #     except (OdrError, ValueError) as e:
        #         logging.warning("Errored on time index {} when trying to fit IV curve.".format(time_index))
        #         error_during_optimization[i] = True
        # return all_optimized_parameters, all_std_dev_errors, all_sigma_vals, error_during_optimization

        v_probe_data = te_data.extract_working(te_data.vprobe)[:, time_indeces]
        i_probe_data = te_data.extract_working(te_data.iprobe)[:, time_indeces]
        probe_area = te_data.area
    else:
        if analysis_time_range is not None:
            logging.warning("I don't know how to use the passed 'analysis_time_range' as there is no associated time array.\nI'm going to get fits for all data points instead. To fix this issue, pass a Te Probe Data object.")
        v_probe_data = v_probe_data_or_te_data

    # Get slices that are assigned to each process to complete.
    num_time_points = v_probe_data.shape[1]
    if num_time_points == 1:
        result, _ = solve_odr(v_probe_data, i_probe_data, np.zeros_like(v_probe_data), probe_area, lambda V_p: V_p, lambda I_p: I_p, np.array([10**17, 3, 0]))
        return result.beta[:, np.newaxis], result.sd_beta[:, np.newaxis], np.ones((3, 1)), np.zeros(num_time_points)
        # result = get_plasma_fit(v_probe_data, i_probe_data, probe_area)
        # return result[0], result[1], result[2], result[3]

    if num_processes == 1:
        return fitting_function(v_probe_data, i_probe_data, probe_area)

    num_points_per_thread = num_time_points // num_processes
    # Set all slices except the last. The last will take care of everything remaining up to the last point.
    time_slices = [slice(i * num_points_per_thread, (i + 1) * num_points_per_thread) for i in range(num_processes - 1)]
    time_slices.append(slice((num_processes - 1) * num_points_per_thread, num_time_points))

    with Pool(num_processes) as pool:
        all_results = pool.starmap(fitting_function, ((v_probe_data[:, t_slice], i_probe_data[:, t_slice], probe_area) for t_slice in time_slices))
        # all_results = pool.starmap(get_plasma_fit, ((v_probe_data[:, t_slice], i_probe_data[:, t_slice], probe_area) for t_slice in time_slices))

    # Split the results into arrays and return the arrays.
    all_optimized_parameters = np.zeros((3, num_time_points))
    all_std_dev_errors = np.zeros((3, num_time_points))
    all_sigma_vals = np.zeros_like(i_probe_data)
    error_during_optimization = np.zeros(num_time_points, dtype=bool)
    for i, time_slice in enumerate(time_slices):
        all_optimized_parameters[:, time_slice], all_std_dev_errors[:, time_slice], all_sigma_vals[:, time_slice], error_during_optimization[time_slice] = all_results[i]

    return all_optimized_parameters, all_std_dev_errors, all_sigma_vals, error_during_optimization
