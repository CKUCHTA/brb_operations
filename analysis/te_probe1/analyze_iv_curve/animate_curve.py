from te_probe1.get_data import Te_Probe1_Data
from te_probe3.get_data import Te_Probe3_Data
from te_probe3.calibration import manual_get_data
from te_probe1.analyze_iv_curve.fit_curve import expected_current, get_all_fits, get_plasma_fit
from modules.shot_loader import get_remote_shot_tree
import matplotlib.pyplot as plt
from matplotlib.animation import FFMpegWriter, FuncAnimation
from scipy.io import savemat, loadmat
import numpy as np
import logging
import tqdm

# The following errorbar updating code was taken from: https://stackoverflow.com/questions/25210723/matplotlib-set-data-for-errorbar-plot
def update_errorbar(errobj, x, y, xerr=None, yerr=None):
    """
    Update an errorbar object for animation.
    
    Parameters
    ----------
    errobj : Errorbar 
        Errorbar object to update from axis object.
    x, y : np.array[float]
        `x` and `y` positions of the points.
    xerr, yerr : np.array[float]
        Size of errorbars in x and y direction.
    """
    # Extract the points, errorbar caps, and the error bar lines.
    scatter_points, caps, bars = errobj

    # Extract x and y bars and caps.
    if len(bars) == 2:
        # If the bars are in both x and y then need errors for both.
        if xerr is None and yerr is None:
            raise ValueError("Your errorbar object has 2 dimension of error bars defined. You must provide xerr and yerr.")
        barsx, barsy = bars

        # Try to extract the errorbar caps if they exist. Otherwise ignore setting them.
        try:
            errx_top, errx_bot, erry_top, erry_bot = caps
        except ValueError:
            pass
    elif len(bars) == 1:
        if not ((xerr is     None and yerr is not None) or\
                (xerr is not None and yerr is     None)):
            raise ValueError("Your errorbar object has 1 dimension of error bars defined. You must provide xerr or yerr.")

        if xerr is not None:
            barsx, = bars
            try:
                errx_top, errx_bot = caps
            except ValueError:
                pass
        else:
            barsy, = bars
            try:
                erry_top, erry_bot = caps
            except ValueError:
                pass

    scatter_points.set_data(x, y)

    # Set the error bars + caps in the x direction.
    try:
        errx_top.set_xdata(x + xerr)
        errx_bot.set_xdata(x - xerr)
        errx_top.set_ydata(y)
        errx_bot.set_ydata(y)
    except NameError:
        pass
    try:
        barsx.set_segments([np.array([[xt, y], [xb, y]]) for xt, xb, y in zip(x + xerr, x - xerr, y)])
    except NameError:
        pass

    # Set the error bars + caps in the y direction.
    try:
        erry_top.set_xdata(x)
        erry_bot.set_xdata(x)
        erry_top.set_ydata(y + yerr)
        erry_bot.set_ydata(y - yerr)
    except NameError:
        pass
    try:
        barsy.set_segments([np.array([[x, yt], [x, yb]]) for x, yt, yb in zip(x, y + yerr, y - yerr)])
    except NameError:
        pass

def extract_errorbar_artist_list(errobj):
    """
    Create a list of artist objects from an Errorbar object.
    """
    data_line, caplines, barcols = errobj
    return [data_line] + list(caplines) + list(barcols)

class IV_Curve:
    def __init__(self, axis, te_data, analysis_index_range, optimized_vals, val_stdevs, iprobe_errors):
        """
        Object for plotting the IV curve of the Te probe.

        Parameters
        ----------
        axis : matplotlib.axis
            Axis to plot the IV curve on.
        te_data : Te_Probe#_Data
            Data object for the Te probe.
        analysis_index_range : np.array[int]
            2 element array of start and end time indeces to plot.
        optimized_vals : np.array[float]
            2D array of shape `(3, NUM_TIMES)` of optimized values for each 
            time in the `analysis_index_range`.
        val_stdevs : np.array[float]
            2D array of shape `(3, NUM_TIMES)` of standard deviations of 
            optimized values for each time in the `analysis_index_range`.
        iprobe_errors : np.array[float]
            2D array of shape `(NUM_TIPS, NUM_TIMES)` of errors on each I_probe 
            value for each time in the `analysis_index_range`.

        Notes
        -----
        `NUM_TIMES = analysis_index_range[1] - analysis_index_range[0]`
        """
        self.axis = axis
        self.start_data_index = analysis_index_range[0]

        # Set constant properties of the plot.
        self.axis.set_title("Shot {}: IV-curve".format(te_data.shot_number))
        self.axis.set_xlabel('vprobe (V)')
        self.axis.set_ylabel('iprobe (A)')

        vprobe_diff = np.max(te_data.vprobe) - np.min(te_data.vprobe)
        self.axis.set_xlim(np.min(te_data.vprobe) - vprobe_diff * 0.1, np.max(te_data.vprobe) + vprobe_diff * 0.1)
        iprobe_diff = np.max(te_data.iprobe) - np.min(te_data.iprobe)
        self.axis.set_ylim(np.min(te_data.iprobe) - iprobe_diff * 0.1, np.max(te_data.iprobe) + iprobe_diff * 0.1)

        # Get the IV points for the working and not-working points. These are arrays of (2 x # (not) working tips x # time slices) points.
        self.working_points = np.array((te_data.extract_working(te_data.vprobe), te_data.extract_working(te_data.iprobe)))
        self.not_working_points = np.array((te_data.extract_not_working(te_data.vprobe), te_data.extract_not_working(te_data.iprobe)))
        self.density, self.temperature, self.vplasma = optimized_vals
        self.optimized_value_stdevs = val_stdevs
        # Scale the relative errors so that they look nicer. As their magnitudes don't have exact physical meaning this is just fine.
        self.iprobe_rel_errors = iprobe_errors.T * 0.05 * (self.axis.get_ybound()[1] - self.axis.get_ybound()[0])

        self.te_data = te_data
        
        # Create background axis.
        self.axis.axvline(x=0, color='grey', linewidth=0.5)
        self.axis.axhline(y=0, color='grey', linewidth=0.5)

        # Initialize the curves.
        self.working_curve = self.axis.errorbar([0], [0], yerr=[0], fmt='o', marker='o', color='black', label='working', capsize=3, animated=True)
        self.not_working_curve = self.axis.scatter([], [], marker='x', color='red', label='not working', animated=True)
        # Initialize the fit curve to be all zeros at each point on the x axis.
        self.v_points = np.linspace(*self.axis.get_xbound(), num=200)
        self.fit_curve, = self.axis.plot(self.v_points, np.zeros_like(self.v_points), color='blue', animated=True)

        # Initialize the text.
        self.info_text = self.axis.text(0.05, 0.1, '', transform=self.axis.transAxes, animated=True)
        self.working_tip_annotations = []
        self.not_working_tip_annotations = []
        for tip_index in range(te_data.working.size):
            if te_data.working[tip_index]:
                self.working_tip_annotations.append(self.axis.text(0, 0, tip_index + 1, color='black', animated=True))
            else:
                self.not_working_tip_annotations.append(self.axis.text(0, 0, tip_index + 1, color='red', animated=True))

    def update(self, data_index):
        # Index to use for all data from the fit procedure.
        fit_data_index = data_index - self.start_data_index
        # Update the curves and text.
        update_errorbar(self.working_curve, self.working_points[0, :, data_index], self.working_points[1, :, data_index], yerr=self.iprobe_rel_errors[fit_data_index])
        self.not_working_curve.set_offsets(self.not_working_points[:, :, data_index].T)
        self.fit_curve.set_ydata(expected_current(self.v_points, self.density[fit_data_index], self.temperature[fit_data_index], self.vplasma[fit_data_index], self.te_data.area))
        self.info_text.set_text("time = {:.7f}\ndensity = {:.3e} +/- {:.1e}\ntemperature = {:.3e} +/- {:.1e}\nvplasma = {:.3e} +/- {:.1e}".format(
            round(self.te_data.time[data_index], 7), 
            self.density[fit_data_index], self.optimized_value_stdevs[0, fit_data_index],
            self.temperature[fit_data_index], self.optimized_value_stdevs[1, fit_data_index],
            self.vplasma[fit_data_index], self.optimized_value_stdevs[2, fit_data_index])
        )

        for i, point in enumerate(self.working_points[:, :, data_index].T):
            self.working_tip_annotations[i].set_position(point + np.array((0.003, 0)))
        for i, point in enumerate(self.not_working_points[:, :, data_index].T):
            self.not_working_tip_annotations[i].set_position(point + np.array((0.003, 0)))

        # Return all artists that have been updated.
        return extract_errorbar_artist_list(self.working_curve) + [self.not_working_curve, self.fit_curve, self.info_text] + self.working_tip_annotations + self.not_working_tip_annotations

class Voltage_Curve:
    def __init__(self, axis, te_data, analysis_index_range):
        self.axis = axis

        # Set constant properties of the plot.
        self.axis.set_title("Shot {}: Probe Voltage".format(te_data.shot_number))
        self.axis.set_xlabel("Time (s)")
        self.axis.set_ylabel("Voltage (V)")

        self.axis.set_xlim(te_data.time[0], te_data.time[-1])
        vprobe_diff = np.max(te_data.vprobe) - np.min(te_data.vprobe)
        self.axis.set_ylim(np.min(te_data.vprobe) - vprobe_diff * 0.1, np.max(te_data.vprobe) + vprobe_diff * 0.1)

        # Get the Voltage points for the working and not-working points.
        working_vprobe = te_data.extract_working(te_data.vprobe)
        not_working_vprobe = te_data.extract_not_working(te_data.vprobe)
        v_initial = te_data.vprobe[:, 0]

        self.te_data = te_data

        # Draw the curves.
        for v in working_vprobe:
            self.axis.plot(te_data.time, v, color='black')
        for v in not_working_vprobe:
            self.axis.plot(te_data.time, v, color='red')

        # Draw the ticks.
        self.axis.set_yticks(v_initial)
        self.axis.set_yticklabels(["Tip #{}: {} V".format(i + 1, round(v_initial[i])) for i in range(v_initial.size)])
        for i in range(v_initial.size):
            if te_data.working[i]:
                self.axis.get_yticklabels()[i].set_color('black')
            else:
                self.axis.get_yticklabels()[i].set_color('red')

        # Draw boxes over unused data.
        time_range = (self.te_data.time[analysis_index_range[0]], self.te_data.time[analysis_index_range[-1]])
        self.axis.axvspan(self.te_data.time[0], time_range[0], color='black', alpha=0.1)
        self.axis.axvspan(time_range[1], self.te_data.time[-1], color='black', alpha=0.1)
        
        # Initialize the vertical line of data currently being shown.
        self.time_text = self.axis.text(0.05, 0.9, '', transform=self.axis.transAxes, animated=True, bbox={'color': 'white'})
        self.time_line = self.axis.axvline(time_range[0])

    def update(self, data_index):
        self.time_text.set_text("time = {:.7f}".format(round(self.te_data.time[data_index], 7)))
        self.time_line.set_xdata(self.te_data.time[data_index])

        return [self.time_text, self.time_line]

class Bdot_Curve:
    def __init__(self, axis, te_data, analysis_index_range):
        self.axis = axis

        # Set constant properties of the plot.
        self.axis.set_title("Shot {}: Probe Bdot".format(te_data.shot_number))
        self.axis.set_xlabel("Time (s)")
        self.axis.set_ylabel("Bdot (uncalibrated)")

        bdot_magnitude = np.linalg.norm(te_data.bdot, ord=2, axis=0)
        self.axis.set_xlim(te_data.time[0], te_data.time[-1])
        bdot_diff = np.max(bdot_magnitude) - np.min(bdot_magnitude)
        self.axis.set_ylim(np.min(te_data.bdot) - bdot_diff * 0.1, np.max(bdot_magnitude) + bdot_diff * 0.1)

        self.te_data = te_data

        # Draw the curves.
        for i, bdot in enumerate(te_data.bdot):
            self.axis.plot(te_data.time, bdot, label='Axis {}'.format(i + 1))
        self.axis.plot(te_data.time, bdot_magnitude, color='black', label='Magnitude')

        # Draw boxes over unused data.
        time_range = (self.te_data.time[analysis_index_range[0]], self.te_data.time[analysis_index_range[-1]])
        self.axis.axvspan(self.te_data.time[0], time_range[0], color='black', alpha=0.1)
        self.axis.axvspan(time_range[1], self.te_data.time[-1], color='black', alpha=0.1)
        
        # Initialize the vertical line of data currently being shown.
        self.time_text = self.axis.text(0.05, 0.9, '', transform=self.axis.transAxes, animated=True, bbox={'color': 'white'})
        self.time_line = self.axis.axvline(time_range[0])

        self.axis.legend(loc='upper right')

    def update(self, data_index):
        self.time_text.set_text("time = {:.7f}".format(round(self.te_data.time[data_index], 7)))
        self.time_line.set_xdata(self.te_data.time[data_index])

        return [self.time_text, self.time_line]

class Param_Curve:
    def __init__(self, axis, te_data, analysis_index_range, optimized_vals, val_stdevs) -> None:
        self.axis = axis

        # Set constant properties of the plot.
        self.axis.set_title("Shot {}: Fit Parameters".format(te_data.shot_number))
        self.axis.set_xlabel("Time (s)")
        self.axis.set_ylabel("Normalized Parameters")

        # Normalize the values by their initial value.    
        self.normalization_factor = [np.mean(val) for val in optimized_vals]
        self.normalized_vals = [val / norm for val, norm in zip(optimized_vals, self.normalization_factor)]
        self.normalized_stdevs = [stdev / norm for stdev, norm in zip(val_stdevs, self.normalization_factor)]

        self.axis.set_xlim(te_data.time[0], te_data.time[-1])
        # Set the ylim to be proportional to the scale in the y axis.
        norm_diff = np.max(self.normalized_vals) - np.min(self.normalized_vals)
        self.axis.set_ylim(np.min(self.normalized_vals) - 0.1 * norm_diff, np.max(self.normalized_vals) + 0.1 * norm_diff)

        self.te_data = te_data

        # Draw the curves.
        colors = ['red', 'green', 'blue']
        for name, norm_val, norm_stdev, normalization, color in zip(['n', 'Te', 'Vp'], self.normalized_vals, self.normalized_stdevs, self.normalization_factor, colors):
            self.axis.fill_between(te_data.time[analysis_index_range], norm_val - norm_stdev, norm_val + norm_stdev, color=color, alpha=0.2)
            self.axis.plot(te_data.time[analysis_index_range], norm_val, label='{} (Normalized by {:.1e}'.format(name, normalization), color=color)

        # Draw boxes over unused data.
        time_range = (self.te_data.time[analysis_index_range[0]], self.te_data.time[analysis_index_range[-1]])
        self.axis.axvspan(self.te_data.time[0], time_range[0], color='black', alpha=0.1)
        self.axis.axvspan(time_range[1], self.te_data.time[-1], color='black', alpha=0.1)
        
        # Initialize the vertical line of data currently being shown.
        self.time_text = self.axis.text(0.05, 0.9, '', transform=self.axis.transAxes, animated=True, bbox={'color': 'white'})
        self.time_line = self.axis.axvline(time_range[0])

        self.axis.legend(loc='upper right')

    def update(self, data_index):
        self.time_text.set_text("time = {:.7f}".format(round(self.te_data.time[data_index], 7)))
        self.time_line.set_xdata(self.te_data.time[data_index])

        return [self.time_text, self.time_line]


def iterate_plots(te_data, analysis_time_range, optimized_vals, val_stdevs, iprobe_errors, view_time_range=None, save_path_iv=None, save_path_signals=None):
    """
    Iterate through plots of the IV curve and various parameters for the Te probe.

    Parameters
    ----------
    te_data : Te_Probe#_Data
    analysis_time_range : tuple[float]
        Times to iterate plots over.
    optimized_vals : np.array[float]
        `(3 x # of time slices)` array of optimized values for each time in the `time_range`.
    val_stdevs : np.array[float]
        `(3 x # of time slices)` array of standard deviations of optimized values for each time in the `time_range`.
    iprobe_errors : np.array[float]
        `(# of tips x # of time slices)` array of errors on each iprobe value for each time in the `time_range`.
    view_time_range : tuple[float] or None, default=None
        Time range to show when plotting. If `None` then will plot all data in `te_data` object.
    save_path_iv : str or None, default=None
        Path to save IV curve movie to. Default is don't save.
    save_path_signals : str or None, default=None
        Path to save signal curve movie to. Default is don't save.
    """
    # Time between frames.
    frame_interval = 200 # ms

    if view_time_range is None:
        cloned_te_data = te_data
    else:
        # Indeces for time points we want to view.
        view_index_range = np.nonzero(np.logical_and(te_data.time >= view_time_range[0], te_data.time <= view_time_range[1]))[0]
        
        # r_diggnd, r_div, r_sense, r_wire, a_rel, alternate_vp, tips_used = manual_get_data.load_parameters(all_tips=True)
        # V_b = te_data.vbias
        # v_s_data = np.array([manual_get_data.v_s(te_data, tip)[view_index_range] for tip in range(1, V_b.size + 1)])
        # v_n_data = np.array([manual_get_data.v_n(te_data, tip)[view_index_range] for tip in range(1, V_b.size + 1)])

        # # Create a new Te data object to hold a subset of the original data. We only want to use data in the view_time_range.
        # cloned_te_data = type(te_data)(te_data.shot_number, time_range=view_time_range)
        # cloned_te_data._vprobe = manual_get_data.v_p_func(v_s_data, v_n_data, r_diggnd, r_div, r_sense, r_wire, V_b, alternate_vp)
        # cloned_te_data._iprobe = manual_get_data.i_p_func(v_s_data, v_n_data, r_diggnd, r_div, r_sense, a_rel)
        # tip_valid = np.zeros_like(te_data.working)
        # tip_valid[tips_used - 1] = True
        # tip_valid = np.logical_or(te_data.working, tip_valid)
        # cloned_te_data._working = tip_valid
        # cloned_te_data._bdot = te_data.bdot[:, view_index_range]
        # cloned_te_data._area = te_data.area
        # cloned_te_data._vbias = V_b
        # cloned_te_data._time = te_data.time[view_index_range]
        
        # Create a new Te data object to hold a subset of the original data. We only want to use data in the view_time_range.
        cloned_te_data = type(te_data)(te_data.shot_number, time_range=view_time_range)
        cloned_te_data._iprobe = te_data.iprobe[:, view_index_range]
        cloned_te_data._vprobe = te_data.vprobe[:, view_index_range]
        cloned_te_data._working = te_data.working
        cloned_te_data._bdot = te_data.bdot[:, view_index_range]
        cloned_te_data._area = te_data.area
        cloned_te_data._vbias = te_data.vbias
        cloned_te_data._time = te_data.time[view_index_range]

    # Indeces of array that are in the passed time ranges.
    analysis_index_range = np.nonzero(np.logical_and(cloned_te_data.time >= analysis_time_range[0], cloned_te_data.time <= analysis_time_range[1]))[0]
    # IV Curve plotting
    iv_fig = plt.figure("IV Curve", figsize=(7, 9))
    iv_axis = iv_fig.add_subplot(111)

    logging.debug("Creating IV curve animation.")
    iv_curve = IV_Curve(iv_axis, cloned_te_data, analysis_index_range, optimized_vals, val_stdevs, iprobe_errors)
    iv_animation = FuncAnimation(iv_fig, iv_curve.update, analysis_index_range, blit=True, interval=frame_interval)

    # Signal plotting
    signal_fig = plt.figure("Signal", figsize=(7, 9))
    v_axis, bdot_axis, param_axis = signal_fig.subplots(3, 1, sharex=True)

    logging.debug("Creating V curve animation.")
    v_curve = Voltage_Curve(v_axis, cloned_te_data, analysis_index_range)
    v_animation = FuncAnimation(signal_fig, v_curve.update, analysis_index_range, blit=True, interval=frame_interval)
    logging.debug("Creating Bdot curve animation.")
    bdot_curve = Bdot_Curve(bdot_axis, cloned_te_data, analysis_index_range)
    bdot_animation = FuncAnimation(signal_fig, bdot_curve.update, analysis_index_range, blit=True, interval=frame_interval)
    logging.debug("Creating Optimized Parameter curve animation.")
    param_curve = Param_Curve(param_axis, cloned_te_data, analysis_index_range, optimized_vals, val_stdevs)
    param_animation = FuncAnimation(signal_fig, param_curve.update, analysis_index_range, blit=True, interval=frame_interval)

    moviewriter = FFMpegWriter(10**3 / frame_interval)
    if save_path_iv is not None:
        with moviewriter.saving(iv_fig, save_path_iv, dpi=300):
            for i in tqdm.tqdm(analysis_index_range, desc='IV Curve Movie Writer'):
                iv_curve.update(i)
                moviewriter.grab_frame()

    if save_path_signals is not None:
        with moviewriter.saving(signal_fig, save_path_signals, dpi=300):
            for i in tqdm.tqdm(analysis_index_range, desc='Signal Curve Movie Writer'):
                v_curve.update(i)
                bdot_curve.update(i)
                param_curve.update(i)
                moviewriter.grab_frame()

    plt.show()


if __name__ == "__main__":
    # logging.basicConfig(level=logging.DEBUG)
    shot_num = 59862
    full_time_range = (0.017995, 0.018030) # Time range to plot.
    # full_time_range = (0.01795, 0.01815)
    analysis_time_range = (0.018000, 0.018025) # Time range to get curves for.
    # analysis_time_range = (0.01799, 0.0181)
    # te_data = Te_Probe1_Data(shot_num)
    te_data = Te_Probe3_Data(60226)

    # Temporarily pretend that tips 5, 10, and 15 are bad.
    # TODO: Either recalibrate these tips or set them to not working in the mdsplus config.
    te_data.working
    # te_data._working[3] = False
    for tip in [11, 13, 15]:
        te_data._working[tip - 1] = False

    r_diggnd, r_div, r_sense, r_wire, a_rel, alternate_vp, tips_used = manual_get_data.load_parameters(
        filepath='../analysis_codes/data/te_probe3/optimized_vals_no_4_no_res_w_prior.mat', all_tips=True
    )
    tips_used_mask = np.zeros(r_diggnd.size, dtype=bool)
    tips_used_mask[tips_used - 1] = True
    te_data._working = np.logical_and(te_data._working, tips_used_mask)
    te_data._vprobe = np.array([manual_get_data.v_p(te_data, i + 1, r_diggnd[i], r_div[i], r_sense[i], r_wire[i], te_data.vbias[i], alternate_vp) for i in range(r_diggnd.size)])
    te_data._iprobe = np.array([manual_get_data.i_p(te_data, i + 1, r_diggnd[i], r_div[i], r_sense[i], a_rel[i]) for i in range(r_diggnd.size)])

    logging.debug("Fitting curves for IV curve.")
    optimized_vals, val_stdevs, iprobe_errors, optimization_error = get_all_fits(te_data, analysis_time_range=analysis_time_range, num_processes=2)
    logging.debug("Finished fitting IV curves.")

    # savemat('./notebooks/codes/te_probe3/paul_shot_60086_found_values.mat', {
    #     'n_values': optimized_vals[0],
    #     'Te_values': optimized_vals[1],
    #     'Vplasma_values': optimized_vals[2],
    #     'n_standard_deviation': val_stdevs[0],
    #     'Te_standard_deviation': val_stdevs[1],
    #     'Vplasma_standard_deviation': val_stdevs[2],
    # })
    iterate_plots(te_data, analysis_time_range, optimized_vals, val_stdevs, iprobe_errors, view_time_range=full_time_range, )
        # save_path_iv='./notebooks/codes/te_probe3/paul_shot_60086_iv.mp4',
        # save_path_signals='./notebooks/codes/te_probe3/paul_shot_60086_signals.mp4')
