from modules.generic_get_data import Get, Data, lazy_get
import numpy as np
from scipy.special import ellipk, ellipe
from scipy.integrate import quad
from numbers import Number


class Helmholtz_Data(Data):
    def __init__(self, tree, *args, **kwargs):
        super().__init__(tree, [], [], *args, **kwargs)

    @lazy_get
    def v(self):
        """
        Power supply voltage mean.
        
        Returns
        -------
        float
            Units of Volts.
        """
        return np.mean(self.v_curve)

    @lazy_get
    def i(self):
        """
        Power supply current mean.
        
        Returns
        -------
        float
            Units of Amps.
        """
        return np.mean(self.i_curve)

    @lazy_get
    def b_h(self):
        """
        Returns
        -------
        float
            Units of Teslas
        """
        return 0.3521 * self.i * 10**-4

    @staticmethod
    def coil_locations(insert=False):
        """
        The r and z locations of the coils.
        
        Parameters
        ----------
        insert : bool, default=False
            Whether the insert in in the machine which means the helmholtz coils are slightly further apart.

        Returns
        -------
        coil_r_locations, coil_z_locations : np.array[float]
            1D arrays of coil position in meters.
        """
        # TODO: Change the storage of these parameters to somewhere like MDSplus or MySQL?
        in_to_m = 0.0254
        total_coil_z = 86.23 / 2 * in_to_m
        if insert:
            total_coil_z += 3.88 / 2 * in_to_m

        total_coil_r = 157.3 / 2 * in_to_m

        individual_coil_dl = 0.55 * in_to_m # Each coil is square so this is the side length.

        num_r_coils = 5
        num_z_coils = 16

        coil_r_vals = np.linspace(total_coil_r + individual_coil_dl / 2, total_coil_r + individual_coil_dl * (num_r_coils + 0.5), num=num_r_coils)
        coil_z_vals = np.linspace(total_coil_z + individual_coil_dl / 2, total_coil_z + individual_coil_dl * (num_z_coils + 0.5), num=num_z_coils)

        coil_z_vals = np.concatenate((-coil_z_vals, coil_z_vals))

        coil_r_locations, coil_z_locations = np.meshgrid(coil_r_vals, coil_z_vals)
        # Add additional half turns that connect each coil pancake.
        coil_r_locations = np.append(coil_r_locations.flatten(), (total_coil_r + individual_coil_dl * (num_r_coils + 1.5)) * np.ones(num_z_coils // 2))
        coil_z_locations = np.append(coil_z_locations.flatten(), np.linspace(total_coil_z + individual_coil_dl, total_coil_z + individual_coil_dl * num_z_coils, num=num_z_coils // 2))
        # Add opposite side.
        coil_r_locations = np.append(coil_r_locations, (total_coil_r + individual_coil_dl * (num_r_coils + 1.5)) * np.ones(num_z_coils // 2))
        coil_z_locations = np.append(coil_z_locations, -np.linspace(total_coil_z + individual_coil_dl, total_coil_z + individual_coil_dl * num_z_coils, num=num_z_coils // 2))

        return coil_r_locations, coil_z_locations

    def analytic_b_h(self, r, z, insert=False):
        """Analytic magnetic field from helmholtz coils.
        
        Parameters
        ----------
        r, z : float or np.array
            Position in meters to get the magnetic field at. `z` is measured from the equator.
            The arrays should be 1D. If only one is a float then it is assumed a single `r` or `z` value is used for all locations.
        insert : bool, default=False TODO: Change this so we load it in from MySQL or MDSplus
            Whether the insert in in the machine which means the helmholtz coils are slightly further apart.
        
        Returns
        -------
        b_h_r, b_h_z : float or np.array
            Field strength in Tesla of helmholtz field in r and z directions.
        """
        coil_r_locations, coil_z_locations = self.coil_locations(insert)

        was_float = False
        array_shape = (1,)
        if isinstance(r, Number) and isinstance(z, Number):
            was_float = True
            r = np.array([r])
            z = np.array([z])
        elif isinstance(z, np.ndarray) and isinstance(r, Number):
            r = r * np.ones_like(z)
            array_shape = z.shape
        elif isinstance(r, np.ndarray) and isinstance(z, Number):
            z = z * np.ones_like(r)
            array_shape = r.shape
        elif r.shape != z.shape:
            raise ValueError("`r` and `z` don't have same shape. Can not calculate the analytic B_H value.")
        else:
            array_shape = z.shape

        # Force the r values to always be positive.
        r = np.abs(r)

        # Calculate the magnetic field using the equations given by https://tiggerntatie.github.io/emagnet/offaxis/iloopoffaxis.htm
        b_h_r = np.zeros(array_shape)
        b_h_z = np.zeros(array_shape)

        b_h_r, b_h_z = self._analytic_magnetic_field(r.flatten(), z.flatten(), coil_r_locations.flatten(), coil_z_locations.flatten(), self.i)
        b_h_r = b_h_r.reshape(array_shape)
        b_h_z = b_h_z.reshape(array_shape)

        if was_float:
            return b_h_r[0], b_h_z[0]
        else:
            return b_h_r, b_h_z

    @staticmethod
    def _analytic_magnetic_field(r, z, coil_r, coil_z, current):
        if not isinstance(coil_r, np.ndarray):
            if isinstance(coil_r, Number):
                coil_r = np.array([coil_r])
            else:
                coil_r = np.array(coil_r)
        if not isinstance(coil_z, np.ndarray):
            if isinstance(coil_z, Number):
                coil_z = np.array([coil_z])
            else:
                coil_z = np.array(coil_z)
        
        if len(coil_r.shape) > 1:
            raise ValueError("Can only get magnetic field using float or 1D array for `coil_r`/`coil_z`.")
        elif coil_r.shape != coil_z.shape:
            raise ValueError("The r and z arrays must have the same shape ({} and {} respectively).".format(coil_r.shape, coil_z.shape))

        if isinstance(current, Number):
            current = current * np.ones_like(coil_r)
        elif current.shape != coil_r.shape:
            raise ValueError("`current` must have same shape as the coil locations or must be a float.")

        mu_0 = 4 * np.pi * 10**-7
        
        b_r = np.where(r == 0, 0, 
            np.sum(current[None, :] * mu_0 * (z[:, None] - coil_z[None, :]) * (
                (coil_r[None, :]**2 + r[:, None]**2 + (coil_z[None, :] - z[:, None])**2) * 
                ellipe(4 * (coil_r[None, :] * r[:, None] / ((coil_r[None, :] + r[:, None])**2 + (coil_z[None, :] - z[:, None])**2))) / 
                ((coil_r[None, :] - r[:, None])**2 + (coil_z[None, :] - z[:, None])**2) -
                ellipk(4 * (coil_r[None, :] * r[:, None] / ((coil_r[None, :] + r[:, None])**2 + (coil_z[None, :] - z[:, None])**2)))
            ) / (2 * coil_r[None, :] * np.pi * r[:, None] * (((coil_r[None, :] + r[:, None])**2 + (coil_z[None, :] - z[:, None])**2) / coil_r[None, :]**2)**0.5), axis=1)
        )
        b_z = np.sum(current[None, :] * mu_0 * (
            (coil_r[None, :]**2 - r[:, None]**2 - (coil_z[None, :] - z[:, None])**2) * 
            ellipe(4 * (coil_r[None, :] * r[:, None] / ((coil_r[None, :] + r[:, None])**2 + (coil_z[None, :] - z[:, None])**2))) / 
            ((coil_r[None, :] - r[:, None])**2 + (coil_z[None, :] - z[:, None])**2) +
            ellipk(4 * (coil_r[None, :] * r[:, None] / ((coil_r[None, :] + r[:, None])**2 + (coil_z[None, :] - z[:, None])**2)))
        ) / (2 * coil_r[None, :] * np.pi * (((coil_r[None, :] + r[:, None])**2 + (coil_z[None, :] - z[:, None])**2) / coil_r[None, :]**2)**0.5), axis=1)

        return b_r, b_z

    @lazy_get
    def v_curve(self):
        """
        Power supply voltage curve.
        
        Returns
        -------
        np.array
            Units of Volts.
        """
        if self.shot_number > 62826:
            channel = '\\top.raw.a373_direct:ch_44'
        else:
            channel = '\\top.raw.a470_direct:ch_44'
        return 50 * self.get(Get(channel, name='helmholtz_v_uncalibrated'))

    @lazy_get
    def i_curve(self):
        """
        Power supply current curve.
        
        Returns
        -------
        np.array
            Units of Amps.
        """
        if self.shot_number > 62826:
            channel = '\\top.raw.a373_direct:ch_43'
        else:
            channel = '\\top.raw.a470_direct:ch_43'
        return 200 * self.get(Get(channel, name='helmholtz_i_uncalibrated'))

    @lazy_get
    def time(self):
        """
        Time points for curves.
        
        Returns
        -------
        np.array
            Units of seconds.
        """
        return self.get(Get('DIM_OF(\\top.raw.a470_direct:ch_43)', name='helmholtz_time'))


if __name__=="__main__":
    import matplotlib.pyplot as plt
    import numpy as np

    for insert in [False, True]:
        helm = Helmholtz_Data(57952)

        plot_r_slice = False
        plot_z_slice = False
        plot_2D = True
        plot_coil_locations = False

        if plot_r_slice:
            z = 0
            r = np.linspace(-1, 1)

            b_r, b_z = helm.analytic_b_h(r, z, insert)

            plt.figure()
            plt.plot(r, b_r, label=r'$B_r$')
            plt.plot(r, b_z, label=r'$B_z$')
            plt.legend()
            plt.xlabel(r'r')
            if insert:
                plt.title("R slice w/ insert")
            else:
                plt.title("R slice")
            plt.show(block=False)
        if plot_z_slice:
            z = np.linspace(-1, 1)
            r = 0

            b_r, b_z = helm.analytic_b_h(r, z, insert)

            plt.figure()
            plt.plot(z, b_r, label=r'$B_r$')
            plt.plot(z, b_z, label=r'$B_z$')
            plt.legend()
            plt.xlabel(r'z')
            if insert:
                plt.title("Z slice w/ insert")
            else:
                plt.title("Z slice")
            plt.show(block=False)
        if plot_2D:
            num_r = 50
            num_z = 50
            r, dr = np.linspace(-0.8, 0.8, num=num_r, retstep=True)
            z, dz = np.linspace(-0.8, 0.8, num=num_z, retstep=True)
            r_vals, z_vals = np.meshgrid(r, z)

            b_r, b_z = helm.analytic_b_h(r_vals, z_vals, insert)
            b_mag = (b_r**2 + b_z**2)**0.5

            b_r_at_center, b_z_at_center = helm.analytic_b_h(0, 0, insert)
            b_mag_at_center = (b_r_at_center**2 + b_z_at_center**2)**0.5

            plt.figure()
            # plt.pcolormesh(np.linspace(z[0] - dz / 2, z[-1] + dz / 2, num=num_z + 1), np.linspace(r[0] - dr / 2, r[-1] + dr / 2, num=num_r + 1), b_mag)
            plt.pcolormesh(r, z, b_mag, shading='gouraud')
            plt.colorbar()
            plt.contour(r, z, b_mag, colors='black')
            if insert:
                plt.title(r'$B_{{mag}}$ w/ insert ({} T at center)'.format(round(b_mag_at_center, 5)))
            else:
                plt.title(r'$B_{{mag}}$ ({} T at center)'.format(round(b_mag_at_center, 5)))
            plt.xlabel(r'$r$')
            plt.ylabel(r'$z$')
            plt.show(block=False)
        if plot_coil_locations:
            coil_rs, coil_zs = helm.coil_locations(insert)

            plt.figure()
            plt.scatter(coil_rs, coil_zs, marker='s')
            plt.axis('equal')
            plt.xlabel(r'$r$')
            plt.ylabel(r'$z$')
            plt.show(block=False)

    plt.show()
