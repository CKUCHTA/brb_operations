% load Flux Array data from dtacs 323 324 325 326 116 128 129 and 320. Channels from the dtaq's 
% are assigned to thier associated probes, and corrections are made to
% probes with funky signals. NO HELMHOLTZ field is added to the B-field! 
% Created 20Nov2023


function [dBSetRaw, SampleRange, Bh, tpoints] = load3DFarray(daq, Bh, dt) 
% clear
% shot = 64943;
% SamRange=69000:73000;
% IIm=940:1040; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% dt=1e-7; % time between samples
% IIoff=1000; %Makes Bz=Bh at this index
% saveData = 1;
% [daq, Bh] = loadDtaqData3DFarray(shot, saveData, SamRange); % load dtac from harddrive or from mdsplus

PlotRaw = 1; % change to 1 for plot of digitizer data
xlims = [1800 2200];

% determine SampleRange size using first daq entry
daqField = fieldnames(daq);
daq1 = daq.(daqField{1});
SampleRange = 1:size(daq1,1);

tpoints = (SampleRange(1):SampleRange(end))*dt*1e6; % global time array in ms

% each Board corrisponds to the same stick on the array
board1a = daq.a323(SampleRange,1:16); 
board1b = daq.a323(SampleRange,17:32);
board2a = daq.a324(SampleRange,1:16);
board2b = daq.a324(SampleRange,17:32);

board3a = daq.a325(SampleRange,1:16);
board3b = daq.a325(SampleRange,17:32);
board4a = daq.a326(SampleRange,1:16);  
board4b = daq.a326(SampleRange,17:32);

board5a = daq.a116(SampleRange,1:16);
board5b = daq.a116(SampleRange,17:32);
board6a = daq.a128(SampleRange,1:16);
board6b = daq.a128(SampleRange,17:32);

board7a = daq.a320(SampleRange,1:16);
board7b = daq.a320(SampleRange,17:32);
board8a = daq.a129(SampleRange,1:16);
board8b = daq.a129(SampleRange,17:32);

boards = {board1a board1b board2a board2b board3a board3b board4a board4b...
    board5a board5b board6a board6b board7a board7b board8a board8b};

titleName = {'1a' '1b' '2a' '2b' '3a' '3b' '4a' '4b' '5a' '5b' '6a' '6b'...
    '7a'  '7b' '8a' '8b'};

if PlotRaw == 1
    figure(81), clf
    for i = 1:8
        subplot(2,4,i)
        
        for ch = 1:16
            plot(boards{i}(:,ch)+0.1*ch)
            hold on
            title(sprintf('Board %s',titleName{i}))
        end
    end
    
    figure(82), clf
    k = 1;
    for i = 9:16
        subplot(2,4,k)
        
         for ch = 1:16
            plot(boards{i}(:,ch)+0.1*ch)
            hold on
            title(sprintf('Board %s',titleName{i})) 
        end
        k = k+1;
    end

end


% stick1 furthest into the vessel when installed on the pole
stick1 = [board1b board1a];
stick2 = [board2b board2a];
stick3 = [board3b board3a];
stick4 = [board4b board4a];
stick5 = [board5b board5a];
stick6 = [board6b board6a];
stick7 = [board7b board7a];
stick8 = [board8b board8a];

sticks = cat(3,stick1, stick2, stick3, stick4, stick5, stick6, stick7, stick8);

dBSetR = nan(size(sticks,1),10, 8);
dBSetP = nan(size(sticks,1),10, 8);
dBSetZ = nan(size(sticks,1),12, 8);



% Sticks 1 thru 8. Channels ordered from furthest to closest to manifold
for k = 1:8
    if k == 7 % channels 7 and 21 swapped
        dBSetR(:,:,k) = [sticks(:,31:(-2):23,k)... 
            sticks(:,7,k) sticks(:,19:(-2):13,k)];
        dBSetP(:,:,k) = sticks(:,32:(-2):14,k);
        dBSetZ(:,:,k) = [sticks(:,12:(-1):8,k)...
            sticks(:,21,k) sticks(:,6:(-1):1,k)];
    elseif k == 8 % channels 14 and 29 swapped
        dBSetR(:,:,k) = [sticks(:,31,k) sticks(:,14,k)... 
            sticks(:,27:(-2):13,k)];
        dBSetP(:,:,k) = [sticks(:,32:(-2):16,k) sticks(:,29,k)];
        dBSetZ(:,:,k) = sticks(:,12:(-1):1,k);
    else
        dBSetR(:,:,k) = sticks(:,31:(-2):13,k);
        dBSetP(:,:,k) = sticks(:,32:(-2):14,k);
        dBSetZ(:,:,k) = sticks(:,12:(-1):1,k);
    end
end


% Fix Bad Probes
% Stick 2
dBSetR(:,1,2) = (dBSetR(:,1,1) + dBSetR(:,2,2))/2;
dBSetP(:,9,2) = (dBSetP(:,8,2) + dBSetP(:,10,2))/2;

% Stick 4
dBSetP(:,1,4) = (dBSetP(:,1,3) + dBSetP(:,2,4))/2;

% Stick 5
dBSetP(:,4,5) = (dBSetP(:,5,6) + dBSetP(:,3,5))/2;
dBSetP(:,5,5) = (dBSetP(:,4,6) + dBSetP(:,6,5))/2;
dBSetP(:,8,5) = (dBSetP(:,7,5) + dBSetP(:,9,5))/2;

dBSetR(:,9,5) = (dBSetR(:,10,4) + dBSetR(:,8,5))/2;

% Stick 6
dBSetR(:,2,6) = (dBSetR(:,1,6) + dBSetR(:,3,6))/2;
dBSetP(:,2,6) = (dBSetP(:,3,6) + dBSetP(:,1,6))/2;

% Stick 8
dummy = dBSetR(:,10,8);
dBSetR(:,10,8) = dBSetP(:,10,8);
dBSetP(:,10,8) = dummy;
dBSetP(:,1,8) = (dBSetP(:,2,8) + dBSetP(:,1,7))/2;

dBSetR_raw = dBSetR;
dBSetP_raw = dBSetP;
dBSetZ_raw = dBSetZ;

dBSetRaw = {dBSetR_raw dBSetP_raw dBSetZ_raw};
direction = {'R' 'P' 'Z'};

% Raw Data Plots Orginized by Direction
if PlotRaw == 1
    for i = 1:3
        figure(90+i), clf
        sgtitle(sprintf('%s-direction', direction{i}))
        for j = 1:8
            for ch = 1:size(dBSetRaw{i}(:,:,j),2)
                subplot(2,4,j)
                plot(dBSetRaw{i}(:,ch,j)+0.1*ch)
                hold on
                xlim(xlims)
                title(sprintf('Stick %d', j))
            end
        end
    end
end

