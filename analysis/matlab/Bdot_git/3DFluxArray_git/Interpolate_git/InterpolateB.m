% This function is used with InterpByEye3DFarrayZdir.m.  It takes 
% PoinUse from InterpByEye3DFarrayZdir.m and uses it to interpolate B and dB.
% Modified 20Dec23

function [BintTot2, dBintTot2] = InterpolateB(shot, B, dB, Bh, GUI_onOFF,...
     FirstPoint, ylims, facClim, CheckPlots, UseOldPoints, ProbeType, Folder)
% clear
% shot = 62271;
% ProbeType = 2; % 1 for 1D flux array. 2 for 3D flux array
% GUI_onOFF = 1; %turn GUI on/off
% CheckPlots = 0; % turn check plots on/off98;  %
% FirstPoint = 1050;
% ylims = [1000, 1380]; 
% facClim = 0.7;
% UseOldPoints = 0;
% [Br, Bp, Bz, dBr, dBp, dBz, Bh, smoothBzZeroIndx, smoothdBzMinIndx] = reorderB(shot,ProbeType, UseOldPoints);
% B = Br;
% dB = dBr;
% Folder = 'DipoleCusp2024';

ProbeNum = size(B, 3);

% load points from z direction 
try
    if ProbeType == 1
        ShotFolder = '1DFarray';
    elseif ProbeType == 2
        ShotFolder = '3DFarray';
    end
    DataStructure = what(Folder);
    DataFolder = DataStructure.path;
   
    PointsUsedFile = fullfile(PointsUsedFolder, sprintf('Scripts/%d_PointsUsed',shot));
    load(PointsUsedFile, 'PoinUse')
    for k = 1:size(PoinUse,2)
        PoinUse{k} = PoinUse{k}(:,1:ProbeNum);
    end
    
catch
end

% PoinUse is the variable that contains lines for interolating flux
First=0;
if exist('PoinUse')==0 % if PoinUse doesn't exist, load zeros into variable
    PoinUse = cell([1,ProbeNum]);
    for Rprobe=1:ProbeNum % number of R probes
        PoinUse{Rprobe}=0; % load zeros into variable for firts run
    end
    First=1;
end

if GUI_onOFF==1 % turns on GUI for fitting by eye
    BintTot1 = cell([1,8]); 
    dBintTot1 = cell([1,8]); 
    for stick=1:8 % Interpolate B and dB for each stick
        
        PoinIn=PoinUse{stick}; %
        if and(First,stick>1) % if this is the first run and stick>1, use PoinUse from previous stick
            PoinIn=PoinUse{stick-1}; 
        end
        
        % Fields for the kth stickpp
        BInput = squeeze(B(:,stick,:));
        dBInput = squeeze(dB(:,stick,:));

        [Poin, Bint, dBint]=dolinesFarray(PoinIn,BInput,dBInput,Bh,stick,ylims, facClim, FirstPoint, UseOldPoints); % calls GUI
        PoinUse{stick}=Poin; % points chosen by eye

        % interpolated B found after implementing GUI
        BintTot1{stick}=Bint; 
        dBintTot1{stick}=dBint;
        
    end
end


%%

PoinUse2 = cell([1,ProbeNum]); 
for Rprobe=1:ProbeNum
    for stick=1:8
        Poin1=PoinUse{stick};
        Poin2(:,stick)=Poin1(:,Rprobe); % flip Poin1 to be in lightsaber format
        SamRange=find(Poin2(:,stick)>FirstPoint); % find the contour lines that were used after time 1050
        Poin2(SamRange,stick)=sort(Poin2(SamRange,stick)); % sorts lines such that they are in order
    end
                
    PoinUse2{Rprobe}=Poin2;
end
%%  

% plotting output from dolinesFarray to confirm fits were good
if CheckPlots == 1
    figure(101),clf
    for k=1:8
        H2(k)=subplot(2,4,k);
    end

    for k=1:8
        axes(H2(k))
        pcolor(squeeze(dBz(:,k,:))), shading interp
        hold on
        plot(PoinUse{k}')
        caxis([-1 1]*9e3)
        ylim(ylims)
    end    

    figure(102),clf
    for k=1:ProbeNum
        H(k)=subplot(3,4,k);
    end
    for k=1:ProbeNum
        axes(H(k))
        pcolor(squeeze(dBz(:,:,k))), shading interp
        caxis([-1 1]*9e3)
        hold on
        plot(PoinUse2{k}')
        ylim(ylims)
    end

    figure(101),clf
    for k=1:8
        H2(k)=subplot(2,4,k);
    end

    for k=1:8
        axes(H2(k))
        pcolor(squeeze(dBz(:,k,:))), shading interp
        hold on
        plot(PoinUse{k}')
        caxis([-1 1]*9e3)
        ylim(ylims)
    end    

    figure(102),clf
    for k=1:ProbeNum
        H(k)=subplot(3,4,k);
    end
    for k=1:ProbeNum
        axes(H(k))
        pcolor(squeeze(dBz(:,:,k))), shading interp
        caxis([-1 1]*9e3)
        hold on
        plot(PoinUse2{k}')
        ylim(ylims)
    end
end
%%

% change format of data such that it matches light saber probe and redu
% GUI to get output in lighsaber format

BintTot2 = cell([1,ProbeNum]);
dBintTot2 = cell([1,ProbeNum]);
PoinUse3 = cell([1,ProbeNum]); 

% build B field array along z-axis by using probes from every stick
for Rprobe=1:ProbeNum 
    PoinIn=PoinUse2{Rprobe};
    BInput = squeeze(B(:,:,Rprobe));
    dBInput = squeeze(dB(:,:,Rprobe));

    [Poin, Bint, dBint]=dolinesFarray(PoinIn,BInput,dBInput,Bh,Rprobe, ylims, facClim, FirstPoint, UseOldPoints);
    %%
    for stick=1:8
        PoinInt(stick,:)=4*(interp1(1:8,Poin(stick,:),1:0.25:8)-1)+1;
    end
    PoinInt=smoo(PoinInt',3)'; % Do I need this if I'm using PoinInt from Z direction?!!!!!!!
    plot(PoinInt')
    %%
    PoinUse3{Rprobe}=PoinInt;
    BintTot2{Rprobe}=Bint;
    dBintTot2{Rprobe}=dBint;
end

%%   

% Plot outputs from dolinesFarray
if CheckPlots == 1
    figure(103),clf
    for k=1:ProbeNum
        H(k)=subplot(3,4,k);
    end

    for k=1:ProbeNum
        axes(H(k))
        pcolor(dBintTot2{k}), shading interp
        caxis([-1 1]*9e3)
        hold on
        plot(PoinUse3{k}')
        ylim(4*ylims)
    end  


    %%   
    figure(104),clf
    for k=1:ProbeNum
        H(k)=subplot(3,4,k);
    end

    for k=1:ProbeNum
        axes(H(k))
        pcolor(BintTot2{k}), shading interp
        caxis([-1 1]*Bh*1.6)
        hold on
        plot(PoinUse3{k}')
        ylim(4*ylims)
    end 
end
