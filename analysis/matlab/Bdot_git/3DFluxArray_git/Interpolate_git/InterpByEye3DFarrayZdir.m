% This script uses the GUI from dolinesFarray to set points along features 
% for a flux array shot. The points that are set by hand form a corrdinate 
% system that allows the matlab function interp1, which is called with 
% doInter, to interpolate B and dB.  The interpolated fields are dBzIntTot2 
% and BzIntTot2 and saved in the scripts folder. The points used to generate 
% the coordinate system are PoinUse and PoinUse3.  There are two sets of
% point due to the script creating a set for each stick and a set for each
% row of probes along the z-axis.  The first set of points is generated
% with the plots in an orintation that is easiest to identify features, the
% second set is generated from the first and is best for creating movies.
% Modified 20Dec23

function InterpByEye3DFarrayZdir(shot, ProbeType, GUI_onOFF, SkipThruGUI, SaveData, UseOldPoints,...
    CheckPlots_Zdir, Folder, SamRange,IIm, IIoff, facClim, dt, ylims, FirstPoint, LastPoint, dBzLastPoint)
% clear
% ProbeType = 1; % 1 for 1D flux array. 2 for 3D flux array
% GUI_onOFF = 1; % 1 to turn GUI on
% SaveData = 1; % 1 to save data to harddrive
% UseOldPoints = 0; % Use Saved points
% SkipThruGUI = 1; % 1 for auto clicking through GUI 
% CheckPlots_Zdir = 0; % 1 to plot Bz and dBz for each stick
% shot = 64944;
% % shot = 62259;
% Folder = 'DriveCylinderCalibration';

%TREX Coils Perameters
% SamRange = 69000:71000;
% IIm = 901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% IIoff = 1000; %Makes Bz=Bh at this index
% facClim = 0.1; % multiplies range of clim
% dt=1e-7; % time between samples
% ylims = [1000, 1380]; 
% FirstPoint = 1050; % time fitlines begin
% LastPoint = 1350; % time fitlines end
% dBzLastPoint = 1200;

% Dipole Perameters
% SamRange = 69000:73000;
% IIm=901:980; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% IIoff=1020;
% facClim = 0.1; % multiplies range of clim
% dt=1e-7; % time between samples
% ylims = [1800, 2200]; 
% FirstPoint = ylims(1) + 50; % time fitlines begin
% LastPoint = ylims(2)-50; % time fitlines end
% dBzLastPoint = ylims(2)-100;



if ProbeType == 1
    ShotFolder = '1DFarray';

elseif ProbeType == 2
    ShotFolder = '3DFarray';
end
DataStucture = what(Folder);
DataFolder = DataStucture.path;  

PointsUsedFolder = fullfile(DataFolder,ShotFolder,'Matlab_Data','PointsUsed');
if ~exist(PointsUsedFolder, 'dir')
    mkdir(PointsUsedFolder)
end

InterpFolder = fullfile(DataFolder, ShotFolder, 'Matlab_Data', 'InterpData');
if ~exist(InterpFolder,'dir')
    mkdir(InterpFolder)
end

IntBdataFile = fullfile(InterpFolder, sprintf('%d_IntBdata',shot));
PointsUsedFile = fullfile(PointsUsedFolder, sprintf('%d_PointsUsed',shot));



if ProbeType == 1
    [daq, Bh] = loadDtaqDataFarray(shot, 0, SamRange); % load daq data from harddrive or from mdsplus
    [dBSetRaw, ~, Bh, tpoints] = loadFarray(shot, daq, Bh, dt); 
    [BSet, dBSet, Bh, ~, tpoints] = Calibrate1DFluxArray(IIoff, IIm, dt, dBSetRaw, Bh, tpoints, 0);
elseif ProbeType == 2
    [daq, Bh] = loadDtaqData3DFarray(shot, 0, SamRange); % load daq data from harddrive or from mdsplus
    [dBSetRaw, ~, Bh, tpoints] = load3DFarray(daq, Bh, dt);
    [BSet, dBSet, ~, Bh] = calibrate3DFarray(IIoff, IIm, dt, dBSetRaw, Bh);
    % [BSet, dBSet, ~ , ~, Bh, tpoints] = load3DFarray(daq, Bh, SamRange, dt, IIm, IIoff);
end

%Convert Cell frormat from (time, r, z) to (time, z, r)
if ProbeType == 1
    dBz=zeros([length(tpoints) , 8,12]);
    Bz=zeros([length(tpoints) , 8,12]);
    for k=1:8 
        dBz(:,k,:)=dBSet{k,3};
        Bz(:,k,:)=BSet{k,3};
    end
elseif ProbeType == 2
    Br = permute(BSet{1},[1,3,2]);
    Bp = permute(BSet{2},[1,3,2]);
    Bz = permute(BSet{3},[1,3,2]);
    
    dBr = permute(dBSet{1},[1,3,2]);
    dBp = permute(dBSet{2},[1,3,2]);
    dBz = permute(dBSet{3},[1,3,2]);
end

% Reorder probes so lowest probe number is closest to the manifold
Bz=Bz(:,:,end:(-1):1);
dBz=dBz(:,:,end:(-1):1);
if ProbeType == 2
    Br=Br(:,:,end:(-1):1);
    dBr=dBr(:,:,end:(-1):1);
    
    Bp=Bp(:,:,end:(-1):1);
    dBp=dBp(:,:,end:(-1):1);
end

% find time index for first Bz = 0 and minimum dBz in each stick
BzZeroIndx = zeros(8,12);
dBzMinIndx = zeros(8,12);
dBzLimit = dBz(FirstPoint:dBzLastPoint,:,:);
for stick = 1:8
    for probe = 1:12
        BzTest = Bz + Bh;
        testBz = find(BzTest(FirstPoint:LastPoint,stick,probe)< 0,1); % If sign of Bh changes then change < to >!!!!!!
        testdBz = find(dBzLimit(:,stick,probe)==min(dBzLimit(:,stick,probe)),1);
       
        if testdBz < 10
            if probe == 1
                dBzMinIndx(stick,probe) = FirstPoint;
            else
                dBzMinIndx(stick,probe) = dBzMinIndx(stick,probe-1); 
            end
        else
        dBzMinIndx(stick,probe) = testdBz+FirstPoint;
        end
        
        if isempty(testBz)
            if probe == 1
                BzZeroIndx(stick,probe) = FirstPoint;
            else
                BzZeroIndx(stick,probe) = BzZeroIndx(stick,probe-1); 
            end
        else
        BzZeroIndx(stick,probe) = testBz+FirstPoint;
        end 
    end
end
smoothBzZeroIndx = zeros(size(BzZeroIndx));
smoothdBzMinIndx = zeros(size(dBzMinIndx));
for index = 1:size(BzZeroIndx,1)
    smoothBzZeroIndx(index,:) = smoo(BzZeroIndx(index,:),3);
    smoothdBzMinIndx(index,:) = smoo(dBzMinIndx(index,:),3);
end
% dBzMaxIndx = max(BzZeroIndx,[],2); 

%%%%%% Interpolation by Eye Starts %%%%%%%%
% PoinUse is the variable that contains points for generating a 
% coordinate sytem for interolating field data

if UseOldPoints == 1
    try
        load(PointsUsedFile, 'PoinUse');  % load points used to interpolate B
    catch
    end
end

StartPointsArray = ones(size(BzZeroIndx))*FirstPoint; % set start points for each stick
First=0;
if exist('PoinUse')==0 % if PoinUse does not exist load zeros into variable
    PoinUse = cell([1,12]); 
    for k=1:12
        if k<9
            StartPointsArray(2,:) = smoothBzZeroIndx(k,:); % replace Bz = 0 times with initial start point
            % StartPointsArray(1,:) = smoothdBzMinIndx(k,:); % replace dBz max times with initial start point  
            PoinUse{k} = StartPointsArray; 
        else
            PoinUse{k} = 0;
        end
    end
    % First=1;
end

if GUI_onOFF==1 % turns on GUI for choosing points 
    BzIntTot1 = cell([1,8]); 
    dBzIntTot1 = cell([1,8]); 
    %for k=1:12
    for k=1:8 % Interpolate B and dB for each stick
        PoinIn=PoinUse{k};
        if and(First,k>1) % if First=1 and k>1 use points from previous stick
            PoinIn=PoinUse{k-1};
        end
        
        % Fields for the kth stick
        BzIn=squeeze(Bz(:,k,:));
        dBzIn=squeeze(dBz(:,k,:));
    
        [Poin, BzInt, dBzInt]=dolinesFarray(PoinIn,BzIn,dBzIn,Bh,k,ylims, facClim, FirstPoint, SkipThruGUI); % calls GUI
        PoinUse{k}=Poin; % points chosen by eye

        % interpolated B found after implementing GUI
        BzIntTot1{k}=BzInt; 
        dBzIntTot1{k}=dBzInt;
    end
end
    
%%

PoinUse2 = cell([1,12]); 
for k=1:12
    for l=1:8
        Poin1=PoinUse{l};
        Poin2(:,l)=Poin1(:,k); % flip Poin1 to be in lightsaber format
        SamRange=find(Poin2(:,l)>FirstPoint); % find the contour lines that were used after time 1050
        Poin2(SamRange,l)=sort(Poin2(SamRange,l)); % sorts lines such that they are in order
    end
                
    PoinUse2{k}=Poin2;
end
%%  

if CheckPlots_Zdir == 1
% ploting output from dolinesFarray to confirm fits were good
    figure(101),clf
    for k=1:8
        H2(k)=subplot(2,4,k);
    end

    for k=1:8
        axes(H2(k))
        pcolor(squeeze(dBz(:,k,:))), shading interp
        hold on
        plot(PoinUse{k}')
        clim([-1 1]*9e3)
        ylim(ylims)
    end    
    
    figure(102),clf
    for k=1:12
        H(k)=subplot(3,4,k);
    end
    for k=1:12
        axes(H(k))
        pcolor(squeeze(dBz(:,:,k))), shading interp
        clim([-1 1]*9e3)
        hold on
        plot(PoinUse2{k}')
        ylim(ylims)
    end
end
%%

% change format of data such that it matches light saber probe and redu
% GUI to get output in lighsaber format

BzIntTot2 = cell([1,12]);
dBzIntTot2 = cell([1,12]);
PoinUse3 = cell([1,12]); 

% build B field array along z-axis by using probes from every stick
for k=1:12 
    PoinIn=PoinUse2{k};
    BzIn=squeeze(Bz(:,:,k));
    dBzIn=squeeze(dBz(:,:,k));

    [Poin, BzInt, dBzInt]=dolinesFarray(PoinIn,BzIn,dBzIn,Bh,k, ylims, facClim, FirstPoint, SkipThruGUI);
    %%
    for ll=1:8
        PoinInt(ll,:)=4*(interp1(1:8,Poin(ll,:),1:0.25:8)-1)+1;
    end
    PoinInt=smoo(PoinInt',3)';
    plot(PoinInt')
    %%
    PoinUse3{k}=PoinInt;
    BzIntTot2{k}=BzInt;
    dBzIntTot2{k}=dBzInt;
end

%%   
if CheckPlots_Zdir == 1
% Plot outputs from dolinesFarray
    figure(103),clf
    for k=1:12
        H(k)=subplot(3,4,k);
    end

    for k=1:12
        axes(H(k))
        pcolor(dBzIntTot2{k}), shading interp
        clim([-1 1]*9e3)
        hold on
        plot(PoinUse3{k}')
        ylim(4*ylims)
    end  


%%   
    figure(104),clf
    for k=1:12
        H(k)=subplot(3,4,k);
    end

    for k=1:12
        axes(H(k))
        pcolor(BzIntTot2{k}), shading interp
        clim([-1 1]*Bh*1.6)
        hold on
        plot(PoinUse3{k}')
        ylim(4*ylims)
    end 
end
%%
% pause(0.2)

CheckPlots=0;
UseOldPoints=1; % This should always be 1 such that the Z-direction points are used
PoinUsePhi = PoinUse;
for sticks=1:8
    PoinUsePhi{sticks}=PoinUse{sticks}(:,1:10); % remove extra points for phi direction
end    

if SaveData == 1
   save(PointsUsedFile, 'PoinUse', 'PoinUse3','PoinUsePhi')
end


% Interpolate B and dB in phi and r directions for 3DFarray
if ProbeType == 2
    [BpIntTot2, dBpIntTot2] =  InterpolateB(shot, Bp, dBp, Bh, GUI_onOFF,...
    FirstPoint, ylims, facClim, CheckPlots, UseOldPoints, ProbeType, 'DipoleCusp2024');
    
    [BrIntTot2, dBrIntTot2] =  InterpolateB(shot, Br, dBr, Bh, GUI_onOFF,...
    FirstPoint, ylims, facClim, CheckPlots, UseOldPoints, ProbeType, 'DipoleCusp2024'); 
end

if SaveData == 1
    if ProbeType == 2
        save(IntBdataFile, 'dBzIntTot2', 'BzIntTot2','dBrIntTot2','BrIntTot2','dBpIntTot2','BpIntTot2','Bh')
    else
        save(IntBdataFile, 'dBzIntTot2', 'BzIntTot2','Bh')
    end
end

 