%reoder Bdata such that it is in the correct format for dolinesFarry

function [Br, Bp, Bz, dBr, dBp, dBz, Bh, smoothBzZeroIndx, smoothdBzMinIndx] = reorderB(shot,ProbeType, UseOldPoints)
% clear
% shot = 62271;
% ProbeType = 2;
% UseOldPoints = 0;

%TREX Coils Parameters
SamRange = 69000:71000;
IIm = 901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIoff = 1000; %Makes Bz=Bh at this index
ylims = [1000, 1380]; 
facClim = 0.7; % multiplies range of clim
FirstPoint = 1050; % time fitlines begin
LastPoint = 1350; % time fitlines end
dBzLastPoint = 1200;
dt=1e-7; % time between samples

if ProbeType == 1
    ShotFolder = 'FluxArray_Calibration';
elseif ProbeType == 2
    ShotFolder = '3DFluxArray_Calibration/';
end

DataStructure = what('Data');
DataFolder = DataStructure.path;

IntBdataFile = fullfile(DataFolder,ShotFolder, sprintf('Scripts/IntBdata_%d',shot));
PointsUsedFile = fullfile(DataFolder,ShotFolder, sprintf('Scripts/PointsUsed_%d',shot));

if UseOldPoints == 1
    try
        load(PointsUsedFile, 'PoinUse');  % load points used to interpolate B
    catch
    end
end

if ProbeType == 1
    [daq, Bh] = loadDtaqDataFarray(shot); % load daq data from harddrive or from mdsplus
    [BSet, dBSet, ProbePos , ~, Bh, tpoints] = loadFarray(daq, Bh, SamRange, dt, IIm, IIoff);
elseif ProbeType == 2
    [daq, Bh] = loadDtaqData3DFarray(shot); % load daq data from harddrive or from mdsplus
    [BSet, dBSet, ProbePos , ~, Bh, tpoints] = load3DFarray(daq, Bh, SamRange, dt, IIm, IIoff);
end

%Convert Cell frormat from (time, r, z) to (time, z, r)
if ProbeType == 1
    dBz=zeros([length(tpoints) , 8,12]);
    Bz=zeros([length(tpoints) , 8,12]);
    for k=1:8 
        dBz(:,k,:)=dBSet{k,3};
        Bz(:,k,:)=BSet{k,3};
    end
elseif ProbeType == 2
    Br = permute(BSet{1},[1,3,2]);
    Bp = permute(BSet{2},[1,3,2]);
    Bz = permute(BSet{3},[1,3,2]);
    
    dBr = permute(dBSet{1},[1,3,2]);
    dBp = permute(dBSet{2},[1,3,2]);
    dBz = permute(dBSet{3},[1,3,2]);
end

% Reorder probes so largest R value starts with lowest probe number
Bz=Bz(:,:,end:(-1):1);
dBz=dBz(:,:,end:(-1):1);
if ProbeType == 2
    Br=Br(:,:,end:(-1):1);
    dBr=dBr(:,:,end:(-1):1);
    
    Bp=Bp(:,:,end:(-1):1);
    dBp=dBp(:,:,end:(-1):1);
end

% find time index for first Bz = 0 and minimum dBz in each stick
BzZeroIndx = zeros(8,12);
dBzMinIndx = zeros(8,12);
dBzLimit = dBz(FirstPoint:dBzLastPoint,:,:);
for stick = 1:8
    for probe = 1:12
        BzTest = Bz - Bh;
        testBz = find(BzTest(FirstPoint:LastPoint,stick,probe)> 0,1);
        testdBz = find(dBzLimit(:,stick,probe)==min(dBzLimit(:,stick,probe)),1);
       
        if testdBz < 10
            if probe == 1
                dBzMinIndx(stick,probe) = FirstPoint;
            else
                dBzMinIndx(stick,probe) = dBzMinIndx(stick,probe-1); 
            end
        else
        dBzMinIndx(stick,probe) = testdBz+FirstPoint;
        end
        
        if isempty(testBz)
            if probe == 1
                BzZeroIndx(stick,probe) = FirstPoint;
            else
                BzZeroIndx(stick,probe) = BzZeroIndx(stick,probe-1); 
            end
        else
        BzZeroIndx(stick,probe) = testBz+FirstPoint;
        end 
    end
end
smoothBzZeroIndx = smoo(BzZeroIndx,3);
smoothdBzMinIndx = smoo(dBzMinIndx,3);
%