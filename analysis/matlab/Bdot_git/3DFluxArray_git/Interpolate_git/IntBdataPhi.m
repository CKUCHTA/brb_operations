% This function is used with InterpByEye3DFarrayZdir.m.  It takes 
% PoinUse from InterpByEye3DFarrayZdir.m and uses it to interpolate B and dB.
% Modified 20Dec23

function [BpIntTot2, dBpIntTot2] = IntBdataPhi(Bp, dBp, Bh, GUI_onOFF,...
     PoinUse, PointStart, ylims, facClim, CheckPlots, UseOldPoints)
% clear
% ProbeType = 2; % 1 for 1D flux array. 2 for 3D flux array
% GUI_onOFF = 1; %turn GUI on/off
% CheckPlots = 1; % turn check plots on/off


% PoinUse is the variable that contains lines for interolating flux
First=0;
if exist('PoinUse')==0 % if PoinUse doesn't exist, load zeros into variable
    PoinUse = cell([1,10]); 
    for Rprobe=1:10 % 10 R probes
        PoinUse{Rprobe}=0; % load zeros into variable for firts run
    end
    First=1;
end

if GUI_onOFF==1 % turns on GUI for fitting by eye
    BpIntTot1 = cell([1,8]); 
    dBpIntTot1 = cell([1,8]); 
    for stick=1:8 % Interpolate B and dB for each stick
        
        %%%% What's This For %%%%%%%%
        PoinIn=PoinUse{stick}; %
        if and(First,stick>1) % if this is the first run and stick>1, use PoinUse from previous stick
            PoinIn=PoinUse{stick-1}; 
        end
        
        % Fields for the kth stick
        BpInput = squeeze(Bp(:,stick,:));
        dBpInput = squeeze(dBp(:,stick,:));

        [Poin, BpInt, dBpInt]=dolinesFarray(PoinIn,BpInput,dBpInput,Bh,stick,ylims, facClim, PointStart, UseOldPoints); % calls GUI
        PoinUse{stick}=Poin; % points chosen by eye

        % interpolated B found after implementing GUI
        BpIntTot1{stick}=BpInt; 
        dBpIntTot1{stick}=dBpInt;
        
    end
end


%%

PoinUse2 = cell([1,10]); 
for Rprobe=1:10
    for stick=1:8
        Poin1=PoinUse{stick};
        Poin2(:,stick)=Poin1(:,Rprobe); % flip Poin1 to be in lightsaber format
        SamRange=find(Poin2(:,stick)>PointStart); % find the contour lines that were used after time 1050
        Poin2(SamRange,stick)=sort(Poin2(SamRange,stick)); % sorts lines such that they are in order
    end
                
    PoinUse2{Rprobe}=Poin2;
end
%%  

% plotting output from dolinesFarray to confirm fits were good
if CheckPlots == 1
    figure(101),clf
    for k=1:8
        H2(k)=subplot(2,4,k);
    end

    for k=1:8
        axes(H2(k))
        pcolor(squeeze(dBz(:,k,:))), shading interp
        hold on
        plot(PoinUse{k}')
        caxis([-1 1]*9e3)
        ylim(ylims)
    end    

    figure(102),clf
    for k=1:10
        H(k)=subplot(3,4,k);
    end
    for k=1:10
        axes(H(k))
        pcolor(squeeze(dBz(:,:,k))), shading interp
        caxis([-1 1]*9e3)
        hold on
        plot(PoinUse2{k}')
        ylim(ylims)
    end

    figure(101),clf
    for k=1:8
        H2(k)=subplot(2,4,k);
    end

    for k=1:8
        axes(H2(k))
        pcolor(squeeze(dBz(:,k,:))), shading interp
        hold on
        plot(PoinUse{k}')
        caxis([-1 1]*9e3)
        ylim(ylims)
    end    

    figure(102),clf
    for k=1:10
        H(k)=subplot(3,4,k);
    end
    for k=1:10
        axes(H(k))
        pcolor(squeeze(dBz(:,:,k))), shading interp
        caxis([-1 1]*9e3)
        hold on
        plot(PoinUse2{k}')
        ylim(ylims)
    end
end
%%

% change format of data such that it matches light saber probe and redu
% GUI to get output in lighsaber format

BpIntTot2 = cell([1,10]);
dBpIntTot2 = cell([1,10]);
PoinUse3 = cell([1,10]); 

% build B field array along z-axis by using probes from every stick
for Rprobe=1:10 
    PoinIn=PoinUse2{Rprobe};
    BpInput = squeeze(Bp(:,:,Rprobe));
    dBpInput = squeeze(dBp(:,:,Rprobe));

    [Poin, BpInt, dBpInt]=dolinesFarray(PoinIn,BpInput,dBpInput,Bh,Rprobe, ylims, facClim, PointStart, UseOldPoints);
    %%
    for stick=1:8
        PoinInt(stick,:)=4*(interp1(1:8,Poin(stick,:),1:0.25:8)-1)+1;
    end
    PoinInt=smoo(PoinInt',3)'; % Do I need this if I'm using PoinInt from Z direction?!!!!!!!
    plot(PoinInt')
    %%
    PoinUse3{Rprobe}=PoinInt;
    BpIntTot2{Rprobe}=BpInt;
    dBpIntTot2{Rprobe}=dBpInt;
end

%%   

% Plot outputs from dolinesFarray
if CheckPlots == 1
    figure(103),clf
    for k=1:10
        H(k)=subplot(3,4,k);
    end

    for k=1:10
        axes(H(k))
        pcolor(dBpIntTot2{k}), shading interp
        caxis([-1 1]*9e3)
        hold on
        plot(PoinUse3{k}')
        ylim(4*ylims)
    end  


    %%   
    figure(104),clf
    for k=1:10
        H(k)=subplot(3,4,k);
    end

    for k=1:10
        axes(H(k))
        pcolor(BpIntTot2{k}), shading interp
        caxis([-1 1]*Bh*1.6)
        hold on
        plot(PoinUse3{k}')
        ylim(4*ylims)
    end 
end
