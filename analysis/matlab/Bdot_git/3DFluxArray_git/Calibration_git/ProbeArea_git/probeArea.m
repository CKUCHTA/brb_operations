% Calculate the probe area for lightsaber and flux array.  The area is
% determined by finding the vector for each length segment and taking the
% cross product of that vector with the vector from the origin.
% X Y Z corrispond to R PHI Z in the machine
clear

% R-Probe
L1X = [1.075 1.15 1.125 1.175 1.2 1.3 1.325 1.325 1.325 1.35 1.5 1.5 1.5 1.475 1.475 1.475 1.475 1.475 1.475 1.475 ...
    1.45 1.45 1.45 1.45 1.45 1.425 1.425 1.425 1.425 1.425 1.425 1.4 1.4 1.4 1.4 1.4 1.4 1.375 1.375 1.375 1.375 ...
    1.375 1.375 1.35 1.35 1.35 1.35 1.35 1.325 1.3 1.3 1.3 1.3 1.3 1.3 1.275 1.275 1.275 1.275 1.275 1.275 1.25 ...
    1.25 1.25 1.25 1.25 1.25 1.225 1.225 1.225 1.225 1.225 1.225 1.2 1.2 1.2 1.2 1.175 1.175 1.125 1.15 1.075 1.075];

L1Y = [0.075 0.074 0.05 0.05 0 0 0.025 0.15 0.15 0.2 0.2 0.175 0.1 0.075 0.05 0.05 0.175 0.175 0.1 0.075 0.025 ...
    0.025 0.15 0.15 0.1 0.075 0.05 0.05 0.175 0.175 0.1 0.075 0.025 0.025 0.15 0.15 0.1 0.075 0.05 0.05 0.175 ...
    0.175 0.1 0.075 0.025 0.025 0.125 0.125 0.1 0.075 0.05 0.05 0.175 0.175 0.1 0.075 0.025 0.025 0.15 0.15 0.1 ...
    0.075 0.05 0.05 0.175 0.175 0.1 0.075 0.025 0.025 0.15 0.15 0.1 0.075 0.05 0.05 0.1 0.125 0.175 0.175 0.15 0.15 0.075];

L1Z = [1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 1 1 0 0 0 0 1 1 0 0 0 0 1 1 0 0 0 0 1 1 0 0 0 0 1 1 0 0 0 0 1 1 0 0 0 0 1 1 0 0 ...
    0 0 1 1 0 0 0 0 1 1 0 0 0 0 1 1 0 0 0 0 1 1 1 1 1 1 1 1];

figure(1), clf
plot3(L1X, L1Y, L1Z)

L1X=L1X * 0.0254;  % positions in m
L1Y=L1Y * 0.0254;
L1Z=L1Z*0.059 * 0.0254; % distance between top/bottem layer

plot3(L1X,L1Y,L1Z)
L1=[L1X;L1Y;L1Z];

 RProbeAreas=0;
 for k=1:(length(L1X)-1) %corners
     xv=L1(:,k); %vector from origin (coordinate of coorner wrt origin)
     dx=L1(:,k+1)- L1(:,k); % vector from one corner to next
     RProbeAreas=RProbeAreas+ cross(xv,dx)/2; % area between each corner added up
 end


% Z Probe
L2X = [0.55 0.55 0.575 0.6 0.625 0.625 0.65 1 1 0.675 0.675 0.975 0.975 0.7...
    0.7 0.7 0.7 0.975 0.975 0.675 0.675 1 1 0.65 0.625 0.6 0.575 0.55 0.55 0.55];

L2Y = [0.075 0.05 0.025 0.025 0.05 0.05 0 0 0.2 0.2 0.025 0.025 0.175 0.175...
    0.15 0.15 0.025 0.025 0.175 0.175 0 0 0.2 0.2 0.175 0.2 0.2 0.175 0.15 0.075];

L2Z = [1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1];

figure(2), clf
plot3(L2X, L2Y, L2Z)

L2X=L2X * 0.0254;  % positions in m, 1/40 distance between 
L2Y=L2Y * 0.0254;
L2Z=L2Z*0.059 * 0.0254; % distance between top/bottem layer

plot3(L2X,L2Y,L2Z)
L2=[L2X;L2Y;L2Z];

 ZProbeAreas=0;
 for k=1:(length(L2X)-1) %corners
     xv=L2(:,k); %vector from origin (coordinate of coorner wrt origin)
     dx=L2(:,k+1)- L2(:,k); % vector from one corner to next
     ZProbeAreas=ZProbeAreas+ cross(xv,dx)/2; % area between each corner added up
 end
     

 % Phi Probe
L1X=[3 4 4 5 6 6 7 8 8 9 19 19 12    10 10 20 20 12 10 9 9 19 19 12     10 10 20 20 12 10 9 9 19 19 12   10 10 20 20 12 10 9 9 19 19 12 10 8 8 7 6 6 6 5 4 3 3  3];
    
L1Y=[5 7 8 8 7 6 5 4 2 1 1  1   1     2 2  2  2  2   3 3 3  3  3  3      4 4  4  4  4   5 5 5  5  5  5    6  6  6  6  6  7 7 7  7  7  7  8 8 5 4 4 4 2 1 1 2 3  5];

L1Z=[1 1 1 1 1 1 1 1 1 1 1  0   0     0  1  1  0  0   0 0 1  1  0  0     0  1  1  0  0  0 0 1  1  0  0    0  1  1  0  0  0 0 1  1  0  0  0 0 0 0 0 1 1 1 1 1 1  1];

% top corners 1 bottem corners 0
figure(3),clf

plot3(L1X,L1Y,L1Z)

% all position are integer numbers of 1/40 of an inch
L1X=L1X/40 * 0.0254;  % positions in m, 1/40 distance between 
L1Y=L1Y/40 * 0.0254;
L1Z=L1Z*0.059 * 0.0254; % distance between top/bottem layer

plot3(L1X,L1Y,L1Z)
L1=[L1X;L1Y;L1Z];

 PhiProbeAreas=0;
 for k=1:(length(L1X)-1) %corners
     xv=L1(:,k); %vector from origin (coordinate of coorner wrt origin)
     dx=L1(:,k+1)- L1(:,k); % vector from one corner to next
     PhiProbeAreas=PhiProbeAreas + cross(xv,dx)/2; % area between each corner added up
 end

 RAreaFac = abs(RProbeAreas(1)/ZProbeAreas(3));
 PhiAreaFac = PhiProbeAreas(2)/ZProbeAreas(3);

 % save('/Users/paulgradney/Research/Matlab Drive Cylinder/Bdot/3DFluxArray/Calibration/ProbeArea/AreaCorrection.mat',...
 %      'RProbeAreas', 'PhiProbeAreas','ZProbeAreas', 'RAreaFac', 'PhiAreaFac'); 

