% Calculates the calibration correction factor for the flux array. The
% calibration factor is determine by finding the difference between the
% B-field before reconnection and after reconnection.  This difference is
% then corrected using vacuum plots and the ration of the corrected
% difference to the original difference is plotted.  There should be no
% more than a 10 percent change between probes, which can be confirmed in
% the fac plot.

clear
% shotlist = 62259:62264;
shotlist = 64921;
% shotlist = 62271:62272;

SamRange=69000:73000;
IIm=1740:1800; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
dt=1e-7; % time between samples
IIoff=1800; %Makes Bz=Bh at this index

facTot = struct(); 
kk = 0;
for shot = shotlist
    kk = kk+1;
    [daq, Bh] = loadDtaqData3DFarray(shot, SamRange); % load dtac from harddrive or from mdsplus
    [dBSetRaw, SampleRange, Bh, tpoints] = load3DFarray(daq, Bh, dt);
    [BSet, dBSet, ProbePos, Bh] = calibrate3DFarray(IIoff, IIm, dt, dBSetRaw, Bh);
    
    BSetR = BSet{1};
    BSetP = BSet{2};
    BSetZ = BSet{3};
    
    dBSetR = dBSet{1};
    dBSetP = dBSet{2};
    dBSetZ = dBSet{3};
    
    % DelB should be a smooth field and it should not go through zero when HH
    % field is not added to B. 
    DelB=0;
    % t_initial = 1100;
    % t_final = 1395;
    % t_initial = 1130;
    % t_final = 1395;

    t_initial = 1845;
    % t_final = 1890;
    t_final = 1976;
    
    DelB=DelB+squeeze(BSetZ(t_initial,:,:)-BSetZ(t_final,:,:)); % Change of Bz in the time before drive coils are on and when 
                                                  % the reconneciton starts.  
    
    
    DelBc=DelB; % DelB corrected. More bad probe corrections. Not bad enough to turn off. (Indexes are reversed from loadFarray)
    DelBc(12,7)=(DelB(11,7)+DelB(12,8))/2;
    % DelBc(12,5)=(DelB(11,5)+DelB(12,4))/2;
    % DelBc(11,1)=(DelB(12,1)+DelB(10,1))/2;
    % DelBc(12,2)=(DelB(12,1)+DelB(12,3))/2;
    % DelBc(2,8)=(DelB(3,8)+DelB(1,8))/2;
    
    % smooth Corrected DelB
    DelBc=smoo(smoo(DelBc',2)',2); % 2 is number of points smoothing over, each transpose is a smooth over each direction
    
    facShot=DelBc./DelB; % calibration factor from difference between time before event and after event
    
    facTot(kk).shot = shot;
    facTot(kk).facShot = facShot;
end                                                  

%%
% find calibration factor by averaging over each shot calibration factor
for i = 1:length(facTot)
    if i == 1
        SumFac = facTot(i).facShot;
    else
        SumFac = SumFac + facTot(i).facShot;
    end
end
CalfacZ = SumFac/length(facTot);

figure(106),clf
subplot(1,3,1)
pcolor(DelB),shading interp
colorbar
% caxis([0 0.056])
title('DelB')

subplot(1,3,2)
pcolor(DelBc),shading interp
colorbar
% caxis([0 0.056])
title('DelB Corrected')


subplot(1,3,3)
pcolor(CalfacZ),shading interp
colorbar
% clim([0.9 1.1])
title('Correction Factor')

FluxArrayStruct = what('3DFluxArray_git');
FluxArrayPath = FluxArrayStruct.path;
% save(fullfile(FluxArrayPath,'facZ.mat'), 'CalfacZ')

