% Given a sample point this generates a plot in the RZ plan of 
% Br calulated from the flux function

clear
shot = 62301;
PlotBr = 1; % 1 to plot Br, 0 to not plot Br
Stick1Pos = -0.24; % Position of stick 1 in cm
time = 400; % sample point

%%%% TREX Parameters %%%%
SamRange = 69000:71000;
IIm = 901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIoff = 1000; %Makes Bz=Bh at this index
dt=1e-7; % time between samples
dBzLastPoint = 1200;
LastPoint = 1350; % time fitlines end
ProbeType = 2; % 1 for 1D flux array. 2 for 3D flux array
GUI_onOFF = 1; % 1 to turn GUI on
FirstPoint = 1050; % sample point for first point in dolinesFarray
facClim = 0.7; % multiplies range of clim
ylims = [1000, 1380]; 
UseOldPoints = 1; % 1 for auto clicking through GUI
CheckPlots_Zdir = 0; % 1 to plot Bz and dBz for each stick


DataFolder = '/Users/paulgradney/Research/Data/';
ShotFolder = '3DFluxArray_Calibration/';
FullShotFolder = fullfile(DataFolder, ShotFolder);

[daq, Bh] = loadDtaqData3DFarray(shot); % load daq data from harddrive or from mdsplus
[BSet, dBSet, ProbePos , ~, ~, tpoints] = load3DFarray(daq, Bh, SamRange, dt, IIm, IIoff);

Configuration = 1; % 1 for TREX, 2 for Jets
SaveData = 0; % 1 to save data to harddrive

%Convert Cell frormat from (time, r, z) to (time, z, r)
if ProbeType == 1
    dBz=zeros([length(tpoints) , 8,12]);
    Bz=zeros([length(tpoints) , 8,12]);
    for k=1:8 
        dBz(:,k,:)=dBSet{k,3};
        Bz(:,k,:)=BSet{k,3};
    end
elseif ProbeType == 2
    % BrCal = permute(BrCal,[1,3,2]); % Calculated Br values from flux
    BrMeasured = permute(BSet{1},[1,3,2]);
    Bp = permute(BSet{2},[1,3,2]);
    Bz = permute(BSet{3},[1,3,2]);
    
    % dBrCal = permute(dBrCal,[1,3,2]); % Calculated dBr values from flux
    dBrMeasured = permute(dBSet{1},[1,3,2]);
    dBp = permute(dBSet{2},[1,3,2]);
    dBz = permute(dBSet{3},[1,3,2]);
end

% Reorder probes so largest R value starts with lowest probe number
Bz=Bz(:,:,end:(-1):1);
dBz=dBz(:,:,end:(-1):1);
if ProbeType == 2
    BrMeasured = BrMeasured(:,:,end:(-1):1);
    dBrMeasured = dBrMeasured(:,:,end:(-1):1); 
    % dBrCal=dBrCal(:,:,end:(-1):1);
    
    Bp=Bp(:,:,end:(-1):1);
    dBp=dBp(:,:,end:(-1):1);
end

% interpolate Bz and BrMeasured 
try
    [BzMovie, dBzMovie, Bclims, dBclims, PsiTot, JphiMovie, Zvec, Rvec, timeIndex, ShotFolder] = ...
        DataMovie3DFarrayZ(shot, Configuration, ProbeType, SaveData, Stick1Pos);
catch
    SaveData = 1;
    InterpByEye3DFarrayZdir(shot, ProbeType, GUI_onOFF, SaveData, UseOldPoints, CheckPlots_Zdir)
    [BzMovie, dBzMovie, BzClims, dBzClims, PsiTot, JphiMovie, Zvec, Rvec, timeIndex, ShotFolder] = DataMovie3DFarrayZ(shot,...
    Configuration, ProbeType, SaveData, Stick1Pos);
end
[BrIntTot2Measured, dBrIntTot2Measured] = InterpolateB(shot, BrMeasured, dBrMeasured, Bh, GUI_onOFF,...
    FirstPoint, ylims, facClim, CheckPlots_Zdir, UseOldPoints, ProbeType);

[BrCal, dBrCal, ~] = BrCalculated(shot, PlotBr, PsiTot, Zvec, Rvec); % calculates Br based on flux
% return
% Calculate Flux from interpolated Bz
% PsiSet = zeros(size(dBzIntTot2{1}));
% for stk = 1:8
%     Bz = BSetZ(:,:,stk);
%     for ch=1:size(Bz,2)
%         BzTemp = Bz - Bh;
%         flux = BzFlux_FluxArray(BzTemp, dBzRpos); % calculate flux function
%         PsiSet(:,:,stk) = flux/(2*pi); % convert flux to flux function
%     end
% end

%%
% Generate B field data in the RZ plan for each sample point
[BrMovieCal, dBrMovie, BrClim, dBrClim, RvecR, ~] = DataMovie3DFarrayR(shot, BrIntTot2, dBrIntTot2, Configuration,...
        ProbeType, SaveData, Stick1Pos);

[BrMovieMeasured, dBrMovie, BrClim, dBrClim, RvecR, ~] = DataMovie3DFarrayR(shot, BrIntTot2Measured, dBrIntTot2Measured, Configuration,...
        ProbeType, SaveData, Stick1Pos);



%%
time = 440;
Psi = squeeze(PsiTot(:,time,:));
PsiV3=linspace(-12,12,61)*1e-4;
PsiV4=linspace(-0.4,0.4,6)*1e-4;
lw=1.15; % linewidth of flux contour

figure(22), clf
subplot(2,1,1)
pcolor(Zvec, RvecR, -squeeze(BrMovieCal(:,time,:)*1e3)), shading interp
hold on
contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
contour(Zvec,Rvec,Psi,PsiV3(2:2:end),'k','linewidth',lw)
contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
clim([-2 2])
colorbar
xlabel('Z(m)')
ylabel('R(m)')
title('Br (mT) calulated from \Psi')

subplot(2,1,2)
pcolor(Zvec, RvecR, squeeze(BrMovieMeasured(:,time,:)*1e3)), shading interp
hold on
contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
contour(Zvec,Rvec,Psi,PsiV3(2:2:end),'k','linewidth',lw)
contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
clim([-2 2])
colorbar
xlabel('Z(m)')
ylabel('R(m)')
title('Br (mT) measured')
