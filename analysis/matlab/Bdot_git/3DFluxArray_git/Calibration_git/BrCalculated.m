% This function calculates Br by lineraly interpolating Bz and calulating the flux function
% and then taking the derivative of the flux function to get Br. 

% function [BSetR_Cal, dBSetR_Cal, Bh, PsiSet] = BrCalculated(shot, PlotBr, Stick1Pos)
clear
shot = 62271;
PlotBr = 0;
Stick1Pos = -24; % stick 1 position in cm

SamRange=69000:71000;
IIm=940:1040; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
dt=1e-7; % time between samples
IIoff=1000; %Makes Bz=Bh at this index
xlims = [950 1400];

[daq, Bh] = loadDtaqData3DFarray(shot); % load dtac from harddrive or from mdsplus

[BSet, dBSet, ProbePos , ~, ~, tpoints] = load3DFarray(daq, Bh, SamRange, dt, IIm, IIoff);
tlims = tpoints(xlims);

BSetZ = BSet{3};
BSetR = BSet{1};
dBSetR = dBSet{1};

Nr = 100; % number of points to interpolate in r direction
dBzRvec = ProbePos{3};
dBrRvec = ProbePos{1};
BrRvec = linspace(dBrRvec(1), dBrRvec(end), Nr); % R vector to interpolate Br
RvecInterp = linspace(dBzRvec(1), dBzRvec(end), Nr); % R vector to interpolate Bz
IndR0 = find( RvecInterp > 0, 1, 'first') - 1; % index of first r value less than 0 (subtracting 1 get the first negative rather than first positive value)

Nz = 20; % number of points to interpolate in z direction
stickZvec = (1:8)*12 - 12 + Stick1Pos; % z position of sticks in m
stickZvec = stickZvec*1e-2; % convert to m
ZvecIneterp = linspace(stickZvec(1), stickZvec(end), Nz); % z vector to interpolate Bz

% Mesh grids for interpolation
[Zmesh,Rmesh] = meshgrid(stickZvec,dBzRvec);
[ZmeshInterp, RmeshInterp] = meshgrid(ZvecIneterp, RvecInterp);

[~,BrRmesh] = meshgrid(stickZvec,dBrRvec);
[~,BrRmeshInterp] = meshgrid(ZvecIneterp,BrRvec);

BSetZinterp = zeros(size(BSetZ,1), Nr, Nz);
BSetRinterp = zeros(size(BSetR,1), Nr, Nz);
% Interpolate Bz along R and Z
for iit = 1:size(BSetZ,1)
    BzFrame = squeeze(BSetZ(iit,:,:)); %(t, R, Z), squeezing it in time to get one frame in time
    BSetZinterp(iit,:,:) = interp2(Zmesh, Rmesh, BzFrame, ZmeshInterp, RmeshInterp, 'linear', 0);
    BrFrame = squeeze(BSetR(iit,:,:)); %(t, R, Z), squeezing it in time to get one frame in time
    % BSetRinterp(iit,:,:) = interp2(Zmesh, BrRmesh, BrFrame, ZmeshInterp, BrRmeshInterp, 'linear', 0);
end

% for stk = 1:8
%     Bz = BSetZ(:,:,stk);
%     for samp = 1:size(Bz,1)
%         BSetZinterp(samp,:,stk) = interp1(dBzRvec, Bz(samp,:), Rvec, 'linear', 'extrap');
%     end
% end

% figure(31), clf
stick = 1;
sample = 1280;
% sgtitle('Bz Interpolated Along R')
% subplot(1,2,1)
% plot(dBzRvec, BSetZ(sample,:,stick)*1e3, '-x')
% xlabel('R[m]') 
% ylabel('Bz[mT]')
% subplot(1,2,2) 
% plot(Rvec, BSetZinterp(sample,:,stick)*1e3, '-x')
% xlabel('R[m]') 
% ylabel('Bz[mT]')
% 
% figure(32), clf
% sgtitle(sprintf('Stick %d', stick))
% subplot(1,2,1)
% pcolor(BSetZinterp(:,:,stick)'*1e3), shading interp
% colorbar
% clim([-10 10])
% title('Interpolated Bz')
% xlabel('t [\mus]')
% ylabel('R[m]')
% xlim(xlims)

% subplot(1,2,2)
% pcolor(BSetZ(:,:,stick)'*1e3), shading interp
% colorbar
% clim([-10 10])
% title('Original Bz')
% xlabel('t [\mus]')
% ylabel('R[m]')
% xlim(xlims)

% figure(33), clf
% radius = 0.3;
% [~, RindIndterp] = min(abs(Rvec - radius));
% [~, RindInd] = min(abs(dBzRvec - radius));

% sgtitle(sprintf('Radius = %0.1f', radius))
% subplot(1,2,1)
% plot(Zvec, squeeze(BSetZinterp(sample,RindIndterp,:))*1e3, '-x')
% title('Interpolated Bz')
% xlabel('Z[m]')
% ylabel('[mT]')

% subplot(1,2,2)
% plot(stickZvec, squeeze(BSetZ(sample,RindInd,:))*1e3, '-x')
% title('Measured Bz')
% xlabel('Z[m]')
% ylabel('[mT]')



% Calculate Flux Function
PsiSet = BSetZinterp*0;
% Zvec = ((1:12:8*12)-1 + Stick1Pos)*1e-2; % z probe position in meters
[Rmat,Zmat]=meshgrid(RvecInterp,ZvecIneterp); % create an R and Z matrix to circumvent using for loops
Rmatu = sqrt(Rmat.^2 + 0.02^2)';% Add offset to reduce the effect of dividing by zero

for iit = 1:size(BSetZinterp,1)
    BzFrame = squeeze(BSetZinterp(iit,:,:)); %(t, R, Z), squeezing it in time to get one frame in time
    % BzFrame = BzFrame + Bh;
    Psi = BzFrame.*Rmatu; % use the meshgrid output to illiminate for loop
    Psi = cumtrapz(Psi,1)*(RvecInterp(2)-RvecInterp(1));
    Psi = Psi-Psi(IndR0,:); % account for probes below zero
    PsiSet(iit,:,:) = Psi;
end
 
% Calculate Br
BSetR_Cal = PsiSet*0;
for iit=1:size(PsiSet,1)
     % calculate Br from Psi
    [~,Psidz] = gradient(squeeze(PsiSet(iit,:,:))', RvecInterp, ZvecIneterp);
    Psidz = Psidz';
    Br_Cal = Psidz./Rmatu;
    BSetR_Cal(iit,:,:) = Br_Cal;
end

% Calculate the time derivative of BSetR_Cal
dBSetR_Cal = BSetR_Cal*0;
for zz=1:size(dBSetR_Cal,3)
    for rr = 1:size(dBSetR_Cal,2)
        dBrdt = gradient(squeeze(BSetR_Cal(:,rr,zz))', tpoints);
        dBSetR_Cal(:,rr,zz) = dBrdt';
    end
end



if PlotBr == 1
    %%
    figure(32), clf
    sgtitle(sprintf('Br(\\Psi)[mT] from Flux Function'))
    for i = 1:8
        % for ch = 1:10
        subplot(2,4,i)
        pcolor(tpoints, RvecInterp, BSetR_Cal(:,:,i)'*1e3), shading interp
        % pcolor(BSetR_Cal(:,:,i)'*1e3), shading interp
        colorbar
        clim([-2 2])
        xlim(tlims)
        title(sprintf('Stick %d', i))
        xlabel('t [\mus]')
        ylabel('R[m]')
        % end
    end
    %%
    figure(33), clf
    sgtitle(sprintf('dBr(\\Psi)[T/s] Derivative of Br(\\Psi)'))
    for i = 1:8
        % for ch = 1:10
        subplot(2,4,i)
        pcolor(dBSetR_Cal(:,:,i)'), shading interp
        colorbar
        clim([-1 1]*1e-4)
        xlim(xlims)
        title(sprintf('Stick %d', i))
        % end
    end

    figure(34), clf
    sgtitle('dBr[T/s] Measured') 
    for i = 1:8
        % for ch = 1:10
        subplot(2,4,i)
        pcolor(dBSetR(:,:,i)'), shading interp
        colorbar
        clim([-1.5 1.5]*1e2)
        xlim(xlims)
        title(sprintf('Stick %d', i))
        % end
    end

    figure(35), clf
    sgtitle('Br[mT/s] Measured') 
    for i = 1:8
        % for ch = 1:10
        subplot(2,4,i)
        pcolor(BSetR(:,:,i)'*1e3), shading interp
        colorbar
        clim([-2 2])
        xlim(xlims)
        title(sprintf('Stick %d', i))
        % end
    end
    %%
    figure(36), clf
    sgtitle('\Psi') 
    for i = 1:8
        % for ch = 1:10
        subplot(2,4,i)
        pcolor(tpoints, RvecInterp, PsiSet(:,:,i)'), shading interp
        % pcolor(PsiTot(:,:,i)'), shading interp
        colorbar
        clim([-12 12]*1e-4)
        xlim(tlims)
        title(sprintf('Stick %d', i))
        % end
    end

   
    %%
    stick = 1;
    probe = 1;
    
    figure(37), clf
    plot(PsiSet(:,stick,probe)')
    xlim(xlims)

    figure(1), clf
    sgtitle(sprintf('stick %d Probe %d', stick, probe))
    subplot(4,1,1)
    plot(BSetR_Cal(:,stick,probe)*1e3)
    xlim(xlims)
    title(sprintf('Br(\\Psi)[mT] Calculated from \\Psi'))

    subplot(4,1,2)
    plot(dBSetR_Cal(:,stick,probe))
    title(sprintf('dBr[T/s] (derivative of Br(\\Psi)'))
    xlim(xlims)

    subplot(4,1,3)
    plot(dBSetR(:,stick,probe))
    title('dBr[T/s] Measured')
    xlim(xlims)
    
    subplot(4,1,4)
    plot(-BSetR(:,stick,probe)*1e3)
    title('Br[mT] Measured')
    xlim(xlims)


%%
end
% Plot of Br in the R-Z plane
    figure(55), clf
    sample = 1280;
    Psi = squeeze(PsiSet(sample,:,:));
    PsiV3=linspace(-12,12,61)*1e-4;
    PsiV4=linspace(-0.4,0.4,6)*1e-4;
    lw=1.15; % linewidth of flux contour
    BSetR_Compare = -squeeze(BSetR(sample,:,:)); 

    subplot(2,1,1)
    pcolor(ZvecIneterp, RvecInterp, -squeeze(BSetR_Cal(sample,:,:))*1e3), shading interp
    hold on
    % contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
    contour(ZvecIneterp,RvecInterp,Psi,PsiV3(2:2:end),'b','linewidth',lw)
    % contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
    clim([-2 2])
    colorbar
    xlabel('Z(m)')
    ylabel('R(m)')
    title('Br (mT) calulated from \Psi')
    
    subplot(2,1,2)
    pcolor(stickZvec,ProbePos{1}, BSetR_Compare*1e3), shading interp
    hold on
    % contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
    contour(ZvecIneterp,RvecInterp,Psi,PsiV3(2:2:end),'b','linewidth',lw)
    % contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
    clim([-2 2])
    colorbar
    xlabel('Z(m)')
    ylabel('R(m)')
    title('Br (mT) measured')



%%
[BrModel, PsiModel, Rmodel, Zmodel] = multiCoilAsym(1);
% find the index of the R and Z from the model to values closest to Rvec and Zvec
RSizePsiInd = nan(size(RvecInterp));
ZSizePsiInd = nan(size(ZvecIneterp));
for i = 1:length(RvecInterp)
    [~, RSizePsiInd(i)] = min(abs(Rmodel - RvecInterp(i)));
end
for i = 1:length(ZvecIneterp)
    [~, ZSizePsiInd(i)] = min(abs(Zmodel - ZvecIneterp(i)));
end 

RsizePsi = Rmodel(RSizePsiInd);
ZsizePsi = Zmodel(ZSizePsiInd);
BrModelSizePsi = BrModel(RSizePsiInd, ZSizePsiInd);
PsiModelSizePsi = PsiModel(RSizePsiInd, ZSizePsiInd);

% Plot of Br in the R-Z plane
figure(56), clf
sample = 1280;
Psi = squeeze(PsiSet(sample,:,:));
PsiV3=linspace(-12,12,61)*1e-4;
PsiV4=linspace(-0.4,0.4,6)*1e-4;
lw=1.15; % linewidth of flux contour
Br_CalCompare = -squeeze(BSetR_Cal(sample,:,:));

subplot(2,1,1)
pcolor(ZvecIneterp, RvecInterp, -Br_CalCompare*1e3), shading interp
hold on
% contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
contour(ZvecIneterp,RvecInterp,Psi,PsiV3(2:2:end),'b','linewidth',lw)
% contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
clim([-2 2])
colorbar
xlabel('Z(m)')
ylabel('R(m)')
title('Br (mT) calulated from \Psi')

subplot(2,1,2)
pcolor(ZsizePsi, RsizePsi, -BrModelSizePsi*1e3), shading interp
hold on
% contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
contour(Zmodel,Rmodel,PsiModel,PsiV3(2:2:end),'b','linewidth',lw)
% contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
clim([-2 2])
colorbar
xlabel('Z(m)')
ylabel('R(m)')
title('Br (mT) from Model')

% Scale Br from model to match Br from Psi
ScaleFactor = dot(Br_CalCompare, BrModelSizePsi, 1)/dot(BrModelSizePsi, BrModelSizePsi, 1);
BrModelScaled = ScaleFactor*BrModelSizePsi;
figure(57), clf
subplot(2,1,1)
pcolor(ZvecIneterp, RvecInterp, -Br_CalCompare*1e3), shading interp
hold on
% contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
contour(ZvecIneterp,RvecInterp,Psi,PsiV3(2:2:end),'b','linewidth',lw)
% contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
clim([-2 2])
colorbar
xlabel('Z(m)')
ylabel('R(m)')
title('Br (mT) calulated from \Psi')

subplot(2,1,2)
pcolor(ZsizePsi, RsizePsi, -BrModelScaled*1e3), shading interp
hold on
% contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
contour(Zmodel,Rmodel,PsiModel,PsiV3(2:2:end),'b','linewidth',lw)
% contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
clim([-2 2])
colorbar
xlabel('Z(m)')
ylabel('R(m)')
title(sprintf('Br (mT) from Model Scaling factor = %0.3f', ScaleFactor))
%%


% Calculated Correction Factor 
probeIndx = nan(1,length(dBrRvec));
stickIndx = nan(1,8);
for n = 1:length(dBrRvec)
    [~, probeIndx(n)] = min(abs(RvecInterp - abs(dBrRvec(n)))); % find index values for Model field that correspond to probe location
end

for n = 1:8
    [~,stickIndx(n)] = min(abs(ZvecIneterp - stickZvec(n)));
end

sample1a = 1040; %times before discharge
sample1b = 1050;
sample2a = 1270; %times at peak of discharge
sample2b = 1280;
BrFac = nan(size(BSetR, 2), size(BSetR, 3));
BSetRCorrected = zeros(size(dBSetR));
for stick = 1:8
    BrMeasured = BSetR(sample,:,stick);
    for probe = 1:size(BrMeasured, 2)
        DelBr = mean(BSetR(sample2a:sample2b, probe, stick)) - mean(BSetR(sample1a:sample1b, probe, stick));
        DelBz = mean(BSetZ(sample2a:sample2b, probe, stick)) - mean(BSetZ(sample1a:sample1b, probe, stick));
       
        BrFac(probe, stick) = (BrModelScaled(probeIndx(probe), stickIndx(stick)) - DelBr)/DelBz; % correction factor
        % Correct Br using Correction Factor
        BSetRCorrected(:,probe, stick) = BSetR(:,probe,stick); %+ BrFac(probe,stick)*BSetZ(:,probe,stick);
    end
end

% save '/Users/paulgradney/Research/brb_operations/analysis/matlab/Bdot_git/3DFluxArray_git/BrFac.mat' BrFac

figure(58), clf
subplot(3,1,1)
pcolor(stickZvec,ProbePos{1}, -squeeze(BSetRCorrected(sample,:,:))*1e3), shading interp
hold on
% contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
contour(ZvecIneterp,RvecInterp,Psi,PsiV3(2:2:end),'b','linewidth',lw)
% contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
clim([-2 2])
colorbar
xlabel('Z(m)')
ylabel('R(m)')
title('Br (mT) Measured Corrected')

subplot(3,1,2)
pcolor(stickZvec,ProbePos{1}, -BSetR_Compare*1e3), shading interp
hold on
% contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
contour(ZvecIneterp,RvecInterp,Psi,PsiV3(2:2:end),'b','linewidth',lw)
% contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
clim([-2 2])
colorbar
xlabel('Z(m)')
ylabel('R(m)')
title('Br (mT) measured')

subplot(3,1,3)
pcolor(ZsizePsi, RsizePsi, -BrModelScaled*1e3), shading interp
hold on
% contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
contour(Zmodel,Rmodel,PsiModel,PsiV3(2:2:end),'b','linewidth',lw)
% contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
clim([-2 2])
colorbar
xlabel('Z(m)')
ylabel('R(m)')
title('Br (mT) model')

fontsize(15,"points")

figure(59), clf
pcolor(ZvecIneterp,RvecInterp, Psi), shading interp
hold on
contour(ZvecIneterp,RvecInterp,Psi,[0 0],'m','linewidth',lw)
% contour(Zmodel,Rmodel,PsiModel,PsiV3(2:2:end),'b','linewidth',lw)
% contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
% clim([-2 2])
colorbar
xlabel('Z(m)')
ylabel('R(m)')
title('\Psi(B_z)')
