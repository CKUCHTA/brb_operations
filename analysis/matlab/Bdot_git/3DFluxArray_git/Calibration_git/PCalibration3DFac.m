% Calculates the calibration correction factor for the flux array. The
% calibration factor is determine by finding the difference between the
% B-field before reconnection and after reconnection.  This difference is
% then corrected using vacuum plots and the ration of the corrected
% difference to the original difference is plotted.  There should be no
% more than a 10 percent change between probes, which can be confirmed in
% the fac plot.

clear
shotlist = 62259:62264;
% shot = 61877;

SamRange=69000:71000;
IIm=901:1001; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
dt=1e-7; % time between samples
IIoff=1000; %Makes Bz=Bh at this index

facTot = struct(); 
kk = 0;
for shot = shotlist
    kk = kk+1;
    [daq, Bh] = loadDtaqData3DFarray(shot); % load dtac from harddrive or from mdsplus
    [BSet, dBSet, ProbePos , SamRange, Bh, tpoints] = load3DFarray(daq, Bh, SamRange, dt, IIm, IIoff); 
    
    BSetR = BSet{1};
    BSetP = BSet{2};
    BSetZ = BSet{3};
    
    dBSetR = dBSet{1};
    dBSetP = dBSet{2};
    dBSetZ = dBSet{3};
    
    % DelB should be a smooth field and it should not go through zero when HH
    % field is not added to B. 
    DelB=0;
    t_initial = 1146;
    t_final = 1390;
    
    DelB=DelB+squeeze(BSetP(t_initial,:,:)-BSetP(t_final,:,:)); % Change of Bz in the time before drive coils are on and when 
                                                  % the reconneciton starts.  
    
    
    DelBc=DelB; % DelB corrected. More bad probe corrections. Not bad enough to turn off. (Indexes are reversed from loadFarray)
    % DelBc(12,1)=(DelB(11,1)+DelB(12,2))/2;
    % DelBc(11,1)=(DelB(12,1)+DelB(10,1))/2;
    % DelBc(12,2)=(DelB(12,1)+DelB(12,3))/2;
    DelBc(2,8)=(DelB(3,8)+DelB(1,8))/2;
    
    % smooth Corrected DelB
    DelBc=smoo(smoo(DelBc',2)',2); % 2 is number of points smoothing over, each transpose is a smooth over each direction
    
    facShot=DelBc./DelB; % calibration factor from difference between time before event and after event
    
    facTot(kk).shot = shot;
    facTot(kk).facShot = facShot;
end                                                  

%%
% find calibration factor by averaging over each shot calibration factor
for i = 1:length(facTot)
    if i == 1
        SumFac = facTot(i).facShot;
    else
        SumFac = SumFac + facTot(i).facShot;
    end
end
Calfac = SumFac/length(facTot);

figure(106),clf
subplot(1,3,1)
pcolor(DelB),shading interp
colorbar
% caxis([0 0.056])
title('DelB')

subplot(1,3,2)
pcolor(DelBc),shading interp
colorbar
% caxis([0 0.056])
title('DelB Corrected')


subplot(1,3,3)
pcolor(Calfac),shading interp
colorbar
% clim([0.9 1.1])
title('Correction Factor')

% save '/Users/paulgradney/Research/Matlab Drive Cylinder/Bdot/3DFluxArray/facZ.mat' CalfacZ

