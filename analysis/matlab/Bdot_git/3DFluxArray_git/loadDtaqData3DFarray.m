% Load dtaq data from external harddrive or from mdsplus if data is not in
% external harddrive and save to Temporary folder

function [daq, Bh] = loadDtaqData3DFarray(shot, saveData, SampleRange)
% clear
% shot = 62479;
% saveData = 0;

% DataFolder = '/Users/paulgradney/Research/Data';
% TemporaryFolder = 'Temporary';

dtaqFile = sprintf('3DFarrayDTAC_%d.mat',shot);
try
    dtaqFilePath = which(dtaqFile); % path of TeData file
    load(dtaqFilePath, 'daq', 'Bh');
catch
    disp(['Could not find file: ', dtaqFile])
    [daq, ~, Bh] = acq_data3DFarray(shot, SampleRange); % load data from MDSPlus

    if saveData
        TempStruct = what('Temporary');
        TemporaryFolder = TempStruct.path; % relative file path  
        fileName = fullfile(TemporaryFolder, dtaqFile);
        save(fileName)
    end
end