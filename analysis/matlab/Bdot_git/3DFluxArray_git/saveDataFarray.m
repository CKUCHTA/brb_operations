clear

% shotlist = [64079:64104 64106:64121 64129:64211 64214:64505];
shotlist = 64621:64643;

% data1DFolderStucture = what('1DFarray');
% data1DFolder = data1DFolderStucture.path;

% SampleRange = 69000:77000;

k = 0;
for shot = shotlist
    k = k+1;
    try
        ProbeType = 1;
        [daq, AllBz, Bh] = acq_data3DFarray(shot);
        dtaqFile = sprintf('3DFarrayDTAC_%d.mat',shot);
        fileName3D = fullfile(data3DFolder, 'dtac_data', dtaqFile);
        save(fileName3D)
        % 
        % ProbeType = 2;
        % [daq, AllBz, Bh] = acq_data3DFarray(shot, SampleRange, ProbeType);
        % dtaqFile = sprintf('1DFarrayDTAC_%d.mat',shot);
        % fileName1D = fullfile(data1DFolder, 'dtac_data', dtaqFile);
        % save(fileName1D)

    catch error
        warning('Shot %d failed to load',shot);
        display(error);
        badShotList(k) = shot;
        continue
    end

end
badShotFile = fullfile(Farray3DFolder, 'badShotNumbers.mat');
if exist('badShotList','var')==1
    save(badShotFile, 'badShotList')
end
% save_TeData 
