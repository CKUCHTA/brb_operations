% This grabs data from the 3DFarry at different Z position for a given sample point for every probe

function [BSetZdata, dBSetZdata, ZposArray, ProbePos, FarrayZPos, tpoints] = GetZscanData(FolderName, SampleEval, SaveData)
% clear
% % FolderName = 'DipoleCuspVertical2024'; % parent directory of data folder
% FolderName = 'DipoleCusp2024'; % parent directory of data folder
% SampleEval = 1950; % sample point to evaluate for each probe and each shot
% SaveData = 1;

DataFolderStruct = what(FolderName);
DataFolder = DataFolderStruct.path;
ZscanFolder = fullfile(DataFolder,'3DFarray','Matlab_Data','ZScanData');
if ~exist(ZscanFolder, "dir")
    mkdir(ZscanFolder)
end
FileName = sprintf('ZScanData_Sample%d.mat', SampleEval);
DataFile = fullfile(ZscanFolder, FileName); % file matlab data is saved

%Create shotlist array from file names in data directory
shotlist = GetShotNumbers(FolderName);

savePlot = 0;
saveData = 0;

IncludeContour = 0; % set to 1 to plot flux contours (Unless you are using interpolated data, these contours are wrong!!!)
PlotFlux = 0    ; % set to 1 to plot flux
PlotTimeTrace = 0; % set to 1 for raw data time trace

xlims = [1000 2000]; % plot range for sample points
SampleRange=69000:73000;
dt=1e-7; % time between samples
IIm=901:980; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIoff=1020; %Makes Bz=Bh at this index

% B and dB arrays for Z position.  z indx is the index of the z position in the ZposArray
BxSetZdata = zeros(10, 8, length(shotlist)); %  (probe, stick, z indx))
BySetZdata = zeros(12, 8, length(shotlist)); %  (probe, stick, z indx))
BzSetZdata = zeros(10, 8, length(shotlist)); %  (probe, stick, z indx))

dBxSetZdata = zeros(10, 8, length(shotlist)); %  (probe, stick, z indx))
dBySetZdata = zeros(12, 8, length(shotlist)); %  (probe, stick, z indx))
dBzSetZdata = zeros(10, 8, length(shotlist)); %  (probe, stick, z indx))

% collect data from excel file
excelFileName = 'DipoleCusp2024.xlsx';
DataFolderName = 'DipoleCusp2024';
docid = '1YbMnfhPIyXe3SAJL9hR5xkr6GhluWjYWQPfE1WV9LpA';
webSave = 0; % set to 1 to download current excel file from google spread
[shotData, ~] = DipoleShotData(excelFileName, DataFolderName, docid, webSave);

FarrayZPos = zeros(length(shotlist),2);
ZposArray = zeros(length(shotlist), 2);
badshotlist = zeros(1, length(shotlist));
k = 0;
for shot = shotlist
    k = k+1;
    shotIndx = find(shotData(:,1) == shot);
    if isempty(shotIndx)
        badshotlist(k) = shot;
        continue
    end

    [daq, Bh] = loadDtaqData3DFarray(shot, saveData, SampleRange); % load dtac from harddrive or from mdsplus
    [~, ~, tpoints, dBSetRaw] = load3DFarrayDipole(daq, SampleRange, dt, shot, PlotTimeTrace, xlims); 
    [BSet, dBSet, ProbePos] = Calibrate_3DFarryDipole(IIoff, IIm, dt, dBSetRaw);

    BSetX = BSet{1};
    BSetY = BSet{2};
    BSetZ = BSet{3} + Bh;

    dBSetX = dBSet{1};
    dBSetY = dBSet{2};
    dBSetZ = dBSet{3};

    Zpos = shotData(shotIndx, 3);
    ZposArray(k, 1) = shot;
    ZposArray(k, 2) = Zpos;
    
    FarrayZPos(k, 1) = shot;
    FarrayZPos(k, 2) = shotData(shotIndx, 3);    
    
    BxSetZdata(:,:,k) = BSetX(SampleEval, :, :);
    BySetZdata(:,:,k) = BSetY(SampleEval, :, :);
    BzSetZdata(:,:,k) = BSetZ(SampleEval, :, :);

    dBxSetZdata(:,:,k) = dBSetX(SampleEval, :, :);
    dBySetZdata(:,:,k) = dBSetY(SampleEval, :, :);
    dBzSetZdata(:,:,k) = dBSetZ(SampleEval, :, :);
end
BSetZdata = {BxSetZdata, BySetZdata, BzSetZdata};
dBSetZdata = {dBxSetZdata, dBySetZdata, dBzSetZdata};

if SaveData == 1
    save(DataFile, 'BSetZdata', 'dBSetZdata', 'ZposArray', 'SampleEval', 'ProbePos', 'tpoints', 'FarrayZPos')
end


