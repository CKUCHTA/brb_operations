% Using GeZscanData_Shotlist.m, this script creates a structure array of the
% magnetic field data for a set of shots.  The data is saved in the file:
% /Users/paulgradney/Research/Data/DipoleCusp/DipoleCuspVertical_FullScan/3DFarray/Matlab_Data/ZScanData/ZscanBdata_AllSamples.mat

% created 29May2024

clear
ParentFolder = 'DipoleCuspVertical_FullScan';
DataFolderStruct = what(ParentFolder);
DataFolder = DataFolderStruct.path;
ZscanFolder = fullfile(DataFolder,'3DFarray','Matlab_Data','ZScanData');
if ~exist(ZscanFolder, "dir")
    mkdir(ZscanFolder)
end

% collect data from excel file
excelFileName = 'DipoleCusp2024.xlsx';
docid = '1YbMnfhPIyXe3SAJL9hR5xkr6GhluWjYWQPfE1WV9LpA';
webSave = 0; % set to 1 to download current excel file from google spread
[shotData, ~] = DipoleShotData(excelFileName, ParentFolder, docid, webSave);
%%%%%%%%%%%%%%%%%%% Spreadsheet data %%%%%%%%%%%%%%%%%
% columns in shotData: [ShotNumber(1); FluxArray2_Zpos(2); MachProbStick1_ZPos(3);
% DipoleCenter_ZPos(4); Te1_ZPos(5); NumberOfGunsUsed(6); DipoleCap_Volt(7); CT2_Volt(8)]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% shots used for Te data
ShotList_Te = [64162 64150 64217 64231 64267 64278 64454 64427 64328 64368 64366 64478 64493 ...
        64164 64152 64220 64235 64271 64287 64302 64314 64330 64345 64359 64479 64497 ...
        64167 64155 64223 64238 64272 64289 64304 64318 64333 64376 64361 64484 64499];

ZscanBdata = struct('BSetZdata', {}, 'dBSetZdata', {}, 'ZposArray', {}, ...
 'ProbePos', {}, 'FarrayZPos', {}, 'tpoints', {}, 'shotData', {}, 'shotList_Te', {});

ZscanBdata(1).shotData = shotData;
ZscanBdata(1).shotList_Te = ShotList_Te;
SampleRange = 1820:2150; % dtaq range starting just before dipole is turned on
for SampleEval = SampleRange
    fprintf('Sample %d of Samples %d\n', SampleEval, SampleRange(end))
    SaveData = 0; % set to 1 to save data collected in GetZscanData_Shotlist
    [BSetZdata, dBSetZdata, ZposArray, ProbePos, FarrayZPos, tpoints] = ...
    GetZscanData_Shotlist(ParentFolder, SampleEval, SaveData, ShotList_Te, 'dipole1kV');
    
    ZscanBdata(SampleEval).BSetZdata = BSetZdata;
    ZscanBdata(SampleEval).dBSetZdata = dBSetZdata;
    ZscanBdata(SampleEval).ZposArray = ZposArray;
    ZscanBdata(SampleEval).ProbePos = ProbePos;
    ZscanBdata(SampleEval).FarrayZPos = FarrayZPos;
    ZscanBdata(SampleEval).tpoints = tpoints;
end

% save data
save(fullfile(ZscanFolder, 'ZscanBdata_AllSamples.mat'), 'ZscanBdata')


