% Collects data for a sample range using GetZscanData_ShotList.  This data is used
% to generate plots in the XZ and YZ planes as well as the time traces for
% Plot Zscan

clear
SampleRange = 1820:2150;
FolderName = 'DipoleCuspVertical_FullScan';
FolderStructure = what('DipoleCuspVertical_FullScan');
FolderPath = FolderStructure.path;
DataFolder = fullfile(FolderPath, '3DFarray', 'dtac_data');
SaveData = 1;
dipoleType = 'dipole1kV'; % 'dipole1kV', 'dipole2kV'
OnePerPosition = 1; % 1 = only one shot per flux probe position, 0 = all shots

%Using DipoleShotData generate the shotlist for specified shot parameters
excelFileName = 'DipoleCusp2024.xlsx';
docid = '1YbMnfhPIyXe3SAJL9hR5xkr6GhluWjYWQPfE1WV9LpA';
webSave = 1;
[shotData, spreadData] = DipoleShotData(excelFileName, FolderName, docid, webSave);
%%%%%%%%%%%%%%%%%%%%%%% Spreadsheet data %%%%%%%%%%%%%%%%%
% columns in shotData: [ShotNumber(1); FluxArray2_Zpos(2); MachProbStick1_ZPos(3);
% DipoleCenter_ZPos(4); Te1_ZPos(5); NumberOfGunsUsed(6); DipoleCap_Volt(7); CT2_Volt(8)]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get shot numbers from folder with saved shot data
FullshotList = GetShotNumbers(FolderName);

% find shots with specified parameter
k = 1;
n = 1;
i = 1;
j = 1;
for shot = FullshotList
    [TestShotInd, ~] = find(shot == shotData(:,1));
    Number_of_Guns = shotData(TestShotInd, 6);

    if Number_of_Guns == 1
       continue % skip bad shots   
    end

    DipoleCap = shotData(TestShotInd, 7);
    FluxProbePos = shotData(TestShotInd,2);
    CT2 = shotData(TestShotInd, 8);
    if CT2 == 5 % shots with CT cap at 5kV
        if Number_of_Guns == 6 % plasma shots
            if DipoleCap == 1  % diplole cap at 1kV
                if j > 1 && OnePerPosition == 1
                    compareShot = shotList_DriveAndDipole1kV(j-1,2);
                else
                    compareShot = [];
                end
                if compareShot == FluxProbePos
                    continue % skip shots taken at same flux probe position
                else 
                    shotList_DriveAndDipole1kV(k,:) = shotData(TestShotInd,:); %#ok<*SAGROW> %#ok<*SAGROW> %#ok<SAGROW> %#ok<SAGROW> %#ok<*SAGROW> %#ok<SAGROW> %#ok<*SAGROW> %#ok<S
                end
            elseif DipoleCap == 2  % diplole cap at  2kV and 0V
                if k > 1 && OnePerPosition == 1
                    compareShot = shotList_DriveAndDipole2kV(k-1,2);
                else
                    compareShot = [];
                end
                if compareShot == FluxProbePos
                    continue % skip shots taken at same flux probe position
                else 
                    shotList_DriveAndDipole2kV(k,:) = shotData(TestShotInd,:); %#ok<*SAGROW> %#ok<*SAGROW> %#ok<SAGROW> %#ok<SAGROW> %#ok<*SAGROW> %#ok<SAGROW> %#ok<*SAGROW> %#ok<SAGROW> %#ok<*SAGROW> %#ok<*SAGROW> %#ok<*SAGROW> %#ok<*SAGROW> %#ok<*SAGROW> %#ok<SAGROW> %#ok<SAGROW> %#ok<SAGROW> % plasma shots with dipole cap = 1kV
                    k = k +1;
                end
            else DipoleCap == 0  % diplole cap at off
                if n > 1 && OnePerPosition == 1
                    compareShot = shotList_DriveOnly(i-1,2);
                else
                    compareShot = [];
                end
                if compareShot == FluxProbePos
                    continue % skip shots taken at same flux probe position
                else
                    shotList_DriveOnly(i,:) = shotData(TestShotInd,:); %#ok<*SAGROW> % plasma shots with drive only
                    i = i +1;
                end
                
            end
        end
    end


    %Vacuum ShotList
    if CT2 == 5
        if Number_of_Guns == 0
            if DipoleCap == 2
                if n > 1
                    compareShot = shotList_VacDipole2kV(n-1,2);
                else
                    compareShot = [];
                end
                if compareShot == FluxProbePos
                    continue % skip shots taken at same flux probe position
                else
                    shotList_VacDipole2kV(n,:) = shotData(TestShotInd,:); % vacuum shots with dipole cap = 1kV
                    n = n +1;
                end
            end
        end
    end
end

% sort the shot list by flux probe position
shotList_DipoleAndDrive = sortrows(shotList_DriveAndDipole1kV, 2);
shotList_DriveOnly = sortrows(shotList_DriveOnly, 2);
shotList_VacDipole2kV = sortrows(shotList_VacDipole2kV, 2);

DipoleDriveVacShotList = [64091 64110 64133 64146 64147 64173 64186 64187 64230 64247 64263 64264 ...
    64295 64309 64325 64341 64353 64396 64388 64417 64446 64473 64491 64503]; % Vacuum 1kV Dipole Cap full scan
PlasmaShotList = [64081 64093 64112 64134 64148 64160 64174 64190 64209 64231 64248 64265 ...
    64281 64298 64311 64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan
DriveShotList = [64086 64097 64118 64140 64154 64167 64180 64196 64204 64238 64257 64272 ...
    64288 64303 64317 64332 64348 64361 64376 64406 64434 64464 64483 64498]; % Drive Only full scan
NewShots = [64621 64622 64627 64628 64629 64630 64635 64636 64641 64642];

AllShots = [PlasmaShotList, DriveShotList, DipoleDriveVacShotList, NewShots];

% return

tic
for SampleEval = SampleRange
    fprintf('sample %d out of %d\n', SampleEval, SampleRange(end))
    [BSetZdata, dBSetZdata, ZposArray, ProbePos, FarrayZPos, tpoints]...
    = GetZscanData_Shotlist(FolderName, SampleEval, SaveData, AllShots, dipoleType);
end
toc