% Creates a movie from the PlotXZPlan script at a specified sample point.  The data is not interpolated.

function XZPlanDriveCoilOnlyMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, ShotList, stickList)
% clear
% saveFrame = 1;
% saveMovie = 1;
% SampleSet = 1840:1:2110; % range of sample point movie is created from
% SaveData = 1; % Save GetScanZ data
% % ShotList =   [62575 62818 62628 62640 62650 62661 62678 62694 62705 62716 62727 62738 62751 62762 62775 62784 62796 62812]; % drive coils plasma shots
% % FolderName = 'DipoleCusp2024';
% ShotList = [63350 63361 63372 63383 63394 63405 63416 63428 63438 63450 63461 63472]; % Drive Coil Only dipole vertical
% Folder = 'DipoleCuspVertical2024';
% stickList = 1:8; % plot the plan created by the stick scanned in the Z direction

DataFolderStruct = what(Folder);
DataFolder = DataFolderStruct.path;
MovieFolder = fullfile(DataFolder, '3DFarray','Plots','Movies','XZPlane_DriveCoil');
if ~exist(MovieFolder, 'dir')
    mkdir(MovieFolder)
end 

for stick = stickList
    if saveMovie == 1
        videoFile = fullfile(MovieFolder, sprintf('XZmovie_DriveCoil_Stick_%d.mp4', stick));
        writerObj = VideoWriter(videoFile, 'MPEG-4');
        writerObj.FrameRate = 10; % Set the frame rate of the video
        
        open(writerObj);
    end

    frame = 0;
    PNGFolder = fullfile(MovieFolder,sprintf('PNG_Stick_%d',stick));

    if ~exist(PNGFolder, 'dir')
        mkdir(PNGFolder);
    end
    for SamplePoint = SampleSet
        frame = frame + 1;
        PNGFile = fullfile(PNGFolder,sprintf('DriveCoil_Stick%d_Frame%d.png', stick, frame));
        [XZPlanDriveCoilOnlyFrame, ~, ~] = PlotXZPlanDriveCoilsOnly(ShotList, Folder, SamplePoint, stick, SaveData, 1);
    
        if saveFrame == 1
            print('-f1', PNGFile, '-dpng', '-r400')
        else
            pause(0.5)
        end
        if saveMovie == 1
            CurrentFrame = getframe(XZPlanDriveCoilOnlyFrame);
            writeVideo(writerObj, CurrentFrame); % Write the frame to the video
        else
            pause(0.5)
        end
       
    end
    
    % Close the VideoWriter object
    if saveMovie == 1
        close(writerObj);
    end
end