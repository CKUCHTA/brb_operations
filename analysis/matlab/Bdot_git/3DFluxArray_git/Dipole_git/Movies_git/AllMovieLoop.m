% plots movies in the XY, XZ, and YZ plan for dipole and drive, drive only
% and dipole removed.

clear

% inputs for movie scripts
saveFrame = 0;
saveMovie = 1;
SampleSet = 1840:1:2200; % range of sample point movie is created from
SaveData = 1; % save dtaq data if it is not already saved

% parent folder the data is located and the plots are saved
Folder = 'DipoleCuspVertical2024'; 
% FolderName = 'DipoleCusp2024';

% Shots with dipole in horrizontal position
% ShotList = [62604 62617 62626 62634 62645 62657 62674 62688 62699 62712 62721 62733 62749 62757 62768 62779 62790 62802]; %plasma shots
% ShotList = [62610 62612 62631 62654 62830 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% VacShotList =    [62610 62612 62631 62830 62654 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum 

% shots with dipole in vertical position
ShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots dipole vertical
DriveOnlyShotList = [63350 63361 63372 63383 63394 63405 63416 63428 63438 63450 63461 63472]; % Drive Coil Only dipole vertical
VacShotList = [63354 63365 63376 63387 63398 63409 63420 63431 63442 63454 63465 63476]; % vacuum shots dipole vertical

ProbeList = 1:10; % plot the plan created by the stick scanned in the Z direction
stickList = 1:8; % plot the plan created by the stick scanned in the Z direction
xlims = [1000 2000]; % sets the sample range plotted in the raw data time trace plots (does not effect movie plots)

% run movie scripts
YZPlanMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, ShotList, ProbeList)
YZPlanDriveCoilsOnlyMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, DriveOnlyShotList, ProbeList)
YZPlanDipoleRemovedMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, ShotList, VacShotList, ProbeList)

XZPlanMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, ShotList, stickList)
XZPlanDipoleRemovedMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, ShotList, VacShotList, stickList)
XZPlanDriveCoilOnlyMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, DriveOnlyShotList, stickList)

XYPlanMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, ShotList, xlims)
