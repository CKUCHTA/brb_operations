% Creates a movie from the PlotXZDipoleRemoved script at a specified sample point.  The data is not interpolated.

function XZPlanDipoleRemovedMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, PlasmaShotList, VacShotList, stickList)
% clear
% saveFrame = 0;
% saveMovie = 1;
% SampleSet = 1840:1:2110; % range of sample point movie is created from
% SaveData = 1;
% % PlasmaShotList = [62604 62617 62626 62634 62645 62657 62674 62688 62699 62712 62721 62733 62749 62757 62768 62779 62790 62802]; %plasma shots
% % VacShotList =    [62610 62612 62631 62830 62654 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% % FolderName = 'DipoleCusp2024';
% PlasmaShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots
% VacShotList = [63354 63365 63376 63387 63398 63409 63420 63431 63442 63454 63465 63476]; % Dipole vacuum shots
% Folder = 'DipoleCuspVertical2024';
% stickList = 1:8; % plot the plan created by the stick scanned in the Z direction

DataFolderStruct = what(Folder);
DataFolder = DataFolderStruct.path;
MovieFolder = fullfile(DataFolder, '3DFarray','Plots','Movies','XZPlane_DipoleRemoved');

if ~exist(MovieFolder, 'dir')
    mkdir(MovieFolder);
end

for stick = stickList
    if saveMovie == 1
        videoFile = fullfile(MovieFolder, sprintf('XZmovieNoDip_Stick_%d.mp4', stick));
        writerObj = VideoWriter(videoFile, 'MPEG-4');
        writerObj.FrameRate = 10; % Set the frame rate of the video
        
        open(writerObj);
    end

    frame = 0;
    PNGFolder = fullfile(MovieFolder,sprintf('PNG_Stick_%d',stick));

    if ~exist(PNGFolder, 'dir')
        mkdir(PNGFolder);
    end
    for SamplePoint = SampleSet
        frame = frame + 1;
        PNGFile = fullfile(PNGFolder,sprintf('NoDipole_Stick%d_Frame%d.png', stick, frame));
        [XZPlanFrame, ~, ~] = PlotXZPlanDipoleRemoved(PlasmaShotList, VacShotList, Folder, SamplePoint, stick, SaveData, 1);
    
        if saveFrame == 1
            print('-f1', PNGFile, '-dpng', '-r400')
        else
            pause(0.5)
        end
        if saveMovie == 1
            CurrentFrame = getframe(XZPlanFrame);
            writeVideo(writerObj, CurrentFrame); % Write the frame to the video
        else
            pause(0.5)
        end
       
    end
    
    % Close the VideoWriter object
    if saveMovie == 1
        close(writerObj);
    end
end