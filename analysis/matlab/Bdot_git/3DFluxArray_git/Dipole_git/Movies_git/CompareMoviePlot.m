% Create a movie from PlotXZComparison or PlotYZComparison using a given
% set of shots.  This generates plots in X, Y, and Z direction for all
% probes and all sticks, depending on what plane is plotted.

clear
saveFrame = 0;
saveMovie = 1;
SampleSet = 1850:2100; % range of sample points movie is created from
SaveData = 1;
% ShotList = [62604 62617 62626 62634 62645 62657 62674 62688 62699 62712 62721 62733 62749 62757 62768 62779 62790 62802]; %plasma shots
% VacShotList =    [62610 62612 62631 62830 62654 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% FolderName = 'DipoleCusp2024';
% ShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots
% VacShotList = [63354 63365 63376 63387 63398 63409 63420 63431 63442 63454 63465 63476]; % Dipole vacuum shots
% DriveShotList = [63350 63361 63372 63383 63394 63405 63416 63428 63438 63450 63461 63472]; % Drive Coil Only dipole vertical

VacShotList = [64091 64110 64133 64146 64147 64173 64186 64187 64230 64247 64263 64264 ...
    64295 64309 64325 64341 64353 64396 64388 64417 64446 64473 64491 64503]; % Vacuum 1kV Dipole Cap full scan
ShotList = [64081 64093 64112 64134 64148 64160 64174 64190 64209 64231 64248 64265 ...
    64281 64298 64311 64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan
DriveShotList = [64086 64097 64118 64140 64154 64167 64180 64196 64204 64238 64257 64272 ...
    64288 64303 64317 64332 64348 64361 64376 64406 64434 64464 64483 64498]; % Drive Only full scan


ParentFolder = 'DipoleCuspVertical2024';
ParentFolder = 'DipoleCuspVertical_FullScan';
MovieTypeSet = {'YZPlane_Compare', 'XZPlane_Compare'};
BdirectionAll = 1:3; % choose component of B and dB to plot; 1=X, 2=Y, 3=Z


for TypeIndx = 1:numel(MovieTypeSet)
    for Bdirection = BdirectionAll
        CreateCompareMoviePlot(saveFrame, saveMovie, SampleSet, ParentFolder, MovieTypeSet{TypeIndx},...
            ShotList, DriveShotList, VacShotList, SaveData, Bdirection)
    end
end



function CreateCompareMoviePlot(saveFrame, saveMovie, SampleSet, ParentFolder, MovieType,...
    ShotList, DriveShotList, VacShotList, SaveData, Bdirection)

    % Choose plotting script to run
    if isequal(MovieType,'YZPlane_Compare')
        ProbeOrStickString = 'Probe';
        ProbeORStickList = 1:10; % Probe Plan from Z-Scan
        if Bdirection == 2
            ProbeORStickList = 1:12; % Y direction has 12 probes
        end
    elseif isequal(MovieType,'XZPlane_Compare')
        ProbeOrStickString = 'Stick';
        ProbeORStickList = 1:8; % Stick Plane from Z-Scan
    end
    
    % Create direction Folder name
    if Bdirection == 1
        DirectionFolder = 'X_direction';
    elseif Bdirection == 2
        DirectionFolder = 'Y_direction';
    else
        DirectionFolder = 'Z_direction';
    end
    
    DataFolderStruct = what(ParentFolder);
    DataFolder = DataFolderStruct.path;
    MovieFolder = fullfile(DataFolder, '3DFarray','Plots','Movies',MovieType, DirectionFolder);
    if ~exist(MovieFolder, 'dir')
        mkdir(MovieFolder)
    end
    
    
    for ProbeOrStick = ProbeORStickList
        if saveMovie == 1
            videoFile = fullfile(MovieFolder, sprintf('%smovie_%s_%d.mp4', MovieType, ProbeOrStickString, ProbeOrStick));
            writerObj = VideoWriter(videoFile, 'MPEG-4');
            writerObj.FrameRate = 10; % Set the frame rate of the video
            
            open(writerObj);
        end
    
        frame = 0;
        PNGFolder = fullfile(MovieFolder,sprintf('PNG_%s_%d', ProbeOrStickString, ProbeOrStick));
    
        if ~exist(PNGFolder, 'dir')
            mkdir(PNGFolder);
        end
        for SamplePoint = SampleSet
            frame = frame + 1;
            PNGFile = fullfile(PNGFolder,sprintf('%s%d_Frame%d.pdf', ProbeOrStickString, ProbeOrStick, frame));
            
            % Choose plotting script to run
            if isequal(MovieType,'YZPlane_Compare')
                Frame = PlotYZComparison(ShotList, DriveShotList, VacShotList, ...
                    ParentFolder, SamplePoint, ProbeOrStick, SaveData, 0, Bdirection);
            elseif isequal(MovieType,'XZPlane_Compare')
                Frame = PlotXZComparison(ShotList, DriveShotList,...
                    VacShotList, ParentFolder, SamplePoint, ProbeOrStick, SaveData, 0, Bdirection);
            end
                  
            if saveFrame == 1
                exportgraphics(Frame,PNGFile,"Resolution",400)
            else
                pause(0.5)
            end
            if saveMovie == 1
                CurrentFrame = getframe(Frame);
                writeVideo(writerObj, CurrentFrame); % Write the frame to the video
            else
                pause(0.5)
            end
           
        end
        
        % Close the VideoWriter object
        if saveMovie == 1
            close(writerObj);
        end
    end
end