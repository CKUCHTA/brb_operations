% Creates a movie from the PlotXZPlan script at a specified sample point.  The data is not interpolated.

function YZPlanMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, ShotList, ProbeList)
% clear
% saveFrame = 0;
% saveMovie = 1;
% SampleSet = 1840:1:2110; % range of sample point movie is created from
% SaveData = 1;
% Folder = 'DipoleCuspVertical2024';
% % ShotList = [62604 62617 62626 62634 62645 62657 62674 62688 62699 62712 62721 62733 62749 62757 62768 62779 62790 62802]; %plasma shots
% % ShotList = [62610 62612 62631 62654 62830 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% % FolderName = 'DipoleCusp2024';
% ShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots dipole vertical
% ProbeList = 1:10; % plot the plan created by the stick scanned in the Z direction

DataFolderStruct = what(Folder);
DataFolder = DataFolderStruct.path;
MovieFolder = fullfile(DataFolder, '3DFarray','Plots','Movies','YZPlane');
if ~exist(MovieFolder, 'dir')
    mkdir(MovieFolder)
end

for ProbeNum = ProbeList
    if saveMovie == 1
        videoFile = fullfile(MovieFolder, sprintf('YZmovie_Probe_%d.mp4', ProbeNum));
        writerObj = VideoWriter(videoFile, 'MPEG-4');
        writerObj.FrameRate = 10; % Set the frame rate of the video
        
        open(writerObj);
    end

    frame = 0;
    PNGFolder = fullfile(MovieFolder,sprintf('PNG_Probe_%d',ProbeNum));

    if ~exist(PNGFolder, 'dir')
        mkdir(PNGFolder);
    end
    for SamplePoint = SampleSet
        frame = frame + 1;
        PNGFile = fullfile(PNGFolder,sprintf('Probe%d_Frame%d.png', ProbeNum, frame));
        [YZPlanFrame, ~, ~, ~, ~, ~] = PlotYZPlan(ShotList, Folder, SamplePoint, ProbeNum, SaveData, 1);
        if saveFrame == 1
            print('-f1', PNGFile, '-dpng', '-r400')
        else
            pause(0.5)
        end
        if saveMovie == 1
            CurrentFrame = getframe(YZPlanFrame);
            writeVideo(writerObj, CurrentFrame); % Write the frame to the video
        else
            pause(0.5)
        end
       
    end
    
    % Close the VideoWriter object
    if saveMovie == 1
        close(writerObj);
    end
end