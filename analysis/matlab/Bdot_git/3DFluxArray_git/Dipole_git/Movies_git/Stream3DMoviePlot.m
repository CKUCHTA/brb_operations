% Create a movie from PlotXZComparison or PlotYZComparison using a given
% set of shots.  This generates plots in X, Y, and Z direction for all
% probes and all sticks, depending on what plane is plotted.

clear
saveFrame = 0;
saveMovie = 1;
SampleSet = 1850:2100; % range of sample points movie is created from
SaveData = 1;
% ShotList = [62604 62617 62626 62634 62645 62657 62674 62688 62699 62712 62721 62733 62749 62757 62768 62779 62790 62802]; %plasma shots
% VacShotList =    [62610 62612 62631 62830 62654 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% FolderName = 'DipoleCusp2024';
% PlasmaShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots
% DipoleDriveVacShotList = [63352 63363 63374 63385 63396 63407 63418 63429 63441 63452 63463 63474]; % Drive Coil and Dipole vacuum shot dipole vertical
% DriveShotList = [63350 63361 63372 63383 63394 63405 63416 63428 63438 63450 63461 63472]; % Drive Coil Only dipole vertical

% DipoleDriveVacShotList = [64091 64110 64133 64146 64147 64173 64186 64187 64230 64247 64263 64264 ...
    % 64295 64309 64325 64341 64353 64396 64388 64417 64446 64473 64491 64503]; % Vacuum 1kV Dipole Cap full scan
% PlasmaShotList = [64081 64093 64112 64134 64148 64160 64174 64190 64209 64231 64248 64265 ...
    % 64281 64298 64311 64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan
DriveShotList = [64086 64097 64118 64140 64154 64167 64180 64196 64204 64238 64257 64272 ...
    64288 64303 64317 64332 64348 64361 64376 64406 64434 64464 64483 64498]; % Drive Only full scan

PlasmaShotList = [64082 64095 64115 64137 64151 64163 64177 64193 64220 64234 64254 64269 ...
    64285 64300 64315 64331 64345 64358 64370 64400 64428 64456 64479 64495]; % 2kV Dipole Cap full scan
DipoleDriveVacShotList = [64089 64108 64131 64144 64158 64171 64184 64205 64227 64244 64261 64276 ...
    64293 64307 64323 64339 64352 64395 64385 64421 64443 64469 64488 64505]; % Vacuum 2kV Dipole Cap full scan
% Bad Vacuum shot: 64227, 


% ParentFolder = 'DipoleCuspVertical2024';
ParentFolder = 'DipoleCuspVertical_FullScan';
MovieTypeSet = {'DipoleDrive3D'};
% MovieTypeSet = {'DipoleRemoved3D'};
dipoleType = 'dipole2kV'; % 'dipole1kV', 'dipole2kV'

for TypeIndx = 1:numel(MovieTypeSet)
    CreateStreamMoviePlot(saveMovie, SampleSet, ParentFolder, MovieTypeSet{TypeIndx},...
        PlasmaShotList, DriveShotList, DipoleDriveVacShotList, SaveData, dipoleType)
end



function CreateStreamMoviePlot(saveMovie, SampleSet, ParentFolder, MovieType,...
    PlasmaShotList, DriveShotList, DipoleDriveVacShotList, SaveData, dipoleType)
    
    DataFolderStruct = what(ParentFolder);
    DataFolder = DataFolderStruct.path;
    MovieFolder = fullfile(DataFolder, '3DFarray','Plots','Movies','3DStreamPlot');
    if ~exist(MovieFolder, 'dir')
        mkdir(MovieFolder)
    end
    
    
    if saveMovie == 1
        videoFile = fullfile(MovieFolder, sprintf('%s_%s_movie.mp4', MovieType, dipoleType));
        writerObj = VideoWriter(videoFile, 'MPEG-4');
        writerObj.FrameRate = 10; % Set the frame rate of the video
        
        open(writerObj);
    end

    frame = 0;

    for SamplePoint = SampleSet
        frame = frame + 1;
        
        % Choose plotting script to run
        if isequal(MovieType,'DipoleDrive3D')
            WhichPlot = 1;
            [Frame] = Stream3DPlot(ParentFolder, PlasmaShotList, DriveShotList, DipoleDriveVacShotList, SamplePoint, SaveData, WhichPlot, dipoleType);
        elseif isequal(MovieType,'DipoleDriveVac3D')
            WhichPlot = 2;
            [Frame] = Stream3DPlot(ParentFolder, PlasmaShotList, DriveShotList, DipoleDriveVacShotList, SamplePoint, SaveData, WhichPlot, dipoleType);
        elseif isequal(MovieType,'DipoleRemoved3D')
            WhichPlot = 3;
            [Frame] = Stream3DPlot(ParentFolder, PlasmaShotList, DriveShotList, DipoleDriveVacShotList, SamplePoint, SaveData, WhichPlot, dipoleType);
        elseif isequal(MovieType,'DipoleRemoved3DInterp')
            WhichPlot = 1;
            [Frame] = InterpStream3D(ParentFolder, PlasmaShotList, DriveShotList, DipoleDriveVacShotList, SamplePoint, SaveData, WhichPlot, dipoleType);
        end
             
        if saveMovie == 1
            CurrentFrame = getframe(Frame);
            writeVideo(writerObj, CurrentFrame); % Write the frame to the video
        else
            pause(0.5)
        end
       
    end
    
    % Close the VideoWriter object
    if saveMovie == 1
        close(writerObj);
    end
end
