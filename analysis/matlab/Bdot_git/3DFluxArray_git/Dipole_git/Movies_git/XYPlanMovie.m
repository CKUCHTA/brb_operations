% Creates a movie from the PlotXYPlan script at a specified sample point.  The data is not interpolated.

function XYPlanMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, ShotList, xlims)
% clear
% ShotList = [62604 62617 62626 62634 62645 62657 62674 62688 62699 62712 62721 62733 62749 62757 62768 62779 62790 62802];
% xlims = [1000 2000];
% SaveData = 1;
% saveFrame = 0;
% saveMovie = 1;
% SampleSet = 1840:1:2110; % range of sample point movie is created from
% Folder = 'DipoleCuspVertical2024';

SampleRange=69000:77000;
DataFolderStruct = what(Folder);
DataFolder = DataFolderStruct.path;
MovieFolder = fullfile(DataFolder, '3DFarray','Plots','Movies','XYPlane');

for shot = ShotList
    PNGFolder = fullfile(MovieFolder,sprintf('PNGFrames_%d',shot));
    if ~exist(PNGFolder, 'dir')
        mkdir(PNGFolder);
    end
    
    if saveMovie == 1
        videoFile = fullfile(MovieFolder, sprintf('movie_%d.mp4', shot));
        writerObj = VideoWriter(videoFile, 'MPEG-4');
        writerObj.FrameRate = 10; % Set the frame rate of the video
        
        open(writerObj);
    end
        
    frame = 0;
    for SamplePoint = SampleSet
        frame = frame + 1;
        PNGFile = fullfile(PNGFolder,sprintf('%d_Frame%d.png', shot, frame));
        
        XYPlanFrame =  PlotXYPlan(shot, SamplePoint, SampleRange, xlims, SaveData);
    
        if saveFrame == 1
            print('-f1', PNGFile, '-dpng', '-r400')
        else
            pause(0.5)
        end
        if saveMovie == 1
            CurrentFrame = getframe(XYPlanFrame);
            writeVideo(writerObj, CurrentFrame); % Write the frame to the video
        else
            pause(0.5)
        end
       
    end
    
    % Close the VideoWriter object
    if saveMovie == 1
        close(writerObj);
    end
end

