% Creates a movie from the PlotXZPlan script at a specified sample point.  The data is not interpolated.

function YZPlanDriveCoilsOnlyMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, ShotList, ProbeList)
% clear
% saveFrame = 0;
% saveMovie = 1;
% SampleSet = 1840:1:2110; % range of sample point movie is created from
% SaveData = 1;
% Folder = 'DipoleCuspVertical2024';
% % ShotList = [62575 62818 62628 62640 62650 62661 62678 62694 62705 62716 62727 62738 62751 62762 62775 62784 62796 62812]; % drive coils plasma shots
% % FolderName = 'DipoleCusp2024';
% ShotList = [63350 63361 63372 63383 63394 63405 63416 63428 63438 63450 63461 63472]; % Drive Coil Only dipole vertical
% ProbeList = 1:10; % plot the plan created by the stick scanned in the Z direction

DataFolderStruct = what(Folder);
DataFolder = DataFolderStruct.path;
MovieFolder = fullfile(DataFolder, '3DFarray','Plots','Movies','YZPlane_DriveCoil');
if ~exist(MovieFolder, 'dir')
    mkdir(MovieFolder)
end

for ProbeNum = ProbeList
    if saveMovie == 1
        videoFile = fullfile(MovieFolder, sprintf('YZmovie_DriveCoil_Probe_%d.mp4', ProbeNum));
        writerObj = VideoWriter(videoFile, 'MPEG-4');
        writerObj.FrameRate = 10; % Set the frame rate of the video
        
        open(writerObj);
    end

    frame = 0;
    PNGFolder = fullfile(MovieFolder,sprintf('PNG_Probe_%d',ProbeNum));

    if ~exist(PNGFolder, 'dir')
        mkdir(PNGFolder);
    end
    for SamplePoint = SampleSet
        frame = frame + 1;
        PNGFile = fullfile(PNGFolder,sprintf('DriveCoil_Probe%d_Frame%d.png', ProbeNum, frame));
        [YZPlanDriveCoilsOnlyFrame, ~, ~] = PlotYZPlanDriveCoilsOnly(ShotList, Folder, SamplePoint, ProbeNum, SaveData, 1);
        
        if saveFrame == 1
            print('-f1', PNGFile, '-dpng', '-r400')
        else
            pause(0.5)
        end
        if saveMovie == 1
            CurrentFrame = getframe(YZPlanDriveCoilsOnlyFrame);
            writeVideo(writerObj, CurrentFrame); % Write the frame to the video
        else
            pause(0.5)
        end
       
    end
    
    % Close the VideoWriter object
    if saveMovie == 1
        close(writerObj);
    end
end
