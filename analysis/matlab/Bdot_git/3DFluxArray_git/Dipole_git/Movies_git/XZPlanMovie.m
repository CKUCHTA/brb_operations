% Creates a movie from the PlotXZPlan script at a specified sample point.  The data is not interpolated.

% function XZPlanMovie(saveFrame, saveMovie, SampleSet, SaveData, Folder, ShotList, stickList)
clear
saveFrame = 0;
saveMovie = 1;
SampleSet = 1:1:331; % range of sample point movie is created from
SaveData = 1;
% Folder = 'DipoleCuspVertical2024';
% ShotList = [62604 62617 62626 62634 62645 62657 62674 62688 62699 62712 62721 62733 62749 62757 62768 62779 62790 62802]; %plasma shots
% ShotList = [62610 62612 62631 62654 62830 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% FolderName = 'DipoleCusp2024';
% ShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots dipole vertical
ShotList = [64081 64093 64112 64134 64148 64160 64174 64190 64209 64231 64248 64265 ...
    64281 64298 64311 64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan
Folder = 'DipoleCuspVertical_FullScan';
stickList = 5; % plot the plan created by the stick scanned in the Z direction


% FolderName = 'DipoleCuspVertical_FullScan';
% SampleEvalActual = 180;
% Stick = 4; % plot the plan created by the stick scanned in the Z direction
% SaveData = 1;
% PlotPlane = 1;

DataFolderStruct = what(Folder);
DataFolder = DataFolderStruct.path;
MovieFolder = fullfile(DataFolder, '3DFarray','Plots','Movies','XZPlane');
if ~exist(MovieFolder, 'dir')
    mkdir(MovieFolder)
end


for stick = stickList
    if saveMovie == 1
        videoFile = fullfile(MovieFolder, sprintf('XZmovie_Stick_%d.mp4', stick));
        writerObj = VideoWriter(videoFile, 'MPEG-4');
        writerObj.FrameRate = 10; % Set the frame rate of the video
        
        open(writerObj);
    end

    frame = 0;
    PNGFolder = fullfile(MovieFolder,sprintf('PNG_Stick_%d',stick));

    if ~exist(PNGFolder, 'dir')
        mkdir(PNGFolder);
    end
    for SamplePoint = SampleSet
        frame = frame + 1;
        PNGFile = fullfile(PNGFolder,sprintf('Stick%d_Frame%d.png', stick, frame));
        [XZPlanFrame, ~, ~, ~, ~] = PlotXZPlan(ShotList, Folder, SamplePoint, stick, SaveData, 1);
    
        if saveFrame == 1
            print('-f1', PNGFile, '-dpng', '-r400')
        else
            pause(0.5)
        end
        if saveMovie == 1
            CurrentFrame = getframe(XZPlanFrame);
            writeVideo(writerObj, CurrentFrame); % Write the frame to the video
        else
            pause(0.5)
        end
       
    end
    
    % Close the VideoWriter object
    if saveMovie == 1
        close(writerObj);
    end
end