% load Flux Array data from dtacs 323 324 325 326 116 128 129 and 320. Channels from the dtaq's 
% are assigned to thier associated sticks and boards.
% Created 23Feb2024

% function [boards, sticks, tpoints, dBSetRaw] = load3DFarrayDipole(daq, SampleRange, dt, shot, PlotTimeTrace, xlims) 
clear
shot = 64921;
% SamRange=69000:89000;
SampleRange=69000:73000;
dt=1e-7; % time between samples
saveData = 1;
[daq, Bh] = loadDtaqData3DFarray(shot, saveData, SampleRange); % load dtac from harddrive or from mdsplus
PlotTimeTrace = 1; % change to 1 for plot of digitizer data associated with each board
% xlims = [1000 20000];
xlims = [1800 2200];

% tpoints = (SampleRange(1):SampleRange(end))*dt*1e6; % global time array in ms


% each Board corrisponds to the same stick on the array
board1a = daq.a323(:,1:16); 
board1b = daq.a323(:,17:32);
board2a = daq.a324(:,1:16);
board2b = daq.a324(:,17:32);

board3a = daq.a325(:,1:16);
board3b = daq.a325(:,17:32);
board4a = daq.a326(:,1:16);  
board4b = daq.a326(:,17:32);

board5a = daq.a116(:,1:16);
board5b = daq.a116(:,17:32);
board6a = daq.a128(:,1:16);
board6b = daq.a128(:,17:32);

board7a = daq.a320(:,1:16);
board7b = daq.a320(:,17:32);
board8a = daq.a129(:,1:16);
board8b = daq.a129(:,17:32);

boards = {board1a board1b board2a board2b board3a board3b board4a board4b...
    board5a board5b board6a board6b board7a board7b board8a board8b};

tpoints = (1:size(board1a,1))*1e-1; % time elapsed over data range

titleName = {'1a' '1b' '2a' '2b' '3a' '3b' '4a' '4b' '5a' '5b' '6a' '6b'...
    '7a'  '7b' '8a' '8b'};

if PlotTimeTrace == 1
    figure(81), clf
    sgtitle(sprintf('Shot %d', shot))
    for i = 1:8
        subplot(2,4,i)
        for ch = 1:16           
            plot(boards{i}(:,ch)+0.1*ch)
            hold on
            title(sprintf('Board %s',titleName{i}))
            xlim(xlims)
            ylim([0 1.8])
        end
    end
    
    figure(82), clf
    sgtitle(sprintf('Shot %d', shot))
    k = 1;
    for i = 9:16
        subplot(2,4,k)
        
         for ch = 1:16
            % if i == 9 && ch == 8 % turn off bad dtaq channel
                % disp('board = %d, ch = %d', i, ch)
                % continue
            % end
            plot(boards{i}(:,ch)+0.1*ch)
            hold on
            title(sprintf('Board %s',titleName{i})) 
            xlim(xlims)
            ylim([0 1.8])
        end
        k = k+1;
    end
end

% stick1 furthest from manifold conflat joint
stick1 = [board1b board1a];
stick2 = [board2b board2a];
stick3 = [board3b board3a];
stick4 = [board4b board4a];
stick5 = [board5b board5a];
stick6 = [board6b board6a];
stick7 = [board7b board7a];
stick8 = [board8b board8a];

sticks = cat(3,stick1, stick2, stick3, stick4, stick5, stick6, stick7, stick8);


dBSetX = nan(size(sticks,1),10, 8);
dBSetZ = nan(size(sticks,1),10, 8);
dBSetY = nan(size(sticks,1),12, 8);

% Sticks 1 thru 8. Channels ordered from furthest to closest from manifold
for k = 1:8
    if k == 5 % Stick 5 was repaired, but 5 Bdot coils are broken
        dBSetX(:,:,k) = [sticks(:,29,k) sticks(:,31,k) sticks(:,22,k)...
            sticks(:,26,k) sticks(:,23,k) sticks(:,21,k) sticks(:,19,k)...
            sticks(:,17,k) sticks(:,18,k) sticks(:,13,k)];
        dBSetY(:,:,k) = [sticks(:,7,k) sticks(:,11:-1:8,k) sticks(:,30,k)...
            sticks(:,6:-1:1,k)];
        % dBSetY(:,:,k) = [sticks(:,26,k) sticks(:,22:1:25,k) sticks(:,3,k)...
        %     sticks(:,27:1:32,k)];
        dBSetZ(:,:,k) = [sticks(:,24,k) sticks(:,25,k) sticks(:,27,k)...
            sticks(:,18,k) sticks(:,28,k) sticks(:,16,k) sticks(:,20,k)...
            sticks(:,12,k) sticks(:,32,k) sticks(:,14,k)];

    elseif k == 7 % channels 7 and 21 swapped
        dBSetX(:,:,k) = [sticks(:,31:(-2):23,k)... 
            sticks(:,7,k) sticks(:,19:(-2):13,k)];
        dBSetZ(:,:,k) = sticks(:,32:(-2):14,k);
        dBSetY(:,:,k) = [sticks(:,12:(-1):8,k)...
            sticks(:,21,k) sticks(:,6:(-1):1,k)];
    elseif k == 8 % channels 14 and 29 swapped
        dBSetX(:,:,k) = [sticks(:,31,k) sticks(:,14,k)... 
            sticks(:,27:(-2):13,k)];
        dBSetZ(:,:,k) = [sticks(:,32:(-2):16,k) sticks(:,29,k)];
        dBSetY(:,:,k) = sticks(:,12:(-1):1,k);
    else
        dBSetX(:,:,k) = sticks(:,31:(-2):13,k);
        dBSetZ(:,:,k) = sticks(:,32:(-2):14,k);
        dBSetY(:,:,k) = sticks(:,12:(-1):1,k);
    end
end
 
% Reverse order such that first probe is closest to manifold
for stk = 1:8
    dBSetX(:,:,stk) = dBSetX(:, end:-1:1, stk);
    dBSetY(:,:,stk) = dBSetY(:, end:-1:1, stk);
    dBSetZ(:,:,stk) = dBSetZ(:, end:-1:1, stk);
end

%%
% Fix Bad Probes

% dBSetX(:,10,5) = (dBSetX(:,10,6) + dBSetX(:,10,4))/2; % rogowski coil connected to bad channel
% dBSetZ(:,6,5) = (dBSetX(:,6,4) + dBSetX(:,6,5))/2; % bad channel

% Stick 5
if shot < 64855
    dBSetX(:,9,5) = (dBSetX(:,8,5) + dBSetX(:,10,5))/2; % rogowski coil connected to bad channel
    dBSetX(:,4,5) = (dBSetX(:,3,5) + dBSetX(:,5,4))/2; % bad channel

    dBSetY(:,9,5) = (dBSetY(:,8,5) + dBSetY(:,10,5))/2; % bad channel

    dBSetZ(:,7,5) = (dBSetZ(:,6,5) + dBSetZ(:,8,5))/2; % bad channel
    dBSetZ(:,10,5) = (dBSetZ(:,9,5) + dBSetZ(:,10,4))/2; % bad channel
    dBSetZ(:,5,5) = (dBSetZ(:,4,5) + dBSetZ(:,6,4))/2; % This channel measured good, but it is giving a signal. Same channel with bad dBPhi calibration value
elseif shot >= 64855
    % New Stick 5 (after repair)
    dBSetZ(:,10,5) = (dBSetZ(:,9,5) + dBSetZ(:,10,4))/2; % bad channel
    dBSetZ(:,7,5) = (dBSetZ(:,6,5) + dBSetZ(:,8,5))/2; % bad channel
    dBSetX(:,2,5) = (dBSetX(:,1,5) + dBSetX(:,3,5))/2; % bad channel
end
% Sticks 1, 6, 8, 2
dBSetX(:,10,2) = (dBSetX(:,9,2) + dBSetX(:,10,1))/2; % Newly found bad channel 7Jun24
dBSetZ(:,2,6) = (dBSetZ(:,1,6) + dBSetZ(:,3,6))/2; % bad channel
dBSetX(:,9,6) = (dBSetX(:,8,6) + dBSetX(:,10,6))/2; % bad channel
% dBSetY(:,7,1) = (dBSetY(:,8,1) + dBSetY(:,6,1))/2; % bad channel
% dBSetX(:,9,8) = -dBSetX(:,9,8);
dum=dBSetX(:,9,8,:);
dBSetX(:,9,8,:)=dBSetZ(:,1,8,:);
dBSetZ(:,1,8,:)=dBSetX(:,1,8,:);
dBSetX(:,1,8,:)=dum;

%Collect dB data
dBSetRaw = {-dBSetX dBSetY dBSetZ};

% Plot dB raw data
if PlotTimeTrace == 1
    figNum = 91:93;
    dir = {'X' 'Y' 'Z'};
    ind = 0;
    multiple = 0.08;
    for Num = figNum
        ind = ind + 1;
        figure(Num), clf
        sgtitle(sprintf('dB %s-direction', dir{ind}))     
        for i = 1:8
            for ch = 1:10
            subplot(2,4,i)
            plot(dBSetRaw{ind}(:,ch,i)+multiple*ch)
            hold on
            ylim([0 (10*multiple+multiple)])
            xlim(xlims)
            title(sprintf('Stick %d', i))
            end
        end
    end
end


