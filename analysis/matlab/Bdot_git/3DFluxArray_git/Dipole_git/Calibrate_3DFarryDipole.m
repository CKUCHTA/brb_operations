% Orginizes probes in XYZ coordinates and give probe location in XYZ 
% coordinates where Y points North and X vertically up.  This also assumes
% the flux array is orientated with the manifold below the sticks (sticks
% pointing up) and sticks are parallel to the Y axis with stick 1 closest
% to the polar axis.
%
% In this orinetation the coordinate tranformation compared to when the
% probe is inserted from the north pole becomes:
% R --> X
% Phi --> Z 
% Z --> Y
%
% created 23Feb24

function [BSet, dBSet, ProbePos] = Calibrate_3DFarryDipole(IIoff, IIm, dt, dBSetRaw)
% clear
% shot = 62634;
% % SamRange=69000:71000;
% SamRange=69000:75000;
% xlims = [1000 2000];
% PlotTimeTrace = 1;
% saveData = 1;
% dt=1e-7; % time between samples
% [daq, ~] = loadDtaqData3DFarray(shot, saveData); % load dtac from harddrive or from mdsplus
% [boards, sticks, tpoints, dBSetRaw] = load3DFarrayDipole(daq, SamRange, dt, shot, PlotTimeTrace, xlims); 
% IIm=1000:1040; % times for evaluating the off-sets on the digitizers (before cap bank is triggered)
% IIoff=1000; %Makes Bz=Bh at this index

dBSetX = dBSetRaw{1};
dBSetY = dBSetRaw{2};
dBSetZ = dBSetRaw{3};

%Convertion Correction Factor for converting measured voltage to dB value 
AreaCorrectionFile = which('AreaCorrection.mat');
load(AreaCorrectionFile, 'RProbeAreas', 'PhiProbeAreas','ZProbeAreas');  % load Area correction factor from probeArea.m

facZ=(3.25e4/1.25)/4.4; % calibration factor converts the probe voltage signals to Bdot signals
DividerFac = 0.8199;% 0.861; % Voltage divider factor
facZCal = 1/(ZProbeAreas(3)*DividerFac);
error = 1 - (facZCal - facZ)/facZ; % percent difference of calculated from observed

facR = abs(1/(RProbeAreas(1)*DividerFac)*error); % 1/[(probeArea)*(voltage divider coeficient)]
facP = 1/(PhiProbeAreas(2)*DividerFac)*error;

% % Calculate Phi Correction Factor.  Use this section with a vacuum shot to
% cacluate and save DelFac.
% sample1a = 1040; %times before discharge
% sample1b = 1050;
% sample2a = 1060; %times at peak of discharge
% sample2b = 1070;
% 
% DelFac = nan(8,10);
% for stk = 1:8
%     for ch = 1:size(dBSetP,2) % loop over channels
%         DelPhi = mean(dBSetP(sample2a:sample2b, ch, stk)) - mean(dBSetP(sample1a:sample1b, ch, stk));
%         DelZ = mean(dBSetZ(sample2a:sample2b, ch, stk)) - mean(dBSetZ(sample1a:sample1b, ch, stk));
% 
%         DelFac(stk,ch) = DelPhi/DelZ;
%         dBSetP(:,ch,stk) = dBSetP(:,ch, stk) - DelPhi/DelZ*dBSetZ(:,ch,stk); % corrected phi
%     end
% end
% save '/Users/paulgradney/Research/Matlab Drive Cylinder/Bdot/3DFluxArray/PhiFac.mat' DelFac

% Use DelFac to correct Phi signals due to stick rotation
PhiFacFile = which('PhiFac.mat');
load(PhiFacFile, 'DelFac')
for stk = 1:8
    for ch = 1:size(dBSetZ,2) % loop over channels
        if stk == 5 && ch == 5 % stick 5 channel 5 is not calibrated properly
            continue
        end
        dBSetZ(:,ch,stk) = dBSetZ(:,ch, stk) - DelFac(stk,ch)*dBSetY(:,ch,stk); % corrected phi
    end
end


% Subtract off Digitizer and convert signal voltage to Bdot values
for stick = 1:8 
    dBX = dBSetX(:,:,stick); % R-dir for stick j
    dBZ = dBSetZ(:,:,stick);
    dBY = dBSetY(:,:,stick);
    for k=1:size(dBX,2)
        dBX(:,k) = dBX(:,k)-mean(dBX(IIm,k));
    end
    for k=1:size(dBZ,2)
        dBZ(:,k) = dBZ(:,k)-mean(dBZ(IIm,k));
    end
    for k=1:size(dBY,2)
        dBY(:,k) = dBY(:,k)-mean(dBY(IIm,k));
    end
    %reassign valus to dBset and multiply by convertion correction factor
    dBSetX(:,:,stick) = dBX*facR;
    dBSetZ(:,:,stick) = dBZ*facP;
    dBSetY(:,:,stick) = dBY*facZ;    
end

XmanifoldPos = -0.305; % x position of bottem of manifold (meters)
% X position of Y probes
Ydist = [0 5.05 10.15 15.25 20.35 25.4 30.45 35.55 40.15 45.75 50.8 55.9]; % distance between Y probes relative to probe closest to manifold
FirstProbeDist = 0.8 + 5; % distance from bottem of manifold to first probe in cm
YprobePos = (FirstProbeDist + Ydist)*1e-2; % distance each probe is from manifold in meters
dByXpos = XmanifoldPos + YprobePos; % dBy probe X position calculated from bottem of manifold (meters)
% dByXpos= dByXpos(end:(-1):1);

% X position of X probes
Xdist = [8.85 13.95 19.05 24.1 29.15 34.25 39.35 44.45 49.55 54.65]; % distance between X probes relative to probe closest to manifold
XprobePos = (FirstProbeDist + Xdist)*1e-2; % Probe Position realitive to manifold
dBxXpos = XmanifoldPos + XprobePos;
% dBxXpos= dBxXpos(end:(-1):1);

% X position of Z probes
Zdist = [11.35 16.45 21.55 26.6 31.7 36.75 41.85 46.95 52 57.1]; % distance between Z probes relative to probe closest to manifold
ZprobePos = (FirstProbeDist + Zdist)*1e-2; % Probe Position realitive to manifold; 
dBzXpos = XmanifoldPos + ZprobePos;
% dBzXpos= dBzXpos(end:(-1):1);

ProbePos = {dBxXpos dByXpos dBzXpos}; % collection of all radial probe locations

% Calculating B
BSetX = zeros(size(dBSetX));
BSetZ = zeros(size(dBSetZ));
BSetY = zeros(size(dBSetY));
tGapStart = 1853;
tGapStop = 1886;
for k = 1:8 
    % skip over time when drive coils turns on (high oscilation in Bdot signal
    % BSetX(1:tGapStart,:,k) = cumtrapz(dBSetX(1:tGapStart,:,k))*dt; % 
    % BSetX(tGapStop:end,:,k) = BSetX(tGapStart,:,k) + cumtrapz(dBSetX(tGapStop:end,:,k))*dt;
    
    BSetX(:,:,k) = cumtrapz(dBSetX(:,:,k))*dt; % Bp
    BSetZ(:,:,k) = cumtrapz(dBSetZ(:,:,k))*dt; % Bp
    BSetY(:,:,k) = cumtrapz(dBSetY(:,:,k))*dt; % Bz
end

% substracting off off-set to set B=0 before discharge
facZfile = which('facZ.mat');
load(facZfile, 'CalfacZ')  % load correction factor from ZCalibration3DFac.m 
for stk = 1:8
    Bx = BSetX(:,:,stk);
    Bz = BSetZ(:,:,stk);
    By = BSetY(:,:,stk);
    dBy = dBSetY(:,:,stk);
    facC = CalfacZ(:,stk);
    
    % IIoff is time when B components are forced to zero
    for ch=1:size(Bz,2)
        Bz(:,ch) = Bz(:,ch)-Bz(IIoff,ch);
        Bx(:,ch) = Bx(:,ch)-Bx(IIoff,ch); 
    end
    for ch=1:size(By,2)
        By(:,ch) = By(:,ch)-By(IIoff,ch);
        By(:,ch) = By(:,ch)*facC(ch); % calibrate field based on CalFacZ to smooth flux contours
        dBy(:,ch) = dBy(:,ch)*facC(ch); % calibrate field based on CalFacZ to smooth flux contours
    end
    BSetX(:,:,stk) = Bx;
    BSetZ(:,:,stk) = Bz;
    BSetY(:,:,stk) = By; 
    dBSetY(:,:,stk) = dBy; 
end

BSet = {BSetX BSetY BSetZ};
dBSet = {dBSetX dBSetY dBSetZ};

% Plot B raw data
%     figNum = 101:103;
%     dir = {'X' 'Y' 'Z'};
%     ind = 0;
%     for Num = figNum
%         ind = ind + 1;
%         figure(Num), clf
%         sgtitle(sprintf('B %s-direction Uncalabrated', dir{ind}))     
%         for i = 1:8
%             for ch = 1:10
%             subplot(2,4,i)
%             plot(BSet{ind}(:,ch,i)+0.005*ch)
%             hold on
%             % ylim([0 1.4])
%             xlim(xlims)
%             title(sprintf('Stick %d', i))
%             end
%         end
%     end


% BdataFile = sprintf('/Users/paulgradney/Research/Data/3DFluxArray_Calibration/Scripts/Bdata/Bdata_%d', shot);
% save(BdataFile, 'BSet', 'dBSet')
% BSet = {BrInt BSetP BSetZ};
% dBSet = {dBrCorrected dBSetZ dBSetY};

