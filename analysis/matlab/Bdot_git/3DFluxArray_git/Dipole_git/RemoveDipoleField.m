% Remove the dipole field from a plasma shot using its associated vacuum
% shot with only the dipole field.

function [BSetNoDipole, dBSetNoDipole, ProbePos, Bh] = RemoveDipoleField(VacShot, PlasmaShot, IIm, IIoff, dt, SampleRange, PlotTimeTrace, saveData, xlims)
% clear
% VacShot = 62608;
% PlasmaShot = 62604;

% IIm=901:980; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% IIoff=1000; %Makes Bz=Bh at this index
% dt=1e-7; % time between samples
% SampleRange=69000:72000;
% PlotTimeTrace = 0;
% saveData = 0;
% xlims = [1800 2200];

% Load Vacuum shot
[daq, Bh] = loadDtaqData3DFarray(VacShot, saveData, SampleRange); % load dtac from harddrive or from mdsplus
[~, ~, ~, dBSetRaw] = load3DFarrayDipole(daq, SampleRange, dt, VacShot, PlotTimeTrace, xlims);
[BSetVac, dBSetVac, ProbePos] = Calibrate_3DFarryDipole(IIoff, IIm, dt, dBSetRaw);

% Load Plasma shot
[daq, ~] = loadDtaqData3DFarray(PlasmaShot, saveData); % load dtac from harddrive or from mdsplus
[~, ~, ~, dBSetRaw] = load3DFarrayDipole(daq, SampleRange, dt, PlasmaShot, PlotTimeTrace, xlims);
[BSetPlasma, dBSetPlasma, ~] = Calibrate_3DFarryDipole(IIoff, IIm, dt, dBSetRaw);

BSetNoDipole = cell(size(BSetPlasma));
dBSetNoDipole = cell(size(BSetPlasma));

% Remove the dipole field from plasma shot
for ii = 1:length(BSetPlasma)
    BiVac = BSetVac{ii};
    BiPlasma = BSetPlasma{ii};
    BSetNoDipole{ii} = BiPlasma - BiVac;

    dBiVac = dBSetVac{ii};
    dBiPlasma = dBSetPlasma{ii};
    dBSetNoDipole{ii} = dBiPlasma - dBiVac;
end
  