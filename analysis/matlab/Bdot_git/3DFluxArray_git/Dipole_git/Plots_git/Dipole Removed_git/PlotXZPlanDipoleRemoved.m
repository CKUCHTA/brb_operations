% Creates a plot with the dipole vacuum field removed in the XZ plane based on 
% multiple shots scanning in the Z direction. The data for these plots is collected 
% using GetZscanData.m

function [XZPlanipoleRemovedFig, BSet, dBSet] = PlotXZPlanDipoleRemoved(PlasmaShotList, VacShotList, FolderName, SampleEvalActual, stick, SaveScanData, PlotPlane)
% clear
% PlasmaShotList = [62604 62617 62626 62634 62645 62657 62674 62688 62699 62712 62721 62733 62749 62757 62768 62779 62790 62802]; %plasma shots
% VacShotList =    [62610 62612 62631 62830 62654 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% FolderName = 'DipoleCusp2024';
% SampleEvalActual = 2000;
% stick = 5; % plot the plan created by the stick scanned in the Z direction
% SaveScanData = 1;


DataFolderStruct = what(FolderName);
DataFolder = DataFolderStruct.path;
FarrayFolder = fullfile(DataFolder, '3DFarray');
% Save figur files
PlotFolder = fullfile(FarrayFolder, 'Plots','XZPlan');

% FileName = sprintf('ZScanData_DipRM_Sample%d.mat', SampleEvalActual);
% DataFile = fullfile(FarrayFolder, 'Matlab_Data', 'ZScanData_DipoleRemoved',FileName); % file matlab data is saved
FileName = sprintf('ZScanData_Sample%d.mat', SampleEvalActual);
DataFile = fullfile(FarrayFolder, 'Matlab_Data', 'ZScanData',FileName); % file matlab data is saved
try
    disp('Loading data')
    load(DataFile, 'BSetZdata', 'dBSetZdata', 'ZposArray', 'SampleEval', 'ProbePos')
    if SampleEvalActual ~= SampleEval
        disp('Data for sample point note created yet')
        % [BSetZdata, dBSetZdata, ZposArray, ProbePos, ~] = GetZscanDataDipoleRemoved(FolderName, SampleEvalActual, SaveScanData);
        [BSetZdata, dBSetZdata, ZposArray, ProbePos, ~, ~] = GetZscanData(FolderName, SampleEvalActual, SaveScanData);
    end
catch
    disp('running GetZscanData')
    % [BSetZdata, dBSetZdata, ZposArray, ProbePos, ~] = GetZscanDataDipoleRemoved(FolderName, SampleEvalActual, SaveScanData);
    [BSetZdata, dBSetZdata, ZposArray, ProbePos, ~, ~] = GetZscanData(FolderName, SampleEvalActual, SaveScanData);
end
BSetXall = BSetZdata{1}; %  (probe, stick, z indx))
BSetYall = BSetZdata{2};
BSetZall = BSetZdata{3};

dBSetXall = dBSetZdata{1};
dBSetYall = dBSetZdata{2};
dBSetZall = dBSetZdata{3};

BSetX = zeros(size(BSetXall, 1), size(BSetXall, 2), length(PlasmaShotList));
BSetY = zeros(size(BSetYall, 1), size(BSetYall, 2), length(PlasmaShotList));
BSetZ = zeros(size(BSetZall, 1), size(BSetZall, 2), length(PlasmaShotList));

dBSetX = zeros(size(dBSetXall, 1), size(dBSetXall, 2), length(PlasmaShotList));
dBSetY = zeros(size(dBSetYall, 1), size(dBSetYall, 2), length(PlasmaShotList));
dBSetZ = zeros(size(dBSetZall, 1), size(dBSetZall, 2), length(PlasmaShotList));

% extract the data for each shot from PlasmaShotList and subtract associated vacuum shot
ZposVec = zeros(length(PlasmaShotList), 1); % vector to store the Z position of each shot
for ii = 1:length(PlasmaShotList)
    VacShot = VacShotList(ii);
    PlasmaShot = PlasmaShotList(ii);
    PlasmaShotIndex = find(ZposArray(:,1) == PlasmaShot);
    VacShotIndex = find(ZposArray(:,1) == VacShot);
    ZposVec(ii) = ZposArray(PlasmaShotIndex, 2);
    
    BSetX(:,:,ii) = BSetXall(:,:,PlasmaShotIndex) - BSetXall(:,:,VacShotIndex);
    BSetY(:,:,ii) = BSetYall(:,:,PlasmaShotIndex) - BSetYall(:,:,VacShotIndex);
    BSetZ(:,:,ii) = BSetZall(:,:,PlasmaShotIndex) - BSetZall(:,:,VacShotIndex);

    dBSetX(:,:,ii) = dBSetXall(:,:,PlasmaShotIndex) - dBSetXall(:,:,VacShotIndex);
    dBSetY(:,:,ii) = dBSetYall(:,:,PlasmaShotIndex) - dBSetYall(:,:,VacShotIndex);
    dBSetZ(:,:,ii) = dBSetZall(:,:,PlasmaShotIndex) - dBSetZall(:,:,VacShotIndex);
end

BSet = {BSetX BSetY BSetZ};
dBSet = {dBSetX dBSetY dBSetZ};

% Plot Parameters
label = {'x' 'y' 'z'};

% pcolor bar limits for sticks 1-8
climBx = {[-1.5 1.5] [-1.5 1.5] [-1.5 1.5] [-1 1] [-1.5 1.5] [-1.5 1.5] [-1.5 1.5] [-1.5 1.5]};
climDBx = {[-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-100 100]};

climBy = {[-4.5 1.5] [-4.5 1.5] [-4.5 1.5] [-1.5 0] [-3.5 1.5] [-2.5 1] [-4.5 1.5] [-4.5 1.5]};
climDBy = {[-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-250 50] [-100 100] [-100 100]};

climBz = {[-2 2] [-2 2] [-2 2] [-3 0] [-2 2] [-2 0.5] [-2 2] [-2 2]};
climDBz = {[-200 200] [-200 200] [-200 200] [-200 200] [-200 200] [-200 200] [-200 200] [-200 200]};

Bclims = {climBx{stick} climBy{stick} climBz{stick}}; % pcolor limits for B
dBclims = {climDBx{stick} climDBy{stick} climDBz{stick}};

XZPlanipoleRemovedFig = figure(22);
clf
if PlotPlane == 1
    time = sprintf('%0.1f\\mus', SampleEvalActual*1e-1);
    sgtitle({'Dipole Removed, Stick = ' + string(stick) + ', t = ' + time, strjoin(string(PlasmaShotList))})
    ind = 0;
    for k = 1:6
        if k == 1||k ==2||k ==3
            B = squeeze(BSet{k}(:,stick,:));
            subplot(2,3,k)
            pcolor(ZposVec,ProbePos{k},B*1e3), shading interp
            % pcolor(B*1e3), shading interp
            colorbar
            xlabel('Z[m]')
            ylabel('X[m]')
            title(sprintf('B%s[mT]', label{k}))
            clim(Bclims{k})
        else
            ind = ind + 1;
            % dB = squeeze(dBSet{k}(SamplePoint,:,:));
            dBSetk = dBSet{ind};
            dB = squeeze(dBSetk(:,stick,:));
            if ind == 3
                % dB(3,5) = -dB(3,5);
            end
            subplot(2,3,k)
            pcolor(ZposVec,ProbePos{ind},dB), shading interp
            % pcolor(dB), shading interp
            colorbar
            xlabel('Z[m]')
            ylabel('X[m]')
            title(sprintf('dB%s', label{ind}))
            clim(dBclims{ind})
        end
    end
    set(gcf, 'Position',  [200, 100, 1000, 550])
end
close(XZPlanipoleRemovedFig)