% Plot data from scan of 3Dfluxarray at a specifid sample point using GetZscanData
% Only shot with only the drive coils on are chosen.

% function PlotZscanDataiDriveCoilsOnly(FolderName, SampleEvalActual, ShotList)
SampleEvalActual = 1900; % sample point plots are evaluated at
SavePlots = 1; % 1 to save plots, 0 to not save
FolderName = 'DipoleCusp2024';
SaveData = 0; % Save data from GetZScanData

% Get data folder
DataFolderStruct = what(FolderName);
DataFolder = DataFolderStruct.path;
FarrayFolder = fullfile(DataFolder, '3DFarray');
% Save figur files
PlotFolder = fullfile(FarrayFolder, 'Plots', 'Z_ScanTimeTrace_DriveCoils');
if ~exist(PlotFolder, 'dir')
    mkdir(PlotFolder)
end

BxPlotFile = fullfile(PlotFolder, 'BxScan_DriveCoils.png');
ByPlotFile = fullfile(PlotFolder, 'ByScan_DriveCoils.png');
BzPlotFile = fullfile(PlotFolder, 'BzScan_DriveCoils.png');    

dBxPlotFile = fullfile(PlotFolder, 'dBxScan_DriveCoils.png');
dByPlotFile = fullfile(PlotFolder, 'dByScan_DriveCoils.png');
dBzPlotFile = fullfile(PlotFolder, 'dBzScan_DriveCoils.png');


ShotList =   [62575 62818 62628 62640 62650 62661 62678 62694 62705 62716 62727 62738 62751 62762 62775 62784 62796 62812]; %plasma shots
% ShotList = []; % dipole vacuum

% Get data
FileName = sprintf('ZScanData_Sample%d.mat', SampleEvalActual);
DataFile = fullfile(FarrayFolder, 'Matlab_Data', 'ZScanData', FileName); % file matlab data is saved

try
    disp('Loading data')
    load(DataFile, 'BSetZdata', 'dBSetZdata', 'ZposArray', 'SampleEval', 'ProbePos', 'tpoints')
    if SampleEvalActual ~= SampleEval
        disp('Data for sample point note created yet')
        [BSetZdata, dBSetZdata, ZposArray, ProbePos, tpoints] = GetZscanData(FolderName, SampleEvalActual, SaveData);
    end
catch
    disp('running GetZscanData')
    [BSetZdata, dBSetZdata, ZposArray, ProbePos, tpoints] = GetZscanData(FolderName, SampleEvalActual, SaveData);
end

BSetXall = BSetZdata{1};
BSetYall = BSetZdata{2};
BSetZall = BSetZdata{3};

dBSetXall = dBSetZdata{1};
dBSetYall = dBSetZdata{2};
dBSetZall = dBSetZdata{3};

BSetX = zeros(size(BSetXall, 1), size(BSetXall, 2), length(ShotList));
BSetY = zeros(size(BSetYall, 1), size(BSetYall, 2), length(ShotList));
BSetZ = zeros(size(BSetZall, 1), size(BSetZall, 2), length(ShotList));

dBSetX = zeros(size(dBSetXall, 1), size(dBSetXall, 2), length(ShotList));
dBSetY = zeros(size(dBSetYall, 1), size(dBSetYall, 2), length(ShotList));
dBSetZ = zeros(size(dBSetZall, 1), size(dBSetZall, 2), length(ShotList));

% extract the data for each shot from ShotList
ZposVec = zeros(length(ShotList), 1); % vector to store the Z position of each shot
for ii = 1:length(ShotList)
    Shot = ShotList(ii);
    ShotIndex = find(ZposArray(:,1) == Shot);
    ZposVec(ii) = ZposArray(ShotIndex, 2);
    
    BSetX(:,:,ii) = BSetXall(:,:,ShotIndex);
    BSetY(:,:,ii) = BSetYall(:,:,ShotIndex);
    BSetZ(:,:,ii) = BSetZall(:,:,ShotIndex);

    dBSetX(:,:,ii) = dBSetXall(:,:,ShotIndex);
    dBSetY(:,:,ii) = dBSetYall(:,:,ShotIndex);
    dBSetZ(:,:,ii) = dBSetZall(:,:,ShotIndex);
end
%%
% plot dB data
dBxPlot = figure(91);
clf
sgtitle(sprintf('dBx Drive Coils Only, Sample = %d', SampleEvalActual))     
for stk = 1:8
    for ch = 1:10
    subplot(2,4,stk)
    offset = 100*ch;
    % if stk == 5 && ch == 8 || ch == 7 || ch == 6
    %     continue
    % end
    plot(ZposVec*0.01,squeeze(dBSetX(ch,stk,:))+offset)
    hold on
    ylim([-400 1200])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end

dByPlot = figure(92);
clf
sgtitle(sprintf('dBy Drive Coils Only, Sample = %d', SampleEvalActual))
for stk = 1:8
    for ch = 1:10
    subplot(2,4,stk)
    offset = 100*ch;
    plot(ZposVec*0.01,squeeze(dBSetY(ch,stk,:))+offset)
    hold on
    ylim([-400 1200])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end

dBzPlot = figure(93);
clf
sgtitle(sprintf('dBz Drive Coils Only, Sample = %d', SampleEvalActual))
for stk = 1:8
    for ch = 1:10
    subplot(2,4,stk)
    offset = 100*ch;
    plot(ZposVec*0.01,squeeze(dBSetZ(ch,stk,:))+offset)
    hold on
    ylim([-400 1200])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end

%%
%Plot B data
BxPlot = figure(81);
clf
sgtitle(sprintf('Bx Drive Coils Only, Sample = %d', SampleEvalActual))     
for stk = 1:8
    for ch = 1:10
    subplot(2,4,stk)
    offset = 1.1*ch;
    plot(ZposVec*0.01,(squeeze(BSetX(ch,stk,:))*1e3)+offset)
    hold on
    ylim([0 14])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end
%%
ByPlot = figure(82);
clf
sgtitle(sprintf('By Drive Coils Only, Sample = %d', SampleEvalActual))
for stk = 1:8
    for ch = 1:12
    subplot(2,4,stk)
    offset = 2*ch;
    % if stk == 5 && ch == 7
    %    continue
    % end
    plot(ZposVec*0.01,squeeze(BSetY(ch,stk,:)*1e3)+offset)
    hold on
    % ylim([-5 22])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end
%%
BzPlot = figure(83);
clf
sgtitle(sprintf('Bz Drive Coils Only, Sample = %d', SampleEvalActual));
for stk = 1:8
    for ch = 1:10
    subplot(2,4,stk)
    offset = 2*ch;
    % if stk == 5 && ch==6
    %    continue
    % end
    plot(ZposVec*0.01,squeeze(BSetZ(ch,stk,:)*1e3)+offset)
    hold on
    ylim([-5 22])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end

if SavePlots == 1
    saveas(BxPlot, BxPlotFile)
    saveas(ByPlot, ByPlotFile)
    saveas(BzPlot, BzPlotFile)
    saveas(dBxPlot, dBxPlotFile)
    saveas(dByPlot, dByPlotFile)
    saveas(dBzPlot, dBzPlotFile)
end
