% Creates a plot in the XZ plane based on multiple shots scanning in the Z direction.
% The data for these plots is collected using GetZscanData.m

function [XZPlanDriveCoilOnlyFig, BSet, dBSet] = PlotXZPlanDriveCoilsOnly(ShotList, FolderName, SampleEvalActual, stick, SaveData, PlotPlane)
% clear
% ShotList =   [62575 62818 62628 62640 62650 62661 62678 62694 62705 62716 62727 62738 62751 62762 62775 62784 62796 62812]; % drive coils plasma shots
% FolderName = 'DipoleCusp2024';
% SampleEvalActual = 2000;
% stick = 5; % plot the plan created by the stick scanned in the Z direction
% SaveData = 1;


DataFolderStruct = what(FolderName);
DataFolder = DataFolderStruct.path;
FarrayFolder = fullfile(DataFolder, '3DFarray');
% Save figur files
PlotFolder = fullfile(FarrayFolder, 'Plots','XZPlan_DriveCoils');
if ~exist(PlotFolder, 'dir')
    mkdir(PlotFolder)
end 

FileName = sprintf('ZScanData_Sample%d.mat', SampleEvalActual);
DataFile = fullfile(FarrayFolder, 'Matlab_Data', 'ZScanData',FileName); % file matlab data is saved
try
    disp('Loading data')
    load(DataFile, 'BSetZdata', 'dBSetZdata', 'ZposArray', 'SampleEval', 'ProbePos', 'tpoints')
    if SampleEvalActual ~= SampleEval
        disp('Data for sample point note created yet')
        [BSetZdata, dBSetZdata, ZposArray, ProbePos, tpoints] = GetZscanData(FolderName, SampleEvalActual, SaveData);
    end
catch
    disp('running GetZscanData')
    [BSetZdata, dBSetZdata, ZposArray, ProbePos, tpoints] = GetZscanData(FolderName, SampleEvalActual, SaveData);
end

BSetXall = BSetZdata{1}; %  (probe, stick, z indx))
BSetYall = BSetZdata{2};
BSetZall = BSetZdata{3};

dBSetXall = dBSetZdata{1};
dBSetYall = dBSetZdata{2};
dBSetZall = dBSetZdata{3};

BSetX = zeros(size(BSetXall, 1), size(BSetXall, 2), length(ShotList));
BSetY = zeros(size(BSetYall, 1), size(BSetYall, 2), length(ShotList));
BSetZ = zeros(size(BSetZall, 1), size(BSetZall, 2), length(ShotList));

dBSetX = zeros(size(dBSetXall, 1), size(dBSetXall, 2), length(ShotList));
dBSetY = zeros(size(dBSetYall, 1), size(dBSetYall, 2), length(ShotList));
dBSetZ = zeros(size(dBSetZall, 1), size(dBSetZall, 2), length(ShotList));

% extract the data for each shot from ShotList
ZposVec = zeros(length(ShotList), 1); % vector to store the Z position of each shot
for ii = 1:length(ShotList)
    Shot = ShotList(ii);
    ShotIndex = find(ZposArray(:,1) == Shot);
    ZposVec(ii) = ZposArray(ShotIndex, 2);
    
    BSetX(:,:,ii) = BSetXall(:,:,ShotIndex);
    BSetY(:,:,ii) = BSetYall(:,:,ShotIndex);
    BSetZ(:,:,ii) = BSetZall(:,:,ShotIndex);

    dBSetX(:,:,ii) = dBSetXall(:,:,ShotIndex);
    dBSetY(:,:,ii) = dBSetYall(:,:,ShotIndex);
    dBSetZ(:,:,ii) = dBSetZall(:,:,ShotIndex);
end

BSet = {BSetX BSetY BSetZ};
dBSet = {dBSetX dBSetY dBSetZ};

% Plot Parameters
label = {'x' 'y' 'z'};

climBx = [-1.5 1.5];
climDBx = [-1 1]*1e2;

climBy = [-3.5 1.5];%[-1.5 0];
climDBy = [-2 2]*1e2;

climBz = [-3 -0.5];
climDBz = [-2 2]*1e2;

Bclims = {climBx climBy climBz}; % pcolor limits for B
dBclims = {climDBx climDBy climDBz};

XZPlanDriveCoilOnlyFig = figure(2);
clf
if PlotPlane == 1
    time = sprintf('%0.1f\\mus', SampleEvalActual*1e-1);
    sgtitle({'Drive Coils Only, Stick = ' + string(stick) + ', t = ' + time, strjoin(string(ShotList))})
    ind = 0;
    for k = 1:6
        if k == 1||k ==2||k ==3
            B = squeeze(BSet{k}(:,stick,:));
            subplot(2,3,k)
            pcolor(ZposVec,ProbePos{k},B*1e3), shading interp
            % pcolor(B*1e3), shading interp
            colorbar
            xlabel('Z[m]')
            ylabel('X[m]')
            title(sprintf('B%s[mT]', label{k}))
            clim(Bclims{k})
        else
            ind = ind + 1;
            % dB = squeeze(dBSet{k}(SamplePoint,:,:));
            dBSetk = dBSet{ind};
            dB = squeeze(dBSetk(:,stick,:));
            if ind == 3
                % dB(3,5) = -dB(3,5);
            end
            subplot(2,3,k)
            pcolor(ZposVec,ProbePos{ind},dB), shading interp
            % pcolor(dB), shading interp
            colorbar
            xlabel('Z[m]')
            ylabel('X[m]')
            title(sprintf('dB%s', label{ind}))
            clim(dBclims{ind})
        end
    end
    set(gcf, 'Position',  [200, 100, 1000, 550])
end
close(XZPlanDriveCoilOnlyFig)