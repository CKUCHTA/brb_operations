% Plot flux Array data using load3DFarray.  Helmholtz Field is added to Bz component that is
% in the BSet output.
% created 20Nov2023

% function  [BSet, dBSet]= Plot3DFarryXYZ(shot, savePlot, saveData, PlotTimeTrace, xlims, SampleRange, webSave, PlotPcolor)
clear
shot = 64864;
% shot = 62731;
savePlot = 0;
saveData = 1;
IncludeContour = 0; % set to 1 to plot flux contours (Unless you are using interpolated data, these contours are wrong!!!)
PlotFlux = 0    ; % set to 1 to plot flux
PlotTimeTrace = 1; % set to 1 for raw data time trace
xlims = [1800 2200]; % plot range for sample points
SampleRange=69000:73000;
webSave = 0;
PlotPcolor = 1; % set to 1 to plot figures

SetFigurePos = 0; % set to 1 to orginize figures on screen

[daq, Bh] = loadDtaqData3DFarray(shot, saveData, SampleRange); % load dtac from harddrive or from mdsplus

dt=1e-7; % time between samples
[~, ~, tpoints, dBSetRaw] = load3DFarrayDipole(daq, SampleRange, dt, shot, PlotTimeTrace, xlims); 

if PlotTimeTrace == 1
    return % Only plot raw data time trace
end


IIm=901:980; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIoff=1020; %Makes Bz=Bh at this index
[BSet, dBSet, ProbePos] = Calibrate_3DFarryDipole(IIoff, IIm, dt, dBSetRaw);

dBSetX = dBSet{1};
dBSetY = dBSet{2};
dBSetZ = dBSet{3};

BSetX = BSet{1};
BSetY = BSet{2};
BSetZ = BSet{3} + Bh; % add Helmholtz field to Bz component
BSet = {BSetX, BSetY, BSetZ};

% Probe X Positions
dBxXpos = ProbePos{1};
dByXpos = ProbePos{2};
dBzXpos = ProbePos{3};

Stick1Ypos = 0.03; % stick 1 y position (meters)
StickPos = Stick1Ypos + (0:(-12):-84)*1e-2; % stick positions

ShotFolderStruct = what('DipoleCusp2024');
ShotFolder = ShotFolderStruct.path;
PlotFolder = fullfile(ShotFolder,'3DFarray/Plots/VerticalPlots');
PlotFile1to4X = fullfile(PlotFolder,sprintf('fArrayPlot_%d_X_Stk1to4.png', shot));
PlotFile5to8X = fullfile(PlotFolder,sprintf('fArrayPlot_%d_X_Stk5to8.png', shot));

PlotFile1to4Y = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Y_Stk1to4.png', shot));
PlotFile5to8Y = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Y_Stk5to8.png', shot));

PlotFile1to4Z = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Z_Stk1to4.png', shot));
PlotFile5to8Z = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Z_Stk5to8.png', shot));

% Use Excel Spreadsheet data to get distance between dipole and flux array
excelFileName = 'DipoleCusp2024.xlsx';
DataFolderName = 'DipoleCusp2024'; % folder name excel file will be saved to
docid = '1YbMnfhPIyXe3SAJL9hR5xkr6GhluWjYWQPfE1WV9LpA'; % from google sheets url
[shotData, ~] = DipoleShotData(excelFileName, DataFolderName, docid, webSave);
shotIndex = find(shotData(:,1) == shot);
DipolePos = shotData(shotIndex,4);
FluxArrayPos = shotData(shotIndex,3);

DipoleFlux_Distance = abs(DipolePos - FluxArrayPos)*1e-2; % distance between flux array and dipole
FluxToCoil_Distance = (15 - FluxArrayPos)*1e-2;

%%
%Plotting parameters
tlims = tpoints(xlims);

climBx = [-2 2];
climDBx = [-0.5 0.5]*1e3;

climBy = [-3 3];
climDBy = [-0.8 0.8]*1e3;

climBz = [-10 -2];
climDBz = [-1 1]*1e3;

%%
% By field Plots for Sticks 1-4
if PlotPcolor == 1
    fluxArrayPlot1to4_Z = figure(55);
    clf
    sgtitle(sprintf('Shot %d, 3DFArray, B_h = %0.1f[mT],  \\DeltaZ_{Flux to Dip} = %0.2fm, \\DeltaZ_{Flux to Coil} = %0.2fm',...
        shot, Bh*1e3, DipoleFlux_Distance, FluxToCoil_Distance))
    k = 0;
    for i = 1:4
        k = k+2;
        subplot(4,2,k)
        pcolor(tpoints, dByXpos,BSetY(:,:,i)'*1e3),shading interp
        % pcolor(BSetY(:,:,i)'*1e3),shading interp
        colorbar
        clim(climBy)
        xlim(tlims)
        % xlim(xlims)
        title(sprintf('By[mT] Stick %d', i))
        ylabel('X [m]')
        xlabel('t [\mus]')
    end

    % dBy field Plots for Sticks 1-4
    k = 1;
    for i = 1:4
        subplot(4,2,k)
        pcolor(tpoints, dByXpos,dBSetY(:,:,i)'),shading interp
        % pcolor(dBSetY(:,:,i)'),shading interp
        colorbar
        clim(climDBy)
        xlim(tlims)
        % xlim(xlims)
        title(sprintf('dBy[T/s] Y = %0.2fm', StickPos(i)))
        ylabel('X [m]')
        xlabel('t [\mus]')
        k = k+2;
    end
    if SetFigurePos == 1
        set(gcf, 'Position',  [-50, 100, 650, 900])
        fontsize(gcf, 16,'points')
    end
    %%


    % By field Plots for Sticks 5-8
    fluxArrayPlot5to8_Z = figure(56);
    clf
    sgtitle(sprintf('Shot %d, 3DFArray, B_h = %0.1f[mT],  \\DeltaZ_{Flux to Dip} = %0.2fm, \\DeltaZ_{Flux to Coil} = %0.2fm',...
        shot, Bh*1e3, DipoleFlux_Distance, FluxToCoil_Distance))
    k = 0;
    for i = 5:8
        k = k+2;
        subplot(4,2,k)
        pcolor(tpoints, dByXpos,BSetY(:,:,i)'*1e3),shading interp
        % pcolor(BSetY(:,:,i)'*1e3),shading interp
        colorbar
        clim(climBy)
        xlim(tlims)
        % xlim(xlims)
        title(sprintf('By[mT] Stick %d', i))
        ylabel('X [m]')
        xlabel('t [\mus]')
    end

    % dBy field Plots for Sticks 5-8
    k = 1;
    for i = 5:8
        subplot(4,2,k)
        pcolor(tpoints, dByXpos,dBSetY(:,:,i)'),shading interp
        % pcolor(dBSetY(:,:,i)'),shading interp
        colorbar
        clim(climDBy) 
        xlim(tlims)
        % xlim(xlims)
        title(sprintf('dBy[T/s] Y = %0.2fm', StickPos(i)))
        ylabel('X [m]')
        xlabel('t [\mus]')
        k = k+2;
    end
    if SetFigurePos == 1
        set(gcf, 'Position',  [200, 100, 650, 900])
        fontsize(gcf, 16,'points')
    end

    % Bx field Plots for Sticks 1-4
    fluxArrayPlot1to4_R = figure(65);
    clf
    sgtitle(sprintf('Shot %d, 3DFArray, B_h = %0.1f[mT],  \\DeltaZ_{Flux to Dip} = %0.2fm, \\DeltaZ_{Flux to Coil} = %0.2fm',...
        shot, Bh*1e3, DipoleFlux_Distance, FluxToCoil_Distance))
    k = 0;
    for i = 1:4
        k = k+2;
        subplot(4,2,k)
        pcolor(tpoints, dBxXpos,BSetX(:,:,i)'*1e3),shading interp
        hold on
        % contour(tpoints, dBzXpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5)
        colorbar
        clim(climBx)
        xlim(tlims)
        title(sprintf('Bx[mT] Stick %d', i))
        ylabel('X [m]')
        xlabel('t [\mus]')
    end



    % dBx field Plots for Sticks 1-4
    k = 1;
    for i = 1:4
        subplot(4,2,k)
        pcolor(tpoints, dBxXpos,dBSetX(:,:,i)'),shading interp
        hold on
        % contour(tpoints, dBzXpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5)
        colorbar
        clim(climDBx)
        xlim(tlims)
        title(sprintf('dBx[T/s] Y = %0.2fm', StickPos(i)))
        ylabel('X [m]')
        xlabel('t [\mus]')
        k = k+2;
    end
    if SetFigurePos == 1
        set(gcf, 'Position',  [450, 100, 650, 900])
        fontsize(gcf, 16,'points')
    end


    % Bx field Plots for Sticks 5-8
    fluxArrayPlot5to8_R = figure(66);
    clf
    sgtitle(sprintf('Shot %d, 3DFArray, B_h = %0.1f[mT],  \\DeltaZ_{Flux to Dip} = %0.2fm, \\DeltaZ_{Flux to Coil} = %0.2fm',...
        shot, Bh*1e3, DipoleFlux_Distance, FluxToCoil_Distance))
    k = 0;
    for i = 5:8
        k = k+2;
        subplot(4,2,k)
        pcolor(tpoints, dBxXpos,BSetX(:,:,i)'*1e3),shading interp
        hold on
        % contour(tpoints, dBzXpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5)
        colorbar
        clim(climBx)
        xlim(tlims)
        title(sprintf('Bx[mT] Stick %d', i))
        ylabel('X [m]')
        xlabel('t [\mus]')
    end

    % dBx field Plots for Sticks 5-8
    k = 1;
    for i = 5:8
        subplot(4,2,k)
        pcolor(tpoints, dBxXpos,dBSetX(:,:,i)'),shading interp
        hold on
        % contour(tpoints, dBzXpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
        colorbar
        clim(climDBx)
        xlim(tlims)
        title(sprintf('dBx[T/s] Y = %0.2fm', StickPos(i)))
        ylabel('X [m]')
        xlabel('t [\mus]')
        k = k+2;
    end
    if SetFigurePos == 1
        set(gcf, 'Position',  [700, 100, 650, 900])
        fontsize(16,'points')
    end


    % Bz field Plots for Sticks 1-4
    fluxArrayPlot1to4_P = figure(75);
    clf
    sgtitle(sprintf('Shot %d, 3DFArray, B_h = %0.1f[mT],  \\DeltaZ_{Flux to Dip} = %0.2fm, \\DeltaZ_{Flux to Coil} = %0.2fm',...
        shot, Bh*1e3, DipoleFlux_Distance, FluxToCoil_Distance))
    k = 0;
    for i = 1:4
        k = k+2;
        subplot(4,2,k)
        pcolor(tpoints, dBzXpos,BSetZ(:,:,i)'*1e3),shading interp
        hold on
        % contour(tpoints, dBzXpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
        colorbar
        clim(climBz)
        xlim(tlims)
        title(sprintf('Bz[mT] Stick %d', i))
        ylabel('X [m]')
        xlabel('t [\mus]')
    end

    % dBz field Plots for Sticks 1-4
    k = 1;
    for i = 1:4
        subplot(4,2,k)
        pcolor(tpoints, dBzXpos,dBSetZ(:,:,i)'),shading interp
        hold on
        % contour(tpoints, dBzXpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
        colorbar
        clim(climDBz)
        xlim(tlims)
        title(sprintf('dBz[T/s] Y = %0.2fm', StickPos(i)))
        ylabel('X [m]')
        xlabel('t [\mus]')
        k = k+2;
    end
    if SetFigurePos == 1
        set(gcf, 'Position',  [950, 100, 650, 900])
        fontsize(16,'points')
    end



    % Bz field Plots for Sticks 5-8
    fluxArrayPlot5to8_P = figure(76);
    clf
    sgtitle(sprintf('Shot %d, 3DFArray, B_h = %0.1f[mT],  \\DeltaZ_{Flux to Dip} = %0.2fm, \\DeltaZ_{Flux to Coil} = %0.2fm',...
        shot, Bh*1e3, DipoleFlux_Distance, FluxToCoil_Distance))
    k = 0;
    for i = 5:8
        k = k+2;
        subplot(4,2,k)
        pcolor(tpoints, dBzXpos,BSetZ(:,:,i)'*1e3),shading interp
        hold on
        % contour(tpoints, dBzXpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
        colorbar
        clim(climBz)
        xlim(tlims)
        title(sprintf('Bz[mT] Stick %d', i))
        ylabel('X [m]')
        xlabel('t [\mus]')
    end

    % dBz field Plots for Sticks 5-8
    k = 1;
    for i = 5:8
        subplot(4,2,k)
        pcolor(tpoints, dBzXpos,dBSetZ(:,:,i)'),shading interp
        hold on
        % contour(tpoints, dBzXpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
        colorbar
        clim(climDBz) 
        xlim(tlims)
        title(sprintf('dBz[T/s] Y = %0.2fm', StickPos(i)))
        ylabel('X [m]')
        xlabel('t [\mus]')
        k = k+2;
    end
    if SetFigurePos == 1
        set(gcf, 'Position',  [1200, 100, 650, 900])
        fontsize(gcf, 16,'points')
    end

    if savePlot == 1
        saveas(fluxArrayPlot1to4_Z, PlotFile1to4Z)
        saveas(fluxArrayPlot5to8_Z, PlotFile5to8Z)
        
        saveas(fluxArrayPlot1to4_R, PlotFile1to4X)
        saveas(fluxArrayPlot5to8_R, PlotFile5to8X)
        
        saveas(fluxArrayPlot1to4_P, PlotFile1to4Y)
        saveas(fluxArrayPlot5to8_P, PlotFile5to8Y)
    end
end
return
%%

%dB and B Plots for a single stick 
stick = 4;

dB_stk = dBSetZ(:,:,stick)';
B_stk= BSetZ(:,:,stick)';
climGen = climBz;
climGenDT = climDBz;
xlims = [950 1550];


figure(57), clf 
subplot(1,2,1)
% pcolor(tpoints, ZprobeRpos,dBz_stk'),shading interp
pcolor(dB_stk),shading interp
hold on
contour(BSetZ(:,1:10,stick)'*1e3,[0 0],'r', 'linewidth', 2),shading interp
if IncludeContour == 1
    contour(Phi(stick).flux', phiContour1,'k', 'Linewidth', 1.2)
    contour(Phi(stick).flux', phiContour2,'k')
end
colorbar
clim(climGenDT)
% xlim(tlims)
xlim(xlims)
title(sprintf('dBz Stick %d', stick))
ylabel('X [m]')
xlabel('t [\mus]')



subplot(1,2,2)
% pcolor(tpoints, ZprobeRpos,Bz_stk*1e3),shading interp
pcolor(B_stk*1e3),shading interp
hold on
% contour(tpoints, ZprobeRpos,Bz_stk*1e3,[0 0],'r','linewidth', 2),shading interp
contour(BSetZ(:,1:10,stick)'*1e3,[0 0],'r', 'linewidth', 2),shading interp
if IncludeContour == 1
    contour(Phi(stick).flux', phiContour1,'k', 'Linewidth', 1.2)
    contour(Phi(stick).flux', phiContour2,'k')
end
colorbar
clim(climGen)
xlim(xlims)
title(sprintf('Bz[mT] Stick %d', stick))
ylabel('X [m]')
xlabel('t [\mus]')


%%
% dBz plot for a single channel
StickNum = 5;
ProbeNum = 1;
% xlims = [950 1550];

Rpos = dBxXpos(ProbeNum);

SingleSig = figure(58);
clf
sgtitle(sprintf('Stick %d, R = %0.2f', StickNum, Rpos))
% subplot(3,1,1)
dBset1 = dBSetX(:,ProbeNum,StickNum);
plot(tpoints,dBset1)
% xlim(tlims)
% ylim([-2.2 2.2]*1e3)
% xlabel('Samples')
xlabel('t[\mus]')
ylabel('dB_z[T/s]')

% subplot(3,1,2)
% Bset1 = BSetX(:,ProbeNum,StickNum);
% % plot(tpoints,-Bset1*1e3)
% plot(-Bset1*1e3)
% % xlim(tlims)
% xlim(xlims)
% xlabel('t[\mus]')
% ylabel('B_z[mT]')
% 
% fontsize(SingleSig,scale=1.5)
% % FFT of dBz
% range = xlims(1):xlims(2);
% SamRate = 10e6;
% dBsig = dBSetZ(:,ProbeNum,StickNum);
% % dBsig = dBset1(:,ProbeNum);
% dBsig = dBsig(range);
% 
% L = length(dBsig); %number of sample points in window
% % fftdBsig = abs(fftshift(fft(dBsig))/L);
% fftdBsig = fft(dBsig);
% Hz = SamRate/L*(-L/2:L/2);
% 
% P1 = fftdBsig(1:L/2+1);
% P1(2:end-1) = 2*P1(2:end-1);
% 
% f = SamRate/L*(0:(L/2));
% 
% 
% P2 = abs(fftdBsig/L);
% P1 = P2(1:L/2+1);
% P1(2:end-1) = 2*P1(2:end-1);
% 
% f = SamRate*(0:(L/2))/L;
% 
% subplot(3,1,3)
% % plot(Hz,log10(fftdBsig))
% semilogx(f,P1)
% xlabel('Hz')
% ylabel('fft(dBz)')
% title('fft of dB_z')

