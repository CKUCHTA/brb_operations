% Creates a plot in the XZ plane based on multiple shots scanning in the Z direction.
% The data for these plots is collected using GetZscanData_Shotlist.m where the B data
% is saved in the file ZScanBdata_AllShots.mat.
% edited 16June2024

function [YZPlanFig, BSet, dBSet, ProbePos, FarrayZvec, StickPos] = PlotYZPlan(ShotList, FolderName, SampleEvalActual, ProbeNum, SaveData, PlotPlane)
% clear
% ShotList = [62604 62617 62626 62634 62645 62657 62674 62688 62699 62712 62721 62733 62749 62757 62768 62779 62790 62802]; %plasma shots
% % ShotList = [62610 62612 62631 62654 62830 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% ShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots Dipole Vertical
% ShotList = [64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan
% FolderName = 'DipoleCuspVertical_FullScan';
% SampleEvalActual = 80;
% ProbeNum = 4; % plot the plan created at the probe number scanned in the Z direction.  Probe numbers start from the manifold and increse
% PlotPlane = 1;  % 1 = plot the plane, 0 = do not plot the plane

DataFolderStruct = what(FolderName);
DataFolder = DataFolderStruct.path;
FarrayFolder = fullfile(DataFolder, '3DFarray');
% Save figure files
PlotFolder = fullfile(FarrayFolder, 'Plots','YZPlan');
if ~exist(PlotFolder, 'dir')
    mkdir(PlotFolder)
end

% load data from GetZscandData_Shotlist.m
ZscanFolder = fullfile(FarrayFolder,'Matlab_Data','ZScanData');
disp('Loading data')
load(fullfile(ZscanFolder, 'ZscanBdata_AllShots.mat'), 'BSetZdata', 'dBSetZdata', ...
'ZposArray', 'SampleEval', 'ProbePos', 'tpoints', 'FarrayZPos', 'shotData')

BSetXall = BSetZdata{1}; %  (probe, stick, z indx, sample))
BSetYall = BSetZdata{2};
BSetZall = BSetZdata{3};

dBSetXall = dBSetZdata{1};
dBSetYall = dBSetZdata{2};
dBSetZall = dBSetZdata{3};

BSetX = zeros(size(BSetXall, 1), size(BSetXall, 2), length(ShotList));
BSetY = zeros(size(BSetYall, 1), size(BSetYall, 2), length(ShotList));
BSetZ = BSetX;

dBSetX = BSetX; 
dBSetY = BSetY;
dBSetZ = BSetX;

% extract the data for each shot from ShotList
ZposVec = zeros(length(ShotList), 1); % vector to store the Z position of each shot
FarrayZvec = zeros(length(ShotList), 1); % flux array Z position for a given shot number
Shot = ShotList(1);
ShotIndex = shotData(:,1) == Shot;
DipoleTypeCompare = shotData(ShotIndex, 7);
DipoleTypeString = sprintf('Dipole Voltage = %dkV', DipoleTypeCompare);

for ii = 1:length(ShotList)
    Shot = ShotList(ii);
    ShotIndex = find(ZposArray(:,1) == Shot);
    if isempty(ShotIndex)
        error('Shot %d not found in ZposArray', Shot)
    end
    if length(ShotIndex) > 1
        ShotIndex = ShotIndex(1); % take the first shot if there are multiple entries
    end
    ShotDataIndex = shotData(:,1) == Shot; 
    DipoleType = shotData(ShotDataIndex, 7);
    if DipoleType ~= DipoleTypeCompare
        error('Dipole Cap Voltage does not match for Shot %d', Shot)
    end
    ZposVec(ii) = ZposArray(ShotIndex, 2)*1e-2;
    FarrayZvec(ii) = FarrayZPos(ShotIndex, 2);
    
    BSetX(:,:,ii) = BSetXall(:,:, ShotIndex, SampleEvalActual);
    BSetY(:,:,ii) = BSetYall(:,:, ShotIndex, SampleEvalActual);
    BSetZ(:,:,ii) = BSetZall(:,:, ShotIndex, SampleEvalActual);

    dBSetX(:,:,ii) = dBSetXall(:,:, ShotIndex, SampleEvalActual);
    dBSetY(:,:,ii) = dBSetYall(:,:, ShotIndex, SampleEvalActual);
    dBSetZ(:,:,ii) = dBSetZall(:,:, ShotIndex, SampleEvalActual);
end

Bx = squeeze(BSetX);
By = squeeze(BSetY);
Bz = squeeze(BSetZ);
BSet = {Bx, By, Bz};

dBx = squeeze(dBSetX);
dBy = squeeze(dBSetY);
dBz = squeeze(dBSetZ);
dBSet = {dBx, dBy, dBz};

Stick1Ypos = 0.03; % stick 1 y position (meters)
StickPos = Stick1Ypos + (0:(-12):-84)*1e-2; % stick positions

%%
% Plot Parameters
label = {'x' 'y' 'z'};

% Pcolor bar limits for probes 1-10
climBx = {[-1.5 1.5] [-1.5 1.5] [-1.5 1.5] [-3 3] [-1.5 1.5] [-1.5 1.5] [-1.5 1.5] [-1.5 1.5] [-1.5 1.5] [-1.5 1.5]};
climDBx = {[-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-100 100]};

climBy = {[-4.5 1.5] [-4.5 1.5] [-5.5 1.5] [-3 3] [-4.5 1.5] [-2.5 1] [-4.5 1.5] [-4.5 1.5] [-4.5 1.5] [-4.5 1.5] [-5.5 1.5] [-3 3] };
climDBy = {[-100 100] [-100 100] [-200 200] [-100 100] [-100 100] [-250 50] [-100 100] [-100 100] [-100 100] [-100 100] [-200 200] [-100 100] [-100 100]};

climBz = {[-2 5] [-2 2] [-4 4] [-3 5] [-2 2] [-2 0.5] [-2 2] [-2 2] [-2 2] [-2 2]};
climDBz = {[-200 200] [-200 200] [-200 200] [-200 200] [-200 200] [-200 200] [-200 200] [-200 200] [-200 200] [-200 200]};
% climBx = [-1.5 1.5];
% climDBx = [-1 1]*1e2;
% 
% climBy = [-1.5 0];%[-4.5 1.5];
% climDBy = [-1 1]*1e2;
% 
% climBz = [-2 2];
% % climDBz = [-2 2]*1e2;
% 
Bclims = {climBx climBy climBz}; % pcolor limits for B
dBclims = {climDBx climDBy climDBz};

Te_Xpos = 0.05; % X position of Te probe in meters

YZPlanFig = figure(2);
clf
if PlotPlane == 1
    
    time = sprintf(', \\Deltat = %0.1f\\mus', SampleEvalActual*1e-1);
    TePos = sprintf(', Te Probe X = %0.2fm', Te_Xpos);
    sgtitle({strjoin({DipoleTypeString TePos time}), strjoin(string(ShotList))})
    ind = 0;
    for k = 1:6
        if k == 1||k ==2||k ==3
            B = squeeze(BSet{k}(ProbeNum,:,:));
            ProbePos_k = ProbePos{k};
            Bclim = Bclims{k};
            subplot(2,3,k)
            pcolor(FarrayZvec, StickPos,B*1e3), shading interp
            % pcolor(B*1e3), shading interp
            colorbar
            xlabel('Z[m]')
            ylabel('Y[m]')
            title(sprintf('B%s[mT], X = %0.2fm', label{k}, ProbePos_k(ProbeNum)))
            clim(Bclim{ProbeNum})
        else
            ind = ind + 1;
            % dB = squeeze(dBSet{k}(SamplePoint,:,:));
            dBSetk = dBSet{ind};
            dB = squeeze(dBSetk(ProbeNum,:,:));
            dBclim = dBclims{ind};
            ProbePos_ind = ProbePos{ind};
            if ind == 3
                % dB(3,5) = -dB(3,5);
            end
            subplot(2,3,k)
            pcolor(FarrayZvec, StickPos, dB), shading interp
            % pcolor(dB), shading interp
            colorbar
            xlabel('Z[m]')
            ylabel('Y[m]')
            title(sprintf('dB%s , X = %0.2fm', label{ind}, ProbePos_ind(ProbeNum)))
            clim(dBclim{ProbeNum})
        end
    end
    set(YZPlanFig, 'Position',  [200, 100, 1000, 550])
end
close(YZPlanFig)