% Interpolate 3D B data from Stream3DPlot

function [Interp3Dplot] = InterpStream3D(ParentFolder, PlasmaShotList, DriveShotList, DipoleDriveVacShotList, SampleEvalActual, SaveData, WhichPlot)
% clear
% % ShotList = [62610 62612 62631 62654 62830 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% VacShotList = [63354 63365 63376 63387 63398 63409 63420 63431 63442 63454 63465 63476]; % vacuum shots dipole vertical
% PlasmaShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots dipole vertical
% DriveShotList = [63350 63361 63372 63383 63394 63405 63416 63428 63438 63450 63461 63472]; % Drive Coil Only dipole vertical
% DipoleDriveVacShotList = [63352 63363 63374 63385 63396 63407 63418 63429 63441 63452 63463 63474]; % Drive Coil and Dipole vacuum shot dipole vertical
% ParentFolder = 'DipoleCuspVertical2024';
% % ParentFolder = 'DipoleCusp2024';
% SampleEvalActual = 1900;
% SaveData = 1;
% WhichPlot = 1; % 1 = DipoleDrive3D, 2 = DipoleDriveVac3D, 3 = DipoleRemoved, 0 = No plots


[~, XX, YY, ZZ, startX, startY,startZ, ~, ~, BSet_DipoleRemoved, PositionVectors] = ...
    Stream3DPlot(ParentFolder, PlasmaShotList, DriveShotList, DipoleDriveVacShotList, SampleEvalActual, ...
    SaveData, 0);

BSetX_DipoleRemoved = BSet_DipoleRemoved{1};
BSetY_DipoleRemoved = BSet_DipoleRemoved{2};
BSetZ_DipoleRemoved = BSet_DipoleRemoved{3};

XposVec = PositionVectors{1};
YposVec = PositionVectors{2};
ZposVec = PositionVectors{3};

% interpolation points
Xinterp = linspace(XposVec(1),XposVec(end), 41);
Yinterp = linspace(YposVec(1),YposVec(end), 39);
Zinterp = linspace(ZposVec(1),ZposVec(end), 40);

[YYinterp, ZZinterp, XXinterp] = meshgrid(Yinterp, Zinterp, Xinterp);

% interplat B data
BSetXinterp_DipoleRemoved = interp3(YY, ZZ, XX, BSetX_DipoleRemoved, YYinterp, ZZinterp, XXinterp);
BSetYinterp_DipoleRemoved = interp3(YY, ZZ, XX, BSetY_DipoleRemoved(:,:,1:10), YYinterp, ZZinterp, XXinterp);
BSetZinterp_DipoleRemoved = interp3(YY, ZZ, XX, BSetZ_DipoleRemoved, YYinterp, ZZinterp, XXinterp);

% plot dipole
yCoordinate = -0.4;
zCoordinate = -0.39;
xCoordinate = -0.05;
R = 0.257/2;
theta = 0:0.01:2*pi;
yDipole = ones(length(theta))*yCoordinate;
xDipole = R*sin(theta) + xCoordinate;
zDipole = R*cos(theta) + zCoordinate;

if WhichPlot == 1
    Interp3Dplot = figure(3);
    clf
    title(sprintf('Interpolated Dipole Removed, time = %0.1f', SampleEvalActual*1e-1))
    streamslice(YYinterp, ZZinterp, XXinterp, BSetYinterp_DipoleRemoved, BSetZinterp_DipoleRemoved, BSetXinterp_DipoleRemoved, YposVec(1), ZposVec(12),XposVec(1));
    
    verts = stream3(YYinterp, ZZinterp, XXinterp, BSetYinterp_DipoleRemoved, BSetZinterp_DipoleRemoved, BSetXinterp_DipoleRemoved,startY, startZ, startX);
    lineobj = streamline(verts);
    view(220, 35)
    % view(-45, 15)
    hold on
    % quiver3(XX, YY, ZZ, BSetXnew, BSetYnew(:,1:10,:), BSetZnew);
    plot3(yDipole, zDipole, xDipole, 'k')
    xlabel('Y (sticks)')
    ylabel('Z (shots)')
    zlabel('X (probes)')
    axis equal

elseif WhichPlot == 0
    Interp3Dplot = 0;
end
