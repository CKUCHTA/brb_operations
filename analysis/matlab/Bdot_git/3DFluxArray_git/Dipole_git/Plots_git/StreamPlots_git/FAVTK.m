

clear
% ShotList = [62610 62612 62631 62654 62830 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% VacShotList = [63354 63365 63376 63387 63398 63409 63420 63431 63442 63454 63465 63476]; % vacuum shots dipole vertical
% PlasmaShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots dipole vertical
% DriveShotList = [63350 63361 63372 63383 63394 63405 63416 63428 63438 63450 63461 63472]; % Drive Coil Only dipole vertical
% DipoleDriveVacShotList = [63352 63363 63374 63385 63396 63407 63418 63429 63441 63452 63463 63474]; % Drive Coil and Dipole vacuum shot dipole vertical

DipoleDriveVacShotList = [64091 64110 64133 64146 64147 64173 64186 64187 64230 64247 64263 64264 ...
    64295 64309 64325 64341 64353 64396 64388 64417 64446 64473 64491 64503]; % Vacuum 1kV Dipole Cap full scan
PlasmaShotList = [64081 64093 64112 64134 64148 64160 64174 64190 64209 64231 64248 64265 ...
    64281 64298 64311 64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan
DriveShotList = [64086 64097 64118 64140 64154 64167 64180 64196 64204 64238 64257 64272 ...
    64288 64303 64317 64332 64348 64361 64376 64406 64434 64464 64483 64498]; % Drive Only full scan


% ParentFolder = 'DipoleCuspVertical2024';
ParentFolder = 'DipoleCuspVertical_FullScan';
% ParentFolder = 'DipoleCusp2024';
% SampleEvalActual = 1912;
SaveData = 0;
WhichPlot = 0; % 1 = DipoleDrive3D, 2 = DipoleDriveVac3D, 0 = no plot


DataPath = what(ParentFolder);
ParentFolderPath = DataPath.path;
MatlabFolder = fullfile(ParentFolderPath,'3DFarray', 'Matlab_Data');
ParaFolder = fullfile(ParentFolderPath,'3DFarray','ParaViewData');
if ~exist(ParaFolder, 'dir')
    mkdir(ParaFolder)
end

SampleRange = 1850:2100;
BSetX = zeros(24,8,10,length(SampleRange));
BSetY = BSetX;
BSetZ = BSetX;
BMag = BSetZ;

indx = 0;
for Sample = SampleRange
    disp(Sample)
    indx = 1 + indx;
    [~, XX, YY, ZZ,startX, startY,startZ, BSetnew, BSetnew_DriveDipoleVac, BSetnew_DipoleRemoved] = ...
        Stream3DPlot(ParentFolder, PlasmaShotList, DriveShotList, DipoleDriveVacShotList, Sample, SaveData, WhichPlot);
    
    BSetYnew = BSetnew{2}(:,:,1:10);

    BSetXNew = BSetnew{1}*1e4;
    BSetYNew = BSetYnew*1e4;
    BSetZNew = BSetnew{3}*1e4;

    BMag(:,:,:,indx) = sqrt(BSetXNew.^2 + BSetYNew.^2 + BSetZNew.^2);
    % BSetX(:,:,:,indx) = BSetXNew./BMag(:,:,:,indx);
    % BSetY(:,:,:,indx) = BSetYNew./BMag(:,:,:,indx);
    % BSetZ(:,:,:,indx) = BSetZNew./BMag(:,:,:,indx);

    ParaFile = fullfile(ParaFolder, sprintf('ParaViewData_%d.vtk', indx));
    vtkwrite(ParaFile, 'structured_grid', XX,YY,ZZ,'vectors', 'BFieldVec',...
    BSetXNew,BSetYNew,BSetZNew,'scalars', 'BFieldMag', squeeze(BMag(:,:,:,indx)));


end




% ParaFile = fullfile(ParaFolder,'ParaViewData4D.vtk');
MatDataFile = fullfile(MatlabFolder, '4D_Bdata.mat');

save(MatDataFile, 'BSetX', 'BSetY', 'BSetZ', 'XX', 'YY', 'ZZ', 'startX', 'startY', 'startZ')

% vtkwrite(ParaFile, 'structured_grid', XX,YY,ZZ,'vectors', 'BFieldVec',...
%     BSetX,BSetY,BSetZ,'scalars', 'BFieldMag', BMag);

% figure(111)
% quiver3(XX,YY,ZZ,BSetX,BSetY,BSetZ)
% 
% figure(112)
% streamline(XX,YY,ZZ,BSetX,BSetY,BSetZ,startX,startY,startZ);
% view(3)
% xlabel('X')
% ylabel('Y')
% zlabel('Z')
% grid on