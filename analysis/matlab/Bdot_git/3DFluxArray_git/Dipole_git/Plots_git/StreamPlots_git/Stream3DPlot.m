% Creates steamline plot of the 3D magnetic field field

function [DipoleDrive3D, XX, YY, ZZ,startX, startY,startZ, BSetnew_DriveDipole, BSetnew_DriveDipoleVac, BSetnew_DipoleRemoved, PositionVectors]...
    = Stream3DPlot(ParentFolder, PlasmaShotList, DriveShotList, DipoleDriveVacShotList, SampleEvalActual, SaveData, WhichPlot, dipoleType)
% clear
% % % ShotList = [62610 62612 62631 62654 62830 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% % VacShotList = [63354 63365 63376 63387 63398 63409 63420 63431 63442 63454 63465 63476]; % vacuum shots dipole vertical
% % PlasmaShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots dipole vertical
% % DriveShotList = [63350 63361 63372 63383 63394 63405 63416 63428 63438 63450 63461 63472]; % Drive Coil Only dipole vertical
% % DipoleDriveVacShotList = [63352 63363 63374 63385 63396 63407 63418 63429 63441 63452 63463 63474]; % Drive Coil and Dipole vacuum shot dipole vertical
% % PlasmaShotList = [64081 64093 64112 64134 64148 64160 64174 64190 64209 64231 64248 64265 ...
%     % 64281 64298 64311 64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan
% PlasmaShotList = [64082 64095 64115 64137 64151 64163 64177 64192 64220 64234 64254 64269 ...
%     64283 64300 64314 64329 64345 64358 64370 64400 64428 64456 64479 64495]; % 2kV Dipole Cap full scan
% DipoleDriveVacShotList = [64089 64108 64131 64144 64158 64171 64184 64205 64227 64244 64261 64276 ...
%     64293 64307 64323 64339 64352 64385 64395 64412 64443 64469 64488 64505]; % Vacuum 2kV Dipole Cap full scan
% DriveShotList = [64086 64097 64118 64140 64154 64167 64180 64196 64204 64238 64257 64272 ...
%     64288 64303 64317 64332 64348 64361 64376 64406 64434 64464 64483 64498]; % Drive Only full scan
% ParentFolder = 'DipoleCuspVertical2024';
% % ParentFolder = 'DipoleCusp2024'
% ParentFolder = 'DipoleCuspVertical_FullScan';
% SampleEvalActual = 1900;
% SaveData = 0;
% WhichPlot = 2; % 1 = DipoleDrive3D, 2 = DipoleDriveVac3D, 3 = DipoleRemoved, 0 = No plots
% dipoleType = 'dipole2kV'; % 'dipole1kV', 'dipole2kV'

DataFolderStruct = what(ParentFolder);
DataFolder = DataFolderStruct.path;
FarrayFolder = fullfile(DataFolder, '3DFarray');
MatlabDataFolder = fullfile(FarrayFolder,'Matlab_Data');
% Save figure files
FileName = sprintf('ZScanData_Sample%d.mat', SampleEvalActual);
DataFile = fullfile(FarrayFolder, 'Matlab_Data', 'ZScanData', dipoleType, FileName); % file matlab data is saved

% AllShots = [PlasmaShotList DriveShotList, DipoleDriveVacShotList];
AllShots = [PlasmaShotList];
try
    disp('Loading data')
    load(DataFile, 'BSetZdata', 'dBSetZdata', 'ZposArray', 'SampleEval', 'ProbePos')
    if SampleEvalActual ~= SampleEval
        disp('Data for sample point not created yet')
        % [BSetZdata, dBSetZdata, ZposArray, ProbePos, ~, ~] = GetZscanData(ParentFolder, SampleEvalActual, SaveData);
        [BSetZdata, dBSetZdata, ZposArray, ProbePos, ~, ~] = GetZscanData_Shotlist(ParentFolder, SampleEvalActual, SaveData, AllShots);
    end
catch
    disp('running GetZscanData')
    % [BSetZdata, dBSetZdata, ZposArray, ProbePos, ~, ~] = GetZscanData(ParentFolder, SampleEvalActual, SaveData);
    [BSetZdata, dBSetZdata, ZposArray, ProbePos, ~, ~] = GetZscanData_Shotlist(ParentFolder, SampleEvalActual, SaveData, AllShots);
end

BSetXall = BSetZdata{1}; %  (probe(xdir), stick(ydir), z indx))
BSetYall = BSetZdata{2};
BSetZall = BSetZdata{3};

dBSetXall = dBSetZdata{1};
dBSetYall = dBSetZdata{2};
dBSetZall = dBSetZdata{3};

BSetX_DriveDipole = zeros(size(BSetXall, 1), size(BSetXall, 2), length(PlasmaShotList));
BSetY_DriveDipole = zeros(size(BSetYall, 1), size(BSetYall, 2), length(PlasmaShotList));
BSetZ_DriveDipole = zeros(size(BSetZall, 1), size(BSetZall, 2), length(PlasmaShotList));

dBSetX_DriveDipole = zeros(size(dBSetXall, 1), size(dBSetXall, 2), length(PlasmaShotList));
dBSetY_DriveDipole = zeros(size(dBSetYall, 1), size(dBSetYall, 2), length(PlasmaShotList));
dBSetZ_DriveDipole = zeros(size(dBSetZall, 1), size(dBSetZall, 2), length(PlasmaShotList));

BSetX_DriveOnly = BSetX_DriveDipole;
BSetY_DriveOnly = BSetY_DriveDipole;
BSetZ_DriveOnly = BSetZ_DriveDipole;

dBSetX_DriveOnly = dBSetX_DriveDipole;
dBSetY_DriveOnly = dBSetY_DriveDipole;
dBSetZ_DriveOnly = dBSetZ_DriveDipole;

BSetX_DriveDipoleVac = BSetX_DriveDipole;
BSetY_DriveDipoleVac = BSetY_DriveDipole;
BSetZ_DriveDipoleVac = BSetZ_DriveDipole;

dBSetX_DriveDipoleVac = dBSetX_DriveDipole;
dBSetY_DriveDipoleVac = dBSetY_DriveDipole;
dBSetZ_DriveDipoleVac = dBSetZ_DriveDipole;

BSetX_DipoleRemoved = BSetX_DriveDipole;
BSetY_DipoleRemoved = BSetY_DriveDipole;
BSetZ_DipoleRemoved = BSetZ_DriveDipole;

dBSetX_DipoleRemoved = dBSetX_DriveDipole;
dBSetY_DipoleRemoved = dBSetY_DriveDipole;
dBSetZ_DipoleRemoved = dBSetZ_DriveDipole;

% extract the data for each shot from ShotList
ZposVec = zeros(length(PlasmaShotList), 1); % vector to store the Z position of each shot
for ii = 1:length(PlasmaShotList)
    Shot = PlasmaShotList(ii);
    ShotIndex = find(ZposArray(:,1) == Shot);
    VacShot = DipoleDriveVacShotList(ii);
    DriveShotIndex = find(ZposArray(:,1) == Shot);
    VacShotIndex = find(ZposArray(:,1) == VacShot);
    ZposVec(ii) = ZposArray(ShotIndex, 2)*1e-2;
    
    BSetX_DriveDipole(:,:,ii) = BSetXall(:,:,ShotIndex);
    BSetY_DriveDipole(:,:,ii) = BSetYall(:,:,ShotIndex);
    BSetZ_DriveDipole(:,:,ii) = BSetZall(:,:,ShotIndex);

    dBSetX_DriveDipole(:,:,ii) = dBSetXall(:,:,ShotIndex);
    dBSetY_DriveDipole(:,:,ii) = dBSetYall(:,:,ShotIndex);
    dBSetZ_DriveDipole(:,:,ii) = dBSetZall(:,:,ShotIndex);

    BSetX_DriveOnly(:,:,ii) = BSetXall(:,:,DriveShotIndex);
    BSetY_DriveOnly(:,:,ii) = BSetYall(:,:,DriveShotIndex);
    BSetZ_DriveOnly(:,:,ii) = BSetZall(:,:,DriveShotIndex);

    dBSetX_DriveOnly(:,:,ii) = dBSetXall(:,:,DriveShotIndex);
    dBSetY_DriveOnly(:,:,ii) = dBSetYall(:,:,DriveShotIndex);
    dBSetZ_DriveOnly(:,:,ii) = dBSetZall(:,:,DriveShotIndex);

    BSetX_DriveDipoleVac(:,:,ii) = BSetXall(:,:,VacShotIndex);
    BSetY_DriveDipoleVac(:,:,ii) = BSetYall(:,:,VacShotIndex);
    BSetZ_DriveDipoleVac(:,:,ii) = BSetZall(:,:,VacShotIndex);

    dBSetX_DriveDipoleVac(:,:,ii) = dBSetXall(:,:,VacShotIndex);
    dBSetY_DriveDipoleVac(:,:,ii) = dBSetYall(:,:,VacShotIndex);
    dBSetZ_DriveDipoleVac(:,:,ii) = dBSetZall(:,:,VacShotIndex);

    BSetX_DipoleRemoved(:,:,ii) = BSetXall(:,:,ShotIndex) - BSetXall(:,:,VacShotIndex);
    BSetY_DipoleRemoved(:,:,ii) = BSetYall(:,:,ShotIndex) - BSetYall(:,:,VacShotIndex);
    BSetZ_DipoleRemoved(:,:,ii) = BSetZall(:,:,ShotIndex) - BSetZall(:,:,VacShotIndex);

    dBSetX_DipoleRemoved(:,:,ii) = dBSetXall(:,:,ShotIndex) - dBSetXall(:,:,VacShotIndex);
    dBSetY_DipoleRemoved(:,:,ii) = dBSetYall(:,:,ShotIndex) - dBSetYall(:,:,VacShotIndex);
    dBSetZ_DipoleRemoved(:,:,ii) = dBSetZall(:,:,ShotIndex) - dBSetZall(:,:,VacShotIndex);
end


Stick1Ypos = 0.03; % stick 1 y position (meters)
YposVec = Stick1Ypos + (0:(-12):-84)*1e-2; % stick positions
ZposVec = ZposVec';

dBxXposVec = ProbePos{1};
XposVec = ProbePos{2};
dBzXposVec = ProbePos{3};

XposVec = XposVec(1:10);

PositionVectors = {XposVec, YposVec, ZposVec};

[YY, ZZ, XX] = meshgrid(YposVec, ZposVec, XposVec);
% [XX, YY, ZZ] = meshgrid(dByXposVec(1:10), YPosVec, ZposVec);
% [XX, YY, ZZ] = meshgrid(dBzXposVec, YPosVec, ZposVec);

% [startX,startY,startZ] = meshgrid(-0.45, 0.011, ZposVec);
% [startX,startY,startZ] = meshgrid(dBxXposVec(3:5), YPosVec(3:7), ZposVec(1:10));
[startY,startZ, startX] = meshgrid(YposVec(), ZposVec(:), XposVec(:));

% swap X and Y dimension (stream3 didn't like the other orintation)
BSetXnew_DriveDipole = permute(BSetX_DriveDipole,[3,2,1]);
BSetYnew_DriveDipole = permute(BSetY_DriveDipole,[3,2,1]);
BSetZnew_DriveDipole = permute(BSetZ_DriveDipole,[3,2,1]);

BSetXnew_DriveOnly = permute(BSetX_DriveOnly,[3,2,1]);
BSetYnew_DriveOnly = permute(BSetY_DriveOnly,[3,2,1]);
BSetZnew_DriveOnly = permute(BSetZ_DriveOnly,[3,2,1]);

BSetXnew_DriveDipoleVac = permute(BSetX_DriveDipoleVac,[3,2,1]);
BSetYnew_DriveDipoleVac = permute(BSetY_DriveDipoleVac,[3,2,1]);
BSetZnew_DriveDipoleVac = permute(BSetZ_DriveDipoleVac,[3,2,1]);

BSetXnew_DipoleRemoved = permute(BSetX_DipoleRemoved,[3,2,1]);
BSetYnew_DipoleRemoved = permute(BSetY_DipoleRemoved,[3,2,1]);
BSetZnew_DipoleRemoved = permute(BSetZ_DipoleRemoved,[3,2,1]);

% collect B data
BSetnew_DriveDipole = {BSetXnew_DriveDipole, BSetYnew_DriveDipole, BSetZnew_DriveDipole};
BSetnew_DriveDipoleVac = {BSetXnew_DriveDipoleVac, BSetYnew_DriveDipoleVac, BSetZnew_DriveDipoleVac};
BSetnew_DipoleRemoved = {BSetXnew_DipoleRemoved, BSetYnew_DipoleRemoved, BSetZnew_DipoleRemoved};


% plot dipole
yCoordinate = -0.4;
zCoordinate = -0.39;
xCoordinate = 0;
R = 0.257/2;
theta = 0:0.01:2*pi;
yDipole = ones(length(theta))*yCoordinate;
xDipole = R*sin(theta) + xCoordinate;
zDipole = R*cos(theta) + zCoordinate;

if WhichPlot == 1
    DipoleDrive3D = figure(1);
    clf
    title(sprintf('Dipole and Drive Plasma, time = %0.1f', SampleEvalActual*1e-1))
    % streamslice(YY, ZZ, XX, BSetYnew_DriveDipole(:,:,1:10), BSetZnew_DriveDipole, BSetXnew_DriveDipole, YposVec(1), ZposVec(1),XposVec(1));
    
    verts = stream3(YY, ZZ, XX, BSetYnew_DriveDipole(:,:,1:10), BSetZnew_DriveDipole, BSetXnew_DriveDipole,startY, startZ, startX);
    lineobj = streamline(verts);
    view(220, 35)
    % view(-45, 15)
    hold on
    % quiver3(XX, YY, ZZ, BSetXnew, BSetYnew(:,1:10,:), BSetZnew);
    plot3(yDipole, zDipole, xDipole, 'k', 'linewidth', 2)
    xlabel('Y (sticks)')
    ylabel('Z (shots)')
    zlabel('X (probes)')
    axis equal

elseif WhichPlot == 2
    DipoleDrive3D = figure(2);
    clf
    title(sprintf('Dipole and Drive Vacuum, time = %0.1f', SampleEvalActual*1e-1))
    % streamslice(YY, ZZ, XX, BSetYnew_DriveDipoleVac(:,:,1:10), BSetZnew_DriveDipoleVac, BSetXnew_DriveDipoleVac, YposVec(1), ZposVec(12),XposVec(1));
    
    verts = stream3(YY, ZZ, XX, BSetYnew_DriveDipoleVac(:,:,1:10), BSetZnew_DriveDipoleVac, BSetXnew_DriveDipoleVac,startY, startZ, startX);
    lineobj = streamline(verts);
    view(220, 35)
    % view(-45, 15)
    hold on
    % quiver3(XX, YY, ZZ, BSetXnew, BSetYnew(:,1:10,:), BSetZnew);
    plot3(yDipole, zDipole, xDipole, 'k', 'linewidth', 2)
    xlabel('Y (sticks)')
    ylabel('Z (shots)')
    zlabel('X (probes)')
    axis equal

elseif WhichPlot == 3
    DipoleDrive3D = figure(3);
    clf
    title(sprintf('Drive Coils Only, time = %0.1f', SampleEvalActual*1e-1))
    % streamslice(YY, ZZ, XX, BSetYnew_DipoleRemoved(:,:,1:10), BSetZnew_DipoleRemoved, BSetXnew_DipoleRemoved, YposVec(1), ZposVec(12),XposVec(1));
    
    verts = stream3(YY, ZZ, XX, BSetYnew_DipoleRemoved(:,:,1:10), BSetZnew_DipoleRemoved, BSetXnew_DipoleRemoved,startY, startZ, startX);
    lineobj = streamline(verts);
    view(220, 35)
    % view(-45, 15)
    hold on
    % quiver3(XX, YY, ZZ, BSetXnew, BSetYnew(:,1:10,:), BSetZnew);
    plot3(yDipole, zDipole, xDipole, 'k', 'linewidth', 2)
    xlabel('Y (sticks)')
    ylabel('Z (shots)')
    zlabel('X (probes)')
    axis equal

elseif WhichPlot == 0
    DipoleDrive3D = 0;
end

% BSetYnew = BSetYnew(:,1:10,:);
% save(fullfile(MatlabDataFolder,'3DStreamData.mat'), 'XX', 'YY', 'ZZ', 'BSetXnew', 'BSetYnew', 'BSetZnew', 'startX', 'startY', 'startZ')

% DriveOnly3D = figure(5);
% clf
% title(sprintf('Drive Coils Only, time = %0.1f', SampleEvalActual*1e-1))
% streamslice(XX, YY, ZZ, -BSetXnew_DriveOnly, BSetYnew_DriveOnly(:,1:10,:), BSetZnew_DriveOnly, dByXposVec(1), YPosVec(1), ZposVec(12));
% % streamslice(XX, YY, ZZ, BSetXnew, BSetYnew(:,1:10,:), BSetZnew, -0.1585, 0.03, -0.413);
% 
% verts = stream3(XX, YY, ZZ, -BSetXnew_DriveOnly, BSetYnew_DriveOnly(:,1:10,:), BSetZnew_DriveOnly,startX, startY,startZ);
% lineobj = streamline(verts);
% view(77.5, 20)
% hold on
% % quiver3(XX, YY, ZZ, BSetXnew, BSetYnew(:,1:10,:), BSetZnew);
% plot3(xDipole, yDipole, zDipole, 'k')
% xlabel('X (probes)')
% ylabel('Y (sticks)')
% zlabel('Z')
% axis equal
