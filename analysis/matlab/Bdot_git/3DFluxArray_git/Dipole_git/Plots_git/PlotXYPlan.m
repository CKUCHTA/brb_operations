% Generate a plot in the XY plan for a single point in time.
% created 23Feb24

function XYPlanFig = PlotXYPlan(shot, SamplePoint, SampleRange, xlims, saveData)
% clear
% shot = 64188;
% SamplePoint = 1960; % sample point plots are created from
% SampleRange=69000:75000;
% xlims = [1000 2000];
% saveData = 0;

[daq, Bh] = loadDtaqData3DFarray(shot, saveData, SampleRange); % load dtac from harddrive or from mdsplus
PlotTimeTrace = 0;

% SamRange=69000:72000;

dt=1e-7; % time between samples
[boards, sticks, tpoints, dBSetRaw] = load3DFarrayDipole(daq, SampleRange, dt, shot, PlotTimeTrace, xlims);

IIm=901:980; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIoff=1000; %Makes Bz=Bh at this index
[BSet, dBSet, ProbePos] = Calibrate_3DFarryDipole(IIoff, IIm, dt, dBSetRaw);


% dBSet = dBSetRaw;

% Probe X Positions
dBxXpos = ProbePos{1};
dByXpos = ProbePos{2};
dBzXpos = ProbePos{3};

Stick1Ypos = 0.03; % stick 1 y position (meters)
Yvec = Stick1Ypos + (0:(-12):-84)*1e-2; % stick positions

label = {'x' 'y' 'z'};
climBx = [-3 3];
climDBx = [-0.25 0.25]*1e3;

climBy = [-9 3];
climDBy = [-0.4 0.4]*1e3;

climBz = [-5 3];
climDBz = [-0.25 0.25]*1e3;

% Bclims = {[-8 8] [-8 8] [-8 8]}; % pcolor limits for B
% dBclims = {[-90 90] [-250 250] [-300 300]}; % pcolor limits for dB
Bclims = {climBx climBy climBz}; % pcolor limits for B
dBclims = {climDBx climDBy climDBz}; % pcolor l


% Use Excel Spreadsheet data to get distance between dipole and flux array
excelFileName = 'DipoleCusp2024.xlsx';
DataFolderName = 'DipoleCusp2024'; % folder name excel file will be saved to
docid = '1YbMnfhPIyXe3SAJL9hR5xkr6GhluWjYWQPfE1WV9LpA'; % from google sheets url
webSave = 1; 
[shotData, ~] = DipoleShotData(excelFileName, DataFolderName, docid, webSave);
shotIndex = find(shotData(:,1) == shot);
DipolePos = shotData(shotIndex,4);
FluxArrayPos = shotData(shotIndex,3);

DipoleFlux_Distance = (FluxArrayPos - DipolePos)*1e-2; % distance between flux array and dipole
FluxToCoil_Distance = (15 - FluxArrayPos)*1e-2;

XYPlanFig = figure(1);
clf
sgtitle(sprintf('shot %d, t = %0.2f\\mus, \\DeltaZ_{Flux to Dip} = %0.2fm, \\DeltaZ_{Flux to Coil} = %0.2fm',...
    shot, tpoints(SamplePoint),DipoleFlux_Distance, FluxToCoil_Distance))
ind = 0;
for k = 1:6
    if k == 1||k ==2||k ==3
        B = squeeze(BSet{k}(SamplePoint,:,:));
        subplot(2,3,k)
        pcolor(Yvec,ProbePos{k},B*1e3), shading interp
        % pcolor(B*1e3), shading interp
        colorbar
        xlabel('Y[m]')
        ylabel('X[m]')
        title(sprintf('B%s[mT]', label{k}))
        clim(Bclims{k})
    else
        ind = ind + 1;
        % dB = squeeze(dBSet{k}(SamplePoint,:,:));
        dBSetk = dBSet{ind};
        dB = squeeze(dBSetk(SamplePoint,:,:));
        if ind == 3
            % dB(3,5) = -dB(3,5);
        end
        subplot(2,3,k)
        pcolor(Yvec,ProbePos{ind},dB), shading interp
        % pcolor(dB), shading interp
        colorbar
        xlabel('Y[m]')
        ylabel('X[m]')
        title(sprintf('dB%s', label{ind}))
        clim(dBclims{ind})
    end
end
% set(gcf, 'Position',  [200, 100, 1000, 550])
%%
return
figure(3), clf
subplot(2,1,1)
plot(BSet{3}(:,10,7))
hold on
plot(BSet{3}(:,9,7))
plot(BSet{3}(:,8,7))
plot(BSet{3}(:,7,7))
% plot(BSet{3}(:,6,5))

subplot(2,1,2)
% plot(BSet{3}(:,5,5))
hold on
plot(dBSet{3}(:,10,7))
plot(dBSet{3}(:,9,7))
plot(dBSet{3}(:,8,7))
