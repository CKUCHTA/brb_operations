% Creates a plot in the XZ plane based on multiple shots scanning in the Z direction.
% The data for these plots is collected using GetZscanData_Shotlist.m where the B data
% is saved in the file ZScanBdata_AllShots.mat.

function [XZPlanFig, BSet, dBSet, ProbePos, ZposVec] = PlotXZPlan(ShotList, FolderName, SampleEvalActual, Stick, SaveData, PlotPlane);
% clear
% % ShotList = [62604 62617 62626 62634 62645 62657 62674 62688 62699 62712 62721 62733 62749 62757 62768 62779 62790 62802]; %plasma shots
% % ShotList = [62610 62612 62631 62654 62830 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum
% % ShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots dipole vertical
% ShotList = [64081 64093 64112 64134 64148 64160 64174 64190 64209 64231 64248 64265 ...
%     64281 64298 64311 64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan
% FolderName = 'DipoleCuspVertical_FullScan';
% SampleEvalActual = 180;
% Stick = 5; % plot the plan created by the stick scanned in the Z direction
% SaveData = 1;
% PlotPlane = 1;

DataFolderStruct = what(FolderName);
DataFolder = DataFolderStruct.path;
FarrayFolder = fullfile(DataFolder, '3DFarray');
% Save figure files
PlotFolder = fullfile(FarrayFolder, 'Plots','XZPlan');
if ~exist(PlotFolder, 'dir')
    mkdir(PlotFolder)
end

% load data from GetZscandData_Shotlist.m
ZscanFolder = fullfile(FarrayFolder,'Matlab_Data','ZScanData');
disp('Loading data')
load(fullfile(ZscanFolder, 'ZscanBdata_AllShots.mat'), 'BSetZdata', 'dBSetZdata', ...
'ZposArray', 'SampleEval', 'ProbePos', 'tpoints', 'FarrayZPos', 'shotData')

BSetXall = BSetZdata{1}; %  (probe, stick, z indx))
BSetYall = BSetZdata{2};
BSetZall = BSetZdata{3};

dBSetXall = dBSetZdata{1};
dBSetYall = dBSetZdata{2};
dBSetZall = dBSetZdata{3};

BSetX = zeros(size(BSetXall, 1), size(BSetXall, 2), length(ShotList));
BSetY = zeros(size(BSetYall, 1), size(BSetYall, 2), length(ShotList));
BSetZ = zeros(size(BSetZall, 1), size(BSetZall, 2), length(ShotList));

dBSetX = zeros(size(dBSetXall, 1), size(dBSetXall, 2), length(ShotList));
dBSetY = zeros(size(dBSetYall, 1), size(dBSetYall, 2), length(ShotList));
dBSetZ = zeros(size(dBSetZall, 1), size(dBSetZall, 2), length(ShotList));

% extract the data for each shot from ShotList
Shot = ShotList(1);
ShotIndex = shotData(:,1) == Shot;
DipoleTypeCompare = shotData(ShotIndex, 7);
DipoleTypeString = sprintf(', Dipole Voltage = %d[kV]', DipoleTypeCompare);
ZposVec = zeros(length(ShotList), 1); % vector to store the Z position of each shot
for ii = 1:length(ShotList)
    Shot = ShotList(ii);
    ShotIndex = find(ZposArray(:,1) == Shot);
    if isempty(ShotIndex)
        error('Shot %d not found in Data from GetZscanData_Shotlist', Shot)
    end
    if length(ShotIndex) > 1
        ShotIndex = ShotIndex(1); % take the first shot if there are multiple entries
    end
    ShotDataIndex = shotData(:,1) == Shot; 
    DipoleType = shotData(ShotDataIndex, 7);
    if DipoleType ~= DipoleTypeCompare
        error('Dipole Cap Voltage does not match for Shot %d', Shot)
    end
    
    ZposVec(ii) = ZposArray(ShotIndex, 2)*1e-2;
    
    BSetX(:,:,ii) = BSetXall(:,:, ShotIndex, SampleEvalActual);
    BSetY(:,:,ii) = BSetYall(:,:, ShotIndex, SampleEvalActual);
    BSetZ(:,:,ii) = BSetZall(:,:, ShotIndex, SampleEvalActual);

    dBSetX(:,:,ii) = dBSetXall(:,:, ShotIndex, SampleEvalActual);
    dBSetY(:,:,ii) = dBSetYall(:,:, ShotIndex, SampleEvalActual);
    dBSetZ(:,:,ii) = dBSetZall(:,:, ShotIndex, SampleEvalActual);
end
Bx = squeeze(BSetX);
By = squeeze(BSetY);
Bz = squeeze(BSetZ);
BSet = {Bx, By, Bz};

dBx = squeeze(dBSetX);
dBy = squeeze(dBSetY);
dBz = squeeze(dBSetZ);
dBSet = {dBx, dBy, dBz};

Stick1Ypos = 0.03; % stick 1 y position (meters)
StickPos = Stick1Ypos + (0:(-12):-84)*1e-2; % stick positions

% plot dipole
yCoordinate = -0.4;
zCoordinate = -0.39;
xCoordinate = 0;
R = 0.257/2;
theta = 0:0.01:2*pi;
yDipole = ones(length(theta))*yCoordinate;
xDipole = R*sin(theta) + xCoordinate;
zDipole = R*cos(theta) + zCoordinate;

% Plot Parameters
label = {'x' 'y' 'z'};
climBx = {[-1.5 1.5] [-1.5 1.5] [-1.5 1.5] [-1.5 1.5] [-3 3] [-1.5 1.5] [-1.5 1.5] [-1.5 1.5]};
climDBx = {[-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-100 100] [-100 100]};

climBy = {[-1.5 1.5] [-1.5 1.5] [-1.5 1.5] [-1.5 1.5] [-3 3] [-1.5 1.5] [-1.5 1.5] [-1.5 1.5]};
climDBy = {[-100 100] [-100 100] [-100 100] [-100 100] [-600 200] [-100 100] [-100 100] [-100 100]};

climBz = {[-2 2] [-2 2] [-2 2] [-2 2] [-2 10] [-2 2] [-2 2] [-2 2]}; 
climDBz = {[-200 200] [-200 200] [-200 200] [-200 200] [-300 100] [-200 200] [-200 200] [-200 200]};

Bclims = {climBx climBy climBz}; % pcolor limits for B
dBclims = {climDBx climDBy climDBz};

XZPlanFig = figure(2);
clf
if PlotPlane == 1
    time = sprintf(', \\Deltat = %0.1f\\mus', SampleEvalActual*1e-1);
    sgtitle({'Y = ' + string(StickPos(Stick)) + DipoleTypeString + time, strjoin(string(ShotList))})
    ind = 0;
    for k = 1:6
        if k == 1||k ==2||k ==3
            B = squeeze(BSet{k}(:,Stick,:));
            Bclim = Bclims{k};
            subplot(2,3,k)
            pcolor(ZposVec,ProbePos{k},B*1e3), shading interp
            % pcolor(B*1e3), shading interp
            hold on
            plot(zDipole, xDipole, 'r', 'LineWidth', 2)
            colorbar
            xlabel('Z[m]')
            ylabel('X[m]')
            title(sprintf('B%s[mT]', label{k}))
            clim(Bclim{Stick})
            axis equal
        else
            ind = ind + 1;
            % dB = squeeze(dBSet{k}(SamplePoint,:,:));
            dBSetk = dBSet{ind};
            dBclim = dBclims{ind};
            dB = squeeze(dBSetk(:,Stick,:));
            if ind == 3
                % dB(3,5) = -dB(3,5);
            end
            subplot(2,3,k)
            pcolor(ZposVec,ProbePos{ind},dB), shading interp
            hold on
            plot(zDipole, xDipole, 'r', 'LineWidth', 2)
            % pcolor(dB), shading interp
            colorbar
            xlabel('Z[m]')
            ylabel('X[m]')
            title(sprintf('dB%s[T/s]', label{ind}))
            clim(dBclim{Stick})
            axis equal
        end
    end
    set(XZPlanFig, 'Position',  [100, 100, 1200, 650])
end
% close(XZPlanFig)