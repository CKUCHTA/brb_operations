% Plot data from scan of 3Dfluxarray at a specifid sample point using GetZscanData

% function PlotZscanData(FolderName, SampleEvalActual, ShotList)
clear
SampleEvalActual = 1950; % sample point plots are evaluated at
SavePlots = 0; % 1 to save plots, 0 to not save
% FolderName = 'DipoleCuspVertical2024';
FolderName = 'DipoleCuspVertical_FullScan';
SaveData = 0; % Save data from GetZScanData

% Get data folder
DataFolderStruct = what(FolderName);
DataFolder = DataFolderStruct.path;
FarrayFolder = fullfile(DataFolder, '3DFarray');
% Save figur files
PlotFolder = fullfile(FarrayFolder, 'Plots', 'Z_ScanTimeTrace');
if ~exist(PlotFolder, 'dir')
    mkdir(PlotFolder)
end

BxPlotFile = fullfile(PlotFolder, 'BxScan.png');
ByPlotFile = fullfile(PlotFolder, 'ByScan.png');
BzPlotFile = fullfile(PlotFolder, 'BzScan.png');    

dBxPlotFile = fullfile(PlotFolder, 'dBxScan.png');
dByPlotFile = fullfile(PlotFolder, 'dByScan.png');
dBzPlotFile = fullfile(PlotFolder, 'dBzScan.png');


% ShotList = [62604 62617 62626 62634 62645 62657 62674 62688 62699 62712 62721 62733 62749 62757 62768 62779 62790 62802]; %plasma shots
% ShotList = [62610 62612 62631 62654 62830 62665 62687 62698 62709 62720 62731 62816 62755 62766 62777 62788 62800 62815]; % dipole vacuum

% Dipole at Y = 40cm in Vertical Orinatation
% ShotList = [63345 63358 63367 63382 63390 63401 63415 63426 63434 63445 63457 63467]; % Plasma shots
% ShotList = [63354 63365 63376 63387 63398 63409 63420 63431 63442 63454 63465 63476]; % Dipole vacuum shots

% ShotList = [64091 64110 64133 64146 64147 64173 64186 64187 64230 64247 64263 64264 ...
%     64295 64309 64325 64341 64353 64396 64388 64417 64446 64473 64491 64503]; % Vacuum 1kV Dipole Cap full scan

% ShotList = [64081 64093 64112 64134 64148 64160 64174 64190 64209 64231 64248 64265 ...
    % 64281 64298 64311 64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan
% ShotList = [64081 64093 64112 64134 64148 64160 64174 64190 64209 64231 64248 64621 ...
    % 64629 64635 64311 64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan with Redone shots

% ShotList = [64086 64097 64118 64140 64154 64167 64180 64196 64204 64238 64257 64272 ...
    % 64288 64303 64317 64332 64348 64361 64376 64406 64434 64464 64483 64498]; % Drive Only full scan

% ShotList = [64082 64095 64115 64137 64151 64163 64177 64193 64220 64234 64254 64269 ...
    % 64285 64300 64315 64331 64345 64358 64370 64400 64428 64456 64479 64495]; % 2kV Dipole Cap full scan
% ShotList = [64082 64095 64115 64137 64151 64163 64177 64193 64220 64234 64254 64623 ...
%     64631 64637 64315 64331 64345 64358 64370 64400 64428 64456 64479 64495]; % 2kV Dipole Cap full scan with Redone shots

% ShotList = [64089 64108 64131 64144 64158 64171 64184 64205 64227 64244 64261 64276 ...
    % 64293 64307 64323 64339 64352 64395 64385 64421 64443 64469 64488 64505]; % Vacuum 2kV Dipole Cap full scan
% ShotList = [64089 64108 64131 64144 64158 64171 64184 64205 64643 64244 64261 64276 ...
    % 64293 64307 64323 64339 64352 64395 64385 64421 64443 64469 64488 64505]; % Vacuum 2kV Dipole Cap full scan with Redone shots
% Bad Vacuum shots: 64227, 

dipoleType = 'dipole1kV'; % 'dipole1kV', 'dipole2kV'
FileName = sprintf('ZScanData_Sample%d.mat', SampleEvalActual);
DataFile = fullfile(FarrayFolder, 'Matlab_Data', 'ZScanData', dipoleType, FileName); % file matlab data is saved

try
    disp('Loading data')
    load(DataFile, 'BSetZdata', 'dBSetZdata', 'ZposArray', 'SampleEval', 'ProbePos', 'tpoints')
    if SampleEvalActual ~= SampleEval
        disp('Data for sample point note created yet')
        [BSetZdata, dBSetZdata, ZposArray, ProbePos, tpoints] = GetZscanData_shotlistGetZscanData_Shotlist(FolderName, SampleEval, SaveData, ShotList, dipoleType);
    end
catch
    disp('running GetZscanData')
    [BSetZdata, dBSetZdata, ZposArray, ProbePos, tpoints] = GetZscanData_shotlistGetZscanData_Shotlist(FolderName, SampleEval, SaveData, ShotList, dipoleType);
end

BSetXall = BSetZdata{1};
BSetYall = BSetZdata{2};
BSetZall = BSetZdata{3};

dBSetXall = dBSetZdata{1};
dBSetYall = dBSetZdata{2};
dBSetZall = dBSetZdata{3};

BSetX = zeros(size(BSetXall, 1), size(BSetXall, 2), length(ShotList));
BSetY = zeros(size(BSetYall, 1), size(BSetYall, 2), length(ShotList));
BSetZ = zeros(size(BSetZall, 1), size(BSetZall, 2), length(ShotList));

dBSetX = zeros(size(dBSetXall, 1), size(dBSetXall, 2), length(ShotList));
dBSetY = zeros(size(dBSetYall, 1), size(dBSetYall, 2), length(ShotList));
dBSetZ = zeros(size(dBSetZall, 1), size(dBSetZall, 2), length(ShotList));

% extract the data for each shot from ShotList
ZposVec = zeros(length(ShotList), 1); % vector to store the Z position of each shot
for ii = 1:length(ShotList)
    Shot = ShotList(ii);
    ShotIndex = find(ZposArray(:,1) == Shot);
    ZposVec(ii) = ZposArray(ShotIndex, 2);
    
    BSetX(:,:,ii) = BSetXall(:,:,ShotIndex);
    BSetY(:,:,ii) = BSetYall(:,:,ShotIndex);
    BSetZ(:,:,ii) = BSetZall(:,:,ShotIndex);

    dBSetX(:,:,ii) = dBSetXall(:,:,ShotIndex);
    dBSetY(:,:,ii) = dBSetYall(:,:,ShotIndex);
    dBSetZ(:,:,ii) = dBSetZall(:,:,ShotIndex);
end
%%
% plot dB data
dBxPlot = figure(91);
clf
sgtitle(sprintf('dBx, Sample = %d', SampleEvalActual))     
for stk = 1:8
    for ch = 1:10
    subplot(2,4,stk)
    offset = 100*ch;
    % if stk == 5 && ch == 8 || ch == 7 || ch == 6
    %     continue
    % end
    plot(ZposVec*0.01,squeeze(dBSetX(ch,stk,:))+offset)
    hold on
    ylim([-400 1200])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end

dByPlot = figure(92);
clf
sgtitle(sprintf('dBy, Sample = %d', SampleEvalActual))
for stk = 1:8
    for ch = 1:10
    subplot(2,4,stk)
    offset = 100*ch;
    plot(ZposVec*0.01,squeeze(dBSetY(ch,stk,:))+offset)
    hold on
    ylim([-400 1200])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end

dBzPlot = figure(93);
clf
sgtitle(sprintf('dBz, Sample = %d', SampleEvalActual))
for stk = 1:8
    for ch = 1:10
    subplot(2,4,stk)
    offset = 100*ch;
    plot(ZposVec*0.01,squeeze(dBSetZ(ch,stk,:))+offset)
    hold on
    ylim([-400 1200])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end

%%
%Plot B data
BxPlot = figure(81);
clf
sgtitle(sprintf('Bx, Sample = %d', SampleEvalActual))     
for stk = 1:8
    for ch = 1:10
    subplot(2,4,stk)
    offset = 1.1*ch;
    plot(ZposVec*0.01,(squeeze(BSetX(ch,stk,:))*1e3)+offset)
    hold on
    ylim([0 14])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end
%%
ByPlot = figure(82);
clf
sgtitle(sprintf('By, Sample = %d', SampleEvalActual))
for stk = 1:8
    for ch = 1:12
    subplot(2,4,stk)
    offset = 2*ch;
    % if stk == 5 && ch == 7
    %    continue
    % end
    plot(ZposVec*0.01,squeeze(BSetY(ch,stk,:)*1e3)+offset)
    hold on
    % ylim([-5 22])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end
%%
BzPlot = figure(83);
clf
sgtitle(sprintf('Bz, Sample = %d', SampleEvalActual));
for stk = 1:8
    for ch = 1:10
    subplot(2,4,stk)
    offset = 2*ch;
    % if stk == 5 && ch==6
    %    continue
    % end
    plot(ZposVec*0.01,squeeze(BSetZ(ch,stk,:)*1e3)+offset)
    hold on
    ylim([-5 22])
    % xlim(xlims)
    title(sprintf('Stick %d', stk))
    xlabel('Z (m)')
    end
end

if SavePlots == 1
    saveas(BxPlot, BxPlotFile)
    saveas(ByPlot, ByPlotFile)
    saveas(BzPlot, BzPlotFile)
    saveas(dBxPlot, dBxPlotFile)
    saveas(dByPlot, dByPlotFile)
    saveas(dBzPlot, dBzPlotFile)
end
