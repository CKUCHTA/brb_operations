% This grabs data from the 3DFarry at different shots with the flux array at
% different Z position for a given sample point and shot list.  The B data is collected
% into a array for all of the position and time and saved in the file:
% /Users/paulgradney/Research/Data/DipoleCusp/DipoleCuspVertical_FullScan/3DFarray/Matlab_Data/ZScanData/ZscanBdata_AllSamples.mat

clear
SampleWindow = 1820:2150; % sample points to evaluate for each shot
ParentFolder = 'DipoleCuspVertical_FullScan';
SaveData = 1; % set to 1 to save data from data collected in ZcanData

% Create Shotlist
ShotList_Te = [64162 64150 64217 64231 64267 64278 64454 64427 64328 64368 64366 64478 64493 ...
    64164 64152 64220 64235 64271 64287 64302 64314 64330 64345 64359 64479 64497 ...
    64167 64155 64223 64238 64272 64289 64304 64318 64333 64376 64361 64484 64499]; % Te probe shots

OnePerPosition = 0; % 1 = chooses one shot from set of shots with same flux probe position, 0 = all shots
[PlasmaShotLists, VacuumShotLists, shotData] = CreateShotList_dipole(ParentFolder, OnePerPosition);

AllShots = [PlasmaShotLists{1}(:,1)', PlasmaShotLists{2}(:,1)', PlasmaShotLists{3}(:,1)', ...
    VacuumShotLists{1}(:,1)', VacuumShotLists{2}(:,1)', VacuumShotLists{3}(:,1)', ShotList_Te];

ZscanData(ParentFolder, SampleWindow, SaveData, AllShots);


function [BSetZdata, dBSetZdata, ZposArray, ProbePos, FarrayZPos, tpoints] = ...
    ZscanData(ParentFolder, SampleWindow, SaveData, Shotlist)
    % clear
    % % ParentFolder = 'DipoleCuspVertical2024'; % parent directory of data folder
    % % ParentFolder = 'DipoleCusp2024'; % parent directory of data folder
    % ParentFolder = 'DipoleCuspVertical_FullScan';
    % SampleEval = 1950; % sample point to evaluate for each probe and each shot
    % SaveData = 0;
    % SampleWindow = 1820:2150; % sample points to evaluate for each shot
    % % Shotlist = [63350 63361 63372 63383 63394 63405 63416 63428 63438 63450 63461 63472]; % Drive Coil Only dipole vertical
    % % DipoleDriveVacShotList = [63352 63363 63374 63385 63396 63407 63418 63429 63441 63452 63463 63474]; % Drive Coil and Dipole vacuum shot dipole vertical
    % Shotlist = [64091 64110 64133 64146 64147 64173 64186 64187 64230 64247 64263 64264 ...
    %     64295 64309 64325 64341 64353 64388 64396 64415 64446 64473 64491 64503]; % 1kV Dipole Cap full scan


    DataFolderStruct = what(ParentFolder);
    DataFolder = DataFolderStruct.path;
    ZscanFolder = fullfile(DataFolder,'3DFarray','Matlab_Data','ZScanData');
    
    % ZscanFolder = fullfile(DataFolder,'3DFarray','Matlab_Data','ZScanData', dipoleType);
    if ~exist(ZscanFolder, "dir")
        mkdir(ZscanFolder)
    end
    % FileName = sprintf('ZScanData_Sample%d.mat', SampleEval);
    DataFile = fullfile(ZscanFolder, 'ZScanBdata_AllShots'); % file matlab data is saved

    savePlot = 0;
    saveData = 0; % set to 1 to save dtaq data (if you are using previously saved data, set to 0)

    IncludeContour = 0; % set to 1 to plot flux contours (Unless you are using interpolated data, these contours are wrong!!!)
    PlotFlux = 0    ; % set to 1 to plot flux
    PlotTimeTrace = 0; % set to 1 for raw data time trace

    xlims = [1000 2000]; % plot range for sample points
    SampleRange=69000:73000; % only matters for data that has not been downloaded from mdsplus
    dt=1e-7; % time between samples
    IIm=901:980; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
    IIoff=1020; %Makes Bz=Bh at this index

    % B and dB arrays for Z position.  z indx is the index of the z position in the ZposArray
    BxSetZdata = zeros(10, 8, length(Shotlist), length(SampleWindow)); %  (probe, stick, z indx))
    BySetZdata = zeros(12, 8, length(Shotlist), length(SampleWindow)); %  (probe, stick, z indx))
    BzSetZdata = zeros(10, 8, length(Shotlist), length(SampleWindow)); %  (probe, stick, z indx))

    dBxSetZdata = zeros(10, 8, length(Shotlist), length(SampleWindow)); %  (probe, stick, z indx))
    dBySetZdata = zeros(12, 8, length(Shotlist), length(SampleWindow)); %  (probe, stick, z indx))
    dBzSetZdata = zeros(10, 8, length(Shotlist), length(SampleWindow)); %  (probe, stick, z indx))

    % collect data from excel file
    excelFileName = 'DipoleCusp2024.xlsx';
    docid = '1YbMnfhPIyXe3SAJL9hR5xkr6GhluWjYWQPfE1WV9LpA';
    webSave = 0; % set to 1 to download current excel file from google spread
    [shotData, ~] = DipoleShotData(excelFileName, ParentFolder, docid, webSave);
    %%%%%%%%%%%%%%%%%%% Spreadsheet data %%%%%%%%%%%%%%%%%
    % columns in shotData: [ShotNumber(1); FluxArray2_Zpos(2); MachProbStick1_ZPos(3);
    % DipoleCenter_ZPos(4); Te1_ZPos(5); NumberOfGunsUsed(6); DipoleCap_Volt(7); CT2_Volt(8)]
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    FarrayZPos = zeros(length(Shotlist),2);
    ZposArray = zeros(length(Shotlist), 2);
    badshotlist = zeros(1, length(Shotlist));
    ii = 0;
    for SampleEval = SampleWindow
        ii = ii + 1;
        k = 0;
        fprintf('Sample %d of %d\n', SampleEval, SampleWindow(end))
        for shot = Shotlist
            k = k+1;
            shotIndx = find(shotData(:,1) == shot);
            if isempty(shotIndx)
                badshotlist(k) = shot;
                continue
            end

            [daq, Bh] = loadDtaqData3DFarray(shot, saveData, SampleRange); % load dtac from harddrive or from mdsplus
            [~, ~, tpoints, dBSetRaw] = load3DFarrayDipole(daq, SampleRange, dt, shot, PlotTimeTrace, xlims); 
            [BSet, dBSet, ProbePos] = Calibrate_3DFarryDipole(IIoff, IIm, dt, dBSetRaw);

            BSetX = BSet{1};
            BSetY = BSet{2};
            BSetZ = BSet{3} + Bh; % add Helmholtz field to Bz

            dBSetX = dBSet{1};
            dBSetY = dBSet{2};
            dBSetZ = dBSet{3};

            Zpos = shotData(shotIndx, 2);
            ZposArray(k, 1) = shot;
            ZposArray(k, 2) = Zpos;
            
            FarrayZPos(k, 1) = shot;
            FarrayZPos(k, 2) = shotData(shotIndx, 2);    
            
            % B''SetZdata arrays are arranged as: (probe(X), stick(Y), shot (Z), sample point)
            BxSetZdata(:,:,k,ii) = BSetX(SampleEval, :, :);
            BySetZdata(:,:,k,ii) = BSetY(SampleEval, :, :);
            BzSetZdata(:,:,k,ii) = BSetZ(SampleEval, :, :);

            dBxSetZdata(:,:,k,ii) = dBSetX(SampleEval, :, :);
            dBySetZdata(:,:,k,ii) = dBSetY(SampleEval, :, :);
            dBzSetZdata(:,:,k,ii) = dBSetZ(SampleEval, :, :);
        end
    end
    BSetZdata = {BxSetZdata, BySetZdata, BzSetZdata};
    dBSetZdata = {dBxSetZdata, dBySetZdata, dBzSetZdata};

    if SaveData == 1
        save(DataFile, 'BSetZdata', 'dBSetZdata', 'ZposArray', 'SampleEval', ...
        'ProbePos', 'tpoints', 'FarrayZPos', 'shotData')
    end
end
