% Grab data from MDSPlus without saving for the 3D Flux Array.
%
% Parameters
% ----------
% shotnum
%   The number of the shot to get data for.
% firstSample, lastSample
%   The indeces of the start and end to grab from MDSplus. Set the 
%   `lastSample` to `-1` to get all points up to the final point. If 
%   `firstSample` is undefined then it is set to the start of the sample 
%   (0) and if `lastSample` is undefined then it is set to the last index 
%   in the signal, i.e. -1.


function [daq, AllBz, Bh] = acq_data3DFarray(shotnum, firstSample, lastSample)
% clear
% shotnum = 63275;
% SampleRange = 69000:77000;

% Set the `firstSample` and `lastSample` do their default values if they
% aren't passed by the user.
if ~exist('firstSample', 'var')
    firstSample = 0;
end
if ~exist('lastSample', 'var')
    lastSample = -1;
end

tic
AllBz = 0;
mdsconnect('skywalker.physics.wisc.edu');
mdsopen('wipal', shotnum)
% shotnum = mdsvalue('$shot');

daq=struct();
% time=struct();
Bh_Tesla = mdsvalue('.RAW.A373_DIRECT:CH_43');% Helmholtz in T
Bh = mean((Bh_Tesla)*200*0.3521*1e-4);% Helmholtz in mT
sprintf('Bh = %0.4f', Bh)

if shotnum < 61670
    fprintf('flux array did not exist for shot %d\n', shotnum)
else
    all_serials = [116 128 129 320 323 324 325 326];
end


% Call all dtaqs with one command and set the sample range that is downloaded
serialCommands = cell(size(all_serials));
k = 0;
for serial = all_serials
    disp(serial)
    k = k + 1;
    channels = 1:32;
    for ch = channels
        if lastSample == -1
            lastSampleString = sprintf('SHAPE(\\ta%d_%02d)', serial, ch);
        else
            lastSampleString = sprintf('%d', lastSample);
        end

        if ch == 1
            command = sprintf('BUILD_SIGNAL( [ DATA(\\ta%d_%02d)[%d : %s]', serial, ch, firstSample, lastSampleString);
        elseif ch == channels(end)
            command = strcat(command, sprintf(', DATA(\\ta%d_%02d)[%d : %s]], *, DIM_OF(\\ta%d_01)[%d : %s])', serial, ch, firstSample, lastSampleString, serial, firstSample, lastSampleString));
        else
            command = strcat(command, sprintf(', DATA(\\ta%d_%02d)[%d : %s]', serial, ch, firstSample, lastSampleString));
        end
    end
    serialCommands{k} = command;  
end

% Create the full command to send to mdsplus. If there are too many points
% in the full call then it is split into two calls.
if lastSample == -1
    numberOfPointsInFullCall = 360000 * 32 * 8; % number of time points * number of channels * number of digitizers.
else
    numberOfPointsInFullCall = (lastSample - firstSample) * 32 * 8;
end

if numberOfPointsInFullCall > 360000 * 32 * 4
    % Use two separate commands because a single call would be too big.
    firstCommand = sprintf( ...
        'BUILD_SIGNAL( [ %s, %s, %s, %s ], *, DIM_OF(%s))'...
        , serialCommands{1}, serialCommands{2}, serialCommands{3}, serialCommands{4},...
        serialCommands{1} ...
    );
    secondCommand = sprintf( ...
        'BUILD_SIGNAL( [ %s, %s, %s, %s ], *, DIM_OF(%s))'...
        , serialCommands{5}, serialCommands{6}, serialCommands{7}, serialCommands{8},...
        serialCommands{1} ...
    );
    
    % Call the data from the MDSplus tree.
    firstCommandData = mdsvalue(firstCommand);
    secondCommandData = mdsvalue(secondCommand);

    for relativeDigitizerIndex = 1:size(firstCommandData, 3)
        % Get the digitizer numbers for the corresponding row in the data.
        firstCommandDigitizerNumber = all_serials(relativeDigitizerIndex);
        secondCommandDigitizerNumber = all_serials(relativeDigitizerIndex + 4);

        % Load the data into `daq`.
        daq.(['a' num2str(firstCommandDigitizerNumber)]) = firstCommandData(:, :, relativeDigitizerIndex);
        daq.(['a' num2str(secondCommandDigitizerNumber)]) = secondCommandData(:, :, relativeDigitizerIndex);
    end
else
    % Use a single command if it there are fewer points.
    FullCommand = sprintf('BUILD_SIGNAL( [ %s, %s, %s, %s, %s, %s, %s, %s ], *, DIM_OF(%s))'...
        , serialCommands{1}, serialCommands{2}, serialCommands{3}, serialCommands{4},...
        serialCommands{5}, serialCommands{6}, serialCommands{7}, serialCommands{8}, serialCommands{1});
    
    allSerialData = mdsvalue(FullCommand);  % send command to mdsplus
    
    % separate data for each dtaq
    for ind = 1:size(allSerialData,3)
        serial = all_serials(ind);
        daq.(['a' num2str(serial)]) = allSerialData(:,:,ind);
    end
end


mdsclose;
mdsdisconnect;

toc
% example daq.([a113])(:,1) = mdsvalue(['   a113_01']);
%output = mdsvalue('\hook_bdot1_r')