% Plot flux Array data using load3DFarray
% created 20Nov2023

function  Plot3DFarray(shot, savePlot, saveData, IncludeContour, PlotFlux, Folder, Stick1Pos, setFigurePosition)
% clear
% % shot = 0;
% % shot = 62271;
% shot = 64944;
% savePlot = 0;
% saveData = 1; % save dtaq data
% IncludeContour = 0; % set to 1 to plot flux contours (Unless you are using interpolated data, these contours are wrong!!!)
% PlotFlux = 0    ; % set to 1 to plot flux
% Folder = '3DFluxArray_Calibration'; % Parent folder to all plots, dtaq data, and matlab data are saved
% Stick1Pos = 0;
% setFigurePosition = 1; %set to 1 to use preset figure positions

% Parameters for load3DFarray
SamRange=69000:73000;
IIm=1740:1800; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
dt=1e-7; % time between samples
IIoff=1800; %Makes Bz=Bh at this index

[daq, Bh] = loadDtaqData3DFarray(shot, saveData, SamRange); % load data from harddrive or from mdsplus
% [BSet, dBSet, ProbePos , SampleRange, Bh, tpoints] = load3DFarray_12July24(daq, Bh, SamRange, dt, IIm, IIoff);
[dBSetRaw, SamRange, Bh, tpoints] = load3DFarray(daq, Bh, dt); % organize data by sticks
[BSet, dBSet, ProbePos, Bh] = calibrate3DFarray(IIoff, IIm, dt, dBSetRaw, Bh); % convert voltage to Bdot values and calibrate


if savePlot == 1
    ShotFolderStruct = what(Folder);
    ShotFolder = ShotFolderStruct.path;
    if IncludeContour == 1
        PlotFolder = fullfile(ShotFolder,'Plots/StickContourPlots');
    else
        PlotFolder = fullfile(ShotFolder,'Plots/StickPlots');
    end
    PlotFile1to4R = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Stk1to4_R.png', shot));
    PlotFile5to8R = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Stk5to8_R.png', shot));
    
    PlotFile1to4P = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Stk1to4_Phi.png', shot));
    PlotFile5to8P = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Stk5to8_Phi.png', shot));
    
    PlotFile1to4Z = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Stk1to4_Z.png', shot));
    PlotFile5to8Z = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Stk5to8_Z.png', shot));
end


BSetR = BSet{1};
BSetP = -BSet{2};
BSetZ = -BSet{3};

dBSetR = dBSet{1};
dBSetP = -dBSet{2};
dBSetZ = -dBSet{3};

RprobeRpos = ProbePos{1}; % radial position of R probes
PprobeRpos = ProbePos{2}; % radial position of Phi probes
ZprobeRpos = ProbePos{3}; % radial position of Z probes

StickPos = -((1:12:8*12)-1 + Stick1Pos)*1e-2; % Stick Position in meters

%%
%Plotting parameters
xlims=[950, 1400];
xlims=[1800, 2050];
% xlims=[1000, 1780];
GlobalRange = SamRange(xlims);
% xlims = [7000 9000]; %Jets
tlims = tpoints(xlims);
coord = {'z' 'r' 'p'};
cAxes = {[-10 10] [-4 4] [-4 4]}; % pcolor limits for B

% climBr = [-5 5];
climBr = [-2 2];
climDBr = [-1 1]*1e3;
% climDBr = [-0.1 0.1]*1e3; % for vacuum


climBp = [-2 2];
climDBp = [-1 1]*1e3;
% climDBp = [-0.05 0.05]*1e3; %vacuum

climBz = [-10 10];
climDBz = [-5 5]*1e3;

% Add HH field and Calculate Psi
Phi = struct();
for j = 1:size(BSetZ,3)
    BZ = BSetZ(:,:,j);
    BZ = BZ - Bh;

    % Smooth Bz Data
    % sBz=smoo(BZ,4);
    % ssBz=BZ-sBz;
    % ssBz=smoo(ssBz,75);
    % BZ=BZ-ssBz;
    BSetZ(:,:,j) = BZ;
    
    % Calculate Flux
    Phi(j).flux = BzFlux_FluxArray(BZ, ZprobeRpos);
end

phiContour1 = (-1:0.5:7)*1e-3; %countour line spacing
phiContour2 = (-1:0.3:7)*1e-3; %countour line spacing
%%
% plot Flux
if PlotFlux == 1
    figure(44), clf
    pcolor(tpoints, ZprobeRpos, Phi(1).flux'), shading interp
    hold on
    contour(tpoints, ZprobeRpos,Phi(1).flux', phiContour1,'k', 'Linewidth', 1.2)
    contour(tpoints, ZprobeRpos,Phi(1).flux', phiContour2,'k')
    % contour(tpoints, ZprobeRpos,Phi(1).flux','k')
    colorbar
    clim([-4 2]*1e-3)
    xlim(tlims)
end

%%

% Bz field Plots for Sticks 1-4
fluxArrayPlot1to4_Z = figure(55);
clf
sgtitle(sprintf('Shot %d, FluxArray Probe, B_h = %0.1f[mT]', shot, Bh*1e3))
k = 0;
for i = 1:4
    k = k+2;
    subplot(4,2,k)
    pcolor(tpoints, ZprobeRpos,BSetZ(:,:,i)'*1e3),shading interp
    hold on
    contour(tpoints, ZprobeRpos,BSetZ(:,:,i)'*1e3,[0 0],'r', 'Linewidth', 1.5)
    if IncludeContour == 1
        contour(tpoints, ZprobeRpos,Phi(i).flux', phiContour1,'k', 'Linewidth', 1.2)
        contour(tpoints, ZprobeRpos,Phi(i).flux', phiContour2,'k')
    end
    colorbar
    %clim(climBz)
    xlim(tlims)
    title(sprintf('Bz[mT] Stick %d', i))
    ylabel('r [m]')
    xlabel('t [\mus]')
end

% dBz field Plots for Sticks 1-4
k = 1;
for i = 1:4
    subplot(4,2,k)
    pcolor(tpoints, ZprobeRpos,dBSetZ(:,:,i)'),shading interp
    hold on
    contour(tpoints, ZprobeRpos,BSetZ(:,:,i)'*1e3,[0 0],'r', 'Linewidth', 1.5)
    if IncludeContour == 1
        contour(tpoints, ZprobeRpos,Phi(i).flux', phiContour1,'k', 'Linewidth', 1.2)
        contour(tpoints, ZprobeRpos,Phi(i).flux', phiContour2,'k')
    end
    colorbar
    %clim(climDBz)
    xlim(tlims)
    title(sprintf('dBz[T/s] Z = %0.2fm', StickPos(i)))
    ylabel('r [m]')
    xlabel('t [\mus]')
    k = k+2;
end
if setFigurePosition == 1
    set(gcf, 'Position',  [-50, 100, 650, 900])
    fontsize(16,'points')
end
%%


% Bz field Plots for Sticks 5-8
fluxArrayPlot5to8_Z = figure(56);
clf
sgtitle(sprintf('Shot %d, FluxArray Probe, B_h = %0.1f[mT]', shot, Bh*1e3))
k = 0;
for i = 5:8
    k = k+2;
    subplot(4,2,k)
    pcolor(tpoints, ZprobeRpos,BSetZ(:,:,i)'*1e3),shading interp
    hold on
    contour(tpoints, ZprobeRpos,BSetZ(:,:,i)'*1e3,[0 0],'r', 'Linewidth', 1.5)
    if IncludeContour == 1
        contour(tpoints, ZprobeRpos,Phi(i).flux', phiContour1,'k', 'Linewidth', 1.2)
        % contour(tpoints, ZprobeRpos,Phi(i).flux', phiContour2,'k')
    end
    colorbar
    clim(climBz)
    xlim(tlims)
    title(sprintf('Bz[mT] Stick %d', i))
    ylabel('r [m]')
    xlabel('t [\mus]')
end

% dBz field Plots for Sticks 5-8
k = 1;
for i = 5:8
    subplot(4,2,k)
    % try
        pcolor(tpoints, ZprobeRpos,dBSetZ(:,:,i)'),shading interp
    % catch
    % end
    hold on
    contour(tpoints, ZprobeRpos,BSetZ(:,:,i)'*1e3,[0 0],'r', 'Linewidth', 1.5)
    if IncludeContour == 1
        contour(tpoints, ZprobeRpos,Phi(i).flux', phiContour1,'k', 'Linewidth', 1.2)
        % contour(tpoints, ZprobeRpos,Phi(i).flux', phiContour2,'k')
    end
    colorbar
    %clim(climDBz) 
    xlim(tlims)
    title(sprintf('dBz[T/s] Z = %0.2fm', StickPos(i)))
    ylabel('r [m]')
    xlabel('t [\mus]')
    k = k+2;
end
if setFigurePosition == 1
    set(gcf, 'Position',  [200, 100, 650, 900])
    fontsize(16,'points')
end


% Br field Plots for Sticks 1-4
fluxArrayPlot1to4_R = figure(65);
clf
sgtitle(sprintf('Shot %d, Sticks 1-4, B_h = %0.1f[mT]', shot, Bh*1e3))
k = 0;
for i = 1:4
    k = k+2;
    subplot(4,2,k)
    pcolor(tpoints, RprobeRpos,BSetR(:,:,i)'*1e3),shading interp
    hold on
    contour(tpoints, RprobeRpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5)
    if IncludeContour == 1
        flux = Phi(i).flux';
        contour(tpoints, RprobeRpos,flux(1:10,:), phiContour1,'k', 'Linewidth', 1.2)
        % contour(tpoints, RprobeRpos,flux(1:10,:), phiContour2,'k')
    end
    colorbar
    clim(climBr)
    xlim(tlims)
    title(sprintf('Br[mT] Stick %d', i))
    ylabel('r [m]')
    xlabel('t [\mus]')
end



% dBr field Plots for Sticks 1-4
k = 1;
for i = 1:4
    subplot(4,2,k)
    pcolor(tpoints, RprobeRpos,dBSetR(:,:,i)'),shading interp
    hold on
    contour(tpoints, RprobeRpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5)
    if IncludeContour == 1
        flux = Phi(i).flux';
        contour(tpoints, RprobeRpos,flux(1:10,:), phiContour1,'k', 'Linewidth', 1.2)
        % contour(tpoints, RprobeRpos,flux(1:10,:), phiContour2,'k')
    end
    colorbar
    %clim(climDBr)
    xlim(tlims)
    title(sprintf('dBr[T/s] Z = %0.2fm', StickPos(i)))
    ylabel('r [m]')
    xlabel('t [\mus]')
    k = k+2;
end
if setFigurePosition == 1
    set(gcf, 'Position',  [450, 100, 650, 900])
    fontsize(16,'points')
end


% Br field Plots for Sticks 5-8
fluxArrayPlot5to8_R = figure(66);
clf
sgtitle(sprintf('Shot %d, Sticks 5-8, B_h = %0.1f[mT]', shot, Bh*1e3))
k = 0;
for i = 5:8
    k = k+2;
    subplot(4,2,k)
    pcolor(tpoints, RprobeRpos,BSetR(:,:,i)'*1e3),shading interp
    hold on
    contour(tpoints, RprobeRpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5)
    if IncludeContour == 1
        flux = Phi(i).flux';
        contour(tpoints, RprobeRpos,flux(1:10,:), phiContour1,'k', 'Linewidth', 1.2)
        % contour(tpoints, RprobeRpos,flux(1:10,:), phiContour2,'k')
    end
    colorbar
    clim(climBr)
    xlim(tlims)
    title(sprintf('Br[mT] Stick %d', i))
    ylabel('r [m]')
    xlabel('t [\mus]')
end

% dBr field Plots for Sticks 5-8
k = 1;
for i = 5:8
    subplot(4,2,k)
    pcolor(tpoints, RprobeRpos,dBSetR(:,:,i)'),shading interp
    hold on
    contour(tpoints, RprobeRpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
    if IncludeContour == 1
        flux = Phi(i).flux';
        contour(tpoints, RprobeRpos,flux(1:10,:), phiContour1,'k', 'Linewidth', 1.2)
        % contour(tpoints, RprobeRpos,flux(1:10,:), phiContour2,'k')
    end
    colorbar
    %clim(climDBr)
    xlim(tlims)
    title(sprintf('dBr[T/s] Z = %0.2fm', StickPos(i)))
    ylabel('r [m]')
    xlabel('t [\mus]')
    k = k+2;
end
if setFigurePosition == 1
    set(gcf, 'Position',  [700, 100, 650, 900])
    fontsize(16,'points')
end


% Bphi field Plots for Sticks 1-4
fluxArrayPlot1to4_P = figure(75);
clf
sgtitle(sprintf('Shot %d, Sticks 1-4, B_h = %0.1f[mT]', shot, Bh*1e3))
k = 0;
for i = 1:4
    k = k+2;
    subplot(4,2,k)
    pcolor(tpoints, PprobeRpos,BSetP(:,:,i)'*1e3),shading interp
    hold on
    contour(tpoints, PprobeRpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
    if IncludeContour == 1
        flux = Phi(i).flux';
        contour(tpoints, RprobeRpos,flux(1:10,:), phiContour1,'k', 'Linewidth', 1.2)
        % contour(tpoints, RprobeRpos,flux(1:10,:), phiContour2,'k')
    end
    colorbar
    clim(climBp)
    xlim(tlims)
    title(sprintf('B\\phi[mT] Stick %d', i))
    ylabel('r [m]')
    xlabel('t [\mus]')
end

% dBphi field Plots for Sticks 1-4
k = 1;
for i = 1:4
    subplot(4,2,k)
    pcolor(tpoints, PprobeRpos,dBSetP(:,:,i)'),shading interp
    hold on
    contour(tpoints, PprobeRpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
    if IncludeContour == 1
        flux = Phi(i).flux';
        contour(tpoints, RprobeRpos,flux(1:10,:), phiContour1,'k', 'Linewidth', 1.2)
        % contour(tpoints, RprobeRpos,flux(1:10,:), phiContour2,'k')
    end
    colorbar
    %clim(climDBp)
    xlim(tlims)
    title(sprintf('dB\\phi[T/s] Z = %0.2fm', StickPos(i)))
    ylabel('r [m]')
    xlabel('t [\mus]')
    k = k+2;
end
if setFigurePosition == 1
    set(gcf, 'Position',  [950, 100, 650, 900])
    fontsize(16,'points')
end



% Bphi field Plots for Sticks 5-8
fluxArrayPlot5to8_P = figure(76);
clf
sgtitle(sprintf('Shot %d, Sticks 5-8, B_h = %0.1f[mT]', shot, Bh*1e3))
k = 0;
for i = 5:8
    k = k+2;
    subplot(4,2,k)
    pcolor(tpoints, PprobeRpos,BSetP(:,:,i)'*1e3),shading interp
    hold on
    contour(tpoints, PprobeRpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
    if IncludeContour == 1
        flux = Phi(i).flux';
        contour(tpoints, PprobeRpos,flux(1:10,:), phiContour1,'k', 'Linewidth', 1.2)
        % contour(tpoints, PprobeRpos,flux(1:10,:), phiContour2,'k')
    end
    colorbar
    clim(climBp)
    xlim(tlims)
    title(sprintf('B\\phi[mT] Stick %d', i))
    ylabel('r [m]')
    xlabel('t [\mus]')
end

% dBphi field Plots for Sticks 5-8
k = 1;
for i = 5:8
    subplot(4,2,k)
    % try
        pcolor(tpoints, PprobeRpos,dBSetP(:,:,i)'),shading interp
    % catch
    % end
    hold on
    contour(tpoints, PprobeRpos,BSetZ(:,1:10,i)'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
    if IncludeContour == 1
        flux = Phi(i).flux';
        contour(tpoints, PprobeRpos,flux(1:10,:), phiContour1,'k', 'Linewidth', 1.2)
        % contour(tpoints, PprobeRpos,flux(1:10,:), phiContour2,'k')
    end
    colorbar
    clim(climDBp) 
    xlim(tlims)
    title(sprintf('dB\\phi[T/s] Z = %0.2fm', StickPos(i)))
    ylabel('r [m]')
    xlabel('t [\mus]')
    k = k+2;
end
if setFigurePosition == 1
    set(gcf, 'Position',  [1200, 100, 650, 900])
    fontsize(16,'points')
end

if savePlot == 1
    saveas(fluxArrayPlot1to4_Z, PlotFile1to4Z)
    saveas(fluxArrayPlot5to8_Z, PlotFile5to8Z)
    
    saveas(fluxArrayPlot1to4_R, PlotFile1to4R)
    saveas(fluxArrayPlot5to8_R, PlotFile5to8R)
    
    saveas(fluxArrayPlot1to4_P, PlotFile1to4P)
    saveas(fluxArrayPlot5to8_P, PlotFile5to8P)
end


return

%%

%dB and B Plots for a single stick 
stick = 4;

dB_stk = dBSetZ(:,:,stick)';
B_stk= BSetZ(:,:,stick)';
climGen = climBz;
climGenDT = climDBz;
xlims = [950 1550];


figure(57), clf 
subplot(1,2,1)
% pcolor(tpoints, ZprobeRpos,dBz_stk'),shading interp
pcolor(dB_stk),shading interp
hold on
contour(BSetZ(:,1:12,stick)'*1e3,[0 0],'r', 'linewidth', 2),shading interp
if IncludeContour == 1
    contour(Phi(stick).flux', phiContour1,'k', 'Linewidth', 1.2)
    contour(Phi(stick).flux', phiContour2,'k')
end
colorbar
clim(climGenDT)
% xlim(tlims)
xlim(xlims)
title(sprintf('dBz Stick %d', stick))
ylabel('r [m]')
xlabel('t [\mus]')



subplot(1,2,2)
% pcolor(tpoints, ZprobeRpos,Bz_stk*1e3),shading interp
pcolor(B_stk*1e3),shading interp
hold on
% contour(tpoints, ZprobeRpos,Bz_stk*1e3,[0 0],'r','linewidth', 2),shading interp
contour(BSetZ(:,1:10,stick)'*1e3,[0 0],'r', 'linewidth', 2),shading interp
if IncludeContour == 1
    contour(Phi(stick).flux', phiContour1,'k', 'Linewidth', 1.2)
    contour(Phi(stick).flux', phiContour2,'k')
end
colorbar
clim(climGen)
xlim(xlims)
title(sprintf('Bz[mT] Stick %d', stick))
ylabel('r [m]')
xlabel('t [\mus]')


%%
% dBz plot for a single channel
StickNum = 6;
ProbeNum = 5;
% xlims = [950 1550];

Rpos = RprobeRpos(ProbeNum);

SingleSig = figure(58);
clf
sgtitle(sprintf('Stick %d, R = %0.2f', StickNum, Rpos))
subplot(3,1,1)
dBset1 = dBSetR(:,ProbeNum,StickNum);
plot(tpoints,dBset1)
xlim(tlims)
% ylim([-2.2 2.2]*1e3)
% xlabel('Samples')
xlabel('t[\mus]')
ylabel('dB_z[T/s]')

subplot(3,1,2)
Bset1 = BSetR(:,ProbeNum,StickNum);
% plot(tpoints,-Bset1*1e3)
plot(-Bset1*1e3)
% xlim(tlims)
xlim(xlims)
xlabel('t[\mus]')
ylabel('B_z[mT]')

fontsize(SingleSig,scale=1.5)
% FFT of dBz
range = xlims(1):xlims(2);
SamRate = 10e6;
dBsig = dBSetZ(:,ProbeNum,StickNum);
% dBsig = dBset1(:,ProbeNum);
dBsig = dBsig(range);

L = length(dBsig); %number of sample points in window
% fftdBsig = abs(fftshift(fft(dBsig))/L);
fftdBsig = fft(dBsig);
Hz = SamRate/L*(-L/2:L/2);

P1 = fftdBsig(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);

f = SamRate/L*(0:(L/2));


P2 = abs(fftdBsig/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);

f = SamRate*(0:(L/2))/L;

subplot(3,1,3)
% plot(Hz,log10(fftdBsig))
semilogx(f,P1)
xlabel('Hz')
ylabel('fft(dBz)')
title('fft of dB_z')

