% Using the data orginized in load3DFarry.m, this script calibrates the data for each direction
% and takes into account the digitizer offsets. The probe position are also calculated using the geometry
% of the arrays being parallel with the z-axis and the sticks orintated below the manifold.

function [BSet, dBSet, ProbePos, Bh] = calibrate3DFarray(IIoff, IIm, dt, dBSetRaw, Bh)
% clear
% IIm=1740:1800; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% dt=1e-7; % time between samples
% IIoff=1800; %Makes Bz=Bh at this index
% shot = 64862; % Z probe parallel to Z axis shot with TREX Coils
% shot = 64870; % Phi probe parallel to Z axis shot with TREX Coils
% % shot = 64879; % R probe parallel to Z axis shot with TREX Coils
% % shot = 64920; % Drive Cylinder vacuum shot
% % shot = 64943;
% SamRange=69000:73000;
% saveData = 1;
% [daq, Bh] = loadDtaqData3DFarray(shot, saveData, SamRange); % load dtac from harddrive or from mdsplus
% [dBSetRaw , SampleRange, Bh, tpoints] = load3DFarray(daq, Bh, dt);

PlotRaw = 0; % change to 1 for plot of digitizer data
xlims = [1800 2200];

dBSetR = dBSetRaw{1};
dBSetP = dBSetRaw{2};
dBSetZ = dBSetRaw{3};

%Convert digitizer voltage to Bdot values
AreaCorrectionFile = which('AreaCorrection.mat');
load(AreaCorrectionFile, 'RProbeAreas', 'PhiProbeAreas','ZProbeAreas');  % load Area correction factor from probeArea.m

facZ=(3.25e4/1.25)/4.4; % calibration factor converts the probe voltage signals to Bdot signals
DividerFac = 0.8199;% 0.861; % Voltage divider factor
facZCal = 1/(ZProbeAreas(3)*DividerFac);
error = 1 - (facZCal - facZ)/facZ; % percent difference of calculated from observed

facR = abs(1/(RProbeAreas(1)*DividerFac)*error); % 1/[(probeArea)*(voltage divider coeficient)]
facP = 1/(PhiProbeAreas(2)*DividerFac)*error;


%%%%%%%%% Calculate Phi Correction Factor.  Use this section with a vacuum shot to
%%% Turn the first for loop on after calibrating Phi probes and use calibrated phi probes for calibrating Z probes
% PhiFacFile = which('PhiRotationFac.mat');
% ZFacFile = which('ZRotationFac.mat');
% load(PhiFacFile, 'DelFacPhi')
% % load(ZFacFile, 'DelFacZ') 
% for stk = 1:8
%     for ch = 1:size(dBSetP,2) % loop over channels
%         dBSetP(:,ch,stk) = dBSetP(:,ch, stk) - DelFacPhi(stk,ch)*dBSetZ(:,ch,stk); % corrected phi
%     end
% end
% sample1a = 1830; %times before discharge
% sample1b = 1845;
% sample2a = 1855; %times at peak of discharge
% sample2b = 1865;

% DelFacZ = nan(8,10);
% for stk = 1:8
%     for ch = 1:size(dBSetP,2) % loop over channels
%         DelZ = mean(dBSetZ(sample2a:sample2b, ch, stk)) - mean(dBSetZ(sample1a:sample1b, ch, stk));
%         DelPhi = mean(dBSetP(sample2a:sample2b, ch, stk)) - mean(dBSetP(sample1a:sample1b, ch, stk));
%         DelR = mean(dBSetR(sample2a:sample2b, ch, stk)) - mean(dBSetR(sample1a:sample1b, ch, stk));

%         DelFacZ(stk,ch) = DelZ/DelPhi;
%         DelFacPhi(stk,ch) = DelPhi/DelZ;
        
%         % dBSetP(:,ch,stk) = dBSetP(:,ch, stk) - DelPhi/DelZ*dBSetZ(:,ch, stk); % corrected phi
%         % dBSetZ(:,ch,stk) = dBSetZ(:,ch, stk) - DelZ/DelPhi*dBSetP(:,ch,stk); % corrected z
%     end
% end
% FluxArrayStruct = what('3DFluxArray_git');
% FluxArrayFile = FluxArrayStruct.path;
% % save(fullfile(FluxArrayFile, 'PhiRotationFac.mat'), 'DelFacPhi')
% save(fullfile(FluxArrayFile, 'ZRotationFac.mat'), 'DelFacZ')

PhiFacFile = which('PhiRotationFac.mat');
ZFacFile = which('ZRotationFac.mat');
load(PhiFacFile, 'DelFacPhi')
load(ZFacFile, 'DelFacZ') 
for stk = 1:8
    for ch = 1:size(dBSetP,2) % loop over channels
        dBSetP(:,ch,stk) = dBSetP(:,ch, stk) - DelFacPhi(stk,ch)*dBSetZ(:,ch,stk); % corrected phi
    end
    for ch = 1:size(dBSetZ,2) % loop over channels
        if ch>10
            dBSetZ(:,ch,stk) = dBSetZ(:,ch, stk) - DelFacZ(stk,10)*dBSetP(:,10,stk); % corrected z
        else
            dBSetZ(:,ch,stk) = dBSetZ(:,ch, stk) - DelFacZ(stk,ch)*dBSetP(:,ch,stk); % corrected z
        end
    end
end

% Remove digitizer off-set for each stick such that signal is zero prior to drive coils turning on
% and convert signal voltage to Bdot values
for j = 1:8 
    dBR = dBSetR(:,:,j); % R-dir for stick j
    dBP = dBSetP(:,:,j);
    dBZ = dBSetZ(:,:,j);
    for k=1:size(dBR,2)
        dBR(:,k) = dBR(:,k)-mean(dBR(IIm,k)); % substract off-set
    end
    for k=1:size(dBP,2)
        dBP(:,k) = dBP(:,k)-mean(dBP(IIm,k)); % substract off-set
    end
    for k=1:size(dBZ,2)
        dBZ(:,k) = dBZ(:,k)-mean(dBZ(IIm,k)); % substract off-set
    end
    %reassign valus to dBset and multiply by convertion factor
    dBSetR(:,:,j) = dBR*facR;
    dBSetP(:,:,j) = dBP*facP;
    dBSetZ(:,:,j) = dBZ*facZ;    
end

% Integrate Bdot to get B
BSetR = zeros(size(dBSetR));
BSetP = zeros(size(dBSetP));
BSetZ = zeros(size(dBSetZ));
for k = 1:8 
    BSetR(:,:,k) = cumtrapz(dBSetR(:,:,k))*dt; % Br
    BSetP(:,:,k) = cumtrapz(dBSetP(:,:,k))*dt; % Bp
    BSetZ(:,:,k) = cumtrapz(dBSetZ(:,:,k))*dt; % Bz
end

% Smooth Bz and dBz using calibration factors and subtract offset digitizer to zero signal
% facZfile = which('facZ.mat');
% load(facZfile, 'CalfacZ')  % load correction factor from ZCalibration3DFac.m 
for stk = 1:8
    Br = BSetR(:,:,stk);
    Bp = BSetP(:,:,stk);
    Bz = BSetZ(:,:,stk);
    dBz = dBSetZ(:,:,stk);
    % facC = CalfacZ(:,stk);
    
    %substracting off the digitizer
    for ch=1:size(Bp,2)
        Bp(:,ch) = Bp(:,ch)-Bp(IIoff,ch);
        Br(:,ch) = Br(:,ch)-Br(IIoff,ch); %substract offset
    end
    for ch=1:size(Bz,2)
        Bz(:,ch) = Bz(:,ch)-Bz(IIoff,ch);
        % Bz(:,ch) = Bz(:,ch)*facC(ch); % calibrate field based on CalFacZ to smooth flux contours
        % dBz(:,ch) = dBz(:,ch)*facC(ch); % calibrate field based on CalFacZ to smooth flux contours
    end
    BSetR(:,:,stk) = Br;
    BSetP(:,:,stk) = Bp;
    BSetZ(:,:,stk) = Bz; 
    dBSetZ(:,:,stk) = dBz; 
end

% Collect B and dB data
BSet = {BSetR BSetP BSetZ};
dBSet = {dBSetR dBSetP dBSetZ};

% Determine Probe Positions
Rmanifold = 0.54; % radial position of manifold

%Radial position of Z probes
ZRvecZ12 = [0 5.05 10.15 15.25 20.35 25.4 30.45 35.55 40.15 45.75 50.8 55.9]; % Z probe locations relative to probe 11, which is furthest radially inward)
ZRvecZ1 = (0.8 + 5) - 1.5/2; % distance from manifold to first z-probe
ZRvecZ12 = (ZRvecZ1 + ZRvecZ12)*1e-2; % Probe Position realitive to manifold
dBzRpos = Rmanifold - ZRvecZ12; % Z probe radial position calculated from manifold location
dBzRpos= dBzRpos(end:(-1):1);

%Radial position of R probes
RRvecZ10 = [8.85 13.95 19.05 24.1 29.15 34.25 39.35 44.45 49.55 54.65];
RRvecZ10 = (ZRvecZ1 + RRvecZ10)*1e-2; % Probe Position realitive to manifold
dBrRpos = Rmanifold - RRvecZ10;
dBrRpos= dBrRpos(end:(-1):1);

%Radial position of Phi probes
PRvecZ10 = [11.35 16.45 21.55 26.6 31.7 36.75 41.85 46.95 52 57.1];
PRvecZ10 = (ZRvecZ1 + PRvecZ10)*1e-2; % Probe Position realitive to manifold; 
dBpRpos = Rmanifold - PRvecZ10;
dBpRpos= dBpRpos(end:(-1):1);

ProbePos = {dBrRpos dBpRpos dBzRpos}; % collection of all radial probe locations

direction = {'R' 'P' 'Z'};
if PlotRaw == 1
    for i = 1:3
        figure(10+i), clf
        sgtitle(sprintf('%s-direction Calibrated', direction{i}))
        for j = 1:8
            for ch = 1:size(dBSet{i}(:,:,j),2)
                subplot(2,4,j)
                plot(dBSet{i}(:,ch,j)+1000*ch)
                hold on
                xlim(xlims)
                title(sprintf('Stick %d', j))
            end
        end
    end
end

% plot a single channel for specified stick
figure(20), clf
stick = 2;
ch = 3;
subplot(2,1,1)
plot(dBSetZ(:,ch,stick))
xlim(xlims)
xlabel('Sample')
ylabel('dB (T/s)')
title(sprintf('Z direction, Stick %d, Channel %d', stick, ch))

subplot(2,1,2)
plot(BSetZ(:,ch,stick))
xlim(xlims)
xlabel('Sample')
ylabel('B (T)')
title(sprintf('Stick %d, Channel %d', stick, ch))
