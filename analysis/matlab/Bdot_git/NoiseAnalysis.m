% Finds the fourier transform of dBz from SpeedProbe data.  Rinput is the
% radial location the dBz data is taken from.

clear
shot = 59805;
shot = 59854;
% shot = 58635;
Rinput = 0.58; % raidal location Bdot is taken from

[Bspeed, dBspeed, rvecS, SamRange, time, Bh, probLoc] = getSpeedProbe(shot);
Bz = Bspeed{1}';
dBz = dBspeed{1}';

%Reduce Evaluation range
tMin = 2055;
tMax = 2085;
time_loc = time(tMin:tMax);
Bz_loc = Bz(:,tMin:tMax);
dBz_loc = dBz(:,tMin:tMax);

Fs = 10*1e6; % sampling frequency for dtaq
L = length(time_loc); % length of time window
% L = time_loc(1,end)-time_loc(1,1); % length of signal
T = 1/Fs; % period

[~, probLocInd] = min(abs(probLoc(1,:) - Rinput));
probPos = probLoc(1,probLocInd); % probe location where velocity is measured in meters

%find fourier transform of dBz
fourierB = fft(dBz_loc(probLocInd,:));
n = length(Bz_loc);
fshift = (-n/2:n/2-1)*(Fs/n);
shiftFourierB = fftshift(fourierB);


f = Fs*(0:(L/2))/L;


freq = (0:length(fourierB)-1)*Fs/length(fourierB);

% freq = 1./(time_loc*1e6);

% dBz Plot at specified R values
%%
figure(44), clf
sgtitle(sprintf('shot %d, at R = %0.2f [m], Snubber Connected', shot, probPos))
subplot(3,1,1)
% plot(time_loc,dBz_loc(probLocInd,:)*1e3, 'k', 'Linewidth', 2)
plot(time,dBz(probLocInd,:), 'k', 'Linewidth', 1)
yline(0, '--k')
ylabel('dB_z')
xlabel('t(\mus)')
xlim([200 225])
title(sprintf('dB_z'))

subplot(3,1,2)
% plot(time_loc,dBz_loc(probLocInd,:)*1e3, 'k', 'Linewidth', 2)
plot(time_loc,dBz_loc(probLocInd,:), 'k', 'Linewidth', 1)
yline(0, '--k')
ylabel('dB_z')
xlabel('t(\mus)')
xlim([200 225])
title(sprintf('dB_z'))

subplot(3,1,3)
% plot(f,abs(P1), 'b', 'Linewidth', 1)
semilogy(fshift,abs(shiftFourierB), 'b', 'Linewidth', 1)
yline(0, '--k')
xlabel('frequncy(Hz)')
xlim([0 max(fshift)])
title(sprintf('Frequency'))