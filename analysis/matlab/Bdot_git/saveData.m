clear
% script for saving acq2106 data to external harddrive
% [57625 57728] perturbation
% [57736:57777 57924:57945 58183:58314] No perturbation 
% [58475:58509 58511:58512 58514:58522 58525:58555 58557:585605 58607 58625:68628] No perturbation with All coils on
% [58631:58635 58637:58640 58642:58643 58645:58656 58699:58704 58706:58710
% 58712 58714:58716 58718:58752 58755:58767 58769:58779 58782 58784:58789
% 58791:58803 58805:58819] No middle coil
% [58391:58409 58411:58423 58425:58440 58455:58460 58462:58471]
shots = 59988;

for shot = shots
%     try
        [daq, AllBz, time, Bh] = acq_data2(shot);
        DataFolder = '/Users/paulgradney/Research/Data/TeSaber_Radial_Scan/6k_100G_MiddleOff/Movie1Shots';
        shot_data_file = sprintf('%d.mat',shot);
        fileName = fullfile(DataFolder, shot_data_file);
        
        save(fileName)
%     catch
%         warning('Error in saveData.m');
%     end

end

save_TeData 