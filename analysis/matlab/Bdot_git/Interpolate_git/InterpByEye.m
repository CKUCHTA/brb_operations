% This script uses the GUI from dolinesFarray to set points along features 
% for a flux array shot. The points that are set by hand form a corrdinate 
% system that allows the matlab function interp1, which is called with 
% doInter, to interpolate B and dB.  The interpolated fields are dBzIntTot2 
% and BzIntTot2 and saved in the scripts folder. The points used to generate 
% the coordinate system are PoinUse and PoinUse3.  There are two sets of
% point due to the script creating a set for each stick and a set for each
% row of probes along the z-axis.  The first set of points is generated
% with the plots in an orintation that is easiest to identify features, the
% second set is generated from the first and is best for creating movies.
clear

Configuration = 2; % set to 1 for TREX coils and 2 for Jets

DataFolder = '/Users/paulgradney/Research/Data/';


% 

GUI_onOFF = 1; %turn GUI on/off

%TREX Coils Parameters
if Configuration == 1
    ShotFolder = 'FluxArray_Calibration';
    shotsz=61994:61998;  % and 58852

    SamRange=69000:71000;
    IIm=901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
    IIoff=1000; %Makes Bz=Bh at this index
    ylims=[1000, 1380]; 
    facClim = 0.7; % multiplies range of clim
    PoinBeg = 1050; % time fitlines begin

%Jets Parameters
elseif Configuration == 2
    ShotFolder = 'Jets';
    shots = 61927; % Jets Shot

    SamRange = 17000:18200;
    IIm=1:100; 
    IIoff=100;
    ylims=[1 length(SamRange)]; 
    facClim = 8e-2; % multiplies range of clim
    PoinBeg = IIoff;
end

dt=1e-7; % time between samples

cnt=0; % count loops
  
% load '/Users/paulgradney/Research/Matlab Drive Cylinder/Bdot/FluxArray/fac.mat'  % load correction factor
 
DelB=0;
for shot=shots
    cnt=cnt+1;

    IntBdataFile = fullfile(DataFolder,ShotFolder, sprintf('Scripts/IntBdata_%d',shot));
    PointsUsedFile = fullfile(DataFolder,ShotFolder, sprintf('Scripts/PointsUsed_%d',shot));
    try
        load(PointsUsedFile, 'PoinUse', 'PoinUse3');
    catch
    end
    [daq, Bh] = loadDtaqDataFarray(shot); % load daq data from harddrive or from mdsplus
    if Configuration == 1
        [BSet, dBSet, ProbePos , ~, Bh, ~, tpoints] = loadFarray(daq, Bh, SamRange, dt, IIm, IIoff); 
    elseif Configuration == 2
        [BSet, dBSet, ProbePos , ~, Bh, ~, tpoints] = JetsloadFarray(daq, Bh, shot, SamRange, IIm, IIoff);
    end
    %   com=['load ''C:\Users\Jan Egedal\LocalWork\Matmac\2023\TREX\FluxArray\DATA\Poin' num2str(shot+3) ''''];  % loads PoinUse
% eval(com)
%  

    %Convert Cell frormat to 3D array
    dBz=zeros([length(tpoints) , 8,12]);
    Bz=zeros([length(tpoints) , 8,12]);
    for k=1:8 
        dBz(:,k,:)=dBSet{k,3};
        Bz(:,k,:)=BSet{k,3};
    end

    % Reorder probes so largest R value starts with lowest probe number
    Bz=Bz(:,:,end:(-1):1);
    dBz=dBz(:,:,end:(-1):1);
   
    
    % Interpolation by Eye Starts
    % PoinUse is the variable that contains lines for interolating flux
    % contours
    First=0;
    if exist('PoinUse')==0
        PoinUse = cell([1,12]); 
        for k=1:12
            PoinUse{k}=0; % load zeros into variable for firts run
        end
        First=1;
    end
    
    if GUI_onOFF==1 % turns on GUI for fitting by eye
        BzIntTot1 = cell([1,8]); 
        dBzIntTot1 = cell([1,8]); 
        %for k=1:12
        for k=1:8 % Interpolate B and dB for each stick
            
            %%%% What's This For %%%%%%%%
            PoinIn=PoinUse{k};
            if and(First,k>1)
                PoinIn=PoinUse{k-1};
            end
            
            % Fields for the kth stick
            BzIn=squeeze(Bz(:,k,:));
            dBzIn=squeeze(dBz(:,k,:));
      
            [Poin, BzInt, dBzInt]=dolinesFarray(PoinIn,BzIn,dBzIn,Bh,k,ylims, facClim, PoinBeg); % calls GUI
            PoinUse{k}=Poin; % points chosen by eye
    
            % interpolated B found after implementing GUI
            BzIntTot1{k}=BzInt; 
            dBzIntTot1{k}=dBzInt;
        end
    end
      
  %%

    PoinUse2 = cell([1,12]); 
    for k=1:12
        for l=1:8
            Poin1=PoinUse{l};
            Poin2(:,l)=Poin1(:,k); % flip Poin1 to be in lightsaber format
            SamRange=find(Poin2(:,l)>PoinBeg); % find the contour lines that were used after time 1050
            Poin2(SamRange,l)=sort(Poin2(SamRange,l)); % sorts lines such that they are in order
         end
                 
        PoinUse2{k}=Poin2;
    end
  %%  

 % ploting output from dolinesFarray to confirm fits were good
    figure(101),clf
    for k=1:8
        H2(k)=subplot(2,4,k);
    end

    for k=1:8
        axes(H2(k))
        pcolor(squeeze(dBz(:,k,:))), shading interp
        hold on
        plot(PoinUse{k}')
        caxis([-1 1]*9e3)
        ylim(ylims)
    end    
    
    figure(102),clf
    for k=1:12
        H(k)=subplot(3,4,k);
    end
    for k=1:12
        axes(H(k))
        pcolor(squeeze(dBz(:,:,k))), shading interp
        caxis([-1 1]*9e3)
        hold on
        plot(PoinUse2{k}')
        ylim(ylims)
    end
%%
  
    % change format of data such that it matches light saber probe and redu
    % GUI to get output in lighsaber format
    
    BzIntTot2 = cell([1,12]);
    dBzIntTot2 = cell([1,12]);
    PoinUse3 = cell([1,12]); 
    
    % build B field array along z-axis by using probes from every stick
    for k=1:12 
      PoinIn=PoinUse2{k};
      BzIn=squeeze(Bz(:,:,k));
      dBzIn=squeeze(dBz(:,:,k));
  
      [Poin, BzInt, dBzInt]=dolinesFarray(PoinIn,BzIn,dBzIn,Bh,k, ylims, facClim, PoinBeg);
      %%
      for ll=1:8
          PoinInt(ll,:)=4*(interp1(1:8,Poin(ll,:),1:0.25:8)-1)+1;
      end
      PoinInt=smoo(PoinInt',3)';
      plot(PoinInt')
      %%
      PoinUse3{k}=PoinInt;
      BzIntTot2{k}=BzInt;
      dBzIntTot2{k}=dBzInt;
    end
    
 %%   

 % Plot outputs from dolinesFarray
    figure(103),clf
    for k=1:12
        H(k)=subplot(3,4,k);
    end

    for k=1:12
        axes(H(k))
        pcolor(dBzIntTot2{k}), shading interp
        caxis([-1 1]*9e3)
        hold on
        plot(PoinUse3{k}')
        ylim(4*ylims)
    end  


 %%   
    figure(104),clf
    for k=1:12
        H(k)=subplot(3,4,k);
    end

    for k=1:12
        axes(H(k))
        pcolor(BzIntTot2{k}), shading interp
        caxis([-1 1]*Bh*1.6)
        hold on
        plot(PoinUse3{k}')
        ylim(4*ylims)
    end 

%%
      
  
    % figure(101)
    % com=['print -djpeg -r400  ''C:\Users\Jan Egedal\LocalWork\Matmac\2023\TREX\BgMovie\Jpegs\Movie1\Ashot' num2str(cnt) '.jpg'''];
    % eval(com)

    pause(0.2)
    
    save(IntBdataFile, 'dBzIntTot2', 'BzIntTot2', 'Bh')
    save(PointsUsedFile, 'PoinUse', 'PoinUse3')

end
 