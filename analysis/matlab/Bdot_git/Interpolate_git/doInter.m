function [datInt] =doInter(dat,yu, Yi, Y, IIi,Np,Ny,Nt,IIgoodProbes)

Npi=(Np-1)*4+1; % number of interpolated probe locations
Nti=4*Nt-1; % number if interpolated time-points


datOffY=repmat(0,[Ny,Np]);

for k=1:Np
    datOffY(:,k)=interp1(Y(:,k),dat(:,k),yu);
end

datI=repmat(0,[Ny,Npi]);
datInt=repmat(0,[Nti,Npi]);

for k=1:Ny
    datI(k,:)=interp1(IIgoodProbes,datOffY(k,IIgoodProbes),IIi);
end
 for k=1:Npi
     datInt(:,k)=interp1(yu,datI(:,k),Yi(:,k));
 end



end

