%load 'C:\Users\Jan Egedal\LocalWork\Matmac\2021\TREX\Poin26' % Poin PoinBeg

Npz=size(Bz,2);
Np=size(Bz,2);
Nt=size(Bz,1);
PoinZ=[[1:Np]*0+1;Poin; [1:Np]*0+Nt];
Ny=4*Nt;


% switch WhichProbe
%     case 1;
%      IIgBz=[1:4 6:14 16:24];  % good Bz channels Linear Probe
%      if Npz>Np  % case with double Bz channels
%        IIgBz=[1:6 8:10 12:17 19:28 31 33:48];  % good Bz channels
%      end
%      IIgBr=[1: 24];  % good Br channels
%      IIgBp=[1:2 4:9 11 13 15 17 19 21 23 24 ];  % good Bp channels
%     case 2;
%      IIgBz=[1:4 6:15];  % good Bz channels  SH probe
%      IIgBr=[1 3 5:6 8:9 11:15];  % good Br channels
%      IIgBp=[1:2 4:15 ];  % good Bp channels
%    case 3;
%      IIgBz=[1:15];  % good Bz channels Speed Probe
%      IIgBr=[1:15];  % good Br channels
%      IIgBp=[1:10 12:15 ];  % good Bp channels
%    case 4;
%      IIgBz=[1:4 6:15 ];
%      IIgBz=[IIgBz, 31-IIgBz(end:(-1):1)];% good Bz channels  combined SH probe
%      IIgBz=[1:4 6:20 22:25 27:30];  %getting rid of channel 21...
%      IIgBr=[1 3 5:6 8:9 11:15];  % good Br channels
%      IIgBr=[IIgBr, 31-IIgBr(end:(-1):1)];% good Br channels  combined SH probe
%      IIgBp=[1:2 4:15 ];  % good Bp channels
%      IIgBp=[IIgBp, 31-IIgBp(end:(-1):1)];% good Br channels  combined SH probe
% 
% 
% end

IIgBz=1:Np;



dPoin=gradient(PoinZ);


PoinU=PoinZ;
NpOld=Np;


    PoinU=PoinZ;
getIntMatrix  % this computes yu Yi Y IIi 
Np=NpOld;

[BzInt] =doInter(Bz,yu, Yi, Y, IIi,Npz,Ny,Nt,IIgBz);
[dBzInt] =doInter(dBz,yu, Yi, Y, IIi,Npz,Ny,Nt,IIgBz);






ylimsU=ylims*4;

figure(92),clf
subplot(2,1,1)
pcolor(1e3*BzInt),shading interp
%colorbar
hold on
contour(1e3*BzInt,[0 1e3],'r')
%caxis([-0.25 0.25]/1.2)
title(['B_{z} [\pm 8mT]'])
caxis([-8 8])
ylim(ylimsU)








subplot(2,1,2)
pcolor(dBzInt*facJ),shading interp
hold on
contour(1e3*BzInt,[0 1e3],'r')

caxis([-1 1]*150)
title(['J_{\phi} \propto dB_{z}/dt [\pm150kA/m^2]'])
ylim(ylimsU)




