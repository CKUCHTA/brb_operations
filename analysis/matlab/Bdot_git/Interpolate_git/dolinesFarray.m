% This script generatates a GUI for creating the points needed to
% interpolate the data.  These point form a new coordinate system that
% allows the interpolation to follow shared features that exist between
% sticks.  InterpByEyes calls this script.


function [Poin, BzInt, dBzInt]=dolinesFarray(PoinIn,Bz,dBz,Bh,row, ylims, facC, PoinBeg, UseOldPoints)

BzInt=0;
dBzInt=0;

Npz=size(Bz,2); % number of points in z direction (number of sticks)
Np=size(Bz,2);
Nt=size(Bz,1);
IIpz=1:Npz;

%Bh=5e-3;
% PoinBeg=1050;

if length(PoinIn)>1 % if PoinIn is not empty
    Poin=PoinIn; % use PoinIn
else
    Poin=zeros([8,Np]); % otherwise, create Poin
    Poin=Poin+PoinBeg; % and fill it with PoinBeg
end

aP=8;  %active Point
aL=1;   %active Line
WhatDat=6;
KeepGoing=1;

Vlayer=50e3;
facJ=1/1.2e-6/Vlayer/1e3; % mult into dB/dt to get current in kA/m^2

%load 'C:\Users\Jan Egedal\LocalWork\Matmac\2021\TREX\Poin26' % Poin PoinBeg
%load 'C:\Users\Jan Egedal\LocalWork\Matmac\2021\TREX\Poin769' % Poin PoinBeg

while KeepGoing

    actP=0*[1:Np];
    actP(aP)=1;
    actL=[0 0 0 0 0 0 0 0 0 0];
    actL(aL)=1;

    
        
        
    figure(301),clf
    
    % facC=1.5;
        
    if WhatDat==1
        dat=(Bz-0*Bh)*1e3;
        tit='B_z';
        clims=[-6 6]*facC;
        delZ=0;
    elseif WhatDat==2
        dat=(Bz-0*Bh)*1e3;
        tit='B_z';
        clims=[-6 6]*facC;

            
    elseif WhatDat==3
        dat=(Bz-0*Bh)*1e3;
        tit='B_z';
        clims=[-6 6]*facC;
        delZ=0.75;

    elseif WhatDat==4
        dat=dBz*facJ;
        tit='dB_z/dt';
        clims=[-100 100]*facC;
        delZ=0.0;

    elseif WhatDat==5
        dat=dBz*facJ;
        tit='dB_z/dt';
        clims=[-100 100]*facC;

    elseif WhatDat==6
        dat=dBz*facJ;
        tit='dB_z/dt';
        clims=[-100 100]*facC;

    end

    % Np=size(dat,2);
    % Nt=size(dat,1);
    % 
    col='brgmckwy';

    h1=subplot(4,7,[1 19]);
    pcolor(IIpz,1:Nt,dat),shading interp
    hold on
    title([tit ', row #' num2str(row) ])
    %contour(IIpz,1:Nt,1e3*(Bz-0*Bh),[0 1e3],'r')
    %contour(IIpz,1:Nt,1e3*(Bz-0*Bh),[5 1e3],'k')
    %contour(IIpz,1:Nt,1e3*(Bz-0*Bh),[-1.5 1e3],'c')
    for k=1:8
        plot(1:Np,Poin(k,:),[col(k) '*'])
        plot(1:Np,Poin(k,:),[col(k) '-'])
    end

    xlim([1 Np])
    ylim(ylims)
    caxis(clims)
    plot([aP aP],ylims,'--k')


    h2a=subplot(4,14,[43 52]);
    plot(1:Np,actP,'b')
    axis([1 (Np+1) 0 1])
    text(5,0.7,'Select point')


    h2b=subplot(4,14,[53 56]);
    plot((1:10)+200,actL,'b')

    hold on
    axis([[100.5 110.5]+100 0 1])
    for k=1:8
        plot(100+[99.5 100.5]+k,[0 0]+0.9,col(k),'linewidth',2)
    end
    text(101+100,0.7,'Select line')
    text(109+100,0.2,'Lower Ylim','rotation',90)
    text(110+100,0.2,'Upper Ylim','rotation',90)



    h3=subplot(4,14,[11 39]);
    axis([-200 -199 0 2])
    set(h3,'xtick',[1000])
    set(h3,'ytick',[1000])

    text(-199.5,0.7,'move down','rotation',90)

    h4=subplot(4,14,[12 40]);
    axis([-199 -198 0 2])
    set(h4,'xtick',[1000])
    set(h4,'ytick',[0 1 2])
    set(h4,'yticklabels',['  1';' 10';'100'])
    text(-198.5,0.7,'move up','rotation',90)


    h5=subplot(4,14,[13 42]);
    set(h5,'xtick',[1000])
    set(h5,'ytick',[1000])
    plot([201 400]+100,[1 1],'k')
    hold on
    plot([201 400]+100,[2 2],'k')
    plot([201 400]+100,[3 3],'k')
    plot([201 400]+100,[4 4],'k')
    plot([201 400]+100,[5 5],'k')
    plot([201 400]+100,[6 6],'k')
    plot([301 301]+100,[0 5],'k')
    axis([[201 400]+100 0 7])


    text(240+100,2.3,'B_z','rotation',90)
    text(340+100,2.3,'dB_z/dt','rotation',90)
    text(240+100,1.3,'B_r','rotation',90)
    text(340+100,1.3,'dB_r/dt','rotation',90)
    text(240+100,0.3,'B_{\phi}','rotation',90)
    text(340+100,0.3,'dB_{\phi}/dt','rotation',90)
    text(250+100,6.4,'STOP')
    text(250+100,5.4,'reset')
    text(210+100,4.4,'copy')
    text(310+100,4.4,'smooth')
    text(210+100,3.4,'fit')
    text(310+100,3.4,'apply')


    % saved points are used if UseOldPoints==1 
    if UseOldPoints==0
        A=ginput(1);
        xin=A(1);
        yin=A(2);
    
    else
        xin=401;
        yin=3.1;
        KeepGoing=0;
    end
    
    if xin>400
        if yin>6
            KeepGoing=0;
        elseif and(yin>3,aL>8)
        axes(h2b)
        text(101,0.4,'Select a line','color','r')
        pause(0.5)
        elseif yin>5
            Poin(aL,:)=PoinBeg;
        elseif yin>4
            PoiL=Poin(aL,:);
            Poin(aL,:)=smoo(PoiL,3);
            Poin(aL,[1,end])=PoiL([1,end]);
        elseif yin>3
    %         PoiL=Poin(aL,:);
    %         II=find(PoiL~=PoinBeg);
    %         if length(II)>1
    %             III=II;
    %             III=[1-1e-3 II Np+1e-3];
    %             PoiLL=[PoiL(II(1)) PoiL(II) PoiL(II(end))];
    %             PoiL = interp1(III,PoiLL,1:Np);
    %             Poin(aL,:)=PoiL;
    %        end
            InterBlines

        elseif yin>2
            WhatDat=4;
        elseif yin>1
            WhatDat=5;
        elseif yin>0
            WhatDat=6;
        end
    elseif xin>300
        if yin>6
            KeepGoing=0;
        elseif and(yin>3,aL>8)
        axes(h2b)
        text(101,0.4,'Select a line','color','r')
        pause(0.5)
        elseif yin>5
            Poin(aL,:)=PoinBeg;
        elseif yin>4
            aL=aL+1;
            Poin(aL,:)=Poin(aL-1,:);

        elseif yin>3
            PoiL=Poin(aL,:);
            II=find(PoiL~=PoinBeg);
            if length(II)>1
                III=II;
                III=[1-1e-3 II Np+1e-3];
                PoiLL=[PoiL(II(1)) PoiL(II) PoiL(II(end))];
                PoiL = interp1(III,PoiLL,1:Np);
                Poin(aL,:)=PoiL;
            end
        elseif yin>2
            WhatDat=1;
        elseif yin>1
            WhatDat=2;
        elseif yin>0
            WhatDat=3;
        end
    elseif xin>200
        aL=round(xin-200);
    elseif xin>0
        aP=round(xin);
        if aP>Np
            aP=Np;
        end
        if yin>PoinBeg;
            Poin(aL,aP)= yin;
        end
    elseif xin>-199
        IIs=10^yin;
        if aL==10
            ylims(2)=ylims(2)+IIs;
        elseif aL==9
            ylims(1)=ylims(1)+IIs;
        else
        Poin(aL,:)= Poin(aL,:)+IIs;
        end
    elseif xin>-200
        IIs=-10^yin;
        if aL==10
            ylims(2)=ylims(2)+IIs;
        elseif aL==9
            ylims(1)=ylims(1)+IIs;
        else
        Poin(aL,:)= Poin(aL,:)+IIs;
        end
    end

end










