mPoinU=mean(PoinU');
II=find(abs(mPoinU-PoinBeg)>5); % these are the lines that have been filled plus one at the beg and one at the end
Nlines=length(II);
PoinU=PoinU(II,:);
for k=1:Np
    PoinU(:,k)=sort(PoinU(:,k)); % making sure that no lines are crossing
end



% figure(92),clf
% subplot(2,3,1)
% plot(Poin')

Y=repmat(0,[Nt,Np]);
tu=linspace(1,Nt,Ny);
yu=linspace(1,Nlines,Ny);
Npi=(Np-1)*4+1; % number of interpolated probe locations
Nti=4*Nt-1; % number if interpolated time-points
Yi=repmat(0,[Nti,Npi]); %y value at interpolated locations
ti=linspace(1,Nt,Nti);
try
for k=1:Np
    Y(:,k)=interp1(PoinU(:,k),1:Nlines,1:Nt);
end
    

for k=1:(Np-1)
    for kk=0:3
    Yi(:,4*(k-1)+kk+1)=interp1(((4-kk)*PoinU(:,k)+kk*PoinU(:,k+1))/4,1:Nlines,ti);
    end
end
    Yi(:,Npi)=interp1(PoinU(:,Np),1:Nlines,ti);

IIi=linspace(1,Np,Npi);

catch 
    k
    PoinU(:,k)
    'something is wrong'
end

