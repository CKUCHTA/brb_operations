
%shot=56487;
function [fig2] = linPlot(shot) 
for shot=shot
% for shot=58081
II=64001:95500;
 [Bz,dBz,Br,dBr,Bp,dBp, zAndp, tpoints, Bh] = loadBlin(shot, II);
 

 Bz=Bz+Bh;
 
% AddiFacZ= Bz(4000,:)./smoo(Bz(4000,:),5)';
% 
% for k=1:24
%     Bz(:,k)=Bz(:,k)/AddiFacZ(k);
% end
 
%  ylims=[6.500e4 6.53e4];%TREX and CT
%  ylims=[1000 30825];
 zpoints = zAndp(2,:);
 ylims=[1000 1200];
 tlims = tpoints(ylims);
 II=ylims(1):ylims(2);
 tlim_a = tpoints(II);
 
 fig2 = figure(2);
 clf;

 subplot(3,6,6)
 % Bz Plots at Specific Probes
 plot( Bz(II,2)*1e3,tlim_a,'b')
 hold on
 plot( Bz(II,8)*1e3,tlim_a,'r')
 plot( Bz(II,15)*1e3,tlim_a,'g')
 plot( Bz(II,22)*1e3,tlim_a,'k')
%  plot(0*II,II,'--k')
 plot(0*tlim_a,tlim_a,'--k')
 ylim(tlims)
 xlabel('B_z[mT]')

 %pcolor Bz plot
 subplot(3,6,[1 5])
 pcolor(zpoints,tpoints,Bz*1e3),shading interp,colorbar
 ylim(tlims)
 hold on
 title(['Linear Probe B_z[mT], shot #' num2str(shot)] )
 plot([zAndp(2,2) zAndp(2,2)],tlims,'-b')
 plot([zAndp(2,8) zAndp(2,8)],tlims,'-r')
 plot([zAndp(2,15) zAndp(2,15)],tlims,'-g')
 plot([zAndp(2,22) zAndp(2,22)],tlims,'-k')
 xlabel('z[cm]')
 ylabel('t[\mus]')
 caxis([-25 25])
 
 
 %dBz plot at specific z value
  subplot(3,6,12)
 plot(dBz(II,8),tlim_a, '-r')
 hold on
 ylim(tlims)
 xline(0,'--k')
 xlabel('dB_z/dt')

 %pcolor dBz plot
 subplot(3,6,[7 11])
 pcolor(zpoints,tpoints,dBz),shading interp,colorbar
 ylim(tlims)
caxis([-1 1]*1e4)
%   caxis([-1 1]*2e4)
 hold on
 plot([zAndp(2,8) zAndp(2,8)],tlims,'-r')
  xlabel('z[cm]')
 ylabel('t[\mus]')
 title('Linear Probe dB_z/dt')

 subplot(3,6,18)
 plot(dBp(II,8),tlim_a, '-r')
 hold on
 ylim(tlims)
 xline(0,'--k')
xlabel('dB_\phi/dt')

 %pcolor dBp plot
 subplot(3,6,[13 17])
 pcolor(zpoints,tpoints,dBp),shading interp,colorbar
 ylim(tlims)
caxis([-1 1]*1e4)
 hold on
 plot([zAndp(2,8) zAndp(2,8)],tlims,'-r')
  xlabel('z[cm]')
 ylabel('t[\mus]')
 title('Linear Probe dB_\phi/dt')

%  set(gcf,'Position',[-2500 200 800 1200]) 
%  pause(0.5)
end   