
function [Bz,dBz,Br,dBr,Bp,dBp, zAndp, tpoints,Bh] = loadBlin(shot, II)

AllBz = 0;

[daq, Bh] = loadDtaqData(shot); % load data from harddrive or from mdsplus
%ylims=[950  1650];
%ylims=[1000  1650];

IIm=901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIm=1:length(II);

%II = 1:length(daq.a116(:,1:12));

dt=1e-7; % time between samples
IIoff=1000;  % time where we force B-components to zero (before the reconnection pulse)

facZ=3.25e4; % calibration factors
facR=facZ;
facP=facZ;
% Vlayer=50e3;
% facJ=1/1.2e-6/Vlayer/1e3; % mult into dB/dt to get current in kA/m^2


if AllBz==1
% define data matrices
dBz1=repmat(0,[length(II) 24]);
dBz2=repmat(0,[length(II) 24]);
dBz=repmat(0,[length(II) 48]);

% fill linear probe data
dBz1(:,1:2:23)=daq.a116(II,1:12);
dBz1(:,1:2:23)=daq.a116(II,1:12);
dBz1(:,2:2:24)=daq.a128(II,(1:12)+4);
dBz1(:,15)=dBz1(:,15)*0;
dBz2(:,1:2:23)=daq.a129(II,(1:12)+8);
dBz2(:,2:2:24)=daq.a129(II,(1:12)+20);

dBz(:,1:2:47)=dBz1;
dBz(:,2:2:48)=-dBz2;

else 
    dBz=repmat(0,[length(II) 24]);
    dBz(:,1:2:23)=daq.a116(II,1:12);
    dBz(:,1:2:23)=daq.a116(II,1:12);
    dBz(:,2:2:24)=daq.a128(II,(1:12)+4);
    dBz(:,15)=dBz(:,15)*0;
end
Nbz=size(dBz,2);

dBz=-dBz*facZ;


for k=1:Nbz
    dBz(:,k)=dBz(:,k)-mean(dBz(IIm,k));
end

%clean up bad channels
if AllBz
dBz(:,18)=(dBz(:,17)+dBz(:,19))/2;
dBz(:,29)=(2*dBz(:,28)+dBz(:,31))/3;
dBz(:,30)=(dBz(:,28)+2*dBz(:,31))/3;
dBz(:,32)=(dBz(:,31)+dBz(:,33))/2;
dBzN= (dBz(:,7)-(dBz(:,6)+dBz(:,8))/2 + dBz(:,11)-(dBz(:,10)+dBz(:,12))/2)/2;
dBz(:,7)=dBz(:,7)-dBzN;
dBz(:,11)=dBz(:,11)-dBzN;
dBz(1:1140,9)=(dBz(1:1140,8)+dBz(1:1140,10))/2;
dBz(1:1140,24)=(dBz(1:1140,23)+dBz(1:1140,25))/2;
else
 dBz(:,15)=(dBz(:,14)+dBz(:,16))/2;
 dBzN= (dBz(:,4)-(dBz(:,3)+dBz(:,3))/2 + dBz(:,6)-(dBz(:,7)+dBz(:,7))/2)/2;
 dBz(:,4)=dBz(:,4)-dBzN;
 dBz(:,6)=dBz(:,6)-dBzN;
end


dBr=repmat(0,[length(II) 24]);
dBr(:,1:2:23)=daq.a116(II,(1:12)+12);
dBr(:,2:2:24)=daq.a128(II,(1:12)+16);
for k=1:24
    dBr(:,k)=dBr(:,k)-mean(dBr(IIm,k));
end

dBr=dBr*facR;

dBp=repmat(0,[length(II) 24]);
dBp(:,1:2:15)=daq.a116(II,(1:8)+24);
dBp(:,17:2:23)=daq.a128(II,(1:4));
dBp(:,2:2:8)=daq.a128(II,(29:32));
if AllBz
  dBp(:,10:2:24)=daq.a129(II,(1:8));
else
  dBp(:,10:2:24)= (dBp(:,9:2:23)+dBp(:,[11:2:23 23] ))/2;
end

for k=1:24
    dBp(:,k)=dBp(:,k)-mean(dBp(IIm,k));
end

dBp(:,16)=(dBp(:,15)+dBp(:,17))/2;

dBp(1:1140,7)=(dBp(1:1140,6)+dBp(1:1140,8))/2;
dBp(:,3)=(dBp(:,2)+dBp(:,4))/2;
dBp=dBp*facP;



Bz=cumsum(dBz)*dt;
Br=cumsum(dBr)*dt;
Bp=cumsum(dBp)*dt;

% for k=1:48
%     Bz(:,k)=Bz(:,k)-Bz(IIoff,k);
% end
for k=1:Nbz
    Bz(:,k)=Bz(:,k)-Bz(IIoff,k);
end
for k=1:24
    Br(:,k)=Br(:,k)-Br(IIoff,k);
    Bp(:,k)=Bp(:,k)-Bp(IIoff,k);
end
%Bz=Bz+Bh;



%eliminate some systematic noise on B
sBz=smoo(Bz',4)';
ssBz=Bz-sBz;
ssBz=smoo(ssBz,75);
Bz=Bz-ssBz;
sBr=smoo(Br',4)';
ssBr=Br-sBr;
ssBr=smoo(ssBr,75);
Br=Br-ssBr;
sBp=smoo(Bp',4)';
ssBp=Bp-sBp;
ssBp=smoo(ssBp,75);
Bp=Bp-ssBp;

sdBz=smoo(dBz',4)';
ssdBz=dBz-sdBz;
ssdBz=smoo(ssdBz,75);
dBz=dBz-ssdBz;
sdBr=smoo(dBr',4)';
ssdBr=dBr-sdBr;
ssdBr=smoo(ssdBr,75);
dBr=dBr-ssdBr;
sdBp=smoo(dBp',4)';
ssdBp=dBp-sdBp;
ssdBp=smoo(ssdBp,75);
dBp=dBp-ssdBp;

% calculate distance and time axis
tpoints = (1:length(Bz))*dt*1e6; % time array in ms
if shot < 51349
    zpoints = linspace(100,-20,24);
else
    zpoints = linspace(33.3,33.3-121.92,24);   
end
zAndp = cat(1, 1:24, zpoints); % probe number and z positions
end

