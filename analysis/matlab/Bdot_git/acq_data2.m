%Grab data from MDSPlus without saving

function [daq, AllBz, time, Bh] = acq_data2(shotnum)
% clear
% shotnum = 60363;

AllBz = 0;
%mdsconnect('192.168.113.62');
mdsconnect('skywalker.physics.wisc.edu');
mdsopen('wipal', shotnum)

daq=struct();
time=struct();
Bh_Tesla = mdsvalue('.RAW.A373_DIRECT:CH_43');% Helmholtz in T
Bh = mean((Bh_Tesla)*200*0.3521*1e-4);% Helmholtz in mT
sprintf('Bh = %0.4f', Bh)

if shotnum < 58831 & shotnum > 53416
    all_serials = [113 114 115 116 128 129 320];
elseif shotnum < 53416
    all_serials = [113 114 115 116 128 129];
else
    all_serials = [113 114 115 116 128 129 320 323 324 325 326];
end

for serial = all_serials
    for ch = 1:32
        daq.(['a' num2str(serial)])(:,ch) = mdsvalue(['\ta' num2str(serial) '_' num2str(ch,'%02d')]);
        time.(['a' num2str(serial)]) = mdsvalue(['\ta' num2str(serial) '_time']);
    end
end


mdsclose;
mdsdisconnect;

% example daq.([a113])(:,1) = mdsvalue(['\ta113_01']);
%output = mdsvalue('\hook_bdot1_r')