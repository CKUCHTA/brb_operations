% Calculates the correction factor for the flux array. The
% calibration factor is determine by finding the difference between the
% B-field before reconnection and after reconnection.  This difference is
% then corrected using vacuum plots and the ration of the corrected
% difference to the original difference is plotted.  There should be no
% more than a 10 percent change between probes, which can be confirmed in
% the fac plot.

clear
% shotlist = 62259:62264;
shotlist = 64982;
% shotlist = 62609:62614;

SamRange=69000:73000;
IIm=901:1001; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
dt=1e-7; % time between samples
IIoff=1000; %Makes Bz=Bh at this index

PlotCalibrated = 0;



facTot = struct(); 
kk = 0;
for shot = shotlist
    kk = kk+1;
    [daq, Bh] = loadDtaqDataFarray(shot, 1, SamRange); % load dtac from harddrive or from mdsplus
    [dBSetRaw, SampleRange, Bh, tpoints] = loadFarray(shot, daq, Bh, dt);
    [BSet, dBSet, Bh, ProbePos, tpoints] = Calibrate1DFluxArray(IIoff, IIm, dt, dBSetRaw, Bh, tpoints, PlotCalibrated);
    % [BSet, dBSet, ProbePos , SampleRange, Bh, tpoints] = loadFarray(shot, daq, Bh, SamRange, dt, IIm, IIoff);
    
    dBSetR = zeros(size(dBSet{1,1},1),8,size(dBSet{1,1},2));
    dBSetP = zeros(size(dBSet{1,2},1),8,size(dBSet{1,2},2));
    dBSetZ = zeros(size(dBSet{1,3},1),8,size(dBSet{1,3},2));

    BSetR = zeros(size(dBSet{1,1},1),8,size(dBSet{1,1},2));
    BSetP = zeros(size(dBSet{1,2},1),8,size(dBSet{1,2},2));
    BSetZ = zeros(size(dBSet{1,3},1),8,size(dBSet{1,3},2));
    for j = 1:length(dBSet)
        dBSetR(:,j,:) = dBSet{j,1};
        dBSetP(:,j,:) = dBSet{j,2};
        dBSetZ(:,j,:) = dBSet{j,3};

        BSetR(:,j,:) = BSet{j,1};
        BSetP(:,j,:) = BSet{j,2};
        BSetZ(:,j,:) = BSet{j,3};
    end
    
    % DelB should be a smooth field and it should not go through zero when HH
    % field is not added to B. 
    DelB=0;
    % t_initial = 1100;
    % t_final = 1395;
    % t_initial = 1130;
    % t_final = 1395;

    t_initial = 1840;
    % t_final = 1890;
    t_final = 1970;
    
    DelB=DelB+squeeze(BSetZ(t_initial,:,:)-BSetZ(t_final,:,:)); % Change of Bz in the time before drive coils are on and when 
                                                  % the reconneciton starts.  
    
    DelB(7,7)=(DelB(7,8)+DelB(7,6))/2; 
    DelB(7,7)=(DelB(7,8)+DelB(7,6))/2;
    % DelB(8,5) = (DelB(8,6)+DelB(8,4))/2;
    DelB(4,8) = (DelB(4,9)+DelB(4,7))/2;
    % DelB(6,11) = (DelB(6,10)+DelB(6,12))/2; 
    % DelB(6,10) = (DelB(6,9)+DelB(6,11))/2; 
    DelBc=DelB; % DelB corrected. More bad probe corrections. Not bad enough to turn off. (Indexes are reversed from loadFarray)
    
    % smooth Corrected DelB
    DelBc=smoo(smoo(DelBc',2)',2); % 2 is number of points smoothing over, each transpose is a smooth over each direction
    
    facShot=DelBc./DelB; % calibration factor from difference between time before event and after event
    
    facTot(kk).shot = shot;
    facTot(kk).facShot = facShot;
end                                                  

%%
% find calibration factor by averaging over each shot calibration factor
for i = 1:length(facTot)
    if i == 1
        SumFac = facTot(i).facShot;
    else
        SumFac = SumFac + facTot(i).facShot;
    end
end
CalfacZ = SumFac/length(facTot);

figure(106),clf
subplot(1,3,1)
pcolor(DelB),shading interp
colorbar
% caxis([0 0.056])
title('DelB')

subplot(1,3,2)
pcolor(DelBc),shading interp
colorbar
% caxis([0 0.056])
title('DelB Corrected')


subplot(1,3,3)
pcolor(CalfacZ), shading interp
colorbar
% clim([0.9 1.1])
title('Correction Factor')


facZ_FromDifference4 = CalfacZ;

FluxArrayStruct = what('FluxArray_git');
FluxArrayPath = FluxArrayStruct.path;
save(fullfile(FluxArrayPath,'facZ1DArray4.mat'), 'facZ_FromDifference4')
