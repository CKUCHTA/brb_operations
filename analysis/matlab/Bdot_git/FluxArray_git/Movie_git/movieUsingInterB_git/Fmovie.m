% Generates jpeg frames for movies based on InterpByEye points (Jan's
% LookAtShots script).

clear
DataStruct = what('DipoleCusp2024');
DataFolder = DataStruct.path;
% DataFolder = '/Users/paulgradney/Research/Data/';

%%%% Inputs for each shot %%%%%%
Configuration = 1; % 1 for TREX, 2 for Jets
saveMovie = 1; % 1 to save

Z0=-0.20; % stick 1 position

if Configuration == 1
    % ShotFolder = 'FluxArray_Calibration';
    ShotFolder = '1DFarray';
    shot = 63271;
    % LowerTime = 3300; % sample time less than lowest line made in InterpByEye (Lower limit of interpolation)
    % UpperTime = 6200; % sample time greater than highest line made in InerpByEye (Upper limit of interpolation)
    % timeIndex = 4220:2:5800; % time index range of movie
    LowerTime = 7200; % sample time less than lowest line made in InterpByEye (Lower limit of interpolation)
    UpperTime = 8800; % sample time greater than highest line made in InerpByEye (Upper limit of interpolation)
    timeIndex = 7200:2:8400; % time index range of movie
    Bclims = [-10 10];
    dBclims = [-60 60];
 
elseif Configuration == 2 
    shot = 61927;
    ShotFolder = 'Jets';
    LowerTime = 400;
    UpperTime = 2400;
    timeIndex=800:2:2000; % time index range of movie
    Bclims = [-3 3];
    dBclims = [-300 300];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FullShotFolder = fullfile(DataFolder, ShotFolder);

JPEGFolder = fullfile(FullShotFolder, 'Plots/Movies_withJph/', sprintf('PNG_%d', shot));
mkdir(JPEGFolder)

%Load data calculated in InterpByEye

load(fullfile(FullShotFolder,sprintf('Matlab_Data/InterpData/%d_IntBdata',shot)), 'dBzIntTot2', 'BzIntTot2', 'Bh')
load(fullfile(FullShotFolder,sprintf('Matlab_Data/PointsUsed/%d_PointsUsed',shot)), 'PoinUse', 'PoinUse3')

Nrow=12;  %number of positions on each flux array stick
Nz = 29;  %number of probe positions after interpolating by 4: that is 29 = (8-1)*4+1 (interpolated in r between the 8 existing values)

% determine the number of lines made from dolinesFarry
Poin=PoinUse3{1};
II=find(Poin(:,1)> Poin(end,1));

Npoin=length(II); % number of lines
Zvec=linspace(0,7*0.12,Nz)+Z0; % z values based on stick 1 position

%%
% adding a line above and below the lines from dolinesFarray
PoinTot=zeros([Nrow,Npoin+2,Nz]);
for ll=1:Nrow
    Poin=PoinUse3{ll};
    PoinTot(ll,2:(Npoin+1),:)= Poin(1:Npoin,:);
end

% Where the interpolation starts. The interpolation is 4 time the original sample rate 
PoinTot(:,1,:)=LowerTime; % below the lines 
PoinTot(:,Npoin+2,:)=UpperTime; % above the lines

IIt=timeIndex; % time index movie data

IIr=1:((size(PoinTot,1)-1)*8+1); % number of interpolation going to do
[TindexRZ,TindexPos,Tpos] = Tindexing(PoinTot,IIt,IIr); %implements jogging method
Nr=length(IIr); %number of r position after interpolating up by 8

%%
% array that defines how to fade between r postions as time evolves
alpv2=[(1:9)*0 0:8 8:(-1):0 (1:9)*0]/8;
alpv=reshape(alpv2,[9,4]);
BzMovie=zeros(size(TindexRZ));
dBzMovie=zeros(size(TindexRZ));


% creating the movie depending on the 12 position in R
for ll=1:(Nrow-1)  
    % determining what row to use from the R position
    for cnt=2:3
        llu=ll-2+cnt;
        if llu<1
            llu=1;
        elseif llu> Nrow
            llu=Nrow;
        end
        alp=alpv(:,5-cnt); % picks position from alphv to fade from one probe to the next depending on where you are
        [Bz1,dBz1] = loadMovieData(BzIntTot2,dBzIntTot2,llu);
        for k=1:Nz
            addOn= ll==(Nrow-1); % to fill in last row
            for rr=1:(8+addOn)
                IIr=(ll-1)*8+rr; %index of r-position being filled 
                IItu=interp1(squeeze(TindexPos(llu,:,k)),Tpos,squeeze(TindexRZ(IIr,:,k))); %time index in raw data
               
                % Interpolated fields using the jogging method (fading from one probe to the next based on alp) 
                BzMovie(IIr,:,k)=BzMovie(IIr,:,k)+alp(rr)*interp1(1:size(Bz1,1), Bz1(:,k),IItu); 
                dBzMovie(IIr,:,k)=dBzMovie(IIr,:,k)+alp(rr)*interp1(1:size(Bz1,1), dBz1(:,k),IItu);
             end
        end
    end
end
        
%%

Nr0=70; % index to move the r=0 point. 
Lprobe = 11*(0.0254*2); % length of each probe stick in cm
Rvec=linspace(Lprobe,0.001,  Nr);
Rvec=Rvec-Rvec(Nr0); % accounts for probes below R = 0
[Rmat,Zmat]=meshgrid(Rvec,Zvec); % create an R and Z matrix to circumvent using for loops

% Load constant fields for specific configuration
if Configuration == 1
    HHfieldPath = which('HHfield.mat');
    % load '/Users/paulgradney/Research/Matlab Drive Cylinder/Bdot/FluxArray/Movie/movie using InterB/HHfield.mat' %BzHnorm ZH RH
    load(HHfieldPath)
    RH(1)=0;
    BHint= interp2(ZH,RH,BzHnorm,Zmat(:),abs(Rmat(:))); % meshgrid values allow all combination of R and Z
    BHint=reshape(BHint,size(Rmat))*Bh*0.9; % Reshapes BHint into a 2D array. Multiply by Bh to normalize and 0.9 is a correction factor

elseif Configuration == 2
    load('/Users/paulgradney/Research/Matlab Drive Cylinder/Bdot/FluxArray/Movie/movie using InterB/BiasCoils.mat','BzBias', 'Z', 'R')
    Rm = R; Zm = Z;
    Rm(1)=0;
    B_Biasint= interp2(Zm,Rm,BzBias,Zmat(:),abs(Rmat(:)));
    B_Biasint=reshape(B_Biasint,size(Rmat))*Bh*0.9;
end


% looping through all of R and Z to add BHint for all time
for k=1:Nz
    for IIr=1:Nr
        if Configuration == 1
            BzMovie(IIr,:,k)=BzMovie(IIr,:,k)+BHint(k,IIr); % (R, t, Z) adding Helmholtz field to all time points
        elseif Configuration == 2
            BzMovie(IIr,:,k)=BzMovie(IIr,:,k)+B_Biasint(k,IIr);
        end
    end
end


% Plasma Current Calculaiton
mu0 = 4*pi*1e-7;
Rmatu = sqrt(Rmat.^2 + 0.02^2)';% Add offset to reduce the effect of dividing by zero
PsiMovie=BzMovie*0;
JphiMovie=BzMovie*0;
for iit=1:size(BzMovie,2)

    dat1=squeeze(BzMovie(:,iit,:));
    dat1=smoo(dat1',2)';
    Psi=dat1.*Rmatu;
    Psi=cumsum(Psi,1)*(Rvec(2)-Rvec(1));
    Psi=Psi-Psi(Nr0,:);
    PsiMovie(:,iit,:)=Psi;
    
     % calculate Current with Grad-Shafranov
     % (see https://en.wikipedia.org/wiki/Grad%E2%80%93Shafranov_equation)
    [dPsidr,dPsidz] = gradient(Psi', Rvec, Zvec);
    dPsidr = dPsidr';
    dPsidz = dPsidz';
    [~,dPsidzz] = gradient(dPsidz', Rvec, Zvec);
    dPsidzz = dPsidzz';
    [dPsidrr,~] = gradient(dPsidr'./Rmatu', Rvec, Zvec);
    dPsidrr = dPsidrr';
    Jphi = dPsidrr + dPsidzz./Rmatu;
    Jphi = Jphi.*sign(Rmat');
    JphiMovie(:,iit,:) =Jphi/mu0;
end


% Eliminating Noise in Jphi
JphiMovieSm=JphiMovie*0;
NsmoZ=8; %smoothing in Z 
NsmoT=200; % smoothing in T
for k=1:size(JphiMovie,1)
    dum=smoo(squeeze(JphiMovie(k,:,:)),NsmoT); %low pass filter in time
    JphiMovieSm(k,:,:)= dum-smoo(dum',NsmoZ)'; % high pass filter in z
end
JphiMovie=JphiMovie-JphiMovieSm; % Subtract of noise from Jphi


%Calculate current from Layer Velocity
Vlayer=100e3;
facJ=1/1.2e-6/Vlayer/1e3; % mult into dB/dt to get current in kA/m^2

%%
%%%% Begin Plotting Each Frame %%%%%
xpos=[0.11 0.54];
ypos=[0.7 0.39 0.08];
dx=0.43*2;
dy=0.22;

%%
frames = 0;
%for iit=51:2:251
% for iit=51:2:251
% for iit=1:1:1
%for iit=619:3:641
% for iit=1:3:580 % TREX movies
% for iit=580:3:630
for iit = 100
% for iit=150:3:250
    frames = frames +1;
    moviePlot = figure(1);
    clf
   

    H(1) = subplot('position',[xpos(1) ypos(1) dx dy]); % Bz Plot
    H(2) = subplot('position',[xpos(1) ypos(2) dx dy]); % dBz Plot
    H(3) = subplot('position',[xpos(1) ypos(3) dx dy]); % Jphi Plot
    
    colormap('jet') % hsv also works well 
    
    %%%%%%%%% B Plot %%%%%%%%%%%%%%%%%%%%%%
    axes(H(1))
 
    BzFrame = squeeze(BzMovie(:,iit,:)); %(R, t, Z), squeezing it in time to get one frame in time
    
    % Calcluate Flux
    % BzFrame=smoo(BzFrame',2)'; % Do Not Smooth when using Grad-Shafranov
    Psi = BzFrame.*Rmat'; % use the meshgrid output to illiminate for loop
    Psi = cumsum(Psi,1)*(Rvec(2)-Rvec(1));
    Psi = Psi-Psi(Nr0,:); % account for probes below zero

   
    % Define Psi Contour lines to plot
    PsiV1=(linspace(-1,1,31)-1/100)*2e-3;
    PsiV2=(linspace(0,1,3)+0.01)*0.7e-4;
    PsiV3=linspace(-12,12,61)*1e-4;
    PsiV4=linspace(-0.4,0.4,6)*1e-4;
    
    %%%% Plot Bz %%%%%%
    pcolor(Zvec,Rvec,-BzFrame*1e3)
    hold on
    lw=1.15;
    contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV3(2:2:end),'k','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
    
    TimeMuSec=IIt(iit)/40 -4; % Calculate time of frame
    % text( 0.05+Z0,0.48, [sprintf('shot # %d, t = %0.2f\\mus, iit = %d', shot, TimeMuSec, iit)], 'fontsize', 13)
    text( 0.05+Z0,0.48, [sprintf('shot # %d, t = %0.2f\\mus', shot, TimeMuSec)], 'fontsize', 13)
    
    % Set Tick Spacing
    h=gca;
    set(h,'xtick',0:0.2:1)
    set(h,'ytick',-0.8:0.2:1)
    
    xlabel('Z [m]')
    text(-0.05+Z0,0.05,'R [m]','rotation',90)
    text( 0.82+Z0,0.48, 'B_z [mT]')
    
    shading interp
    clim(Bclims)
    h=colorbar;
    % Account for different configurations 
    if Bclims(2)>5
        set(h,'ytick',-10:5:10) % set Colorbar tick spacing
    else
        set(h,'ytick',-10:1:10) % set Colorbar tick spacing
    end
    pbaspect([84 60 1])
    %%%%%%%%%%%%%%% Grad-Shafranov Jphi Plot %%%%%%%%%%%%%%%%%%%%
    axes(H(2))
    
    Jphi=squeeze(JphiMovie(:,iit,:));
    pcolor(Zvec,Rvec,Jphi*1e-3) % plot based on Grad-Shafranov
    hold on
    shading interp
    h=gca;
    % contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV3(2:2:end),'k','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)

    set(h,'xtick',0:0.2:1)
    set(h,'ytick',-0.8:0.2:1)
    xlabel('Z [m]')
    text(-0.05+Z0,0.05,'R [m]','rotation',90)
    text( 0.65+Z0,0.48, 'J_{\phi} [kA/m^2] from \Psi')
    % title('$J_{\phi} = \frac{\partial}{\partial r}(\frac{1}{r}\frac{\partial\psi}{\partial r}) + \frac{1}{r}\frac{\partial^2\psi}{\partial z^2}$', 'fontsize',13 ,'interpreter', 'latex')

    shading interp
    h=colorbar;
    if Configuration == 1
        Jclims=[-100 100]*(0.3+ 3*iit/600);
        clim(Jclims)
    elseif Configuration == 2
        % clim(dBclims)
        % set(h,'ytick',-300:100:300)
        clim([-40 40])
        set(h,'ytick',-40:20:40)
    end
    pbaspect([84 60 1])
    

    %%%%%%%%%%%%%%%% dBz Plot %%%%%%%%%%%%%%%%%%%%
    axes(H(3))
  
    dBzFrame=squeeze(dBzMovie(:,iit,:));
    
    if Configuration == 1
        pcolor(Zvec,Rvec,dBzFrame*facJ) % plot based on Vlayer
    elseif Configuration == 2
        pcolor(Zvec,Rvec,dBzFrame) % plot based on Vlayer
    end

    hold on
    % shading interp
    h=gca;
    % contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV3(2:2:end),'k','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)

    set(h,'xtick',0:0.2:1)
    set(h,'ytick',-0.8:0.2:1)
    xlabel('Z [m]')
    text(-0.05+Z0,0.05,'R [m]','rotation',90)

    if Configuration == 1
        text( 0.62+Z0,0.48, '-dB_{z}/dt (J_{\phi} [kA/m^2]))')
    elseif Configuration == 2
        text( 0.75+Z0,0.48, '-dB_{z}/dt [T/s))')
    end

    shading interp
    
    clim(dBclims)
    h2=colorbar;
    % Account for different configurations 
    if dBclims(2)<80
        set(h2,'ytick',-80:40:80)
    else
        set(h2,'ytick',-300:100:300)
    end
    
    pbaspect([84 60 1])
    fontsize(moviePlot, 14, "points")
%%%%%%%%%%%%%%%%%%% Save Frame %%%%%%%%%%%%%%%%%%%%%
    PNGFile = fullfile(JPEGFolder,sprintf('%d_Frame%d.png', shot, frames));
    if saveMovie == 1
        print('-f1', PNGFile, '-dpng', '-r400')
    else
        pause(0.5)
    end
    
    
end
