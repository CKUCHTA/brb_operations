function [TindexRZ,TindexPos,Tpos] = Tindexing(PoinTot,IIt,IIr)

SizeMat= [length(IIr), length(IIt), size(PoinTot,3)] ;
SizeMat2= [length(IIr), size(PoinTot,2), size(PoinTot,3)] ;



Tpos=PoinTot(1,1,1):PoinTot(1,end,1); %times considered in the raw data
TindexPos=repmat(0,[size(PoinTot,1), length(Tpos), size(PoinTot,3)]); %this will be the time index for each of the SH positions
for k=1:size(PoinTot,1)
    for l=1:size(PoinTot,3)
        TindexPos(k,:,l)=interp1(squeeze(PoinTot(k,:,l)),1:size(PoinTot,2),Tpos);
    end
end



IIri=linspace(1,size(PoinTot,1),length(IIr));
PoinTotMoreR=repmat(0,SizeMat2);
for k=1:size(PoinTotMoreR,2)
    for l=1:size(PoinTotMoreR,3)
        PoinTotMoreR(:,k,l)=interp1(1:size(PoinTot,1),squeeze(PoinTot(:,k,l)),IIri);
      %  PoinTotMoreR(:,k,l)=smoo(PoinTotMoreR(:,k,l),16);  % Nr is multiplied by 8, so this averages the curves over 2 positions
        PoinTotMoreR(:,k,l)=smoo(PoinTotMoreR(:,k,l),1);  % Nr is multiplied by 8, so this averages the curves over 1 positions
    end 
end


TindexRZ=repmat(0,SizeMat);

for k=1:size(PoinTotMoreR,1)
    for l=1:size(PoinTotMoreR,3)
        TindexRZ(k,:,l)=interp1(squeeze(PoinTotMoreR(k,:,l)),1:size(PoinTot,2),IIt);
    end
end

