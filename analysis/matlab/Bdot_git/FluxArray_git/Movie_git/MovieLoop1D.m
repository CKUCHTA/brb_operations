% This runs the 1DFarray files for creating movies from interpolated data
% using Jan's dolines scripts.  The files necessary for running this are:
%
% 1. InterpByEye3DFarrayZdir  .... This runs the GUI which chooses the points
%  for creating lines to interpolate along shared features.  The data is
%  interpolated up by 4.

% 2. DataMovie3DFarrayZ ... This takes the interpolated data from 
%  InterpByEye3DFarrayZdir and interpolates in time using the jogging
%  method.  This is done for R, Phi, and Z. This script is imbeded in
%  PlotMovie3DFarrayAll.

% 3. PlotMovie1DFarrayAll ... The fields and currents from 
% DataMovie#DFarray(component)are plotted at the specified TimeIndex.

% NOTE: If the B=0 points are not working change the sign of Bh in
% InterpByEye3DFarrayZdir and the > sign in line 118



clear
% shotlist = 62263:62269;
shotlist = 63271;
ProbeType = 1; % 1 for 1D flux array. 2 for 3D flux array
GUI_onOFF = 1; % 1 to turn GUI on
UseOldPoints = 1; % 1 for Using saved points
SkipThruGUI = 0; % 1 for auto clicking through GUI
CheckPlots_Zdir = 0; % 1 to plot Bz and dBz for each stick

Stick1Pos=-0.24; % stick 1 position
saveMovie = 0;
saveFrame = 0;
SaveData = 0; % 1 to save matlab data to harddrive

NumberOfProbesZ = 12;  %number of positions on each flux array stick. R and PHi have 10, Z has 12.
InterpNumberOfProbesZ = 29;  %number of probe positions after interpolating by 4: that is 29 = (8-1)*4+1 (interpolated in r between the 8 existing values


%%%%%%%%%%%%%%%%%%% TREX Coils Perameters %%%%%%%%%%%%%%%%%%%
% SamRange = 69000:71000;
% IIm = 901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% IIoff = 1000; %Makes Bz=Bh at this index
% facClim = 0.1; % multiplies range of clim in GUI
% dt=1e-7; % time between samples
% ylims = [1000, 1380]; % sample point range in speed probe configuration
% FirstPoint = 1050; % time fitlines begin
% LastPoint = 1350; % time fitlines end
% dBzLastPoint = 1200;
% Folder = '3DFluxArray_Calibration'; % Parent folder for data and plots

% TimeIndexRange = 290; % Time index when current sheet is ~25cm 
% % TimeIndexRange=1:3:580; % TREX movies
% 
% % Multiply ylims by 4 to get starting range of LowerTime and UpperTime
% LowerTime = 3300; % sample time less than lowest line made in InterpByEye (Lower limit of interpolation)
% UpperTime = 6200; % sample time greater than highest line made in InerpByEye (upper limit of interpolation)
% timeIndex = 4220:2:5800; % time index range of movie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%% Dipole with TREX Perameters %%%%%%%%%%%%%%%%%%%%%%%%%%
SamRange = 69000:73000;
IIm=901:980; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIoff=1020;
facClim = 0.1; % multiplies range of clim
dt=1e-7; % time between samples
ylims = [1800, 2200]; 
FirstPoint = ylims(1) + 50; % time fitlines begin
LastPoint = ylims(2)-50; % time fitlines end
dBzLastPoint = ylims(2)-100;
Folder= 'DipoleCusp2024';

TimeIndexRange = 370;
% TimeIndexRange =100:1:600; % dipole movies

% Multiply ylims by 4 to get starting range of LowerTime and UpperTime
LowerTime = 7000; % sample time less than lowest line made in InterpByEye (Lower limit of interpolation)
UpperTime = 8800; % sample time greater than highest line made in InerpByEye (upper limit of interpolation)
timeIndex = 7200:2:8400; % time index range of movie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Colorbar limits
BzClims = [-10 10]; % TREX Coils
% BzClims = [-5 5]; % vacuum shots
dBzClims = [-3 3]*1e3; % TREX Coils
% dBzClims = [-.7 .7]*1e3; %vacuum shots


for shot = shotlist
    InterpByEye3DFarrayZdir(shot, ProbeType, GUI_onOFF, SkipThruGUI, SaveData, UseOldPoints,...
        CheckPlots_Zdir, Folder, SamRange, IIm, IIoff, facClim, dt, ylims, FirstPoint,...
        LastPoint, dBzLastPoint);
    
    PlotMovie1DFarrayAll(shot,saveMovie, saveFrame, SaveData, Stick1Pos,...
        ProbeType, Folder, TimeIndexRange, LowerTime, UpperTime, timeIndex,...
        BzClims, dBzClims, NumberOfProbesZ, InterpNumberOfProbesZ)
end