% Generates jpeg frames for movies based on InterpByEye points (Jan's
% LookAtShots script).
% Updated 08Dec23

function  PlotMovie1DFarrayAll(shot,saveMovie, saveFrame, saveData, Stick1Pos,...
    ProbeType, Folder, TimeIndexRange, LowerTime, UpperTime, timeIndex,...
     BzClims, dBzClims, NumberOfProbesZ, InterpNumberOfProbesZ)
% clear
% shot = 63271;
% ProbeType = 1; % 1 for 1D flux array, 2 for 3D flux array
% saveData = 1; % 1 to save data output from DataMovie3DFaray functions
% Stick1Pos= -0.24; % stick 1 position in cm
% saveMovie = 0;
% saveFrame = 0;
% Folder = 'DipoleCusp2024';
% NumberOfProbesZ = 12;  %number of positions on each flux array stick. R and PHi have 10, Z has 12.
% InterpNumberOfProbesZ = 29;  %number of probe positions after interpolating by 4: that is 29 = (8-1)*4+1 (interpolated in r between the 8 existing values
% % Multiply ylims by 4 to get starting range of LowerTime and UpperTime
% LowerTime = 7200; % sample time less than lowest line made in InterpByEye (Lower limit of interpolation)
% UpperTime = 8800; % sample time greater than highest line made in InerpByEye (upper limit of interpolation)
% timeIndex = 7300:2:8600; % time index range of movie
% TimeIndexRange = 300;
% BzClims = [-10 10]; % TREX Coils
% % BzClims = [-5 5]; % vacuum shots
% dBzClims = [-3 3]*1e3; % TREX Coils
% dBzClims = [-.7 .7]*1e3; %vacuum shots

% RdataFile = fullfile(ScriptFolder,sprintf('%d_FluxData_RField',shot));
% PhidataFile = fullfile(ScriptFolder,sprintf('%d_FluxData_PhiField',shot));
% ZdataFile = fullfile(ScriptFolder,sprintf('%d_FluxData_ZField',shot));

% try
%     load(ZdataFile, 'BzMovie' ,'dBzMovie', 'PsiTot', 'JphiMovie', 'Zvec','Rvec', 'timeIndex','ShotFolder')
% catch
[BzMovie, dBzMovie, PsiTot, JphiMovie, Zvec, Rvec, FullShotFolder] = DataMovie3DFarrayZ(shot,...
    ProbeType, saveData, Stick1Pos, Folder, LowerTime,...
    UpperTime, timeIndex, NumberOfProbesZ, InterpNumberOfProbesZ);
% end

MovieFolder = fullfile(FullShotFolder, 'Plots/Movies/Interpolated');
if ~exist(MovieFolder,'dir')
    mkdir(MovieFolder)
end
JPEGFolder = fullfile(MovieFolder, sprintf('Movie_PNG_%d', shot));
if ~exist(JPEGFolder, 'dir')
   mkdir(JPEGFolder)
end

% Create and open a VideoWriter object
if saveMovie == 1
    videoFile = fullfile(MovieFolder, sprintf('movie_Interp_%d.mp4', shot));
    writerObj = VideoWriter(videoFile, 'MPEG-4');
    writerObj.FrameRate = 10; % Set the frame rate of the video

    open(writerObj);
end


%%
%%%% Begin Plotting Each Frame %%%%%
% coordinates for bottem left corner of subplot
xpos=[0.08 0.37 0.67];
ypos=[0.68 0.35 0.03];
% width and hight of subplot 
dy=0.26; % 2.4
dx=0.75;


frames = 0;
% TimeIndexRange = 370;
% TimeIndexRange = 1:3:15;
% TimeIndexRange =100:3:600; % dipole movies
% TimeIndexRange =1:3:580; % TREX movies
% TimeIndexRange =580:3:630;
for iit = TimeIndexRange
    frames = frames +1;
    moviePlot = figure(1);
    clf
    moviePlot.Position = [400 100 500 760];

    H(1) = subplot('position',[xpos(1) ypos(1) dx dy]); % Bz Plot
    H(2) = subplot('position',[xpos(1) ypos(2) dx dy]); % dBz Plot
    H(3) = subplot('position',[xpos(1) ypos(3) dx dy]); % Jp Plot
    
    colormap('jet') % hsv also works well       
  
    
    %%%%%%%%% Bz Plot %%%%%%%%%%%%%%%%%%%%%%
    axes(H(1))
 
    BzFrame = squeeze(BzMovie(:,iit,:)); %(R, t, Z), squeezing it in time to get one frame in time
    Psi = squeeze(PsiTot(:,iit,:));
   
    % Define Psi Contour lines to plot
    PsiV1=(linspace(-1,1,31)-1/100)*2e-3;
    PsiV2=(linspace(0,1,3)+0.01)*0.7e-4;
    PsiV3=linspace(-12,12,61)*1e-4;
    PsiV4=linspace(-0.4,0.4,6)*1e-4;
    
    %%%% Plot Bz %%%%%%
    pcolor(Zvec,Rvec,-BzFrame*1e3), shading interp
    hold on
    lw=1.15;
    % contour(Zvec,Rvec,BzFrame*1e3,[0 0],'r', 'Linewidth', 1)
    contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV3(2:2:end),'k','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)
    
    TimeMuSec = timeIndex(iit)/40 -4; % Calculate time of frame in micro seconds
    % text( 0.05+Stick1Pos,0.48, [sprintf('shot # %d, t = %0.2f\\mus, iit = %d', shot, TimeMuSec, iit)], 'fontsize', 13)
    text(0.05+Stick1Pos,0.48, [sprintf('shot # %d, t = %0.2f\\mus', shot, TimeMuSec)], 'fontsize', 13)
    
    % Set Tick Spacing
    h=gca;
    set(h,'xtick',-0.2:0.2:1)
    set(h,'ytick',-0.8:0.2:1)
    
    xlabel('Z [m]')
    text(-0.05+Stick1Pos,0.05,'R [m]','rotation',90)
    text(0.82+Stick1Pos,0.48, 'B_z [mT]')
    
    clim(BzClims)
    cBar=colorbar;
    % Account for different configurations 
    if BzClims(2)>5
        set(cBar,'ytick',-10:5:10) % set Colorbar tick spacing
    else
        set(cBar,'ytick',-10:1:10) % set Colorbar tick spacing
    end
    pbaspect([84 60 1]) % set aspect ratio of plots based on probe dimensions
    

    %%%%%%%%%%%%%%% dBz Plot %%%%%%%%%%%%%%%%%%%%
    axes(H(2))
    
    dBz = squeeze(dBzMovie(:,iit,:));
    pcolor(Zvec,Rvec,dBz),shading interp 
    hold on
    
    % contour(Zvec,Rvec,BzFrame*1e3,[0 0],'r', 'Linewidth', 1)
    contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV3(2:2:end),'k','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)

    h=gca;
    set(h,'xtick',-0.2:0.2:1)
    set(h,'ytick',-0.8:0.2:1)
    xlabel('Z [m]')
    text(-0.05+Stick1Pos,0.05,'R [m]','rotation',90)
    text( 0.75+Stick1Pos,0.48, 'dB_z/dt [T/s]')
    
    colorbar;

    clim(dBzClims)
    pbaspect([84 60 1])
    
%%%%%%%%%%%%%%%% Jphi Plot %%%%%%%%%%%%%%%%%%%%
    axes(H(3))
  
    Jphi=squeeze(JphiMovie(:,iit,:));
    pcolor(Zvec,Rvec,Jphi*1e-3),shading interp % plot based on Grad-Shafranov
    hold on
  
    h=gca;
    % contour(Zvec,Rvec,BzFrame*1e3,[0 0],'r', 'Linewidth', 1)
    contour(Zvec,Rvec,Psi,PsiV3(1:2:end),'m','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV3(2:2:end),'k','linewidth',lw)
    contour(Zvec,Rvec,Psi,PsiV4,'w','linewidth',lw)

    set(h,'xtick',-0.2:0.2:1)
    set(h,'ytick',-0.8:0.2:1)
    xlabel('Z [m]')
    text(-0.05+Stick1Pos,0.05,'R [m]','rotation',90)
    text( 0.65+Stick1Pos,0.48, 'J_{\phi}(\psi) [kA/m^2]')

    colorbar;
  
    Jclims=[-100 100]*(0.1+ 2*iit/1200);
    clim(Jclims)
    pbaspect([84 60 1]) 
    fontsize(moviePlot, 14, "points")

    %%%%%%%%%%%%%%%%%%% Save Frame %%%%%%%%%%%%%%%%%%%%%
    PNGFile = fullfile(JPEGFolder,sprintf('%d_Frame%d.png', shot, frames));

    if saveFrame == 1
        print('-f1', PNGFile, '-dpng', '-r400')
    else
        pause(0.5)
    end
    if saveMovie == 1
        
        CurrentFrame = getframe(moviePlot);
        writeVideo(writerObj, CurrentFrame); % Write the frame to the video
    else
        pause(0.5)
    end
    
end
% Close the VideoWriter object
if saveMovie == 1
    close(writerObj);
end

