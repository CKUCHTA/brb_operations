% load Flux Array data from dtacs 323 324 325 326. Channels from the dtaq's 
% are assigned to thier associated probes, and corrections are made to
% probes with funky signals. NO HELMHOLTZ field is added to the B-field! A
% calculation of the flux is made for FarrayPlot.

function [BSet, dBSet, ProbePos , SampleRange, Bh, tpoints] = loadFarray(shot, daq, Bh, SampleRange, dt, IIm, IIoff) 
% clear
% shot = 64920;
% SampleRange=69000:72000;
% IIm=901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% dt=1e-7; % time between samples
% IIoff=1000; %Makes Bz=Bh at this index
% saveData = 1; % save daq
% ProbeType = 2; % 2 for 1DFarray
% [daq, Bh] = loadDtaqDataFarray(shot, saveData, SampleRange);

PlotRaw = 1; % change to 1 for plot of digitizer data

facZ=(3.25e4/1.25)/3; % calibration factor that multiplies the voltage signal from probe
% facR=facZ;
% facP=facZ;


daqField = fieldnames(daq);
daq1 = daq.(daqField{1});
SampleRange = 1:size(daq1,1);

tpoints = (SampleRange(1):SampleRange(end))*dt*1e6; % global time array in ms

% each Board corrisponds to the same stick on the array
% if shot > 61670 && shot < 62498 && shot >64880
%     board1 = daq.a323(SampleRange,1:16); 
%     board2 = daq.a323(SampleRange,17:32);
%     board3 = daq.a324(SampleRange,1:16);
%     board4 = daq.a324(SampleRange,17:32);

%     board5 = daq.a325(SampleRange,1:16);
%     board6 = daq.a325(SampleRange,17:32);
%     board7 = daq.a326(SampleRange,1:16);
%     board8 = daq.a326(SampleRange,17:32);

% elseif shot > 64880
%     board1 = daq.a323(SampleRange,1:16); 
%     board2 = daq.a323(SampleRange,17:32);
%     board3 = daq.a324(SampleRange,1:16);
%     board4 = daq.a324(SampleRange,17:32);

%     board5 = daq.a325(SampleRange,1:16);
%     board6 = daq.a325(SampleRange,17:32);
%     board7 = daq.a326(SampleRange,1:16);
%     board8 = daq.a326(SampleRange,17:32);

% else
    board1 = daq.a115(SampleRange,1:16); 
    board2 = daq.a115(SampleRange,17:32);
    board3 = daq.a113(SampleRange,1:16);
    board4 = daq.a113(SampleRange,17:32);
    
    board5 = daq.a114(SampleRange,1:16);
    board6 = daq.a114(SampleRange,17:32);
    board7 = daq.a322(SampleRange,1:16);
    board8 = daq.a322(SampleRange,17:32);
% end

boards = {board1, board2, board3, board4, board5, board6, board7, board8};

%Plot Raw digitizer data from each board
if PlotRaw == 1
    figure(81), clf
    sgtitle(sprintf('Shot %d', shot))
    titleName = {'1' '2' '3' '4' '5' '6' '7' '8'};
    for i = 1:8
        subplot(2,4,i)
        for ch = 1:16
            plot(boards{i}(:,ch)+0.1*ch)
            hold on
            title(sprintf('Board %s',titleName{i}))
            ylim([0 1.8])
        end
    end
end

% Assign boards and channels to Sticks in array
dBSet = cell(8,3); % size of cell is from number of boards and nuber of direction (r, phi, z)

% Sticks 1 thru 6. Channels ordered from lowest to highest radial position
for k = 1:6
    dBSet{k,1} = [boards{k}(:, 15) boards{k}(:, 14)]; %r dirction
    dBSet{k,2} = [boards{k}(:, 13) boards{k}(:, 12)]; %phi dirction
    dBSet{k,3} = [boards{k}(:, 11) boards{k}(:, 10) boards{k}(:, 9)...
        boards{k}(:, 8) boards{k}(:, 7) boards{k}(:, 6) boards{k}(:, 5)...
        boards{k}(:, 4) boards{k}(:, 3) boards{k}(:, 2) boards{k}(:, 1)...
        boards{k}(:, 16)]; %z dirction
end

% Sticks 7 and 8
dBSet{7,1} = [boards{7}(:, 15) boards{7}(:, 14)]; %r dirction
dBSet{7,2} = [boards{7}(:, 13) boards{7}(:, 12)]; %phi dirction
dBSet{7,3} = [boards{7}(:,7) boards{7}(:,10) boards{7}(:,9)...
    boards{7}(:,8) boards{7}(:,15) boards{7}(:,2) boards{7}(:,5)...
    boards{7}(:,4) boards{7}(:,3) boards{7}(:,12) boards{7}(:,1)...
    boards{7}(:,16)]; %z dirction

dBSet{8,1} = boards{8}(:, 12:13);%r dirction
dBSet{8,2} = [boards{8}(:, 15) boards{8}(:, 14)]; %phi dirction
dBSet{8,3} = [boards{8}(:, 16) boards{8}(:, 1:11)]; %z dirction

% multiply stick 8 channel 10 by negative sign
dBSet{8,3}(:,10) = -dBSet{8,3}(:,10);

% %Fixing Probes
% dBSet{1,3}(:,8) = (dBSet{1,3}(:,7) + dBSet{1,3}(:,9))/2;
% dBSet{8,3}(:,9) = (dBSet{8,3}(:,8) + dBSet{8,3}(:,10))/2;
% dBSet{6,3}(:,11) = (dBSet{5,3}(:,10) + dBSet{6,3}(:,12))/2;
% dBSet{6,3}(:,10) = (dBSet{5,3}(:,11) + dBSet{6,3}(:,9))/2;
% dBSet{8,3}(:,5) = (dBSet{8,3}(:,4) + dBSet{8,3}(:,6))/2;

if PlotRaw ==1
    figure(93), clf
    sgtitle(sprintf('Z-direction, Shot %d', shot))

end

%substracting off off-set 
for j = 1:length(dBSet)
    dBSetR = dBSet{j,1};
    dBSetP = dBSet{j,2};
    dBSetZ = dBSet{j,3};
    if PlotRaw == 1
        for ch = 1:12
        subplot(2,4,j)
        plot(dBSetZ(:,ch)+0.1*ch)
        hold on
        % xlim(xlims)
        title(sprintf('Stick %d', j))  
        end
    end
    for k=1:size(dBSetR,2)
        dBSetR(:,k) = dBSetR(:,k)-mean(dBSetR(IIm,k));
    end
    for k=1:size(dBSetP,2)
        dBSetP(:,k) = dBSetP(:,k)-mean(dBSetP(IIm,k));
    end
    for k=1:size(dBSetZ,2)
        dBSetZ(:,k) = dBSetZ(:,k)-mean(dBSetZ(IIm,k));
    end
    dBSet{j,1} = dBSetR;
    dBSet{j,2} = dBSetP;
    dBSet{j,3} = dBSetZ;    
end

dBSet=cellfun(@(x) x*facZ,dBSet,'un',0); % multiply dB cell by SAME correction factor

% tpoints = (length(SamRange(1)):length(SamRange(end)))*dt*1e6; % time array in ms

%Radial position of R probes
RRvecZ11 = [21.8 47.2]*1e-2;
RRvecZ16 = 0.562 - RRvecZ11; 
dBrRpos = 0.463 - RRvecZ16;

%Radial position of Phi probes
PRvecZ11 = [24.3 44.7]*1e-2;
PRvecZ16 = 0.562 - PRvecZ11; 
dBpRpos = 0.463 - PRvecZ16;

%Radial position of Z probes
manifoldRpos = 0.51; % radial position of the manifold
ZRvecZ11 = [0 5.1 10.2 15.3 20.4 25.5 30.6 35.7 40.8 45.9 51 56.2]*1e-2; % Z probe locations relative to probe 11, which is furthest radially inward)
ZRvecZ16 = 0.562 - ZRvecZ11; % Z probe position relative to probe 16, which is closest to the manifold
% dBzRpos = 0.463 - ZRvecZ16; % Z probe radial position calculated based on probe 16 position
dBzRpos = manifoldRpos - 0.057 - ZRvecZ16; % Z probe radial position (0.057 is the distance from the manifold to the first probe)

ProbePos = {dBrRpos dBpRpos dBzRpos}; % collection of all probe locations

% Calculating B
BSet = cell(length(dBSet),3); 
for k = 1:length(dBSet) 
    BSet{k,1} = cumsum(dBSet{k,1})*dt; % Br
    BSet{k,2} = cumsum(dBSet{k,2})*dt; % Bp
    BSet{k,3} = cumsum(dBSet{k,3})*dt; % Bz
end

FluxArrayStruc = what('FluxArray_git');
FluxArrayFolder = FluxArrayStruc.path;
load(fullfile(FluxArrayFolder,'facZ1DArray.mat'), 'facZ_FromDifference')  % load correction factor

% Phi = struct();
for j = 1:length(BSet)
    BSetR = BSet{j,1};
    BSetP = BSet{j,2};
    BSetZ = BSet{j,3};
    dBSetZ = dBSet{j,3};
    facC = facZ_FromDifference(j,:);
    %setting B = 0 just before drive coils are turn on.
    for k=1:size(BSetR,2)
        BSetR(:,k) = BSetR(:,k)-BSetR(IIoff,k);
    end
    for k=1:size(BSetP,2)
        BSetP(:,k) = BSetP(:,k)-BSetP(IIoff,k);
    end
    for k=1:size(BSetZ,2)
        BSetZ(:,k) = BSetZ(:,k)-BSetZ(IIoff,k);
        % BSetZ(:,k) = BSetZ(:,k)*facC(k); % calibrate field based on CalculateFac
        % dBSetZ(:,k) = dBSetZ(:,k)*facC(k); % calibrate field based on CalculateFac
    end


    dBSet{j,1} = dBSetR;
    dBSet{j,2} = dBSetP;
    dBSet{j,3} = dBSetZ;    

    BSet{j,1} = BSetR;
    BSet{j,2} = BSetP;
    BSet{j,3} = BSetZ; 
end

if PlotRaw ==1
    figure(94), clf
    sgtitle(sprintf('Z-direction Corrected, Shot %d', shot))
    for j = 1:length(dBSet)
        dBSetZ = dBSet{j,3};
        for ch = 1:12
            subplot(2,4,j)
            plot(dBSetZ(:,ch)+2000*ch)
            hold on
            % xlim(xlims)
            title(sprintf('Stick %d', j))  
        end
    end
end