% load Flux Array data from dtacs 323 324 325 326. Channels from the dtaq's 
% are assigned to thier associated probes, and corrections are made to
% probes with funky signals. NO HELMHOLTZ field is added to the B-field! A
% calculation of the flux is made for FarrayPlot.

function [dBSetRaw, SampleRange, Bh, tpoints] = loadFarray(shot, daq, Bh, dt) 
% clear
% shot = 64968;
% SampleRange=69000:73000;
% dt=1e-7; % time between samples
% saveData = 1; % save daq
% [daq, Bh] = loadDtaqDataFarray(shot, saveData, SampleRange);

PlotRaw = 1; % change to 1 for plot of digitizer data

% Calculate SampleRange size using first daq entry
daqField = fieldnames(daq);
daq1 = daq.(daqField{1});
SampleRange = 1:size(daq1,1);

tpoints = (SampleRange(1):SampleRange(end))*dt*1e6; % global time array in ms

% each Board corrisponds to the same stick on the array
if shot > 61670 && shot < 62498 && shot < 64880
    board1 = daq.a323(SampleRange,1:16); 
    board2 = daq.a323(SampleRange,17:32);
    board3 = daq.a324(SampleRange,1:16);
    board4 = daq.a324(SampleRange,17:32);

    board5 = daq.a325(SampleRange,1:16);
    board6 = daq.a325(SampleRange,17:32);
    board7 = daq.a326(SampleRange,1:16);
    board8 = daq.a326(SampleRange,17:32);

elseif shot > 64880 && shot < 64917
    board1 = daq.a323(SampleRange,1:16); 
    board2 = daq.a323(SampleRange,17:32);
    board3 = daq.a324(SampleRange,1:16);
    board4 = daq.a324(SampleRange,17:32);

    board5 = daq.a325(SampleRange,1:16);
    board6 = daq.a325(SampleRange,17:32);
    board7 = daq.a326(SampleRange,1:16);
    board8 = daq.a326(SampleRange,17:32);

elseif shot > 64917
    board1 = daq.a115(SampleRange,1:16); 
    board2 = daq.a115(SampleRange,17:32);
    board3 = daq.a113(SampleRange,1:16);
    board4 = daq.a113(SampleRange,17:32);
    
    board5 = daq.a114(SampleRange,1:16);
    board6 = daq.a114(SampleRange,17:32);
    board7 = daq.a322(SampleRange,1:16);
    board8 = daq.a322(SampleRange,17:32);
end

boards = {board1, board2, board3, board4, board5, board6, board7, board8};

%Plot Raw digitizer data from each board
if PlotRaw == 1
    figure(81), clf
    sgtitle(sprintf('Shot %d', shot))
    titleName = {'1' '2' '3' '4' '5' '6' '7' '8'};
    for i = 1:8
        subplot(2,4,i)
        for ch = 1:16
            plot(boards{i}(:,ch)+0.1*ch)
            hold on
            title(sprintf('Board %s',titleName{i}))
            ylim([0 1.8])
        end
    end
end

% Assign boards and channels to Sticks in array
dBSet = cell(8,3); % size of cell is from number of boards and nuber of direction (r, phi, z)

% Sticks 1 thru 6. Channels ordered from lowest to highest radial position
for k = 1:6
    dBSet{k,1} = [boards{k}(:, 15) boards{k}(:, 14)]; %r dirction
    dBSet{k,2} = [boards{k}(:, 13) boards{k}(:, 12)]; %phi dirction
    dBSet{k,3} = [boards{k}(:, 11) boards{k}(:, 10) boards{k}(:, 9)...
        boards{k}(:, 8) boards{k}(:, 7) boards{k}(:, 6) boards{k}(:, 5)...
        boards{k}(:, 4) boards{k}(:, 3) boards{k}(:, 2) boards{k}(:, 1)...
        boards{k}(:, 16)]; %z dirction
end

% Sticks 7 and 8
dBSet{7,1} = [boards{7}(:, 15) boards{7}(:, 14)]; %r dirction
dBSet{7,2} = [boards{7}(:, 13) boards{7}(:, 12)]; %phi dirction
dBSet{7,3} = [boards{7}(:,7) boards{7}(:,10) boards{7}(:,9)...
    boards{7}(:,8) boards{7}(:,15) boards{7}(:,2) boards{7}(:,5)...
    boards{7}(:,4) boards{7}(:,3) boards{7}(:,12) boards{7}(:,1)...
    boards{7}(:,16)]; %z dirction

dBSet{8,1} = boards{8}(:, 12:13);%r dirction
dBSet{8,2} = [boards{8}(:, 15) boards{8}(:, 14)]; %phi dirction
dBSet{8,3} = [boards{8}(:, 16) boards{8}(:, 1:11)]; %z dirction

% multiply stick 8 channel 10 by negative sign
dBSet{8,3}(:,10) = -dBSet{8,3}(:,10);

% %Fixing Probes
dBSet{4,3}(:,8) = (dBSet{4,3}(:,7) + dBSet{4,3}(:,9))/2;
dBSet{8,3}(:,9) = (dBSet{8,3}(:,8) + dBSet{8,3}(:,10))/2;
dBSet{7,3}(:,7) = (dBSet{7,3}(:,8) + dBSet{7,3}(:,6))/2;
dBSet{6,3}(:,10) = (dBSet{5,3}(:,11) + dBSet{6,3}(:,9))/2;
dBSet{8,3}(:,5) = (dBSet{8,3}(:,4) + dBSet{8,3}(:,6))/2;

dBSetRaw = dBSet;

if PlotRaw ==1
    figure(93), clf
    sgtitle(sprintf('Z-direction, Shot %d', shot))

end

if PlotRaw ==1
    figure(94), clf
    sgtitle(sprintf('Z-direction Corrected, Shot %d', shot))
    for j = 1:length(dBSet)
        dBSetZ = dBSet{j,3};
        for ch = 1:12
            subplot(2,4,j)
            plot(dBSetZ(:,ch)+0.3*ch)
            hold on
            % xlim(xlims)
            title(sprintf('Stick %d', j))  
        end
    end
end