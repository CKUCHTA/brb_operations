% Using the data orginized in loadFarry.m, this script calibrates the data for the z direction
% and takes into account the digitizer offsets. The probe positions are also calculated using the geometry
% of the arrays being parallel with the z-axis and the sticks orintated below the manifold.

function [BSet, dBSet, Bh, ProbePos, tpoints] = Calibrate1DFluxArray(IIoff, IIm, dt, dBSetRaw, Bh, tpoints, PlotCalibrated)
% clear
% shot = 64968;
% IIm=1740:1800; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% IIoff=1800; %Makes Bz=Bh at this index
% dt=1e-7; % time between samples
% saveData = 1; % save daq
% SampleRange = 69000:73000;
% [daq, Bh] = loadDtaqDataFarray(shot, saveData, SampleRange);
% [dBSetRaw, SampleRange, Bh, tpoints] = loadFarray(shot, daq, Bh, dt); 
% PlotCalibrated = 1; % change to 1 for plot time trace of calibrated data

facZ=(3.25e4/1.25)/3; % calibration factor that multiplies the voltage signal from probe
% facR=facZ;
% facP=facZ;

dBSet = dBSetRaw;

%substracting off off-set 
for j = 1:length(dBSet)
    dBSetR = dBSet{j,1};
    dBSetP = dBSet{j,2};
    dBSetZ = dBSet{j,3};

    for k=1:size(dBSetR,2)
        dBSetR(:,k) = dBSetR(:,k)-mean(dBSetR(IIm,k));
    end
    for k=1:size(dBSetP,2)
        dBSetP(:,k) = dBSetP(:,k)-mean(dBSetP(IIm,k));
    end
    for k=1:size(dBSetZ,2)
        dBSetZ(:,k) = dBSetZ(:,k)-mean(dBSetZ(IIm,k));
    end
    dBSet{j,1} = dBSetR;
    dBSet{j,2} = dBSetP;
    dBSet{j,3} = dBSetZ;    
end

dBSet=cellfun(@(x) x*facZ,dBSet,'un',0); % multiply dB cell by SAME correction factor

% tpoints = (length(SamRange(1)):length(SamRange(end)))*dt*1e6; % time array in ms


% Calculating B
BSet = cell(length(dBSet),3); 
for k = 1:length(dBSet) 
    BSet{k,1} = cumsum(dBSet{k,1})*dt; % Br
    BSet{k,2} = cumsum(dBSet{k,2})*dt; % Bp
    BSet{k,3} = cumsum(dBSet{k,3})*dt; % Bz
end

FluxArrayStruc = what('FluxArray_git');
FluxArrayFolder = FluxArrayStruc.path;
load(fullfile(FluxArrayFolder,'facZ1DArray4.mat'), 'facZ_FromDifference4')  % load correction factor
load(fullfile(FluxArrayFolder,'facZ1DArray2.mat'), 'facZ_FromDifference2')  % load correction factor
load(fullfile(FluxArrayFolder,'facZ1DArray3.mat'), 'facZ_FromDifference3')  % load correction factor
% Phi = struct();
for j = 1:length(BSet)
    BSetR = BSet{j,1};
    BSetP = BSet{j,2};
    BSetZ = BSet{j,3};
    dBSetZ = dBSet{j,3};
    facC = facZ_FromDifference4(j,:);
    facC2 = facZ_FromDifference2(j,:); 
    facC3 = facZ_FromDifference3(j,:); 
    % facC = facC1.*facC2.*facC3; % combine all correction factors
    %setting B = 0 just before drive coils are turn on.
    for k=1:size(BSetR,2)
        BSetR(:,k) = BSetR(:,k)-BSetR(IIoff,k);
    end
    for k=1:size(BSetP,2)
        BSetP(:,k) = BSetP(:,k)-BSetP(IIoff,k);
    end
    for k=1:size(BSetZ,2)
        BSetZ(:,k) = BSetZ(:,k)-BSetZ(IIoff,k);
        BSetZ(:,k) = BSetZ(:,k)*facC(k); % calibrate field based on CalculateFac
        dBSetZ(:,k) = dBSetZ(:,k)*facC(k); % calibrate field based on CalculateFac
    end
    dBSet{j,1} = dBSetR;
    dBSet{j,2} = dBSetP;
    dBSet{j,3} = dBSetZ;    

    BSet{j,1} = BSetR;
    BSet{j,2} = BSetP;
    BSet{j,3} = BSetZ; 
end


%Radial position of R probes
RRvecZ11 = [21.8 47.2]*1e-2;
RRvecZ16 = 0.562 - RRvecZ11; 
dBrRpos = 0.463 - RRvecZ16;

%Radial position of Phi probes
PRvecZ11 = [24.3 44.7]*1e-2;
PRvecZ16 = 0.562 - PRvecZ11; 
dBpRpos = 0.463 - PRvecZ16;

%Radial position of Z probes
manifoldRpos = 0.51; % radial position of the manifold
ZRvecZ11 = [0 5.1 10.2 15.3 20.4 25.5 30.6 35.7 40.8 45.9 51 56.2]*1e-2; % Z probe locations relative to probe 11, which is furthest radially inward)
ZRvecZ16 = 0.562 - ZRvecZ11; % Z probe position relative to probe 16, which is closest to the manifold
% dBzRpos = 0.463 - ZRvecZ16; % Z probe radial position calculated based on probe 16 position
dBzRpos = manifoldRpos - 0.057 - ZRvecZ16; % Z probe radial position (0.057 is the distance from the manifold to the first probe)

ProbePos = {dBrRpos dBpRpos dBzRpos}; % collection of all probe locations


if PlotCalibrated == 1
    figure(44), clf
    for j = 1:length(dBSet)
        dBSetR = dBSet{j,1};
        dBSetP = dBSet{j,2};
        dBSetZ = dBSet{j,3};
        for ch = 1:12
            subplot(2,4,j)
            plot(dBSetZ(:,ch)+2000*ch)
            hold on
            % xlim(xlims)
            title(sprintf('Stick %d', j))  
        end
    end
end
