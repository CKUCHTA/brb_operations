%Grab data from MDSPlus without saving

function [daq, Bh] = acq_dataFarray(shotnum, SampleRange)
% clear
% shotnum = 64881;
% SampleRange = 69000:77000;

tic
mdsconnect('skywalker.physics.wisc.edu');
mdsopen('wipal', shotnum)

daq=struct();
Bh_Tesla = mdsvalue('.RAW.A373_DIRECT:CH_43');% Helmholtz in T
Bh = mean((Bh_Tesla)*200*0.3521*1e-4);% Helmholtz in mT
sprintf('Bh = %0.4f', Bh)

% define sample range of data to download
firstSample = SampleRange(1);
lastSample = SampleRange(end);

% if shotnum > 61670 && shotnum < 62498 || shotnum > 64880
%     all_serials = [323 324 325 326];
% elseif shotnum >= 62498   
%     all_serials = [114 115 113 322];
% end
all_serials = [114 115 113 322]; 

% Call all dtaqs with one command and set the sample range that is downloaded
serialCommands = cell(size(all_serials));
k = 0;
for serial = all_serials
    k = k + 1;
    disp(serial)
    for ch = 1:32
        if ch == 1
            command = sprintf('BUILD_SIGNAL( [ DATA(\\ta%d_%02d)[%d : %d]', serial, ch, firstSample, lastSample);
        elseif ch == 32
            command = strcat(command, sprintf(', DATA(\\ta%d_%02d)[%d : %d]], *, DIM_OF(\\ta%d_01)[%d : %d])', serial, ch, firstSample, lastSample, serial, firstSample, lastSample));
        else
            command = strcat(command, sprintf(', DATA(\\ta%d_%02d)[%d : %d]', serial, ch, firstSample, lastSample));
        end
    end
    serialCommands{k} = command;  
end

% Create the full command to send to mdsplus
FullCommand = sprintf('BUILD_SIGNAL( [ %s, %s, %s, %s ], *, DIM_OF(%s))'...
    , serialCommands{1}, serialCommands{2}, serialCommands{3}, serialCommands{4}, serialCommands{1});


allSerialData = mdsvalue(FullCommand);  % send command to mdsplus

% separate data for each dtaq
for ind = 1:size(allSerialData,3)
    serial = all_serials(ind);
    daq.(['a' num2str(serial)]) = allSerialData(:,:,ind);
end


mdsclose;
mdsdisconnect;

toc
% example daq.([a113])(:,1) = mdsvalue(['\ta113_01']);
%output = mdsvalue('\hook_bdot1_r')