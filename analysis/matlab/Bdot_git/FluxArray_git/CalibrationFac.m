% Calculates the calibration correction factor for the flux array. The
% calibration factor is determine by finding the difference between the
% B-field before reconnection and after reconnection.  This difference is
% then corrected using vacuum plots and the ration of the corrected
% difference to the original difference is plotted.  There should be no
% more than a 10 percent change between probes, which can be confirmed in
% the fac plot.

clear
shotlist = 61994:61998;
% shot = 61877;

SamRange=69000:71000;
IIm=901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
dt=1e-7; % time between samples
IIoff=1000; %Makes Bz=Bh at this index

facTot = struct(); 
kk = 0;
for shot = shotlist
    kk = kk+1;
    [daq, Bh] = loadDtaqDataFarray(shot); % load dtac from harddrive or from mdsplus
    [BSet, dBSet, ProbePos , SamRange, Bh, tpoints] = loadFarray(daq, Bh, SamRange, dt, IIm, IIoff); 
    
    %Convert Cell to 3D array
    dBz=zeros([length(tpoints) , 8,12]);
    Bz=zeros([length(tpoints) , 8,12]);
     
    for k=1:8 
        dBz(:,k,:)=dBSet{k,3};
        Bz(:,k,:)=BSet{k,3};
    end
    
    % DelB should be a smooth field and it should not go through zero when HH
    % field is not added to B. 
    DelB=0;
    t_initial = 1100;
    t_final = 1460;
    
    DelB=DelB+squeeze(Bz(t_initial,:,:)-Bz(t_final,:,:)); % Change of Bz in the time before drive coils are on and when 
                                                  % the reconneciton starts.  
    
    
    DelBc=DelB; % DelB corrected. More bad probe corrections. Not bad enough to turn off. (Indexes are reversed from loadFarray)
    % DelBc(8,1)=(DelB(7,1)+DelB(8,2))/2;
    % DelBc(8,11)=(DelB(8,10)+DelB(8,12))/2;
    % DelBc(4,3)=(DelB(4,2)+DelB(4,4))/2;
    % DelBc(4,5)=(DelB(4,4)+DelB(4,6))/2;
    
    DelBc(4,2)=(DelB(4,3)+DelB(4,1))/2;
    DelBc(4,10)=(DelB(4,11)+DelB(4,9))/2;
    DelBc(1,4)=(DelB(1,5)+DelB(1,3))/2;
    
    % smooth Corrected DelB
    DelBc=smoo(smoo(DelBc',2)',2); % 2 is number of points smoothing over, each transpose is a smooth over each direction
    
    facShot=DelBc./DelB; % calibration factor from difference between time before event and after event
    
    facTot(kk).shot = shot;
    facTot(kk).facShot = facShot;
end                                                  

%%
% find calibration factor by averaging over each shot calibration factor
for i = 1:length(facTot)
    if i == 1
        SumFac = facTot(i).facShot;
    else
        SumFac = SumFac + facTot(i).facShot;
    end
end
Calfac = SumFac/length(facTot);


figure(104),clf
pcolor(squeeze(Bz(t_initial,:,:)-Bz(t_final,:,:))),shading interp % calibration for understanding DelB
colorbar
title(sprintf('Bz(t = %d) - Bz(t = %d)', t_initial, t_final))

figure(105),clf
subplot(1,3,1)
pcolor(DelB),shading interp
colorbar
% caxis([0 0.056])
title('DelB')

subplot(1,3,2)
pcolor(DelBc),shading interp
colorbar
% caxis([0 0.056])
title('DelB Corrected')


subplot(1,3,3)
pcolor(Calfac),shading interp
colorbar
% clim([0.9 1.1])
title('Correction Factor')

% save '/Users/paulgradney/Research/Matlab Drive Cylinder/Bdot/FluxArray/fac.mat' Calfac

