% Plot flux Array data using loadFarray
% created 07Apr23

function  Plot1DFarray(shot, savePlot, saveData, IncludeContour, PlotFlux)
% clear
% shot = 64959;
% savePlot = 0;
% IncludeContour = 0;
% PlotFlux = 0;
% saveData = 1;

if savePlot == 1
    ShotFolderStruct = what('DipoleCusp2024');
    ShotFolder = ShotFolderStruct.path;
    if IncludeContour == 1
        PlotFolder = fullfile(ShotFolder,'1DFarray/Plots/VerticalContourPlots');
    else
        PlotFolder = fullfile(ShotFolder,'1DFarray/Plots/VerticalPlots');
    end
    
    PlotFile1to4 = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Stk1to4.png', shot));
    PlotFile5to8 = fullfile(PlotFolder,sprintf('fArrayPlot_%d_Stk5to8.png', shot));
end


% Parameters for loadFarray
% SamRange=69000:71000;
SampleRange=69000:73000;
IIm=1740:1800; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIoff=1800; %Makes Bz=Bh at this index
dt=1e-7; % time between samples
PlotCalibrated = 0; % change to 1 for plot time trace of calibrated data

[daq, Bh] = loadDtaqDataFarray(shot, saveData, SampleRange);
[dBSetRaw, SampleRange, Bh, tpoints] = loadFarray(shot, daq, Bh, dt); 
[BSet, dBSet, Bh, ProbePos, tpoints] = Calibrate1DFluxArray(IIoff, IIm, dt, dBSetRaw, Bh, tpoints, PlotCalibrated);


RprobeRpos = ProbePos{1}; % radial position of R probes
PprobeRpos = ProbePos{2}; % radial position of Phi probes
ZprobeRpos = ProbePos{3}; % radial position of Z probes

%%
%Plotting parameters
xlims=[1800, 2050];
% xlims=[1000, 2380];
GlobalRange = SampleRange(xlims);
% xlims = [7000 9000]; %Jets
tlims = tpoints(xlims);
coord = {'z' 'r' 'p'};
cAxes = {[-10 10] [-4 4] [-4 4]}; % pcolor limits for B

% plot Flux
if PlotFlux == 1
    figure(44), clf
    pcolor(tpoints, ZprobeRpos, Phi(1).flux'), shading interp
    hold on
    contour(tpoints, ZprobeRpos,Phi(1).flux', Phi.phiLevel1,'k', 'Linewidth', 1.2)
    contour(tpoints, ZprobeRpos,Phi(1).flux', Phi.phiLevel2,'k')
    contour(tpoints, ZprobeRpos,Phi(1).flux','k')
    colorbar
    clim([-4 2]*1e-3)
    xlim(tlims)
end

% Add HH field
Phi = struct();
for j = 1:length(BSet)
    BSet{j,3} = BSet{j,3} + Bh;
    BSetZ = BSet{j,3};

    % Smooth Bz Data
    sBz=smoo(BSetZ,4);
    ssBz=BSetZ-sBz;
    ssBz=smoo(ssBz,75);
    BSetZ=BSetZ-ssBz;

    BSet{j,3} = BSetZ;
    
    % Calculate Flux
    Phi(j).flux = BzFlux_FluxArray(BSet{j,3}, ZprobeRpos);
end

Phi(1).phiLevel1 = (-1:0.5:7)*1e-3; %countour line spacing
Phi(1).phiLevel2 = (-1:0.3:7)*1e-3; %countour line spacing

%%

% B field Plots for Sticks 1-4
fluxArrayPlot1to4 = figure(53);
clf
sgtitle(sprintf('Shot %d, FluxArray Probe, B_h = %0.1f[mT]', shot, Bh*1e3))
k = 0;
for i = 1:4
    k = k+2;
    subplot(4,2,k)
    pcolor(tpoints, ZprobeRpos,BSet{i,3}'*1e3),shading interp
    hold on
    contour(tpoints, ZprobeRpos,BSet{i,3}'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
    if IncludeContour == 1
        contour(tpoints, ZprobeRpos,Phi(i).flux', Phi(1).phiLevel1,'k', 'Linewidth', 1.2)
        contour(tpoints, ZprobeRpos,Phi(i).flux', Phi(1).phiLevel2,'k')
    end
    colorbar
    %clim([-20 20])
    xlim(tlims)
    title(sprintf('Bz[mT] Stick %d', i))
    ylabel('r [m]')
    xlabel('t [\mus]')
end

% dB field Plots for Sticks 1-4
k = 1;
for i = 1:4
    subplot(4,2,k)
    pcolor(tpoints, ZprobeRpos,dBSet{i,3}'),shading interp
    hold on
    contour(tpoints, ZprobeRpos,BSet{i,3}'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
    if IncludeContour == 1
        contour(tpoints, ZprobeRpos,Phi(i).flux', Phi(1).phiLevel1,'k', 'Linewidth', 1.2)
        contour(tpoints, ZprobeRpos,Phi(i).flux', Phi(1).phiLevel2,'k')
    end
    colorbar
    clim([-4 4]*1e3/10)
    xlim(tlims)
    title(sprintf('dBz[T/s] Stick %d', i))
    ylabel('r [m]')
    xlabel('t [\mus]')
    k = k+2;
end
% set(gcf, 'Position',  [100, 100, 1000, 900])
% fontsize(gcf, 16,'points')

if savePlot == 1
    saveas(fluxArrayPlot1to4, PlotFile1to4)
end

% B field Plots for Sticks 5-8
fluxArrayPlot5to8 = figure(54);
clf
sgtitle(sprintf('Shot %d, FluxArray Probe, B_h = %0.1f[mT]', shot, Bh*1e3))
k = 0;
for i = 5:8
    k = k+2;
    subplot(4,2,k)
    pcolor(tpoints, ZprobeRpos,BSet{i,3}'*1e3),shading interp
    hold on
    contour(tpoints, ZprobeRpos,BSet{i,3}'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
    if IncludeContour == 1
        contour(tpoints, ZprobeRpos,Phi(i).flux', Phi(1).phiLevel1,'k', 'Linewidth', 1.2)
        contour(tpoints, ZprobeRpos,Phi(i).flux', Phi(1).phiLevel2,'k')
    end
    colorbar
    %clim([-20 20])
    xlim(tlims)
    title(sprintf('Bz[mT] Stick %d', i))
    ylabel('r [m]')
    xlabel('t [\mus]')
end

% dB field Plots for Sticks 5-8
k = 1;
for i = 5:8
    subplot(4,2,k)
    % try
        pcolor(tpoints, ZprobeRpos,dBSet{i,3}'),shading interp
    % catch
    % end
    hold on
    contour(tpoints, ZprobeRpos,BSet{i,3}'*1e3,[0 0],'r', 'Linewidth', 1.5),shading interp
    if IncludeContour == 1
        contour(tpoints, ZprobeRpos,Phi(i).flux', Phi(1).phiLevel1,'k', 'Linewidth', 1.2)
        contour(tpoints, ZprobeRpos,Phi(i).flux', Phi(1).phiLevel2,'k')
    end
    colorbar
    %clim([-4 4]*1e3) 
    xlim(tlims)
    title(sprintf('dBz[T/s] Stick %d', i))
    ylabel('r [m]')
    xlabel('t [\mus]')
    k = k+2;
end
% set(gcf, 'Position',  [100, 100, 1000, 900])
% fontsize(gcf, 16,'points')

if savePlot == 1
    saveas(fluxArrayPlot5to8, PlotFile5to8)
end

return

%%

%dB and B Plots for one stick 
stick = 4;
dBz_stk = dBSet{stick,3};
% range = xlims(1):xlims(2);
% dBz_stk = dBz_stk(range,:);

Bz_stk= BSet{stick,3};
Bz_stk = Bz_stk';

figure(57), clf 
subplot(1,2,1)
% pcolor(tpoints, ZprobeRpos,dBz_stk'),shading interp
pcolor(dBz_stk'),shading interp
hold on
contour(Bz_stk*1e3,[0 0],'r', 'linewidth', 2),shading interp
if IncludeContour == 1
        contour(Phi(stick).flux', Phi(1).phiLevel1,'k', 'Linewidth', 1.2)
        contour(Phi(stick).flux', Phi(1).phiLevel2,'k')
end
colorbar
clim([-5 5]*1e3)
% xlim(tlims)
xlim(xlims)
title(sprintf('dBz Stick %d', stick))
ylabel('r [m]')
xlabel('t [\mus]')



subplot(1,2,2)
pcolor(tpoints, ZprobeRpos,Bz_stk*1e3),shading interp
hold on
contour(tpoints, ZprobeRpos,Bz_stk*1e3,[0 0],'r','linewidth', 2),shading interp
if IncludeContour == 1
        contour(tpoints, ZprobeRpos,Phi(stick).flux', Phi(1).phiLevel1,'k', 'Linewidth', 1.2)
        contour(tpoints, ZprobeRpos,Phi(stick).flux', Phi(1).phiLevel2,'k')
end
colorbar
clim([-10 10])
xlim(tlims)
title(sprintf('Bz[mT] Stick %d', stick))
ylabel('r [m]')
xlabel('t [\mus]')


%%
% dBz plot for 1 channel
StickNum = 4;
ProbeNum = 8;
Rpos = ZprobeRpos(ProbeNum);

SingleSig = figure(58);
clf
sgtitle(sprintf('Stick %d, R = %0.2f', StickNum, Rpos))
subplot(3,1,1)
dBset1 = dBSet{StickNum,3};
plot(tpoints,dBset1(:,ProbeNum))
xlim(tlims)
ylim([-2.2 2.2]*1e3)
% xlabel('Samples')
xlabel('t[\mus]')
ylabel('dB_z[T/s]')

subplot(3,1,2)
Bset1 = BSet{StickNum,3};
plot(tpoints,Bset1(:,ProbeNum)*1e3)
xlim(tlims)
xlabel('t[\mus]')
ylabel('B_z[mT]')

fontsize(SingleSig,scale=1.5)
% FFT of dBz
range = xlims(1):xlims(2);
SamRate = 10e6;
dBset1 = dBSet{StickNum,3};
dBsig = dBset1(:,ProbeNum);
dBsig = dBsig(range);

L = length(dBsig); %number of sample points in window
% fftdBsig = abs(fftshift(fft(dBsig))/L);
fftdBsig = fft(dBsig);
Hz = SamRate/L*(-L/2:L/2);

P1 = fftdBsig(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);

f = SamRate/L*(0:(L/2));


P2 = abs(fftdBsig/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);

f = SamRate*(0:(L/2))/L;

subplot(3,1,3)
% plot(Hz,log10(fftdBsig))
semilogx(f,P1)
xlabel('Hz')
ylabel('fft(dBz)')
title('fft of dB_z')

