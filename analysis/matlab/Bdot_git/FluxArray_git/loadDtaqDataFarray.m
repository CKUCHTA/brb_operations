% Load dtaq data from external harddrive or from mdsplus if data is not in
% external harddrive and save to Temporary folder

function [daq, Bh] = loadDtaqDataFarray(shot, saveData, SampleRange)
% clear
% shot = 63217;
% SampleRange = 69000:77000;
% ProbeType = 2; % 1 for 3D Farray, 2 for 1D Farray

% dtaqFileOld = sprintf('FarrayDTAC_%d.mat',shot);
dtaqFile = sprintf('1DFarrayDTAC_%d.mat',shot);
dtaqFilePath = which(dtaqFile); % path of TeData file
try
   load(dtaqFilePath, 'daq', 'Bh');
catch
    disp(['Could not find file: ', dtaqFile])
    % load data from mdsplus
    [daq, Bh] = acq_dataFarray(shot, SampleRange);
    % [daq, Bh] = acq_dataFarray(shot, SamRange); % load data from MDSPlus
    if saveData
        TempStruct = what('Temporary');
        TemporaryFolder = TempStruct.path; % relative file path  
        fileName = fullfile(TemporaryFolder, dtaqFile);
        save(fileName)
    end
end