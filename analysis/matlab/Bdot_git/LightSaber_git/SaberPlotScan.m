clear
DataFolder = '/Users/paulgradney/Research/Data';
%%%
% VacuumRadialFolder = fullfile(DataFolder, '/VacRadial_Scan/');
% 
% NoMidFolder = fullfile(VacuumRadialFolder,'NoMiddleCoil');
% MidFolder = fullfile(VacuumRadialFolder, 'WithMiddleCoil');
% NoMidDataFolder = fullfile(NoMidFolder,'/Scripts/LightsaberBdata');
% MidDataFolder = fullfile(MidFolder,'/Scripts/LightsaberBdata');
% 
% NoMidPlotFolder = fullfile(NoMidFolder, 'Plots');
% MidPlotFolder = fullfile(MidFolder, 'Plots');
%%%

% DriveVoltageFolder = fullfile(DataFolder, '/Drive_Voltage_Scan');
% LsaberDataFolder = fullfile(DriveVoltageFolder, 'Scripts/LightSaberBdata');

RadialScanFolder = 'TeSaber_Radial_Scan/6k_100G_MiddleOn';
FullRadialScanFolder = fullfile(DataFolder, RadialScanFolder);

plotFolder = fullfile(FullRadialScanFolder, '/Plots/LightSaberPlots');
dataFolder = fullfile(FullRadialScanFolder, '/Scripts/LightSaberBdata');

[radialShots, LightProbePos, RadialArray] = LightRadialShotnum(DataFolder, RadialScanFolder);
RadialArray = RadialArray(end-2:end,:);

for n = 1:length(RadialArray)
%     n = n+3
    shot = RadialArray(n,1);
    probePosition = RadialArray(n,3);
    [B, dB, Zvec, daq, Bh, SampleRange] = saberPlotBland(shot, probePosition);
    plotFile = fullfile(plotFolder, sprintf('Saber%d.png', shot));
    dataFile = fullfile(dataFolder, sprintf('SaberBdata%d.mat', shot));
    saveas(gcf, plotFile)
    save(dataFile, 'shot','B', 'dB', 'Zvec', 'daq','Bh','SampleRange','probePosition')
end
