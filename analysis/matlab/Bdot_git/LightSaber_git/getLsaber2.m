
function [BzIn,BrIn,BpIn,dBzIn,dBrIn,dBpIn,Zvec] = getLsaber2(daq,Bh,II, IIm,IIoff,dt)
% 
% WithMcoil=59137:59148;
% 
%   TfOnShotsUse=59000+[724:751  755:801];
% 
% shotnum=  59760;
%shotnum=  WithMcoil(1);
%shotnum=OnlyOuterCoils(1);
%shotnum=OnlyInnerCoils(end);
%shotnum=TfOnShotsUse(38);
 %  com=['load ''C:\Users\Jan Egedal\LocalWork\Matmac\2023\TREX\Calibrate\shots\shot' num2str(shotnum) ''''];
%   com=['load ''C:\Users\Jan Egedal\LocalWork\Matmac\2023\TREX\BgShots\shot' num2str(shotnum) ''''];
%eval(com)

II1=-0; % with these indices we can move the time between the digitizers; some times needed
II2=0;
II3=0;
II4=-0;


IIcal2=1160; %makes sure that Bz is smooth at IIcal2 and IIcal1
IIcal1=1080;



% IIm=901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% IIm=1:length(II);

% dt=1e-7; % time between samples
% IIoff=1000;  % time where we force B-components to zero (before the reconnection pulse)

%facZ=3.25e4/1.35; % calibration factors
facZ=3.25e4/1.6; % calibration factors
facR=facZ;
facP=facZ;






%dB1=daq.a113(II,:);
%dB1=daq.a113;

dB1=daq.a323(II+II1,:);
dB2=daq.a324(II+II2,:);
dB3=daq.a325(II+II3,:);
dB4=daq.a326(II+II4,:);


for k=1:32
    dB1(:,k)=dB1(:,k)-mean(dB1(IIm,k));
    dB2(:,k)=dB2(:,k)-mean(dB2(IIm,k));
    dB3(:,k)=dB3(:,k)-mean(dB3(IIm,k));
    dB4(:,k)=dB4(:,k)-mean(dB4(IIm,k));
end

temp=dB4(:,25);
dB4(:,25)=dB4(:,23);
dB4(:,23)=temp;


Zvec=linspace(0.0137, 0.7747+0.0345, 4*16);
Zvec=[-Zvec(end:-1:1) Zvec]; % these are the z-locations where we have probes. 

IIz1a=2:4:(4*16); % indices for first set of Bz-probes for negative z
IIz1b=(3:4:(4*16))+4*16;  % same for positive z

IIra=3:4:(4*16); % indices for Br-probes for negative z
IIrb=(2:4:(4*16))+4*16; % same for positive z

IIz2a=4:4:(4*16); % indices for first second set of Bz-probes
IIz2b=(1:4:(4*16))+4*16;

IIpa=1:4:(4*16); % indices for Bp-probes
IIpb=(4:4:(4*16))+4*16;

IIza(1:2:32)=IIz1a; %indices for all Bz-probes for negative z
IIza(2:2:32)=IIz2a;
IIzb(2:2:32)=IIz1b; %indices for all Bz-probes for positive z
IIzb(1:2:32)=IIz2b;


dBz=repmat(0,[length(II), 8*16]);
dBr=repmat(0,[length(II), 8*16]);
dBp=repmat(0,[length(II), 8*16]);


% figure(101),clf
% plot(Zvec)

dBz(:,IIz1a)= -dB2(:,1:16);
dBz(:,IIz1b)= dB4(:,16:(-1):1);
dBz(:,IIz2a)= -dB1(:,1:16);
dBz(:,IIz2b)= dB3(:,16:(-1):1);

dBz(:,IIz1a(7))=dBz(:,IIz1a(7))*1.3;




dBp(:,IIpa)=dB1(:,17:32);
dBp(:,IIpb)=dB3(:,32:(-1):17);
dBr(:,IIra)=dB2(:,17:32);
dBr(:,IIrb)=-dB4(:,32:(-1):17);



dBz=dBz*facZ;
dBr=dBr*facR;
dBp=dBp*facP;



ratR = [     0.1064    0.0796    0.0574    0.0525    0.0539    0.0409    0.0345    0.0276    0.0367    0.0439    0.0310    0.0190    0.0400 ...
    0.0193    0.0054    0.0326   -0.0282   -0.0459   -0.0608   -0.0484   -0.0369   -0.0393   -0.0377   -0.0563   -0.0465   -0.0360 ...
    -0.0429   -0.0188   -0.0620   -0.0507   -0.0637   -0.0489];


ratP = [   -0.0018    0.0049    0.0120   -0.0015    0.0221    0.0464    0.0291    0.0470    0.0368    0.0256    0.0358    0.0496    0.0354 ...
          0.0339    0.0456    0.0478    0.0479    0.0518    0.0605    0.0439    0.0350    0.0265    0.0257    0.0332    0.0399    0.0215 ...
    0.0337    0.0199    0.0333    0.0708    0.0397    0.0415 ];



 IIuz=[IIz1a IIz1b];
 IIur=[IIra IIrb];
 IIup=[IIpa IIpb];

for k=1:32
    dBr(:,IIur(k))=dBr(:,IIur(k)) - ratR(k)*dBz(:,IIuz(k));
    dBp(:,IIup(k))=dBp(:,IIup(k)) - ratP(k)*dBz(:,IIuz(k));
end


% calibrate Bz using Bz(1200,IIu)-Bz(1070,IIu) of vacuum shot 59148, with probe just underneath the edge of the cylinder
IIg=[1 3 5 7 9 11 13 15 17:20 22:31    34  36 38 40 42 48 50 52 54 56 58 62 64 ];

faa=[ 0.9905*1.05    1.0067    1.0224    1.0140    1.0052    1.0322    1.0177    0.9658    1.0067    0.9964    0.9907    0.9990 ...
   0.9914    1.0126    1.0002    0.9912    0.9847    1.0078    1.0044    1.0336    1.0001    0.9741    0.9819    1.0139 ...
    0.9906    0.9636    0.9884    1.0214    0.9642    1.0096    1.0128    0.9984    1.0040*1.02    1.0038*1.1  1.1 ];

IIu=[IIza IIzb];
for k=1:length(IIg)
    dBz(:,IIu(IIg(k)))=dBz(:,IIu(IIg(k)))/faa(k);
end
Bz=cumsum(dBz)*dt;
for k=1:128
    Bz(:,k)=Bz(:,k)-Bz(IIoff,k);
end



%fac=Bz(1200,IIu(IIg))-Bz(1070,IIu(IIg));
fac=Bz(IIcal2,IIu(IIg))-Bz(IIcal1,IIu(IIg));

%fac(3)=10;

% figure(10),clf
% plot(fac)
% hold on
facsm=fac;
for k=1:3
facsm=smoo(facsm,2)';
facsm([1,end])=fac([1, end]);
end
%plot(facsm)

fac=fac./facsm;
for k=1:length(IIg)
    dBz(:,IIu(IIg(k)))=dBz(:,IIu(IIg(k)))/fac(k);
end
Bz=cumsum(dBz)*dt;
for k=1:128
    Bz(:,k)=Bz(:,k)-Bz(IIoff,k);
end



fac=Bz(IIcal1,IIu(IIg));  % the following is a fix for in-perfect integration during the initial ringing. 
facsm=fac;
for k=1:3
facsm=smoo(facsm,2)';
facsm([1,end])=fac([1, end]);
end
 mult=[zeros([1 IIoff]) linspace(0,1,IIcal1-IIoff) ones([1 size(Bz,1)-IIcal1])];
 for k=1:length(IIg)
    Bz(:,IIu(IIg(k)))=Bz(:,IIu(IIg(k))) +mult'*(facsm(k)-fac(k));
end

IIu=[IIza IIzb];
IIuz=IIu(IIg);
for k=1:length(IIg)
    dBz(:,IIuz(k))=dBz(:,IIuz(k))/faa(k);
end





% fac=Bz(IIcal2,IIu(IIg))-Bz(IIcal1,IIu(IIg));
% 
% plot(fac,'--k')



    angs= [-2.2484   -6.3784   -6.7567   -8.9430   21.1805   24.3873   22.4539   19.8053   21.2105   21.9717   23.1710   24.3176 ...
   26.3986   24.1380   22.4465   20.1591   26.2113   26.4591   26.9538   25.9849   21.7312   19.6112   16.3330   16.3628...
   19.3495   16.9886   15.4292   11.9960   12.0777    6.4236   -2.1710    2.1975];
 
cosPR=cosd(angs);
sinPR=sind(angs);


dBRnew=dBr;
dBPnew=dBp;
for k=1:32
    dBRnew(:,IIur(k))=cosPR(k)*dBr(:,IIur(k)) +  sinPR(k)*dBp(:,IIup(k));
    dBPnew(:,IIup(k))=-sinPR(k)*dBr(:,IIur(k)) + cosPR(k)*dBp(:,IIup(k));
end
dBr=dBRnew;
dBp=dBPnew;


Br=cumsum(dBr)*dt;
Bp=cumsum(dBp)*dt;

for k=1:128
    Br(:,k)=Br(:,k)-Br(IIoff,k);
    Bp(:,k)=Bp(:,k)-Bp(IIoff,k);
end




fac=Br(IIcal1,IIur);  % the following is a fix for in-perfect integration during the initial ringing. 
facsm=fac;
for k=1:3
facsm=smoo(facsm,2)';
facsm([1,end])=fac([1, end]);
end
 mult=[zeros([1 IIoff]) linspace(0,1,IIcal1-IIoff) ones([1 size(Bz,1)-IIcal1])];
 for k=1:length(IIur)
    Br(:,IIur(k))=Br(:,IIur(k)) +mult'*(facsm(k)-fac(k));
 end


IIg=[1:15 17:20 22:32];
IIup=[IIpa IIpb];
IIup=IIup(IIg);
 
fac=Bp(IIcal1,IIup);  % the following is a fix for in-perfect integration during the initial ringing. 
facsm=fac;
for k=1:3
facsm=smoo(facsm,2)';
facsm([1,end])=fac([1, end]);
end
 mult=[zeros([1 IIoff]) linspace(0,1,IIcal1-IIoff) ones([1 size(Bz,1)-IIcal1])];
 for k=1:length(IIup)
    Bp(:,IIup(k))=Bp(:,IIup(k)) +mult'*(facsm(k)-fac(k));
end





%%% some systematic noise on B
for k=1:2
sBz=smoo(Bz(:,IIuz)',2)';
ssBz=Bz(:,IIuz)-sBz;
ssBz=smoo(ssBz,35);
Bz(:,IIuz(4:(end-3)))=Bz(:,IIuz(4:(end-3)))-ssBz(:,4:length(IIuz)-3);
sBr=smoo(Br(:,IIur)',2)';
ssBr=Br(:,IIur)-sBr;
ssBr=smoo(ssBr,35);
Br(:,IIur(4:(end-3)))=Br(:,IIur(4:(end-3)))-ssBr(:,4:length(IIur)-3);
sBp=smoo(Bp(:,IIup)',2)';
ssBp=Bp(:,IIup)-sBp;
ssBp=smoo(ssBp,35);
Bp(:,IIup(4:(end-3)))=Bp(:,IIup(4:(end-3)))-ssBp(:,4:length(IIup)-3);

end



%%
%%%%%%%%%%%%%% interpolate in z-direction
IIg=[1 3 5 7 9 11 13 15 17:20 22:31 34 36 38 40 42 48 50 52 54 56 58 62 64]; % good probe channels in the z-direction
IIu=[IIza IIzb];
IIu=IIu(IIg);
IIuz=IIu; % good indices for z-direction
IIuzP1=[1 IIuz 128];  % to be used in z-vec for extrapolation
IIuzP2=[IIuz(1)  IIuz IIuz(end)];  % to be used in dBz for extrapolation
IIt=1:length(II);
dBzIn=dBz*0;
BzIn=Bz*0;
for nt=IIt
dBzIn(nt,:)=interp1(Zvec(IIuzP1),dBz(nt,IIuzP2),Zvec);
BzIn(nt,:)=interp1(Zvec(IIuzP1),Bz(nt,IIuzP2),Zvec);
end

%%%%%%%%%%%%%% interpolate in r-direction
%IIg=[1 3 5 7 9 11 13 15 17:20 22:31    34  36 38 40 42 48 50 52 54 56 58 62]; % good probe channels in the z-direction
IIu=[IIra IIrb]; % all channels are good
%IIu=IIu(IIg);
IIur=IIu; % good indices for r-direction
IIurP1=[1 IIur 128];  % to be used in z-vec for extrapolation
IIurP2=[IIur(1)  IIur IIur(end)];  % to be used in dBr for extrapolation
IIt=1:length(II);
dBrIn=dBr*0;
BrIn=Br*0;
for nt=IIt
dBrIn(nt,:)=interp1(Zvec(IIurP1),dBr(nt,IIurP2),Zvec);
BrIn(nt,:)=interp1(Zvec(IIurP1),Br(nt,IIurP2),Zvec);
end


%%%%%%%%%%%%%% interpolate in r-direction
IIg=[1:15 17:20 22:32];  % good probe channels in the p-direction
IIu=[IIpa IIpb];
IIu=IIu(IIg);
IIup=IIu; % good indices for p-direction
IIupP1= IIup;  % to be used in z-vec for extrapolation
IIupP2= IIup;  % to be used in dBp for extrapolation
IIt=1:length(II);
dBpIn=dBp*0;
BpIn=Bp*0;
for nt=IIt
dBpIn(nt,:)=interp1(Zvec(IIupP1),dBp(nt,IIupP2),Zvec);
BpIn(nt,:)=interp1(Zvec(IIupP1),Bp(nt,IIupP2),Zvec);
end

BzIn=BzIn-Bh;


