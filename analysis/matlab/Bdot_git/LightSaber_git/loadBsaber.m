% load lightsaber probe data.  Change the value of PlotRaw to 1 to generate
% a plot of the raw digitizer data
% created Jan23


function [B, dB, tpoints, ProbePos , SampleRange, Bh] = loadBsaber(shot) 
% clear
% shot = 58875;

PlotRaw = 0; % change to 1 for plot of digitizer data
[daq, Bh] = loadDtaqData(shot); % load data from harddrive or from mdsplus
SampleRange=64001:95500;% Data Range
%IIm=901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIm=1:length(SampleRange);
IIoff=1000;  % time where we force B-components to zero (before the reconnection pulse)

dt=1e-7; % time between samples


facZ=3.25e4/1.25; % calibration factors
facR=facZ;
facP=facZ;

boardL1 = daq.a323(SampleRange,1:16); % 3 direction
boardL2 = daq.a323(SampleRange,17:32);% 2 direction
boardL3 = daq.a324(SampleRange,1:16);% 3 direction
boardL4 = daq.a324(SampleRange,17:32); % 1 direction

boardR1 = daq.a325(SampleRange,1:16);% 3 direction
boardR2 = daq.a325(SampleRange,17:32); % 2 direction
boardR3 = daq.a326(SampleRange,1:16);% 3 direction
boardR4 = daq.a326(SampleRange,17:32);% 1 direction

boards = {boardL1, boardL2, boardL3, boardL4, boardR1, boardR2, boardR3, boardR4};

%Plot Raw digitizer data from each board
if PlotRaw == 1
    figure(81), clf
    subplot(1,8,1)
    plot(boards{1})
    title('Board L1')
    hold on
    for i = 2:8
        %figure(i), clf
        subplot(1,8,i)
        plot(boards{i})
        if i<5
            title(sprintf('Board L%d',i))
        else
            title(sprintf('Board R%d',i-4))
        end
    end
end

% combine boards and flip right board probes
dBr = -[-boardL4 boardR4(:,16:(-1):1)]; % r direction was determined from presence of B at edges 
dBp = [boardL2 boardR2(:,16:(-1):1)]; % negative sign due to coil orintation changing relative to axis
dBz = [boardL3 -boardR3(:,16:(-1):1)];% negative to match Jan's code

% fix bad probes
dBp(:,16) = (dBp(:,15) + dBp(:,17))/2;
dBp(:,21) = (dBp(:,20) + dBp(:,22))/2; 
dBz(:,11) = (boardL1(:,10) + boardL1(:,12))/2;

% fix the attenuation in the dBp channels
dBp(:,[15 17 18])=dBp(:,[15 17 18])*1.15;
dBp(:, 17 )=dBp(:, 17 )*1.3;

% swap channels on boardR4
dB1_25 = dBr(:,26);
dBr(:,26) = dBr(:,24);
dBr(:,24) = dB1_25;

tpoints = (1:length(dBz))*dt*1e6; % time array in ms

%location of probes
dB1posR = 1.37:5.08:77.57;
dB1posL = flip(-dB1posR);
dB1pos = [dB1posL dB1posR];
dB2pos = [dB1posL+1 dB1posR+1];
dB3pos = [dB1posL+2.5 dB1posR+2.5];
dB4pos = [dB1posL+3.45 dB1posR+3.45];

dBzpos = dB1pos;
dBrpos = dB4pos;
dBppos = dB2pos;

ProbePos = {dBzpos dBrpos dBppos}; % collection of all probe locations

dBz=-dBz*facZ;
dBr=dBr*facR;
dBp=dBp*facP;

% Correcting for the Br component that is in the Bz component
% ratio of dBr/dBz at R = 2.2cm
ratR = [    0.1065    0.0796    0.0574    0.0525    0.0539    0.0409    0.0345    0.0276    0.0367    0.0439    0.0412    0.0190    0.0400    0.0193 ...
    0.0054    0.0326   -0.0282   -0.0459   -0.0608   -0.0484   -0.0369   -0.0393   -0.0378   -0.0564   -0.0465   -0.0360   -0.0429   -0.0188 ...
   -0.0620   -0.0508   -0.0637   -0.0489 ];


% ratio of dBp/dBz at R = 2.2cm
ratP = [       -0.0019    0.0049    0.0120   -0.0015    0.0221    0.0464    0.0291    0.0471    0.0368    0.0256    0.0359    0.0496    0.0354    0.0339 ...
    0.0397    0.0368    0.0321    0.0451    0.0605    0.0439    0.0351    0.0265    0.0257    0.0332    0.0399    0.0215    0.0338    0.0199 ...
    0.0333    0.0708    0.0397    0.0415 ];

% subtracting correction calculated at R =2.2
for k=1:32
    dBr(:,k)=dBr(:,k) - ratR(k)*dBz(:,k); 
    dBp(:,k)=dBp(:,k) - ratP(k)*dBz(:,k);
end
% ratio of dBp/dBr at R = 40cm
ratPR = [ -0.2242   -0.1572   -0.1045   -0.2312    0.1613    0.2804    0.1599   -0.0336   -0.0996   -0.3012*0   [0.3961*0.5    0.2994*0.5    0.3293]   ...
  [   0.2326    0.1928    0.3470    0.4627    0.3481    0.2860    0.2657    0.2413        ] ...
     0.1384    0.1755   -0.1607      0.2577    0.3396   0.4093    0.1691    0.2071    0.1379    0.0438    0.1113 ];

angle = atan(ratPR); % calculating the angle between dBr and dBp

% componetents of the rotation matrix
cosPR=cos(angle);
sinPR=sin(angle);

dBRnew=dBr;
dBPnew=dBp;

% Rotating dBr and dBp such that there is no dBp in vacuum shots
for k=1:32
    dBRnew(:,k)=cosPR(k)*dBr(:,k) + sinPR(k)*dBp(:,k);
    dBPnew(:,k)=-sinPR(k)*dBr(:,k) + cosPR(k)*dBp(:,k);
end

dBr=dBRnew;
dBp=dBPnew;

% Calculating B
Bz=cumsum(dBz)*dt;
Br=cumsum(dBr)*dt;
Bp=cumsum(dBp)*dt;

%substracting off the digitizer 
for k=1:32
    Bz(:,k)=Bz(:,k)-Bz(IIoff,k);
    Br(:,k)=Br(:,k)-Br(IIoff,k);
    Bp(:,k)=Bp(:,k)-Bp(IIoff,k);
end

Bz=Bz-Bh; % adding helmholz field

% Smooth Data
sBz=smoo(Bz',4)';
ssBz=Bz-sBz;
ssBz=smoo(ssBz,75);
Bz=Bz-ssBz;
sBr=smoo(Br',4)';
ssBr=Br-sBr;
ssBr=smoo(ssBr,75);
Br=Br-ssBr;
sBp=smoo(Bp',4)';
ssBp=Bp-sBp;
ssBp=smoo(ssBp,75);
Bp=Bp-ssBp;

B = {Bz Br Bp};
dB = {dBz dBr dBp};

