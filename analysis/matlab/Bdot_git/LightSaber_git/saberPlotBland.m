% Plot lightsaber data using loadBsaber
% created 07Apr23

% function [B, dB, Zvec, daq, Bh, SampleRange] = saberPlotBland(shot, SaberPosition)
clear
shot = 60033;
[~, shotData] = AllShotData(shot);  
SaberPosition = round(shotData(1,4))*1e-2;


% %[B, dB, tpoints, ProbePos , range, Bh] = loadBsaber(shot);

SampleRange=64001:95500;% Data Range
IIm=901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIoff=1000;  % time where we force B-components to zero (before the reconnection pulse)
dt = 1/(10e6);
[daq, Bh] = loadDtaqData(shot); % load data from harddrive or from mdsplus
[BzIn,BrIn,BpIn,dBzIn,dBrIn,dBpIn,Zvec] = getLsaber2(daq,Bh,SampleRange, IIm,IIoff,dt);

B = {BzIn BrIn BpIn};
dB = {dBzIn dBrIn dBpIn};

tpoints = (1:length(SampleRange))*dt*1e6;
%tpoints = tpoints';

[CT, ~, ~, noData] = ExcelShotData(shot);
if noData ==1
    CT = NaN; 
end
%%
%Plotting parameters
ylims=[1030 1200];
caxi=[-1 1]*1e4*0.5; % pcolor limits for dB
tlims = tpoints(ylims);
coord = {'z' 'r' 'p'};
cAxes = {[-10 10] [-4 4] [-4 4]}; % pcolor limits for B

% B field Plots

figure(55), clf
sgtitle(sprintf(['Lightsaber Probe, Shot %d, ' ...
    'B_h = %dG, CT = %dkV, R = %0.2fm'], [shot round(Bh*1e4) CT SaberPosition]))
k = 0;
for i = 2:2:6
    k = k+1;
    subplot(3,2,i)
    pcolor(Zvec,tpoints,B{k}*1e3),shading interp
    hold on
%     if k == 1
        contour(Zvec,tpoints,B{1}*1e3,[0 0],'k'),shading interp
%     end

    colorbar
    clim(cAxes{k})
    ylim(tlims)
    title(sprintf('B%s',coord{k}))
    xlabel('z cm')
    ylabel('t \mus')
end

% Bdot Plots
jj = 0;
for ii = 1:2:5
    jj = 1+jj;
    subplot(3,2,ii)
    pcolor(Zvec,tpoints,dB{jj}),shading interp
    hold on
%     if jj == 1
        contour(Zvec,tpoints,B{1}*1e3,[0 0],'k'),shading interp
%     end
    cb = colorbar();
    cb.Ruler.Exponent = 3; % set the colorbar to display scientific notation
    %colorbar
    clim(caxi)
    ylim(tlims)
    title(sprintf('dB%s',coord{jj}))      
    xlabel('z cm')
    ylabel('t \mus')
end
set(gcf, 'Position',  [100, 100, 1000, 900])

