
clear
DataFolder = '/Users/paulgradney/Research/Data/';
TempPlotFolder = 'Temporary/Plots/SaberPlots';
DriveVoltScanFolder = '/Drive_Voltage_Scan/Plots/Lightsaber';
SaberScanFolder = 'TeSaber_Radial_Scan';
plotFolder = fullfile(DataFolder, DriveVoltScanFolder);

% Shotfolder = '/No_Perturbation_NoMiddle/';
% Shotfolder = '/No_Perturbation_AllCoils/';
% Shotfolder = '/No_Perturbation/';
Shotfolder = '/Drive_Voltage_Scan';
%Shotfolder = 'No_Perturbation_NoMiddle/withSaber';

[shotnum, ~] = collectShotNum(DataFolder, Shotfolder);
% shotnum(1:170) =[];
%%
for shot = shotnum
    Rinput = 0.3;
%     speedPlot(shot,-1); % Use for vacuum shots
%     speedPlot_vel(shot, 0, 1, Rinput); % speedProb_vel(shot, PoinIn, plotVel, plotBdot, Rinput)
%     shepPlot(shot);
%     linPlot(shot);
    if shot>58870
%     [CT, TREX, Bh, noData, excelShot] = ExcelShotData(shot);   
    saberPlotBland(shot)
    plotFile = fullfile(plotFolder, sprintf('Saber%d.png', shot));
    saveas(gcf, plotFile)
    end
%           GetTeData(shot);
%          langmuir_fitScan_working(shot, Rinput);
%          density_inTime_working(shot,Rinput); %density_inTime_working(shot,Rinput)
%     
end
