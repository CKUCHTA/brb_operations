% Plot lightsaber data using loadBsaber
% Created Jan23

% function saberPlot(shot)
clear
shot = 60153;

SampleRange=64001:95500;% Data Range
IIm=901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIoff=1000;  % time where we force B-components to zero (before the reconnection pulse)
dt = 1/(10e6);
[daq, Bh] = loadDtaqData(shot); % load data from harddrive or from mdsplus
[BzIn,BrIn,BpIn,dBzIn,dBrIn,dBpIn,Zvec] = getLsaber2(daq,Bh,SampleRange, IIm,IIoff,dt);

B = {BzIn BrIn BpIn};
dB = {dBzIn dBrIn dBpIn};

tpoints = (1:length(SampleRange))*dt*1e6;

pos1 = 65; %closest Zvec location to zero
pos2 = 85;
pos3 = 45;

[CT, ~, ~, noData] = ExcelShotData(shot);
if noData ==1
    CT = NaN; 
end

%Plotting parameters
ylims=[1030 1200];
caxi=[-1 1]*1e4; % pcolor limits for dB
tlims = tpoints(ylims);
coord = {'z' 'r' 'p'};
cAxes = {[-10 10] [-4 4] [-4 4]}; % pcolor limits for B

% B field Plots
figure(55), clf
sgtitle(sprintf(['Lightsaber Probe, Shot %d, ' ...
    'B_h = %dG, CT = %dkV'], [shot round(Bh*1e4) CT]))
k = 0;
for i = 7:12:31
    k = k+1;
    subplot(3,12,[i i+4])
    pcolor(Zvec,tpoints,B{k}*1e3),shading interp
    hold on
%     if k == 1
        contour(Zvec,tpoints,B{1}*1e3,[0 0],'k'),shading interp
%     end
    plot([Zvec(pos1) Zvec(pos1)],tlims,'-b')
    plot([Zvec(pos2) Zvec(pos2)],tlims,'-r')
    plot([Zvec(pos3) Zvec(pos3)],tlims,'-g')
    colorbar
    clim(cAxes{k})
    ylim(tlims)
    title(sprintf('B%s',coord{k}))
    xlabel('z cm')
    ylabel('t \mus')
end

% Bdot Plots
jj = 0;
for ii = 1:12:35
    jj = 1+jj;
    subplot(3,12,[ii ii+4])
    pcolor(Zvec,tpoints,dB{jj}),shading interp
    hold on
%     if jj == 1
        contour(Zvec,tpoints,B{1}*1e3,[0 0],'k'),shading interp
%     end
    plot([Zvec(pos1) Zvec(pos1)],tlims,'-b')
    plot([Zvec(pos2) Zvec(pos2)],tlims,'-r')
    plot([Zvec(pos3) Zvec(pos3)],tlims,'-g')
    cb = colorbar();
    cb.Ruler.Exponent = 3; % set the colorbar to display scientific notation
    %colorbar
    clim(caxi)
    ylim(tlims)
    title(sprintf('dB%s',coord{jj}))      
    xlabel('z cm')
    ylabel('t \mus')
end

%B plots at specified probe locations
sampLim = ylims(1):ylims(2);
timeLim = tpoints(sampLim);

% Individual B-field Plots
ii = 0;
for k = 12:12:36
    ii = 1+ii;
    subplot(3,12,k)
    plot(B{ii}(sampLim,pos1)*1e3,timeLim,'b')
    hold on
    plot(B{ii}(sampLim,pos2)*1e3,timeLim,'r')
    plot(B{ii}(sampLim,pos3)*1e3,timeLim,'g')
    xline(0,'--k')
    ylim(tlims)
    xlabel(sprintf('B_%s mT', coord{ii}))
end

% Individual dB Plots
ii = 0;
for k = 6:12:30
    ii = 1+ii;
    subplot(3,12,k)
    plot(dB{ii}(sampLim,pos1),timeLim,'b')
    hold on
    plot(dB{ii}(sampLim,pos2),timeLim,'r')
    plot(dB{ii}(sampLim,pos3),timeLim,'g')
    xline(0,'--k')
    ylim(tlims)
    xlabel(sprintf('dB_%s', coord{ii}))
end
set(gcf, 'Position',  [100, 100, 1000, 900])

%%
% single B-Plot
BPlot = figure(22);
clf
deltaTime = (timeLim/tlims(1)-1)*1e2;

plot(deltaTime, abs(-B{1}(sampLim,pos1)*1e3-Bh*1e3),'k', 'linewidth', 2)
yline(0,'--k')
ylim([-2 32])
xlim([0 deltaTime(end)])
ylabel(sprintf('B_%s [mT]', coord{1}))
xlabel('\Deltat [\mus]')
fontsize(BPlot, scale=3)
