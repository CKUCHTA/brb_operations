function PltsaberData (shot)
% clear
% shot = 60108;

SampleRange=64001:95500;% Data Range
IIm=901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIoff=1000;  % time where we force B-components to zero (before the reconnection pulse)
dt = 1/(10e6);
[daq, Bh] = loadDtaqData(shot); % load data from harddrive or from mdsplus
[BzIn,BrIn,BpIn,dBzIn,dBrIn,dBpIn,Zvec] = getLsaber2(daq,Bh,SampleRange, IIm,IIoff,dt);

% 
%% 
figure(102),clf
caxi=[-1 1]*1e4*1.5;
caxi2=[-10 10]*1.2;
ylims=[1040 1240];

H1=subplot(2,3,1);
pcolor(Zvec,1:length(SampleRange),dBzIn),shading interp
hold on
contour(Zvec,1:length(SampleRange),BzIn,[0 0],'--k'),shading interp

% colormap('jet')
colorbar
ylim(ylims)
% caxis(caxi*1.5)

title('dB_z')



subplot(2,3,4)

pcolor(Zvec,1:length(SampleRange),BzIn*1e3),shading interp
hold on
contour(Zvec,1:length(SampleRange),BzIn,[0 0],'--k'),shading interp

colorbar
caxis(caxi2)
ylim(ylims)
title('B_z')




subplot(2,3,2)
pcolor(Zvec,1:length(SampleRange),dBrIn),shading interp
hold on
contour(Zvec,1:length(SampleRange),BzIn,[0 0],'--k')

colorbar
ylim(ylims)
caxis(caxi)
title('dB_r')


subplot(2,3,3)
pcolor(Zvec,1:length(SampleRange),dBpIn),shading interp
hold on
contour(Zvec,1:length(SampleRange),BzIn,[0 0],'--k')
colorbar
ylim(ylims)
caxis(caxi)
title('dB_p')


subplot(2,3,5)
pcolor(Zvec,1:length(SampleRange),BrIn*1e3),shading interp
hold on
contour(Zvec,1:length(SampleRange),BzIn,[0 0],'--k')

colorbar
ylim(ylims)
caxis(caxi2/2)
title('B_r')



subplot(2,3,6)



pcolor(Zvec,1:length(SampleRange),BpIn*1e3),shading interp
hold on
contour(Zvec,1:length(SampleRange),BzIn,[0 0],'--k')

colorbar
ylim(ylims)
caxis(caxi2/2)
title('B_p')






