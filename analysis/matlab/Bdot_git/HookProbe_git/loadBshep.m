
function [Bz,dBz,Br,dBr,Bp,dBp1,dBp2,zAndp,tpoints, Bh] = loadBshep(shot, II) 

%[daq,AllBz, time, Bh] = acq_data2(shot); % Load Data from MDSPlus

[daq, Bh] = loadDtaqData(shot); % load data from harddrive or from mdsplus
IIm=901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIm=1:length(II);
dt=1e-7; % time between samples
IIoff=1000;  % time where we force B-components to zero (before the reconnection pulse)

facZ=3.25e4; % calibration factors
facR=facZ;
facP=facZ;
% Vlayer=50e3;
% facJ=1/1.2e-6/Vlayer/1e3; % mult into dB/dt to get current in kA/m^2


% define data matrices
dBp1=repmat(0,[length(II) 15]);
dBp2=repmat(0,[length(II) 15]);
dBr=repmat(0,[length(II) 15]);
dBz=repmat(0,[length(II) 15]);



% fill hook-probe data
dBr(:,1:2:15)=daq.a113(II,(1:2:15)+16);
dBr(:,2:2:14)=daq.a115(II,(1:2:13)+16);
dBz(:,1:2:15)=daq.a113(II,(2:2:16)+16);
dBz(:,2:2:14)=daq.a115(II,(2:2:14)+16);
dBp1(:,1:2:15)=daq.a113(II,(1:2:15));
dBp1(:,2:2:14)=daq.a115(II,(1:2:13));
dBp2(:,1:2:15)=daq.a113(II,(2:2:16));
dBp2(:,2:2:14)=daq.a115(II,(2:2:14));

dBz(:,5)=(dBz(:,4)+dBz(:,6))/2;
dBr(:,2)=(dBr(:,1)+dBr(:,3))/2;
dBr(:,7)=(dBr(:,6)+dBr(:,8))/2;
dBr(:,4)=(dBr(:,3)+dBr(:,5))/2;
dBr(:,10)=(dBr(:,9)+dBr(:,11))/2;
dBp1(:,3)=(dBp1(:,2)+dBp1(:,4))/2;


for k=1:15
    dBz(:,k)=dBz(:,k)-mean(dBz(IIm,k));
    dBr(:,k)=dBr(:,k)-mean(dBr(IIm,k));
    dBp1(:,k)=dBp1(:,k)-mean(dBp1(IIm,k));
    dBp2(:,k)=dBp2(:,k)-mean(dBp2(IIm,k));
end


dBz=-dBz*facZ;
dBr=dBr*facR;
dBp1=dBp1*facP;
dBp2=dBp2*facP;

Bz=cumsum(dBz)*dt;
Br=cumsum(dBr)*dt;
Bp=cumsum(dBp1)*dt;
for k=1:15
    Bz(:,k)=Bz(:,k)-Bz(IIoff,k);
    Br(:,k)=Br(:,k)-Br(IIoff,k);
    Bp(:,k)=Bp(:,k)-Bp(IIoff,k);
end
%Bz=Bz+Bh; % better to correct for Bh outside routine, as sign depends on
%angle of the hook probe


sBz=smoo(Bz',4)';
ssBz=Bz-sBz;
ssBz=smoo(ssBz,75);
Bz=Bz-ssBz;
sBr=smoo(Br',4)';
ssBr=Br-sBr;
ssBr=smoo(ssBr,75);
Br=Br-ssBr;
sBp=smoo(Bp',4)';
ssBp=Bp-sBp;
ssBp=smoo(ssBp,75);
Bp=Bp-ssBp;

sdBz=smoo(dBz',4)';
ssdBz=dBz-sdBz;
ssdBz=smoo(ssdBz,75);
dBz=dBz-ssdBz;
sdBr=smoo(dBr',4)';
ssdBr=dBr-sdBr;
ssdBr=smoo(ssdBr,75);
dBr=dBr-ssdBr;
sdBp=smoo(dBp1',4)';
ssdBp=dBp1-sdBp;
ssdBp=smoo(ssdBp,75);
dBp1=dBp1-ssdBp;

tpoints = (1:length(Bz))*dt*1e6; % time array in ms

p1 = 83.8 - 3.65; % probe 1 location (futhest from the equator)
p15 = 83.8 - 81.4; % probe 15 location (closest to equator)
probe_numbers=1:15;
zpoints = linspace(p1,p15,15);
zAndp = cat(1,probe_numbers,zpoints);



end

