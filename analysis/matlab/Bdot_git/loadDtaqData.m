% Load dtaq data from external harddrive or from mdsplus if data is not in
% external harddrive and save to Temporary folder

function [daq, Bh] = loadDtaqData(shot)
% clear
% shot = 59039;

DataFolder = '/Users/paulgradney/Research/Data';
TemporaryFolder = 'Temporary';

dtaqFile = strcat(num2str(shot),'.mat');
dtaqFilePath = which(dtaqFile); % path of TeData file

try
   load(dtaqFilePath, 'daq', 'Bh');
catch
    disp(['Could not find file: ', dtaqFile])
    % load data from mdsplus
    [daq, ~, ~, Bh] = acq_data2(shot); % load data from MDSPlus
    shot_data_file = sprintf('%d.mat',shot);
    fileName = fullfile(DataFolder, TemporaryFolder, shot_data_file);
    save(fileName)
end