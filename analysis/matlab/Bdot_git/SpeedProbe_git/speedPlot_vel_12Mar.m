% Plots Speed Probe data and calculates speed of current sheet using a 
% 3rd order polynomial fit of the minimum values from dBz.
% last updated 12Mar23

function [dBz, Bz, probeLoc, InputProbeLocMeters, probeInd, velAtRinput, time, SamRange, SpeedRange, SpeedBdotPlot, Xline] = speedPlot_vel(shot, plotV, plotBdot, Rinput)
clear
plotBdot = 1;
shot = 59990;
plotV = 1;
[~, shotData] = AllShotData(shot);
Rinput = round(shotData(1,4))*1e-2; % radial location of lighsaber probe [m]
% % Rinput = 0.05;

% index range that seperatix is evaluated over
% Xline = 5:11; % Middle On
Xline = 4:12;% Middle Off

% [Bz,dBz,Br,dBr,Bp,dBp,~,probeLoc,time,Bh,InitialSampleRange] = loadBspeed(shot);
[Bspeed, dBspeed, rvecS, SamRange, time, Bh, probeLoc] = getSpeedProbe(shot);

Bz = Bspeed{1};
dBz = dBspeed{1};

% Set the Sample range
SpeedRange = 2001:2250;
Bz=Bz(SpeedRange,:)';
dBz = dBz(SpeedRange,:)';
time = time(SpeedRange);

flux = BzFluxSpeed(Bz, probeLoc); %calculate flux

[~,probeInd] = min(abs(Rinput - probeLoc)); % find the probe number closest to BzRposition
BzprobeLocCM = round(probeLoc(probeInd),2)*100; % location of probe in cm closest to BzRposition
InputProbeLocMeters = BzprobeLocCM/100; % probe location where velocity is measured in meters

RprobePosInside = probeLoc(1:13); % probes inside drive cylinder
dBzMinInd = zeros(1, length(RprobePosInside));

% Find seperatrix from Minimum values of dB
% to account for causasilty, find minimum dBz of each probe after the time
% of the previous probe
for ii = length(RprobePosInside):-1:1
    if ii ==13
        [~, dBzMinInd(1,ii)] = max(-dBz(ii,:),[],2); % initial min dBz at largest R
    else
        ind = ii+1;
        PreviousdBInd = dBzMinInd(1,ind);
        [~, dBMaxIndPartial] = max(-dBz(ii,PreviousdBInd:end),[],2); % min dBz of next probe after time of preious probes minimum
        dBzMinInd(1,ii) = PreviousdBInd + dBMaxIndPartial; % index of next min dBz
    end
end

if dBzMinInd(1,1) > length(time)
    dBzMinInd(1,1) = length(time);
end
timedBzMin = time(dBzMinInd); % time dBzMin occur in mus


% Display warning if BzRpos is outside evaluation range
maxR = RprobePosInside(Xline(end));
if Rinput > maxR
    fprintf('Input value of %0.2f outside range for evaluating velocity \n', Rinput);
end

% 3rd order polynomial fit of dBmax values
timedBzMaxfit = timedBzMin';
rmaxfit = RprobePosInside';
fitfun = fittype(@(a, b, c, t) a*t.^2 + b*t + c, ...
    'independent','t', 'dependent', 'r');
[RofT, ~, ~] = fit(timedBzMaxfit(Xline) , rmaxfit(Xline), fitfun, 'StartPoint', [.001, .001,.001]);

% Velocity of current sheet along seperatrix based on dBmax
timeRangeX = linspace(timedBzMin(Xline(1)),timedBzMin(max(Xline)), 100);
dBzMaxPositionX = RofT(timeRangeX');
velX = differentiate(RofT, timeRangeX'); % Velocity of current sheet along seperatrix
velRposition = probeLoc(Xline); % radial position velocity is calculated along
timeCurrentSheetTravels = velRposition./velX;

% Velocity at BzRposition

[~, ClosestInd] = min(abs(dBzMaxPositionX-InputProbeLocMeters)); % closest value in Rloc to probe location
probTimeIndex = find(dBzMaxPositionX == dBzMaxPositionX(ClosestInd)); % index of closest value to probe location
probeTime = timeRangeX(probTimeIndex);
velAtRinput = abs(velX(probTimeIndex))*1e3; %velocity of current sheet at BzRposition

%plot parameters
tlims = [200 time(end)];

if plotV == 1
    figure(50);
    clf
    % Plot of Current sheet velocity
    subplot(2,1,1)
    plot(timeRangeX, velX*1e3)
    xline(probeTime)
    xlabel('t\mus')
    ylabel('Velocity [km/s]')
    
    % Plot of Current Sheet position
    subplot(2,1,2)
    plot(RofT, timedBzMin(Xline), RprobePosInside(Xline))
    xline(probeTime)
    xlabel('t\mus')
    ylabel('r [m]')
end

if plotBdot == 1
    SpeedBdotPlot = figure(51);
    clf
    SpeedBdotPlot.Position = [100, 100, 900, 700];
    sgtitle(sprintf('shot %d, B_h = %0.1f[mT], v = %0.1f[km/s], R = %0.0f[cm]', shot, Bh*1e3, velAtRinput, BzprobeLocCM))
    
    % Bz Plot at specified R values
    subplot(5,3,[1 3])
    plot(time,Bz(probeInd,:)*1e3, 'k','Linewidth', 2)
    yline(0, '--k')
    ylabel('B(mT)')
    %xlabel('t(\mus)')
    xlim(tlims)
    title(['Bz at R = ' num2str(BzprobeLocCM) ' [cm]'])
    
    % dBz Plot at specified R values
    subplot(5,3,[4 6])
    plot(time,dBz(probeInd,:)*1e3, 'k', 'Linewidth', 2)
    yline(0, '--k')
    ylabel('dB')
    %xlabel('t(\mus)')
    xlim(tlims)
    title(['dBz at R = ' num2str(BzprobeLocCM) ' [cm]'])
    
    % dBz color countor plot
    subplot(5,3,[7;13])
    pcolor(time, probeLoc*1e2, dBz), shading interp
    colorbar 
    title(num2str(shot))
    xlim(tlims)
    clim([-1 1]*1e4)
    hold on
    plot(timedBzMin,RprobePosInside*1e2,'r*')
    plot(timeRangeX, dBzMaxPositionX*100)
    ylabel('r(cm)')
    xlabel('t(\mus)')
    title('dB_z/dt [mT/s]')
    
    % Bz pcolor countor plot
    subplot(5,3,[8;14])
    pcolor(time, probeLoc*1e2, Bz*1e3), shading interp
    colorbar 
    hold on
    contour(time, probeLoc*1e2, Bz*1e3,[0 0 ],'k')
    clim([-25 25])
    xlim(tlims)
    %ylim(rarray(3),rarray(end))
    plot(timeRangeX, dBzMaxPositionX*100,'r','LineWidth',3)
    yline(BzprobeLocCM,'--r')
    ylabel('r(cm)')
    xlabel('t(\mus)')
    title1 = sprintf('B_z[mT]');
    title(title1)
    
    % Flux pcolr plot
    subplot(5,3,[9;15])
    pcolor(time, probeLoc*1e2,flux), shading interp
    colorbar
    hold on
    contour(time, probeLoc*1e2, Bz*1e3,[0 0 ],'k')
    title(num2str(shot))
    %     clim([-15 25]*1e-3)
    ylim(probeLoc([1 end])*1e2)
    plot(timeRangeX, dBzMaxPositionX*100,'r','LineWidth',3)
    yline(BzprobeLocCM,'--r')
    xlim(tlims)
    ylabel('r(cm)')
    xlabel('t(\mus)')
    title(sprintf('\\Psi(R,t) [mT/m^2]'))
else
    SpeedBdotPlot = 0;
end

    