
% Loads dB and B data using loadDtaqData
% Last updated (??) at least before Jan23

function [Bz,dBz,Br,dBr,Bp,dBp,dt,probeLoc,time,Bh,range] = loadBspeed(shot) 
% clear
% shot = 60363;


[daq, Bh] = loadDtaqData(shot); % load data from harddrive or from mdsplus
range = 63001:65250; %sample points that are choosen from full data set
Bh = double(Bh); % Set helholtz field value as double persition

IIm=101:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
dt=1e-7; % time between samples
IIoff=1000;  % time where we force B-components to zero (before the reconnection pulse)

AllBz=0;
%facZ=3.25e4; % old calibration factors
facZ=3.25e4/1.6; % calibration factors
facR=facZ;
facP=facZ;

% define data matrices
dBz=zeros([length(range) 16]);
dBz2=zeros([length(range) 16]);
dBr=zeros([length(range) 16]);
dBp=zeros([length(range) 16]);


if AllBz==0
    dBz(:,1:2:15)=daq.a114(range,1:2:15);
    dBz(:,2:2:16)=daq.a129(range,1:2:15);
    dBz(:,14)=daq.a129(range,14);
    
    dBz2(:,1:2:15)=daq.a114(range,2:2:16);
    dBz2(:,2:2:16)=daq.a129(range,2:2:16);
    dBz2(:,10)=daq.a129(range,9);
    
    
    dBr(:,1:2:15)=daq.a114(range,(2:2:16)+16);
    dBr(:,2:2:14)=daq.a129(range,(2:2:14)+16);
    
    dBp(:,1:2:15)=daq.a114(range,(1:2:15)+16);
    dBp(:,2:2:14)=daq.a129(range,(1:2:13)+16);
end

for k=1:16
    dBz(:,k)=dBz(:,k)-mean(dBz(IIm,k));
    dBz2(:,k)=dBz2(:,k)-mean(dBz2(IIm,k));
    dBr(:,k)=dBr(:,k)-mean(dBr(IIm,k));
    dBp(:,k)=dBp(:,k)-mean(dBp(IIm,k));
end

dBp(:,1)=dBp(:,2);
dBp(:,11)=(dBp(:,10)+dBp(:,12))/2;

dBz=-dBz*facZ; % Multiplying by Negative to compare with Joe's paper
dBz2=-dBz2*facZ; % Multiplying by Negative to compare with Joe's paper
dBr=dBr*facR;
dBp=dBp*facP;

Bz=cumsum(dBz)*dt;
Bz2=cumsum(dBz2)*dt;
Br=cumsum(dBr)*dt;
Bp=cumsum(dBp)*dt;

for k=1:16
    Bz(:,k)=Bz(:,k)-Bz(IIoff,k);
    Bz2(:,k)=Bz2(:,k)-Bz2(IIoff,k);
    Br(:,k)=Br(:,k)-Br(IIoff,k);
    Bp(:,k)=Bp(:,k)-Bp(IIoff,k);
end
 
Bz=(Bz+Bz2)/2; % combine Bz and Bz2 by averaging them
dBz=(dBz+dBz2)/2;
try
    Bz=Bz+Bh; % add Helmholtz Field to Bz
catch
    Bh = mean(Bh);
    Bz = Bz+Bh;
end
% calculate radial positions and time axis
time = (1:length(Bz))*dt*1e6; % time array in ms
probeLoc = linspace(-0.066, 0.734, 16); % r position of probes (m)  

 



