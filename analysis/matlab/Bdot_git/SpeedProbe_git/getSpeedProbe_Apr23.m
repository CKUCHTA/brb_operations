% Update to loadBspeed.
% Created 01Apr23

function [BzS,BrS,BpS,dBzS,dBrS,dBpS,rS] = getSpeedProbe_Apr23(daq,Bh,II, IIm,IIoff,dt)
%UNTITLED Summary of this function goes here
% 


rS = linspace(-0.066, 0.734, 16); 


II1=-0; % with these indices we can move the time between the digitizers; some times needed
II2=0;
II3=0;
II4=-0;


IIcal2=1160; %makes sure that Bz is smooth at IIcal2 and IIcal1
IIcal1=1080;



% IIm=901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% IIm=1:length(II);

% dt=1e-7; % time between samples
% IIoff=1000;  % time where we force B-components to zero (before the reconnection pulse)

facZ=3.25e4/1.35; % calibration factors
facZ=3.25e4/1.6; % calibration factors
facR=facZ;
facP=facZ;




dBzS=repmat(0,[length(II) 16]);
dBrS=repmat(0,[length(II) 16]);
dBpS=repmat(0,[length(II) 16]);


dBzS(:,1:2:15)=daq.a114(II,1:2:15);
dBzS(:,2:2:16)=daq.a129(II,1:2:15);
dBzS(:,14)=daq.a129(II,14);
dBrS(:,1:2:15)=daq.a114(II,(2:2:16)+16);
dBrS(:,2:2:14)=daq.a129(II,(2:2:14)+16);
dBpS(:,1:2:15)=daq.a114(II,(1:2:15)+16);
dBpS(:,2:2:14)=daq.a129(II,(1:2:13)+16);

for k=1:15
    dBzS(:,k)=dBzS(:,k)-mean(dBzS(IIm,k));
    dBrS(:,k)=dBrS(:,k)-mean(dBrS(IIm,k));
    dBpS(:,k)=dBpS(:,k)-mean(dBpS(IIm,k));
end

 dBpS(:,1)=dBpS(:,2);
 dBpS(:,11)=(dBpS(:,10)+dBpS(:,12))/2;
 
ratR=[ 0.0281 0.1016 0.0599  0.1315  0.1160  0.0589  0.0436  0.0209  0.0458  0.0337  -0.0306  0.0175  0.3168  0.5723  -0.1216   0];
ratP=[ -0.0727 -0.0727 -0.0434 0.0549 -0.0428  0.0428  0.0179 -0.0918 -0.1090 -0.1814 -0.1289  -0.0756  -0.1468  0.8317 -0.1470  0];
 for k=1:11
    dBrS(:,k)=dBrS(:,k) - ratR(k)*dBzS(:,k);
    dBpS(:,k)=dBpS(:,k) - ratP(k)*dBzS(:,k);
end

 
faa=[   1.0675    1.0680    1.0330    0.9607    1.0239    1.0237    0.9441    0.9731    1.0297    0.9788    0.9628    0.9346];

for k=1:length(faa)
    dBzS(:,k)=dBzS(:,k)/faa(k);
end

  
dBzS=dBzS*facZ*7/9.5*1.01;
dBrS=dBrS*facR*7/9.5*1.01;
dBpS=dBpS*facP*7/9.5*1.01;

BzS=cumsum(dBzS)*dt;
BrS=cumsum(dBrS)*dt;
BpS=cumsum(dBpS)*dt;
for k=1:15
    BzS(:,k)=BzS(:,k)-BzS(IIoff,k);
    BrS(:,k)=BrS(:,k)-BrS(IIoff,k);
    BpS(:,k)=BpS(:,k)-BpS(IIoff,k);
end

BzS=BzS-Bh;


