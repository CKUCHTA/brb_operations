% Loads Speed Probe data from loadBspeed and calculates the flux. 
% Plots of dB, B, and flux are generated as well as a plot of B at a 
% specifice R value. NO OTHER CALCULATIONS ARE MADE. 
% last updated 12Mar23

% function speedPlot(shot, BzRposition)
clear
shot = 60158;%60043;
BzRposition = 0.10; % Radial location of Bz plot in meters (closest probe location will be plotted)

[Bspeed, dBspeed, rvecS, SamRange, time, Bh, probeLoc] = getSpeedProbe(shot);

Bz = Bspeed{1}';
dBz = dBspeed{1}';

%calculate flux
flux = BzFluxSpeed(Bz, probeLoc);

[~,probeInd] = min(abs(BzRposition - probeLoc)); % find the probe number closest to BzRposition
BzprobeLoc = round(probeLoc(probeInd),2)*100; % location of probe in cm closest to BzRposition

figure(1), clf
tlim = [200 time(end)];

sgtitle(sprintf('Shot %d, B_h = %0.1f [mT]', shot, Bh*1e3))
% Bz Plot at specified R values
subplot(3,3,[1 3])
plot(time,Bz(8,:)*1e3,'r')
yline(0, '--k')
ylabel('B(mT)')
xlabel('t(\mus)')
xlim(tlim)
title(['Bz at R = ' num2str(BzprobeLoc) ' [cm]'])

% dBz pcolor plot
subplot(3,3,[4 7])
pcolor(time, probeLoc*1e2, dBz), shading interp
colorbar
title(num2str(shot))
clim([-1 1]*1e4)
hold on
ylabel('r(cm)')
xlabel('t(\mus)')
xlim(tlim)
title('dB_z [mT/s]')

% Bz pcolor plot
subplot(3,3,[5 8])
pcolor(time, probeLoc*1e2, Bz*1e3), shading interp
colorbar
hold on
contour(time, probeLoc*1e2, Bz*1e3,[0 0 ],'k')
clim([-25 25])
yline(BzprobeLoc,'--r')
xlim(tlim)
ylabel('r(cm)')
xlabel('t(\mus)')
title1 = sprintf('B_z[mT]');
title(title1)

% Flux pcolor plot
subplot(3,3,[6 9])
pcolor(time, probeLoc*1e2,flux), shading interp
colorbar
hold on
contour(time, probeLoc*1e2, Bz*1e3,[0 0 ],'k')
title(num2str(shot))
%clim([-15 25]*1e-3)
ylim(probeLoc([1 end])*1e2)
xlim(tlim)
ylabel('r(cm)')
xlabel('t(\mus)')
title('\Psi(R,t) [T\cdotm^2]')
