% Used for running in the control room

savePlot = 0;
savedtaqData = 1;
IncludeContour = 0; % set to 1 to plot flux contours (Unless you are using interpolated data, these contours are wrong!!!)
PlotFlux = 0    ; % set to 1 to plot flux
PlotTimeTrace = 1; % set to 1 for raw data time trace
PlotPcolor = 0; % set to 1 to plot pcolor 3DFlux probe data
% xlims = [1000 4000]; % plot range for sample points
% SamRange=69000:73000; % Total sample sange evaluated over

plotRawBoards = 0;
plotRawSticks = 1;
webSave = 1;



% clear
% shotlist = 62263:62269;
% ProbeType = 2; % 1 for 1D flux array. 2 for 3D flux array
GUI_onOFF = 1; % 1 to turn GUI on
UseOldPoints = 1; % 1 for using saved points
SkipThruGUI = 1;% 1 for auto clicking through GUI
CheckPlots_Zdir = 0; % 1 to plot Bz and dBz for each stick

Stick1Pos= 0.0; % stick 1 position in cm
saveMovie = 1;
saveFrame = 0;
SaveData = 1; % 1 to save matlab data to harddrive

NumberOfProbesR = 10;  %number of positions on each flux array stick. R and PHi have 10, Z has 12.
InterpNumberOfProbesR = 29;  %number of probe positions after interpolating by 4: that is 29 = (8-1)*4+1 (interpolated in r between the 8 existing values
NumberOfProbesPhi = 10;  %number of positions on each flux array stick. R and PHi have 10, Z has 12.
InterpNumberOfProbesPhi = 29;  %number of probe positions after interpolating by 4: that is 29 = (8-1)*4+1 (interpolated in r between the 8 existing values
NumberOfProbesZ = 12;  %number of positions on each flux array stick. R and PHi have 10, Z has 12.
InterpNumberOfProbesZ = 29;  %number of probe positions after interpolating by 4: that is 29 = (8-1)*4+1 (interpolated in r between the 8 existing values


%%%%%%%%%%%%%%%%%%% TREX Coils Perameters %%%%%%%%%%%%%%%%%%%
% SamRange = 69000:71000;
% IIm = 901:1000; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% IIoff = 1000; %Makes Bz=Bh at this index
% facClim = 0.1; % multiplies range of clim in GUI
% dt=1e-7; % time between samples
% ylims = [1000, 1380]; % sample point range in speed probe configuration
% FirstPoint = 1050; % time fitlines begin
% LastPoint = 1350; % time fitlines end
% dBzLastPoint = 1200;
% Folder = 'DriveCylinderCalibration'; % Parent folder for data and plots
% TimeIndexRange = 290; % Time index when current sheet is ~25cm 
% TimeIndexRange=0:3:580; % TREX movies

% Multiply ylims by 4 to get starting range of LowerTime and UpperTime
% LowerTime = 3300; % sample time less than lowest line made in InterpByEye (Lower limit of interpolation)
% UpperTime = 6200; % sample time greater than highest line made in InerpByEye (upper limit of interpolation)
% timeIndex = 4220:2:5800; % time index range of movie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%% Dipole Perameters %%%%%%%%%%%%%%%%%%%%%%%%%%
% SamRange = 69000:73000;
% IIm=901:980; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% IIoff=1020;
% facClim = 0.5; % multiplies range of clim
% dt=1e-7; % time between samples
% ylims = [1800, 2200]; 
% FirstPoint = ylims(1) + 50; % time fitlines begin
% LastPoint = ylims(2)-50; % time fitlines end
% dBzLastPoint = ylims(2)-100;
% Folder= 'DriveCylinderCalibration';

% % Multiply ylims by 4 to get starting range of LowerTime and UpperTime
% LowerTime = 7200; % sample time less than lowest line made in InterpByEye (Lower limit of interpolation)
% UpperTime = 8800; % sample time greater than highest line made in InerpByEye (upper limit of interpolation)
% timeIndex = 7200:2:8400; % time index range of movie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%% Drive Cylinder Perameters %%%%%%%%%%%%%%%%%%%%%%%%%%
SamRange=69000:73000;
IIm=1740:1800; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
IIoff=1800; %Makes Bz=Bh at this index
facClim = 0.5; % multiplies range of clim
dt=1e-7; % time between samples
ylims = [1800, 2090]; 
FirstPoint = ylims(1) + 50; % time fitlines begin
LastPoint = ylims(2)-50; % time fitlines end
dBzLastPoint = ylims(2)-100;
Folder= 'DriveCylinderCalibration';
Stick1Pos_probe1 = 0.05; % stick 1 position in m
Stick1Pos_probe2 = -0.05; % stick 2 position in m 

% Multiply ylims by 4 to get starting range of LowerTime and UpperTime
LowerTime = 7200; % sample time less than lowest line made in InterpByEye (Lower limit of interpolation)
UpperTime = 8800; % sample time greater than highest line made in InerpByEye (upper limit of interpolation)
timeIndex = 7200:2:8400; % time index range of movie

% TimeIndexRange = 290; % Time index when current sheet is ~25cm 
TimeIndexRange=100:1:460; % TREX movies
TimeIndexSet = [165 190 250]; % Time index for 3 plots in movie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Colorbar limits
BzClims = [-15 15]; % TREX Coils
% BzClims = [-5 5]; % vacuum shots
dBzClims = [-5 5]*1e3; % TREX Coils
% dBzClims = [-.7 .7]*1e3; %vacuum shots

% BpClims = [-1.5 1.5]; TREX Coils
BpClims = [-1 1]; 
% dBpClims = [-1.5 1.5]*1e3; % TREX Coils
dBpClims = [-0.8 0.8]*1e3;

% BrClim = [-1.5 1.5]; TREX Coils
BrClims = [-5 5];
% dBrClim = [-1 1]*1e3; % TREX Coils
dBrClims = [-0.3 0.3]*1e3;
% Vacuum shots
% BrClim = [-2 2];
% dBrClim = [-0.1 0.1]*1e3;

setFigurePosition = 1; % set to 1 to set Flux Array 2 figure positions
shotlist = 64990;
for shot = shotlist
    Plot3DFarray(shot, savePlot, savedtaqData, IncludeContour, PlotFlux, Folder, Stick1Pos, setFigurePosition)
    % MachPlots(shot, plotRawBoards, plotRawSticks)
    % pColorTe1_CalibratePlot(shot, WhichProbe)
    Plot1DFarray(shot, savePlot, savedtaqData, IncludeContour, PlotFlux)
    % % langmuir_fitScan(shot)

    ProbeType = 2;
    InterpByEye3DFarrayZdir(shot, ProbeType, GUI_onOFF, SkipThruGUI, SaveData, UseOldPoints,...
        CheckPlots_Zdir, Folder, SamRange,IIm, IIoff, facClim, dt, ylims, FirstPoint, LastPoint, dBzLastPoint);
    ProbeType = 1;
    InterpByEye3DFarrayZdir(shot, ProbeType, GUI_onOFF, SkipThruGUI, SaveData, UseOldPoints,...
        CheckPlots_Zdir, Folder, SamRange,IIm, IIoff, facClim, dt, ylims, FirstPoint, LastPoint, dBzLastPoint);
         
    Plot1Dand3DatASpeciticTime(shot,saveMovie, saveFrame, SaveData, Stick1Pos_probe1,...
    Stick1Pos_probe2, Folder, TimeIndexSet, LowerTime, UpperTime, ...
    timeIndex, BzClims, dBzClims, NumberOfProbesZ, InterpNumberOfProbesZ)
end
