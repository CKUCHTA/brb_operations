% function CT = CTdata(shotnum)
clear
% shotnum = 59594;
shotnum = 62081;
mdsconnect('skywalker.physics.wisc.edu');
mdsopen('wipal', shotnum)

CT1=mdsvalue('\CT_PS1_I_MAIN');
CT2=mdsvalue('\CT_PS2_I_MAIN');

CT = {CT1 CT2};
CT1 = CT1*1e-3+5;
% CT2 = CT2*1e-3-9;
avgCT2 = mean(CT2*1e-3);
CT2 = CT2*1e-3-avgCT2;

mdsclose;
mdsdisconnect;

%%
MaxSamples = length(CT2);
SampleRate = 10e6;
tmax = MaxSamples/SampleRate;
dt = 1/SampleRate;
time = (0:dt:tmax)*1e6; % time in microseconds
time = time(2:end);

figure(1), clf
plot(time,CT2, '-') %'Linewidth', 1)
hold on
% plot(time(:,2:end),CT1, 'Linewidth', 2)
ylabel('I [kA]')
xlabel('t [\mus]')
% legend('CT1', 'CT2')
legend('CT2')
ylim([-100 100])
% xlim([3200 3450])