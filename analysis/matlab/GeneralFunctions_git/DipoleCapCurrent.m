% Plots the current from the dipole cap bank measured via a rogowski coil

% function DipoleCapCurrent(shot, boards)
clear
shot = 64355;
% SamRange=69000:71000;
SamRange=69000:75000;
xlims = [1000 2000];
PlotTimeTrace = 0;
dt=1e-7; % time between samples
saveData = 1;
[daq, ~] = loadDtaqData3DFarray(shot, saveData, SamRange); % load dtac from harddrive or from mdsplus
[boards, sticks, tpoints, dBSetRaw] = load3DFarrayDipole(daq, SamRange, dt, shot, PlotTimeTrace, xlims);

Rogowski_V = boards{9}(:,15);
Vrog=Rogowski_V-mean(Rogowski_V(1:500));

CalFac=3.3e9;
Irog=1e-7*cumsum(Vrog)*CalFac;


IIs = 1053; %sample when coil turns on. 

C=30e-6; %  Value of C for the cap-bank 
 L=71.3e-6; % Total self-inductance of circuit
 R=0.105;     % resistance of circuit
 V0cap=1e3;  % voltage on cap
 V0rog=-0.101; % voltage at t~0 observed on Vrog



 W0 = sqrt(1/(L*C) - R/(4*L^2));
 gam= R/(2*L);


 t=((1:length(Vrog))-IIs)*1e-7;

 delPhi=0.033; %small phase-delay, needed to make current zero at t=0 (introduced by R)
 Vmodel= V0cap*real( exp( i* ((W0+i*gam)*t-delPhi) ));

 Imodel=12*C*gradient(Vmodel,t); % total current in the 12 turns

II=find(t<0);
Vmodel(II)=0;
Imodel(II)=0;



xlims=[-100 500];

figure(1),clf
sgtitle(sprintf('Shot %d', shot))
subplot(3,1,1)
plot(t*1e6,Vrog)
hold on
plot(t*1e6,V0rog*Vmodel/V0cap)
xlabel('t {\mu}s')
title('V_{rog} measured, and modeled')
xlim(xlims)
ylim([-0.15 0.15])

subplot(3,1,2)
plot(t*1e6,Vmodel)
xlabel('t {\mu}s')
title(sprintf('V_{model}, scaled to V0 = %dV on cap', V0cap))
xlim(xlims)


subplot(3,1,3)
plot(t*1e6,Irog/1e3)
hold on
plot(t*1e6,Imodel/1e3)
xlabel('t {\mu}s')
title('I_{rog} [kA] calibrated, and I_{model}')
xlim(xlims)

ys=linspace(16, 4, 4);
text(-60, ys(1), ['L=' num2str(L*1e6) '{\mu}H'])
text(-60, ys(2), ['C=' num2str(C*1e6) '{\mu}F'])
text(-60, ys(3), ['R=' num2str(R*1e3) 'm\Omega'])
text(-60, ys(4), ['Fac=' num2str(CalFac/1e9) '*10^9'])



% print -djpeg -r400 'C:\Users\Jan Egedal\Box Sync\WorkMadison\matmac\2024\TREX\Cusp\RogCalibration.jpg'

