% Creates a langmuir fit given a single sample point using the Iprobe
% and Vprobe data at that point.  
% Created 11Mar23

% function LangmuirFitSinglePlot(StringTitle, shot, Sample, GlobalRange, save, ShotType)
clear
shot = 64032;
Sample = 1;
StringTitle = 'Test';
GlobalRange = 32501:32525;
save = 0;
ShotType = 'AllTips';

[Iprobe, Vprobe, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~] = loadTeData(shot);

if GlobalRange ~=0
    Iprobe = Iprobe(GlobalRange, :);
    Vprobe = Vprobe(GlobalRange, :);
    time = Sample/(5*1e6)*1e6 + 199.1; % 199.1 is the start time for the Te data in density_inTime
end

[Vp, Ip, ~, ~, ~, Vplasma, ne, Te, ~, confidence, fit2, outliers] = LangmuirFit_Working(Iprobe, Vprobe, int64(Sample), ShotType);

fig = figure(Sample);
clf
yline(0,'--k', 'HandleVisibility','off')
hold on
fitPlot = plot(fit2,'r-', Vp, Ip,'k.',outliers,'m*');
clear xlabel
clear ylabel
xlabel('V_{probe}')
ylabel('I_{probe}')
titleStr = sprintf(['%s, shot %d, n_e =%0.2em^{-3}, ' ...
    'T_e = %0.1feV, V_p = %0.1fV, time = %0.1f\\mus'], StringTitle, shot, ne, Te, Vplasma, time);
% title(titleStr)
legend('location', 'best')
set(fitPlot,'lineWidth',2);
set(fitPlot,'MarkerSize',12);
fontsize(fig, 14, "points")
fig.Position = [400,200,500,300];

if save == 1
    plotFolder = sprintf('/Volumes/DC_DATA/Research/Data/Drive_Voltage_Scan/Plots/DensityinTimePlots_%s', ShotType);
    plotFile = fullfile(plotFolder, sprintf('%d_%sIVcurve.png', shot, StringTitle));
    saveas(gcf, plotFile)
end
