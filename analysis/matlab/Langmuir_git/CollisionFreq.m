
% clear
% shot = 60111;
% DataFolder = '/Users/paulgradney/Research/Data/';
% DriveVoltageFolder = 'Drive_Voltage_Scan/';
% ScriptFolder = fullfile(fullfile(DataFolder,DriveVoltageFolder), 'Scripts');
% 
% [shotnum, ~] = collectShotNum(DataFolder, DriveVoltageFolder);
% savePlot =0;
% physicalConstants

for shot = shot %shot = shotnum
%     dataFile = fullfile(ScriptFolder, sprintf('%d_DensityinTimeData.mat', shot));

%     load(dataFile)
%     savePlot =0;
%     close(gcf)

    tauei = zeros(1,length(ne2));
    tauJan = zeros(1,length(ne2));
    tauNRL= zeros(1,length(ne2));
    Va = zeros(1,length(ne2));
    diJan = zeros(1,length(ne2));
    diNorm1 = zeros(1,length(ne2));
    diNormAlpha = zeros(1,length(ne2));
    deby = zeros(1,length(ne2));
    lnL = zeros(1,length(ne2));
    Bz_Te = BzSpeed(1:2:end);
    S = zeros(1,length(ne2));
    Bred = zeros(1,length(ne2));
    vAH =  zeros(1,length(ne2));
    
    SaberLocation = 0.27; % radial location of lighsaber probe

    [Erec, PsiData, positionData, timeData, fluxRatioPlot] = FluxOnSeperatrix(shot, XprobeRange, SpeedRange, SaberLocation);
    
    %calculate v_ah
    numerator = B2*B3*(B2 +B3);
    denominator = mu0*mi*(neAtB3*B2 + neAtB2*B3);
    v_ah = sqrt(numerator/denominator);
    Bred = (2*B2*B3)/(B2 + B3);

    alpha = Erec/(v_ah*Bred);

    for ii = 1:length(ne2)
        ne = ne2(1,ii);
        di_loc = di_fit2(1,ii);
        Te_eV = Te(1,ii);
        B = Bz_Te(1,ii);
        TeJ = Te_eV*q; % Joules
        deby(1,ii) = sqrt(eps0*TeJ/(ne*q^2));
        lnL(1,ii) = log(12*pi*ne*(deby(1,ii))^3); % Coulumb Logarithm
        const1 = (12*pi*eps0^(3/2))/q^3;
        const2 = 6*sqrt(2*pi^3*me)*eps0^2/(lnL(1,ii)*q^4);
        tauei(1,ii) = const2*((TeJ)^1.5)/ne; % electron-ion relaxation time
        Va(1,ii) = sqrt(B^2/(mu0*ne*mi));  % alfven velocity using BzSpeed
        diNorm1(1,ii) = di_loc/(0.1*Va(1,ii));  % electron fluid reconnection crossing time
%         [diJan(1,ii), de, tauJan(1,ii), vthe, lamdae, VaJan(1,ii), Erec, eta, S(1,ii), A] = MITDXparShort(Te_eV,ne,B,1,0.5,2); 
        diNormAlpha(1,ii) = di_loc/(alpha*Va(1,ii));
    
    end
    %%

    rvec = velAtInput*1e3*timeDTAC*1e-6 - velAtInput*1e3*B0time*1e-6 + SaberLocation; % first number is the B=0 location second is the probe location
    B2position = velAtInput*1e3*B2time*1e-6 - velAtInput*1e3*B0time*1e-6 + SaberLocation;
    B3position = velAtInput*1e3*B3time*1e-6 - velAtInput*1e3*B0time*1e-6 + SaberLocation;
    B0position = velAtInput*1e3*B0time*1e-6 - velAtInput*1e3*B0time*1e-6 + SaberLocation;

    CollisionPlot = figure(111);
    clf
    subplot(2,1,1)
%     semilogy(timeCell1, diNorm1, 'r', 'linewidth', 1)
    
    semilogy(timeCell1, diNormAlpha,'m')
    hold on
    semilogy(timeCell1, tauei, 'b')
    xline(B0time, '--k','linewidth', 2)
    xlim([0 25])
    ylabel('time s')
    xlabel('Experimental time \mus')
    diAlphaLabel = sprintf('di/(\\alphav_{a})');
    legend(diAlphaLabel, ...
    'tau_{ei}','B=0', 'FontSize', 10, 'Location', 'Best')
    title(sprintf('Shot %d , \\alpha = %0.1f,  Relaxation Time Compared to Electron Transit Time', shot, alpha))

    subplot(2,1,2)
    plot(rvec,BzSpeed*1e3,'Linewidth',2 ,'HandleVisibility','off')
    hold on
    xline(B2position, '--r', 'LineWidth',2)
    xline(B3position, '--k', 'LineWidth',2)
    xline(B0position, '--b', 'Linewidth', 2, 'HandleVisibility','off')
    %xline(B0timeSaber, '--b', 'Linewidth', 2, 'HandleVisibility','off')
    yline(0, '--k', 'HandleVisibility','off')
    ylabel('B_z(mT)')
    xlabel('R(m)')
%     xlim([min(timeDTAC) max(timeCell1)])
    xlim([0 0.6])
    ylim([-30 20])
    B2Label = sprintf('B_2 = %0.2f mT', B2*1e3);
    B3Label = sprintf('B_3 = %0.2f mT', B3*1e3);
    legend(B2Label, B3Label,'Location', 'Best') 
    title(sprintf('di = %0.2f', di_fit2(Cell1B2Ind)))
%     set(gca,'XTick',(rvec(1)-0.0001):.5:(rvec(end)-0.0001));

    %%
    if savePlot == 1
        plotFileCollision = fullfile(plotFolder, sprintf('%d_CollistionPlot.png', shot));
        saveas(CollisionPlot, plotFileCollision)
    end
end
