% Load Te data that is on external harddrive if it exist.  If it does not
% exist, load data from MDSPlus and save it to Temporary folder on external
% harddrive.

function [Iprobe, Vprobe, Working, dB_ax1, dB_ax2, dB_ax3, dBp, dBr, dBz, Area, ...
    TeNoise, TeSig, Vbias, Rsense, Rdiv, Rdiggnd, Bh] = loadTe1Data(shot)
% clear
% shot = 63799;

TeDataStructure = what('Te1_Data');
TemporaryFolder = TeDataStructure.path;

TeFile = strcat(num2str(shot),'_Te1Data.mat');
TeFilePath = which(TeFile); % path of TeData file

try
   load(TeFilePath, 'Iprobe', 'Vprobe', 'Working', 'dB_ax1', 'dB_ax2', ...
       'dB_ax3', 'dBp', 'dBr', 'dBz', 'Area', 'TeNoise', 'TeSig', 'Vbias', ...
       'Rsense', 'Rdiv', 'Rdiggnd', 'Bh');
catch
    disp(['Could not find file: ', TeFile])
    % load data from mdsplus
    [Iprobe, Vprobe, Working, dB_ax1, dB_ax2, dB_ax3, dBp, dBr, dBz,...
    Area, TeNoise, TeSig, Vbias, Rsense, Rdiv, Rdiggnd, Bh] = Te1_data(shot);
    shot_data_file = sprintf('%d_Te1Data.mat',shot);
    fileName = fullfile(TemporaryFolder, shot_data_file);
    save(fileName)
end