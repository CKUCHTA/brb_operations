% Produces a plot of the relaxation time compared to the transite time of a
% electron fluid element through the reconnection region

% DataFolder = '/Users/paulgradney/Data/';
% DriveVoltageFolder = 'Drive_Voltage_Scan/';
% ScriptFolder = fullfile(fullfile(DataFolder,DriveVoltageFolder), 'Scripts/DensityinTimeData');

% [shotnum, ~] = collectShotNum(DataFolder, DriveVoltageFolder);
% savePlot =0;
% shot = 60147;
% physicalConstants
% 
% dataFile = fullfile(ScriptFolder, sprintf('%d_DensityinTimeData.mat', shot));
% 
% load(dataFile)
close(gcf)

tauei = zeros(1,length(ne2));
taueeJan = zeros(1,length(ne2));
tauNRL= zeros(1,length(ne2));
Va = zeros(1,length(ne2));
diJan = zeros(1,length(ne2));
diNorm1 = zeros(1,length(ne2));
diNormAlpha = zeros(1,length(ne2));
deby = zeros(1,length(ne2));
Jan_deby = zeros(1,length(ne2));
lnL = zeros(1,length(ne2));
BzSpeed_TeTimes = BzSpeedAtTeLoc(1:2:end);
Dreicer = zeros(1,length(ne2));

physicalConstants

% [~, shotData] = AllShotData(shot);
% SaberLocation = shotData(1,4); % radial location of lighsaber probe
SaberLocation = Rinput;

[Erec, PsiData, positionData, timeData, fluxRatioPlot] = FluxOnSeperatrix(shot, XprobeRange, SpeedRange, SaberLocation);

%calculate v_ah
numerator = B2*B3*(B2 +B3);
denominator = mu0*mi*(neAtB3*B2 + neAtB2*B3);
v_ah = sqrt(numerator/denominator);

alpha = Erec/(v_ah*Bred);

for ii = 1:length(ne2)
    ne = ne2(1,ii);
    diB2 = di_fit2(1,ii);
    Te_eV = Te(1,ii);
    B = BzSpeed_TeTimes(1,ii);
    TeJ = Te_eV*q; % Joules
%     deby(1,ii) = sqrt(eps0*TeJ/(ne*q^2));
    deby(1,ii) = sqrt((eps0*Te_eV*q)/(ne*q^2));
%     lnL(1,ii) = log(12*pi*ne*(deby(1,ii))^3); % Coulumb Logarithm
%     lnL(1,ii) = log((12*pi*(eps0*Te_eV)^1.5)/(sqrt(ne)*q^3));
    lnL(1,ii) =log(12*pi*sqrt((eps0*Te_eV*q).^3./(ne*q^6)));
    const1 = (12*pi*eps0^(3/2))/q^3;
%     const2 = 6*sqrt(2*pi^3*me)*eps0^2/(lnL(1,ii)*q^4);
    const2 = (4*pi*eps0)^2*sqrt(me)/(q^4*lnL(1,ii));
%     tauei(1,ii) = const2*((TeJ)^1.5)/ne; % electron-ion relaxation time
%     tauei(1,ii) = const2*((q*Te_eV)^1.5)/ne; 
    tauei(1,ii) =6*sqrt(6*pi*me)*eps0^2*((3*Te_eV*q).^1.5)./ ((ne*q^4).*lnL(1,ii)); 
    Va(1,ii) = sqrt(B^2/(mu0*ne*mi));  % alfven velocity using BzSpeed
    diNorm1(1,ii) = diB2/(0.1*Va(1,ii));  % electron fluid reconnection crossing time
%         [diJan(1,ii), de, tauJan(1,ii), vthe, lamdae, VaJan(1,ii), Erec, eta, S(1,ii), A] = MITDXparShort(Te_eV,ne,B,1,0.5,2); 
    diNormAlpha(1,ii) = diB2/(alpha*Va(1,ii));
    vthe=sqrt(2*TeJ*q/me);
    Jan_deby(1,ii)=vthe*tauei(1,ii);
%     [di, de, taueeJan(1,ii), vthe, Jan_deby(1,ii), Va, Erec, eta, S, A] = MITDXparShort(Te_eV,ne,B,1,1,2);
    Dreicer(1,ii) = ne*q^3*lnL(1,ii)/(8*pi*eps0^2*TeJ);
end

Avg_lnL = mean(lnL(1,:), 'omitnan');

%%
CollisionPlot = figure(111);
clf
semilogy(timeCell1, diNorm1, 'k', 'linewidth', 1)
hold on
semilogy(timeCell1, diNormAlpha,'r', 'Linewidth',1)
semilogy(timeCell1, tauei, 'b','Linewidth',1)
semilogy(timeCell1, taueeJan, '--m','Linewidth',1)
xline(B0time, '--k','Linewidth', 2)
% xlim([9 15])
ylabel('time [s]')
xlabel('\Deltat [\mus]')
diAlphaLabel = sprintf('di/(\\alphav_{a})');
if alpha<0
    legend('di/(0.1v_a)','tau_{ei}','B=0', 'FontSize', 14, 'Location', 'Best')
elseif isnan(alpha)
    legend('di/(0.1v_a)','tau_{ei}','B=0', 'FontSize', 14, 'Location', 'Best')
else
    legend('di/(0.1v_a)',diAlphaLabel,'tau_{ei}','B=0', 'FontSize', 12, 'Location', 'Best')
end
title(sprintf('Shot %d , \\alpha = %0.1f,  Collision Time Compared to Electron Crossing Time', shot, alpha))

%%
IndVariablesPlot = figure(112);
clf
sgtitle(sprintf('Shot %d, ln\\Lambda \\approx %0.0f, R = %0.2f[m], E_{rec} = %0.1f[V/m] at B=0', shot, Avg_lnL, Rinput, Erec))
subplot(2,2,1)
plot(timeCell1, ne2)
xline(B0timeSec*1e6, '--k','linewidth', 1)
xlabel('t \mus')
ylabel('n_e[m^{-3}]')
title('Density[m^{-3}]')

subplot(2,2,2)
plot(timeCell1, Dreicer, 'HandleVisibility','off')
yline(0, '--k')
xline(B0timeSec*1e6, '--k','linewidth', 1, 'HandleVisibility','off')
xline(B2time, '--g','linewidth', 1)
xline(B3time, '--r','linewidth', 1)
xlabel('t \mus')
ylabel('E_d[V/m]')
title('Dreicer Field')
ylim([-10,80])
legend('B2', 'B3')

subplot(2,2,3)
plot(timeCell1, Te)
xline(B0timeSec*1e6, '--k','linewidth', 1)
xlabel('t \mus')
ylabel('Te[eV]')
title('Te[eV]')

subplot(2,2,4)
semilogy(timeCell1, deby)
hold on
semilogy(timeCell1, Jan_deby,'r', 'linewidth', 1)
xline(B0timeSec*1e6, '--k','linewidth', 1)
xlabel('t \mus')
ylabel('ln(\lambda_D)')
title('Deby Length [m]')
legend('$\lambda = \sqrt{\frac{\epsilon_0 T_e k_b}{nq^2}}$', '$v_{the}\tau_{ei}$','Interpreter','latex', 'fontsize',12)

IndVariablesPlot.Position = [400, 400, 800, 600];

%%
if savePlot == 1
    plotFileCollision = fullfile(plotFolder, sprintf('CollisionPlots/%d_CollistionPlot.png', shot));
    plotFileFluxRatio = fullfile(plotFolder, sprintf('FluxPlots/%d_FluxRatioPlot.png', shot));
    plotFileIndVariables = fullfile(plotFolder, sprintf('CollisionPlots/%d_IndVariables.png', shot));
    saveas(CollisionPlot, plotFileCollision)
    saveas(fluxRatioPlot, plotFileFluxRatio)  
    saveas(IndVariablesPlot, plotFileIndVariables)
end

