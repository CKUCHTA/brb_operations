% This script plots the voltage of all tips for a given sample window.
% From these plots the Te probe rails can be adjusted such that the
% floating potential is measured within the range of the Te probe tips.

function pColorTe1_CalibratePlot(shot, WhichProbe)
% clear
% shot = 64232;
% shots = 60116;
WhichProbe = 1; % 1 for Te1, 3 for Te3

% Set Dtaq sample range
InitialDtaqSample = 1840;
FinalDtaqSample = InitialDtaqSample + 250;
SampleRange_Dtaq = InitialDtaqSample:FinalDtaqSample;
SampleSize_Dtaq = length(SampleRange_Dtaq);

timeShift = -5;
% Set Te probe sample range
Initial_TeSample = 32920 - timeShift;
Final_TeSample = Initial_TeSample + round(SampleSize_Dtaq/2);
% Final_TeSample =  Initial_TeSample + 500;
timeTe = (Initial_TeSample:Final_TeSample)*2e-1;
SampleIndexes=Initial_TeSample:Final_TeSample;



tips = [2 4 6 8 9 11 13 15];  % evaluated Te probe tips
% tips = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16];  % evaluated Te probe tips

%choose Te1 or Te3 data
if WhichProbe ==1
    [Iprobe, Vprobe, Working, dB_ax1, dB_ax2, dB_ax3, dBp, dBr, dBz, Area, TeNoise, TeSig, Vbias, Rsense, Rdiv, Rdiggnd, Bh] = loadTe1Data(shot);
elseif WhichProbe == 3
    [Iprobe, Vprobe, Working, dB_ax1, dB_ax2, dB_ax3, dBp, dBr, dBz, Area, TeNoise, TeSig, Vbias, Rsense, Rdiv, Rdiggnd, Bh] = loadTeData(shot);
end

%%
% Iprobe Time Trace
% figure(5), clf
% for probe = 1:size(Iprobe,2)
%     subplot(4,4,probe)
%     plot(Iprobe(:,probe))
%     xlabel('Sample')
%     ylabel('I(A)')
%     title(sprintf('probe %d', probe))
% end


% Current = Iprobe(:,tips)';
% minVoltage = min(TeSig(SampleIndexes,tips), [], [1 2]);
% maxVoltage = max(max(TeSig(SampleIndexes,tips), [], 2));
% Vaxis = linspace(minVoltage, maxVoltage, numel(tips))*1e3;
% SampleAxis = 1:size(Iprobe,1);

% Current = Iprobe(:,tips)';
% minVoltage = min(Vbias(SampleIndexes,tips), [], [1 2]);
% maxVoltage = max(Vbias(SampleIndexes,tips), [], [1 2]);
% Vaxis = linspace(minVoltage, maxVoltage, numel(tips));
% SampleAxis = 1:size(Iprobe,1);
% 
Current = Iprobe(:,tips)';
minVoltage = min(Vprobe(SampleIndexes,tips), [], [1 2]);
maxVoltage = max(max(Vprobe(SampleIndexes,tips), [], 2));
Vaxis = linspace(minVoltage, maxVoltage, numel(tips));
SampleAxis = 1:size(Iprobe,1);

figure(7), clf
% subplot(2,1,1)
pcolor(SampleAxis, Vaxis,Current*1e3), shading interp
% pcolor(Current), shading interp
hold on
caxis([-1 1]*10)
contour(SampleAxis, Vaxis, Current*1e3,[0 0],'k', 'linewidth', 2)
% contour(Current,[0 0],'k', 'linewidth', 2)
cbar = colorbar;
cbar.Title.String = 'I_{probe}[mA]';
cbar.Title.FontSize = 12;

for ProbeTip = tips
    plot(Vprobe(:,ProbeTip)', 'linewidth', 1.5)
end
title(sprintf('Shot %d', shot))
xlim([Initial_TeSample, Final_TeSample])
xlabel('Sample')
ylabel('V_{probe}[V]')

return

subplot(2,1,2)
B=squeeze(TeIprobe(:,:,1));
pcolor(B'),shading interp
%xlim([501 600])
caxis([-1 1]*0.3)
hold on
contour(B',[0 0],'k')
colorbar
title(sprintf('shot %d, current sigs', shots))
xlabel('Sample')
ylabel('Probe')

% for k=1:numel(shots)
% subplot(2,1,k)
% A=squeeze(TeSigTot(:,:,k));
% pcolor(A'),shading interp
% %xlim([501 600])
% caxis([-1 1]*0.3)
% hold on
% contour(A',[0 0],'k')
% colorbar
% title(sprintf('shot %d, raw voltage sigs', shots(k)))
% end

% figure(3),
% for kk=540:570
%     clf
% for k=1:9
%     A=squeeze(TeSigTot(:,:,k));
% plot(A(kk,:)+k*0.3)
% hold on
% end
% title(num2str(kk))
% pause
% 
% end
    

% figure(7),clf
% for k=1:numel(shots)
% subplot(2,4,k)
% A=squeeze(TeSigTot(:,:,k));
% A=A(:,1:2:end)-A(:,2:2:end);
% pcolor(A'),shading interp
% %xlim([501 600])
% caxis([-1 1]*0.1)
% hold on
% contour(A',[0 0],'k')
% title(['shot ' num2str(shots(k)) ', even - odd'])
% colorbar
% 
% end