% fit the Te data to the exponetial IV curve using Hutchinson equation 3.2.33.
% created 31Dec22
% Last Edited 11Mar23

%function langmuir_fitScan(shot,Rinput)
    clear
    shot = 64032;
    % shot = 62397;
    Rinput = 31;

    % Load Te data
    [Iprobe, Vprobe, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~] = loadTeData(shot);

    % get dBz and Bz data from speed probe
    % [time_loc, dBz_loc, Bz_loc, ~, ~,~] = speedProb_vel(shot, 0, 0, 0, Rinput);
    [dBz_loc, Bz_loc, ~, ~, ~, ~, time_loc, ~, ~, ~, ~] = speedPlot_vel(shot, 0, 0, Rinput);
   

    physicalConstants % load constants

    dt = 2e-7; % frequency rate of Cell7 digitizer is 5MHz
    %tarray = (1:length(dB_ax3))*dt*1e6;
    %time = 32553;
    %time_loc = time_loc - 1; %shift
    SamIndexes = (time_loc/(dt*1e6)); % set the index to the same window as Speed Probe data
    SampleMax = SamIndexes(end);
    SampleMin = SamIndexes(1);
    tmax = SampleMax*dt*1e6;
    tmin = SampleMin*dt*1e6;

figure(11), clf
    for jj = 1:2:numel(SamIndexes)
        figure(11)
        time = SamIndexes(jj)*dt*1e6;
        % dBz Plot from Speed Probe data
        subplot(6,1,5)
        plot(time_loc, dBz_loc(8,:))
        xline(SamIndexes(jj)*dt*1e6, '--k', 'LineWidth', 2)
        xlabel('t[\mus]')
        ylabel('dBz')
        xlim([tmin tmax])
    
        % dBz Plot from Speed Probe data
        subplot(6,1,6)
        plot(time_loc, Bz_loc(8,:)*1e3)
        xline(SamIndexes(jj)*dt*1e6, '--k', 'LineWidth', 2)
        xlabel('t[\mus]')
        ylabel('Bz [mT]')
        xlim([tmin tmax])
        
        %pause()
        
        % Create fit function using Vp and Ip data
        ShotType = 'AllTips';
        % [Vp, Ip, fitfun, options, Working, V_plasma2, ne2, Te, di_fit2, confidence, fit2, outliers] = LangmuirFit(Iprobe, Vprobe, int64(SamIndexes(jj)));
        [Vp, Ip, fitfun, options, Working, V_plasma2, ne2, Te, di_fit2,...
            confidence, fit2, outliers, diError, neError] = LangmuirFit(Iprobe, Vprobe, int64(SamIndexes(jj)), ShotType);

        probNum = cellstr(num2str(Working')); % labels for plot

        %plot I vs V without fitting
        ax1 = subplot(6,1,[1 2]);
        cla(ax1)
        plot(Vp(1:2:end), Ip(1:2:end), '*b')
        hold on
        plot(Vp(2:2:end), Ip(2:2:end), '*g')
        text(Vp, Ip, probNum)
        xlabel('V')
        ylabel('I')
%         ylim([-2 15]/1e3)
        %ylim([-0.01 0.03]) %gun plasma
        hold on
        yline(0,'--k')
        title(['Shot ' num2str(shot)])

        % Skip sample points that could not be fit
        if isnan(ne2)
            continue
        end
        
        ax2 = subplot(6,1,[3 4]);
        cla(ax2)
        hold on
        yline(0,'k', 'HandleVisibility','off')
%        ylim([-2 15]/1e3)
%        ylim([-0.01 0.03])
        plot(fit2,'r-', Vp, Ip,'k.',outliers,'m*') 
        
        clear xlabel
        clear ylabel
        xlabel('V_{probe}')
        ylabel('I_{probe}')
        
        SubGrpTitle = sprintf(['Shot = %d, n_e=%0.2e[m^{-3}], T_e=%0.3f[eV], V_{plasma}=%0.2f[V], ' ...
            'd_i=%0.2f[m], time=%0.1f[\\mus]'], [shot, ne2, Te, V_plasma2, di_fit2, time]);
        sgtitle(SubGrpTitle)
        legend( 'Data', 'Data excluded from fit',...
        'Weighted Fit with points excluded', 'Location', 'northwest' )
        
         % pause()
        
    end
