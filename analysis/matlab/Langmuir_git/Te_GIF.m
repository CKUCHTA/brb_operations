% Creates gif and saves Speedplot

clear
shotnums=[186 183 88 99 164 108 119  130 135 141 148 156]+59900; % shot numbers for first movie
% shotnums=[77 83 90 93 106 160 124 128 135 143 147 157]+59900; % shot numbers for second movie
badshots = [];
for shot = shotnums
    shot
    try
        langmuir_fitScan(shot, 1, 1)
    catch
        badshots = [badshots shot];
    end
    
end