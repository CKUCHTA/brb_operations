% Save Te Data on to computer
% clear
%no perturbation [57736:57777 58183:58314]
% [57625:57728 57924:57945] perturbation
% [57950:58153 58317:58375] Te Scan
% [58475:58509 58511:58512 58514:58522 58525:58555 58557:585605 58607] No perturbation with All coils on
DataFolder = '/Users/paulgradney/Research/Data/';
% Shotfolder = '/No_Perturbation_NoMiddle/';
% Shotfolder = '/No_Perturbation_AllCoils/';
% Shotfolder = '/No_Perturbation/';
Shotfolder = 'TeSaber_Radial_Scan/6k_100G_MiddleOff';

% [shotnum, ~] = collectShotNum(DataFolder, Shotfolder);
% shotnum(1:171) = [];
% shotnum(1:170) =[];
for shot = 59988%shotnum
    try
        [Iprobe, Vprobe, Working, dB_ax1, dB_ax2, dB_ax3, dBp, dBr, dBz, Area, TeNoise, TeSig, Vbias, Rsense, Rdiv, Rdiggnd, Bh] = Te_data(shot);        
        DataFolder = '/Volumes/DC_DATA/Research/Data/TeSaber_Radial_Scan/6k_100G_MiddleOff/TeData/Movie1Shots';
        shot_data_file = sprintf('%d_TeData.mat',shot);
        fileName = fullfile(DataFolder, shot_data_file);
        
        save(fileName)
    catch
        warning('Error in saveData (line 8)');
    end
    clear
end

% SaberPlotScan
% RadialSaberGridPlots