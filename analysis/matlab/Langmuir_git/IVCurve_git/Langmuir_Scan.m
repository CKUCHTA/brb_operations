% This script will plot the IV curve for a given shot number. The IV curve
% data is saved in a structure array with fields for each parameter. The
% data is saved in a folder named 'IVdata' in the same directory as the data
% file. The data is saved in a file named 'IVdata.mat' with the same name as
% the data file. The IV curve data is plotted with the fit and saved as a
% movie. The movie is saved in a folder named 'Movies' in the same directory
% as the data file. The movie is saved in a file named 'IVCurve_%d' with the
% shot number as the file name.
% created 31Dec
% Last Edited 28Mar2024

% function IVCurvePlot = Langmuir_Scan(shot, saveMovie, ParentFolder, dipoleType,  exculdeOutliers, Working)
    clear
    shot = 64152;
    saveMovie = 1;
    ParentFolder = 'DipoleCuspVertical_FullScan';
    dipoleType = 'Dipole2kV';
    exculdeOutliers = 0;
    Working = [2 4 6 8 11 13]; % Working tips for the shot
    
    DataFolderStruct = what(ParentFolder);
    DataFolder = DataFolderStruct.path;
    MovieFolder = fullfile(DataFolder, '3DFarray','Plots','IVplots','Movies', dipoleType);
    if ~exist(MovieFolder, 'dir')
        mkdir(MovieFolder)
    end
    
    if saveMovie == 1
        videoFile = fullfile(MovieFolder,  sprintf('IVCurve_%d', shot));
        writerObj = VideoWriter(videoFile, 'MPEG-4');
        writerObj.FrameRate = 5; % Set the frame rate of the video
        open(writerObj);
    end

    % Load dipole current data
    SampleRange_Dtaq = 69000:73000;
    xlims = [1000 2000];
    PlotTimeTrace = 0;
    dt=1e-7; % time between samples
    saveData = 1;
    [daq, ~] = loadDtaqData3DFarray(shot, saveData, SampleRange_Dtaq); % load dtac from harddrive or from mdsplus
    [boards, ~, ~, ~] = load3DFarrayDipole(daq, SampleRange_Dtaq, dt, shot, PlotTimeTrace, xlims);
    
    Rogowski_V = boards{9}(:,15);
    Vrog=Rogowski_V-mean(Rogowski_V(1:500));
    
    timeShift = -5;
    sampleWindow = 260;
    % Set Dtaq sample range
    InitialDtaqSample = 1840;
    FinalDtaqSample = InitialDtaqSample + sampleWindow;
    SampleRange_Dtaq = InitialDtaqSample:FinalDtaqSample;
    SampleSize_Dtaq = length(SampleRange_Dtaq);
    % Vrog = Vrog(SampleRange_Dtaq);
    % timeRog = (1:length(SampleRange_Dtaq))*1e-1;
    
    % Set Te probe sample range
    Initial_TeSample = 32920 - timeShift;
    Final_TeSample = Initial_TeSample + round(SampleSize_Dtaq/2);
    timeTe = (Initial_TeSample:Final_TeSample)*2e-1;
    SampleRange_Te=Initial_TeSample:Final_TeSample;
    % SampleSize_Te = length(SampleRange_Te);


    % Load IV data
    try
        IVdataFolder = fullfile(DataFolder, '3DFarray','Matlab_data', 'IVdata', 'IVshotdata');
        IVdataFile = fullfile(IVdataFolder, sprintf('IVdata_%d.mat',shot));
        load(IVdataFile, 'IVdata')
        IVdata(:).Vp; % if IVdata is out of data this will throw an error
        fprintf('ploting shot %d IVdata from saved data.\n', shot)
    catch
        fprintf('shot %d IVdata file does not exist.\n Creating IVdata file.\n', shot)
        TeStart = [];
        neStart = [];
        VplasmaStart = [];
        IVdata = SaveIVcurveData(shot, SampleRange_Te, ParentFolder, exculdeOutliers, ...
        TeStart, neStart, VplasmaStart, Working, [], []);
    end
   
    V_plasma2_All  = [IVdata(:).Vplasma]; 
    ne2_All  = [IVdata(:).ne];
    Te_All  = [IVdata(:).Te];
    di_fit2_All  = [IVdata(:).di];
    dB1 = [IVdata(1).dB1];
    dB2 = [IVdata(1).dB2];
    dB3 = [IVdata(1).dB3];

    TeBdot = [dB1 dB2 dB3];
    physicalConstants % load constants

    frame = 0;
    IVCurvePlot = figure(12);
    clf 
    set(gcf, 'Position',  [100, 1, 750, 750])    
    ind1 = 0;
    ind2 = 0;
    for Sample = SampleRange_Te
        frame = frame + 1; 
        ind1 = ind1 + 1;
        ind2 = ind2 + 2;
       
        % use data from IVdata structure array
        Vp = IVdata(ind1).Vp;
        Ip = IVdata(ind1).Ip;
        V_plasma2 = V_plasma2_All(:,ind1); 
        ne2 = ne2_All(:,ind1);
        Te = Te_All(:,ind1);
        di_fit2 = di_fit2_All(:,ind1);
        fit2 = IVdata(ind1).fit2;
        outliers = [IVdata(ind1).outliers];
        
        probNum = cellstr(num2str([IVdata(ind1).working]')); % labels for plot

        %plot I vs V without fitting
        figure(12)
        ax1 = subplot(5,1,1);
        cla(ax1)
        plot(Vp(1:2:end), Ip(1:2:end), '*b')
        hold on
        plot(Vp(2:2:end), Ip(2:2:end), '*g')
        text(Vp, Ip, probNum)
        xlabel('V')
        ylabel('I')
%         ylim([-2 15]/1e3)
        %ylim([-0.01 0.03]) %gun plasma
        hold on
        yline(0,'--k')
        title(['Shot ' num2str(shot)])

        figure(12)
        ax2 = subplot(5,1,2:3);
        cla(ax2)
        hold on
        yline(0,'k', 'HandleVisibility','off')
%        ylim([-2 15]/1e3)
%        ylim([-0.01 0.03])
        % Skip sample points that could not be fit
        if isnan(ne2)
            % no fit data to plot for this sample
        else 
            if exculdeOutliers == 1
                plot(fit2,'r-', Vp, Ip,'k.',outliers,'m*') 
            else
                plot(fit2,'r-', Vp, Ip,'k.') 
            end
        end
        
        clear xlabel
        clear ylabel
        xlabel('V_{probe}')
        ylabel('I_{probe}')
        
        SubGrpTitle = sprintf(['Shot = %d, n_e=%0.2e[m^{-3}], T_e=%0.3f[eV], V_{plasma}=%0.2f[V], ' ...
            'd_i=%0.2f[m], Sample = %d'], [shot, ne2, Te, V_plasma2, di_fit2, Sample]);
        sgtitle(SubGrpTitle)
        if exculdeOutliers == 1
            if isempty(outliers)
                legend( 'Data', 'Weighted Fit', 'Location', 'northwest' )
            else
                legend( 'Data', 'Data excluded from fit',...
                'Weighted Fit with points excluded', 'Location', 'northwest' )
            end
        else
            legend( 'Data', 'Fit', 'Location', 'northwest' )
        end
           
        % Te Bdot plot
        figure(12)
        subplot(5,1,4)
        plot(TeBdot)
        xline(SampleRange_Te(ind1), '--k', 'LineWidth', 2)
        xlim([SampleRange_Te(1) SampleRange_Te(end)])
        ylabel('~[T/s]')
        xlabel('sample')
        legend('dB1', 'dB2', 'dB3')
        

        % Dipole Rogawski Plot
        figure(12)
        subplot(5,1,5)
        plot(Vrog)
        if ind2>length(SampleRange_Dtaq)
            continue
        end
        xline(SampleRange_Dtaq(ind2), '--k', 'LineWidth', 2)
        xlim([SampleRange_Dtaq(1) SampleRange_Dtaq(end)])
        xlabel('samples')
        ylabel('V')
        legend('Dipole Rogowski')

         % Choose plotting script to run
         Frame = IVCurvePlot;
            
        if saveMovie == 1
            CurrentFrame = getframe(Frame);
            writeVideo(writerObj, CurrentFrame); % Write the frame to the video
        end
    end
    % Close the VideoWriter object
    if saveMovie == 1
        close(writerObj);
    end

