%Using the saved IVdata.mat from SaveIVcurveData.m, this script plots
%Te, ne, Vplasma, and di as a function of time for a specified shot, and 
%shows the error in these parameters by plotting the shadded region spanned by
% the Te_confidence, ne_confidence, and Vplasma_confidnece values. The
%plots are saved in a folder named 'IVplots'.
% created 20May2024

clear
ParentFolder = 'DipoleCuspVertical_FullScan';
OnePerPosition = 0; % 1 = chooses one shot from set of shots with same flux probe position, 0 = all shots
webSave = 0; % 1 to download the google sheets data, 0 to use the saved data
[PlasmaShotLists, VacuumShotLists, shotData] = CreateShotList_dipole(ParentFolder, OnePerPosition, webSave);
plotOffOn = 1;
exculdeOutliers = 0; % 1 to exclude outliers 
% Working = [2 4 6 8 9 11 13]; % Working tips for the shot

shotList_DriveAndDipole1kV = PlasmaShotLists{1}(:,1)';
shotList_DriveAndDipole2kV = PlasmaShotLists{2}(:,1)';
shotList_DriveOnly = PlasmaShotLists{3}(:,1)';
shotlist = [64148 64149 64150 64151 64152 64153 64154 64155 64160 64161 ...
64162 64163 64164 64165 64166 64167 64168];
shotList = [64150 64162 64219 64233 64267 64278 64454 64427 64326 64368 64366 64477 64494]; % Te shot list, 1kV Dipole Cap full sca

AllShots = [shotList_DriveAndDipole1kV shotList_DriveAndDipole2kV shotList_DriveOnly shotlist];

SampleRange = 32925:33056;
All_IVData = struct('Te', {}, 'ne', {}, 'Vplasma', {}, 'di', {}, 'Te_confidence', ...
 {}, 'ne_confidence', {}, 'Vplasma_confidence', {}, 'di_error', {}, 'ne_isat', {}, ...
 'Te_isat', {}, 'shot', {}, 'TeZpos', {}, 'DipoleType', {});
indx = 1;
ii = 0;
for shot = shotList_DriveAndDipole2kV 
    if shot<64147 || shot == 64369 || shot == 64425 || shot == 64254 ...
        || shot == 64255 || shot == 64256 || shot == 64257 || shot == 64258 ...
        || shot == 64363 || shot == 64283 || shot == 64174 || shot == 64175 ...
        || shot == 64176 || shot == 64177 || shot == 64178 || shot == 64179 ...
        || shot == 64190 || shot == 64191 || shot == 64192 || shot == 64193 ...
        || shot == 64194 || shot == 64195 || shot == 64180 || shot == 64181 ...
        || shot == 64196 || shot == 64197 || shot == 64204 || shot == 64207 % skip bad shots
        continue
    end

    disp(['Processing shot ' num2str(shot)])
    ii = ii + 1;
    % IVdata = SaveIVcurveData(shot, SampleRange, ParentFolder, ...
    % exculdeOutliers, [], [], [], Working, [], []);  % comment to use saved data
    
    [~, Te, ne, Vplasma, di, Te_confidence, ne_confidence, ...
    Vplasma_confidence, di_error, TeZpos, DipoleType, ne_isat, Te_isat] ...
        =  PlotIVCurveData(shot, ParentFolder, SampleRange, shotData, plotOffOn);
    
    All_IVData(ii).shot = shot;
    All_IVData(ii).TeZpos = TeZpos;
    All_IVData(ii).DipoleType = DipoleType;
    All_IVData(ii).Te = Te;
    All_IVData(ii).ne = ne;
    All_IVData(ii).Vplasma = Vplasma;
    All_IVData(ii).di = di;
    All_IVData(ii).Te_confidence = Te_confidence;
    All_IVData(ii).ne_confidence = ne_confidence;
    All_IVData(ii).Vplasma_confidence = Vplasma_confidence;
    All_IVData(ii).di_error = di_error;
    All_IVData(ii).ne_isat = ne_isat;
    All_IVData(ii).Te_isat = Te_isat;
end

% Save the data
FolderStructure = what(ParentFolder);
FolderPath = FolderStructure.path;
IVdataFolder = fullfile(FolderPath, '3DFarray','Matlab_data', 'IVdata');
save(fullfile(IVdataFolder, sprintf('All_IVData%s.mat', DipoleType)), 'All_IVData')

function [shot, Te, ne, Vplasma, di, Te_confidence, ne_confidence, ...
    Vplasma_confidence, di_error, TeZpos, DipoleType, ne_isat, Te_isat] ...
     = PlotIVCurveData(shot, ParentFolder, SampleRange, shotData, plotOffOn)
    % clear
    % shot = 64151;
    % ParentFolder = 'DipoleCuspVertical_FullScan';
    % SampleRange = 32925:33056;
    % plotOffOn = 1;
    % OnePerPosition = 0; % 1 = chooses one shot from set of shots with same flux probe position, 0 = all shots
    % webSave = 0; % 1 to download the google sheets data, 0 to use the saved data
    % [PlasmaShotLists, VacuumShotLists, shotData] = CreateShotList_dipole(ParentFolder, OnePerPosition, webSave);

    % Find parent directory
    FolderStructure = what(ParentFolder);
    FolderPath = FolderStructure.path;

    % Determin if the IVdata structure array exists
    IVdataFolder = fullfile(FolderPath, '3DFarray','Matlab_data', 'IVdata', 'IVshotdata');
    IVdataFile = fullfile(IVdataFolder, sprintf('IVdata_%d.mat',shot));
    try
        load(IVdataFile, 'IVdata')
        if ~isfield(IVdata, 'ne_isat')
            fprintf('IVdata file does not contain ne_isat\nrunning SaveIVcurveData.m for shot %d\n', shot)
            IVdata = SaveIVcurveData(shot, SampleRange, ParentFolder, 0, [], [], [], [], [], []); 
        end

    catch
        fprintf('shot %d IVdata file does not exist.\n Creating IVdata file.\n', shot)
        IVdata = SaveIVcurveData(shot, SampleRange, ParentFolder, 0, [], [], [], [], [], []);
    end

    % Extract the data from the IVdata structure array0
    Te = [IVdata(:).Te]';
    ne = [IVdata(:).ne]';
    Vplasma = [IVdata(:).Vplasma]';
    di = [IVdata(:).di]';
    ne_isat = [IVdata(:).ne_isat]';
    Te_isat = [IVdata(:).Te_isat]';

    % Extract the confidence values
    Te_confidence = [IVdata(:).Te_confidence]';
    ne_confidence = [IVdata(:).ne_confidence]';
    Vplasma_confidence = [IVdata(:).Vplasma_confidence]';
    di_error = [IVdata(:).di_error]';

    ShotIndx = shotData(:,1) == shot;
    TeZpos = shotData(ShotIndx,5);
    DipoleCap = shotData(ShotIndx,7);

    % Create the IVplots folder if it does not exist
    DipoleType = sprintf('Dipole%dkV', DipoleCap);
    FinalIVplotsFolder = fullfile(FolderPath, '3DFarray','Plots', 'IVplots', DipoleType, 'FinalShotsUsed');
    IVplotsFolder = fullfile(FolderPath, '3DFarray','Plots', 'IVplots', DipoleType);
    if ~exist(IVplotsFolder, 'dir')
        mkdir(IVplotsFolder)
    end
    if ~exist(FinalIVplotsFolder, 'dir')
        mkdir(FinalIVplotsFolder)
    end

    physicalConstants % load constants

    % Plot the data
    if plotOffOn == 1
        figure(1), clf
        set(gcf, 'Position',  [300, 1, 750, 750])    
        sgtitle(sprintf('Shot %d, Dipole = %dkV, Z = %0.2f', shot, DipoleCap, TeZpos))
        % Plot the Te
        subplot(4,1,1)
        plot(Te, 'linewidth', 2)
        hold on
        % Plot the error region as a shaded area
        inbetween = [Te_confidence(:,1); flipud(Te_confidence(:,2))];
        fill([1:length(Te), fliplr(1:length(Te))], inbetween, 'r', 'EdgeColor', 'none', 'FaceAlpha', 0.2)
        plot(Te_confidence(:,1), 'r')
        plot(Te_confidence(:,2), 'r')
        ylim([0 100])
        xlabel('Sample')
        ylabel('Te [eV]')
        title('Te [eV]')

        subplot(4,1,2)
        plot(Vplasma, 'linewidth', 2)
        hold on
        % Plot the error region as a shaded area
        inbetween = [Vplasma_confidence(:,1); flipud(Vplasma_confidence(:,2))];
        fill([1:length(Vplasma), fliplr(1:length(Vplasma))], inbetween, 'r', 'EdgeColor', 'none', 'FaceAlpha', 0.2)
        plot(Vplasma_confidence(:,1), 'r')
        plot(Vplasma_confidence(:,2), 'r')
        ylim([-150 500])
        xlabel('Sample')
        ylabel('Vplasma [V]')
        title('Plasma Potential [V]')

        subplot(4,1,3)
        semilogy(ne, 'linewidth', 2)
        hold on
        % Plot the error region as a shaded area
        inbetween = [ne_confidence(:,1); flipud(ne_confidence(:,2))];
        fill([1:length(ne), fliplr(1:length(ne))], inbetween, 'r', 'EdgeColor', 'none', 'FaceAlpha', 0.2)
        semilogy(ne_confidence(:,1), 'r')
        semilogy(ne_confidence(:,2), 'r')
        ylim([1e14 1e20])
        xlabel('Sample')
        ylabel('ne [m^{-3}]')
        title('Density [m^{-3}]')

        subplot(4,1,4)
        semilogy(ne_isat, 'linewidth', 2)
        ylim([1e14 1e20])
        xlabel('Sample')
        ylabel('ne [m^{-3}]')
        title(sprintf('Density [m^{-3}] from I_{sat} with Te = %d [eV]', Te_isat(1)/q))
        saveas(gcf, fullfile(IVplotsFolder, sprintf('IVdata_%d.png',shot)))
    end
end