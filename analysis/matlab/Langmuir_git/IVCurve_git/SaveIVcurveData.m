% using LangmuirFit.m to find the plasma parameters from the IV curve data,
% this script saves the Te, ne, Vplasma, di, and time data to a .mat file
% for further analysis. The data is saved in a structure array with fields
% for each parameter. The data is saved in a folder named 'IVdata' in the
% same directory as the data file. The data is saved in a file named
% 'IVdata.mat' with the same name as the data file.

function IVdata = SaveIVcurveData(shot, SampleRange, FolderName, exculdeOutliers, ...
    TeStart, neStart, VplasmaStart, Working, EditWorkingSet, ChangeWorkingSampleSet)
% clear
% shot = 64368;
% SampleRange = 32925:33056;
% % SampleRange = 32925;
% FolderName = 'DipoleCuspVertical_FullScan';
% TeStart = [];
% neStart = [];
% VplasmaStart = [];
% exculdeOutliers = 0;
% Working = [2 4 6 8 9 11 13]; % Working tips for the shot
% EditWorkingSet = {[2 6 8] [2 6 8 9 11 13] [2 4 6 8 9 13]}; % Working tips for the shot to be used for Vp and Ip starting points
% ChangeWorkingSampleSet = {32925:32940 32981:32990 33010:33013}; % Sample number to change the working tips

% Set Default values for working tips
if isempty(Working)
    Working = [2 4 6 8 9 11 13];
end

FolderStructure = what(FolderName);
FolderPath = FolderStructure.path;
IVdataFolder = fullfile(FolderPath, '3DFarray','Matlab_data', 'IVdata', 'IVshotdata');


% Create the IVdata folder if it does not exist
if ~exist(IVdataFolder, 'dir')
    mkdir(IVdataFolder)
end

% Load the data file
[Iprobe, Vprobe, ~, dB1, dB2, dB3, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~] = loadTe1Data(shot);

% Create the IVdata structure array
IVdata = struct('shot',{},'Te', {}, 'Te_confidence', {}, 'ne', {}, 'ne_confidence', {}, ...
'neError', {},'Vplasma', {}, 'Vplasma_confidence', {}, 'di', {}, 'di_error', {}, 'Vp', {}, ...
'Ip', {}, 'dB1', {}, 'dB2', {}, 'dB3', {}, 'working', {}, 'fitfun', {}, 'options', {}, ...
'fit2', {}, 'outliers', {});


% Loop through the IV data and save the plasma parameters
i = 0;
for Sample = SampleRange
    i = i + 1;
    % Choose initial values for Te, ne, and Vplasma
    if ~isempty(TeStart) && ~isempty(neStart) && ~isempty(VplasmaStart)
        ne = neStart(i);
        Te = TeStart(i);
        Vplasma = VplasmaStart(i);

    elseif i == 1 || isnan(ne) || isnan(Te) || isnan(Vplasma) ...
        || isinf(ne) || isinf(Te) || isinf(Vplasma) || isnan(confidence(1,2)) || ...
        isnan(confidence(1,3))
        ne = 1e16;
        Te = 3;
        Vplasma = 10;
    end

    % Change working tips for specific samples
    if ~isempty(EditWorkingSet) && ~isempty(ChangeWorkingSampleSet)
        % ChangeWorkingSampleSet = flip(ChangeWorkingSampleSet);
        % EditWorkingSet = flip(EditWorkingSet);
        SaveWorking = Working;
        for ii = 1:length(ChangeWorkingSampleSet)
            ChangeWorkingSample = ChangeWorkingSampleSet{ii};
            if ismember(Sample, ChangeWorkingSample)
                Working2 = EditWorkingSet{ii};
                break
            else
                Working2 = SaveWorking;
                % break
            end
        end
    else
        Working2 = Working;
    end

    [Vp, Ip, fitfun, options, ~, Vplasma, ne, Te, di, confidence, fit2, outliers, diError, neError, ne_isat, Te_isat] ...
    = LangmuirFit_log(Iprobe, Vprobe, Sample, Working2, exculdeOutliers, ne, Te, Vplasma);
    
    % Save the data to the IVdata structure array
    IVdata(i).shot = shot;
    IVdata(i).Te = Te;
    IVdata(i).ne = ne;
    IVdata(i).Vplasma = Vplasma;
    IVdata(i).di = di;
    IVdata(i).di_error = diError;
    IVdata(i).ne_confidence = confidence(:,1);
    IVdata(i).neError = neError;
    IVdata(i).Te_confidence = confidence(:,2);
    IVdata(i).Vplasma_confidence = confidence(:,3);
    IVdata(i).Vp = Vp;
    IVdata(i).Ip = Ip;
    IVdata(i).dB1 = dB1;
    IVdata(i).dB2 = dB2;
    IVdata(i).dB3 = dB3;
    IVdata(i).working = Working2;
    IVdata(i).fitfun = fitfun;
    IVdata(i).options = options;
    IVdata(i).fit2 = fit2;
    IVdata(i).outliers = outliers;
    IVdata(i).ne_isat = ne_isat;
    IVdata(i).Te_isat = Te_isat;
end

% Save the IVdata structure array to a .mat file
save(fullfile(IVdataFolder, sprintf('IVdata_%d.mat',shot)), 'IVdata')
