% Creates a pcolor plot of Te, Vplasma, Density and di for a set of shot that scan across z positions.
% The z position is found for the google sheets data for each shot using DipoleShotData.m, and the plasma
% parameters are loaded from the IVdata structure array saved in the IVdata folder.

% Find the parent directory
clear
ParentFolder = 'DipoleCuspVertical_FullScan';
FolderStructure = what(ParentFolder);
FolderPath = FolderStructure.path;
DipoleType = 'Dipole2kV'; % 'Dipole1kV', 'Dipole2kV', 'DriveOnly'
savePlots = 1; % 1 to save plots, 0 to not save plots

% Load the IV data
IVdataFolder = fullfile(FolderPath, '3DFarray','Matlab_data', 'IVdata');
load(fullfile(IVdataFolder, sprintf('All_IVData%s.mat', DipoleType)), 'All_IVData')
plotFolder = fullfile(FolderPath, '3DFarray','Plots','IVplots', DipoleType, 'pcolorZscan');
if ~exist(plotFolder, 'dir')
    mkdir(plotFolder)
end

if strcmp(DipoleType, 'Dipole1kV')
    shotList = [64150 64162 64219 64233 64267 64278 64454 64427 64326 64368 64366 64477 64494];
    PlasmaShotList = [64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan
elseif strcmp(DipoleType, 'Dipole2kV')
    shotList = [64166 64152 64222 64235 64270 64285 64302 64314 64403 64371 64359 64479 64496]; % Te shot list, 2kV Dipole Cap full scan
    PlasmaShotList = [64329 64345 64358 64370 64400 64428 64456 64479 64495]; % 2kV Dipole Cap full scan
elseif strcmp(DipoleType, 'Dipole0kV')
    shotList = [64167 64155 64223 64238 64272 64289 64304 64318 64333 64376 64361 64484 64499];
    PlasmaShotList = [64332 64348 64361 64376 64406 64434 64464 64483 64498]; % Drive Only full scan
end

TeZvec = zeros(1, length(shotList));
Te1kV = nan(length(shotList), length([All_IVData(1).Te]));
ne1kV = Te1kV;
Vplasma1kV = Te1kV;
Te_plus = Te1kV;
Te_minus = Te1kV;
ne_plus = Te1kV;
ne_minus = Te1kV;
Vplasma_plus = Te1kV;
Vplasma_minus = Te1kV;
ne_isat = Te1kV;
Te_isat = Te1kV;

ii = 0;
for shot = shotList
    ii = ii + 1;
    shotIndx = find(shot == [All_IVData(:).shot]);
    TeZvec(ii) = [All_IVData(shotIndx).TeZpos];
    Te1kV(ii,:) = [All_IVData(shotIndx).Te];
    ne1kV(ii,:) = [All_IVData(shotIndx).ne];
    Vplasma1kV(ii,:) = [All_IVData(shotIndx).Vplasma];
    % di(ii,:) = [All_IVData(shotIndx).di];
    Te_plus(ii,:) =[All_IVData(shotIndx).Te_confidence(:,1)];
    Te_minus(ii,:) =[All_IVData(shotIndx).Te_confidence(:,2)];
    ne_plus(ii,:) =[All_IVData(shotIndx).ne_confidence(:,1)];
    ne_minus(ii,:) =[All_IVData(shotIndx).ne_confidence(:,2)];
    Vplasma_plus(ii,:) =[All_IVData(shotIndx).Vplasma_confidence(:,1)];
    Vplasma_minus(ii,:) =[All_IVData(shotIndx).Vplasma_confidence(:,2)];
    ne_isat(ii,:) = [All_IVData(shotIndx).ne_isat];
    Te_isat(ii,:) = [All_IVData(shotIndx).Te_isat];
    % di_error(ii,:) = [All_IVData(shotIndx).di_error];
end

time = [1:length(Te1kV)]/5;
DipoleEdgeZpos = - 0.4 + 0.129; % position of edge of dipole in meters
TeZvec = TeZvec*1e-2; % convert to meters

% load magnetic field data from flux array probe closest to Te probe
ZscanFolder = fullfile(FolderPath,'3DFarray','Matlab_Data','ZScanData');
load(fullfile(ZscanFolder, 'ZscanBdata_AllShots.mat'), 'BSetZdata', 'dBSetZdata', ...
 'ZposArray', 'SampleEval', 'ProbePos', 'tpoints', 'FarrayZPos', 'shotData')
 
% probe x positions
BxXpos = ProbePos{1};
ByXpos = ProbePos{2};
BzXpos = ProbePos{3};

Te_Xpos = 0.05; % X position of Te probe in meters
[~, Bx_CosestTeProbeIndex] = min(abs(Te_Xpos - BxXpos));
[~, By_CosestTeProbeIndex] = min(abs(Te_Xpos - ByXpos));
[~, Bz_CosestTeProbeIndex] = min(abs(Te_Xpos - BzXpos));
ProbeXpos = {BxXpos(Bx_CosestTeProbeIndex), ByXpos(By_CosestTeProbeIndex), BzXpos(Bz_CosestTeProbeIndex)};

Stick1Ypos = 0.03; % stick 1 y position (meters)
StickPos = Stick1Ypos + (0:(-12):-84)*1e-2; % stick positions
ClosestStick = 5; % stick closest to Te probe
ClosestStickYpos = StickPos(ClosestStick); % y position of stick 5

BSetXall = BSetZdata{1}; %  (probe, stick, z indx, sample point))
BSetYall = BSetZdata{2};
BSetZall = BSetZdata{3};

dBSetXall = dBSetZdata{1};
dBSetYall = dBSetZdata{2};
dBSetZall = dBSetZdata{3};

BSetX = zeros(1, 1, length(PlasmaShotList), size(BSetXall, 4));
BSetY = zeros(1, 1, length(PlasmaShotList), size(BSetXall, 4));
BSetZ = zeros(1, 1, length(PlasmaShotList), size(BSetXall, 4));

dBSetX = zeros(1, 1, length(PlasmaShotList), size(BSetXall, 4));
dBSetY = zeros(1, 1, length(PlasmaShotList), size(BSetXall, 4));
dBSetZ = zeros(1, 1, length(PlasmaShotList), size(BSetXall, 4));

% find the magnetic data for each shot from shotList and probe closest to Te probe
ZposVec = zeros(length(PlasmaShotList), 1); % vector to store the Z position of each shot
FarrayZvec = zeros(length(PlasmaShotList), 1); % flux array Z position for a given shot numbeir

for ii = 1:length(PlasmaShotList)
    Shot = PlasmaShotList(ii);
    ShotIndex = find(ZposArray(:,1) == Shot);
    if isempty(ShotIndex)
        error('Shot %d not found in ZposArray', Shot)
    end
    if length(ShotIndex) > 1
        ShotIndex = ShotIndex(1); % take the first shot if there are multiple entries
    end
    ZposVec(ii) = (ZposArray(ShotIndex, 2))*1e-2;
    
    BSetX(1,1,ii,:) = BSetXall(Bx_CosestTeProbeIndex, ClosestStick, ShotIndex,:);
    BSetY(1,1,ii,:) = BSetYall(By_CosestTeProbeIndex, ClosestStick, ShotIndex,:);
    BSetZ(1,1,ii,:) = BSetZall(Bz_CosestTeProbeIndex, ClosestStick, ShotIndex,:);

    dBSetX(1,1,ii,:) = dBSetXall(Bx_CosestTeProbeIndex, ClosestStick, ShotIndex,:);
    dBSetY(1,1,ii,:) = dBSetYall(By_CosestTeProbeIndex, ClosestStick, ShotIndex,:);
    dBSetZ(1,1,ii,:) = dBSetZall(Bz_CosestTeProbeIndex, ClosestStick, ShotIndex,:);
end
Bx = squeeze(BSetX);
By = squeeze(BSetY);
Bz = squeeze(BSetZ);
Bset = {Bx, By, Bz};

dBx = squeeze(dBSetX);
dBy = squeeze(dBSetY);
dBz = squeeze(dBSetZ);
dBset = {dBx, dBy, dBz};

time_daq = (1:length(Bx))/10; % converst numbers of samples to time in microseconds


% Plot Magnetic data
BdotPlot = figure(32); 
clf
label = {'x' 'y' 'z'};
B_clims = {[-1 8], [-13 5], [-5 8]};
% B_clims = {[-1 1], [-4 1], [-2 4]};
dB_clims = {[-0.3 0.3]*1e3, [-0.4 0.3]*1e3, [-0.4 0.3]*1e3};
sgtitle(sprintf('Flux Probe Stick %d, Y = %0.1f, %s', ClosestStick, ClosestStickYpos, DipoleType))
indx1 = 0;
indx2 = 0;
for ii = 1:6
    if ~mod(ii,2) % choose only even number indices
        indx1 = indx1 + 1;
        subplot(3,2,ii)
        pcolor(time_daq, ZposVec, Bset{indx1}*1e3), shading interp
        hold on
        % contour(time_daq, ZposVec, Bset{indx1}*1e3, [0 0], 'k')
        laxis = yline(DipoleEdgeZpos, '--r', 'LineWidth', 2);
        colorbar
        clim(B_clims{indx2})
        ylabel('Z[m]')
        xlabel(sprintf('\\Deltat[\\mus]'))
        title(sprintf('B%s[mT], X = %0.2fm', label{indx1}, ProbeXpos{indx1}))
        legend(laxis, {'Dipole Edge'},'location', 'southeast')
    else
        indx2 = indx2 + 1;
        subplot(3,2,ii)
        pcolor(time_daq, ZposVec, dBset{indx2}), shading interp
        hold on
        laxis2 = yline(DipoleEdgeZpos, '--r', 'LineWidth', 2);
        colorbar
        clim(dB_clims{indx2})
        ylabel('Z[m]')
        xlabel(sprintf('\\Deltat[\\mus]'))
        title(sprintf('dB%s[T/s], Probe Position X = %0.2fm', label{indx2}, ProbeXpos{indx2}))
        legend(laxis2, {'Dipole Edge'})
    end
end

% Plot Te data
TePlot = figure(33); 
clf
sgtitle(sprintf('%s', DipoleType))
subplot(1,2,1)
pcolor(time, TeZvec, Te1kV)
shading interp
colorbar
hold on
laxis = yline(DipoleEdgeZpos, '--r', 'LineWidth', 2);
clim([0 30])
ylabel('Z[m]')
xlabel(sprintf('\\Deltat[\\mus]'))
title('Te [eV]')
legend(laxis, {'Dipole Edge'},'location', 'southeast')


subplot(1,2,2)
% shots to skip plotting error region
for ii = 1:length(shotList)
    TeMax = max(Te1kV,[], 'all');
    TeNorm = (ii)+(Te1kV(ii,:)/TeMax); 
    % Plot the error region as a shaded area
    if ii ~= 2 && ii ~=3 && ii ~=4 && ii ~=9  
        Te_minusNorm = (ii)+(Te_minus(ii,:)/TeMax);
        Te_plusNorm = (ii)+(Te_plus(ii,:)/TeMax);
        % Fill the region between the error bounds Te_minusNorm and Te_plusNorm
        inbetween = [Te_minusNorm fliplr(Te_plusNorm)];
        time2 = [time fliplr(time)];
        fill(time2, inbetween, 'r', 'EdgeColor', 'none', 'FaceAlpha', 0.2)
        hold on
        plot(time, Te_plusNorm, 'r')
        plot(time, Te_minusNorm, 'r')   
        xlim([min(time+5) max(time)])
    end
    plot(time, TeNorm, 'linewidth', 1.1)
    xlabel(sprintf('\\Deltat[\\mus]'))
    ylim([0 14])
    yticks([])
    title('Te [eV]')
end

% Plot ne data
nePlot = figure(34);
clf
subplot(1,2,1)
pcolor(time, TeZvec, ne1kV), shading interp
colorbar
hold on
laxis = yline(DipoleEdgeZpos, '--r', 'LineWidth', 2);
clim([5e15 5e18])
ylabel('Z[m]')
xlabel(sprintf('\\Deltat[\\mus]'))
title('ne [m^{-3}]')
legend(laxis, {'Dipole Edge'},'location', 'southeast')
set(gca,'ColorScale','log')

subplot(1,2,2)
for ii = 1:length(shotList)
    neMax = max(ne1kV,[], 'all');
    ne_Norm = (ii)+(ne1kV(ii,:)/neMax);
    % Plot the error region as a shaded area
    if ii ~= 2 && ii ~=3 && ii ~=4 && ii ~=1 %&& ii ~= 7 && ii ~= 8 ...
       % && ii ~= 10 && ii ~= 11 && ii ~= 12 && ii ~= 13 && ii ~= 5
        ne_minusNorm = (ii)+(ne_minus(ii,:)/neMax);
        ne_plusNorm = (ii)+(ne_plus(ii,:)/neMax);
        % Fill the region between the error bounds 
        inbetween = [ne_minusNorm fliplr(ne_plusNorm)];
        time2 = [time fliplr(time)];
        fill(time2, inbetween, 'r', 'EdgeColor', 'none', 'FaceAlpha', 0.2)
        hold on
        plot(time, ne_plusNorm, 'r')
        plot(time, ne_minusNorm, 'r')   
        xlim([min(time+5) max(time)])
    end 
    plot(time, ne_Norm, 'linewidth', 1.1)
    hold on
    xlabel(sprintf('\\Deltat[\\mus]'))
    ylim([0 14])
    yticks([])
    title('ne [m^{-3}]')
end

% Plot Vplasma data
VplasmaPlot = figure(35);
clf
subplot(1,2,1)
pcolor(time, TeZvec, Vplasma1kV), shading interp
colorbar
hold on
laxis = yline(DipoleEdgeZpos, '--r', 'LineWidth', 2);
clim([-20 130])
ylabel('Z[m]')
xlabel(sprintf('\\Deltat[\\mus]'))
title('V_{plasma} [V]')
legend(laxis, {'Dipole Edge'},'location', 'southeast')

subplot(1,2,2)
for ii = 1:length(shotList)
    VplasmaMax = max(Vplasma1kV,[], 'all');
    Vplasma_Norm = (ii)+(Vplasma1kV(ii,:)/VplasmaMax);
    % Plot the error region as a shaded area
    if ii ~= 2 && ii ~=3 && ii ~=4 && ii ~= 1 %&& ii ~= 7 && ii ~= 8 ...
       % && ii ~= 10 && ii ~= 11 && ii ~= 12 && ii ~= 13 && ii ~= 5
        Vplasma_minusNorm = (ii)+(Vplasma_minus(ii,:)/VplasmaMax);
        Vplasma_plusNorm = (ii)+(Vplasma_plus(ii,:)/VplasmaMax);
        % Fill the region between the error bounds 
        inbetween = [Vplasma_minusNorm fliplr(Vplasma_plusNorm)];
        time2 = [time fliplr(time)];
        fill(time2, inbetween, 'r', 'EdgeColor', 'none', 'FaceAlpha', 0.2)
        hold on
        plot(time, Vplasma_plusNorm, 'r')
        plot(time, Vplasma_minusNorm, 'r')   
        xlim([min(time+5) max(time)])
    end
    plot(time, Vplasma_Norm, 'linewidth', 1.1)
    hold on
    xlabel(sprintf('\\Deltat[\\mus]'))
    ylim([0 14])
    yticks([])
    title('V_{plasma} [V]')
end

% Plat ne_isat
physicalConstants

ne_isatPlot = figure(36);
clf
subplot(1,2,1)
pcolor(time, TeZvec, ne_isat), shading interp
colorbar
hold on
laxis = yline(DipoleEdgeZpos, '--r', 'LineWidth', 2);
clim([5e15 5e18])
ylabel('Z[m]')
xlabel(sprintf('\\Deltat[\\mus]'))
title(sprintf('ne [m^{-3}] from I_{sat} with Te = %d [eV]', Te_isat(1)/q))
legend(laxis, {'Dipole Edge'},'location', 'southeast')
set(gca,'ColorScale','log')

subplot(1,2,2)
for ii = 1:length(shotList)
    neMax = max(ne_isat,[], 'all');
    ne_Norm = (ii)+(ne_isat(ii,:)/neMax);
    % Plot the error region as a shaded area
    plot(time, ne_Norm, 'linewidth', 1.1)
    hold on
    xlabel(sprintf('\\Deltat[\\mus]'))
    ylim([0 14])
    yticks([])
    title('ne [m^{-3}]')
end

if savePlots == 1
    saveas(BdotPlot, fullfile(plotFolder, sprintf('Bdot_for_IVplot_%s', DipoleType)), 'png')
    saveas(TePlot, fullfile(plotFolder, sprintf('Te_pcolorIVplot_%s', DipoleType)), 'png')
    saveas(nePlot, fullfile(plotFolder, sprintf('ne_pcolorIVplot_%s', DipoleType)), 'png')
    saveas(VplasmaPlot, fullfile(plotFolder, sprintf('Vplasma_pcolorIVplot_%s', DipoleType)), 'png')
end