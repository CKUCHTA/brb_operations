% General function for fitting the Vprb and Iprb at a specified sample (or time) 
% and outputing Te, ne, and V_plasma. This version uses robust fit and
% exludes outlier that are greater than 1.5 std from initial fit.
% Last edited 28May2024

function [Vp, Ip, fitfun, options, Working, Vplasma, ne, Te, di, confidence, fit2, ...
    outliers, diError, neError, ne_isat, Te_isat] = LangmuirFit(Iprobe, Vprobe, Sample, Working, exculdeOutliers, neStart, TeStart, VplasmaStart)
    % clear
    % shot = 64162;
    % Sample = 32925;
    % exculdeOutliers = 0;
    % neStart = 1e16;
    % TeStart = 3;
    % VplasmaStart = 10;
    % Working = [2 4 6 8 9 11 13];
    % [Iprobe, Vprobe, ~, dB1, dB2, dB3, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~] = loadTe1Data(shot);
    
    physicalConstants % load constants
    Iprobe = Iprobe(:,Working);
    Vprobe = Vprobe(:, Working);
    
    AreaPrb = 7.2788e-06; % Area of probe

    warning('off','curvefit:fit:nonDoubleXData');
    warning('off','curvefit:fit:nonDoubleYData');
    Ip = Iprobe(Sample,:); % probe current at specified sample value
    Ip = -Ip'; % take transpose for fit routine. Multiply by negative to compare to book plots
    Vp = Vprobe(Sample,:);
    Vp = Vp';

    %%%%%%%%%%%%%%%%%%%%%%%%%% Calculate ISAT %%%%%%%%%%%%%%%%%%%%%%%%%%
    NumTipsInISAT=3; % number of tips to use in ISAT calculation
    % find three most negative probes
    indx = find(Vp < 0);
    indx = indx(1:NumTipsInISAT);
    
    % fit a line to the most negative probes
    Visat = Vp(indx);
    Iisat = Ip(indx);
    PP = polyfit(Visat,Iisat,1);
    ifit = PP(1)*Visat+PP(2); % fit line to ISAT data

    isat = mean(ifit(:));

    % Calculate density from ISAT using a set Te value
    Te_isat = q*15;  % set Te in eV 
    ne_isat = (abs(isat)/(q*AreaPrb))*sqrt(mi/Te_isat);
    fprintf('ne_isat = %e\n',ne_isat)

    % % plot vfit and ifit overlayed on ifit
    % figure(24)
    % plot(Visat, Iisat, '*b')    
    % hold on
    % plot(Visat, ifit, '-r')
    % xlabel('V')
    % ylabel('I')

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % define the function to fit data to
    fitfun  = fittype( @(ne, Te, V_plasma, V_prb) ...
        (1e18*ne * q * AreaPrb) * (q * Te / mi)^0.5 * ...
        (0.5 * ((2 * mi) / (pi * me))^0.5 *exp((V_prb - V_plasma) / Te) - exp(-0.5)) ...
        ,'independent', 'V_prb', 'dependent', 'I_prb');

    options = fitoptions(fitfun);
    options.StartPoint = [1e-18*neStart, TeStart, VplasmaStart]; %set initial start values for ne, T_e, and V_plasma
    options.Lower = [0.0001,0.1, -100]; %set lower bounds for ne, T_e, and V_plasma
    options.Upper = [100,100,330]; %set upper bounds for ne, T_e, and V_plasma

    % % use try for time point where no values for the fit parameters are found
     if exculdeOutliers == 1
        options.Robust = 'on'; % weighted fit.  Data is weighted based on distance from fitfun
    else
        options.Robust = 'off'; % weighted fit off.  Data is weighted based on distance from fitfun
    end
    try
        [~, ~, fitinfo] = fit(Vp , Ip, fitfun, options); % fit data to fitfun
    
        if exculdeOutliers == 1
            residuals = fitinfo.residuals; % residual = data - fit
            II = abs( residuals) > 1.5 * std( residuals ); % indexes of rediduals that are 1.5 std from fit
            outliers = excludedata(Vp, Ip,'indices',II);
            options.Exclude = outliers;
            fit2 = fit(Vp, Ip, fitfun, options); % fit data without outliers
    
            %  Calculated output values
            Vplasma = fit2.V_plasma;
            ne = 1e18*fit2.ne;
            Te = fit2.Te;
            con1 = sqrt(mi/(mu0*q^2));
            di = con1/sqrt(ne); % ion skin dept
            confidence = confint(fit2); % confidence interval of parameters
            neError = abs(confidence(1,1) - confidence(2,1));
            diError = (con1*neError)/(2*ne^(3/2));
        else
            outliers = [];
            options.Exclude = outliers;
            fit2 = fit(Vp, Ip, fitfun, options); % fit data without outliers
    
            %  Calculated output values
            Vplasma = fit2.V_plasma;
            ne = 1e18*fit2.ne;
            Te = fit2.Te;
            con1 = sqrt(mi/(mu0*q^2));
            di = con1/sqrt(ne); % ion skin dept
            confidence = confint(fit2); % confidence interval of parameters
            neError = abs(confidence(1,1) - confidence(2,1));
            diError = (con1*neError)/(2*ne^(3/2));
        end
    
    catch
        Vplasma = NaN;
        ne = NaN;
        Te = NaN;
        di = NaN;
        diError = NaN;
        neError = NaN;
        confidence = NaN(2,3);
        fit2 = NaN;    
        outliers = NaN;
    
    end
    