% fit the Te data to the exponetial IV curve using Hutchinson equation 3.2.33. This script
% uses SaveIVcurveData.m to first run LangmuirFit.m to find the plasma parameters from the IV curve data,
% then plots the IV curve data with the fit and saves the plot as a movie.  The parameters for each shot
% are saved in a structure array with fields for each parameter. 
% created 31Dec22
% Last Edited 28Mar2024

clear
saveMovie = 1;
exculdeOutliers = 0; % 1 = exclude outliers from fit, 0 = include outliers in fit
ParentFolder = 'DipoleCuspVertical_FullScan';
OnePerPosition = 0; % 1 = chooses one shot from set of shots with same flux probe position, 0 = all shots
Working = [2 4 6 8 9 11 13]; % Working tips for the shot
webSave = 0; % 1 to download the google sheets data, 0 to use the saved data

[PlasmaShotLists, VacuumShotLists, shotData] = CreateShotList_dipole(ParentFolder, OnePerPosition, webSave);

shotList_DriveAndDipole1kV = PlasmaShotLists{1}(:,1)';
shotList_DriveAndDipole2kV = PlasmaShotLists{2}(:,1)';
shotList_DriveOnly = PlasmaShotLists{3}(:,1)';

AllShotList = [shotList_DriveAndDipole1kV shotList_DriveAndDipole2kV shotList_DriveOnly];

shotlist = [64148 64149 64150 64151 64152 64153 64154 64155 64160 64161 ...
64162 64163 64164 64165 64166 64167 64168];

shotList = [64166 64153 64222 64235 64270 64285 64302 64314 64403 64371 64359 64479 64496]; % Te shot list, 2kV Dipole Cap full scan

for shot = shotList
    if shot<64147 || shot == 64369 || shot == 64425 || shot == 64254 ...
        || shot == 64255 || shot == 64256 || shot == 64257 || shot == 64258 ...
        || shot == 64363 || shot == 64283 || shot == 64174 || shot == 64175 ...
        || shot == 64176 || shot == 64177 || shot == 64178 || shot == 64179 ...
        || shot == 64190 || shot == 64191 || shot == 64192 || shot == 64193 ...
        || shot == 64194 || shot == 64195 || shot == 64180 || shot == 64181 ...
        || shot == 64196 || shot == 64197 || shot == 64204 || shot == 64207 % skip bad shots
        continue
    end
    shotIndx = find(shot == shotData(:,1));
    dipoleCapV = shotData(shotIndx, 7);
    if dipoleCapV == 1
        dipoleType = 'Dipole1kV';
    elseif dipoleCapV == 2
        dipoleType = 'Dipole2kV';
    else
        dipoleType = 'DriveOnly';
    end
    Langmuir_Scan(shot, saveMovie, ParentFolder, dipoleType, exculdeOutliers, Working);
end