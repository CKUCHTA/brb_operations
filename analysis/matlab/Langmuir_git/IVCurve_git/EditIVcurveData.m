% This script loads IVData%s.mat file that was created in SaveIVCuverData.m to edit an indivudual shot's 
% IV curve data.  The script edits the shot by using starting points from shots that are closest in
% Z position to the shot being edited.  The script resaved IVData%s.mat and runs SaveAllandPlot_IVCurveData.m 
% to recalculate All_IVData structure array with the edited data.
% created 01June2024

clear
%%%%%%%%%%%%%%%%%%%%%%%% Input Parameters %%%%%%%%%%%%%%%%%%%%%%
SampleRange = 32925:33056; % range of samples to be used for the IV curve fit
exculdeOutliers = 0; % 1 = exclude outliers from fit, 0 = include outliers in fit
ParentFolder = 'DipoleCuspVertical_FullScan';

EditShot = 64493; % shot to be changed
fixStartPoints = 1; % 1 = use start points from another shot at same sample point. 0 = use start points from same shot at previous time step
% fixWorkingTips = 1; % 1 = fix the working tips for the shot being edited

Working = [2 4 6 8 9 11 13]; % Working tips for the shot. DO NOT change this if you want to change working tips.  Use ChangeWorkingtips instead
if EditShot == 64368
    NewWorking = [2 4 6 9 11 13]; % New working tips for the shot
    EditTipWorkingSet = {[2 6 8 9 11 13] [2 4 6 8 9 13]}; % Working tips for the shot to be used for Vp and Ip starting points
    ChangeWorkingSampleSet = {32981:32990 33010:33013}; % Sample number to change the working tips
elseif EditShot == 64162
    NewWorking = [2 4 6 9 11 13]; % New working tips for the shot
    EditTipWorkingSet = {[2 4 6 9 11]};
    ChangeWorkingSampleSet = {[33031:33046]};
elseif EditShot == 64217
    NewWorking = [2 4 6 9 11 13]; % New working tips for the shot
    EditTipWorkingSet = {[2 4 6 9 11]};
    ChangeWorkingSampleSet = {[33021:33054]};
elseif EditShot == 64328
    NewWorking = [2 4 6 9 11 13]; % New working tips for the shot
    EditTipWorkingSet = {[2 4 6 9 11]};
    ChangeWorkingSampleSet = {[32982:33031]};
else
    NewWorking = Working;
    EditTipWorkingSet = [];
    ChangeWorkingSampleSet = [];
end
PlotIV = 1; % 1 = plot the IV curve data, 0 = do not plot the IV curve data
saveMovie = 1; % 1 = save the IV curve data as a movie, 0 = do not save the IV curve data as a movie
SaveChanges = 1; % 1 = save the changes to the All_IVData.mat file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FolderStructure = what(ParentFolder);
FolderPath = FolderStructure.path;
DipoleType = 'Dipole1kV'; % 'Dipole1kV', 'Dipole2kV', 'Dipole0kV'ismember(Sample, ChangeWorkingSample)

% Load IV data
IVdataFolder = fullfile(FolderPath, '3DFarray','Matlab_data', 'IVdata');
IVShotFolder = fullfile(IVdataFolder,'IVshotdata');

if strcmp(DipoleType, 'Dipole1kV') % shot 64233 is bad
    shotList = [64150 64162 64219 64233 64267 64278 64454 64427 64326 64368 64366 64477 64494]; % Te shot list, 1kV Dipole Cap full sca
% Te shot Position [-34.2 -31.4 -28.4 -25.4 -22.2 -19.4 -16.6 -13.9 -10.6 -7.9  -4.7  -1.7  1.1
    PlasmaShotList = [64326 64342 64357 64368 64398 64425 64454 64476 64492]; % 1kV Dipole Cap full scan
elseif strcmp(DipoleType, 'Dipole2kV')
    shotList = [64164 64152 64220 64235 64271 64287 64302 64314 64330 64345 64359 64479 64497]; % Te shot list, 2kV Dipole Cap full scan
    PlasmaShotList = [64329 64345 64358 64370 64400 64428 64456 64479 64495]; % 2kV Dipole Cap full scan]
elseif strcmp(DipoleType, 'Dipole0kV')
    shotList = [64167 64155 64223 64238 64272 64289 64304 64318 64333 64376 64361 64484 64499]; % Te shot list, Drive Only full scan
    PlasmaShotList = [64332 64348 64361 64376 64406 64434 64464 64483 64498]; % Drive Only full scan
end


if fixStartPoints == 1
    StartPointShotIndx = find(EditShot == shotList);
    StartPointShot = shotList(StartPointShotIndx - 1);
    
    % Load IV data from the shot to be used for Vp and Ip starting points
    load(fullfile(IVShotFolder, sprintf('IVData_%d.mat', StartPointShot)), 'IVdata')
    try
        StartTe = [IVdata(:).Te]';
        Startne = [IVdata(:).ne]';
        StartVplasma = [IVdata(:).Vplasma]';
        fprintf('Te and ne data will be used from Shot %d on Shot %d\n', StartPointShot, EditShot)
    catch
        fprintf('Vp and Ip data not found\nRecalculated data for Shot %d', StartPointShot)
        return
    end
    % rerun SaveIVcurveData.m with the new starting points and change working tips for specific sample points
    % StartTe = [];
    % Startne = [];
    % StartVplasma = []; 
    IVdata = SaveIVcurveData(EditShot, SampleRange, ParentFolder, exculdeOutliers, ...
    StartTe, Startne, StartVplasma, NewWorking, EditTipWorkingSet, ChangeWorkingSampleSet);
    if PlotIV == 1
        disp('Plotting IV curve data')
        IVCurvePlot = Langmuir_Scan(EditShot, saveMovie, ParentFolder, DipoleType,  exculdeOutliers, Working);  
    end
else
    % load the IV data and change the working tips for all time, NOT for a specific sample
    IVdata = SaveIVcurveData(EditShot, SampleRange, ParentFolder, exculdeOutliers, [], [], [], NewWorking, [], []);
    if PlotIV == 1
        disp('Plotting IV curve data')
        IVCurvePlot = Langmuir_Scan(EditShot, saveMovie, ParentFolder, DipoleType,  exculdeOutliers, NewWorking);  
    end 
end


% Change a single shot's data in All_IVData.mat, which is created in SaveAllandPlot_IVCurveData.m.
if SaveChanges == 1
    % Load the All_IVData structure array
    load(fullfile(IVdataFolder, sprintf('All_IVData%s.mat', DipoleType)), 'All_IVData');
    shotIndx = find([All_IVData(:).shot] == EditShot);
    if isempty(shotIndx)
        disp('Shot not found in All_IVData')
        returnF
    end

    % All_IVData(shotIndx).shot = IVdata.shot;
    All_IVData(shotIndx).Te = [IVdata(:).Te]';
    All_IVData(shotIndx).ne = [IVdata(:).ne]';
    All_IVData(shotIndx).Vplasma = [IVdata(:).Vplasma]';
    All_IVData(shotIndx).di = [IVdata(:).di]';
    All_IVData(shotIndx).Te_confidence = [IVdata(:).Te_confidence]';
    All_IVData(shotIndx).ne_confidence = [IVdata(:).ne_confidence]';
    All_IVData(shotIndx).Vplasma_confidence = [IVdata(:).Vplasma_confidence]';
    All_IVData(shotIndx).di_error = [IVdata(:).di_error]';

    save(fullfile(IVdataFolder, sprintf('All_IVData%s.mat', DipoleType)), 'All_IVData')
end