% Load Te data that is on external harddrive if it exist.  If it does not
% exist, load data from MDSPlus and save it to Temporary folder on external
% harddrive.

function [Iprobe, Vprobe, Working, dB_ax1, dB_ax2, dB_ax3, dBp, dBr, dBz, Area, TeNoise, TeSig, Vbias, Rsense, Rdiv, Rdiggnd, Bh] = loadTeData(shot)
% clear
% shot = 63799;

DataFolder = '/Users/paulgradney/Research/Data';
TemporaryFolder = 'Temporary/Te_Data';

TeFile = strcat(num2str(shot),'_TeData.mat');
TeFilePath = which(TeFile); % path of TeData file

try
   load(TeFilePath, 'Iprobe', 'Vprobe', 'Working', 'dB_ax1', 'dB_ax2', ...
       'dB_ax3', 'dBp', 'dBr', 'dBz', 'Area', 'TeNoise', 'TeSig', 'Vbias', ...
       'Rsense', 'Rdiv', 'Rdiggnd', 'Bh');
catch
    disp(['Could not find file: ', TeFile])
    % load data from mdsplus
    [Iprobe, Vprobe, Working, dB_ax1, dB_ax2, dB_ax3, dBp, dBr, dBz, Area, TeNoise, TeSig, Vbias, Rsense, Rdiv, Rdiggnd, Bh] = Te_data(shot);
    shot_data_file = sprintf('%d_TeData.mat',shot);
    fileName = fullfile(DataFolder, TemporaryFolder, shot_data_file);
    save(fileName)
end