% download Te data from mdsplus.  This needs to be updated such that only
% the data sample window needed is downloaded rather than all the data.

function [Iprobe, Vprobe, Working, dB_ax1, dB_ax2, dB_ax3, dBp, dBr, dBz, Area, TeNoise, TeSig, Vbias, Rsense, Rdiv, Rdiggnd, Bh] = Te1_data(shot)
% clear
% shot = 63807;

mdsconnect('skywalker.physics.wisc.edu');
mdsopen('wipal', shot)
 
Bh = mean(mdsvalue('.RAW.A373_DIRECT:CH_43')*200*0.3521)*1e-4; % Helmholtz in T
Area =  mdsvalue('\TE1_ACT_AREA'); % area of probe
Iprobe = mdsvalue('\TE1_IPROBE'); % current entering probe tip 
Vprobe = mdsvalue('\TE1_VPROBE'); % potential of probe tip
Working = mdsvalue('\TE1_WORKING'); % index of probes that are working

%Bdot data
dB_ax1 = mdsvalue('\TE1_BDOT_AX1');
dB_ax2 = mdsvalue('\TE1_BDOT_AX2');
dB_ax3 = mdsvalue('\TE1_BDOT_AX3');

dBp = mdsvalue('\TE1_BDOT_AXPHI');
dBr = mdsvalue('\TE1_BDOT_AXR');
dBz = mdsvalue('\TE1_BDOT_AXZ');


% grabing raw data
Vbias = []; 
TeNoise = [];
TeSig = [];
Rsense = [];
Rdiv = [];
Rdiggnd = [];
all_tips = 1:16;

for tip = all_tips
    Vraw = mdsvalue(['\te1_' num2str(tip, '%02d') '_vbias_raw']);
    Voffset = mdsvalue(['\te1_' num2str(tip, '%02d') '_vbias_off']);
    Vdiv = mdsvalue(['\te1_' num2str(tip, '%02d') '_vbias_div']);
   
    Vbias(:,tip) = (Vraw - Voffset)./Vdiv; % calcualted Vbias based
    TeNoise(:,tip)= mdsvalue(['kinetics.te_probe1.tip' num2str(tip,'%02d') '.vmeas.raw']);
    TeSig(:,tip) = mdsvalue(['kinetics.te_probe1.tip' num2str(tip, '%02d') '.vmeas.adjnoise.raw']); % tip raw voltage
    Rsense(:,tip) = mdsvalue(['\te1_' num2str(tip, '%02d') '_rsense']);
    Rdiv(:,tip) = mdsvalue(['\te1_' num2str(tip, '%02d') '_rdiv']);
    Rdiggnd(:,tip) = mdsvalue(['\te1_' num2str(tip, '%02d') '_rdiggnd']);
end


mdsclose;
mdsdisconnect;