# matlab

This directory contains files to be run in matlab to look at probe data.

## Git basics

Git is a software to track changes to code. GitHub or GitLab are both servers that store those updates. There are a few commands we need to use to get updates from the server and send changes on our computers up to the server. The most important thing to do is

***READ THE OUTPUT AFTER YOU WRITE A COMMAND!***

Here are a couple different workflows you might go through.

### Send changes on your computer to the server

1. Open terminal.
2. Use `cd` to change your directory until you are in a subdirectory of `brb_operations/`. 

    - An example command may look like `cd ./Documents/brb_operations/`.
    - You can always check where you are located in the file system by using `pwd`.

3. Type `git fetch`. This will get updates from the server. None of your code will be changed.

4. Type `git status`. This will tell you the status of your computer in relation to the server. There may be lots of output but **YOU BETTER READ THE OUTPUT**! Let's split it into parts.

    - The top line should read `On branch master`. That means you are getting updates correctly. If you don't see that and you are on a different branch, ruh-roh! Figure out how to 'git change branch to master' (i.e. search that phrase on your favorite search engine).
    - The next set of lines could say a variety of things.
        - My favorite is when it says something like `you are up to date`. Then you don't have to do anything!
        - It may say you are behind on commits. Then we need to get those server updates.
        - It may say you are ahead on commits. This is just fine as we are going to add some more commits.
        - It may say you are behind and ahead! This can be an issue as you may have changes on your computer and changes on the server that are incompatible.
    - There may be a set of lines that starts with `Changes to be committed` and has some text in green. These are changes to files on your computer that need to be put into a commit.
    - There may be a set of lines that starts with `Changes not staged for commit`. These are changes to files that are already on the server that need to be committed.
    - There may be a set of lines that starts with `Untracked files`. These are files that are not on the server that need to be sent to the server by using a commit.

5. Now we will construct out commit (which is like an update). Type `git add .`. This will add all your changed files to the commit. If you want to be picky you can type `git add ...` where `...` is the path to the file you want to add.

6. Type `git status`. Now you should have some more green files under the `Changes to be committed` header.

7. Type `git commit -m "..."`. Replace `...` with a short message about your update. This will make a commit on your computer. This update isn't on the server yet!

8. *If you are behind on commits*, you need to commit **all** of your changes. Then run `git pull`. This will get the new updates onto your computer. Run `git status`. There may be some files in red at the bottom. These are files that had competing changes. Look them over and make the necessary changes. Once done, type `git add .`. Now type `git commit`. You will enter into a vi editor. ONLY TYPE `:x` AND THEN PRESS ENTER.

9. Now type `git push`. This will send your updates to the server.

Congratulations! You did it! Now do a little dance.
