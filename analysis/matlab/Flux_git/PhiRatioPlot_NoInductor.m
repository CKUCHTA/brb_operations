clear
%plot the flux ratio from groups of shots with no inductor.  This ratio is calcualted from
%the average if a few shots.  The flux ratio values are found by averaging
%each flux value than calculating the ratio.  A better way for calculating
%this is done in PlotAllFlux_No_Inductor

DataFolder = '/Users/paulgradney/Research/Drive_Cylinder/MATLAB/From_Jabba/Gradney/Data';
Psi_data_folder = 'Flux_output/PsiXratio3';

% DataFolder = '/Users/paulgradney/Research/Drive_Cylinder/MATLAB/From_Jabba/Gradney/Data';
% Psi_data_folder = 'Data__Cfactor/PsiXratio_Induc_newFac';

excel = 'TREX_Drive_Cylinder_28July2022.xlsx';
Psi_filename = fullfile(DataFolder, Psi_data_folder);

%Grab excel data
excel_filename = fullfile(DataFolder, excel);
[spread] = readtable(excel_filename);
spread(:,{'number', 'SHProbePos', 'LinearPosition', ...
    'TePos', 'MeasureSH', 'MeasureLinear', 'GunFails','Notes'}) = []; % Delete unneeded columns 
spread = table2array(spread);

load(Psi_filename);
[len, ~] = size(PsiXratio_data);
psi = struct();

%create a structure from the array and spread sheet
for i=1:len
    psi(i).shot = PsiXratio_data(i,1);
    psi(i).PsiXratio = PsiXratio_data(i,2);
    psi(i).Bh = PsiXratio_data(i,3);
    psi(i).rmax = PsiXratio_data(i,4);
    psi(i).rmin =PsiXratio_data(i,5);
    psi(i).Psi50 = PsiXratio_data(i,6);
    psi(i).Psi25 = PsiXratio_data(i,7);
    
    ii = find(spread==psi(i).shot);
    psi(i).CT = spread(ii,3);
    psi(i).TREX = spread(ii,2);

end

shotnum = [psi.shot];
Bh = round([psi.Bh]*1e4); %converted to Gauss
CT = [psi.CT];
Psi50 = [psi.Psi50];
Psi25 = [psi.Psi25];

Bh_val = [40 50 60 70 80 90 100]; %Possible Bh values
CT_val = [5 6 7 8 9 10];




PsiAvg50 = [];
PsiAvg25 = [];
Ratio = struct();
h = 1;
for j = 1:length(CT_val)
    for i = 1:length(Bh_val)
        Psi50_val = Psi50(Bh==Bh_val(i) & CT==CT_val(j));
        Psi25_val = Psi25(Bh==Bh_val(i) & CT==CT_val(j));
        Ratio.PsiAvg50(h) = mean(Psi50_val);
        Ratio.PsiAvg25(h) = mean(Psi25_val);
    
        Ratio.Bh(h) = Bh_val(i);
        Ratio.CT(h) = CT_val(j);
        Ratio.PsiRatio(h) = round(((Ratio.PsiAvg50(h)-Ratio.PsiAvg25(h))/Ratio.PsiAvg50(h))*100);
        h = h+1;
    end
end

% Bh5 = psi.Bh;

% Define arrays from structure


% PsiRatio = zeros(1, length([psi.PsiXratio]));
% k = 1;
% j = 1;
% 
% %average over shots with same Bh and CT values
% while j < (length(Bh)-1)
%     if Bh(j) == Bh(j+1)
%         if CT(j) == CT(j+1)
%             PsiAvg50(k) = (abs(Psi50(j)) + abs(Psi50(j+1)) + abs(Psi50(j+2)))/3;
%             PsiAvg25(k) = (abs(Psi25(j)) + (Psi25(j+1)) + abs(Psi25(j+2)))/3;
%             
%             Ratio.CT(k)=CT(j);
%             Ratio.Bh(k)=Bh(j);
%             Ratio.PsiRatio(k) = round(((PsiAvg50(k)-PsiAvg25(k))/PsiAvg50(k))*100);
%             k=k+1;
%             j=j+3;
%         end
%     end
% end

%find all runs with same CT values
CTin = [];
CT4 = find([Ratio.CT]==4);
CT5 = find([Ratio.CT]==5);
CT6 = find([Ratio.CT]==6);
CT7 = find([Ratio.CT]==7);
CT8 = find([Ratio.CT]==8);
CT9 = find([Ratio.CT]==9);
CT10 = find([Ratio.CT]==10);

x = linspace(40,100);
y = zeros(length(x),1);
figure(33)
clf
plot(Ratio.Bh(CT5), Ratio.PsiRatio(CT5),'r', 'linewidth',2)
hold on
plot(Ratio.Bh(CT6), Ratio.PsiRatio(CT6), 'linewidth',2)
plot(Ratio.Bh(CT7), Ratio.PsiRatio(CT7), 'linewidth',2)
plot(Ratio.Bh(CT8), Ratio.PsiRatio(CT8), 'linewidth',2)
plot(Ratio.Bh(CT9), Ratio.PsiRatio(CT9), 'linewidth',2)
plot(Ratio.Bh(CT10), Ratio.PsiRatio(CT10), 'linewidth',2)
set(gca,'FontSize', 26)
plot(x,y,'--k','HandleVisibility','off')
xlabel('Helmholtz Field','FontSize', 26)
ylabel('f(\Psi)','FontSize', 26)
title('f(\Psi) at R = 30cm and 48cm','FontSize', 26)
 legend('5k','6k','7k','8k','9k','10k')