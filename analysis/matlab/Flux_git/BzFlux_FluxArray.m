% Calcultes the flux for Bz using speed probe data AFTER Bz has been
% transposed. This calculates Psi with and without probes at r<0.  PsiR0
% only includes probes for r>0.  Use rvecR0 with PsiR0 if you want to use
% that flux.
% Last edited 17Jan2024

function [flux] = BzFlux_FluxArray(Bz, rvec)
% clear
% % shot = 62271;
% shot = 62259;
% SamRange=69000:71000;
% IIm=940:1040; % times for evaluating the off-sets on the digitizers (before the reconnection pulse)
% dt=1e-7; % time between samples
% IIoff=1000; %Makes Bz=Bh at this index
% [daq, Bh] = loadDtaqData3DFarray(shot); % load dtac from harddrive or from mdsplus
% [BSet, dBSet, ProbePos , SamRange, Bh, tpoints] = load3DFarray(daq, Bh, SamRange, dt, IIm, IIoff);
% BzAll = BSet{1,3};
% % Bz = BzAll(7000:7400,:); % 1D Flux Array range
% Bz = BzAll(1000:1400,:,1); % 3D Flux array range stick 1
% rvec = ProbePos{3};

dr=rvec(2)-rvec(1);
IndR0 = find( rvec > 0, 1, 'first') - 1; % index of first r value less than 0 (subtracting 1 get the first negative rather than first positive value)
RBz=Bz*0; % matrix of ones with dimensions Bz

for k=1:size(rvec,2)
    RBz(:,k)=Bz(:,k)*rvec(k); %All probes
end
Psi=cumtrapz(RBz,2)*dr;

for rIndx = 1:size(Psi,2)
    Psi(:,rIndx) = Psi(:,rIndx) - Psi(:,IndR0); % force Psi to zero at R = 0
end
flux = 2*pi*Psi; % convert to flux function to flux

% figure(1),clf
% pcolor(1:size(flux,1),rvec,flux'), shading interp
% hold on
% contour(1:size(flux,1),rvec,flux',[0 0],'r', 'Linewidth', 1.5)
% % contour(flux)
% colorbar

