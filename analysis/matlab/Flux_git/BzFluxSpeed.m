% Calcultes the flux for Bz using speed probe data AFTER Bz has been
% transposed. This calculates Psi with and without probes at r<0.  PsiR0
% only includes probes for r>0.  Use rvecR0 with PsiR0 if you want to use
% that flux.
% Last edited 24Mar23

function [Psi] = BzFluxSpeed(Bz, rvec)

dr=rvec(2)-rvec(1);
RBzAll=Bz*0 + 1; % matrix of ones with dimensions Bz
RBz0 = Bz(3:end,:)*0 + 1;% matrix of ones with dimensions Bz only from r=0

% Calculate integrand for flux function
ii = 2;
for k=1:size(rvec,2)
    ii = ii+1;
    RBzAll(k,:)=Bz(k,:)*rvec(k); %All probes
    try
        RBz0(k,:) =Bz(ii,:)*rvec(ii); % only probes from r>0
    catch
    end
end
Psi=cumsum(RBzAll,1)*dr;
PsiR0 = cumsum(RBz0,1)*dr;
psiBz0 = 0.5*rvec(3)^2.*Bz(4,:); % flux at r =0
PsiR0 = cat(1,psiBz0,PsiR0); % flux from r = 0 only

for k=1:size(Psi,2)
    Psi(:,k)=Psi(:,k)-psiBz0(1,k);  % subtract flux at r=0 from all probes allows us to use probes at r<0
end

Psi = 2*pi*Psi;
end

