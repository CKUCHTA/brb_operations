% This script is called at the end of speedPlot and saves the data for the group PlotAllFlux.
% It plots the flux plots and a set of B-dot graphs.

%rvec = linspace(-0.066, 0.734, 4*15+1)-0*0.04;

dr=rvec(2)-rvec(1);

RBz=BzInt;
for k=1:length(RBz)
    RBz(:,k)=RBz(:,k)*rvec(k);
end
Psi=cumsum(RBz,2)*dr;
ii0=round(interp1(rvec,1:length(rvec),0)); % index for probe a r=0;

for k=1:size(Psi,1)
    Psi(k,:)=Psi(k,:)-Psi(k,ii0);
end




iixplt=1:size(BzInt,2);
% iiyplt=3800:5600;
% iiyplt=3800:5200;


%%%%%%%%%%%%%%%%%%%flux plot%%%%%%%%%%%%%%%%
figure(101),clf


pcolor(Psi),shading interp
colorbar
hold on
contour(Psi,[0 1e3],'r')
hold on
% caxis([-1 1]*1e-3)
xlabel('r[m]')
ylabel('samples')
title('flux')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% iixcont=6:41;
% iixcont=23:30;

%disp([iixcont; rvec(iixcont)])
r12=[.18 0.5]; % set R range for evaluating flux along B=0 contour
r13 = [0 .1]; % set R range for evaluating flux along B=0 contour

rminus=rvec-r12(1);
rplus=rvec-r12(2);
[~,iiminus]=min(abs(rminus));
[~,iiplus]=min(abs(rplus));
disp([iiminus iiplus])
iixcont=iiminus:iiplus;
iixcont=double(ProbePos{1});
[C]=contourc(iixcont,rvec,1e3*BzInt,[0 1e3]);
rX=C(1,2:end);


% figure(1111);
% subplot(3,1,1)
% plot(rX);
rXsmoo=smoo(rX,50);
tX=C(2,2:end); % t values along B=0 contour
%tX=C(2,2:iishort);
tX0=min(tX);
tXend=max(tX);
tXvec=round(tX0):round(tXend);
 [tX,II]=sort(tX);
 tX=tX-(1:length(tX))/1e5;
% tX=-tX;
rX=rX(II); % r values along B=0 contour
rXvec=interp1(tX,rX,tXvec); % finds intep values for rXvex along (tX,rX) at tXvec 

% plot(rXvec,tXvec,'k')
PsiX=interp2(rvec,iiyplt,Psi(iiyplt,iixplt),rXvec,tXvec);

figure(102),clf
cnt=1;
subplot(2,2,1)
plot((tXvec-tX0)/40,PsiX,'b','linewidth',2)
hold on
xlabel('{\Delta}t  [{\mu}s]')
ylabel('\Psi(R,t)_X')
title(['Shot ' num2str(shot)])
xlim([0 5])
ylim([1 5]*1e-4)

subplot(2,2,2)
plot(rXvec,PsiX,'b','linewidth',2)
hold on
xlabel('R [m]')
ylabel('\Psi(R,t)_X')
xlim([0.1 0.5])
ylim([1 5]*1e-4)

rXvecSorted=rXvec;
II=find(isnan(rXvecSorted));
rXvecSorted(II)=0;
rXvecSorted=rXvecSorted+ (1:length(rXvecSorted))/1e6;
[rXvecSorted,II]=sort(rXvecSorted);
PsiXSorted=PsiX(II);
tXSorted=tXvec(II);
rmax = 0.48;
rmin = 0.35;
Psi50=interp1(rXvecSorted,PsiXSorted,rmax);
Psi25=interp1(rXvecSorted,PsiXSorted,rmin);
PsiXratio= round(((Psi50-Psi25)/Psi50)*100);
title(['f(\Psi)_X: ' num2str(PsiXratio) '% for R = ' num2str(rmax) ' to ' num2str(rmin) ])


Erec=gradient(PsiXSorted,dt/4)./rXvecSorted;
%Erec=gradient(PsiXSorted,dt/4);
Erec=smoo(Erec,10);
subplot(2,2,4)
plot(rXvecSorted,Erec,'b','linewidth',2)
hold on
xlabel('R [m]')
ylabel('E_{rec} [V/m]')
xlim([0.1 0.5])
ylim([-2 8]*1e2)

subplot(2,2,3)
plot((tXSorted-tX0)/40,Erec,'b','linewidth',2)
hold on
xlabel('{\Delta}t [{\mu}s]')
ylabel('E_{rec} [V/m]')
xlim([0 5])
ylim([-2 8]*1e2)

% calculate the flux ratio for every R value between rmin and rmax
rdat = linspace(rmax, rmin, 50);
psiX = struct();
psiX.shot = shot;
psiX.rdat = rdat;
psiX.PsiXratio = PsiXratio;
psiX.Bh = Bh;
psiX.rmax = rmax;
psiX.rmin = rmin;
psiX.Psi50 = Psi50;
psiX.Psi25 = Psi25;
psiX.rXvec = rXvec;
psiX.tXvec = tXvec;
psiX.Erec = Erec;
psiX.Psi = PsiX;


for k = 1:length(rdat)
    psiX(k).max=interp1(rXvecSorted,PsiXSorted,rmax);
    psiX(k).min=interp1(rXvecSorted,PsiXSorted,rdat(k));
    psiX(k).Xratio = round(((psiX(k).max-psiX(k).min)/psiX(k).max)*100);
end

% figure(103),clf
% plot(rdat, [psiX.Xratio])
% ylabel('f(\Psi)_X')
% xlabel('R')
% title(['f(\Psi) from R = ' num2str(rmin) 'cm to ' num2str(rmax) 'cm'])

%DataFolder = '/Volumes/DC_DATA/Research/Data/Flux_Output/All_Coils_On';
% DataFolder = '/Volumes/DC_DATA/Research/Data/Flux_Output/Perturbation';
DataFolder = '/Users/paulgradney/Research/Data/Flux_Output/NoPerturbation';
% DataFolder = '/Volumes/DC_DATA/Research/Data/Flux_Output/AllCoils';
% DataFolder = 'C:\Users\MPDX\Documents\Gradney\Data\Flux_output\No_Inductor';
Psi_data = sprintf('PsiXratio_%d.mat',shot);
fileName = fullfile(DataFolder, Psi_data);

%PsiXratio_data = [shot PsiXratio Bh rmax rmin Psi50 Psi25];
% if exist(fileName, 'file')==0
%     save(fileName, 'PsiXratio_data', 'Psi_data'); 
% else
%     output = load(fileName);
%     PsiXratio_data = cat(1, output.PsiXratio_data, PsiXratio_data);
%     save(fileName, 'PsiXratio_data', '-append');
% end

%save(fileName, 'psiX'); 

