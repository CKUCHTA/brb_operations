VoltScanFolder = '/Users/paulgradney/Research/Data/Drive_Voltage_Scan';
PlotFolder = fullfile(VoltScanFolder,'Plots/VloopVac/');
dataFolder = fullfile(VoltScanFolder, 'Scripts/VloopVac/');


plotFile = sprintf('%s/VloopVac_%d.png',PlotFolder, shot);

[shotnum, excelShotList] = collectShotNum('/Users/paulgradney/Research/Data/', 'Drive_Voltage_Scan/vacuum');

for shot = shotnum
    VloopVac(shot)
end
%%
EffVec = zeros(1,length(shotnum));
CTs = zeros(1,length(shotnum));
n = 0;
for shot = shotnum
    n = n +1;
    dataFile = sprintf('%s/VloopVac_%d.mat',dataFolder, shot);
    load(dataFile, 'CT', 'Effeciency')
    EffVec(1,n) = Effeciency;
    CTs(1,n) = CT;
end

figure(22), clf
plot(CTs, EffVec, 'x', 'Linewidth', 2)
xlim([min(CTs)-1 max(CTs)+1])
ylabel('Effeciency')
xlabel('CT Voltage [kV]')
    

