% The flux on the contour Bz = 0 is calcualted from the speed probe data.
% This is used to plot the change in flux along the seperatrix as a 
% function of time and readial postion. This script is based on Jan's
% script DoFluxesSingle.
% 
% The script does not use interpolated Bz data.
% Last Edited 24Mar23


function [ErecAtSaberLoc, PsiData, positionData, timeData, fluxRatioPlot] = FluxOnSeperatrix(shot, XprobeRange, SamRange, SaberLocation)
% clear
% shot = 60015;
% SamRange=2000:2250;
% XprobeRange=4:12;
% SaberLocation = 0.38;

[Bz,~,~,~,~,~,~,~,~,~,~] = loadBspeed(shot);
Bz = Bz';
rvec = linspace(-0.066, 0.734, 16); % speed probe locations
dt = 1/(1e7);

SampleToTimeFull = ((0:length(SamRange)-0.1))/10; %convert Sample range to mus

% Calculate flux, which includes probes for r<0
Psi = BzFluxSpeed(Bz, rvec); 

% B=0 Contour
[C]=contourc(SamRange, rvec(XprobeRange),1e3*Bz(XprobeRange, SamRange),[0 0]); 

%create array of position and samples to interpolate over
rX=C(2,2:end);
tX=C(1,2:end); % is samples NOT time
tX0=min(tX);
tXend=max(tX);
tXvec=round(tX0):round(tXend);
[tX,II]=sort(tX);
tX=tX-(1:length(tX))/1e5;
rX=rX(II);
rXvec=interp1(tX,rX,tXvec);% finds intep values for rXvex along (tX,rX) at tXvec 

PsiX=interp2(SamRange, rvec,Psi(:, SamRange),tXvec, rXvec); % interpolate Psi over tXvec and rXvec

%find how many points were added from interpolation
[~, MinsampleX] = min(abs(tX0 - SamRange));
[~, MaxsampleX] = min(abs(tXend - SamRange));
SampleX = SampleToTimeFull(MinsampleX:MaxsampleX)*10;
addedNumber = length(tX) - length(SampleX); % why is this only 7?
factor = (addedNumber+length(SampleX))/length(SampleX); % factor to multiply by to get number of points per microscond
timeX = SampleX/(10*factor); %time array along B=0 contour in mus


% Calculate the percent of reconnection along separatrix
rXvecSorted=rXvec;
JJ=isnan(rXvecSorted);
rXvecSorted(JJ)=0;
rXvecSorted=rXvecSorted+ (1:length(rXvecSorted))/1e6;
[rXvecSorted,JJ]=sort(rXvecSorted);
PsiXSorted=PsiX(JJ);
tXvecSorted=tXvec(JJ);
rmax = 0.48;
rmin = 0.35;
PsiMax=interp1(rXvecSorted,PsiXSorted,rmax);
PsiMin=interp1(rXvecSorted,PsiXSorted,rmin);
PsiXratio= round(((PsiMax-PsiMin)/PsiMax)*100); % percent of reconnection along serparatrix

dtInt = dt/factor; % divide by "factor" to account for interpolated data

Erec = (1/(2*pi))*gradient(PsiXSorted,dtInt);
for kk = 1: length(rXvecSorted)
    Erec(1,kk)= Erec(1,kk)/rXvecSorted(1,kk); % reconnection electric field
end

PsiData = {Psi, PsiXSorted, PsiXratio, PsiMax, PsiMin};

positionData = {XprobeRange, rmax, rmin, rXvecSorted};
timeData = {tXvecSorted, SampleToTimeFull, timeX, SampleX, SamRange};

% smooth data
Erec=smoo(Erec,3);
PsiXSorted =smoo(PsiXSorted,3); 

%find Erec at Lightsaber Probe location
[~,SaberInd] = min(abs(rXvecSorted - SaberLocation));
ErecAtSaberLoc = Erec(SaberInd);

fluxRatioPlot = figure(107);
clf
subplot(2,2,1)
%plot((tXvecSorted-tX0)/(10*factor),PsiXSorted,'b','linewidth',2) %(tXvec-tX0)/(10*4) converts to time and accounts for the interp?
plot((tXvecSorted-tX0)/(10*factor),PsiXSorted,'b','linewidth',2) %(tXvec-tX0)/(10*4)
hold on
xlabel('{\Delta}t  [{\mu}s]')
ylabel('\Psi(R,t)_X')
title(['Shot ' num2str(shot)])
% xlim([0 3.5])
xlim([0 8])
ylim([0 2]*1e-3)

subplot(2,2,2)
plot(rXvecSorted,PsiXSorted,'b','linewidth',2)
hold on
xlabel('R [m]')
ylabel('\Psi(R,t)_X')
xline(SaberLocation, 'Linewidth', 1)
% xlim([0.17 0.5])
xlim([0.1 0.5])
ylim([0 2]*1e-3)
title(['f(\Psi)_X=' num2str(PsiXratio) '% for R = ' num2str(rmax) ' to ' num2str(rmin) ])

subplot(2,2,4)
plot(rXvecSorted,Erec,'b','linewidth',2) % why are different rvec arrays being used?
hold on
xline(SaberLocation, 'Linewidth', 1)
xlabel('R [m]')
ylabel('E_{rec} [V/m]')
% ylim([-100 800])
ylim([-400 800])
% xlim([0.17 0.5])
xlim([0.1 0.5])
title(sprintf('E_{rec}(r = %0.2f) = %0.3d', SaberLocation, ErecAtSaberLoc))
% ylim([-2 6]*1e2)

subplot(2,2,3)
plot((tXvecSorted-tX0)/(10*factor),Erec,'b','linewidth',2)
hold on
xlabel('{\Delta}t [{\mu}s]')
ylabel('E_{rec} [V/m]')
% ylim([-100 800])
ylim([-400 800])
% xlim([0 3.5])
xlim([0 8])
