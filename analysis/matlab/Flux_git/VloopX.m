% Calculates the loop voltage WITH PLASMA due to the induced electric field that
% results from the coil currents ramping up.  The ratio of Vloop to theThis is calculated using the
% speed probe data.

%function Vloop = VloopVac(shot)
clear
shot = 59252;

[CT, ~, ~, ~, ~] = ExcelShotData(shot);


[Bspeed, dBspeed, rvec, SamRange, time, Bh] = getSpeedProbe(shot);
Bz = Bspeed{1};
Bz = Bz';
dBz = dBspeed{1};
dBz = dBz';

dt = 1/(10*1e6);

[Psi] = BzFluxSpeed(Bz, rvec);

Vloop = -gradient(Psi,dt); 

probPos = 13;
[VloopMax,MaxInd] = min(Vloop(probPos, :));

% Find Vloop along Seperatrix
SamRange=2000:2250;
XprobeRange=4:12;
SaberLocation = 0.38;
[ErecAtSaberLoc, PsiData, positionData, timeData, fluxRatioPlot] = FluxOnSeperatrix(shot, XprobeRange, SamRange, SaberLocation);

%%
figure(5), clf
tlim = [200 time(end)];

sgtitle(sprintf('Shot %d, B_h = %0.1f [mT], CT = %d[kV]', shot, Bh*1e3, CT))

%Vloop Plot at R = 57cm
subplot(3,2,[1 2])
plot(time,Vloop(13,:))
hold on
plot(time(1, MaxInd), VloopMax, 'x', 'Linewidth', 2)
xlim(tlim)
titleStr = sprintf('R = %0.2f[m],   MaxV_{loop}= %0.0f[V],   V_{loop}/CT = %0.2f', rvec(1,probPos), VloopMax, -VloopMax/(CT*1e3)); 
title(titleStr)
ylabel('V_{loop} [V]')
xlabel('t [\mus]')


% dBz pcolor plot
subplot(3,2,5)
pcolor(time, rvec, dBz), shading interp
colorbar
clim([-1 1]*1e4)
hold on
ylabel('r[m]')
xlabel('t[\mus)]')
xlim(tlim)
title('dB_z [T/s]')

% Bz pcolor plot
subplot(3,2,3)
pcolor(time, rvec, Bz*1e3), shading interp
colorbar
hold on
contour(time, rvec, Bz*1e3,[0 0 ],'k')
clim([-25 25])
xlim(tlim)
ylabel('r[m]')
xlabel('t[\mus)]')
title1 = sprintf('B_z[mT]');
title(title1)

% Flux pcolor plot
subplot(3,2,4)
pcolor(time, rvec,Psi), shading interp
colorbar
hold on
contour(time, rvec, Bz*1e3,[0 0 ],'k')
title(num2str(shot))
clim([-2 2]*1e-2)
%ylim(probeLoc([1 end])*1e2)
xlim(tlim)
ylabel('r[m]')
xlabel('t[\mus)]')
title('\Psi(R,t) [T\cdotm^2]')

% Vloop pcolor plot
subplot(3,2,6)
pcolor(time, rvec,Vloop), shading interp
colorbar
hold on
contour(time, rvec, Bz*1e3,[0 0 ],'k')
title(num2str(shot))
% clim([-8 1]*1e2)
%ylim(probeLoc([1 end])*1e2)
xlim(tlim)
ylabel('r[m]')
xlabel('t[\mus)]')
title('V_{loop} [V]')
