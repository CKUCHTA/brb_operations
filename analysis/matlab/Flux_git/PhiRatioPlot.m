%plot the flux ratio from individual shots.  This ratio is calcualted from
%x = 0.2 to x = 0.5

%DataFolder = '/Users/paulgradney/Research/Data/Plot/Config Compare';
DataFolder = 'C:\Users\MPDX\Documents\Gradney\Data\';
Psi_data = 'Flux_output\PsiXratio_Inductor';
excel = 'TREX_Drive_Cylinder_28July2022.xlsx';
Psi_filename = fullfile(DataFolder, Psi_data);
%excel_filename = fullfile(DataFolder, excel);
%[spread] = readtable(excel_filename);

load(Psi_filename);

Bh_CT5k = PsiXratio_data(1:6,3);
ratio_CT5k = PsiXratio_data(1:6,2);

Bh_CT6k = PsiXratio_data(7:13,3);
ratio_CT6k = PsiXratio_data(7:13,2);

Bh_CT7k = PsiXratio_data(14:19,3);
ratio_CT7k = PsiXratio_data(14:19,2);

Bh_CT8k = PsiXratio_data(20:28,3);
ratio_CT8k = PsiXratio_data(20:28,2);

Bh_CT9k = PsiXratio_data(30:36,3);
ratio_CT9k = PsiXratio_data(30:36,2);

Bh_CT10k = PsiXratio_data(37:43,3);
ratio_CT10k = PsiXratio_data(37:43,2);

figure(33)
clf
plot(Bh_CT5k, ratio_CT5k,'r')
hold on
plot(Bh_CT6k, ratio_CT6k)
plot(Bh_CT7k, ratio_CT7k)
plot(Bh_CT8k, ratio_CT8k)
plot(Bh_CT9k, ratio_CT9k)
plot(Bh_CT10k, ratio_CT10k)
xlabel('Bh')
ylabel('flux ratio')
title('flux ratio for x = 0.2 to x = 0.5 No TREX Cap')
legend('CT5k','CT6k','CT7k','CT8k','CT9k','CT10k')