"""Get data about the hook_bdot1 probe from the MDSplus tree."""
from modules.generic_get_data import Get, Data, lazy_get, Port
import logging
import numpy as np


class Flux_Bdot1_Data(Data):
    NUM_STICKS = 8
    NUM_DIRECTED_PROBES_PER_STICK = [2, 2, 8]
    PROBE_NAMES = ['db1', 'db2', 'db3']

    def __init__(self, tree, *args, **kwargs):
        """
        Hold all the data for the flux_bdot1 probe in one class for easier access and reduced network connections.
        """
        super().__init__(tree, [], [], *args, **kwargs)

    def _compose_conglomerated_coordinate_call(self, probe_name, vector_type, coordinate_system_name, directions):
        """Compose the MDSplus call to get the data for the conglomerated coils.
        
        Parameters
        ----------
        probe_name : str
            Name of the probe, e.g. `db1`.
        vector_type : str
            Type of vector, e.g. `position`.
        coordinate_system_name : str
            Name of the coordinate system, e.g. `mach_coords`.
        directions : list[str]
            List of directions, e.g. `['r', 'phi', 'z']`.
        
        Returns
        -------
        str
        """
        return '[' + ','.join(f'DATA(\\flux_bdot1_{probe_name}.{vector_type}.{coordinate_system_name}.{direction})' for direction in directions) + ']'

    @lazy_get
    def coils_machine_position(self):
        """Position of the coils on the `flux_bdot1` probe in machine coordinates.

        Returns
        -------
        list[np.array[float]]
            List of 3D-arrays of machine cylindrical postions, `(r, phi, z)`. 
            Each list is for a single probe type, `(db1, db2, db3)`. Each array 
            of shape `(NUM_STICKS, NUM_DIRECTED_PROBES_PER_STICK, 3)`.
        """
        directions = ['r', 'phi', 'z']
        return [
            np.moveaxis(self.get(Get(
                self._compose_conglomerated_coordinate_call(probe_type, 'position', 'mach_coords', directions),
                name=f'flux_bdot1_{probe_type}_position', signal=False
            )), (0, 1, 2), (2, 0, 1))
            for probe_type in self.PROBE_NAMES
        ]
    
    @lazy_get
    def coils_port_position(self):
        """Position of the coils on the `flux_bdot1` probe in port coordinates.

        Returns
        -------
        list[np.array[float]]
            List of 3D-arrays of port cartesian postions, `(x, y, z)`. 
            Each list is for a single probe type, `(db1, db2, db3)`. Each array 
            of shape `(NUM_STICKS, NUM_DIRECTED_PROBES_PER_STICK, 3)`.
        """
        directions = ['x', 'y', 'z']
        return [
            np.moveaxis(self.get(Get(
                self._compose_conglomerated_coordinate_call(probe_type, 'position', 'port_coords', directions),
                name=f'flux_bdot1_{probe_type}_position', signal=False
            )), (0, 1, 2), (2, 0, 1))
            for probe_type in self.PROBE_NAMES
        ]
    
    @lazy_get
    def coils_stick_position(self):
        """Position of the coils on the `flux_bdot1` probe in stick coordinates.

        Returns
        -------
        list[np.array[float]]
            List of 3D-arrays of stick cartesian positions, `(x, y, z)`. 
            Each list is for a single probe type, `(db1, db2, db3)`. Each array 
            of shape `(NUM_STICKS, NUM_DIRECTED_PROBES_PER_STICK, 3)`.
        """
        directions = ['x', 'y', 'z']
        return [
            np.moveaxis(self.get(Get(
                self._compose_conglomerated_coordinate_call(probe_type, 'position', 'stick_coords', directions),
                name=f'flux_bdot1_{probe_type}_position', signal=False
            )), (0, 1, 2), (2, 0, 1))
            for probe_type in self.PROBE_NAMES
        ]

    @lazy_get
    def coils_machine_orientation(self):
        """Orientation of the coils on the `flux_bdot1` probe in machine coordinates.

        Returns
        -------
        list[np.array[float]]
            List of 3D-arrays of machine cylindrical orientations, `(r, phi, z)`. 
            Each list is for a single probe type, `(db1, db2, db3)`. Each array 
            of shape `(NUM_STICKS, NUM_DIRECTED_PROBES_PER_STICK, 3)`.
        """
        directions = ['r', 'phi', 'z']
        return [
            np.moveaxis(self.get(Get(
                self._compose_conglomerated_coordinate_call(probe_type, 'orientation', 'mach_coords', directions),
                name=f'flux_bdot1_{probe_type}_orientation', signal=False
            )), (0, 1, 2), (2, 0, 1))
            for probe_type in self.PROBE_NAMES
        ]
    
    @lazy_get
    def coils_port_orientation(self):
        """Orientation of the coils on the `flux_bdot1` probe in port coordinates.

        Returns
        -------
        list[np.array[float]]
            List of 3D-arrays of port cartesian orientations, `(x, y, z)`. 
            Each list is for a single probe type, `(db1, db2, db3)`. Each array 
            of shape `(NUM_STICKS, NUM_DIRECTED_PROBES_PER_STICK, 3)`.
        """
        directions = ['x', 'y', 'z']
        return [
            np.moveaxis(self.get(Get(
                self._compose_conglomerated_coordinate_call(probe_type, 'orientation', 'port_coords', directions),
                name=f'flux_bdot1_{probe_type}_orientation', signal=False
            )), (0, 1, 2), (2, 0, 1))
            for probe_type in self.PROBE_NAMES
        ]
    
    @lazy_get
    def coils_stick_orientation(self):
        """Orientation of the coils on the `flux_bdot1` probe in stick coordinates.

        Returns
        -------
        list[np.array[float]]
            List of 3D-arrays of stick cartesian orientations, `(x, y, z)`. 
            Each list is for a single probe type, `(db1, db2, db3)`. Each array 
            of shape `(NUM_STICKS, NUM_DIRECTED_PROBES_PER_STICK, 3)`.
        """
        directions = ['x', 'y', 'z']
        return [
            np.moveaxis(self.get(Get(
                self._compose_conglomerated_coordinate_call(probe_type, 'orientation', 'stick_coords', directions),
                name=f'flux_bdot1_{probe_type}_orientation', signal=False
            )), (0, 1, 2), (2, 0, 1))
            for probe_type in self.PROBE_NAMES
        ]
    
    @lazy_get
    def db(self):
        """Get the dB/dt data for the `flux_bdot1` probe.
        
        Returns
        -------
        list[np.array[float]]
            List of 2D-arrays of dB/dt for each coil. Each list is for a single 
            probe type, `(db1, db2, db3)`. Each array of shape 
            `(NUM_STICKS, NUM_DIRECTED_PROBES_PER_STICK, NUM_TIMES)`.
        """
        return [
            self.get(Get(f'DATA(\\flux_bdot1_{probe_type})', name=f'flux_bdot1_{probe_type}_db')) for probe_type in self.PROBE_NAMES
        ]
    
    @lazy_get
    def time(self):
        """Get the time data for the `flux_bdot1` probe.
        
        Returns
        -------
        np.array[float]
            Array of time values of shape `(NUM_TIMES,)`.
        """
        return self.get(Get('DIM_OF(DATA(\\flux_bdot1_db1))', name='flux_bdot1_time'))
    
    @lazy_get
    def working(self):
        """Get the working status of each of the coils on the `flux_bdot1` probe.
        
        Returns
        -------
        list[np.array[bool]]
            List of 2D-arrays of working status for each coil. Each list is for 
            a single probe type, `(db1, db2, db3)`. Each array of shape 
            `(NUM_STICKS, NUM_DIRECTED_PROBES_PER_STICK)`.
        """
        return [
            self.get(Get(f'DATA(\\flux_bdot1_{probe_type}.working)', name=f'flux_bdot1_{probe_type}_working', signal=False))
            for probe_type in self.PROBE_NAMES
        ]
    
    @lazy_get
    def port(self) -> Port:
        """Get the port of the `flux_bdot1` probe.
        
        Returns
        -------
        Port
            Port object.
        """
        return Port(self, 'flux_bdot1_')
    

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from modules.coordinates import Position
    import scipy as sp
    from helmholtz.get_data import Helmholtz_Data
    logging.basicConfig(level=logging.DEBUG)
    shot = 63397
    time_index_range = (70800, 71200)
    flux_probe = Flux_Bdot1_Data(shot, time_index_range=time_index_range)

    plot_positions_machine_coordinates = False
    plot_positions_port_coordinates = False
    plot_positions_stick_coordinates = False
    plot_raw_db_signals = True
    plot_db_colormaps = True

    if plot_positions_machine_coordinates:
        # Plot the positions of the coils in machine coordinates.
        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')

        all_position_values = [np.array([]) for _ in range(3)]
        for probe_type, positions in zip(flux_probe.PROBE_NAMES, flux_probe.coils_machine_position):
            flat_positions = positions.reshape(-1, 3)
            x = flat_positions[:, 0] * np.cos(flat_positions[:, 1])
            y = flat_positions[:, 0] * np.sin(flat_positions[:, 1])
            z = flat_positions[:, 2]

            all_position_values = [np.concatenate((dir, flat_dir)) for dir, flat_dir in zip(all_position_values, [x, y, z])]

            ax.scatter(x, y, z, label=probe_type)

        # Plot the port position.
        port_latitude = flux_probe.port.lat_rad
        port_longitude = flux_probe.port.long_rad
        port_radius = flux_probe.port.rport

        x_port = port_radius * np.cos(port_longitude) * np.cos(port_latitude)
        y_port = port_radius * np.sin(port_longitude) * np.cos(port_latitude)
        z_port = port_radius * np.sin(port_latitude)

        last_kf40_position = Position()
        last_kf40_position.from_port(
            port_radius, port_longitude, port_latitude, 
            flux_probe.port.alpha_rad, flux_probe.port.beta_rad, flux_probe.port.gamma_rad,
            0,
            0,
            flux_probe.port.insert,
            flux_probe.port.clocking_rad,
        )

        ax.plot(
            [x_port, last_kf40_position.cartesian[0]], 
            [y_port, last_kf40_position.cartesian[1]], 
            [z_port, last_kf40_position.cartesian[2]], 
            label='port to KF40', marker='o'
        )
        all_position_values = [
            np.concatenate((dir, [port_val, kf40_val])) for dir, port_val, kf40_val 
            in zip(all_position_values, [x_port, y_port, z_port], last_kf40_position.cartesian)
        ]

        # Plot the flange position.
        flange_position = Position()
        flange_position.from_port(
            port_radius, port_longitude, port_latitude, 
            flux_probe.port.alpha_rad, flux_probe.port.beta_rad, flux_probe.port.gamma_rad,
            flux_probe.get('\\top.magnetics.flux_bdot1.flange.position.port_coords.x'),
            flux_probe.get('\\top.magnetics.flux_bdot1.flange.position.port_coords.y'),
            flux_probe.get('\\top.magnetics.flux_bdot1.flange.position.port_coords.z')  + flux_probe.port.insert,
            flux_probe.port.clocking_rad,
        )

        ax.scatter(*flange_position.cartesian, label='flange')
        all_position_values = [np.concatenate((dir, [val])) for dir, val in zip(all_position_values, flange_position.cartesian)]

        xyz_ranges = [np.ptp(dir) for dir in all_position_values]
        ax.set_box_aspect(xyz_ranges)
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')

        ax.set_title(f'Coil positions in machine coordinates for shot {shot}')
        ax.legend()

    if plot_positions_port_coordinates:
        # Plot the positions of the coils in port coordinates.
        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')

        all_position_values = [np.array([]) for _ in range(3)]
        for probe_type, positions in zip(flux_probe.PROBE_NAMES, flux_probe.coils_port_position):
            flat_positions = positions.reshape(-1, 3)
            x = flat_positions[:, 0]
            y = flat_positions[:, 1]
            z = flat_positions[:, 2]

            all_position_values = [np.concatenate((dir, flat_dir)) for dir, flat_dir in zip(all_position_values, [x, y, z])]

            ax.scatter(x, y, z, label=probe_type)

        flange_port_coordinates = np.array([
            flux_probe.get('\\top.magnetics.flux_bdot1.flange.position.port_coords.x'),
            flux_probe.get('\\top.magnetics.flux_bdot1.flange.position.port_coords.y'),
            flux_probe.get('\\top.magnetics.flux_bdot1.flange.position.port_coords.z') + flux_probe.port.insert
        ])
        ax.scatter(*flange_port_coordinates, label='flange')
        all_position_values = [np.concatenate((dir, [val])) for dir, val in zip(all_position_values, flange_port_coordinates)]

        kf40_port_coordinates = np.array([0, 0, flux_probe.port.insert])
        ax.plot(
            [0, kf40_port_coordinates[0]], 
            [0, kf40_port_coordinates[1]], 
            [0, kf40_port_coordinates[2]], 
            label='port to KF40', marker='o'
        )
        all_position_values = [np.concatenate((dir, [0, val])) for dir, val in zip(all_position_values, kf40_port_coordinates)]

        xyz_ranges = [np.ptp(dir) for dir in all_position_values]
        ax.set_box_aspect(xyz_ranges)
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')

        ax.set_title(f'Coil positions in port coordinates for shot {shot}')
        ax.legend()

    if plot_positions_stick_coordinates:
        # Plot the positions of the coils in stick coordinates.
        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')

        all_position_values = [np.array([]) for _ in range(3)]
        for probe_type, positions in zip(flux_probe.PROBE_NAMES, flux_probe.coils_stick_position):
            flat_positions = positions.reshape(-1, 3)
            x = flat_positions[:, 0]
            y = flat_positions[:, 1]
            z = flat_positions[:, 2]

            all_position_values = [np.concatenate((dir, flat_dir)) for dir, flat_dir in zip(all_position_values, [x, y, z])]

            ax.scatter(x, y, z, label=probe_type)

        ax.scatter(0, 0, 0, label='flang')
        all_position_values = [np.concatenate((dir, [0])) for dir in all_position_values]

        xyz_ranges = [np.ptp(dir) for dir in all_position_values]
        ax.set_box_aspect(xyz_ranges)
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')

        ax.set_title(f'Coil positions in stick coordinates for shot {shot}')
        ax.legend()
    
    if plot_raw_db_signals:
        factor = 1e3
        for probe_type_index, (probe_type, db) in enumerate(zip(flux_probe.PROBE_NAMES, flux_probe.db)):
            db -= np.mean(db[:, :, :50], axis=-1)[:, :, None]
            fig = plt.figure()

            gs = fig.add_gridspec(2, 4, wspace=0, hspace=0.1)

            initial_ax = None
            for stick_index in range(flux_probe.NUM_STICKS):
                if initial_ax is None:
                    ax = fig.add_subplot(gs[stick_index // 4, stick_index % 4])
                    initial_ax = ax
                else:
                    ax = fig.add_subplot(gs[stick_index // 4, stick_index % 4], sharex=initial_ax, sharey=initial_ax)

                for coil_index in range(db.shape[1]):
                    if flux_probe.working[probe_type_index][stick_index, coil_index]:
                        linestyle = '-'
                    else:
                        linestyle = '--'
                    ax.plot(flux_probe.time, db[stick_index, coil_index] + factor * (coil_index + 1), linestyle=linestyle)

                ax.set_title(f'Stick {stick_index + 1}')

            fig.suptitle(f'{probe_type} dB/dt for shot {shot}')

    if plot_db_colormaps:
        plot_b_colormaps = True
        if plot_b_colormaps:
            helmholtz = Helmholtz_Data(shot)

        for probe_type_index, (probe_type, db) in enumerate(zip(flux_probe.PROBE_NAMES, flux_probe.db)):
            db -= np.mean(db[:, :, :50], axis=-1)[:, :, None]
            db *= 2
            
            probe_radii = flux_probe.coils_machine_position[probe_type_index][:, :, 0]
            probe_angle = flux_probe.coils_machine_position[probe_type_index][:, :, 1]
            radii = probe_radii * np.cos(probe_angle)

            sorted_radii = np.sort(radii, axis=-1)

            data_sets = [np.take_along_axis(db, np.argsort(radii, axis=-1)[:, :, None], axis=1)]
            data_set_names = [f'{probe_type} dB/dt']
            if plot_b_colormaps:
                r_locations = flux_probe.coils_machine_position[probe_type_index][:, :, 0].flatten()
                z_locations = flux_probe.coils_machine_position[probe_type_index][:, :, 2].flatten()
                b_r_initial, b_z_initial = helmholtz.analytic_b_h(r_locations, z_locations)
                b_r_initial = b_r_initial.reshape(flux_probe.coils_machine_position[probe_type_index].shape[:2])
                b_z_initial = b_z_initial.reshape(flux_probe.coils_machine_position[probe_type_index].shape[:2])
                b_initial = np.stack((b_r_initial, np.zeros_like(b_r_initial), b_z_initial), axis=-1)
                # TODO: Fix the time for the flux array.
                dt = 10**-7
                # Calculate the initial field directed in the coil directions by projecting the field onto the coil directions.
                b_initial_directed = np.sum(b_initial * flux_probe.coils_machine_orientation[probe_type_index], axis=-1)
                # Integrate db and add the initial field to get the total field.
                b = sp.integrate.cumtrapz(db, dx=dt, axis=-1, initial=0) + b_initial_directed[:, :, None]

                data_sets.append(np.take_along_axis(b, np.argsort(radii, axis=-1)[:, :, None], axis=1))
                data_set_names.append(f'{probe_type} B')

            for data_set_index, (data_set, data_set_name) in enumerate(zip(data_sets, data_set_names)):
                norm = mpl.colors.CenteredNorm(halfrange=np.max(np.abs(data_set)))
                fig = plt.figure()
                gs = fig.add_gridspec(2, 4, wspace=0, hspace=0.1)

                initial_ax = None
                for stick_index in range(flux_probe.NUM_STICKS):
                    if initial_ax is None:
                        ax = fig.add_subplot(gs[stick_index // 4, stick_index % 4])
                        initial_ax = ax
                    else:
                        ax = fig.add_subplot(gs[stick_index // 4, stick_index % 4], sharex=initial_ax, sharey=initial_ax)

                    ax.pcolormesh(flux_probe.time, sorted_radii[stick_index], data_set[stick_index], shading='auto', norm=norm, cmap='seismic')

                    if plot_b_colormaps and probe_type == 'db3':
                        ax.contour(flux_probe.time, sorted_radii[stick_index], data_sets[1][stick_index], levels=[0], colors='k', linewidths=1, linestyles='--')

                    for radius in sorted_radii[stick_index]:
                        ax.axhline(radius, color='k', linewidth=0.5)
                    ax.set_title(f'Stick {stick_index + 1} (z = {np.mean(flux_probe.coils_machine_position[probe_type_index][stick_index, :, 2]):.2f} m)')

                    if stick_index // 4 == 1:
                        ax.set_xlabel('Time (s)')
                    else:
                        ax.tick_params(axis='x', labelbottom=False)
                    if stick_index % 4 == 0:
                        ax.set_ylabel('Radius (m)')
                    else:
                        ax.tick_params(axis='y', labelleft=False)

                cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])
                cbar = fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap='seismic'), cax=cbar_ax)

                fig.suptitle(data_set_name + f' for shot {shot}')

    plt.show()
