import logging
from modules.shot_loader import get_remote_shot_tree
from modules.generic_get_data import Get, Data, lazy_get
import matplotlib.pyplot as plt
import numpy as np
import MDSplus as mds
from plasma_guns.shot_quality import working_guns


class Plasma_Guns_Data(Data):
    def __init__(self, tree, i_arc=False, i_bias=False, v_arc=False, v_bias=False, time=False, guns=list(range(1, 20)), check_gun_choices=True, *args, **kwargs):
        """
        Lazily get the required data from the tree.
        
        Parameters
        ----------
        tree : MDSplus.Connection of int
            MDSplus tree connection object or shot number.
        i_arc : bool
            Boolean of whether to get i_arc.
        i_bias : bool
            Boolean of whether to get i_bias.
        v_arc : bool
            Boolean of whether to get v_arc.
        v_bias : bool
            Boolean of whether to get v_bias.
        time : bool
            Boolean of whether to get time.
        guns : list[int]
            List of indeces of which guns to get.
        check_gun_choices : bool
            Whether to check that the chosen guns are recording before pulling their info. This slightly slows down the code because of the
            number of get calls needed to check each gun for the shot.
        """
        if check_gun_choices:
            tree = super()._get_tree(tree)
            recording_guns = []
            nonrecording_guns = []

            # Find which guns were recording by doing a get call for each gun.
            for gun_index in guns:
                try:
                    tree.get('DATA( \\gun_{}_v_arc )[0 : 5]'.format(str(gun_index).zfill(2)))
                except mds.connection.MdsIpException as e:
                    logging.debug("Caught exception while trying to get data for gun {} while checking gun choices. Exception was: {}".format(gun_index, e))
                    nonrecording_guns.append(gun_index)
                else:
                    recording_guns.append(gun_index)
            
            recording_guns = np.array(recording_guns)
            self._recording_guns = recording_guns
            nonrecording_guns = np.array(nonrecording_guns)
            if nonrecording_guns.size != 0:
                logging.warning("Not all guns to get were recording. Nonrecording guns were: {}".format(nonrecording_guns))

            # Set the guns to get as only those that were recording.
            logging.debug("Guns to use in get call: {}".format(recording_guns))
            self.gun_indeces = recording_guns
        else:
            self.gun_indeces = np.array(guns)

        variable_booleans = [i_arc, i_bias, v_arc, v_bias, time]

        self.get_calls = [
            Get('[' + ', '.join('DATA(\\gun_{num}_{suffix})'.format(num=str(gun_num).zfill(2), suffix=tag_suffix) for gun_num in self.gun_indeces) + ']', name='gun_{}'.format(tag_suffix)) for tag_suffix in ['i_arc', 'i_bias', 'v_arc', 'v_bias']
        ]
        if self.gun_indeces.size != 0:
            self.get_calls.append(Get('dim_of(\\gun_{}_i_arc)'.format(str(self.gun_indeces[0]).zfill(2)), name='gun_time'))
        else:
            self.get_calls.append(Get('dim_of(\\gun_01_i_arc)', name='gun_time'))

        # Initialize the class variables.
        self._i_arc, self._i_bias, self._v_arc, self._v_bias, self._time = super().__init__(tree, variable_booleans, self.get_calls, *args, **kwargs)

        # Save the gun indeces.
        self.saved_calls['gun_indeces'] = self.gun_indeces

    @lazy_get
    def i_arc(self):
        return self.get(self.get_calls[0])

    @lazy_get
    def i_bias(self):
        return self.get(self.get_calls[1])

    @lazy_get
    def v_arc(self):
        return self.get(self.get_calls[2])

    @lazy_get
    def v_bias(self):
        return self.get(self.get_calls[3])
    
    @lazy_get
    def time(self):
        return self.get(self.get_calls[4])

    @lazy_get
    def recording_guns(self):
        return working_guns.get_recording_guns(self.tree, self.gun_indeces)

    @lazy_get
    def firing_guns(self):
        return working_guns.get_firing_guns(self.gun_indeces, self.v_arc, self.i_arc)

    @lazy_get
    def correctly_firing_guns(self):
        return working_guns.get_correct_gun_fires(self.v_arc, self.gun_indeces, *working_guns.load_avg_curve())

    @lazy_get
    def is_vacuum_shot(self):
        """Whether there are no firing guns."""
        if np.count_nonzero(self.firing_guns) == 0:
            return True
        else:
            return False

    @lazy_get
    def good_shot(self):
        """Whether all guns that fired, fired correctly."""
        if np.count_nonzero(self.firing_guns - self.correctly_firing_guns) == 0:
            return True
        else:
            return False


if __name__=="__main__":
    logging.basicConfig(level=logging.DEBUG)
    all_gun_indeces = list(range(1, 20))
    # gun_indeces = [2, 4, 7, 13, 16, 18]
    for shot_num in [59760]:
        problem_guns = []
        for gun in all_gun_indeces:
            gun_indeces = [gun]

            try:
                data = Plasma_Guns_Data(shot_num, guns=gun_indeces)

                for i, gun_index in enumerate(gun_indeces):
                    plt.plot(data.i_arc[i], label="Gun {}".format(gun_index))
            except Exception as e:
                logging.exception(e)
                problem_guns.append(gun)
                continue

        plt.legend()
        plt.title('Shot #{}'.format(shot_num))
        plt.xlabel('Index')
        plt.ylim(-250, 1500)
        plt.ylabel('I arc')
        plt.show()
        print("Problem guns:", problem_guns)
