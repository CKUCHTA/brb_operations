from cap_bank.get_data import Capacitor_Bank_Data
import numpy as np
import MDSplus as mds
import logging
import scipy.io
import os


NUM_GUNS = 19
def to_gun_index(boolean_array, gun_indeces=np.arange(1, NUM_GUNS + 1)):
    """
    Take in a boolean array of NUM_GUNS elements and return the indeces of which guns had a 'True' value.
    
    params:
    boolean_array   = Array of whether the matching indexed gun was firing.
    gun_indeces     = Indeces of each gun.
    """
    return gun_indeces[np.nonzero(boolean_array)]

def load_avg_curve(mat_filepath=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data/average_v_curve.mat')):
    """
    Load the average gun v_arc curve and standard deviations from a mat file.

    returns:
    average_curve   = Average v_arc curve for use in testing 'goodness' of shot.
    lower_deviation = Magnitude of negative standard deviation in average v_arc at each time point.
    upper_deviation = Magnitude of positive standard deviation in average v_arc at each time point.
    """
    loaded_mat = scipy.io.loadmat(mat_filepath)

    average_curve = loaded_mat['average_curve'][0]
    lower_deviation = loaded_mat['lower_deviation'][0]
    upper_deviation = loaded_mat['upper_deviation'][0]

    return average_curve, lower_deviation, upper_deviation

# Get working guns for all shots from 50066+
# When deciding if a gun is working we want:
# Get which guns were recording for each shot.
# Find which shots are vaccuum shots
def get_recording_guns(tree, guns_to_check=range(1, NUM_GUNS + 1)):
    """
    Find which guns are recording by trying to get i_arc from each gun and testing if an error is returned.
    
    params:
    tree            = MDSplus tree to try to get data from.
    guns_to_check   = Which guns to check for firing.
    
    returns:
    recording_guns  = 1D array that holds whether each gun was firing.
    """
    logging.debug("Getting recording guns.")

    recording_guns = np.zeros(NUM_GUNS, dtype=bool)
    # Find which guns were recording by doing a get call for each gun.
    for gun_index in guns_to_check:
        try:
            tree.get('DATA( \\gun_{}_v_arc )[0 : 5]'.format(str(gun_index).zfill(2)))
        except mds.connection.MdsIpException as e:
            logging.debug("Caught exception while trying to get data for gun {} while checking gun choices. Gun was most likely not recording. Exception was: {}".format(gun_index, e))
        else:
            recording_guns[guns_to_check[0] - 1] = True

    logging.debug("Recording guns were: {}".format(to_gun_index(recording_guns, guns_to_check)))
    return recording_guns

def get_firing_guns(gun_indeces, all_v_arc_data, all_i_arc_data, initial_range=(0, 200), check_range=(1000, 5000)):
    """
    Compare the median values of v and i in the initial range to the range to check. If a gun fired then we expect there to be a very large
    difference. We use median to exclude the effect of large outliers.

    Parameters
    ----------
    gun_indeces     = Indeces of guns we want to check.
    all_v_arc_data  = Array of voltages where each row represents a gun.
    all_i_arc_data  = Array of currents where each row represents a gun.
    initial_range   = Range of indeces to check the voltage/current against.
    check_range     = Range of indeces that will determine whether the guns are firing.

    Returns
    -------
    firing_guns     = 1D boolean array of size gun_indeces of firing guns.
    """
    logging.debug("Getting firing guns.")
    initial_v_median = np.median(all_v_arc_data[:, initial_range[0]:initial_range[1]], axis=1)
    initial_i_median = np.median(all_i_arc_data[:, initial_range[0]:initial_range[1]], axis=1)
    
    check_v_median = np.median(all_v_arc_data[:, check_range[0]:check_range[1]], axis=1)
    check_i_median = np.median(all_i_arc_data[:, check_range[0]:check_range[1]], axis=1)

    # If the difference between the medians is greater than the v_diff or i_diff then the gun did fire.
    v_diff = 20
    i_diff = 100

    v_check = abs(initial_v_median - check_v_median) > v_diff
    i_check = abs(initial_i_median - check_i_median) > i_diff

    firing_guns = np.zeros_like(gun_indeces)
    firing_guns[np.logical_or(v_check, i_check)] = True

    logging.debug("Firing guns were: {}".format(to_gun_index(firing_guns, gun_indeces)))
    return firing_guns

def compare_curve_to_average(gun_curves, avg_curve, avg_curve_std: tuple, index_ranges: list, sigma_multipliers: list, percentages_valid: list):
    """
    Compare the voltage curve from a set of guns to the average curve. Compare multiply blocks of the curve to the average and
    check that the voltage curve falls within some distance of the average curve.
    
    params:
    gun_curves          = Voltage curves of the guns.
    avg_curve           = Average voltage curve over many shots.
    avg_curve_std       = Tuple of 2 elements. (negative standard deviation of avg curve, positive standard ...)
    index_ranges        = List of 2 element tuples. Each tuple is a start and end index to compare.
    sigma_multipliers   = List of 2 element tuples. How many standard deviations off the average to allow in the (-, +) direction.
    percentages_valid   = List of 2 element tuples or list of floats. What percent of the data from the gun curve must lie within 
                          the area (either in the -/+ direction or in both).

    returns:
    falls_in_range      = Boolean of whether the guns voltage curve is valid.
    """
    # Check that all lists have the same length before proceeding.
    if not (len(avg_curve_std) == len(index_ranges) == len(sigma_multipliers) == len(percentages_valid)):
        raise ValueError("The lengths of avg_curve_std (length = {}), index_range (length = {}), sigma_multiplier (length = {}), and percentages_valid (length = {}) are not all equal.".format(
            len(avg_curve_std), len(index_ranges), len(sigma_multipliers), len(percentages_valid)
        ))
    
    falls_in_range = np.ones(gun_curves.shape[0])
    for i in range(len(avg_curve_std)):
        curr_index_range = index_ranges[i]
        # Only compare the values in this index range.
        compare_values = gun_curves[:, curr_index_range[0]:curr_index_range[1]] - avg_curve[curr_index_range[0]:curr_index_range[1]]

        # Get the positive and negative standard deviations in this index range.
        negative_std = avg_curve_std[0][curr_index_range[0]:curr_index_range[1]]
        positive_std = avg_curve_std[1][curr_index_range[0]:curr_index_range[1]]

        # Calculate how many points fell within the number of standard deviations in the positive and negative directions.
        good_negative = compare_values > -sigma_multipliers[i][0] * negative_std
        percent_good_negative = np.sum(good_negative, axis=1) / (curr_index_range[1] - curr_index_range[0])
        good_positive = compare_values < sigma_multipliers[i][1] * positive_std
        percent_good_positive = np.sum(good_positive, axis=1) / (curr_index_range[1] - curr_index_range[0])
        logging.debug("Found {}%, {}% values that fall in the standard deviation range. Each index is a different gun.".format(100 * percent_good_negative, 100 * percent_good_positive))

        # Split the percentages into a set for the negative and positive directions.
        curr_percentages = percentages_valid[i]
        if isinstance(curr_percentages, float):
            negative_good_needed = curr_percentages
            positive_good_needed = curr_percentages
        else:
            negative_good_needed = curr_percentages[0]
            positive_good_needed = curr_percentages[1]
        
        # Check whether there are enough points that fell within the needed range.
        new_passing_guns = np.logical_not(np.logical_or(percent_good_negative < negative_good_needed, percent_good_positive < positive_good_needed))
        logging.debug("New changes in which guns passed: {}".format(new_passing_guns))
        falls_in_range = np.logical_and(new_passing_guns, falls_in_range)
    
    logging.debug("After testing all ranges, passing guns were: {}".format(falls_in_range))
    return falls_in_range

def get_correct_gun_fires(gun_v_curves, gun_indeces, average_curve, lower_curve_std, upper_curve_std):
    """
    Take in a set of voltage curves from a shot for each gun and return whether each had a curve that is close to the average shot.
    
    params:
    gun_v_curves            = Voltage curves of the guns.
    gun_indeces             = Indeces of the guns that map to each curve.
    average_curve           = The average voltage curve of many shots.
    lower_curve_std         = The lower standard deviation of the average curve.
    upper_curve_std         = The upper standard deviation of the average curve.
    
    returns:
    correct_fires           = A boolean array of whether each gun of the passed gun_indeces fired.
    """
    index_ranges = [(400, 425), (1700, 2700)]
    sigma_multipliers = [(10, 10), (0.75, 3)]
    percentages_valid = [0.70, 0.7]

    correct_fires = compare_curve_to_average(gun_v_curves, average_curve, (lower_curve_std, upper_curve_std), index_ranges, sigma_multipliers, percentages_valid)
    return correct_fires

def count_num_firing_guns(firing_gun_array):
    """
    Count the number of guns that fire for each shot.
    
    params:
    firing_gun_array    = Boolean array where each row represents a shot and contains whether a gun fired or not.
    
    returns:
    num_firing_guns     = Integer array where each element is the number of guns that fired.
    """
    logging.debug("Getting number of firing guns.")
    return np.count_nonzero(firing_gun_array, axis=1)

def determine_shot_quality(firing_gun_array, correct_firing_gun_array):
    """Determine whether a shot had all guns firing correctly. Returns a boolean as to whether the shot was good."""
    logging.debug("Determining shot quality.")
    # Subtract the two arrays since we only want to find when a gun fired but incorrectly.
    incorrectly_firing_gun_array = firing_gun_array - correct_firing_gun_array
    # Change all rows that have a nonzero element to a value of 1.
    good_shot = np.where(np.count_nonzero(incorrectly_firing_gun_array, axis=1) > 0, False, True)
    return good_shot

def get_reconnection_timing(tree, rettime=False):
    """
    Get the time when the reconnection begins.
    
    params:
    tree    = MDSplus tree to get data from.
    rettime = Whether to return the time or just the array index.
    """
    cap_banks = Capacitor_Bank_Data(tree)
    
    # Smooth the data by using a moving average.
    smooth_diameter = 11
    smoothing_array = np.ones(smooth_diameter)
    smoothed_vcap = np.convolve(cap_banks.vcap, smoothing_array, mode='valid')

    # Calculate difference between each neighboring point.
    vcap_diff = smoothed_vcap[1:] - smoothed_vcap[:len(smoothed_vcap) - 1]

    # Get the index of the max difference.
    max_diff_index = np.argmax(vcap_diff) + (smooth_diameter - 1) // 2

    if rettime:
        return max_diff_index, cap_banks.time[max_diff_index]
    else:
        return max_diff_index


