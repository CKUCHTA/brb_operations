"""Handle all functions that write whether a gun is working."""
from modules.shot_loader import get_remote_shot_tree
from plasma_guns.get_data import Plasma_Guns_Data
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from tqdm import tqdm
import random
import logging
import numpy as np
import scipy.io
from plasma_guns.shot_quality import working_guns


NUM_GUNS = 19
def write_gun_data(shot_num_array, average_v_curve, lower_curve_std, upper_curve_std, file_name="./analysis/plasma_guns/data/gun_shot_info.mat"):
    # Construct empty arrays for holding info.
    recording_gun_array = np.zeros((shot_num_array.size, NUM_GUNS))
    firing_gun_array = np.zeros((shot_num_array.size, NUM_GUNS))
    correct_firing_gun_array = np.zeros((shot_num_array.size, NUM_GUNS))

    for i, shot_number in enumerate(tqdm(shot_num_array)):
        logging.debug("Evaluating gun info for shot #{}.".format(shot_number))
        tree = get_remote_shot_tree(shot_number)
        # Get recording guns.
        recording_gun_array[i] = working_guns.get_recording_guns(tree)
        
        # Get firing guns.
        recording_gun_indeces = working_guns.to_gun_index(recording_gun_array[i])
        # If there aren't any recording guns then skip it.
        if recording_gun_indeces.size == 0:
            continue
        gun_data = Plasma_Guns_Data(tree, i_arc=True, v_arc=True, guns=recording_gun_indeces)
        firing_gun_array[i] = working_guns.get_firing_guns(recording_gun_indeces, gun_data.v_arc, gun_data.i_arc)

        # Get correctly firing guns.
        firing_gun_indeces = working_guns.to_gun_index(firing_gun_array[i], recording_gun_indeces)
        # If there aren't any firing guns then don't try to find correctly firing guns (i.e. vacuum shot)
        if firing_gun_indeces.size == 0:
            continue
        # Get which guns were recording and firing correctly.
        recording_correct_gun_fires = working_guns.get_correct_gun_fires(gun_data.v_arc, recording_gun_indeces, average_v_curve, lower_curve_std, upper_curve_std)
        # Set which guns were firing correctly over all guns.
        correct_firing_gun_array[i, recording_gun_indeces - 1] = recording_correct_gun_fires
        # plot_guns(recording_gun_indeces, gun_data.time, gun_data.v_arc, gun_data.i_arc, shot_number, plot_time=False)

    logging.debug("Writting gun info to '{}'".format(file_name))
    scipy.io.savemat(file_name, {
        'shot_numbers': shot_num_array,
        'recording_guns': recording_gun_array,
        'firing_guns': firing_gun_array,
        'correct_firing_guns': correct_firing_gun_array,
        'num_firing_guns': working_guns.count_num_firing_guns(firing_gun_array),
        'good_shot': working_guns.determine_shot_quality(firing_gun_array, correct_firing_gun_array)
    })

def plot_guns(gun_numbers, time_data, all_v_arc_data, all_i_arc_data, shot_num, reconnection_index=None, reconnection_time=None, plot_time=True):
    fig, (v_ax, i_ax) = plt.subplots(2, 1, sharex=True, figsize=(12, 8))
    fig.suptitle("Shot #{}: Gun current + voltage".format(shot_num))
    for i in range(len(gun_numbers)):
        v_data = all_v_arc_data[i]

        smooth_diameter = 21
        smooth_radius = (smooth_diameter - 1) // 2
        smoothing_array = np.ones(smooth_diameter)
        smoothed_v_data = np.convolve(v_data, smoothing_array, mode='valid')
        
        if plot_time:
            v_ax.plot(time_data[smooth_radius: -smooth_radius], smoothed_v_data, label="Gun #{}".format(gun_numbers[i]))
            i_ax.plot(time_data, all_i_arc_data[i], label="Gun #{}".format(gun_numbers[i]))

            if reconnection_time is not None:
                v_ax.axvline(x=reconnection_time, color='red', linestyle='dotted', lw=0.5)
                i_ax.axvline(x=reconnection_time, color='red', linestyle='dotted', lw=0.5)
        else:
            v_ax.plot(np.arange(smooth_radius, len(smoothed_v_data) + smooth_radius), smoothed_v_data, label="Gun #{}".format(gun_numbers[i]))
            i_ax.plot(all_i_arc_data[i], label="Gun #{}".format(gun_numbers[i]))

            if reconnection_index is not None:
                v_ax.axvline(x=reconnection_index, color='red', linestyle='dotted', lw=0.5)
                i_ax.axvline(x=reconnection_index, color='red', linestyle='dotted', lw=0.5)
    
    v_ax.set_ylabel('Voltage (V)')
    v_ax.legend()

    i_ax.set_ylabel('Current (A)')
    if plot_time:
        i_ax.set_xlabel('Time (s)')
    else:
        i_ax.set_xlabel('Array Index')
    i_ax.legend()

    plt.show()

def write_average_v_curve(shot_numbers, recording_guns, firing_guns, num_shots_to_plot, filename="./analysis/plasma_guns/data/average_v_curve.mat"):
    """Get the voltage curve averaged over many shots and many guns."""
    kept_curves = []

    average_curve = None
    num_curves = 0

    for i, shot_num in enumerate(tqdm(shot_numbers)):
        tree = get_remote_shot_tree(shot_num)
        gun_indeces = np.arange(1, NUM_GUNS + 1)[np.nonzero(recording_guns[i])]

        gun_data = Plasma_Guns_Data(tree, i_arc=True, v_arc=True, time=True, guns=gun_indeces)

        # Get which guns were firing for this shot.
        for j, gun_index in enumerate(gun_indeces):
            # Skip all guns that weren't firing.
            if not firing_guns[i, gun_index - 1]:
                continue

            # If we haven't yet initialized the average curve, create it and set it to the size of the first curve.
            if average_curve is None:
                average_curve = np.zeros_like(gun_data.v_arc[j])

            # Sum the curves and count the number of curves added.
            gun_v_arc = gun_data.v_arc[j]
            average_curve[:gun_v_arc.size] += gun_v_arc[:average_curve.size]
            num_curves += 1

            # Check whether to keep the shot or not.
            if random.random() < (num_shots_to_plot - len(kept_curves)) / (len(shot_numbers) - i):
                kept_curves.append(gun_v_arc)

    # Change the sum to an average.
    average_curve /= num_curves

    logging.debug("Writing matrix to {}.".format(filename))
    scipy.io.savemat(filename, {'average_curve': average_curve, 'kept_curves': kept_curves})

def write_v_curve_standard_deviation(shot_numbers, recording_guns, firing_guns, filename="./analysis/plasma_guns/data/average_v_curve.mat"):
    """Using the average curve, get the standard deviation over all shots/guns."""
    loaded_mat = scipy.io.loadmat(filename)

    average_curve = loaded_mat['average_curve']

    lower_deviation = np.zeros_like(average_curve)
    upper_deviation = np.zeros_like(average_curve)

    lower_num_curves = np.zeros_like(average_curve)
    upper_num_curves = np.zeros_like(average_curve)

    for i, shot_num in enumerate(tqdm(shot_numbers)):
        tree = get_remote_shot_tree(shot_num)
        gun_indeces = np.arange(1, NUM_GUNS + 1)[np.nonzero(recording_guns[i])]

        try:
            gun_data = Plasma_Guns_Data(tree, i_arc=True, v_arc=True, time=True, guns=gun_indeces)
        except:
            continue

        for j, gun_index in enumerate(gun_indeces):
            if not firing_guns[i, gun_index - 1]:
                continue

            gun_v_arc = gun_data.v_arc[j]
            # Smooth the v_arc data to reduce outliers.
            smooth_diameter = 11
            smoothing_array = np.ones(smooth_diameter)
            smoothed_v_arc = np.convolve(gun_v_arc, smoothing_array, mode='same')
            smoothed_v_arc[:(smooth_diameter - 1) // 2] = gun_v_arc[:(smooth_diameter - 1) // 2]
            smoothed_v_arc[-(smooth_diameter - 1) // 2:] = gun_v_arc[-(smooth_diameter - 1) // 2:]

            curve_difference = average_curve - gun_v_arc

            negative_indeces = curve_difference <= 0
            positive_indeces = curve_difference >= 0
            
            lower_num_curves += negative_indeces
            upper_num_curves += positive_indeces

            lower_deviation += (curve_difference * negative_indeces)**2
            upper_deviation += (curve_difference * positive_indeces)**2

    lower_deviation = (lower_deviation / (lower_num_curves - 1))**0.5
    upper_deviation = (upper_deviation / (upper_num_curves - 1))**0.5

    loaded_mat['lower_deviation'] = lower_deviation
    loaded_mat['upper_deviation'] = upper_deviation
    scipy.io.savemat(filename, loaded_mat)

def plot_average_v_curve(filename="./analysis/plasma_guns/data/average_v_curve.mat"):
    loaded_mat = scipy.io.loadmat(filename)

    average_curve = loaded_mat['average_curve'][0]
    kept_curves = loaded_mat['kept_curves'][:50]
    lower_deviation = loaded_mat['lower_deviation'][0]
    upper_deviation = loaded_mat['upper_deviation'][0]

    for curve in kept_curves:
        plt.plot(curve, color="black", alpha=0.02)

    multiples = list(range(1, 6))
    cmap = LinearSegmentedColormap.from_list('blue_to_white', np.array(('blue', 'white')), N=len(multiples) + 1)
    
    for i, m in enumerate(multiples):
        plt.plot(average_curve - m * lower_deviation, color=cmap(i), lw=1)
        plt.plot(average_curve + m * upper_deviation, color=cmap(i), lw=1)
    plt.plot(average_curve, color="red", lw=2, label="Average")
    plt.xlabel("Data index")
    plt.ylabel("Voltage")
    plt.ylim(-200, 800)
    plt.title("Average Voltage curve of Gun Firing")
    plt.show()


if __name__=="__main__":
    logging.basicConfig(level=logging.INFO)

    shot_numbers = np.arange(50066, 54215)
    # Shot 53651 on has incorrect gun mapping (gun 16 is actually TF coil) Maybe not???

    bad_shots = np.array([
        50779, 50780, 50781, 50782, 50783, 50784, 50785, 50786, 50787, 50788, 50970, 50972, 51437, 
        51438, 51439, 51440, 51441, 51442, 51443, 51444, 51445, 51445, 51446, 51447, 51448, 51449, 
        51450, 51451, 51452, 51453, 51454, 51455, 51456, 51457, 51458
    ])

    avg_curve_mat = scipy.io.loadmat('./analysis/plasma_guns/data/average_v_curve.mat')
    average_v_curve = avg_curve_mat['average_curve'][0]
    lower_curve_std = avg_curve_mat['lower_deviation'][0]
    upper_curve_std = avg_curve_mat['upper_deviation'][0]

    write_gun_data(shot_numbers, average_v_curve, lower_curve_std, upper_curve_std)
    exit()

    gun_shot_info_mat = scipy.io.loadmat('./analysis/plasma_guns/data/gun_shot_info.mat')
    shot_numbers = gun_shot_info_mat['shot_numbers']
    good_shot = gun_shot_info_mat['good_shot']
    bad_shot_indeces = np.nonzero(1 - good_shot)
    recording_guns = gun_shot_info_mat['recording_guns']
    firing_guns = gun_shot_info_mat['firing_guns']
    correct_firing_guns = gun_shot_info_mat['correct_firing_guns']

    found_bad_shots = shot_numbers[bad_shot_indeces]
    for shot in found_bad_shots:
        tree = get_remote_shot_tree(shot)
        
        i = np.where(shot_numbers == shot)[1]
        gun_indeces = to_gun_index(recording_guns[i][0])
        gun_data = Plasma_Guns_Data(tree, guns=gun_indeces)

        logging.info(shot)
        logging.info('Firing guns: {}'.format(to_gun_index(firing_guns[i][0])))
        logging.info('Incorrect firing guns: {}'.format(to_gun_index(firing_guns[i][0] - correct_firing_guns[i][0])))
        plot_guns(gun_indeces, gun_data.time, gun_data.v_arc, gun_data.i_arc, shot, plot_time=False)