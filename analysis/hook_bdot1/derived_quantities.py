from hook_bdot1.get_data import Hook_Bdot1_Data
import numpy as np
from modules.bdot.interpolation import get_db_machine_coords
from modules.coordinates import Position, Vector
import matplotlib.pyplot as plt


if __name__ == "__main__":
    hook_data = Hook_Bdot1_Data(59944, time_index_range=(65000, 65170))

    fig = plt.figure()
    gs = fig.add_gridspec(2, 3, hspace=0, wspace=0)
    axs = gs.subplots(sharex=True, sharey=True)
    titles = [r'$\partial_t B_1$', r'$\partial_t B_{{2}}$', r'$\partial_t B_{{2}}$', r'$\partial_t B_3$']
    
    db_2 = np.where(
        (hook_data.db_2a_working & hook_data.db_2b_working)[:, np.newaxis], (hook_data.db_2a + hook_data.db_2b) / 2, 
        np.where(hook_data.db_2a_working[:, np.newaxis], hook_data.db_2a, 
        np.where(hook_data.db_2b_working[:, np.newaxis], hook_data.db_2b, 0))
    )

    db_2_working = hook_data.db_2a_working | hook_data.db_2b_working
    for ax, title, db_vals, z_vals, working in zip(
        axs[0], titles, [hook_data.db_1, db_2, hook_data.db_3], 
        # [hook_data.db_1_position[:, 2], hook_data.db_2a_position[:, 2], hook_data.db_2b_position[:, 2], hook_data.db_3_position[:, 2]],
        [None, None, None],
        [hook_data.db_1_working, db_2_working, hook_data.db_3_working]
    ):
        db_vals_to_plot = db_vals[np.nonzero(working)]
        db_vals_to_plot -= np.mean(db_vals_to_plot[:, :10], axis=1)[:, None]

        cmesh = ax.pcolormesh(
            # np.arange(1, db_vals.shape[0] + 1)[np.nonzero(working)], np.arange(65000, 65171), db_vals_to_plot.T, 
            hook_data.position[np.nonzero(working), 2], np.arange(65000, 65171), db_vals_to_plot.T, 
            vmin=-np.max(abs(db_vals_to_plot)), vmax=np.max(abs(db_vals_to_plot)), cmap='seismic', shading='nearest'
        )
        
        ax.set_title(title)
        fig.colorbar(cmesh, ax=ax, pad=0)
    
    fig.suptitle("Shot #{}: Hook Data".format(hook_data.shot_number))

    plt.show()
