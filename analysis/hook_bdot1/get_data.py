"""Get data about the hook_bdot1 probe from the MDSplus tree."""
from modules.generic_get_data import Get, Data, lazy_get, Port
import logging
import numpy as np


class Hook_Bdot1_Data(Data):
    def __init__(self, tree, *args, **kwargs):
        """
        Hold all the data for the hook_bdot1 probe in one class for easier access and reduced network connections.

        Notes
        -----
        To get data, call the data without the underscore treating it as an attribute.
        """
        super().__init__(tree, [], [], *args, **kwargs)

    @lazy_get
    def db_r(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB_r/dt for each set of conglomerated coils. Of shape `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(Get('\\hook_bdot1_dbr'))

    @lazy_get
    def db_phi(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB_phi/dt for each set of conglomerated coils. Of shape `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(Get('\\hook_bdot1_dbphi'))

    @lazy_get
    def db_z(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB_z/dt for each set of conglomerated coils. Of shape `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(Get('\\hook_bdot1_dbz'))

    @lazy_get
    def position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each set of conglomerated coils. Of shape `(NUM_PROBES, 3)`.
        """
        return self.get(Get('TRANSPOSE( [' + ','.join('DATA(\\hook_bdot1_{})'.format(direction) for direction in ['r', 'phi', 'z']) + '] )', name='hook_bdot1_position', signal=False))
    
    @lazy_get
    def db_1(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 1 coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\hook_bdot1_db1'))
    
    @lazy_get
    def db_1_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 1 coil. Of shape `(NUM_COILS, 3)`.
        """
        pass
    
    @lazy_get
    def db_1_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 1 coil. Of shape `(NUM_COILS,)`.
        """
        # As the linear_bdot1 probe does not yet have a working attribute we manually set it.
        logging.warning("Using hard-coded `Linear_Bdot1_Data.db_1_working` attribute.")
        return np.array([1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=bool)

    @lazy_get
    def db_2a(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 2a coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\hook_bdot1_db2a'))
    
    @lazy_get
    def db_2a_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 2a coil. Of shape `(NUM_COILS, 3)`.
        """
        pass
    
    @lazy_get
    def db_2a_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 2a coil. Of shape `(NUM_COILS,)`.
        """
        logging.warning("Using hard-coded `Hook_Bdot1_Data.db_2a_working` attribute.")
        return np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=bool)

    @lazy_get
    def db_2b(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 2b coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\hook_bdot1_db2b'))
    
    @lazy_get
    def db_2b_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 2b coil. Of shape `(NUM_COILS, 3)`.
        """
        pass
    
    @lazy_get
    def db_2b_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 2b coil. Of shape `(NUM_COILS,)`.
        """
        logging.warning("Using hard-coded `Hook_Bdot1_Data.db_2b_working` attribute.")
        return np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=bool)

    @lazy_get
    def db_3(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 3 coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\hook_bdot1_db3'))
    
    @lazy_get
    def db_3_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 3 coil. Of shape `(NUM_COILS, 3)`.
        """
        pass
    
    @lazy_get
    def db_3_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 3 coil. Of shape `(NUM_COILS,)`.
        """
        logging.warning("Using hard-coded `Hook_Bdot1_Data.db_3_working` attribute.")
        return np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=bool)

    @lazy_get
    def time(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of time values. Of shape `(NUM_TIMES,)`.
        """
        return self.get(Get('dim_of(\\hook_bdot1_dbphi)', name='hook_bdot1_time'))
        
    @lazy_get
    def port(self) -> Port:
        """
        Returns
        -------
        Port
            Object that represents the port position of this probe.
        """
        return Port(self, 'hook_bdot1_')

