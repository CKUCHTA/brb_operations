"""Get data about the te probe from the MDSplus tree."""
from modules.generic_get_data import Get, Data, lazy_get
import numpy as np
from scipy.io import loadmat
import logging


class Te_Probe3_Data(Data):
    """Hold all the data for the te probe in one class for easier access and reduced network connections."""
    NUM_TIPS = 16
    TIP_RADIUS = 4.953 * 10**-4 # m

    def __init__(self, tree, *args, **kwargs):
        super().__init__(tree, [], [], *args, **kwargs)

    @lazy_get
    def iprobe(self):
        """
        The current flowing into the probe tip.

        Returns
        -------
        np.array[float]
            2D-array of shape `(NUM_TIPS, NUM_TIMES)`.
        """
        return self.get(Get('\\te3_iprobe'))

    @lazy_get
    def vprobe(self):
        """
        The voltage of the probe tip.

        Returns
        -------
        np.array[float]
            2D-array of shape `(NUM_TIPS, NUM_TIMES)`.
        """
        return self.get(Get('\\te3_vprobe'))

    @lazy_get
    def working(self):
        """
        Working status of the probe tips.

        Returns
        -------
        np.array[bool]
            1D-array of shape `(NUM_TIPS,)`.
        """
        if self.shot_number >= 65451:
            logging.warning("Returning a static working array not yet in MDSplus.")
            working_array = self.get(Get('\\te3_working', signal=False), np_data_type=bool)
            if self.shot_number >= 65567:
                bad_tips = np.array([1, 2, 3, 4, 9, 13, 16])
            else:
                bad_tips = np.array([9, 13, 16])
            working_array[bad_tips - 1] = False
            return working_array
        else:
            return self.get(Get('\\te3_working', signal=False), np_data_type=bool)

    @lazy_get
    def bdot(self):
        """
        The measured voltage of the B-dot coils.
        
        Returns
        -------
        np.array[float]
            2D-array of shape `(3, NUM_TIMES)`. The first dimension corresponds 
            to the direction of the coil. The directions are not in general
            mapped to machine coordinates.
        """
        return self.get(Get(
            '[' + ', '.join('DATA(\\te3_bdot_ax{})'.format(i) for i in range(1, 4)) + ']', name='te3_bdot'
        ))

    @lazy_get
    def time(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of time values. Of shape `(NUM_TIMES,)`.
        """
        return self.get(Get('dim_of(\\te3_iprobe)', name='te3_time'))

    @lazy_get
    def vbias(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of bias voltages of shape `(NUM_TIPS,)`.
        """
        return self.get(Get('[' + ', '.join('DATA(\\te3_{}_vbias)'.format(str(i).zfill(2)) for i in range(1, 17)) + ']', name='vbias', signal=False))

    @lazy_get
    def vbias_curves(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of bias voltages over time of shape `(NUM_TIPS, NUM_TIMES)`.

        Notes
        -----
        There is a lot of noise on the bias voltage measurements so this should 
        only be used to see trends, not the actual change in voltage.
        """
        return self.get(Get('[' + ', '.join('DATA((\\te3_{i}_vbias_raw - \\te3_{i}_vbias_off) / \\te3_{i}_vbias_div)'.format(i=str(i).zfill(2)) for i in range(1, 17)) + ']', name='vbias_curves'))

    @lazy_get
    def r(self):
        """
        Returns
        -------
        float
            `r` position of probe in meters.
        """
        return self.get(Get('\\te3_r', signal=False))

    @lazy_get
    def phi(self):
        """
        Returns
        -------
        float
            `phi` position of probe in radians.
        """
        return self.get(Get('\\te3_phi', signal=False))

    @lazy_get
    def z(self):
        """
        Returns
        -------
        float
            `z` position of probe in meters.
        """
        return self.get(Get('\\te3_z', signal=False))

    @lazy_get
    def v_s(self):
        """
        Returns
        -------
        np.array[float]
            2D array of measured signal voltages of shape `(# of probes, # of time slices)`.
        """
        return self.get(Get('[' + ', '.join('DATA(\\te3_{}_sv)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 17)) + ']', name='te3_v_s'))

    @lazy_get
    def v_n(self):
        """
        Returns
        -------
        np.array[float]
            2D array of measured noise voltages of shape `(# of probes, # of time slices)`.
        """
        return self.get(Get('[' + ', '.join('DATA(\\te3_{}_nv)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 17)) + ']', name='te3_v_n'))

    @lazy_get
    def r_diggnd(self):
        """
        Resistance between digitizer measurement and chassis ground.

        Returns
        -------
        np.array[float]
            1D array of shape `(NUM_TIPS,)` of resistances in ohms.
        """
        return self.get(Get('[' + ', '.join('DATA(\\te3_{}_rdiggnd)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 17)) + ']', name='te3_r_diggnd', signal=False))

    @lazy_get
    def r_div(self):
        """
        Resistance between the voltage divider input and chassis ground.

        Returns
        -------
        np.array[float]
            1D array of shape `(NUM_TIPS,)` of resistances in ohms.
        """
        return self.get(Get('[' + ', '.join('DATA(\\te3_{}_rdiv)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 17)) + ']', name='te3_r_div', signal=False))

    @lazy_get
    def r_sense(self):
        """
        Resistance of the sense resistor.
        
        Returns
        -------
        np.array[float]
            1D array of shape `(NUM_TIPS,)` of resistances in ohms.
        """
        return self.get(Get('[' + ', '.join('DATA(\\te3_{}_rsense)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 17)) + ']', name='te3_r_sense', signal=False))

    @lazy_get
    def r_wire(self):
        """
        Resistance of the wire connecting the sense resistor to the probe tip.

        Returns
        -------
        np.array[float]
            1D array of shape `(NUM_TIPS,)` of resistances in ohms.
        """
        return self.get(Get('[' + ', '.join('DATA(\\te3_{}_rwire)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 17)) + ']', name='te3_r_wire', signal=False))

    @lazy_get
    def relative_area(self):
        """
        Relative area of the probe tip normalized by the actual area.
        
        Returns
        -------
        np.array[float]
            1D array of shape `(NUM_TIPS,)` with no units.
        """
        return self.get(Get('[' + ', '.join('DATA(\\te3_{}_relarea)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 17)) + ']', name='te3_relative_area', signal=False))
    
    @lazy_get
    def normalization_area(self):
        """
        Actual area to normalize the relative areas by.

        Returns
        -------
        float
            Area in meters squared.
        """
        logging.warning("Returning a static normalization area not yet in MDSplus.")
        return 8 * 10**-5
        return self.get(Get('\\te3_act_area', name='te3_normalization_area', signal=False))

    # TODO: Change this so that you don't need to do a separate call when getting working stuff?
    def extract_working(self, signal):
        return signal[np.nonzero(self.working)]

    def extract_not_working(self, signal):
        return signal[np.nonzero(1 - self.working)]

