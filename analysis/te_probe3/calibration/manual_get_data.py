"""Manually get the probe currents and voltages instead of using the precalculated MDSplus values."""
from modules.generic_get_data import Get
import numpy as np
import logging
from scipy.io import loadmat


def to_str(tip_number):
    """Change a tip number to a string."""
    return str(tip_number).zfill(2)

def v_s(te_data, tip):
    """Voltage measured by signal digitizer."""
    tip_str = to_str(tip)
    return getattr(te_data, 'v_s_{}'.format(tip_str), te_data.get(Get('\\te3_{}_sv'.format(tip_str))))

def v_n(te_data, tip):
    """Voltage measured by noise digitizer."""
    # Since the noise connection for tip 16 doesn't work we will use tip 15s.
    if tip == 16:
        logging.warning("Changing the tip 16 noise connection to tip 15 since not working.")
        # tip = 16
        tip = 15
    tip_str = to_str(tip)
    return getattr(te_data, 'v_n_{}'.format(tip_str), te_data.get(Get('\\te3_{}_nv'.format(tip_str))))

def v_p(te_data, tip, r_diggnd, r_divider, r_sense, r_wire, v_bias, alternate=False):
    """Probe voltage. Since calibration is often done with all tips at the same bias we pass it as a parameter."""
    return v_p_func(v_s(te_data, tip), v_n(te_data, tip), r_diggnd, r_divider, r_sense, r_wire, v_bias, alternate)

def v_p_func(v_s_val, v_n_val, r_diggnd, r_divider, r_sense, r_wire, v_bias, alternate=False):
    """Probe voltage. Since calibration is often done with all tips at the same bias we pass it as a parameter."""
    # Shape the bias voltage we use so we can do the math.
    if len(v_s_val.shape) == 2:
        if v_bias.shape == v_s_val.shape:
            v_b = v_bias
        elif v_bias.shape[0] == v_s_val.shape[0]:
            v_b = v_bias[:, None]
        elif v_bias.shape[0] == v_s_val.shape[1]:
            v_b = v_bias[None, :]

    # TODO: Choose the correct current calculation.
    if not alternate:
        # This model is if the ground voltage changes throughout the shot.
        if len(v_s_val.shape) == 2:
            return ((r_divider + r_wire + r_divider * r_wire / r_sense) / r_diggnd)[:, None] * (v_s_val - v_n_val) - (r_sense / r_diggnd)[:, None] * v_n_val + v_b
        else:
            return ((r_divider + r_wire + r_divider * r_wire / r_sense) / r_diggnd) * (v_s_val - v_n_val) - (r_sense / r_diggnd) * v_n_val + v_bias
    else:
        # This model is if there is common mode current running down both the signal and noise wires.
        if len(v_s_val.shape) == 2:
            i_s_val = v_s_val / r_diggnd[:, None]
            return i_s_val * ((r_divider * r_sense + r_divider * r_wire + r_sense * r_wire) / r_sense)[:, None] + v_b
        else:
            i_s_val = v_s_val / r_diggnd
            return i_s_val * (r_divider * r_sense + r_divider * r_wire + r_sense * r_wire) / r_sense + v_bias

def i_p(te_data, tip, r_diggnd, r_divider, r_sense, a_rel):
    """Probe current."""
    return i_p_func(v_s(te_data, tip), v_n(te_data, tip), r_diggnd, r_divider, r_sense, a_rel)

def i_p_func(v_s_val, v_n_val, r_diggnd, r_divider, r_sense, a_rel):
    """Probe current."""
    if len(v_s_val.shape) == 2:
        return ((r_divider + r_sense) / (r_sense * a_rel * r_diggnd))[:, None] * (v_s_val - v_n_val)
    else:
        return (r_divider + r_sense) / (r_sense * a_rel * r_diggnd) * (v_s_val - v_n_val)
    
def load_parameters(filepath='./notebooks/data/te_probe3/optimized_vals_no_4_no_res_w_prior.mat', all_tips=False):
    """
    Parameters
    ----------
    filepath : str
        Path to `.mat` file that contains optimized values for specific tips optimized.
    all_tips : bool, default=False
        If `False` then return back the optimized value arrays where the num 
        elements may be less than the total number of tips. If `True` then 
        return arrays that have 16 elements but are just filled with the average 
        of the two neighboring points.
    """
    # TODO: Add these parameters to MDSplus.
    loaded_file = loadmat(filepath)
    r_diggnd = loaded_file['R_diggnd'][0]
    r_div = loaded_file['R_div'][0]
    r_sense = loaded_file['R_sense'][0]
    r_wire = loaded_file['R_wire'][0]
    a_rel = loaded_file['A_rel'][0]
    alternate_vp = bool(loaded_file['alternate_vp'][0, 0])
    tips_used = loaded_file['tip_numbers'][0]

    if not all_tips:
        return r_diggnd, r_div, r_sense, r_wire, a_rel, alternate_vp, tips_used
    else:
        return_arrays = []
        for arr in [r_diggnd, r_div, r_sense, r_wire, a_rel]:
            new_arr = np.zeros(16, dtype=arr.dtype)
            new_arr[tips_used - 1] = arr
            for i in range(new_arr.shape[0]):
                if i not in tips_used - 1:
                    new_arr[i] = new_arr[i - 1]
            return_arrays.append(new_arr)
        
        return *return_arrays, alternate_vp, tips_used
