"""Optimization schemes used for finding probe parameters."""
from te_probe2.calibration.change_data import get_usable_mask, smooth_data
from te_probe2.calibration.plot_data import plot_usable_data
import numpy as np
from numpy.linalg import multi_dot
from te_probe3.get_data import Te_Probe3_Data
from te_probe3.calibration import manual_get_data
from scipy.optimize import differential_evolution
from itertools import combinations
import matplotlib.pyplot as plt
import logging
from scipy.integrate import nquad
from scipy.signal import savgol_filter
from scipy.special import erf
from scipy.io import savemat, loadmat
import random
from os.path import isfile
import tqdm


NUM_TIPS = 16
TIP_NUMBERS = range(1, NUM_TIPS + 1)
TIP_NUM_STRS = [str(i).zfill(2) for i in TIP_NUMBERS]

def optimize_esat(v_signal, v_noise, r_diggnd, r_div, r_sense, r_wire_lims, v_p_act_lims, sigma_v_lims):
    """
    Optimize the the wire resistance by using Bayesian statistics.
    
    Parameters
    ----------
    v_signal, v_noise : np.array
        2D array of voltages of shape (# of tips, # of time slices) that should be used for optimization.
    r_diggnd, r_div, r_sense : np.array
        1D array of shape (# of tips) with probe parameters for an individual tip.
    r_wire_lims, v_p_act_lims, sigma_v_lims : tuple[float]
        Maximum and minimum bounds on wire resistance, actual plasma voltage, and standard deviation of plasma voltage.

    Returns
    -------
    r_wire : np.array
        Maximum of predicted wire resistance.

    Notes
    -----
    We ignore the V_bias as all tips have the same bias voltage.
    """
    num_time_slices, num_tips = v_signal.shape

    c0 = r_div / r_diggnd
    c1 = (1 + r_div / r_sense) / r_diggnd

    method = 4 

    # Choose which method. Method 3 is the simplest and has done most of the math by hand.
    if method == 1 or method == 2:
        term_1 = c0**2 * np.einsum('ij,ij', v_signal, v_signal)
        term_2 = c1**2 * np.einsum('...j,...j', (v_signal - v_noise), (v_signal - v_noise))
        term_3 = c0 * c1 * np.einsum('...j,...j', v_signal, (v_signal - v_noise))
        term_4 = -c0 * np.einsum('ij->j', v_signal)
        term_5 = -c1 * (v_signal - v_noise)
        def log_prob(v_p_act, sigma_v, r_wire):
            """
            Get the log of the probability for a specified parameter set.
            
            Parameters
            ----------
            v_p_act : np.array
                Actual V_plasma values for each time slice. Shape (# of time slices,).
            sigma_v : float
                Standard deviation of V_plasma.
            r_wire : np.array
                Wire resistances for each tip. Shape (# of tips,).
            
            Returns
            -------
            float
            """
            if method == 1:
                return -num_time_slices * num_tips * np.log(sigma_v) - 1 / (2 * sigma_v) * (
                    term_1 + np.dot(term_2, r_wire**2) - np.dot(v_p_act, v_p_act) + np.dot(r_wire, term_3) + np.dot(term_4, v_p_act) + multi_dot((r_wire, term_5, v_p_act))
                )
            else:
                return -num_time_slices * num_tips * np.log(sigma_v) - np.sum((c0 * v_signal + c1 * r_wire[:, None] * (v_signal - v_noise) - v_p_act)**2) / (2 * sigma_v**2)

        full_prob = lambda r_wire: nquad(log_prob, (v_p_act_lims, sigma_v_lims), r_wire)

        # Get the maximum of the probability.
        result = differential_evolution(full_prob, [r_wire_lims for _ in range(num_tips)], workers=4)

        print("Wire resistances:", result.x)
        return result
    elif method == 3:
        term_1 = -c1**2 * np.einsum('...j,...j', (v_signal - v_noise), (v_signal - v_noise))
        term_2 = c1 * np.einsum('ij->i', (v_signal - v_noise)) * ((v_p_act_lims[1]**2 - v_p_act_lims[0]**2) / (2 * (v_p_act_lims[1] - v_p_act_lims[0])) - 
            c0 * c1 * np.einsum('ij->i', v_signal * (v_signal - v_noise)))

        print('1', term_1)
        print('2', term_2)
        full_prob = lambda r_wire: np.dot(r_wire**2, term_1) + np.dot(r_wire, term_2)

        return full_prob
    elif method == 4:
        # Choose what the actual V_p should be for each time slice. We choose the most negative V_p.
        V_p = np.min(c0[:, None] * v_signal, axis=0)

        # Calculate the terms that we can ahead of time.
        term_1 = np.einsum('i,ij,j->', c0**2, v_signal**2, 1 / V_p**2) - 2 * np.einsum('i,ij,j->', c0, v_signal, 1 / V_p) + v_signal.size - np.math.log((2 * np.pi)**0.5)
        term_2 = 2 * np.einsum('i,ij,j->i', c0 * c1, v_signal * (v_signal - v_noise), 1 / V_p**2) - 2 * np.einsum('i,ij,j->i', c1, v_signal - v_noise, 1 / V_p)
        term_3 = np.einsum('i,ij,j->i', c1**2, (v_signal - v_noise)**2, 1 / V_p**2)

        r_at_min = -term_2 / (2 * term_3)
        print("Second derivative:", -1 * term_3)
        print("Critical point at R = {}.".format(r_at_min))

        full_prob = lambda r: -0.5 * (np.dot(r**2, term_3) + np.dot(r, term_2) + term_1)
        print("Probability:", full_prob(r_at_min))

        def alt_prob(r):
            total = 0
            val_diff = 0
            for i in range(v_signal.shape[0]):
                for j in range(v_signal.shape[1]):
                    non_expanded_val = ((c0[i] * v_signal[i, j] + c1[i] * r[i] * (v_signal[i, j] - v_noise[i, j])) / V_p[j] - 1)**2

                    new_term_1 = c0[i]**2 * v_signal[i, j]**2 / V_p[j]**2 + 1 - 2 * c0[i] * v_signal[i, j] / V_p[j]
                    new_term_2 = (2 * c0[i] * c1[i] * v_signal[i,j] * (v_signal[i,j] - v_noise[i,j]) / V_p[j]**2 - 2 * c1[i] * (v_signal[i,j] - v_noise[i,j]) / V_p[j])
                    new_term_3 = c1[i]**2 * (v_signal[i,j] - v_noise[i,j])**2 / V_p[j]**2
                    expanded_val = new_term_1 + r[i] * new_term_2 + r[i]**2 * new_term_3
                    val_diff += expanded_val - non_expanded_val

                    # total -= ((c0[i] * v_signal[i, j] + c1[i] * r[i] * (v_signal[i, j] - v_noise[i, j])) / V_p[j] - 1)**2
                    total -= expanded_val
            # print(val_diff)
            return total

        if False:
            # Plot probabilities for r1, r2 slice.
            r1_vals = np.linspace(r_at_min[0] * 0.9, r_at_min[0] * 1.1, num=10)
            r2_vals = np.linspace(r_at_min[1] * 0.9, r_at_min[1] * 1.1, num=10)

            prob_array = np.zeros((r1_vals.size, r2_vals.size))
            alt_prob_array = np.zeros_like(prob_array)
            from tqdm import tqdm
            for i, r1 in enumerate(tqdm(r1_vals, desc='Probability plotting.')):
                for j, r2 in enumerate(r2_vals):
                    full_r_array = np.zeros(NUM_TIPS)
                    full_r_array[0] = r1
                    full_r_array[1] = r2
                    prob_array[i, j] = full_prob(full_r_array)
                    alt_prob_array[i, j] = alt_prob(full_r_array)
                    print(prob_array[i, j] - alt_prob_array[i, j])

            fig, axs = plt.subplots(1, 2)
            axs[0].imshow(prob_array, origin='lower', extent=(r1_vals[0], r1_vals[-1], r2_vals[0], r2_vals[-1]))
            axs[0].axvline(r_at_min[0])
            axs[0].axhline(r_at_min[1])
            axs[1].imshow(alt_prob_array, origin='lower', extent=(r1_vals[0], r1_vals[-1], r2_vals[0], r2_vals[-1]))
            axs[1].axvline(r_at_min[0])
            axs[1].axhline(r_at_min[1])
            plt.show()
        if True:
            uncorrected = c0[:, None] * v_signal
            corrected = c0[:, None] * v_signal + c1[:, None] * r_at_min[:, None] * (v_signal - v_noise)

            # Plot the V_probe data.
            for data, color, label in zip([uncorrected, corrected], ['red', 'blue'], ['uncorrected', 'corrected']):
                for i, v in enumerate(data):
                    if i == 0:
                        plt.plot(v, color=color, label=label)
                    else:
                        plt.plot(v, color=color)

            plt.plot(V_p, color='green', linewidth=10, alpha=0.25)
            plt.legend()
            plt.show()

        return full_prob

def optimization_func(
        x, sigma_diggnd, sigma_div, sigma_sense, N_esat, N_isat, sigma_v, 
        r_diggnd_array, r_div_array, r_sense_array,
        I_probe_bounds, V_probe_bounds, V_bias, v_signal, v_noise, boundary_tip_index, alternate_vp=False
    ):
    """
    Log probability function to optimize to find resistances and areas.
    
    Parameters
    ----------
    x : np.array
        1D array of possible resistances and areas to get probability of.
    sigma_diggnd, sigma_div, sigma_sense : float
        Standard deviation of resistance values.
    N_esat, N_isat : int
        Number of esat and isat points that are being used.
    sigma_v : float
        Error in measured v_s, v_n.
    r_diggnd_array, r_div_array, r_sense_array : np.array[float]
        Measured values of circuit resistances.
    I_probe_bounds, V_probe_bounds : np.array
        2D array of bounds of shape `(2, N_isat)`, `(4, N_esat)`. The first row 
        (and third row for `V_probe_bounds`) is the lower bound and the second 
        (and fourth row) is the upper for each time point.
    V_bias : np.array
        1D array of bias voltages of shape `(N_esat,)`.
    v_signal, v_noise : dict[str, np.array]
        Dictionary of 2D arrays of measured signal and noise voltage. The dictionary has `'esat'` and `'isat'` and 
        the array shapes are `(NUM_TIPS, N_esat)` or `(NUM_TIPS, N_isat)` respectively.
    boundary_tip_index : int, default=8
        Index where all `indeces < boundary_tip_index` have one V_p they are 
        fitting and all `indeces >= boundary_tip_index` have another.
    alternate_vp : bool, default=False
        Whether to use the alternate V_p function.
    """
    r_diggnd = x[:NUM_TIPS]
    r_div = x[NUM_TIPS:2 * NUM_TIPS]
    r_sense = x[2 * NUM_TIPS:3 * NUM_TIPS]
    r_wire = x[3 * NUM_TIPS:4 * NUM_TIPS]
    a_rel = x[4 * NUM_TIPS:5 * NUM_TIPS]

    # a0 * I_probe = V_ds - V_dn
    a0 = r_sense * a_rel * r_diggnd / (r_div + r_sense)
    # a1 * (V_probe - V_bias) = V_ds - a2 * V_dn
    a1 = r_diggnd / (r_div + r_wire * (1 + r_div / r_sense))
    if alternate_vp:
        a2 = np.ones_like(a1)
    else:
        a2 = 1 + (r_sense / r_diggnd) * a1

    # Since differential evolution tries to minimize the function, we multiply by -1.
    val = -(
        # Prior for relative area.
        np.sum(np.log(a_rel)) +
        # Normal distributions of resistances.
        -np.dot((r_diggnd - r_diggnd_array) / sigma_diggnd, (r_diggnd - r_diggnd_array) / sigma_diggnd) + 
        -np.dot((r_div - r_div_array) / sigma_div, (r_div - r_div_array) / sigma_div) + 
        -np.dot((r_sense - r_sense_array) / sigma_sense, (r_sense - r_sense_array) / sigma_sense) +
        # Isat component
        N_isat * np.sum(np.log(np.pi**0.5 * sigma_v / (2 * a0 / a_rel))) +
        np.sum(np.log(
            erf((a0[:, None] * I_probe_bounds[1][None, :] + v_noise['isat'] - v_signal['isat']) / sigma_v) -
            erf((a0[:, None] * I_probe_bounds[0][None, :] + v_noise['isat'] - v_signal['isat']) / sigma_v)
        )) +
        # Esat component
        np.sum(v_signal['esat'] * (v_signal['esat'] - 1) / sigma_v**2) +
        N_esat * np.sum(np.log(np.pi**0.5 * sigma_v / (2 * a1))) +
        np.sum(np.log(
            erf((a2[:boundary_tip_index, None] * v_noise['esat'][:boundary_tip_index] + a1[:boundary_tip_index, None] * (V_probe_bounds[1][None, :] - V_bias[None, :]) - v_signal['esat'][:boundary_tip_index]) / sigma_v) -
            erf((a2[:boundary_tip_index, None] * v_noise['esat'][:boundary_tip_index] + a1[:boundary_tip_index, None] * (V_probe_bounds[0][None, :] - V_bias[None, :]) - v_signal['esat'][:boundary_tip_index]) / sigma_v) 
        )) + 
        np.sum(np.log(
            erf((a2[boundary_tip_index:, None] * v_noise['esat'][boundary_tip_index:] + a1[boundary_tip_index:, None] * (V_probe_bounds[3][None, :] - V_bias[None, :]) - v_signal['esat'][boundary_tip_index:]) / sigma_v) -
            erf((a2[boundary_tip_index:, None] * v_noise['esat'][boundary_tip_index:] + a1[boundary_tip_index:, None] * (V_probe_bounds[2][None, :] - V_bias[None, :]) - v_signal['esat'][boundary_tip_index:]) / sigma_v) 
        ))
    )
    logging.debug('(R_wire, A_rel) = ({}, {})    Cost = {}'.format(np.around(r_wire, 0), np.around(a_rel, 1), val))
    return val

def optimize(v_signal, v_noise, v_bias, r_diggnd_array, r_div_array, r_sense_array, r_wire_lims, a_rel_lims, boundary_tip=9, save_filepath=None, alternate_vp=False, tips_to_use=None):
    """
    Optimize the parameters used for V_probe and I_probe.

    Parameters
    ----------
    v_signal, v_noise : dict[str, np.array]
        Two element dictionary of measured voltage arrays for esat and isat. Each array is 2D of shape `(NUM_TIPS, # of time slices)`.
    v_bias : dict[str, np.array]
        Two element dictionary of measured bias voltage for esat and isat. Each array is 1D of shape `(# of time slices,)`.
    r_diggnd_array, r_div_array, r_sense_array : np.array[float]
        1D array of approximate resistances for each probe of shape `(NUM_TIPS,)`.
    r_wire_lims, a_rel_lims : tuple[float]
        Two element tuple of `(lower, upper)` bounds for the probe params.
    boundary_tip : int, default=9
        Tip number where all `tip_nums < boundary_tip` have one V_p they are 
        fitting and all `tip_nums >= boundary_tip` have another. This 
        exists because in Esat the plasma is not cold nor dense enough to bring 
        all tips to the same voltage. The `boundary_tip` should separate 
        the tips where there is a large change in `R_sense`.
    save_filepath : str, default=None
        Path to save result to. If `None` then don't save.
    alternate_vp : bool, default=False
        Whether to use the alternate V_p function.
    tips_to_use : list[int], default=None
        Which tip numbers to calibrate. If `None` then use all data.

    Returns
    -------
    r_diggnd, r_div, r_sense, r_wire, a_rel : np.array
        1D array of optimized parameters.
    """
    if tips_to_use is None:
        boundary_tip_index = boundary_tip - 1
    else:
        boundary_tip_index = tips_to_use.index(boundary_tip)

    # We'll assume that the resistance error is some percent of the overall resistance.
    sigma_diggnd = 0.01 * r_diggnd_array
    sigma_div = 0.01 * r_div_array
    sigma_sense = 0.02 * r_sense_array

    N_esat = v_bias['esat'].size
    N_isat = v_bias['isat'].size

    # Estimated error in the measured signals.
    sigma_v = 0.1
    # Isat needs bounds on what we expect the current to be from the probe.
    # Take the minimum to be the minimum I_p assuming we know the resistances and the area is small.
    # Take the maximum to be the maximum I_p assuming we know the resistances and the area is large.
    I_probe_bounds = np.array((
        0.9 * np.min(manual_get_data.i_p_func(v_signal['isat'], v_noise['isat'], r_diggnd_array, r_div_array, r_sense_array, a_rel_lims[1]), axis=0),
        1.1 * np.max(manual_get_data.i_p_func(v_signal['isat'], v_noise['isat'], r_diggnd_array, r_div_array, r_sense_array, a_rel_lims[0]), axis=0),
    ))
    # Esat needs bounds on what we expect the probe voltage to be.
    esat_v_p_low_r_wire = manual_get_data.v_p_func(v_signal['esat'], v_noise['esat'], r_diggnd_array, r_div_array, r_sense_array, r_wire_lims[0], v_bias['esat'], alternate_vp)
    esat_v_p_high_r_wire = manual_get_data.v_p_func(v_signal['esat'], v_noise['esat'], r_diggnd_array, r_div_array, r_sense_array, r_wire_lims[1], v_bias['esat'], alternate_vp)
    V_probe_bounds = np.array((
        np.min(
            np.array((
                esat_v_p_low_r_wire[:boundary_tip_index],
                esat_v_p_high_r_wire[:boundary_tip_index],
            )), axis=(0, 1)
        ),
        np.max(
            np.array((
                esat_v_p_low_r_wire[:boundary_tip_index],
                esat_v_p_high_r_wire[:boundary_tip_index],
            )), axis=(0, 1)
        ),
        np.min(
            np.array((
                esat_v_p_low_r_wire[boundary_tip_index:],
                esat_v_p_high_r_wire[boundary_tip_index:],
            )), axis=(0, 1)
        ),
        np.max(
            np.array((
                esat_v_p_low_r_wire[boundary_tip_index:],
                esat_v_p_high_r_wire[boundary_tip_index:],
            )), axis=(0, 1)
        ),
    ))

    # Construct bounds
    factor = 1
    r_diggnd_bounds = [(r_diggnd_array[i] - factor * sigma_diggnd[i], r_diggnd_array[i] + factor * sigma_diggnd[i]) for i in range(NUM_TIPS)]
    r_div_bounds = [(r_div_array[i] - factor * sigma_div[i], r_div_array[i] + factor * sigma_div[i]) for i in range(NUM_TIPS)]
    r_sense_bounds = [(r_sense_array[i] - factor * sigma_sense[i], r_sense_array[i] + factor * sigma_sense[i]) for i in range(NUM_TIPS)]
    r_wire_bounds = [r_wire_lims for i in range(NUM_TIPS)]
    a_rel_bounds = [a_rel_lims for i in range(NUM_TIPS)]
    all_bounds = r_diggnd_bounds + r_div_bounds + r_sense_bounds + r_wire_bounds + a_rel_bounds

    # Set a good initial point.
    starting_point = np.concatenate((r_diggnd_array, r_div_array, r_sense_array, np.ones(NUM_TIPS), np.ones(NUM_TIPS)))

    # import timeit
    # all_vals = np.concatenate((r_diggnd_array, r_div_array, r_sense_array, np.zeros(NUM_TIPS), np.ones(NUM_TIPS)))
    # other_args = (
    #     sigma_diggnd, sigma_div, sigma_sense, N_esat, N_isat, sigma_v, I_probe_bounds, dVp1, dVp2, dVp3,
    #     np.sum(v_signal['isat'], axis=1), np.dot(v_bias['esat'], v_bias['esat']), np.sum(v_signal['esat'], axis=1), np.dot(v_signal['esat'], v_bias['esat']),
    #     np.sum(v_bias['esat'])
    # )
    # t = timeit.Timer(lambda: optimization_func(all_vals, *other_args))
    # num_times = 100
    # print([i / num_times for i in t.repeat(5, num_times)])
    # exit()
    
    logging.debug("Optimizing resistances and areas.")
    result = differential_evolution(optimization_func, all_bounds, args=[
        sigma_diggnd, sigma_div, sigma_sense, N_esat, N_isat, sigma_v, 
        r_diggnd_array, r_div_array, r_sense_array,
        I_probe_bounds, V_probe_bounds, v_bias['esat'], v_signal, v_noise, boundary_tip_index, alternate_vp
    ], workers=1)
    r_diggnd_opt = result.x[:NUM_TIPS]
    r_div_opt = result.x[NUM_TIPS:2 * NUM_TIPS]
    r_sense_opt = result.x[2 * NUM_TIPS:3 * NUM_TIPS]
    r_wire_opt = result.x[3 * NUM_TIPS:4 * NUM_TIPS]
    a_rel_opt = result.x[4 * NUM_TIPS:5 * NUM_TIPS]

    names = ["R_diggnd", "R_div", "R_sense", "R_wire", "A_rel"]
    original_vals = [r_diggnd_array, r_div_array, r_sense_array, np.zeros(NUM_TIPS), np.ones(NUM_TIPS)]
    new_vals = [r_diggnd_opt, r_div_opt, r_sense_opt, r_wire_opt, a_rel_opt]

    save_dict = {}
    for name, val in zip(names, original_vals):
        save_dict[name + '_old'] = val
    for name, val in zip(names, new_vals):
        save_dict[name] = val
    for i, name in enumerate(names):
        for j in range(NUM_TIPS):
            save_dict[name + '_bound_l'] = all_bounds[NUM_TIPS * i + j][0]
            save_dict[name + '_bound_u'] = all_bounds[NUM_TIPS * i + j][1]
    
    save_dict['alternate_vp'] = alternate_vp
    save_dict['tip_numbers'] = TIP_NUMBERS

    if save_filepath is not None:
        savemat(save_filepath, save_dict)

    print(result)
    for name, orig, new in zip(names, original_vals, new_vals):
        logging.info("{name}:\n\tOriginal:   {o}\n\tNew:        {n}\n\tDifference: {d}".format(name=name, o=orig, n=new, d=new - orig))
    return new_vals

def get_known_resistances(shot_num, load_data_file_func=None, save_data_file_func=None):
    """
    Get the `r_diggnd`, `r_div`, and `r_sense` values for each tip for a single shot.

    Parameters
    ----------
    shot_num : int
    load_data_file_func, save_data_file_func : func(int) -> str, default=None
        Function that takes a shot number and changes it into a filepath string to a saved data file. If not `None` then try to load or save the data.

    Returns
    -------
    r_diggnd_array, r_div_array, r_sense_array : np.array[float]
        1D arrays of resistances of shape (# of tips,)
    
    Notes
    -----
    These values should be the same for all shots so no need to get this more than once.
    """
    if load_data_file_func is not None and isfile(load_data_file_func(shot_num)):
        te_data = Te_Probe3_Data(shot_num, load_filepath=load_data_file_func(shot_num))
    else:
        te_data = Te_Probe3_Data(shot_num)

    r_diggnd_array = np.array([te_data.get('\\te3_{}_rdiggnd'.format(s)) for s in TIP_NUM_STRS])
    r_div_array = np.array([te_data.get('\\te3_{}_rdiv'.format(s)) for s in TIP_NUM_STRS])
    r_sense_array = np.array([te_data.get('\\te3_{}_rsense'.format(s)) for s in TIP_NUM_STRS])

    if save_data_file_func is not None:
        te_data.save(save_data_file_func(shot_num))

    return r_diggnd_array, r_div_array, r_sense_array

def extract_optimization_points(esat_shots, isat_shots, plot_shots=False, load_data_file_func=None, save_data_file_func=None, 
                                load_all_data_filepath=None, save_all_data_filepath=None, downsample=None, seed=None):
    """
    Get the points from the passed shots that we will use for optimization.
    
    Parameters
    ----------
    esat_shots, isat_shots : list[int]
        List of shot numbers to get data from.
    plot_shots : bool, default=False
        Whether to plot which points are usable for each shot.
    load_data_file_func, save_data_file_func : func(int) -> str, default=None
        Function that takes a shot number and changes it into a filepath string to a saved data file. If not `None` then try to load or save the data.
    load_all_data_filepath, save_all_data_filepath : str, default=None
        If not `None` then save or load the final data dictionaries before downsampling.
    downsample : int or float, default=None
        Choose a set of random points from all the points valid for optimization. If `None` then use all points. If a `float` between 0 and 1 then
        choose a corresponding percentage of points. If an `int` then choose that many points total.
    seed : int, default=None
        Seed to use for randomly choosing.
        
    Returns
    -------
    vs_dict, vn_dict, vbias_dict : dict[str, np.array]
        Dictionary to hold `'esat'` and `'isat'` point arrays. The arrays have shape `(# of tips, # of time slices)`.
    """
    # Get all the shot data into the arrays.
    vs_dict = {'esat': np.zeros((NUM_TIPS, 0)), 'isat': np.zeros((NUM_TIPS, 0))}
    vn_dict = {'esat': np.zeros((NUM_TIPS, 0)), 'isat': np.zeros((NUM_TIPS, 0))}
    vb_dict = {'esat': np.array([]), 'isat': np.array([])}

    # Load all the data.
    if load_all_data_filepath is not None and isfile(load_all_data_filepath):
        logging.debug("Loading all data from '{}'.". format(load_all_data_filepath))
        loaded_dictionary = loadmat(load_all_data_filepath)
        for dictionary, name in zip((vs_dict, vn_dict, vb_dict), ('vs', 'vn', 'vb')):
            for key in dictionary:
                if name == 'vb':
                    dictionary[key] = loaded_dictionary["{}_{}".format(name, key)][0]
                else:
                    dictionary[key] = loaded_dictionary["{}_{}".format(name, key)]
    else:
        if load_all_data_filepath is not None:
            logging.warning("Can't load all data from file '{}' since not a file. Reverting to individual shot loading method.".format(load_all_data_filepath))

        compare_index_range = (20000, 50000)

        for saturation in ['esat', 'isat']:
            shots = esat_shots if saturation == 'esat' else isat_shots
            for shot in tqdm.tqdm(shots, desc="Extracting points for {}.".format(saturation)):
                if load_data_file_func is not None and isfile(load_data_file_func(shot)):
                    te_data = Te_Probe3_Data(shot, load_filepath=load_data_file_func(shot))
                else:
                    te_data = Te_Probe3_Data(shot)

                try:
                    usable_mask = get_usable_mask([smooth_data(manual_get_data.v_s(te_data, i)) for i in TIP_NUMBERS], compare_index_range, val_bounds=(-1, +1))

                    if plot_shots:
                        plot_usable_data(shot, [manual_get_data.v_s(te_data, i) for i in TIP_NUMBERS], lambda d: get_usable_mask([smooth_data(d)], compare_index_range, val_bounds=(-1, +1)), separation=1, bool_factor=0.5)
                        plot_usable_data(shot, [manual_get_data.v_n(te_data, i) for i in TIP_NUMBERS], lambda d: get_usable_mask([smooth_data(d)], compare_index_range, val_bounds=(-1, +1)), separation=1, bool_factor=0.5)

                    vs_dict[saturation] = np.append(vs_dict[saturation], np.array([manual_get_data.v_s(te_data, i)[np.where(usable_mask)] for i in TIP_NUMBERS]), axis=1)
                    vn_dict[saturation] = np.append(vn_dict[saturation], np.array([manual_get_data.v_n(te_data, i)[np.where(usable_mask)] for i in TIP_NUMBERS]), axis=1)
                    all_vbias_curves = te_data.vbias_curves
                except:
                    logging.warning("An exception occured while loading data for shot #{} for type '{}'. Skipping shot.".format(shot, saturation))
                    continue

                # Smooth the vbias curve for the correct vbias.
                if saturation == 'esat':
                    curve_index = np.argmax(all_vbias_curves[:, 0])
                elif saturation == 'isat':
                    curve_index = np.argmin(all_vbias_curves[:, 0])
                smoothed_curve = savgol_filter(all_vbias_curves[curve_index], 201, 3)
                vb_dict[saturation] = np.append(vb_dict[saturation], smoothed_curve[np.where(usable_mask)])

                if save_data_file_func is not None:
                    te_data.save(save_data_file_func(shot))
        
    # Save all the data gotten from the shots after cleaning the data.
    if save_all_data_filepath is not None:
        logging.debug("Saving all data to '{}'.".format(save_all_data_filepath))
        save_dict = {}
        for dictionary, name in zip((vs_dict, vn_dict, vb_dict), ('vs', 'vn', 'vb')):
            for key in dictionary:
                save_dict["{}_{}".format(name, key)] = dictionary[key]
        
        save_dict["esat_shots"] = esat_shots
        save_dict["isat_shots"] = isat_shots
        
        savemat(save_all_data_filepath, save_dict)

    if downsample is not None:
        random.seed(seed)
        for saturation in ['esat', 'isat']:
            num_points = vb_dict[saturation].size
            # Create a sample of indeces to use.
            if isinstance(downsample, int):
                if downsample >= num_points:
                    sample = list(range(num_points))
                else:
                    sample = random.sample(range(num_points), downsample)
            elif 0 <= downsample <= 1:
                sample = random.sample(range(num_points), int(downsample * num_points))
            else:
                raise ValueError("Downsample `{}` is invalid. Must be `None`, an `int`, or a `float` between 0 and 1.")
            
            logging.debug("Downsampling {} data to {} / {} points ({}%).".format(saturation, len(sample), vb_dict[saturation].size, round(len(sample) * 100 / vb_dict[saturation].size, 1)))

            # Sample each array in the dictionaries.
            vs_dict[saturation] = vs_dict[saturation][:, sample]
            vn_dict[saturation] = vn_dict[saturation][:, sample]
            vb_dict[saturation] = vb_dict[saturation][sample]

    return vs_dict, vn_dict, vb_dict

def simple_optimize(esat_shots, isat_shots, plot_shots=False):
    vs_dict, vn_dict = extract_optimization_points(esat_shots, isat_shots, plot_shots)
    esat_vs_array = vs_dict['esat']
    isat_vs_array = vs_dict['isat']
    esat_vn_array = vn_dict['esat']
    isat_vn_array = vn_dict['isat']

    num_tips = esat_vs_array.shape[0]
    TIP_NUMBERS = range(1, num_tips + 1)

    # Get the resistances that we already know.
    rdiggnd_array, rdiv_array, rsense_array = get_known_resistances(esat_shots[0])

    # Create the functions we want to optimize.
    def v_p_func(r_wire_arr):
        # r_wire_arr is a 16 element 1D np.array.
        vp_array = rdiv_array[:, None] * esat_vs_array / rdiggnd_array[:, None] + r_wire_arr[:, None] * (1 + rdiv_array / rsense_array)[:, None] * (esat_vs_array - esat_vn_array) / rdiggnd_array[:, None]
        return -np.sum(np.fromiter((np.dot(vp1, vp2) for vp1, vp2 in combinations(vp_array, 2)), dtype=float))
    
    ip_no_area_array = ((rdiv_array + rsense_array) / (rsense_array * rdiggnd_array))[:, None] * (isat_vs_array - isat_vn_array)
    ip_dot_products = np.zeros((num_tips, num_tips))
    for i, j in combinations(range(num_tips), 2):
        ip_dot_products[i, j] = np.dot(ip_no_area_array[i], ip_no_area_array[j])

    def i_p_func(a_rel_array):
        # a_rel_array is a 16 element 1D np.array.
        a_rel_inv_multiplications = np.outer(1 / a_rel_array, 1 / a_rel_array)
        return -np.tensordot(ip_dot_products, a_rel_inv_multiplications, axes=((0,1),(0,1)))

    # Optimize the functions.
    logging.debug("Optimizing functions.")

    logging.debug("Optimizing esat Vp similarity.")
    r_wire_lims = (0, 30) # ohms
    r_wire_result = differential_evolution(v_p_func, [r_wire_lims for _ in TIP_NUMBERS])

    logging.debug("Optimizing isat Ip similarity.")
    a_rel_lims = (0.1, 5) # unitless
    a_rel_result = differential_evolution(i_p_func, [a_rel_lims for _ in TIP_NUMBERS])

    # Print and plot the results
    print("R_wire:", r_wire_result.x)
    print("A_rel:", a_rel_result.x)

    logging.debug("Plotting result.")    
    fig, axs = plt.subplots(1, 2)
    
    axs[0].set_title("R_wire")
    axs[0].scatter(TIP_NUMBERS, r_wire_result.x)

    axs[1].set_title("A_rel")
    axs[1].scatter(TIP_NUMBERS, a_rel_result.x)

    plt.show()


if __name__=="__main__":
    logging.basicConfig(level=logging.DEBUG)
    plt.set_loglevel(level='warning')

    # esat_shots = [57223]
    # esat_shots = list(range(57297, 57320)) + list(range(57321, 57350))
    esat_shots = list(range(58377, 58385))
    # esat_shots = [57300]
    isat_shots = list(range(57562, 57576)) + list(range(57578, 57589))
    # isat_shots = [57565]

    file_path = lambda shot: "../analysis_codes/data/te_probe3/{}.mat".format(shot)
    save_all_file_path = "../analysis_codes/data/te_probe3/all_data_no_4.mat"

    tips_to_use = [1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
    NUM_TIPS = len(tips_to_use)
    TIP_NUMBERS = np.array(tips_to_use)
    TIP_NUM_STRS = [str(i).zfill(2) for i in TIP_NUMBERS]

    vs_dict, vn_dict, v_bias = extract_optimization_points(
        esat_shots, isat_shots, load_data_file_func=file_path, save_data_file_func=file_path, 
        load_all_data_filepath=save_all_file_path, save_all_data_filepath=save_all_file_path,
        downsample=0.1, plot_shots=False,
    )
    r_diggnd_array, r_div_array, r_sense_array = get_known_resistances(esat_shots[0], save_data_file_func=file_path, load_data_file_func=file_path)
    logging.info("R_diggnd: {}".format(r_diggnd_array))
    logging.info("R_div: {}".format(r_div_array))
    logging.info("R_sense: {}".format(r_sense_array))

    res = optimize(
        vs_dict, vn_dict, v_bias, r_diggnd_array, r_div_array, r_sense_array, (0, 30), (0.1, 2), boundary_tip=9, alternate_vp=False,
        save_filepath="../analysis_codes/data/te_probe3/optimized_vals_no_4.mat", tips_to_use=tips_to_use
    )
    for val in res:
        print(val)
    
