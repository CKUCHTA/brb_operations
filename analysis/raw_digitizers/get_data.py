"""Get data from the raw digitizers from the MDSplus tree."""
from modules.generic_get_data import Get, Data, lazy_get
import logging
import matplotlib.pyplot as plt


class Trex_Cell_Data(Data):
    """
    Hold all the data for the designated trex cell in one class for easier access and reduced network connections.
    """
    def __init__(self, tree, *args, **kwargs):
        # The dictionary contains a recursive tree of dictionaries of cell #, card #, and channel #.
        self.data_dict = {}
        super().__init__(tree, [], [], *args, **kwargs)

    def raw(self, cell_number, card_number, channel_number, get_time=False):
        """
        Get the raw data from a specified channel.
        """
        try:
            logging.debug("Trying to load pre-loaded data for cell {}, card {}, and channel {}.".format(cell_number, card_number, channel_number))
            if not get_time:
                return self.data_dict[cell_number][card_number][channel_number]
            else:
                return self.data_dict[cell_number][card_number][channel_number], self.data_dict[cell_number][card_number]['{}_time'.format(channel_number)]
        except KeyError:
            logging.debug("Couldn't find data. Loading data from tree.")

        node_tag = Get("\\c{cell}_{card}_{channel}".format(cell=cell_number, card=card_number, channel=str(channel_number).zfill(2)))
        data = super().get(node_tag)
        
        # Create the dictionaries needed.
        if cell_number not in self.data_dict.keys():
            self.data_dict[cell_number] = {}
        if card_number not in self.data_dict[cell_number].keys():
            self.data_dict[cell_number][card_number] = {}

        self.data_dict[cell_number][card_number][channel_number] = data
        if not get_time:
            return data
        else:
            time_get_call = Get("DIM_OF( {} )".format(node_tag.call_string))
            time = super().get(time_get_call)
            self.data_dict[cell_number][card_number]['{}_time'.format(channel_number)] = time

            return data, time

    def plot_cards(self, cell_card_tuples, channel_range=range(1, 33), factor=1, plot_time=False):
        """
        Plot raw card data for multiple cards as vertical subplots in one window. One subplot per tuple.

        params:
        cell_card_tuples    = Iterable of 2 tuples of (cell number, card number).
        channel_range       = Which channels to plot. Default: Plot all channels on each card.
        factor              = Factor to multiply raw data by. Default: 1
        plot_time           = Boolean of whether to plot time. Default: False

        Returns
        -------
        fig : plt.Figure
        axes : np.array[plt.Axes]
        """
        fig, axes = plt.subplots(1, len(cell_card_tuples))
        # If we only have one axis then change it to an iterable so that the loop works correctly.
        if len(cell_card_tuples) == 1:
            axes = [axes]

        fig.suptitle("Shot #{}: Raw TREX Cell Data".format(self.shot_number))
        x_points = None
        for ax, (cell_number, card_number) in zip(axes, cell_card_tuples):
            ax.set_title("Cell #{}, Card #{}".format(cell_number, card_number))
            ax.set_yticks(channel_range)
            for channel in channel_range:
                ax.axhline(channel, color='grey', alpha=0.25)
                data = self.raw(cell_number, card_number, channel)

                if plot_time:
                    if x_points is None:
                        x_points = super().get(Get("dim_of(\\c{cell}_{card}_{channel})".format(cell=cell_number, card=card_number, channel=str(channel).zfill(2))))
                else:
                    x_points = range(len(data))

                y_points = factor * data + channel
                line = ax.plot(x_points, y_points)
                x_point_diff = x_points[-1] - x_points[0]
                ax.set_xlim(x_points[0] - 0.05 * x_point_diff, x_points[-1] + 0.05 * x_point_diff)
                yaxis_x_coordinate = ax.get_xlim()[0]

                # Draw a line from the tick to the data starting point.
                ax.plot([yaxis_x_coordinate, x_points[0]], [channel, y_points[0]], color=line[0].get_color(), alpha=0.5)
    
        return fig, axes

class ACQ196_Data(Data):
    """
    Hold all the data for ACQ196s in one class for easier access and reduced network connections.
    """
    def __init__(self, tree, *args, **kwargs):
        # The dictionary contains a recursive tree of dictionaries of digitizer number and channel.
        self.data_dict = {}
        super().__init__(tree, [], [], *args, **kwargs)

    def raw(self, dig_number, channel_number, get_time=False):
        """
        Get the raw data from a specified channel.

        Parameters
        ----------
        dig_number : int
            Number of the digitizer to get data from.
        channel_number : int
            Channel on digitizer to get data from.
        get_time : bool, default=False
            If `True`, also return the time array for the channel.

        Returns
        -------
        np.array
            1D array containing data from channel.
        """
        try:
            logging.debug("Trying to load pre-loaded data for 'A{}_DIRECT' and channel {}.".format(str(dig_number).zfill(3), channel_number))
            if not get_time:
                return self.data_dict[dig_number][channel_number]
            else:
                return self.data_dict[dig_number][channel_number], self.data_dict[dig_number]['{}_time'.format(channel_number)]
        except KeyError:
            logging.debug("Couldn't find data. Loading data from tree.")

        channel_str = str(channel_number).zfill(2)
        dig_str = str(dig_number).zfill(3)
        get_name = 'ACQ' + dig_str + '_' + channel_str
        node_tag = Get("\\top.raw.a{}_direct:ch_{}".format(dig_str, channel_str), get_name)
        data = super().get(node_tag)
        
        # Create the dictionaries needed.
        if dig_number not in self.data_dict.keys():
            self.data_dict[dig_number] = {}

        self.data_dict[dig_number][channel_number] = data

        if not get_time:
            return data
        else:
            time_get_call = Get("DIM_OF( {} )".format(node_tag.call_string), get_name + "_time")
            time = super().get(time_get_call)
            self.data_dict[dig_number]['{}_time'.format(channel_number)] = time

            return data, time

class ACQ2106_Data(Data):
    """
    Hold all the data for ACQ2106s in one class for easier access and reduced network connections.
    """
    def __init__(self, tree, *args, **kwargs):
        # The dictionary contains a recursive tree of dictionaries of digitizer number and channel.
        self.data_dict = {}
        super().__init__(tree, [], [], *args, **kwargs)

    def raw(self, dig_number, channel_number):
        """
        Get the raw data from a specified channel.

        Parameters
        ----------
        dig_number : int
            Number of the digitizer to get data from.
        channel_number : int
            Channel on digitizer to get data from.

        Returns
        -------
        np.array
            1D array containing data from channel.
        """
        digitizer_name = 'ACQ2106_{}'.format(str(dig_number).zfill(3))
        try:
            logging.debug("Trying to load pre-loaded data for '{}' and channel {}.".format(digitizer_name, channel_number))
            return self.data_dict[dig_number][channel_number]
        except KeyError:
            logging.debug("Couldn't find data. Loading data from tree.")

        # The 'ACQ2106_035' is different from the others in terms of indexing.
        channel_str = str(channel_number).zfill(2)
        get_name = digitizer_name + '_' + channel_str
        if dig_number == 35:
            node_tag = Get("\\vfluct_{}".format(channel_str), get_name)
        else:
            node_tag = Get("\\ta{}_{}".format(str(dig_number).zfill(3), channel_str), get_name)
        data = super().get(node_tag)
        
        # Create the dictionaries needed.
        if dig_number not in self.data_dict.keys():
            self.data_dict[dig_number] = {}

        self.data_dict[dig_number][channel_number] = data
        return data

