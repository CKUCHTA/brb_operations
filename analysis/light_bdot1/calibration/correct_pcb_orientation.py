"""Enforce constraints on dB / dt to adjust the orientation of each set of 4 pcbs."""
import numpy as np
from light_bdot1.get_data import Light_Bdot1_Data
from modules.generic_get_data import Port
from modules.bdot.interpolation import get_db_machine_coords
from scipy.integrate import simpson
from scipy.optimize import differential_evolution
from modules.coordinates import Position, Vector
import tqdm


def change_orientations(board_orientations, x_angle, y_angle, z_angle):
    """
    Parameters
    ----------
    board_orientations : np.array
        2D array of orientation vectors for all coils on a stick of shape `(NUM_COILS, 3)`.
    x_angle, y_angle, z_angle : float
        Roll, pitch, and yaw rotations of stick in radians.
    
    Returns
    -------
    rotated_orientations : np.array
        2D array of new orientation vectors after rotating.
    """
    x_rotation_matrix = np.array((
        (1, 0, 0),
        (0, np.cos(x_angle), -np.sin(x_angle)),
        (0, np.sin(x_angle), np.cos(x_angle)),
    ))
    y_rotation_matrix = np.array((
        (np.cos(y_angle), 0, np.sin(y_angle)),
        (0, 1, 0),
        (-np.sin(y_angle), 0, np.cos(y_angle)),
    ))
    z_rotation_matrix = np.array((
        (np.cos(z_angle), -np.sin(z_angle), 0),
        (np.sin(z_angle), np.cos(z_angle), 0),
        (0, 0, 1),
    ))

    return (z_rotation_matrix @ y_rotation_matrix @ x_rotation_matrix @ board_orientations.T).T

def cost_function(z_positions, b_values):
    """
    Parameters
    ----------
    z_positions : np.array[float]
        1D-array of z coordinates of shape `(NUM_POSITIONS)` associated with 
        each db_value. See notes.
    b_values : np.array[float]
        2D-array of B components in cylindrical coordinates, `(B_r, B_phi, B_z)`. 
        We use the total magnetic field as it is less prone to noise and time 
        delay issues. Shape of `(NUM_POSITIONS, 3)`.

    Returns
    -------
    total_cost : float

    Notes
    -----
    The `z_positions` array should be sorted and `z[i] == -z[-i - 1]`.
    """
    total_cost = 0

    # B_r should be anti-symmetric across z = 0.
    total_cost += np.sum(np.abs(b_values[:, 0] + b_values[::-1, 0]) / 2)
    # B_r should monotonically decrease as z increases.
    b_r_differences = b_values[1:, 0] - b_values[:-1, 0]
    total_cost += np.sum(np.where(b_r_differences > 0, b_r_differences, 0))

    # B_phi should be zero everywhere.
    total_cost += np.sum(np.abs(b_values[:, 1]))

    # B_z should be symmetric across z = 0.
    total_cost += np.sum(np.abs(b_values[:, 2] - b_values[::-1, 2]) / 2)
    # B_z should increase as z moves away from zero.
    b_z_differences = b_values[1:, 2] - b_values[:-1, 2]
    b_z_differences[:b_z_differences.size // 2] *= -1
    total_cost += np.sum(np.where(b_z_differences < 0, -b_z_differences, 0))

    return total_cost

def rotate_coil_sets(all_probe_orientations, coil_stick_sets, x_angles, y_angles, z_angles):
    """
    Parameters
    ----------
    all_probe_orientations : list[np.array[float]]
        List of `len(3)` of 2D-arrays of shape `(NUM_WORKING_COILS, 3)`. Each 
        set of 3 elements is a coil orientation in probe coordinates.
    coil_stick_sets : list[np.array[int]]
        List of 1D-arrays of shape `(NUM_WORKING_COILS,)`. Each value should be 
        in `range(1, NUM_STICKS + 1)` and signifies which stick the corresponding 
        coil in the `all_probe_orientations` array is on.
    x_angles, y_angles, z_angles : np.array[float]
        1D-arrays of shape `(NUM_STICKS,)` where each angle is how much to 
        rotate each PCB stick in the various probe coordinates. Angles in rad.

    Returns
    -------
    rotated_probe_orientations : list[np.array[float]]
    """
    rotated_probe_orientations = [np.zeros_like(orientations) for orientations in all_probe_orientations]
    # Assume that each pcb contains a db1, a db2, and 2 db3 (3a and 3b) coils. 
    # There are 4 pcbs per stick so we want to rotate each set of 4 pcbs.
    for i, (orientation_set, stick_set) in enumerate(zip(all_probe_orientations, coil_stick_sets)):
        for stick_number in range(1, NUM_STICKS + 1):
            coil_indeces = np.nonzero(stick_set == stick_number)
            rotated_probe_orientations[i][coil_indeces] = change_orientations(
                orientation_set[coil_indeces], x_angles[stick_number - 1], y_angles[stick_number - 1], z_angles[stick_number - 1]
            )

    return rotated_probe_orientations

def get_rotated_b(shot_b_values, shot_positions, shot_orientations, shot_stick_sets, port, 
                  interp_positions, x_angles, y_angles, z_angles):
    """
    Parameters
    ----------
    shot_b_values : list[np.array[float]]
        List of 2D-arrays of measured B values. These are the integrated dB / dt 
        values. Each array in the list is for a different coil set and the 
        arrays are of shape `(NUM_COILS, 1)`.
    shot_positions, shot_orientations : list[np.array[float]]
        List of 2D-arrays of coil positions and orientations in cylindrical 
        coordinates of the ball, `(r, phi, z)`. The positions + orientations 
        should not be adjusted for the TF coil or the insert. Each array should 
        have shape `(NUM_COILS, 3)`.
    shot_stick_sets : list[np.array[int]]
        List of 1D-arrays of shape `(NUM_WORKING_COILS,)`. Each value should be 
        in `range(1, NUM_STICKS + 1)` and signifies which stick the corresponding 
        coil in the `all_probe_orientations` array is on.
    port : Port
        Port object for the lightsaber data.
    interp_positions : np.array[float]
        2D-array of shape `(NUM_POINTS, 3)` of coordinates in cylindrical 
        coordinates.
    x_angles, y_angles, z_angles : np.array[float]
        1D-arrays of shape `(NUM_STICKS,)` where each angle is how much to 
        rotate each PCB stick in the various probe coordinates. Angles in rad.

    Returns
    -------
    interpolation_positions : np.array[float]
        2D-array of cylindrical positions,`(r, phi, z)`, where `phi` is in 
        radians. Shape of `(NUM_POSITIONS, 3)`. If the `interpolation_positions`
        were passed then this is the positions sorted in the direction of the
        probe.
    interpolation_b_values : np.array[float]
        2D-array of interpolated B components in cylindrical coordinates, 
        `(B_r, B_phi, B_z)`, for the last time entry. Shape of `(NUM_POSITIONS, 3)`.
    """
    # Rotate the orientations according to the passed angles.
    rotated_shot_orientations = rotate_coil_sets(shot_orientations, shot_stick_sets, x_angles, y_angles, z_angles)

    # Let's change the orientation of each coil to be in the machine cylindrical coordinates.
    # We also change the position depending on how the BRB is setup.
    for dir_positions, dir_orientations in zip(shot_positions, rotated_shot_orientations):
        for i in range(dir_orientations.shape[0]):
            position = Position()
            position.cylindrical = dir_positions[i]

            orientation = Vector(position)
            orientation.from_port(
                port.long_rad, port.lat_rad, port.alpha_rad, port.beta_rad, port.gamma_rad,
                dir_orientations[i, 0], dir_orientations[i, 1], dir_orientations[i, 2], port.clocking_rad
            )
            
            # We then adjust for the lowered TF coil
            orientation.position.to_tf_centered_coordinates()

            # Rewrite the position and orientation in the new cylindrical coordinates.
            dir_positions[i] = orientation.position.cylindrical
            dir_orientations[i] = orientation.cylindrical

            # Also renormalize the orientations. They can become slightly off.
            dir_orientations[i] /= np.linalg.norm(dir_orientations[i])

    # Also change the interpolation positions.
    position_obj = Position()
    position_obj.cylindrical = interp_positions
    position_obj.to_tf_centered_coordinates()
    new_interp_positions = position_obj.cylindrical

    positions, b_vals = get_db_machine_coords(shot_b_values, shot_positions, rotated_shot_orientations, new_interp_positions)
    b_vals = b_vals[:, :, 0]
    return positions, b_vals

def optimization_function(all_angles, all_shot_b_values, all_shot_positions, all_shot_orientations, all_shot_stick_sets, all_ports, all_interp_positions, only_x):
    """
    Parameters
    ----------
    all_angles : np.array[float]
        1D-array of angles of shape `(3 * NUM_STICKS,)`. The first `NUM_STICKS` 
        entries should be for the x rotation and so on. If `only_x` then of
        shape `(NUM_STICKS,)`.
    all_shot_db_values : list[list[np.array[float]]]
        List of lists of 2D-arrays of measured dB values. Each sublist is for a 
        specific shot. Each array in the sublist is for a different coil set 
        and the arrays are of shape `(NUM_COILS, 1)`.
    all_shot_positions, all_shot_orientations : list[list[np.array[float]]]
        List of lists of 2D-arrays of coil positions and orientations in 
        cylindrical coordinates of the ball, `(r, phi, z)`. The positions + 
        orientations should not be adjusted for the TF coil or the insert. Each 
        array should have shape `(NUM_COILS, 3)`.
    all_shot_stick_sets : list[list[np.array[int]]]
        List of lists of 1D-arrays of shape `(NUM_WORKING_COILS,)`. Each value 
        in the array should be in `range(1, NUM_STICKS + 1)` and signifies 
        which stick the corresponding coil in the `all_probe_orientations` 
        array is on.
    all_ports : list[Port]
        List of `Port` object for the lightsaber data.
    all_interp_positions : list[np.array[float]]
        List of 2D-arrays of shape `(NUM_POINTS, 3)` of coordinates in 
        cylindrical coordinates.
    only_x : bool
        Whether to only try to optimize the rotation around the x axis.
    """
    total_cost = 0
    for i in range(len(all_shot_b_values)):
        if only_x:
            positions, b_values = get_rotated_b(all_shot_b_values[i], all_shot_positions[i], all_shot_orientations[i],
                                            all_shot_stick_sets[i], all_ports[i], all_interp_positions[i],
                                            all_angles, np.zeros_like(all_angles), np.zeros_like(all_angles))
        else:
            positions, b_values = get_rotated_b(all_shot_b_values[i], all_shot_positions[i], all_shot_orientations[i],
                                            all_shot_stick_sets[i], all_ports[i], all_interp_positions[i],
                                            all_angles[:NUM_STICKS], all_angles[NUM_STICKS:2 * NUM_STICKS], all_angles[2 * NUM_STICKS:3 * NUM_STICKS])
        total_cost += cost_function(positions[:, 2], b_values)
    
    global NUM_EVALS
    NUM_EVALS += 1
    print("{}: Tried the following angles (deg): {}\nTotal cost: {}".format(NUM_EVALS, np.rad2deg(all_angles), total_cost))
        
    return total_cost


def get_args(light_data_objects : list[Light_Bdot1_Data], *args, baseline_time_indeces=slice(10)):
    all_shot_b_values = []
    all_shot_positions = []
    all_shot_orientations = []
    all_shot_stick_sets = []
    all_ports = []
    all_interp_positions = []

    for light_data in tqdm.tqdm(light_data_objects, desc="Loading shot data"):
        # Get the data from the working coils for each coil direction.
        db1_indeces = np.nonzero(light_data.db_1_working)
        db1_values = light_data.db_1[db1_indeces]
        db1_positions = light_data.db_1_position[db1_indeces]
        db1_orientations = light_data.db_1_orientation[db1_indeces]
        db1_sticks = np.repeat(np.arange(1, NUM_STICKS + 1), 4)[db1_indeces]

        db2_indeces = np.nonzero(light_data.db_2_working)
        db2_values = light_data.db_2[db2_indeces]
        db2_positions = light_data.db_2_position[db2_indeces]
        db2_orientations = light_data.db_2_orientation[db2_indeces]
        db2_sticks = np.repeat(np.arange(1, NUM_STICKS + 1), 4)[db2_indeces]

        db3a_indeces = np.nonzero(light_data.db_3a_working)
        db3a_values = light_data.db_3a[db3a_indeces]
        db3a_positions = light_data.db_3a_position[db3a_indeces]
        db3a_orientations = light_data.db_3a_orientation[db3a_indeces]
        db3a_sticks = np.repeat(np.arange(1, NUM_STICKS + 1), 4)[db3a_indeces]

        db3b_indeces = np.nonzero(light_data.db_3b_working)
        db3b_values = light_data.db_3b[db3b_indeces]
        db3b_positions = light_data.db_3b_position[db3b_indeces]
        db3b_orientations = light_data.db_3b_orientation[db3b_indeces]
        db3b_sticks = np.repeat(np.arange(1, NUM_STICKS + 1), 4)[db3b_indeces]

        # Now combine the 3 direction.
        db3_values = np.concatenate((db3a_values, db3b_values), axis=0)
        db3_positions = np.concatenate((db3a_positions, db3b_positions), axis=0)
        db3_orientations = np.concatenate((db3a_orientations, db3b_orientations), axis=0)
        db3_sticks = np.concatenate((db3a_sticks, db3b_sticks), axis=0)

        all_values = [db1_values, db2_values, db3_values]
        all_positions = [db1_positions, db2_positions, db3_positions]
        all_orientations = [db1_orientations, db2_orientations, db3_orientations]
        all_stick_sets = [db1_sticks, db2_sticks, db3_sticks]

        # For each set of coils we need to remove the baseline and get the B value.
        all_b_values = []
        for dir_values in all_values:
            dir_values -= np.mean(dir_values[:, baseline_time_indeces], axis=1)[:, None]
            all_b_values.append(simpson(dir_values, even='last', axis=-1)[:, np.newaxis])

        all_shot_b_values.append(all_b_values)
        all_shot_positions.append(all_positions)
        all_shot_orientations.append(all_orientations)
        all_shot_stick_sets.append(all_stick_sets)
        all_ports.append(light_data.port)
        # For the interpolation positions, use the average r and phi positions 
        # and space the points along the z direction.
        all_interp_positions.append(
            np.vstack((
                np.mean([np.mean(pos[:, 0]) for pos in all_positions]) * np.ones(NUM_STICKS * 4), # r
                np.mean([np.mean(pos[:, 1]) for pos in all_positions]) * np.ones(NUM_STICKS * 4), # phi
                np.linspace(-np.max([np.max(np.abs(pos[:, 2])) for pos in all_positions]), 
                            np.max([np.max(np.abs(pos[:, 2])) for pos in all_positions]), num=NUM_STICKS * 4) # z
            )).T
        )
        

    return all_shot_b_values, all_shot_positions, all_shot_orientations, all_shot_stick_sets, all_ports, all_interp_positions, *args


def optimize_rotations(vacuum_shots, only_x=False):
    """
    Parameters
    ----------
    vacuum_shots : list[int]
        List of shot numbers that are vacuum shots and the lightsaber probe is 
        coming in along the insert.
    only_x : bool
        Whether to only try to optimize the rotation around the x axis.
    """
    generic_filepath = './notebooks/data/light_bdot1/{}.mat'
    light_data_objects = [Light_Bdot1_Data(shot, load_filepath=generic_filepath.format(shot), time_index_range=(65000, 65150)) for shot in vacuum_shots]
    x_angle_bounds = [(-np.pi / 6, np.pi / 6)] * NUM_STICKS
    y_angle_bounds = [(-np.pi / 10, np.pi / 10)] * NUM_STICKS
    z_angle_bounds = [(-np.pi / 10, np.pi / 10)] * NUM_STICKS

    if only_x:
        all_angle_bounds = x_angle_bounds
        x0 = np.zeros(NUM_STICKS)
    else:
        all_angle_bounds = x_angle_bounds + y_angle_bounds + z_angle_bounds
        x0 = np.zeros(3 * NUM_STICKS)

    function_args = get_args(light_data_objects, only_x)
    for light_data in light_data_objects:
        light_data.save(generic_filepath.format(light_data.shot_number))

    result = differential_evolution(optimization_function, all_angle_bounds, args=function_args, x0=x0)

    # Print out the optimized orientations.
    if not result.success:
        print("Because of '{}' the rotations couldn't be optimized. Here they are anyhow!".format(result.message))

    for i, dir in enumerate(['x', 'y', 'z']):
        print("{} angles (deg):\t{}".format(dir, np.rad2deg(result.x[i * NUM_STICKS:(i + 1) * NUM_STICKS])))


if __name__ == "__main__":
    NUM_STICKS = 8
    NUM_EVALS = 0
    vac_shots = [60099, 60106, 60163, 60179, 60186, 60195, 60202, 60209, 60218, 60225]
    optimize_rotations(vac_shots, only_x=True)
