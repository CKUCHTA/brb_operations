"""Enforce constraints on dB / dt to adjust the orientation of each set of 4 pcbs."""
import numpy as np
import logging
from light_bdot1.get_data import Light_Bdot1_Data
from helmholtz.get_data import Helmholtz_Data
from pa_probe1.get_data import PA_Probe1_Data
from tf_coil.get_data import TF_Coil_Data
from modules.bdot.interpolation import fast_db_machine_coords
from scipy.optimize import differential_evolution
from modules.coordinates import Position, Vector
import tqdm
import matplotlib.pyplot as plt
from scipy.integrate import cumulative_trapezoid
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from multiprocessing import Pool
import time


NUM_STICKS = 8


def change_orientations(board_orientations, x_angle, y_angle, z_angle):
    """
    Parameters
    ----------
    board_orientations : np.array[float]
        2D array of orientation vectors for all coils on a stick of shape `(NUM_COILS, 3)`.
    x_angle, y_angle, z_angle : np.array[float]
        1D-arrays of shape `(S,)` of cartesian rotations in radians.
    
    Returns
    -------
    rotated_orientations : np.array[float]
        3D-array of new orientation vectors after rotating of shape `(NUM_COILS, 3, S)`.
    """
    x_rotation_matrix = np.zeros((3, 3, x_angle.size))
    x_rotation_matrix[0, 0] = 1
    x_rotation_matrix[1:, 1:] = np.array((
        (np.cos(x_angle), -np.sin(x_angle)),
        (np.sin(x_angle), np.cos(x_angle)),
    ))

    y_rotation_matrix = np.zeros((3, 3, y_angle.size))
    y_rotation_matrix[1, 1] = 1
    y_rotation_matrix[::2, ::2] = np.array((
        (np.cos(y_angle), np.sin(y_angle)),
        (-np.sin(y_angle), np.cos(y_angle)),
    ))

    z_rotation_matrix = np.zeros((3, 3, z_angle.size))
    z_rotation_matrix[2, 2] = 1
    z_rotation_matrix[:2, :2] = np.array((
        (np.cos(z_angle), -np.sin(z_angle)),
        (np.sin(z_angle), np.cos(z_angle)),
    ))

    return np.einsum(z_rotation_matrix, [1, 2, 0], y_rotation_matrix, [2, 3, 0], x_rotation_matrix, [3, 4, 0], board_orientations.T, [4, 5], [5, 1, 0])

def cost_function(db_values, interp_positions, rotation_angle):
    """
    Parameters
    ----------
    db_values : np.array[float]
        4D-array of dB components in cylindrical coordinates, `(dB_r, dB_phi, dB_z)`. 
        Shape of `(NUM_POSITIONS, 3, NUM_TIMES, S)`.
    interp_positions : np.array[float]
        2D-array of shape `(NUM_POINTS, 3)` of coordinates in cartesian 
        coordinates corrected for TF and insert.
    rotation_angle : np.array[float] or None
        1D-array of shape `(S,)` where each angle is the correction
        to this shot set in terms of how much the probe was actually rotated. If 
        `None` then the probe was not rotated.

    Returns
    -------
    total_cost : np.array[float]
        1D-array of costs of shape `(S,)`.

    Notes
    -----
    The `db_values` should be sorted in position such that `z` increases as the 
    position index increases and the interpolated `z` positions should be symmetric.
    """
    total_cost = 0

    if rotation_angle is None:
        # dB_r should be anti-symmetric across z = 0.
        total_cost += np.sum(np.abs(db_values[:, 0] + db_values[::-1, 0]) / 2, axis=(0, 1))
        # dB_r should monotonically decrease as z increases.
        db_r_differences = db_values[1:, 0] - db_values[:-1, 0]
        total_cost += np.sum(np.where(db_r_differences > 0, db_r_differences, 0), axis=(0, 1))
        # # The second derivative of dB_r should be positive for -z and negative for +z.
        # db_r_second_deriv = db_values[2:, 0] - 2 * db_values[1:-1, 0] + db_values[:-2, 0]
        # db_r_second_deriv[:db_r_second_deriv.shape[0] // 2] *= -1
        # total_cost += np.sum(np.where(db_r_second_deriv > 0, db_r_second_deriv, 0), axis=(0, 1))
        
        # dB_phi should be zero everywhere.
        total_cost += np.sum(np.abs(db_values[:, 1]), axis=(0, 1))

        # dB_z should be symmetric across z = 0.
        total_cost += np.sum(np.abs(db_values[:, 2] - db_values[::-1, 2]) / 2, axis=(0, 1))
        # dB_z should increase as z moves away from zero.
        db_z_differences = db_values[1:, 2] - db_values[:-1, 2]
        db_z_differences[:db_z_differences.shape[0] // 2] *= -1
        total_cost += np.sum(np.where(db_z_differences < 0, -db_z_differences, 0), axis=(0, 1))
        # # The second derivative of dB_z should be positive.
        # db_z_second_deriv = db_values[2:, 2] - 2 * db_values[1:-1, 2] + db_values[:-2, 2]
        # total_cost += np.sum(np.where(db_z_second_deriv < 0, -db_z_second_deriv, 0), axis=(0, 1))
    else:
        # This is the approximate radius of the drive cylinder to we'll only 
        # calculate errors for points > 3 cm away from the drive cylinder.
        drive_cylinder_radius = 0.62
        radius_err = 0.04
        inside_drive_indeces = np.nonzero(np.abs(interp_positions[:, 1]) < drive_cylinder_radius - radius_err)
        outside_drive_indeces = np.nonzero(np.abs(interp_positions[:, 1]) > drive_cylinder_radius + radius_err)
        usable_indeces = np.nonzero(np.logical_or(np.abs(interp_positions[:, 1]) < drive_cylinder_radius - radius_err, np.abs(interp_positions[:, 1]) > drive_cylinder_radius + radius_err))

        # dB_r should be symmetric across y = 0 but because of the probe not being exactly symmetric across y=0 we will assign a lower weight.
        total_cost += 0.1 * np.sum(np.abs(db_values[usable_indeces][:, 0] - db_values[usable_indeces][::-1, 0]) / 2, axis=(0, 1))

        # dB_r should decrease as y moves away from zero inside the cylinder and increase outside the cylinder.
        db_r_differences = db_values[inside_drive_indeces][1:, 0] - db_values[inside_drive_indeces][:-1, 0]
        db_r_differences[:db_r_differences.shape[0] // 2] *= -1
        total_cost += np.sum(np.where(db_r_differences > 0, db_r_differences, 0), axis=(0, 1))
        db_r_differences = db_values[outside_drive_indeces][1:, 0] - db_values[outside_drive_indeces][:-1, 0]
        # Remove the center point as it crosses the cylinder.
        db_r_differences[db_r_differences.shape[0] // 2] = 0
        db_r_differences[:db_r_differences.shape[0] // 2] *= -1
        total_cost += np.sum(np.where(db_r_differences < 0, -db_r_differences, 0), axis=(0, 1))

        # dB_phi should be zero everywhere.
        total_cost += np.sum(np.abs(db_values[usable_indeces][:, 1]), axis=(0, 1))

        # dB_z should be symmetric across y = 0 but because of the probe not being exactly symmetric across y=0 we will assign a lower weight.
        total_cost += 0.1 * np.sum(np.abs(db_values[usable_indeces][:, 2] - db_values[usable_indeces][::-1, 2]) / 2, axis=(0, 1))
        # dB_z should decrease as y moves away from zero inside and outside the cylinder.
        db_z_differences = db_values[inside_drive_indeces][1:, 2] - db_values[inside_drive_indeces][:-1, 2]
        db_z_differences[:db_z_differences.shape[0] // 2] *= -1
        total_cost += np.sum(np.where(db_z_differences > 0, db_z_differences, 0), axis=(0, 1))
        db_z_differences = db_values[outside_drive_indeces][1:, 2] - db_values[outside_drive_indeces][:-1, 2]
        db_z_differences[db_z_differences.shape[0] // 2] = 0
        db_z_differences[:db_z_differences.shape[0] // 2] *= -1
        total_cost += np.sum(np.where(db_z_differences > 0, db_z_differences, 0), axis=(0, 1))

    return total_cost

def rotate_coil_sets(all_probe_orientations, stick_assignments, x_angles, y_angles, z_angles, rotation_angle):
    """
    Parameters
    ----------
    all_probe_orientations : list[np.array[float]]
        List of `len(3)` of 2D-arrays of shape `(NUM_WORKING_COILS, 3)`. Each 
        set of 3 elements is a coil orientation in machine cartesian coordinates.
    stick_assignments : list[np.array[int]]
        List of 1D-arrays of shape `(NUM_WORKING_COILS,)`. Each value should be 
        in `range(1, NUM_STICKS + 1)` and signifies which stick the corresponding 
        coil in the `all_probe_orientations` array is on.
    x_angles, y_angles, z_angles : np.array[float]
        2D-arrays of shape `(NUM_STICKS, S)` where each angle is how much to 
        rotate each PCB stick in the various machine cartesian coordinates. Angles in rad.
    rotation_angle : np.array[float] or None
        1D-array of shape `(S,)` where each angle is the correction
        to this shot set in terms of how much the probe was actually rotated. If 
        `None` then the probe was not rotated.

    Returns
    -------
    rotated_probe_orientations : list[np.array[float]]
        List of 3D-arrays of shape `(NUM_WORKING_COILS, 3, S)`.
    """
    rotated_probe_orientations = [np.zeros((orientations.shape[0], orientations.shape[1], x_angles.shape[1])) for orientations in all_probe_orientations]
    # Assume that each pcb contains a db1, a db2, and 2 db3 (3a and 3b) coils. 
    # There are 4 pcbs per stick so we want to rotate each set of 4 pcbs.
    for i, (orientation_set, stick_set) in enumerate(zip(all_probe_orientations, stick_assignments)):
        for stick_number in range(1, NUM_STICKS + 1):
            coil_indeces = np.nonzero(stick_set == stick_number)
            
            if rotation_angle is None:
                rotated_probe_orientations[i][coil_indeces] = change_orientations(
                    orientation_set[coil_indeces], x_angles[stick_number - 1], y_angles[stick_number - 1], z_angles[stick_number - 1]
                )
            else:
                rotated_probe_orientations[i][coil_indeces] = change_orientations(
                    orientation_set[coil_indeces], -x_angles[stick_number - 1], z_angles[stick_number - 1] + rotation_angle, y_angles[stick_number - 1]
                )

    return rotated_probe_orientations

def get_rotated_db(shot_db_values, shot_positions, shot_orientations, stick_assignments, 
                  interp_positions, x_angles, y_angles, z_angles, rotation_angle, plot_rotations=False):
    """
    Parameters
    ----------
    shot_db_values : list[np.array[float]]
        List of 2D-arrays of measured dB values. Each array in the list is for a different coil set and the 
        arrays are of shape `(NUM_COILS, NUM_TIMES)`.
    shot_positions, shot_orientations : list[np.array[float]]
        List of 2D-arrays of coil positions and orientations in 
        cartesian coordinates of the ball, `(x, y, z)`. The positions + 
        orientations should be adjusted for the TF coil or the insert. Each 
        array should have shape `(NUM_WORKING_COILS, 3)`.
    stick_assignments : list[np.array[int]]
        List of 1D-arrays of shape `(NUM_WORKING_COILS,)`. Each value should be 
        in `range(1, NUM_STICKS + 1)` and signifies which stick the corresponding 
        coil in the `all_probe_orientations` array is on.
    interp_positions : np.array[float]
        2D-array of shape `(NUM_POINTS, 3)` of coordinates in cartesian 
        coordinates.
    x_angles, y_angles, z_angles : np.array[float]
        2D-arrays of shape `(NUM_STICKS, S)` where each angle is how much to 
        rotate each PCB stick in the various BRB cartesian coordinates. Angles in rad.
    rotation_angle : np.array[float] or None
        1D-array of shape `(S,)` where each angle is the correction
        to this shot set in terms of how much the probe was actually rotated. If 
        `None` then don't do an extra probe rotation.
    plot_rotations : bool, default=False
        Whether to plot the db values for the rotated coils.

    Returns
    -------
    interpolation_db_values : np.array[float]
        4D-array of interpolated dB components in cylindrical coordinates, 
        `(dB_r, dB_phi, dB_z)`, for the last time entry. Shape of `(NUM_POSITIONS, 3, NUM_TIMES, S)`.
    interp_positions : np.array[float]
        2D-array of shape `(NUM_POINTS, 3)` of coordinates in cartesian 
        coordinates corrected for TF and insert.
    """
    # Rotate the orientations according to the passed angles.
    rotated_shot_orientations = rotate_coil_sets(shot_orientations, stick_assignments, x_angles, y_angles, z_angles, rotation_angle)

    db_vals = fast_db_machine_coords(shot_db_values, shot_positions, rotated_shot_orientations, interp_positions)

    if plot_rotations:
        for trial_index in tqdm.tqdm(range(x_angles.shape[1])):
            fig, axs = plt.subplots(1, 3)

            directions = [r'r', r'\phi', r'z']

            if rotation_angle is None:
                for i, direction in enumerate(directions):
                    db_vals_to_plot = db_vals[:, i, :, trial_index] 
                    upper_bound = np.max(np.abs(db_vals_to_plot))
                    cmesh = axs[i].pcolormesh(np.arange(db_vals.shape[2]), interp_positions[:, 2], db_vals_to_plot, cmap='seismic', vmin=-upper_bound, vmax=upper_bound)
                    axs[i].set_ylabel('z')
                    
                    for j in range(1, NUM_STICKS):
                        point_index = int(j * interp_positions.shape[0] / NUM_STICKS)
                        axs[i].axhline((interp_positions[point_index, 2] + interp_positions[point_index - 1, 2]) / 2, color='black', linestyle='dashed')

                    # for coil3_position in shot_positions[2]:
                    #     axs[i].axhline(coil3_position[2], color='grey')

                    axs[i].set_title(r'$\partial_t B_{{{}}}$'.format(direction))

                    fig.colorbar(cmesh, ax=axs[i])
                
                fig.suptitle("x angles: {}\ny angles: {}\nz angles: {}".format(
                    np.round(np.rad2deg(x_angles[:, trial_index])), np.round(np.rad2deg(y_angles[:, trial_index])), np.round(np.rad2deg(z_angles[:, trial_index]))
                ))
            else:
                for i, direction in enumerate(directions):
                    db_vals_to_plot = db_vals[:, i, :, trial_index] 
                    upper_bound = np.max(np.abs(db_vals_to_plot))
                    cmesh = axs[i].pcolormesh(np.arange(db_vals.shape[2]), interp_positions[:, 1], db_vals_to_plot, cmap='seismic', vmin=-upper_bound, vmax=upper_bound)
                    axs[i].set_ylabel('y')
                    
                    for j in range(1, NUM_STICKS):
                        point_index = int(j * interp_positions.shape[0] / NUM_STICKS)
                        axs[i].axhline((interp_positions[point_index, 1] + interp_positions[point_index - 1, 1]) / 2, color='black', linestyle='dashed')

                    # for coil3_position in shot_positions[2]:
                    #     axs[i].axhline(coil3_position[1], color='grey')

                    axs[i].set_title(r'$\partial_t B_{{{}}}$'.format(direction))

                    fig.colorbar(cmesh, ax=axs[i])

                fig.suptitle("x angles: {}\ny angles: {}\nz angles: {}\nrotation angle: {}".format(
                    np.round(np.rad2deg(x_angles[:, trial_index])), np.round(np.rad2deg(y_angles[:, trial_index])), np.round(np.rad2deg(z_angles[:, trial_index])), np.round(np.rad2deg(rotation_angle[trial_index]))
                ))
            fig.canvas.manager.full_screen_toggle()

            if (trial_index + 1) % 10 == 0:
                plt.show()

    return db_vals, interp_positions

def _full_cost(*args, **kwargs):
    # args[-1] contains the rotation angle of the probe. This changes how the cost is calculated if it is not `None`.
    return cost_function(*get_rotated_db(*args, **kwargs), args[-1])

def optimization_function(all_angles, all_shot_db_values, all_shot_positions, all_shot_orientations, all_shot_stick_assignments, all_interp_positions, rotated_shot_set_indeces, only_z, process_pool):
    """
    Parameters
    ----------
    all_angles : np.array[float]
        2D-array of angles of shape `(3 * NUM_STICKS + NUM_SHOT_SETS, S)`. The 
        first `NUM_STICKS` entries should be for the x rotation and so on. The 
        last `NUM_SHOT_SETS` should be the additional rotation of the probe for 
        that specific shot set. If `only_x` then of shape `(NUM_STICKS + NUM_SHOT_SETS, S)`.
    all_shot_db_values : list[list[np.array[float]]]
        List of lists of 2D-arrays of measured dB values. Each sublist is for a 
        specific shot. Each array in the sublist is for a different coil set 
        and the arrays are of shape `(NUM_WORKING_COILS, NUM_TIMES)`.
    all_shot_positions, all_shot_orientations : list[list[np.array[float]]]
        List of lists of 2D-arrays of coil positions and orientations in 
        cartesian coordinates of the ball, `(x, y, z)`. The positions + 
        orientations should be adjusted for the TF coil or the insert. Each 
        array should have shape `(NUM_WORKING_COILS, 3)`.
    all_shot_stick_assignments : list[list[np.array[int]]]
        List of lists of 1D-arrays of shape `(NUM_WORKING_COILS,)`. Each value 
        in the array should be in `range(1, NUM_STICKS + 1)` and signifies 
        which stick the corresponding coil in the `all_probe_orientations` 
        array is on. Also, `len(stick_assignments) = 3`, one for each coil set.
    all_interp_positions : np.array[float]
        3D-array of shape `(NUM_SHOTS, NUM_POINTS, 3)` of coordinates in 
        cartesian coordinates corrected for TF and insert.
    rotated_shot_set_indeces : list[int]
        List of indeces. The index points to which angle of the `NUM_SHOT_SETS` 
        angles to use.
    only_z : bool
        Whether to only try to optimize the rotation around the z axis of the machine.
    process_pool : Pool
        Pool of processes to use for calculations.
    """
    angles_1D = (len(all_angles.shape) == 1)
    if angles_1D:
        all_angles = all_angles[:, np.newaxis]

    if only_z:
        x_angles = np.zeros_like(all_angles)
        y_angles = np.zeros_like(all_angles)
        z_angles = all_angles[:NUM_STICKS]
        rotation_angles = all_angles[NUM_STICKS:]
    else:
        x_angles = all_angles[:NUM_STICKS]
        y_angles = all_angles[NUM_STICKS:2 * NUM_STICKS]
        z_angles = all_angles[2 * NUM_STICKS:3 * NUM_STICKS]
        rotation_angles = all_angles[3 * NUM_STICKS:]

    get_rotation = lambda index: None if index < len(all_shot_db_values) - len(rotated_shot_set_indeces) else rotation_angles[rotated_shot_set_indeces[index - len(all_shot_db_values) + len(rotated_shot_set_indeces)]]

    # TODO: Change this so we don't have to loop and can do it in a single call.
    start = time.time()
    if all_angles.shape[1] == 1:
        total_cost = 0
        for i in range(len(all_shot_db_values)):
            db_values, interp_positions = get_rotated_db(all_shot_db_values[i], all_shot_positions[i], all_shot_orientations[i],
                                                         all_shot_stick_assignments[i], all_interp_positions[i],
                                                         x_angles, y_angles, z_angles, get_rotation(i), plot_rotations=False)
            plt.show()
            total_cost += cost_function(db_values, interp_positions, get_rotation(i))
    else:
        all_costs = process_pool.starmap(_full_cost, 
            ((all_shot_db_values[i], all_shot_positions[i], all_shot_orientations[i],
            all_shot_stick_assignments[i], all_interp_positions[i], x_angles, y_angles, z_angles, get_rotation(i))
            for i in range(len(all_shot_db_values)))
        )
        total_cost = sum(all_costs)
    
    global NUM_EVALS
    NUM_EVALS += 1
    print("Evaluation #{}: {:.3f} s    ({} trial{})".format(NUM_EVALS, time.time() - start, total_cost.size, '' if total_cost.size == 1 else 's'))
    # print("{}: Tried the following angles (deg): {}\nTotal cost: {}".format(NUM_EVALS, np.rad2deg(all_angles), total_cost))
    
    if angles_1D:
        return total_cost[0]
    else:
        return total_cost
    
def _rotated_shot_positions_orientations(light_data : Light_Bdot1_Data, rotation_angle : float):
    """
    Get the probes positions and orientations for shots `[60421, ..., 60428]`.

    Parameters
    ----------
    light_data : Light_Bdot1_Data
        Data object for a shot in the specified range.
    rotation_angle : float
        Angle of the probe in radians where `0` means coil 2 is measuring `Bz`, 
        `pi / 2` means coil 1 is measuring `-Bz`, and so on.

    Returns
    -------
    all_positions, all_orientations : list[np.array[float]]
        3 element list where each array is for a different coil type. Each array 
        is of shape `(NUM_WORKING_COILS, 3)` and is in cartesian coordinates.
    """
    if light_data.shot_number not in list(range(60421, 60429)):
        raise ValueError("Passed lightsaber probe data is not for a shot in [60421, ..., 60428].")
    
    # Let's first get the positions of each of the probes. We'll put them along 
    # the `z=0` plane and `phi = np.pi / 2` or `phi = 3 * np.pi / 2`.
    db1_indeces = np.nonzero(light_data.db_1_working)
    old_db1_positions = light_data.db_1_position[db1_indeces]
    old_db1_orientations = light_data.db_1_orientation[db1_indeces]
    db1_positions = np.zeros_like(old_db1_positions)
    db1_orientations = np.zeros_like(old_db1_orientations)

    db2_indeces = np.nonzero(light_data.db_2_working)
    old_db2_positions = light_data.db_2_position[db2_indeces]
    old_db2_orientations = light_data.db_2_orientation[db2_indeces]
    db2_positions = np.zeros_like(old_db2_positions)
    db2_orientations = np.zeros_like(old_db2_orientations)

    db3a_indeces = np.nonzero(light_data.db_3a_working)
    old_db3a_positions = light_data.db_3a_position[db3a_indeces]
    old_db3a_orientations = light_data.db_3a_orientation[db3a_indeces]
    db3a_positions = np.zeros_like(old_db3a_positions)
    db3a_orientations = np.zeros_like(old_db3a_orientations)

    db3b_indeces = np.nonzero(light_data.db_3b_working)
    old_db3b_positions = light_data.db_3b_position[db3b_indeces]
    old_db3b_orientations = light_data.db_3b_orientation[db3b_indeces]
    db3b_positions = np.zeros_like(old_db3b_positions)
    db3b_orientations = np.zeros_like(old_db3b_orientations)

    # Now change those old positions to new positions. The new positions are along the y axis.
    for old_position, new_position in zip([old_db1_positions, old_db2_positions, old_db3a_positions, old_db3b_positions], 
                                          [db1_positions, db2_positions, db3a_positions, db3b_positions]):
        new_position[:, :] = np.vstack((
            np.zeros_like(old_position[:, 2]), # x
            old_position[:, 2], # y
            np.zeros_like(old_position[:, 2]) # z
        )).T

    # Now convert the orientations. The probe +x direction is now the -y 
    # direction in cartesian. At 0 rotation, the probe +y direction is now the 
    # -z cartesian direction. Thus the probe +z is now +x.
    for old_orientation, new_orientation in zip([old_db1_orientations, old_db2_orientations, old_db3a_orientations, old_db3b_orientations], 
                                                [db1_orientations, db2_orientations, db3a_orientations, db3b_orientations]):
        unrotated_orientation = np.vstack((
            old_orientation[:, 2], # x
            -old_orientation[:, 0], # y
            -old_orientation[:, 1], # z
        ))

        # Rotations are done around the y axis.
        rotation_matrix = np.array((
            (np.cos(rotation_angle), 0, np.sin(rotation_angle)),
            (0, 1, 0),
            (-np.sin(rotation_angle), 0, np.cos(rotation_angle)),
        ))

        new_orientation[:, :] = (rotation_matrix @ unrotated_orientation).T

    # Combine the 3 direction.
    db3_positions = np.concatenate((db3a_positions, db3b_positions), axis=0)
    db3_orientations = np.concatenate((db3a_orientations, db3b_orientations), axis=0)
    
    return [db1_positions, db2_positions, db3_positions], [db1_orientations, db2_orientations, db3_orientations]


def get_args(light_data_objects : list[Light_Bdot1_Data], rotated_light_data_objects : list[list[Light_Bdot1_Data]], rotations, *args, baseline_time_indeces=slice(40), indeces_to_use=None, integrate_db=True, downsample_period=1):
    """
    Get all the arguments for the `optimization_function`.

    Parameters
    ----------
    light_data_objects : list[Light_Bdot1_Data]
        Lightsaber probe objects to get data from.
    rotated_light_data_objects : list[list[Light_Bdot1_Data]]
        List of lists of probe objects. Each sub-list should have an associated rotation.
    rotations : list[float]
        List of rotations in radians of the probe for when the probe was 
        attached along the cartesian `y` axis. 0 is when the probe `y` was 
        pointed in the cartesian `-z`.
    args : any
        Parameters to pass to the `optimization_function`.
    baseline_time_indeces : slice, default=slice(10)
        Slice of indeces that can be used for removing the measurement baseline.
    indeces_to_use : slice or None, default=None
        If `None` then use all points. If a slice then only optimize using those points.

    Returns
    -------
    See 'Parameters' of `optimization_function`.
    """
    all_shot_db_values = []
    all_shot_positions = []
    all_shot_orientations = []
    all_shot_stick_assignments = []
    all_interp_positions = []

    rotated_shots_start = len(light_data_objects)
    flattened_rotated_objects = []
    repeated_rotations = []
    rotated_shot_set_indeces = []
    for i, (rotation, object_set) in enumerate(zip(rotations, rotated_light_data_objects)):
        flattened_rotated_objects += object_set
        repeated_rotations += len(object_set) * [rotation]
        rotated_shot_set_indeces += len(object_set) * [i]

    for i, light_data in enumerate(tqdm.tqdm(light_data_objects + flattened_rotated_objects, desc="Loading shot data")):
        # Get the data from the working coils for each coil direction.
        db1_indeces = np.nonzero(light_data.db_1_working)
        db1_values = light_data.db_1[db1_indeces]
        db1_positions = light_data.db_1_position[db1_indeces]
        db1_orientations = light_data.db_1_orientation[db1_indeces]
        db1_sticks = np.repeat(np.arange(1, NUM_STICKS + 1), 4)[db1_indeces]

        db2_indeces = np.nonzero(light_data.db_2_working)
        db2_values = light_data.db_2[db2_indeces]
        db2_positions = light_data.db_2_position[db2_indeces]
        db2_orientations = light_data.db_2_orientation[db2_indeces]
        db2_sticks = np.repeat(np.arange(1, NUM_STICKS + 1), 4)[db2_indeces]

        db3a_indeces = np.nonzero(light_data.db_3a_working)
        db3a_values = light_data.db_3a[db3a_indeces]
        db3a_positions = light_data.db_3a_position[db3a_indeces]
        db3a_orientations = light_data.db_3a_orientation[db3a_indeces]
        db3a_sticks = np.repeat(np.arange(1, NUM_STICKS + 1), 4)[db3a_indeces]

        db3b_indeces = np.nonzero(light_data.db_3b_working)
        db3b_values = light_data.db_3b[db3b_indeces]
        db3b_positions = light_data.db_3b_position[db3b_indeces]
        db3b_orientations = light_data.db_3b_orientation[db3b_indeces]
        db3b_sticks = np.repeat(np.arange(1, NUM_STICKS + 1), 4)[db3b_indeces]

        # Now combine the 3 direction.
        db3_values = np.concatenate((db3a_values, db3b_values), axis=0)
        db3_positions = np.concatenate((db3a_positions, db3b_positions), axis=0)
        db3_orientations = np.concatenate((db3a_orientations, db3b_orientations), axis=0)
        db3_sticks = np.concatenate((db3a_sticks, db3b_sticks), axis=0)

        all_values = [db1_values, db2_values, db3_values]
        all_positions = [db1_positions, db2_positions, db3_positions]
        all_orientations = [db1_orientations, db2_orientations, db3_orientations]
        stick_assignments = [db1_sticks, db2_sticks, db3_sticks]

        if i >= rotated_shots_start:
            all_positions_cart, all_orientations_cart = _rotated_shot_positions_orientations(light_data, repeated_rotations[i - rotated_shots_start])
        else:
            all_positions_cart = []
            all_orientations_cart = []
            # Convert the positions and orientations into cartesian coordinates.
            for position, orientation in zip(all_positions, all_orientations):
                position_obj = Position()
                position_obj.cylindrical = position.T
                position_obj.to_tf_centered_coordinates()

                orientation_obj = Vector(position_obj)
                orientation_obj.from_port(
                    light_data.port.long_rad, light_data.port.lat_rad, 
                    light_data.port.alpha_rad, light_data.port.beta_rad, 
                    light_data.port.gamma_rad, orientation[:, 0], orientation[:, 1], 
                    orientation[:, 2], light_data.port.clocking_rad
                )

                all_positions_cart.append(position_obj.cartesian.T)
                all_orientations_cart.append(orientation_obj.cartesian.T)

        # Sort the values, positions, orientations, and assignments.
        for j in range(3):
            if i >= rotated_shots_start:
                # Sort in the -y direction.
                sorted_indeces = np.argsort(-all_positions_cart[j][:, 1])
            else:
                # Sort in the +z direction.
                sorted_indeces = np.argsort(all_positions_cart[j][:, 2])

            all_values[j] = all_values[j][sorted_indeces]
            all_positions_cart[j] = all_positions_cart[j][sorted_indeces]
            all_orientations_cart[j] = all_orientations_cart[j][sorted_indeces]
            stick_assignments[j] = stick_assignments[j][sorted_indeces]

        # For each set of coils we need to remove the baseline and get the dB value.
        all_db_values = []
        for dir_values in all_values:
            dir_values -= np.mean(dir_values[:, baseline_time_indeces], axis=1)[:, None]
            if indeces_to_use is not None:
                dir_values = dir_values[:, indeces_to_use]
            
            if integrate_db:
                dir_values = cumulative_trapezoid(dir_values, axis=-1, initial=0)

            # Downsample the values in the t axis.
            dir_values = dir_values[:, ::downsample_period]

            all_db_values.append(dir_values)

        all_shot_db_values.append(all_db_values)
        all_shot_positions.append(all_positions_cart)
        all_shot_orientations.append(all_orientations_cart)
        all_shot_stick_assignments.append(stick_assignments)

        NUM_POINTS = NUM_STICKS * 16
        if i >= rotated_shots_start:
            # For the rotated shots we space along y from +y to -y.
            all_interp_positions.append(
                np.vstack((
                    np.mean([np.mean(pos[:, 0]) for pos in all_positions_cart]) * np.ones(NUM_POINTS), # x
                    np.linspace(np.max([np.max(np.abs(pos[:, 1])) for pos in all_positions_cart]), 
                                -np.max([np.max(np.abs(pos[:, 1])) for pos in all_positions_cart]), num=NUM_POINTS), # y
                    np.mean([np.mean(pos[:, 2]) for pos in all_positions_cart]) * np.ones(NUM_POINTS), # z
                )).T
            )
        else:
            # For the interpolation positions, use the average x and y positions 
            # and space the points along the +z direction.
            all_interp_positions.append(
                np.vstack((
                    np.mean([np.mean(pos[:, 0]) for pos in all_positions_cart]) * np.ones(NUM_POINTS), # x
                    np.mean([np.mean(pos[:, 1]) for pos in all_positions_cart]) * np.ones(NUM_POINTS), # y
                    np.linspace(-np.max([np.max(np.abs(pos[:, 2])) for pos in all_positions_cart]), 
                                np.max([np.max(np.abs(pos[:, 2])) for pos in all_positions_cart]), num=NUM_POINTS) # z
                )).T
            )

    return all_shot_db_values, all_shot_positions, all_shot_orientations, all_shot_stick_assignments, np.array(all_interp_positions), rotated_shot_set_indeces, *args


def optimize_rotations(vacuum_shots, rotated_shots, rotations, plasma_shots=[], only_z=False):
    """
    Parameters
    ----------
    vacuum_shots : list[int]
        List of shot numbers that are vacuum shots and the lightsaber probe is 
        coming in along the insert.
    rotated_shots : list[list[int]]
        List of lists of shot numbers. Each sub-list should have an associated rotation.
    rotations : list[float]
        List of rotations in radians of the probe for when the probe was 
        attached along the cartesian `y` axis. 0 is when the probe `y` was 
        pointed in the cartesian `-z`. The angle increases for positive angle 
        around the `y` cartesian axis (i.e. what is expected).
    plasma_shots : list[int], default=[]
        List of shot numbers that have plasma to compare the final results.
    only_z : bool
        Whether to only try to optimize the rotation around the z axis of the machine.
    """
    generic_filepath = './notebooks/data/light_bdot1/{}.mat'
    light_data_objects = [Light_Bdot1_Data(shot, load_filepath=generic_filepath.format(shot), time_index_range=(65000, 65170)) for shot in vacuum_shots]
    rotated_light_data_objects = [[Light_Bdot1_Data(shot, load_filepath=generic_filepath.format(shot), time_index_range=(65000, 65170)) for shot in shot_set] for shot_set in rotated_shots]
    plasma_light_data_objects = [Light_Bdot1_Data(shot, load_filepath=generic_filepath.format(shot), time_index_range=(65000, 65170)) for shot in plasma_shots]
    
    x_angle_bounds = [(-np.pi / 50, np.pi / 50)] * NUM_STICKS
    y_angle_bounds = [(-np.pi / 50, np.pi / 50)] * NUM_STICKS
    z_angle_bounds = [(-np.pi / 4, np.pi / 4)] * NUM_STICKS
    rotation_angle_bounds = [(-np.pi / 10, np.pi / 10)] * len(rotations)

    # TODO: Change this as we really want to do the rotations in BRB cartesian.
    if only_z:
        all_angle_bounds = z_angle_bounds + rotation_angle_bounds
        x0 = np.zeros(NUM_STICKS + len(rotations))
    else:
        all_angle_bounds = x_angle_bounds + y_angle_bounds + z_angle_bounds + rotation_angle_bounds
        x0 = np.zeros(3 * NUM_STICKS + len(rotations))
    
    flattened_rotated_shots = [shot for shot_set in rotated_shots for shot in shot_set]

    # Decide how many processors to use.
    max_num_processors = 8
    total_num_shots = len(vacuum_shots) + len(flattened_rotated_shots)
    
    prior_num_shots = 1
    for num_processors in range(max_num_processors, 0, -1):
        shots_per_processor = total_num_shots / num_processors
        if num_processors == max_num_processors:
            prior_num_shots = max(1, shots_per_processor)
        elif int(shots_per_processor) > int(prior_num_shots):
            chosen_num_processors = num_processors + 1
            break
    else:
        chosen_num_processors = 1
    print("Using {} processors for calculation.".format(chosen_num_processors))

    fitting_indeces = slice(80, 170)
    with Pool(processes=chosen_num_processors) as pool:
        function_args = get_args(light_data_objects, rotated_light_data_objects, rotations, only_z, pool, indeces_to_use=fitting_indeces, integrate_db=True, downsample_period=8)

        # Save the data we've loaded.
        for light_data in light_data_objects:
            light_data.save(generic_filepath.format(light_data.shot_number))
        for rotation_set in rotated_light_data_objects:
            for light_data in rotation_set:
                light_data.save(generic_filepath.format(light_data.shot_number))

        # result = differential_evolution(optimization_function, all_angle_bounds, args=function_args, x0=x0, seed=1, vectorized=True)

    result = lambda x: x
    result.success = True
    if only_z:
        result.x = np.zeros(NUM_STICKS + len(flattened_rotated_shots))
    else:
        result.x = np.zeros(3 * NUM_STICKS + len(flattened_rotated_shots))
        x_angles, y_angles, z_angles = get_optimized_rotations()
        result.x[:NUM_STICKS] = x_angles
        result.x[NUM_STICKS:2 * NUM_STICKS] = y_angles
        result.x[2 * NUM_STICKS:3 * NUM_STICKS] = z_angles

    # Print out the optimized orientations.
    if not result.success:
        print("Because of '{}' the rotations couldn't be optimized. Here they are anyhow!".format(result.message))

    if only_z:
        directions = ['z']
    else:
        directions = ['x', 'y', 'x']

    for i, dir in enumerate(directions):
        print("{} angles (deg):\t{}".format(dir, np.rad2deg(result.x[i * NUM_STICKS:(i + 1) * NUM_STICKS])))
    
    print("rotations (deg):\t{}".format(np.rad2deg(result.x[NUM_STICKS * len(directions):])))

    all_function_args = get_args(light_data_objects + plasma_light_data_objects, rotated_light_data_objects, rotations, integrate_db=False, downsample_period=1)
    for plasma_light_data in plasma_light_data_objects:
        plasma_light_data.save(generic_filepath.format(plasma_light_data.shot_number))

    for i, shot in enumerate(vacuum_shots + plasma_shots + flattened_rotated_shots):
        null_angles = np.zeros(NUM_STICKS)[:, np.newaxis]
        if only_z:
            x_angles = np.zeros_like(null_angles)
            y_angles = np.zeros_like(null_angles)
            z_angles = result.x[:NUM_STICKS, np.newaxis]
        else:
            x_angles = result.x[:NUM_STICKS][:, np.newaxis]
            y_angles = result.x[NUM_STICKS:2 * NUM_STICKS][:, np.newaxis]
            z_angles = result.x[2 * NUM_STICKS:3 * NUM_STICKS][:, np.newaxis]

        if shot in flattened_rotated_shots:
            null_rotation = np.zeros(1)
            for k, shot_set in enumerate(rotated_shots):
                if shot in shot_set:
                    if only_z:
                        rotation_angle = np.array([result.x[NUM_STICKS + k]])
                    else:
                        rotation_angle = np.array([result.x[3 * NUM_STICKS + k]])
                    break
        else:
            null_rotation = None
            rotation_angle = None

        unrotated_db_vals, _ = get_rotated_db(all_function_args[0][i], all_function_args[1][i], all_function_args[2][i], all_function_args[3][i], all_function_args[4][i], null_angles, null_angles, null_angles, null_rotation)
        rotated_db_vals, interp_positions = get_rotated_db(all_function_args[0][i], all_function_args[1][i], all_function_args[2][i], all_function_args[3][i], all_function_args[4][i], x_angles, y_angles, z_angles, rotation_angle)

        fig = plt.figure()
        if shot in flattened_rotated_shots:
            y_ax_points = interp_positions[:, 1]
            shot_type = 'Rotated'
        else:
            y_ax_points = interp_positions[:, 2]
            if shot in vacuum_shots:
                shot_type = 'Vacuum'
            else:
                shot_type = 'Plasma'
        fig.suptitle("Shot #{}: {}".format(shot, shot_type))

        gs = fig.add_gridspec(2, 3, hspace=0, wspace=0)
        unrotated_axs, rotated_axs = gs.subplots(sharex=True, sharey=True)

        for j, (db_type, db_val_set, axs) in enumerate(zip(['Old', 'New'], [unrotated_db_vals, rotated_db_vals], [unrotated_axs, rotated_axs])):
            for k in range(3):
                vlim = max(np.max(abs(db_vals)) for db_vals in [unrotated_db_vals[:, k, :, 0], rotated_db_vals[:, k, :, 0]])
                db_vals = db_val_set[:, k, :, 0]
                axs[k].pcolormesh(np.arange(db_val_set.shape[2]), y_ax_points, db_vals, vmin=-vlim, vmax=vlim, cmap='seismic')

                if k == 0:
                    axs[k].set_ylabel(db_type + "\n" + r'$z$')
                if j == 1:
                    axs[k].set_xlabel('index')

                db_directions = ['r', r'\phi', 'z']
                axs[k].text(0.05, 0.05, r"$\partial_t B_{}$".format(db_directions[k]), bbox={'facecolor': 'white', 'pad': 5, 'alpha': 0.5}, transform=axs[k].transAxes)

    plt.show()


def get_optimized_rotations():
    x_angles = np.deg2rad([0.27203229, -0.59787832, -0.51506479, -0.81134022, -0.95119329, 0.08748566, 0.45045631, 0.14634866])[:, np.newaxis]
    y_angles = np.deg2rad([1.6717782, 1.69397267, 1.2931021, 0.58708088, -0.43461917, -1.37663991, -1.79264322, -1.86384422])[:, np.newaxis]
    z_angles = np.deg2rad([-10.87297036, 15.11387698, 16.63533783, 16.11402534, 29.95967132, 22.81563164, 20.70193831, 6.96130888])[:, np.newaxis]

    return x_angles, y_angles, z_angles


def get_db_and_b(light_data : Light_Bdot1_Data, helm_data : Helmholtz_Data, tf_data : TF_Coil_Data, interp_positions=None):
    """
    Parameters
    ----------
    interp_positions : np.array[float], default=None
        If `None` then choose positions along the `z` axis of the machine. If 
        not `None` then should be cartesian coordinates of the ball of shape 
        `(NUM_POSITIONS, 3)`.

    Returns
    -------
    db_vals, b_vals : np.array[float]
        3D-array of interpolated dB and B components in cylindrical coordinates, 
        `(dB_r, dB_phi, dB_z)`. Shape of `(NUM_POSITIONS, 3, NUM_TIMES)`.
    interp_positions : np.array[float]
        2D-array of shape `(NUM_POINTS, 3)` of coordinates in cartesian 
        coordinates corrected for TF and insert.
    """
    function_args = get_args([light_data], [], [], integrate_db=False)

    magnitude_correction = 2
    db_vals, probe_interp_positions = get_rotated_db(function_args[0][0], function_args[1][0], function_args[2][0], function_args[3][0], function_args[4][0], *get_optimized_rotations(), None)
    db_vals = db_vals[:, :, :, 0] * magnitude_correction

    interp_position_obj = Position(probe_interp_positions.T)
    interp_position_obj.to_tf_centered_coordinates(inverse=True)
    background_field = helm_data.analytic_b_h(interp_position_obj.cylindrical[0], interp_position_obj.cylindrical[2], insert=True)
    background_field_vector = Vector(interp_position_obj)
    background_field_vector.cylindrical = np.array([
        background_field[0],
        np.zeros_like(background_field[0]),
        background_field[1]
    ])
    background_field_vector.position.to_tf_centered_coordinates()

    b_vals = cumulative_trapezoid(db_vals, light_data.time, axis=2, initial=0)
    # Now remove the effects of railing in the Bdot measurements by smoothing the final B signal and removing the difference between smoothed and unsmoothed.
    filter_length = b_vals.shape[0] // 4
    # Filter along the spatial direction for the final time to get the smoothed integrated value.
    last_t_integrated_b = savgol_filter(b_vals[:, :, -1], filter_length, 3, axis=0)
    correction_array = b_vals[:, :, -1] - last_t_integrated_b

    b_vals += background_field_vector.cylindrical.T[:, :, np.newaxis]

    # Now get the start of the pressure wave.
    wave_boundary_db_z = -300
    # Take the median of all positions as the pressure wave start.
    wave_start_index = int(np.median(np.argmax(db_vals[:, 2, :] < wave_boundary_db_z, axis=-1)))
    # Now get the lowest index of where B_z = 0. This must be after the pressure wave starts.
    layer_start_index = int(np.median(np.argmax(b_vals[:, 2, wave_start_index:] < 0, axis=-1))) + wave_start_index
    # Now create a factor to multiply the correction by for each time point that ramps up starting at the wave and ends halfway between the wave and the layer.
    ramp_start = wave_start_index
    ramp_end = layer_start_index
    factor = np.concatenate((np.zeros(ramp_start), np.linspace(0, 1, ramp_end - ramp_start), np.ones(b_vals.shape[2] - ramp_end)))
    # Now correct the B values.
    b_vals = b_vals - correction_array[:, :, np.newaxis] * factor[np.newaxis, np.newaxis, :]

    if 59645 <= light_data.shot_number <= 59801:
        logging.info("Adding in B_phi component from TF coil.")
        radius = background_field_vector.position.cylindrical[0, 0]
        b_vals[:, 1] += tf_data.b_phi(radius)

    if interp_positions is None:
        return db_vals, b_vals, probe_interp_positions
    else:
        # Create linear interpolators for the db_vals and b_vals so that we can 
        # find the magnetic field at the interp_positions.
        probe_direction = probe_interp_positions[-1] - probe_interp_positions[0]
        dist_along_probe = probe_interp_positions @ probe_direction
        measurement_dist_along_probe = interp_positions @ probe_direction

        db_interpolator = interp1d(dist_along_probe, db_vals, axis=0, fill_value='extrapolate')
        new_db_vals = db_interpolator(measurement_dist_along_probe)
        b_interpolator = interp1d(dist_along_probe, b_vals, axis=0, fill_value='extrapolate')
        new_b_vals = b_interpolator(measurement_dist_along_probe)

        return new_db_vals, new_b_vals, interp_positions


def plot_db_and_b(light_data : Light_Bdot1_Data, helm_data : Helmholtz_Data, tf_data : TF_Coil_Data):
    rotated_db_vals, integrated_b_vals, interp_positions = get_db_and_b(light_data, helm_data, tf_data)

    fig = plt.figure()
    gs = fig.add_gridspec(2, 3, hspace=0, wspace=0)
    axs = gs.subplots(sharex=True, sharey=True)

    names = [r'$\partial_t B_{{{}}}$', r'$B_{{{}}}$']
    directions = ['r', r'\phi', 'z']
    for name, axs_type, vals_type in zip(names, axs, [rotated_db_vals, integrated_b_vals]):
        for i in range(3):
            ax = axs_type[i]
            vals = vals_type[:, i]

            vlim = np.max(abs(vals))
            try:
                y_points = np.arange(light_data.time_index_range[0], light_data.time_index_range[1] + 1)
                time_to_index = lambda time: (light_data.time_index_range[1] + 1 - light_data.time_index_range[0]) / (light_data.time[-1] - light_data.time[0]) * (time - light_data.time[0]) + light_data.time_index_range[0]
            except TypeError:
                y_points = np.arange(light_data.time.size)
                time_to_index = lambda time: np.argmax(light_data.time - time > 0)
            index_to_time = lambda index: light_data.time[light_data.to_raw_index(index.astype(int))]
            time_ax = ax.secondary_yaxis('right', functions=(index_to_time, time_to_index))
            time_ax.set_ylabel(r'$t$ (s)')

            cmesh = ax.pcolormesh(interp_positions[:, 2], y_points, vals.T, vmin=-vlim, vmax=vlim, cmap='seismic')
            ax.contour(interp_positions[:, 2], y_points, integrated_b_vals[:, 2].T, levels=[0], colors='green')
            # cmesh = ax.pcolormesh(interp_positions[:, 2], light_data.time, vals.T, vmin=-vlim, vmax=vlim, cmap='seismic')
            # ax.contour(interp_positions[:, 2], light_data.time, integrated_b_vals[:, 2].T, levels=[0], colors='green')
            ax.text(0.05, 0.05, name.format(directions[i]), bbox={'facecolor': 'white', 'pad': 5, 'alpha': 0.5}, transform=ax.transAxes)
            ax.set_xlabel(r'$z$ (m)')
            ax.set_ylabel(r'index')
            ax.label_outer()

            # fig.colorbar(cmesh, ax=ax, pad=0)
            # ax.set_xlim(np.min(interp_positions[:, 2]), 0.2832)

    light_radius = np.linalg.norm(np.mean(interp_positions, axis=0)[:2])
    pa_position = Position()
    pa_position.cylindrical = pa_data.position
    pa_position.add_insert('s')
    pa_position.to_tf_centered_coordinates()
    fig.suptitle("Shot #{}: Lightsaber Data\n".format(light_data.shot_number) + r"$r_{{\mathrm{{LS}}}} = {:.3f}$ m, $r_{{\mathrm{{PA}}}} = {:.3f}$ m".format(light_radius, pa_position.cylindrical[0]))
    return fig, axs

if __name__ == "__main__":
    # Optimized rotations:
    # x angles (deg): [ 0.27203229 -0.59787832 -0.51506479 -0.81134022 -0.95119329  0.08748566 0.45045631  0.14634866]
    # y angles (deg): [ 1.6717782   1.69397267  1.2931021   0.58708088 -0.43461917 -1.37663991 -1.79264322 -1.86384422]
    # x angles (deg): [-10.87297036  15.11387698  16.63533783  16.11402534  29.95967132 22.81563164  20.70193831   6.96130888]
    # rotations (deg):        [  0.01847745 -11.57501874  -4.29693101 -15.70536983]

    NUM_EVALS = 0
    # We may want to remove shot 60099 and 59921 as the probe is very close to the TF coil and thus measurements of B_r and B_phi are difficult.
    vac_shots = [60099, 60106, 60163, 60179, 60186, 60195, 60202, 60209, 60218, 60225]
    additional_vac_shots = [59852, 59858, 59864, 59870, 59882, 59883, 59898, 59905, 59908, 59921]
    rotated_shots = [
        [60421, 60422],
        [60423, 60424],
        [60425, 60426],
        [60427, 60428]
    ]
    plasma_shots = [60095, 60104, 60167, 60176, 60182, 60189, 60197, 60205, 60213, 60219]
    r_30_w_tf_shots = [59760, 59761, 59762, 59763, 59764, 59765]
    # All the rotations seem to be shifted by ~10 degrees so implement that for the initial guess.
    rotation_adjustment = np.deg2rad(10)
    rotations = [
        0 + rotation_adjustment + np.deg2rad(0.01847745), 
        np.pi / 2 + rotation_adjustment + np.deg2rad(-11.57501874), 
        np.pi + rotation_adjustment + np.deg2rad(-4.29693101), 
        3 * np.pi / 2 + rotation_adjustment + np.deg2rad(-15.70536983)
    ]
    # optimize_rotations(vac_shots + additional_vac_shots, rotated_shots, rotations, plasma_shots, only_z=False)

    simple_vac_shots = [60106]
    simple_rotated_shots = [[60421]]
    simple_rotations = [0 + rotation_adjustment]
    # optimize_rotations(simple_vac_shots, simple_rotated_shots, simple_rotations, plasma_shots, only_z=False)

    from pa_probe1.analyze_iv_curve import shot_information as shot_info
    # for shot in shot_info.helmholtz_100_drive_6_middle_True_snubber_True_tf_50_z_43_clocking_45:
    # for shot in range(59760, 59766): # Example shots with TF on.
    # for shot in range(60229, 60239): # Example shots with middle on.
    for shot in r_30_w_tf_shots:
        try:
            filepath = '../analysis_codes/data/{}/{}.mat'
            light_data = Light_Bdot1_Data(shot, time_index_range=(65000, 65170), load_filepath=filepath.format('light_bdot1', shot))
            # light_data = Light_Bdot1_Data(shot, time_index_range=(200, 20000), load_filepath=filepath.format('light_bdot1', shot))
            helm_data = Helmholtz_Data(shot, load_filepath=filepath.format('helmholtz', shot))
            tf_data = TF_Coil_Data(shot, load_filepath=filepath.format('tf_coil', shot))
            pa_data = PA_Probe1_Data(shot, load_filepath=filepath.format('pa_probe1', shot))
            pa_position = Position()
            pa_position.cylindrical = pa_data.position
            pa_position.add_insert('s')

            fig, axs = plot_db_and_b(light_data, helm_data, tf_data)

            light_data.save(filepath.format('light_bdot1', shot))
            helm_data.save(filepath.format('helmholtz', shot))
            tf_data.save(filepath.format('tf_coil', shot))

            for ax in axs.flat:
                ax.axvline(pa_position.cartesian[2], color='black', linestyle='dotted')
        except Exception as e:
            logging.warning("Skipping shot #{} because of:\n{}".format(shot, e))
            raise
            continue
        plt.show()
