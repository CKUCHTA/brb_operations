"""Get data about the light_bdot1 probe from the MDSplus tree."""
from modules.generic_get_data import Get, Data, lazy_get, Port
import numpy as np
import logging


class Light_Bdot1_Data(Data):
    def __init__(self, tree, *args, **kwargs):
        """
        Hold all the data for the light_bdot1 probe in one class for easier access and reduced network connections.

        Notes
        -----
        To get data, call the data without the underscore treating it as an attribute.
        """
        super().__init__(tree, [], [], *args, **kwargs)

    @lazy_get
    def db_r(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB_r/dt for each set of conglomerated coils. Of shape `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(Get('\\light_bdot1_dbr'))

    @lazy_get
    def db_phi(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB_phi/dt for each set of conglomerated coils. Of shape `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(Get('\\light_bdot1_dbphi'))

    @lazy_get
    def db_z(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB_z/dt for each set of conglomerated coils. Of shape `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(Get('\\light_bdot1_dbz'))
    
    @lazy_get
    def position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each set of conglomerated coils. Of shape `(NUM_PROBES, 3)`.
        """
        return self.get(
            Get('TRANSPOSE( [' + ', '.join('DATA(\\light_bdot1_{direction})'.format(direction=dir) for dir in ['r', 'phi', 'z']) + '] )', name='light_bdot1_position', signal=False)
        )

    @lazy_get
    def db_1(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 1 coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\light_bdot1_db1'))
    
    @lazy_get
    def db_1_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 1 coil. Of shape `(NUM_COILS, 3)`.
        """
        return self.get(
            Get('TRANSPOSE( [' + ', '.join('DATA(\\light_bdot1_db1{direction})'.format(direction=dir) for dir in ['r', 'phi', 'z']) + '] )', name='light_bdot1_db1_position', signal=False)
        )
    
    @lazy_get
    def db_1_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 1 coil. Of shape `(NUM_COILS,)`.
        """
        return self.get(Get('\\light_bdot1_db1working', name='light_bdot1_db1_working', signal=False), np_data_type=bool)

    @lazy_get
    def db_1_orientation(self):
        """
        Get the local orientation of each db_1 coil.

        Returns
        -------
        np.array[float]
            2D-array of normal unit vectors for each 1 coil in probe coordinates. Of shape `(NUM_COILS, 3)`.
        """
        return self.get(
            Get('TRANSPOSE( [' + ', '.join('DATA(\\light_bdot1_db1.orientation:{direction})'.format(direction=dir) for dir in ['x', 'y', 'z']) + '] )', name='light_bdot1_db1_orientation', signal=False)
        )


    @lazy_get
    def db_2(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 2 coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\light_bdot1_db2'))
    
    @lazy_get
    def db_2_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 2 coil. Of shape `(NUM_COILS, 3)`.
        """
        return self.get(
            Get('TRANSPOSE( [' + ', '.join('DATA(\\light_bdot1_db2{direction})'.format(direction=dir) for dir in ['r', 'phi', 'z']) + '] )', name='light_bdot1_db2_position', signal=False)
        )
    
    @lazy_get
    def db_2_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 2 coil. Of shape `(NUM_COILS,)`.
        """
        return self.get(Get('\\light_bdot1_db2working', name='light_bdot1_db2_working', signal=False), np_data_type=bool)

    @lazy_get
    def db_2_orientation(self):
        """
        Get the local orientation of each db_2 coil.

        Returns
        -------
        np.array[float]
            2D-array of normal unit vectors for each 2 coil in probe coordinates. Of shape `(NUM_COILS, 3)`.
        """
        return self.get(
            Get('TRANSPOSE( [' + ', '.join('DATA(\\light_bdot1_db2.orientation:{direction})'.format(direction=dir) for dir in ['x', 'y', 'z']) + '] )', name='light_bdot1_db2_orientation', signal=False)
        )

    @lazy_get
    def db_3a(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 3a coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\light_bdot1_db3a'))
    
    @lazy_get
    def db_3a_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 3a coil. Of shape `(NUM_COILS, 3)`.
        """
        return self.get(
            Get('TRANSPOSE( [' + ', '.join('DATA(\\light_bdot1_db3a{direction})'.format(direction=dir) for dir in ['r', 'phi', 'z']) + '] )', name='light_bdot1_db3a_position', signal=False)
        )
    
    @lazy_get
    def db_3a_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 3a coil. Of shape `(NUM_COILS,)`.
        """
        return self.get(Get('\\light_bdot1_db3aworking', name='light_bdot1_db3a_working', signal=False), np_data_type=bool)

    @lazy_get
    def db_3a_orientation(self):
        """
        Get the local orientation of each db_3a coil.

        Returns
        -------
        np.array[float]
            2D-array of normal unit vectors for each 3a coil in probe coordinates. Of shape `(NUM_COILS, 3)`.
        """
        return self.get(
            Get('TRANSPOSE( [' + ', '.join('DATA(\\light_bdot1_db3a.orientation:{direction})'.format(direction=dir) for dir in ['x', 'y', 'z']) + '] )', name='light_bdot1_db3a_orientation', signal=False)
        )

    @lazy_get
    def db_3b(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured dB/dt for each 3b coil. Of shape `(NUM_COILS, NUM_TIMES)`.
        """
        return self.get(Get('\\light_bdot1_db3b'))
    
    @lazy_get
    def db_3b_position(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of machine cylindrical postions, `(r, phi, z)` for each 3b coil. Of shape `(NUM_COILS, 3)`.
        """
        return self.get(
            Get('TRANSPOSE( [' + ', '.join('DATA(\\light_bdot1_db3b{direction})'.format(direction=dir) for dir in ['r', 'phi', 'z']) + '] )', name='light_bdot1_db3b_position', signal=False)
        )
    
    @lazy_get
    def db_3b_working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of booleans for each 3b coil. Of shape `(NUM_COILS,)`.
        """
        return self.get(Get('\\light_bdot1_db3bworking', name='light_bdot1_db3b_working', signal=False), np_data_type=bool)

    @lazy_get
    def db_3b_orientation(self):
        """
        Get the local orientation of each db_3b coil.

        Returns
        -------
        np.array[float]
            2D-array of normal unit vectors for each 3b coil in probe coordinates. Of shape `(NUM_COILS, 3)`.
        """
        return self.get(
            Get('TRANSPOSE( [' + ', '.join('DATA(\\light_bdot1_db3b.orientation:{direction})'.format(direction=dir) for dir in ['x', 'y', 'z']) + '] )', name='light_bdot1_db3b_orientation', signal=False)
        )

    @lazy_get
    def time(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of time values. Of shape `(NUM_TIMES,)`.
        """
        server_time_value = self.get(Get('dim_of(\\light_bdot1_db1)', name='light_bdot1_time'))
        if server_time_value.size == 0:
            logging.warning("Since the `light_bdot1` time data doesn't work at the moment, I'm manually returning the timing.")
            return 10**-7 * np.arange(self.db_1.shape[1])
        
    @lazy_get
    def port(self) -> Port:
        """
        Returns
        -------
        Port
            Object that represents the port position of this probe.
        """
        return Port(self, 'light_bdot1_')

