"""Get various derived quantities from the `light_bdot1` probe."""
from light_bdot1.get_data import Light_Bdot1_Data
from helmholtz.get_data import Helmholtz_Data
from tf_coil.get_data import TF_Coil_Data
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.interpolate import interp1d
from scipy.integrate import cumulative_trapezoid
from modules.coordinates import Position, Vector
from modules.bdot.interpolation import get_db_machine_coords
import logging
import tqdm


def align_timing(light_data: Light_Bdot1_Data, wave_index_bounds=(65040, 65075), max_index_offset=5, plot_alignment_procedure=False):
    """
    Align the timing of each digitizer by trying to best align the pressure wave.

    Parameters
    ----------
    light_data : Light_Bdot1_Data
    wave_index_bounds : tuple[int]
        Tuple of 2 ints where the 1st is at least 10 indeces before when the wave may 
        hit the probe and the 2nd is atleast 10 indeces after but before reconnection happens.
    max_index_offset : int
        Maximum difference between any two signals. A large value of this significantly slows the code.
    plot_alignment_procedure : bool, default=False
        Whether to plot how the data was aligned.

    Returns
    -------
    list[int]
        Offsets for each digitizer.

    See Also
    --------
    update_db_arrays : Use the returned offsets to update the db arrays.
    """
    # Each digitizer has a coil in the (1 or 2) direction and a coil in the 
    # (3a or 3b) direction so we'll align the timing using the 3 direction.
    # TODO: Change this if we change the assignment of the digitizers to the coils.
    digitizer_numbers = [323, 324, 325, 326]
    coil_directions = [['2', '3a'], ['1', '3b'], ['2', '3a'], ['1', '3b']]
    pcb_numbers = [np.arange(1, 17), np.arange(1, 17), np.arange(17, 33), np.arange(17, 33)]

    # Create a list of arrays that contain the dbz values for each digitizer.
    dbz_values = []
    # Also an array to hold which coils were working.
    dbz_working_coils = []
    # And finally the positions of each coil in the z direction.
    dbz_z_positions = []

    logging.debug("Getting dB/dt values, working status, and positions of coils.")
    for (_, z_coil_name), pcb_nums in zip(coil_directions, pcb_numbers):
        start_index = light_data.to_raw_index(wave_index_bounds[0])
        if start_index < 0:
            raise ValueError("The `light_data` object needs to start at the time index {} or earlier but starts at {}. This is needed for aligning data.".format(wave_index_bounds[0], light_data.time_index_range[0]))
        end_index = light_data.to_raw_index(wave_index_bounds[1])
        if light_data.time_index_range is not None and light_data.time_index_range[1] < end_index:
            raise ValueError("The `light_data` object needs to end at the time index {} or later but ends at {}. This is needed for aligning data.".format(wave_index_bounds[1], light_data.time_index_range[1]))

        dbz_values.append(getattr(light_data, 'db_{}'.format(z_coil_name))[pcb_nums - 1, start_index:end_index])
        # Remove the digitizer offset.
        dbz_values[-1] = dbz_values[-1] - np.mean(dbz_values[-1][:, :10], axis=1)[:, None]

        dbz_working_coils.append(getattr(light_data, 'db_{}_working'.format(z_coil_name))[pcb_nums - 1])
        dbz_z_positions.append(getattr(light_data, 'db_{}_position'.format(z_coil_name))[pcb_nums - 1, 2])

    NUM_TIMES = dbz_values[0].shape[1]

    # Create a list of all cross correlations between signal pairs up to the max_index_offset 
    # (i.e. each 1D correlation array is of size `2 * max_index_offset + 1`). This array can
    # be indexed as `[dig1_index][dig2_index - dig1_index - 1][max_index_offset + offset12]`.
    # Offsets are defined as dig1_vals[i] * dig2_vals[i - offset12] meaning the dig1_vals
    # happen `offset12` indeces before the dig2_vals, `offset12 = dig2_offset - dig1_offset`.
    logging.debug("Calculating cross correlations of ~dB_z/dt between digitizers {} assuming symmetry across the z axis.".format(digitizer_numbers))
    all_cross_correlations = []
    for i in range(len(digitizer_numbers) - 1):
        all_cross_correlations.append([])

        dbz_i_vals = dbz_values[i][np.nonzero(dbz_working_coils[i])]
        # When interpolating we assume symmetry across the z-axis so we only care about the absolute value of z.
        dbz_i_pos = abs(dbz_z_positions[i][np.nonzero(dbz_working_coils[i])])

        # Sort the array and positions into increasing order.
        dbz_i_sorted_indeces = np.argsort(dbz_i_pos)
        dbz_i_vals = dbz_i_vals[dbz_i_sorted_indeces]
        dbz_i_pos = dbz_i_pos[dbz_i_sorted_indeces]
        for j in range(i + 1, len(digitizer_numbers)):
            all_cross_correlations[i].append(np.zeros(2 * max_index_offset + 1))

            dbz_j_vals = dbz_values[j][np.nonzero(dbz_working_coils[j])]
            dbz_j_pos = abs(dbz_z_positions[j][np.nonzero(dbz_working_coils[j])])
            
            dbz_j_sorted_indeces = np.argsort(dbz_j_pos)
            dbz_j_vals = dbz_j_vals[dbz_j_sorted_indeces]
            dbz_j_pos = dbz_j_pos[dbz_j_sorted_indeces]
            # Interpolate the j dbz values to the i positions.

            i_index = 0
            j_index = 0
            while i_index < dbz_i_pos.size:
                if j_index == dbz_j_pos.size or dbz_i_pos[i_index] <= dbz_j_pos[j_index]:
                    # Linearly interpolate between positions.
                    if j_index == 0:
                        dbz_j_vals_interp = dbz_j_vals[0]
                    elif j_index == dbz_j_pos.size:
                        dbz_j_vals_interp = dbz_j_vals[-1]
                    else:
                        dbz_j_vals_interp = dbz_j_vals[j_index - 1] * (dbz_i_pos[i_index] - dbz_j_pos[j_index - 1]) / (dbz_j_pos[j_index] - dbz_j_pos[j_index - 1]) + dbz_j_vals[j_index] * (dbz_j_pos[j_index] - dbz_i_pos[i_index]) / (dbz_j_pos[j_index] - dbz_j_pos[j_index - 1])
                
                    all_cross_correlations[i][-1] += np.array([
                        np.sum(dbz_i_vals[i_index, max(offset, 0):min(NUM_TIMES, NUM_TIMES + offset)] * dbz_j_vals_interp[max(0, -offset):min(NUM_TIMES - offset, NUM_TIMES)]) 
                        for offset in range(-max_index_offset, max_index_offset + 1)
                    ]) / dbz_i_pos.size # Normalize by the number of probes.

                    i_index += 1
                else:
                    j_index += 1

    # We only care about the absolute values of the cross correlation as some coils are reversed as compared to others.
    max_cross_correlation = 0
    max_offset_combo = None
    logging.debug("Trying all combinations of offsets in index between digitizers where the maximum index difference is {}.".format(max_index_offset))
    for offset_combo in _generate_offsets(len(digitizer_numbers), max_index_offset):
        curr_cross_correlation = 0
        for dig1_index, dig1_offset in enumerate(offset_combo[:-1]):
            for dig2_index, dig2_offset in enumerate(offset_combo[dig1_index + 1:], start=dig1_index + 1):
                curr_cross_correlation += abs(all_cross_correlations[dig1_index][dig2_index - dig1_index - 1][max_index_offset + dig2_offset - dig1_offset])

        if curr_cross_correlation > max_cross_correlation:
            max_cross_correlation = curr_cross_correlation
            max_offset_combo = offset_combo
    
    logging.info("The best offset combination was [{}].".format(', '.join('{}: {}'.format(dig_num, offset) for dig_num, offset in zip(digitizer_numbers, max_offset_combo))))
    
    if plot_alignment_procedure:
        logging.debug("Plotting alignment results.")
        fig = plt.figure(layout='constrained')
        fig_data, fig_correlation = fig.subfigures(1, 2)

        axs_data = fig_data.subplots(2, 2)
        for i, ax_data in enumerate(axs_data.flat):
            plotted_dbz_values = dbz_values[i]
            plotted_dbz_values[np.nonzero(1 - dbz_working_coils[i])] = np.NAN
            plotted_dbz_values = np.ma.array(plotted_dbz_values, mask=np.isnan(plotted_dbz_values))

            custom_cmap = matplotlib.colormaps['seismic']
            custom_cmap.set_bad('grey', 0.2)
            cmesh = ax_data.pcolormesh(
                np.arange(wave_index_bounds[0], wave_index_bounds[1]), abs(dbz_z_positions[i]), plotted_dbz_values, 
                cmap=custom_cmap, vmin=-np.ma.max(np.ma.abs(plotted_dbz_values)), vmax=np.ma.max(np.ma.abs(plotted_dbz_values))
            )

            # Make some vertical lines that show the new alignment of the data.
            num_vlines = 2 * int(((wave_index_bounds[1] - wave_index_bounds[0]) // 2) / max_index_offset) + 1
            middle_index = (wave_index_bounds[1] - wave_index_bounds[0]) // 2 + wave_index_bounds[0]
            for vline in np.arange(
                int(middle_index - (num_vlines // 2) * max_index_offset),
                int(middle_index + (num_vlines // 2 + 1) * max_index_offset),
                max_index_offset
            ):
                ax_data.axvline(vline - max_offset_combo[i], color='green', linestyle='dashed')

            ax_data.set_title("dig {}: coil {}[{} ... {}]".format(digitizer_numbers[i], coil_directions[i][1], min(pcb_numbers[i]), max(pcb_numbers[i])))
            ax_data.set_xlabel("index")
            ax_data.set_ylabel(r"$|z|$")
            fig_data.colorbar(cmesh, ax=ax_data)
        fig_data.suptitle("Raw data")

        axs_correlation = fig_correlation.subplots(4, 4)
        for dig1_index, axs_correlation_row in enumerate(axs_correlation):
            for dig2_index, ax_correlation in enumerate(axs_correlation_row):
                if dig1_index == dig2_index:
                    continue
                elif dig1_index < dig2_index:
                    ax_correlation.scatter(np.arange(-max_index_offset, max_index_offset + 1), abs(all_cross_correlations[dig1_index][dig2_index - dig1_index - 1]))
                else:
                    ax_correlation.scatter(-np.arange(-max_index_offset, max_index_offset + 1), abs(all_cross_correlations[dig2_index][dig1_index - dig2_index - 1]))

                ax_correlation.axvline(max_offset_combo[dig2_index] - max_offset_combo[dig1_index], color='red')
                ax_correlation.set_title("dig {} and dig {}".format(digitizer_numbers[dig1_index], digitizer_numbers[dig2_index]))
        fig_correlation.suptitle("Cross Correlations")

        fig.suptitle("Shot #{}: ".format(light_data.shot_number) + ", ".join(r"dig {dig}: $t {op} {off}$".format(dig=digitizer_numbers[i], op='-' if max_offset_combo[i] >= 0 else '+', off=abs(max_offset_combo[i])) for i in range(len(digitizer_numbers))))

    # return max_offset_combo
    logging.debug("Changing `db1`, `db2`, `db3a`, and `db3b` arrays for the lightsaber data object.")
    for coil_names, pcb_nums, offset in zip(coil_directions, pcb_numbers, max_offset_combo):
        for coil_name in coil_names:
            db_array = getattr(light_data, 'db_{}'.format(coil_name))
            old_db_array = db_array.copy()
            db_array[pcb_nums - 1, max(offset, 0):min(db_array.shape[1], db_array.shape[1] + offset)] = db_array[pcb_nums - 1, max(-offset, 0):min(db_array.shape[1], db_array.shape[1] - offset)]

            # Update the new unknown time points to be the same value as the most recent db value in time.
            if offset > 0:
                db_array[pcb_nums - 1, :offset] = db_array[pcb_nums - 1, offset + 1][:, None]
            elif offset < 0:
                db_array[pcb_nums - 1, db_array.shape[1] + offset:] = db_array[pcb_nums - 1, db_array.shape[1] + offset - 1][:, None]

            setattr(light_data, 'db_{}'.format(coil_name), db_array)

def _generate_offsets(num_signals, max_diff, curr_min=0, curr_max=0, first_recursion=True):
    """For internal use only.
    Generate all possible offsets for a number of signals.

    Parameters
    ----------
    num_signals : int
        Number of signals to generate offsets for.
    max_diff : int
        Maximum difference between any two signal offsets.
    curr_min, curr_max : int, default=0
        Minimum (maximum) offset generated. For use during recursion.
    first_recursion : bool, default=True
        Whether we just entered the recursion. If `True` then we need to do some special
        stuff to make sure that we don't get repeats.

    Yields
    ------
    list[int]
        List of offsets for each signal.
    """
    if num_signals == 0:
        yield []
    
    if first_recursion:
        # If this is the first time thru then keep the first signal at 0 offset to remove repeats.
        for offset_combo in _generate_offsets(num_signals - 1, max_diff, first_recursion=False):
            yield [0] + offset_combo
    else:
        for offset in range(curr_max - max_diff, curr_min + max_diff + 1):
            if num_signals == 1:
                yield [offset]
            else:
                for offset_combo in _generate_offsets(num_signals - 1, max_diff, curr_min=min(curr_min, offset), curr_max=max(curr_max, offset), first_recursion=False):
                    yield [offset] + offset_combo

def get_db(light_data: Light_Bdot1_Data, insert_used=False, tf_coil_lowered=False, interpolation_kind='linear', baseline_time_indeces=slice(10)):
    """
    Get the `r`, `phi`, and `z` components for the Bdot data.

    Parameters
    ----------
    light_data : Light_Bdot1_Data
    insert_used : bool, default=False
        Whether the insert was on the machine. If so, the positions need to be
        adjusted if the probe is not at 0 latitude.
    tf_coil_lowered : bool, default=False
        Whether the TF coil was lowered and the central axis of the machine is
        different than usual which requires corrections for position and 
        direction.
    interpolation_kind : str, default='linear'
        How we interpolate the magnitudes between coils.
    baseline_time_indeces : slice
        Slice of indeces along the time axis where we can remove the digitizer
        baseline offset and nothing should be happening.

    Returns
    -------
    interpolation_positions : np.array[float]
        2D-array of cylindrical positions,`(r, phi, z)`, where `phi` is in 
        radians. Shape of `(NUM_POSITIONS, 3)`.
    interpolation_db_values : np.array[float]
        3D-array of interpolated dB/dt components in cylindrical coordinates, 
        `(dB_r / dt, dB_phi / dt, dB_z / dt)`. Shape of `(NUM_POSITIONS, 3, NUM_TIMES)`.
    
    Notes
    -----
    This operates by first adjusting for timing errors between digitizers and
    then taking three independent vectors and finding the magnitude in the 
    machine directions. Each coil is at a specific location and to get 3 
    independent vectors we must use a coil in the 1, 2, and 3 directions. We 
    interpolate between coils in each unique direction by interpolating in 
    angle and magnitude.
    """
    logging.debug("Getting values, positions, and orientations for each coil set.")
    # Get the data from the working coils for each coil direction.
    db1_values = light_data.db_1[np.nonzero(light_data.db_1_working)]
    db1_positions = light_data.db_1_position[np.nonzero(light_data.db_1_working)]
    db1_orientations = light_data.db_1_orientation[np.nonzero(light_data.db_1_working)]
    db2_values = light_data.db_2[np.nonzero(light_data.db_2_working)]
    db2_positions = light_data.db_2_position[np.nonzero(light_data.db_2_working)]
    db2_orientations = light_data.db_2_orientation[np.nonzero(light_data.db_2_working)]
    db3a_values = light_data.db_3a[np.nonzero(light_data.db_3a_working)]
    db3a_positions = light_data.db_3a_position[np.nonzero(light_data.db_3a_working)]
    db3a_orientations = light_data.db_3a_orientation[np.nonzero(light_data.db_3a_working)]
    db3b_values = light_data.db_3b[np.nonzero(light_data.db_3b_working)]
    db3b_positions = light_data.db_3b_position[np.nonzero(light_data.db_3b_working)]
    db3b_orientations = light_data.db_3b_orientation[np.nonzero(light_data.db_3b_working)]

    # Now combine the 3 direction.
    db3_values = np.concatenate((db3a_values, db3b_values), axis=0)
    db3_positions = np.concatenate((db3a_positions, db3b_positions), axis=0)
    db3_orientations = np.concatenate((db3a_orientations, db3b_orientations), axis=0)

    all_values = [db1_values, db2_values, db3_values]
    all_positions = [db1_positions, db2_positions, db3_positions]
    all_orientations = [db1_orientations, db2_orientations, db3_orientations]

    # For each set of coils we need to remove the baseline.
    for dir_values in all_values:
        dir_values -= np.mean(dir_values[:, baseline_time_indeces], axis=1)[:, None]

    # Let's change the orientation of each coil to be in the machine cylindrical coordinates.
    # We also change the position depending on how the BRB is setup.
    logging.debug("Changing positions and orientations to cylindrical coordinates.")
    for dir_index, (dir_positions, dir_orientations) in enumerate(zip(all_positions, all_orientations)):
        for i in range(dir_orientations.shape[0]):
            position = Position()
            position.cylindrical = dir_positions[i]

            orientation = Vector(position)
            orientation.from_port(
                light_data.port.long_rad, light_data.port.lat_rad, light_data.port.alpha_rad, light_data.port.beta_rad, light_data.port.gamma_rad,
                dir_orientations[i, 0], dir_orientations[i, 1], dir_orientations[i, 2], light_data.port.clocking_rad
            )

            # TODO: Make the information that the insert is on and the TF coil is on a part of the MDSplus tree. Then remove the warnings.
            # We only need to adjust for the insert position if the probe is not on the equator.
            if insert_used and not np.isclose(light_data.port.lat_rad, 0):
                hemisphere = 'n' if light_data.port.lat_rad > 0 else 's'
                orientation.position.add_insert(hemisphere)
            
            # We then adjust for the lowered TF coil
            if tf_coil_lowered:
                orientation.position.to_tf_centered_coordinates()

            # Rewrite the position and orientation in the new cylindrical coordinates.
            all_positions[dir_index][i] = orientation.position.cylindrical
            all_orientations[dir_index][i] = orientation.cylindrical

            # Also renormalize the orientations. They can become slightly off.
            all_orientations[dir_index][i] /= np.linalg.norm(all_orientations[dir_index][i])

    num_points = 8 * 32
    interpolation_positions = np.array((
        sum(np.mean(positions[:, 0]) for positions in all_positions) / 3 * np.ones(num_points), # r
        sum(np.mean(positions[:, 1]) for positions in all_positions) / 3 * np.ones(num_points), # phi
        np.linspace(
            min(np.min(positions[:, 2]) for positions in all_positions), max(np.max(positions[:, 2]) for positions in all_positions), num=num_points
        ) # z
    )).T

    return get_db_machine_coords(all_values, all_positions, all_orientations, interpolation_kind=interpolation_kind, interpolation_positions=interpolation_positions)
    # return get_db_machine_coords(all_values, all_positions, all_orientations, interpolation_kind=interpolation_kind)

def plot_db_data(light_data : Light_Bdot1_Data, plot_raw=False):
    # align_timing(light_data)
    if not plot_raw:
        directions = ['r', '\phi', 'z']
        logging.warning("Assuming usage of insert and lowered TF coil.")
        positions, db_vals = get_db(light_data, insert_used=True, tf_coil_lowered=True)

        all_positions = [positions for _ in range(3)]
        all_db_vals = [db_vals[:, i] for i in range(3)]
    else:
        logging.debug("Plotting raw coil data.")
        directions = ['1', '2', '3']

        db_3_positions = np.concatenate((light_data.db_3a_position, light_data.db_3b_position), axis=0)
        sorted_3_indeces = np.argsort(db_3_positions[:, 2])
        all_positions = [light_data.db_1_position, light_data.db_2_position, db_3_positions[sorted_3_indeces]]

        flipped_db_1 = light_data.db_1
        flipped_db_1[:16] *= -1

        flipped_db_3b = light_data.db_3b
        flipped_db_3b[16:] *= -1

        all_db_vals = [flipped_db_1, light_data.db_2, np.concatenate((light_data.db_3a, flipped_db_3b))[sorted_3_indeces]]

    all_b_vals = [cumulative_trapezoid(db_vals, light_data.time, initial=0) for db_vals in all_db_vals]
    all_b_vals[2] += Helmholtz_Data(light_data.shot_number).b_h * 0.5 # This extra factor is to make sure that the reconnection layer is where the field swaps.
    all_b_vals[1] += TF_Coil_Data(light_data.shot_number).b_phi(light_data.position[0, 0])
    
    fig, axs = plt.subplots(2, 3)
    db_axs = axs[0]
    b_axs = axs[1]
    for i, dir in enumerate(directions):
        db_vals_to_plot = all_db_vals[i]
        db_cmesh = db_axs[i].pcolormesh(all_positions[i][:, 2], light_data.time, db_vals_to_plot.T, cmap='seismic', vmin=-10000, vmax=10000, shading='nearest')
        # db_cmesh = db_axs[i].pcolormesh(light_data.time, all_positions[i][:, 2], db_vals_to_plot, cmap='seismic', vmin=-np.max(np.abs(db_vals_to_plot)), vmax=np.max(np.abs(db_vals_to_plot)), shading='nearest')

        db_axs[i].set_xlabel(r'$z$ (m)')
        db_axs[i].set_ylabel(r'$t$ (s)')
        db_axs[i].set_title(r'$\partial_t B_{}$'.format(dir))
        fig.colorbar(db_cmesh, ax=db_axs[i])

        b_vals_to_plot = all_b_vals[i]

        b_cmesh = b_axs[i].pcolormesh(all_positions[i][:, 2], light_data.time, b_vals_to_plot.T, cmap='seismic', vmin=-np.max(np.abs(b_vals_to_plot)), vmax=np.max(np.abs(b_vals_to_plot)), shading='nearest')

        b_axs[i].set_xlabel(r'$z$ (m)')
        b_axs[i].set_ylabel(r'$t$ (s)')
        b_axs[i].set_title(r'$B_{}$'.format(dir))
        fig.colorbar(b_cmesh, ax=b_axs[i])

        db_axs[i].contour(all_positions[i][:, 2], light_data.time, all_b_vals[2].T, levels=[0], colors=['green'])
        b_axs[i].contour(all_positions[i][:, 2], light_data.time, all_b_vals[2].T, levels=[0], colors=['green'])

    fig.suptitle('Shot #{}: Lightsaber Data'.format(light_data.shot_number))
    return fig, axs


if __name__ == "__main__":
    # logging.basicConfig(level=logging.DEBUG)
    # light_data = Light_Bdot1_Data(60403, time_index_range=(65000, 65150))
    # plot_db_data(light_data)

    vac_shots = [60099, 60106, 60163, 60179, 60186, 60195, 60202, 60209, 60218, 60225]
    additional_vac_shots = [59852, 59858, 59864, 59870, 59882, 59883, 59898, 59905, 59908, 59921]
    rotated_shots = list(range(60421, 60429))
    plasma_shots = [60095, 60104, 60167, 60176, 60182, 60189, 60197, 60205, 60213, 60219]
    shots = [60196]
    r_30_w_tf_shots = [59760, 59761, 59762, 59763, 59764, 59765]
    # shots = range(60366, 60380)
    for shot in tqdm.tqdm(r_30_w_tf_shots):
        light_data = Light_Bdot1_Data(shot, time_index_range=(65000, 65170))
        # plot_db_data(light_data, plot_raw=True)
        plot_db_data(light_data, plot_raw=False)
    plt.show()
