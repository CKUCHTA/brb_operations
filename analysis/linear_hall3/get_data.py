"""Get data about the linear_hall3 probe from the MDSplus tree."""
from modules.generic_get_data import Get, Data, lazy_get, Port
import numpy as np
import logging


class Linear_Hall3_Data(Data):
    def __init__(self, tree, *args, **kwargs):
        """
        Hold all the data for the linear_hall3 probe in one class for easier access and reduced network connections.

        Notes
        -----
        To get data, call the data without the underscore treating it as an attribute.
        """
        super().__init__(tree, [], [], *args, **kwargs)
        self.NUM_PROBES = 15

    @lazy_get
    def b_x(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured B_x of combined hall probes. Of shape `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(Get('\\linear_hall3_bx'))

    @lazy_get
    def b_y(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured B_y of combined hall probes. Of shape `(NUM_PROBES, NUM_TIMES)`
        """
        return self.get(Get('\\linear_hall3_by'))

    @lazy_get
    def b_z(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured B_z of combined hall probes. Of shape `(NUM_PROBES, NUM_TIMES)`
        """
        return self.get(Get('\\linear_hall3_bz'))

    @lazy_get
    def b_1(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured B_1 of combined hall probes. Of shape `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(
            Get(
                '[' + ', '.join('DATA(magnetics.linear_hall3.hp_{probe_num}.axis_1.B)'.format(probe_num=str(num).zfill(2)) for num in range(1, self.NUM_PROBES + 1)) + ']',
                name='linear_hall3_b1'
            )
        )

    @lazy_get
    def b_2(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured B_2 of combined hall probes. Of shape `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(
            Get(
                '[' + ', '.join('DATA(magnetics.linear_hall3.hp_{probe_num}.axis_2.B)'.format(probe_num=str(num).zfill(2)) for num in range(1, self.NUM_PROBES + 1)) + ']',
                name='linear_hall3_b2'
            )
        )

    @lazy_get
    def b_3(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of measured B_3 of combined hall probes. Of shape `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(
            Get(
                '[' + ', '.join('DATA(magnetics.linear_hall3.hp_{probe_num}.axis_3.B)'.format(probe_num=str(num).zfill(2)) for num in range(1, self.NUM_PROBES + 1)) + ']',
                name='linear_hall3_b3'
            )
        )
    
    @lazy_get
    def time(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of time values. Of shape `(NUM_TIMES,)`.
        """
        return self.get(Get('dim_of(\\linear_hall3_bx)', name='linear_hall3_time'))
