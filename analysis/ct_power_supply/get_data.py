from modules.shot_loader import get_remote_shot_tree
from modules.generic_get_data import Get, Data, lazy_get
import numpy as np
import logging


# TODO: Change how time get calls are made so that it uses a node that is actually connected.
class CT_Power_Supply(Data):
    def __init__(self, tree, i_main1=False, i_main2=False, i_preion1=False, i_preion2=False, time=False, *args, **kwargs):
        """
        Lazily get the CT Power Supply data from the MDSplus tree.
        
        params:
        tree                = MDSplus tree connection object.
        """
        variable_booleans = [i_main1, i_main2, i_preion1, i_preion2, time]
        self.get_calls = [
            Get("\\ct_ps1_i_main"),
            Get("\\ct_ps2_i_main"),
            Get("\\ct_ps1_i_preion"),
            Get("\\ct_ps2_i_preion"),
            Get("dim_of(\\ct_ps2_i_main)")
        ]
        self._i_main1, self._i_main2, self._i_preion1, self._i_preion2, self._time = super().__init__(tree, variable_booleans, self.get_calls, *args, **kwargs)

    @lazy_get
    def i_main1(self):
        """
        Returns
        -------
        np.array[float]
            Array of current values for the main power supply in Amps.
        """
        if self.shot_number >= 64917:
            logging.warning("Using current from cell 1 card 4 channels 13 and 14 for CT PS1 current.")
            channel_13 = self.get(Get("\\c1_4_13"))
            channel_14 = self.get(Get("\\c1_4_14"))

            factor = 1983600

            baseline = np.mean((channel_13 - channel_14)[1000:5000])
            return (channel_13 - channel_14 - baseline) * factor
        else:
            return self.get(self.get_calls[0])

    @lazy_get
    def i_main2(self):
        return self.get(self.get_calls[1])

    @lazy_get
    def i_preion1(self):
        return self.get(self.get_calls[2])

    @lazy_get
    def i_preion2(self):
        return self.get(self.get_calls[3])

    @lazy_get
    def firing_index(self):
        """
        Get the data index when the CT power supplies triggered.
        
        Returns
        -------
        np.array[int]
            Array of two integers for each power supply. If `-1` then did not fire.
        """
        non_firing_range = range(0, 10000)
        # Get the mean and standard deviation far before firing.
        means = np.array([np.mean(current[non_firing_range]) for current in [self.i_main1, self.i_main2]])
        stds = np.array([np.std(current[non_firing_range]) for current in [self.i_main1, self.i_main2]])

        num_stds = 10

        # Find the first point at which the current goes over the number of stds.
        result = np.argmax(np.array([self.i_main1, self.i_main2]) > (means + num_stds * stds)[:, None], axis=1).astype(int)
        return np.where(result == 0, -1, result)

    @lazy_get
    def firing_time(self):
        return np.where(self.firing_index == -1, -1, self.time[self.firing_index])

    @lazy_get
    def time(self):
        if self.shot_number >= 64917:
            logging.warning("Using time from cell 1 card 4 channel 13 for CT PS1 time.")
            return self.get(Get("dim_of(\\c1_4_13)"))
        else:
            return self.get(self.get_calls[4])
