import numpy as np
import scipy.integrate as integrate
import matplotlib.pyplot as plt
import logging
import tqdm
import astropy.units as u


def _area_func(x, tip_radius, shell_radius, tip_height, shell_height, theta):
    return np.cos(theta) * (tip_radius**2 - x**2)**0.5 + tip_height * np.sin(theta) - np.minimum(np.cos(theta) * (tip_radius**2 - x**2)**0.5 + tip_height * np.sin(theta),
        np.maximum(-np.cos(theta) * (tip_radius**2 - x**2)**0.5, -np.cos(theta) * (shell_radius**2 - x**2)**0.5 + shell_height * np.sin(theta))
    )


def _cap_area_func(x, tip_radius, shell_radius, tip_height, shell_height, theta):
    return np.cos(theta) * (tip_radius**2 - x**2)**0.5 + tip_height * np.sin(theta) - np.minimum(
        np.cos(theta) * (tip_radius**2 - x**2)**0.5 + tip_height * np.sin(theta),
        np.maximum(
            -np.cos(theta) * (tip_radius**2 - x**2)**0.5 + tip_height * np.sin(theta), 
            -np.cos(theta) * (shell_radius**2 - x**2)**0.5 + shell_height * np.sin(theta)
        )
    )


def _side_area_func(x, tip_radius, shell_radius, tip_height, shell_height, theta):
    return -np.cos(theta) * (tip_radius**2 - x**2)**0.5 + tip_height * np.sin(theta) - np.minimum(
        -np.cos(theta) * (tip_radius**2 - x**2)**0.5 + tip_height * np.sin(theta),
        np.maximum(
            -np.cos(theta) * (tip_radius**2 - x**2)**0.5, 
            -np.cos(theta) * (shell_radius**2 - x**2)**0.5 + shell_height * np.sin(theta)
        )
    )


def cap_projected_area(tip_radius, shell_radius, tip_height, shell_height, theta):
    """Calculate the area of the cap projected from any angle.
    
    Parameters
    ----------
    tip_radius, shell_radius, tip_height, shell_height : `float` or `astropy.units.Quantity`
        The core and shell radii and height.
    theta : `numpy.ndarray[float]` or `astropy.units.Quantity`
        Array of theta values relative to the tip.
    """
    if isinstance(tip_radius, u.Quantity):
        r = tip_radius.to(u.m).value
        R = shell_radius.to(u.m).value
        h = tip_height.to(u.m).value
        H = shell_height.to(u.m).value
        th = theta.to(u.rad).value

        was_quantity = True
    else:
        r = tip_radius
        R = shell_radius
        h = tip_height
        H = shell_height
        th = theta

        was_quantity = False

    projected_area = np.zeros(th.shape)

    full_vs_bottom_theta_boundary = np.arctan((R - r) / (H - h))
    bottom_vs_top_theta_boundary = np.arctan((R**2 - r**2)**0.5 / (H - h))
    top_vs_none_theta_boundary = np.arctan((R + r) / (H - h))
    
    projected_area[th <= full_vs_bottom_theta_boundary] = np.pi * r**2 * np.cos(th[th <= full_vs_bottom_theta_boundary])

    a = r
    b = R
    c = (H - h) * np.tan(th)
    shell_cap_x_intersection = (2 * a**2 * b**2 + 2 * a**2 * c**2 + 2 * b**2 * c**2 - a**4 - b**4 - c**4)**0.5 / (2 * c)

    bottom_cutoff_mask = (full_vs_bottom_theta_boundary < th) & (th < bottom_vs_top_theta_boundary)
    x = shell_cap_x_intersection[bottom_cutoff_mask]
    bottom_cutoff_theta = th[bottom_cutoff_mask]
    projected_area[bottom_cutoff_mask] = (
        x * ((r**2 - x**2)**0.5 + (R**2 - x**2)**0.5) * np.cos(bottom_cutoff_theta) + 
        r**2 * np.arctan(x / (r**2 - x**2)**0.5) * np.cos(bottom_cutoff_theta) +
        R**2 * np.arctan(x / (R**2 - x**2)**0.5) * np.cos(bottom_cutoff_theta) + 
        2 * (h - H) * x * np.sin(bottom_cutoff_theta)
    ) + 2 * np.cos(bottom_cutoff_theta) * (r**2 * np.pi / 2 - (x * (r**2 - x**2)**0.5 + r**2 * np.arctan(x / (r**2 - x**2)**0.5)))

    # `x` is the same for when the top is partially cutoff.
    top_cutoff_mask = (bottom_vs_top_theta_boundary < th) & (th < top_vs_none_theta_boundary)
    x = shell_cap_x_intersection[top_cutoff_mask]
    top_cutoff_theta = th[top_cutoff_mask]
    projected_area[top_cutoff_mask] = (
        x * ((r**2 - x**2)**0.5 + (R**2 - x**2)**0.5) * np.cos(top_cutoff_theta) + 
        r**2 * np.arctan(x / (r**2 - x**2)**0.5) * np.cos(top_cutoff_theta) +
        R**2 * np.arctan(x / (R**2 - x**2)**0.5) * np.cos(top_cutoff_theta) + 
        2 * (h - H) * x * np.sin(top_cutoff_theta)
    )

    if was_quantity:
        return projected_area * u.m**2
    else:
        return projected_area


def projected_area(tip_radius, shell_radius, tip_height, shell_height, theta, area_function=_area_func):
    func = lambda x: area_function(x, tip_radius, shell_radius, tip_height, shell_height, theta)
    result, _ = integrate.quad(func, -tip_radius, tip_radius, epsabs=10**-10)
    return result


def total_area(tip_radius, shell_radius, tip_height, shell_height):
    """Integrate over all `theta` to get the total area of the tip."""
    if isinstance(tip_radius, u.Quantity):
        factor = u.m
    else:
        # Assume units are in meters.
        factor = 1

    if np.isclose(tip_radius, 0.0002921 * factor) and np.isclose(shell_radius, 0.0008255 * factor) and \
        np.isclose(tip_height, 0.003 * factor) and np.isclose(shell_height, 0.004 * factor):
        return 9.944130050911038e-08 * factor**2
    else:
        logging.warning("Passed tip parameters were not the default so recalculating the total area. This is much slower.")
        func = lambda x, theta: 2 * np.sin(theta) * (
            np.cos(theta) * _cap_area_func(x * u.m, tip_radius, shell_radius, tip_height, shell_height, theta) + 
            np.sin(theta) * (1 - (x * u.m)**2 / tip_radius**2)**0.5 * _side_area_func(x * u.m, tip_radius, shell_radius, tip_height, shell_height, theta)
        ).to(u.m).value
        result = 2 * np.pi * integrate.dblquad(func, 0, max_theta(tip_radius, shell_radius, tip_height, shell_height, cap=True).value, 0, tip_radius.to(u.m).value)[0] * u.m**2
        # We divide by pi because if we were to calculate this area for a flat plane of unit area we would get pi so normalize by that.
        result /= np.pi
        return result


def max_theta(core_radius, shell_radius, core_height, shell_height, cap=True):
    """Calculate the maximum theta value to integrate to for the cap or side.
    
    Returns
    -------
    theta_max : `float`
    """
    if cap:
        theta_max_cap = np.arctan((shell_radius + core_radius) / (shell_height - core_height))
        return theta_max_cap
    else:
        theta_max_side = np.arctan((shell_radius**2 - core_radius**2)**0.5 / (shell_height - core_height))
        return theta_max_side


def plot_areas(vary_retractions=False, end_vs_side=False):
    tip_radius = .2553 # mm
    shell_radius = .71755 # mm
    tip_height = 2.54 # mm
    shell_height = 2.54 # mm

    theta_values = np.linspace(0, np.pi / 2, num=250)
    if vary_retractions:
        tip_retractions = np.linspace(0, 3, num=7)

        areas = np.zeros((tip_retractions.size, theta_values.size))

        logging.debug("Getting areas.")
        for i in tqdm.tqdm(range(tip_retractions.size)):
            retraction = tip_retractions[i]
            for j, theta in enumerate(theta_values):
                areas[i, j] = projected_area(tip_radius, shell_radius, tip_height, shell_height + retraction, theta)

        logging.debug("Plotting areas.")
        cmap = plt.get_cmap('viridis', lut=tip_retractions.size)
        for i in range(tip_retractions.size):
            plt.plot(theta_values, areas[i], label=tip_retractions[i], color=cmap(i))

        plt.legend(title="Retraction (mm)")
    elif end_vs_side:
        logging.debug("Getting areas.")
        total_area = np.zeros_like(theta_values)
        end_area = np.zeros_like(theta_values)
        side_area = np.zeros_like(theta_values)

        for i, theta in enumerate(tqdm.tqdm(theta_values, desc="Theta values")):
            total_area[i] = projected_area(tip_radius, shell_radius, tip_height, shell_height, theta, area_function=_area_func)
            end_area[i] = projected_area(tip_radius, shell_radius, tip_height, shell_height, theta, area_function=_cap_area_func)
            side_area[i] = projected_area(tip_radius, shell_radius, tip_height, shell_height, theta, area_function=_side_area_func)

        logging.debug("Plotting areas.")
        plt.plot(theta_values, total_area, label='Total')
        plt.plot(theta_values, end_area, label='End')
        plt.plot(theta_values, side_area, label='Side')

        plt.legend()

    plt.xlabel("Angle")
    plt.xticks(np.linspace(0, np.pi / 2, num=2), ["0", "pi / 2"])
    plt.xlim(0, np.pi / 2)

    plt.ylabel("Area (mm^2)")

    plt.title("Projected Tip Area vs. Angle")

    plt.show()


if __name__=="__main__":
    logging.basicConfig(level=logging.DEBUG)

    # plot_areas(end_vs_side=True)
    from pa_probe1.get_data import PA_Probe1_Data
    pa_data = PA_Probe1_Data(0)
    print(f'{pa_data.core_tip_radius = }\n{pa_data.shell_tip_radius = }\n{pa_data.core_tip_height = }\n{pa_data.shell_tip_height = }')
    print(total_area(pa_data.core_tip_radius * u.m, pa_data.shell_tip_radius * u.m, pa_data.core_tip_height * u.m, pa_data.shell_tip_height * u.m).to(u.m**2))
