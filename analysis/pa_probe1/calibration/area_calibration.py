import numpy as np
from pa_probe1.analyze_iv_curve.shot_information import *
from pa_probe1.get_data import PA_Probe1_Data
from pa_probe1.calibration.manual_get_data import ipc, ips
from itertools import combinations
from scipy.optimize import differential_evolution
import logging


def calculate_abs_differences(ip_values):
    """
    Calculate all the absolute differences between the tips on the 3rd axis.

    Parameters
    ----------
    ip_values : np.array[float]
        4D-array of shape `(NUM_SHOTS, COMPARISON_SETS, TIPS_TO_COMPARE, NUM_TIMES)`.

    Returns
    -------
    total_differences : np.array[float]
        2D-array of shape `(NUM_SHOTS, NUM_TIMES)`.
    """
    total_differences = np.zeros((ip_values.shape[0], ip_values.shape[3]))
    for i in range(ip_values.shape[2] - 1):
        for j in range(i + 1, ip_values.shape[2]):
            total_differences += np.sum(abs(ip_values[:, :, i] - ip_values[:, :, j]), axis=1)

    return total_differences

def get_all_relative_areas(relative_areas):
    """
    Calculate the relative areas for all the tips with the constraint that the product of all the relative areas is one.

    Parameters
    ----------
    relative_areas : np.array[float]
        1D-array of shape `(NUM_TIPS - 1,)`. We calculate the last relative area by forcing the product to be one.
    """
    final_relative_area = 1 / np.product(relative_areas)
    return np.append(relative_areas, final_relative_area)

def cost_function(relative_areas, uncalibrated_ip_values, clocking_deg, shot_index_pairs_to_compare):
    """
    Calculate the cost function by comparing the probe current at different rotations.

    Parameters
    ----------
    relative_areas : np.array[float]
        1D-array of shape `(NUM_TIPS - 1,)`.
    uncalibrated_ip_values : np.array[float]
        3D-array of shape `(NUM_SHOTS, NUM_TIPS, NUM_TIMES)`.
    clocking_deg : np.array[float]
        1D-array of shape `(NUM_SHOTS,)` containing the clocking angle in degrees for each shot.
    shot_index_pairs_to_compare : np.array[int]
        2D-array of shape `(NUM_PAIRS, 2)` containing the indeces to compare.
    """
    all_areas = get_all_relative_areas(relative_areas)
    ip_values = uncalibrated_ip_values / all_areas[np.newaxis, :, np.newaxis]
    # Array of shape `(4, COMPARISON_SETS, TIPS_TO_COMPARE)`. The first index goes as `(0, 90, 180, 270)`.
    int_multiple_of_90_tip_indeces = np.array([
        [
            [0, 9],
            [1, 10],
            [2, 11],
            [3, 6],
            [4, 7],
            [5, 8],
        ],
        [
            [0, 3],
            [1, 4],
            [2, 5],
            [6, 9],
            [7, 10],
            [8, 11],
        ],
        [
            [0, 9],
            [1, 10],
            [2, 11],
            [3, 6],
            [4, 7],
            [5, 8],
        ],
        [
            [0, 3],
            [1, 4],
            [2, 5],
            [6, 9],
            [7, 10],
            [8, 11],
        ],
    ])
    # Array of shape `(4, COMPARISON_SETS, TIPS_TO_COMPARE)`. The first index goes as `(45, 135, 225, 315)`.
    half_int_multiple_of_90_tip_indeces = np.array([
        [
            [3, 4, 5, 9, 10, 11]
        ],
        [
            [0, 1, 2, 6, 7, 8]
        ],
        [
            [3, 4, 5, 9, 10, 11]
        ],
        [
            [0, 1, 2, 6, 7, 8]
        ],
    ])
    
    total_cost = np.sum(np.where(
        (clocking_deg % 90)[:, np.newaxis] == 0, 
        calculate_abs_differences(np.diagonal(ip_values[:, int_multiple_of_90_tip_indeces[clocking_deg // 90]]).transpose([3, 0, 1, 2])),
        calculate_abs_differences(np.diagonal(ip_values[:, half_int_multiple_of_90_tip_indeces[clocking_deg // 90]]).transpose([3, 0, 1, 2])),
    ))

    # Array of shape `(4, TIPS_TO_COMPARE)`. The first index goes as `(0, 90, 180, 270)` or as `(45, 135, 225, 315)`.
    between_shot_tip_indeces = np.array([
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        [3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2],
        [6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5],
        [9, 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8],
    ])

    total_cost += np.sum(abs(
        ip_values[shot_index_pairs_to_compare[:, 0][:, np.newaxis], between_shot_tip_indeces[clocking_deg[shot_index_pairs_to_compare[:, 0]] // 90]] - \
        ip_values[shot_index_pairs_to_compare[:, 1][:, np.newaxis], between_shot_tip_indeces[clocking_deg[shot_index_pairs_to_compare[:, 1]] // 90]]
    ))
    with np.printoptions(precision=3, suppress=True):
        logging.debug("Areas={}\t\tCost={}".format(all_areas, total_cost))

    return total_cost

def testing_values():
    """
    Test whether the calibration gives back the correct result using test data.
    """
    logging.info("Testing area calibration method.")
    NUM_TIPS = 12
    NUM_TIMES = 20
    times = np.linspace(0, 2 * np.pi, num=NUM_TIMES)
    most_correct_areas = np.linspace(0.8, 1.2, num=NUM_TIPS - 1)
    correct_areas = get_all_relative_areas(most_correct_areas)
    
    clocking_angles = np.array([0, 90, 180, 270, 45, 135, 225, 315])
    uncalibrated_ip_values = np.ones((clocking_angles.size, NUM_TIPS, NUM_TIMES))
    for i, clocking in enumerate(clocking_angles):
        if clocking % 90 == 0:
            sin_tips = (np.array([0, 1, 2, 9, 10, 11]) + 3 * (clocking // 90)) % NUM_TIPS
            cos_tips = (np.array([3, 4, 5, 6, 7, 8]) + 3 * (clocking // 90)) % NUM_TIPS
        else:
            sin_tips = (np.array([0, 1, 2]) + 3 * (clocking // 90)) % NUM_TIPS
            cos_tips = (np.array([6, 7, 8]) + 3 * (clocking // 90)) % NUM_TIPS
        uncalibrated_ip_values[i, sin_tips, :] = np.sin(times)[np.newaxis, :]
        uncalibrated_ip_values[i, cos_tips, :] = np.cos(times)[np.newaxis, :]

    uncalibrated_ip_values *= correct_areas[np.newaxis, :, np.newaxis]
    shot_index_pairs_to_compare = np.array([
        [0, 1], [0, 2], [0, 3], [1, 2], [1, 3], [2, 3], [4, 5], [4, 6], [4, 7], [5, 6], [5, 7], [6, 7]
    ])

    logging.debug("Optimizing areas")
    result = differential_evolution(
        cost_function,
        [(0.5, 2) for _ in range(NUM_TIPS - 1)],
        (uncalibrated_ip_values, clocking_angles, shot_index_pairs_to_compare),
        x0=np.ones(NUM_TIPS - 1),
    )
    logging.info("Final areas:   {}".format(get_all_relative_areas(result.x)))
    logging.info("Correct areas: {}".format(correct_areas))
    logging.info("Area diff:     {}".format(get_all_relative_areas(result.x) - correct_areas))


def main(core=True):
    logging.info("Running area calibration for {}.".format('core' if core else 'shell'))
    load_filepath = "../analysis_codes/data/pa_probe1/{}.mat"
    comparison_time_index_range = (16000, 17000)
    NUM_TIPS = 12

    all_shots = helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_0 + \
        helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_45 + \
        helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_90 + \
        helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_135 + \
        helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_180 + \
        helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_225 + \
        helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_0_clocking_0 + \
        helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_0_clocking_90

    pa_data_objects = [PA_Probe1_Data(shot, time_index_range=comparison_time_index_range, load_filepath=load_filepath.format(shot)) for shot in all_shots]
    logging.info("Getting clocking angles.")
    clocking_angles = np.array([pa_data.port.clocking_deg for pa_data in pa_data_objects], dtype=int)
    uncalibrated_ip_values = np.zeros((len(all_shots), NUM_TIPS, comparison_time_index_range[1] - comparison_time_index_range[0] + 1))
    logging.info("Getting iprobe values.")
    for i, pa_data in enumerate(pa_data_objects):
        for j, tip in enumerate(range(1, NUM_TIPS + 1)):
            uncalibrated_ip_values[i, j] = ipc(pa_data, tip, 22070, 1000) if core else ips(pa_data, tip, 22070, 1000)

    shot_index_pairs_to_compare = []
    logging.info("Calculating shot number combinations.")
    # Organize the shots into sets to compare within.
    for comparable_shot_sets in [
        [
            helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_0,
            helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_90,
            helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_180
        ],
        [
            helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_45,
            helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_135,
            helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_225
        ],
        [
            helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_0_clocking_0,
            helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_0_clocking_90,
        ],
    ]:
        for shot_set1, shot_set2 in combinations(comparable_shot_sets, 2):
            for shot1 in shot_set1:
                shot1_index = all_shots.index(shot1)
                for shot2 in shot_set2:
                    shot2_index = all_shots.index(shot2)

                    # If the two shots have a bias difference of less than 5 volts then compare their iprobe values.
                    if abs(pa_data_objects[shot1_index].vbias - pa_data_objects[shot2_index].vbias) <= 5:
                        shot_index_pairs_to_compare.append([shot1_index, shot2_index])
    shot_index_pairs_to_compare = np.array(shot_index_pairs_to_compare)

    logging.info("Saving PA data objects.")
    for pa_data in pa_data_objects:
        pa_data.save(load_filepath.format(pa_data.shot_number))

    logging.info("Optimizing areas.")
    result = differential_evolution(
        cost_function,
        [(0.5, 2) for _ in range(NUM_TIPS - 1)],
        (uncalibrated_ip_values, clocking_angles, shot_index_pairs_to_compare),
        x0=np.ones(NUM_TIPS - 1),
    )
    logging.info("Found optimized areas: {}".format(get_all_relative_areas(result.x)))


if __name__ == "__main__":
    # Area calibrations can be found in `pa_probe1.calibration.manual_get_data`.
    logging.basicConfig(level=logging.DEBUG)
    main(core=False)
    # testing_values()

