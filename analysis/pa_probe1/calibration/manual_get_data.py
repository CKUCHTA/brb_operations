"""Manually get the probe currents and voltages instead of using the precalculated MDSplus values."""
from modules.generic_get_data import Get
import numpy as np


CORE_AREAS = np.array([0.95400808, 1.26068344, 0.84746666, 1.07178893, 1.19954292, 1.08513137, 1.00535084, 1.01338479, 0.83488243, 1.0361139,  0.90225493, 0.88441844])
CORE_AREAS = np.concatenate([CORE_AREAS[8:], CORE_AREAS[:8]])
SHELL_AREAS = np.array([1.01740613, 0.99640098, 0.98634565, 1.02619776, 0.9903032,  1.01852656, 1.01868074, 0.98856075, 1.00232706, 1.02824074, 0.95151605, 0.97838306])
R_DIGGND = (1 / 82 + 1 / 500)**-1
R_SENSE = 995
R_DIVIDER = 22070


def tip_to_str(tip_number):
    """Change a tip number to a string."""
    return str(tip_number).zfill(2)

def calculate_r_diggnd(r_2, r_digitizer):
    """Calculate the resistance between from the digitizer measurement to the chassis.
    
    Parameters
    ----------
    r_2 : `float`
        Resistance of divider resistor between digitizer and chassis.
    r_digitizer : `float`
        Internal resistance of the digitizer channel.
    """
    return (1 / r_2 + 1 / r_digitizer)**-1

def get_idc(pa_data, tip, r_diggnd=R_DIGGND):
    """Current measured by core signal digitizer."""
    return get_vdc(pa_data, tip) / r_diggnd

def get_vdc(pa_data, tip):
    """Voltage measured by core signal digitizer."""
    tip_str = tip_to_str(tip)
    return getattr(pa_data, 'vdc_{}'.format(tip_str), pa_data.get(Get("\\pa1_{}_csv".format(tip_str))))

def get_idn(pa_data, tip, r_diggnd=R_DIGGND):
    """Current measured by core noise digitizer."""
    return get_vdn(pa_data, tip) / r_diggnd

def get_vdn(pa_data, tip):
    """Voltage measured by core noise digitizer."""
    tip_str = tip_to_str(tip)
    return getattr(pa_data, 'vdn_{}'.format(tip_str), pa_data.get(Get("\\pa1_{}_cnv".format(tip_str))))

def get_ids(pa_data, tip, r_diggnd=R_DIGGND):
    """Current measured by shell signal digitizer."""
    return get_vds(pa_data, tip) / r_diggnd

def get_vds(pa_data, tip):
    """Voltage measured by shell signal digitizer."""
    tip_str = tip_to_str(tip)
    return getattr(pa_data, 'vds_{}'.format(tip_str), pa_data.get(Get("\\pa1_{}_ssv".format(tip_str))))

def get_idm(pa_data, tip, r_diggnd=R_DIGGND):
    """Current measured by shell noise digitizer."""
    return get_vdm(pa_data, tip) / r_diggnd

def get_vdm(pa_data, tip):
    """Voltage measured by shell noise digitizer."""
    tip_str = tip_to_str(tip)
    return getattr(pa_data, 'vdm_{}'.format(tip_str), pa_data.get(Get("\\pa1_{}_snv".format(tip_str))))

def swap_resistances(relay_status, rc, rn):
    """Swap the resistances for the core and noise digitizers if the relay is on."""
    return np.where(relay_status, (rc, rn), (rn, rc))

def determine_shell_connected_to_core(pa_data, shell_connected_to_core=None):
    """
    Determine whether the shell is connected to the core for a specific shot.
    """
    if shell_connected_to_core is not None:
        return shell_connected_to_core
    elif pa_data.shot_number > 62826:
        return True
    else:
        return False

def ipc(
        pa_data, tip, 
        r_divider, r_sense, 
        shell_connected_to_core=None
    ):
    """Probe current for core divided by the core relative area."""
    return ipc_full(pa_data, tip, r_divider, r_divider, r_divider, r_divider, r_sense, r_sense, r_sense, r_sense, shell_connected_to_core=shell_connected_to_core)

def ipc_full(
        pa_data, tip, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        r_diggnd_s=R_DIGGND, r_diggnd_m=R_DIGGND, r_diggnd_c=R_DIGGND, r_diggnd_n=R_DIGGND,
        shell_connected_to_core=None
    ):
    r_diggnd_c, r_diggnd_n = swap_resistances(pa_data.noise_relay, r_diggnd_c, r_diggnd_n)
    ids = get_vds(pa_data, tip) / r_diggnd_s
    idm = get_vdm(pa_data, tip) / r_diggnd_m
    idc = get_vdc(pa_data, tip) / r_diggnd_c
    idn = get_vdn(pa_data, tip) / r_diggnd_n

    return ipc_func(
        ids, idm, idc, idn, 
        pa_data.noise_relay, CORE_AREAS[tip - 1], 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        shell_connected_to_core=determine_shell_connected_to_core(pa_data, shell_connected_to_core)
    )

def ipc_func(
        ids, idm, idc, idn, 
        relay_status, core_area, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        shell_connected_to_core=False
    ):
    rdc, rdn = swap_resistances(relay_status, uncorrected_rdc, uncorrected_rdn)
    rsc, rsn = swap_resistances(relay_status, uncorrected_rsc, uncorrected_rsn)

    if shell_connected_to_core:
        return ((idc * rsn * (rdc + rsc) - idn * rsc * (rdn + rsn) + ids * rds * (rsc - rsn)) / (rsc * rsn)) / core_area
    else:
        return (idc * (rdc / rsc + 1) + idm * (rdm / rsn - rdm / rsc) + idn * (-rdn / rsn - 1)) / core_area
        vc = (rsm * rdc / (rsm + rsc) * idc - rdm * idm) / (1 - rsm / (rsm + rsc))
        return rdc / (rsm + rsc) * (idc + vc / rds)

def ips(
        pa_data, tip, 
        r_divider, r_sense, 
        shell_connected_to_core=None
    ):
    """Probe current for shell divided by the shell relative area."""
    return ips_full(pa_data, tip, r_divider, r_divider, r_divider, r_divider, r_sense, r_sense, r_sense, r_sense, shell_connected_to_core=shell_connected_to_core)

def ips_full(
        pa_data, tip, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        r_diggnd_s=R_DIGGND, r_diggnd_m=R_DIGGND, r_diggnd_c=R_DIGGND, r_diggnd_n=R_DIGGND,
        shell_connected_to_core=None
    ):
    r_diggnd_c, r_diggnd_n = swap_resistances(pa_data.noise_relay, r_diggnd_c, r_diggnd_n)
    ids = get_vds(pa_data, tip) / r_diggnd_s
    idm = get_vdm(pa_data, tip) / r_diggnd_m
    idc = get_vdc(pa_data, tip) / r_diggnd_c
    idn = get_vdn(pa_data, tip) / r_diggnd_n

    return ips_func(
        ids, idm, idc, idn, 
        pa_data.noise_relay, SHELL_AREAS[tip - 1], 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        shell_connected_to_core=determine_shell_connected_to_core(pa_data, shell_connected_to_core)
    )

def ips_func(
        ids, idm, idc, idn, 
        relay_status, shell_area, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        shell_connected_to_core=False
    ):
    rdc, rdn = swap_resistances(relay_status, uncorrected_rdc, uncorrected_rdn)
    rsc, rsn = swap_resistances(relay_status, uncorrected_rsc, uncorrected_rsn)

    if shell_connected_to_core:
        return (-idc * rdc / rsc - idm * rdm / rss - idm * rsm / rss + idn * rdn * rsm / (rsn * rss) - 2 * idn * rdn / rsn + idn * rsm / rss - idn - ids * rds * rsm / (rsn * rss) + ids * rds / rss + 2 * ids * rds / rsn + ids * rds / rsc + ids) / shell_area
    else:
        return (idc * (rdc * rsm) / (rsc * rss) + idm * (-2 * (rdm * rsm) / (rsn * rss) - rdm / rss + rdm / rsn - (rdm * rsm) / (rsc * rss) - rsm / rss) + idn * (2 * (rdn * rsm) / (rsn * rss) - rdn / rsn + rsm / rss - 1) + ids * (rds / rss + 1)) / shell_area
        vc = (rsm * rdc / (rsm + rsc) * idc - rdm * idm) / (1 - rsm / (rsm + rsc))
        return rds / rss * (ids + vc / rds)

def im_full(
        pa_data, tip, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        r_diggnd_s=R_DIGGND, r_diggnd_m=R_DIGGND, r_diggnd_c=R_DIGGND, r_diggnd_n=R_DIGGND,
        shell_connected_to_core=None
    ):
    r_diggnd_c, r_diggnd_n = swap_resistances(pa_data.noise_relay, r_diggnd_c, r_diggnd_n)
    ids = get_vds(pa_data, tip) / r_diggnd_s
    idm = get_vdm(pa_data, tip) / r_diggnd_m
    idn = get_vdn(pa_data, tip) / r_diggnd_n

    return im_func(
        ids, idm, idn, 
        pa_data.noise_relay, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        shell_connected_to_core=determine_shell_connected_to_core(pa_data, shell_connected_to_core)
    )

def im_func(
        ids, idm, idn, 
        relay_status, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        shell_connected_to_core=False
    ):
    rdc, rdn = swap_resistances(relay_status, uncorrected_rdc, uncorrected_rdn)
    rsc, rsn = swap_resistances(relay_status, uncorrected_rsc, uncorrected_rsn)

    if shell_connected_to_core:
        raise NotImplementedError("I_m is undefined when the shell is connected to the core.")
    else:
        return (idm*rdm + idm*rsm)/rsm

def in_full(
        pa_data, tip, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        r_diggnd_s=R_DIGGND, r_diggnd_m=R_DIGGND, r_diggnd_c=R_DIGGND, r_diggnd_n=R_DIGGND,
        shell_connected_to_core=None
    ):
    r_diggnd_c, r_diggnd_n = swap_resistances(pa_data.noise_relay, r_diggnd_c, r_diggnd_n)
    ids = get_vds(pa_data, tip) / r_diggnd_s
    idn = get_vdn(pa_data, tip) / r_diggnd_n

    return in_func(
        ids, idn, 
        pa_data.noise_relay, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        shell_connected_to_core=determine_shell_connected_to_core(pa_data, shell_connected_to_core)
    )

def in_func(
        ids, idn, 
        relay_status, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        shell_connected_to_core=False
    ):
    rdc, rdn = swap_resistances(relay_status, uncorrected_rdc, uncorrected_rdn)
    rsc, rsn = swap_resistances(relay_status, uncorrected_rsc, uncorrected_rsn)

    if shell_connected_to_core:
        return (idn * rdn + idn * rsn - ids * rds) / rsn
    else:
        return (idn*rdn + idn*rsn - ids*rds)/rsn

def vpc(
        pa_data, tip, 
        r_divider, r_sense, 
        r_wire_core, 
        shell_connected_to_core=None
    ):
    """Probe voltage for core."""
    return vpc_full(pa_data, tip, r_divider, r_divider, r_divider, r_divider, r_sense, r_sense, r_sense, r_sense, 0, 0, r_wire_core, 0, shell_connected_to_core=shell_connected_to_core)

def vpc_full(
        pa_data, tip, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        rws, rwm, rwc, rwn, 
        r_diggnd_s=R_DIGGND, r_diggnd_m=R_DIGGND, r_diggnd_c=R_DIGGND, r_diggnd_n=R_DIGGND,
        shell_connected_to_core=None
    ):
    r_diggnd_c, r_diggnd_n = swap_resistances(pa_data.noise_relay, r_diggnd_c, r_diggnd_n)
    ids = get_vds(pa_data, tip) / r_diggnd_s
    idm = get_vdm(pa_data, tip) / r_diggnd_m
    idc = get_vdc(pa_data, tip) / r_diggnd_c
    idn = get_vdn(pa_data, tip) / r_diggnd_n

    return vpc_func(
        ids, idm, idc, idn, 
        pa_data.noise_relay, pa_data.vbias, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        rws, rwm, rwc, rwn, 
        shell_connected_to_core=determine_shell_connected_to_core(pa_data, shell_connected_to_core)
    )

def vpc_func(
        ids, idm, idc, idn, 
        relay_status, vbias, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        rws, rwm, rwc, rwn, 
        shell_connected_to_core=False
    ):
    rdc, rdn = swap_resistances(relay_status, uncorrected_rdc, uncorrected_rdn)
    rsc, rsn = swap_resistances(relay_status, uncorrected_rsc, uncorrected_rsn)

    if shell_connected_to_core:
        return vbias + idc * rdc + idc * rdc * rwc / rsc + idc * rwc - idm * rdm - idm * rsm + idn * rdn * rsm / rsn + idn * rsm - ids * rds * rsm / rsn - ids * rds * rwc / rsc
    else:
        return vbias + idc * (rdc + rdc * rsm / rsc + rdc * rwc / rsc + rwc) + idm * (-2 * rdm * rsm / rsn - rdm - rdm * rsm / rsc - rdm * rwc / rsc - rsm) + idn * (2 * rdn * rsm / rsn + rsm)
        return vbias + (rsm + rsc + rwc) * ipc_full(pa_data, tip, rds, rdm, rdc, rdn, rss, rsm, rsc, rsn)

def vps(
        pa_data, tip, 
        r_divider, r_sense, 
        r_wire_shell, 
        shell_connected_to_core=None
    ):
    """Probe voltage for shell."""
    return vps_full(
        pa_data, tip, 
        r_divider, r_divider, r_divider, r_divider, 
        r_sense, r_sense, r_sense, r_sense, 
        r_wire_shell, 0, 0, 0, 
        shell_connected_to_core=shell_connected_to_core
    )

def vps_full(
        pa_data, tip, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        rws, rwm, rwc, rwn, 
        r_diggnd_s=R_DIGGND, r_diggnd_m=R_DIGGND, r_diggnd_c=R_DIGGND, r_diggnd_n=R_DIGGND,
        shell_connected_to_core=None
    ):
    r_diggnd_c, r_diggnd_n = swap_resistances(pa_data.noise_relay, r_diggnd_c, r_diggnd_n)
    ids = get_vds(pa_data, tip) / r_diggnd_s
    idm = get_vdm(pa_data, tip) / r_diggnd_m
    idc = get_vdc(pa_data, tip) / r_diggnd_c
    idn = get_vdn(pa_data, tip) / r_diggnd_n

    return vps_func(
        ids, idm, idc, idn, 
        pa_data.noise_relay, pa_data.vbias, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        rws, rwm, rwc, rwn, 
        shell_connected_to_core=determine_shell_connected_to_core(pa_data, shell_connected_to_core)
    )

def vps_func(
        ids, idm, idc, idn, 
        relay_status, vbias, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        rws, rwm, rwc, rwn, 
        shell_connected_to_core=False
    ):
    rdc, rdn = swap_resistances(relay_status, uncorrected_rdc, uncorrected_rdn)
    rsc, rsn = swap_resistances(relay_status, uncorrected_rsc, uncorrected_rsn)

    if shell_connected_to_core:
        return vbias - idc * rdc * rws / rsc - idm * rdm - idm * rdm * rws / rss - idm * rsm - idm * rsm * rws / rss + idn * rdn * rsm / rsn + idn * rdn * rsm * rws / (rsn * rss) - idn * rdn * rws / rsn + idn * rsm + idn * rsm * rws / rss - ids * rds * rsm / rsn - ids * rds * rsm * rws / (rsn * rss) + ids * rds + ids * rds * rws / rss + ids * rds * rws / rsn + ids * rds * rws / rsc + ids * rws
    else:
        return vbias + idc * (rdc * rsm / rsc + rdc * rsm * rws / (rsc * rss)) + ids * (rds + rds * rws / rss + rws) + idm * (-2 * rdm * rsm / rsn - 2 * rdm * rsm * rws / (rsn * rss) - rdm - rdm * rws / rss - rdm * rsm / rsc - rdm * rsm * rws / (rsc * rss) - rsm - rsm * rws / rss) + idn * (2 * rdn * rsm / rsn + 2 * rdn * rsm * rws / (rsn * rss) + rsm + rsm * rws / rss)
        return vbias + (rss + rws) * ips_full(pa_data, tip, rds, rdm, rdc, rdn, rss, rsm, rsc, rsn)
    
def vc(
        pa_data, tip, 
        r_divider, r_sense, 
        shell_connected_to_core=None
    ):
    """Rack ground voltage."""
    return vc_full(pa_data, tip, r_divider, r_divider, r_divider, r_divider, r_sense, r_sense, r_sense, r_sense, shell_connected_to_core=shell_connected_to_core)

def vc_full(
        pa_data, tip, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        r_diggnd_s=R_DIGGND, r_diggnd_m=R_DIGGND, r_diggnd_c=R_DIGGND, r_diggnd_n=R_DIGGND,
        shell_connected_to_core=None
    ):
    """Rack ground voltage."""
    r_diggnd_c, r_diggnd_n = swap_resistances(pa_data.noise_relay, r_diggnd_c, r_diggnd_n)
    ids = get_vds(pa_data, tip) / r_diggnd_s
    idm = get_vdm(pa_data, tip) / r_diggnd_m
    idc = get_vdc(pa_data, tip) / r_diggnd_c
    idn = get_vdn(pa_data, tip) / r_diggnd_n

    return vc_func(
        ids, idm, idc, idn, 
        pa_data.noise_relay, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        shell_connected_to_core=determine_shell_connected_to_core(pa_data, shell_connected_to_core)
    )

def vc_func(
        ids, idm, idc, idn, 
        relay_status, 
        rds, rdm, uncorrected_rdc, uncorrected_rdn, 
        rss, rsm, uncorrected_rsc, uncorrected_rsn, 
        shell_connected_to_core=False
    ):
    rdc, rdn = swap_resistances(relay_status, uncorrected_rdc, uncorrected_rdn)
    rsc, rsn = swap_resistances(relay_status, uncorrected_rsc, uncorrected_rsn)

    if shell_connected_to_core:
        return -idm * rdm - idm * rsm + idn * rdn * rsm / rsn + idn * rsm - ids * rds * rsm / rsn
    else:
        return idc * rdc * rsm / rsc - 2 * idm * rdm * rsm / rsn - idm * rdm - idm * rdm * rsm / rsc - idm * rsm + 2 * idn * rdn * rsm / rsn + idn * rsm
