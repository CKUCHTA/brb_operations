"""Load the shots we want to compare from the database."""
from pa_probe1.get_data import PA_Probe1_Data
from plasma_guns.get_data import Plasma_Guns_Data
import numpy as np
import logging
from tqdm import tqdm


class Shot:
    def __init__(self, shot_number, num_correct_guns, num_incorrect_guns, bias_voltage, clocking, tips_flipped):
        self.shot_number = shot_number
        self.num_correct_guns = num_correct_guns
        self.num_incorrect_guns = num_incorrect_guns
        self.bias_voltage = bias_voltage
        self.clocking = clocking
        self.tips_flipped = bool(tips_flipped)

    def __str__(self) -> str:
        return "Shot({shot}, correct_guns={correct}, incorrect_guns={incorrect}, vbias={vbias}, clocking={clocking}, tips_flipped={flipped})".format(
            shot=self.shot_number, correct=self.num_correct_guns, incorrect=self.num_incorrect_guns,
            vbias=self.bias_voltage, clocking=self.clocking, flipped=self.tips_flipped
        )

    def __repr__(self) -> str:
        return self.__str__()

class ShotInfo:
    def __init__(self):
        self.start_shot = 56013
        self.clocking_shots = [0] * (56089 - 56013) + [90] * (56165 - 56089) + [180] * (56245 - 56165) + [270] * (56331 - 56245)
        self.shot_list = []
    
    def __len__(self) -> int:
        return len(self.shot_list)

    def __getitem__(self, indeces) -> Shot:
        if isinstance(indeces, int):
            iterable_indeces = [indeces]
        elif isinstance(indeces, slice):
            start = indeces.start if indeces.start is not None else 0
            stop = indeces.stop if indeces.stop is not None else len(self)
            step = indeces.step if indeces.step is not None else 1
            iterable_indeces = range(start, stop, step)

        # Store all the actual indeces of the self.shot_list we are accessing.    
        list_indeces = []
        for index in iterable_indeces:
            if index >= self.start_shot:
                logging.info("Assuming index is actually shot number when indexing shot info since start shot is #{} and index is #{}.".format(self.start_shot, index))
                index -= self.start_shot
            list_indeces.append(index)

            # Append slots to the shot_list to get to the correct length.
            if index >= len(self.shot_list):
                self.shot_list += [None] * (index - len(self.shot_list) + 1)

            if self.shot_list[index] is None:
                shot = self.start_shot + index
                logging.debug("Getting shot info for shot #{}.". format(shot))
                gun_data = Plasma_Guns_Data(shot)
                pa_data = PA_Probe1_Data(shot, vbias=True, noise_relay=True)

                if index < len(self.clocking_shots):
                    clocking = self.clocking_shots[index]
                else:
                    logging.warning("Index does not have clocking information so setting clocking to 'None'.")
                    clocking = None

                self.shot_list[index] = Shot(
                    shot, np.count_nonzero(gun_data.correctly_firing_guns), np.count_nonzero(gun_data.firing_guns - gun_data.correctly_firing_guns),
                    pa_data.vbias, clocking, pa_data.noise_relay
                )

        if len(list_indeces) == 1:
            return self.shot_list[list_indeces[0]]
        else:
            return list(self.shot_list[i] for i in list_indeces)
