from pa_probe1.calibration.curve_analysis import get_fit_parameters
from pa_probe1.get_data import PA_Probe1_Data
from scipy.signal import medfilt
import numpy as np


if __name__=="__main__":
    shot_numbers = range(56716, 56727)
    pa_data_objects = [PA_Probe1_Data(shot) for shot in shot_numbers]

    shell_actarea = 0.002**2
    core_actarea = 0.0005**2
    data_index = 10000

    for tip_index in range(12):
        v_shell_vals = []
        i_shell_vals = []
        v_core_vals = []
        i_core_vals = []
        for data in pa_data_objects:
            for val_list, signal in zip([v_shell_vals, i_shell_vals, v_core_vals, i_core_vals], [data.vshell[tip_index], data.ishell[tip_index], data.vcore[tip_index], data.icore[tip_index]]):
                val_list.append(medfilt(signal, 11)[data_index])

        print("Tip #{}".format(tip_index + 1))
        print("Shell")
        get_fit_parameters(np.array(v_shell_vals), np.array(i_shell_vals), shell_actarea, 1, visualize=True)
        print("Core")
        get_fit_parameters(np.array(v_core_vals), np.array(i_core_vals), core_actarea, 1, visualize=True)
