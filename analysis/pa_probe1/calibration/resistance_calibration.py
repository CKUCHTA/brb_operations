"""
Calibrate the resistances of the PA Probe 1.

We calibrate by forcing `ipc` to be zero when the core and core noise are disconnected.
"""
import numpy as np
import scipy
from pa_probe1.get_data import PA_Probe1_Data
from pa_probe1.calibration import manual_get_data
import matplotlib.pyplot as plt
import matplotlib as mpl
import logging
import tqdm


logging.basicConfig(level=logging.INFO)
plot_raw_signal = False
plot_raw_signal_std = False
plot_mean_ipc = False
plot_covariance_matrix = False
use_relative_covariance = False
plot_cross_correlation_matrix = False
plot_probability_map = True

r_divider = 22070
r_sense = 1000

shots_with_disconnected_core_and_core_noise = {
    tuple(range(1, 7)): np.arange(62900, 62908),
    tuple(range(7, 13)): np.arange(62908, 62916),
}
num_shots_per_tip = np.zeros(PA_Probe1_Data.NUM_TIPS, dtype=int)
for tip_tuple, shots in shots_with_disconnected_core_and_core_noise.items():
    for tip in tip_tuple:
        num_shots_per_tip[tip - 1] = shots.size
    
# The time range when nothing is happening in the plasma.
plasma_equilibrium_time_indeces = (15000, 16460)
plasma_equilibrium_time_indeces = (15000, 15100)
NUM_TIME_POINTS = plasma_equilibrium_time_indeces[1] - plasma_equilibrium_time_indeces[0] + 1

# Create an empty array to hold the raw data.
# Ordered as [S, M, C, N].
id_array = np.zeros((4, np.max(num_shots_per_tip), PA_Probe1_Data.NUM_TIPS, NUM_TIME_POINTS))
ipc_array = np.zeros((np.max(num_shots_per_tip), PA_Probe1_Data.NUM_TIPS, NUM_TIME_POINTS))
relay_status = np.zeros((np.max(num_shots_per_tip), PA_Probe1_Data.NUM_TIPS), dtype=bool)

# Load the data into the arrays.
for set_index, (tips, shots) in enumerate(shots_with_disconnected_core_and_core_noise.items()):
    for shot_index, shot in enumerate(tqdm.tqdm(shots, desc=f"Loading probe data ({set_index + 1} / {len(shots_with_disconnected_core_and_core_noise)})")):
        data_filepath = f"../analysis_codes/data/pa_probe1/{shot}.mat"
        pa_data = PA_Probe1_Data(shot, time_index_range=plasma_equilibrium_time_indeces)

        for tip in tips:
            for data_index, function in enumerate([manual_get_data.get_ids, manual_get_data.get_idm, manual_get_data.get_idc, manual_get_data.get_idn]):
                id_array[data_index, shot_index, tip - 1] = function(pa_data, tip)
            ipc_array[shot_index, tip - 1] = manual_get_data.ipc(pa_data, tip, r_divider, r_sense, shell_connected_to_core=True)
            relay_status[shot_index, tip - 1] = pa_data.noise_relay
        
        pa_data.save(data_filepath)

# Plot the raw signals.
if plot_raw_signal:
    fig, axs = plt.subplots(4, PA_Probe1_Data.NUM_TIPS, figsize=(20, 10), sharex=True)

    for tip_index in range(PA_Probe1_Data.NUM_TIPS):
        for data_index, ax in enumerate(axs[:, tip_index]):
            ax.plot(id_array[data_index, :num_shots_per_tip[tip_index], tip_index].T, alpha=0.5)
            ax.set_title(f'Tip {tip_index + 1}')

            if tip_index == 0:
                ax.set_ylabel('Current [A]')
            if data_index == 0:
                ax.set_xlabel('Time [ms]')

    fig.suptitle('Raw Signal')

# Plot the standard deviations of the raw signal.
if plot_raw_signal_std:
    id_std = np.zeros((4, PA_Probe1_Data.NUM_TIPS))
    idc_minus_idn = id_array[2] - id_array[3]
    idc_minus_idn_std = np.zeros(PA_Probe1_Data.NUM_TIPS)
    for tip_index in range(PA_Probe1_Data.NUM_TIPS):
        id_std[:, tip_index] = np.std(id_array[:, :num_shots_per_tip[tip_index], tip_index] - np.mean(id_array[:, :num_shots_per_tip[tip_index], tip_index], axis=-1)[:, :, None], axis=(1, 2))
        idc_minus_idn_std[tip_index] = np.std(idc_minus_idn[:num_shots_per_tip[tip_index], tip_index] - np.mean(idc_minus_idn[:num_shots_per_tip[tip_index], tip_index]))

    # Create a barchart of the standard deviations.
    fig, ax = plt.subplots()

    labels = ['S', 'M', 'C', 'N', 'C - N']
    bar_width = 1 / (len(labels) + 1)
    for i, (label, data) in enumerate(zip(labels, [*id_std, idc_minus_idn_std])):
        ax.bar(np.arange(PA_Probe1_Data.NUM_TIPS) + i * bar_width, data, bar_width, label=label)

    ax.set_title('Standard Deviation of the Raw Signal')
    ax.legend()
    ax.set_xlabel('Tip')
    ax.set_ylabel('Standard Deviation [A]')
    ax.set_xticks(np.arange(PA_Probe1_Data.NUM_TIPS) + bar_width * (len(labels) / 2 - 0.5))
    ax.set_xticklabels(np.arange(1, PA_Probe1_Data.NUM_TIPS + 1))

# Create a barchart of the mean ipc values with relay on and off.
if plot_mean_ipc:
    fig, ax = plt.subplots()

    bar_width = 0.2
    labels = ['Relay On', 'Relay Off']
    ipc_mean = np.zeros((2, PA_Probe1_Data.NUM_TIPS))
    for tip in range(PA_Probe1_Data.NUM_TIPS):
        for shot in range(num_shots_per_tip[tip]):
            ipc_mean[int(relay_status[shot, tip]), tip] += ipc_array[shot, tip].mean() / num_shots_per_tip[tip]
        
    xtick_locations = np.arange(PA_Probe1_Data.NUM_TIPS) + bar_width * (len(labels) / 2 - 0.5)
    for location in xtick_locations:
        ax.axvline(location, color='black', alpha=0.2)
    for i, label in enumerate(labels):
        ax.bar(np.arange(PA_Probe1_Data.NUM_TIPS) + i * bar_width, ipc_mean[i], bar_width, label=label)

    ax.scatter(xtick_locations, np.mean(ipc_mean, axis=0), label='Relay On + Relay Off', color='black')
    ax.axhline(0, color='black')

    ax.set_title(r'Mean $I_{pc}$ Value')
    ax.legend()
    ax.set_xlabel('Tip')
    ax.set_ylabel(r'Mean $I_{pc}$ [A]')
    ax.set_xticks(xtick_locations)
    ax.set_xticklabels(np.arange(1, PA_Probe1_Data.NUM_TIPS + 1))

# For each tip create a covariance matrix of the `ids`, `idc`, and `idn` signals.
if plot_covariance_matrix:
    fig = plt.figure(figsize=(10, 10))

    gs = fig.add_gridspec(3, 5, width_ratios=[1, 1, 1, 1, 0.1], height_ratios=[1, 1, 1])

    covariance_matrices = []
    relative_covariance_matrices = []
    for tip_index in range(PA_Probe1_Data.NUM_TIPS):
        ids_data = id_array[0, :num_shots_per_tip[tip_index], tip_index]
        idc_data, idn_data = id_array[2:, :num_shots_per_tip[tip_index], tip_index]

        covariance_matrix = np.cov([ids_data.flatten(), idc_data.flatten(), idn_data.flatten()])
        covariance_matrices.append(covariance_matrix)
        relative_data = [(data / np.mean(data, axis=1)[:, None]**2).flatten() for data in (ids_data, idc_data, idn_data)]
        relative_covariance_matrix = np.cov(relative_data)
        relative_covariance_matrices.append(relative_covariance_matrix)

    if use_relative_covariance:
        norm = plt.Normalize(np.min(relative_covariance_matrices), np.max(relative_covariance_matrices))
    else:
        norm = plt.Normalize(np.min(covariance_matrices), np.max(covariance_matrices))

    cov_axs = [fig.add_subplot(gs[i, j]) for j in range(4) for i in range(3)]
    data_names = ['S', 'C', 'N']
    for tip_index, ax in zip(range(PA_Probe1_Data.NUM_TIPS), cov_axs):
        if use_relative_covariance:
            ax.pcolormesh(relative_covariance_matrices[tip_index], cmap='viridis', norm=norm)
        else:
            ax.pcolormesh(covariance_matrices[tip_index], cmap='viridis', norm=norm)
        ax.set_title(f'Tip {tip_index + 1}')
        ax.set_aspect('equal')
        ax.set_xticks(0.5 + np.arange(len(data_names)))
        ax.set_xticklabels(data_names)
        ax.set_yticks(0.5 + np.arange(len(data_names)))
        ax.set_yticklabels(data_names)

    fig.suptitle(f'{"Relative " if use_relative_covariance else ""}Covariance Matrix')

    colorbar_ax = fig.add_subplot(gs[:, 4])
    colorbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap='viridis'), cax=colorbar_ax)
    if use_relative_covariance:
        colorbar.set_label('Relative Covariance []')
    else:
        colorbar.set_label('Covariance [A^2]')

# For each tip create a cross correlation matrix of the `ids`, `idc`, and `idn` signals.
if plot_cross_correlation_matrix:
    fig = plt.figure(figsize=(10, 10))

    gs = fig.add_gridspec(7, 5, width_ratios=[1, 1, 1, 1, 0.1], height_ratios=[1, 1, 1, 0.1, 1, 1, 1])

    cross_correlation_matrices = [[], []]
    for tip_index in range(PA_Probe1_Data.NUM_TIPS):
        ids_data = id_array[0, :num_shots_per_tip[tip_index], tip_index]
        idc_data, idn_data = id_array[2:, :num_shots_per_tip[tip_index], tip_index]

        for relay_value in range(2):
            ids_data_same_relay_status = ids_data[relay_status[:num_shots_per_tip[tip_index], tip_index] == relay_value]
            idc_data_same_relay_status = idc_data[relay_status[:num_shots_per_tip[tip_index], tip_index] == relay_value]
            idn_data_same_relay_status = idn_data[relay_status[:num_shots_per_tip[tip_index], tip_index] == relay_value]
            
            # Subtract the mean from the data.
            ids_data_same_relay_status -= np.mean(ids_data_same_relay_status, axis=1)[:, None]
            idc_data_same_relay_status -= np.mean(idc_data_same_relay_status, axis=1)[:, None]
            idn_data_same_relay_status -= np.mean(idn_data_same_relay_status, axis=1)[:, None]

            # Normalize by the standard deviation.
            ids_data_same_relay_status /= np.std(ids_data_same_relay_status, axis=1)[:, None]
            idc_data_same_relay_status /= np.std(idc_data_same_relay_status, axis=1)[:, None]
            idn_data_same_relay_status /= np.std(idn_data_same_relay_status, axis=1)[:, None]

            # Create the cross correlation matrix.
            cross_correlation_matrix = np.zeros((3, 3))
            for i, data1 in enumerate([ids_data_same_relay_status, idc_data_same_relay_status, idn_data_same_relay_status]):
                for j, data2 in enumerate([ids_data_same_relay_status, idc_data_same_relay_status, idn_data_same_relay_status]):
                    cross_correlation_matrix[i, j] = scipy.signal.correlate(data1, data2, mode='valid')

            cross_correlation_matrices[relay_value].append(cross_correlation_matrix)

    cross_correlation_matrices = [np.array(cross_correlation_matrices[0]), np.array(cross_correlation_matrices[1])]
    norm = mpl.colors.Normalize(np.min(cross_correlation_matrices), np.max(cross_correlation_matrices))

    data_names = ['S', 'C', 'N']
    for relay_value, cross_correlation_matrices_set in zip(range(2), cross_correlation_matrices):
        corr_axs = [fig.add_subplot(gs[relay_value * 4 + i, j]) for j in range(4) for i in range(3)]
        for tip_index, ax in zip(range(PA_Probe1_Data.NUM_TIPS), corr_axs):
            ax.pcolormesh(cross_correlation_matrices_set[tip_index], cmap='viridis', norm=norm)
            ax.set_title(f'Tip {tip_index + 1}')
            ax.set_aspect('equal')
            ax.set_xticks(0.5 + np.arange(len(data_names)))
            ax.set_xticklabels(data_names)
            ax.set_yticks(0.5 + np.arange(len(data_names)))
            ax.set_yticklabels(data_names)

    fig.suptitle('Cross Correlation Matrix')

    colorbar_ax = fig.add_subplot(gs[:, 4])
    colorbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap='viridis'), cax=colorbar_ax)
    colorbar.set_label('Cross Correlation [A^2]')

if plot_probability_map:
    # Calculate the probability map of the sense resistances.
    # Create a meshgrid of the sense resistances.
    r_sense_percent_error = 0.01
    sense_resistance_range, d_sense_resistance = np.linspace(r_sense - 500, r_sense + 500, 100, retstep=True)
    log_probability_mesh = np.zeros((PA_Probe1_Data.NUM_TIPS, sense_resistance_range.size, sense_resistance_range.size), dtype=float)
    ipc_mesh = np.zeros_like(ipc_array, dtype=float)
    # TODO: Figure out what this standard deviation should be.
    ipc_standard_deviation = 1 * 10**-4
    for r1_index, rsense_1 in enumerate(tqdm.tqdm(sense_resistance_range, desc="Calculating probability map")):
        for r2_index, rsense_2 in enumerate(sense_resistance_range):
            # Calculate ipc for each combination of sense resistances.
            for tip_index in range(PA_Probe1_Data.NUM_TIPS):
                for shot_index in range(num_shots_per_tip[tip_index]):
                    ipc_mesh[shot_index, tip_index] = manual_get_data.ipc_func(
                        *id_array[:, shot_index, tip_index], relay_status[shot_index, tip_index], 1,
                        r_divider, r_divider, r_divider, r_divider, r_sense, r_sense, rsense_1, rsense_2, shell_connected_to_core=True
                    )
                    log_probability_mesh[tip_index, r1_index, r2_index] += np.sum(-0.5 * ipc_mesh[:num_shots_per_tip[tip_index], tip_index]**2 / ipc_standard_deviation**2)

    sense_resistance_log_probability_distribution = -0.5 * (sense_resistance_range - r_sense)**2 / (r_sense * r_sense_percent_error)**2

    log_probability_mesh += sense_resistance_log_probability_distribution[None, :, None] + sense_resistance_log_probability_distribution[None, None, :]
    # Convert to a log probability density function.
    log_probability_mesh -= np.log(d_sense_resistance**2)

    # Normalize the log probability mesh.
    # log_probability_mesh /= np.sum(log_probability_mesh, axis=(1, 2))[:, None, None]

    # Plot the log probability mesh.
    fig = plt.figure()

    gs = fig.add_gridspec(3, 5, wspace=0, hspace=0, width_ratios=[1, 1, 1, 1, 0.1], height_ratios=[1, 1, 1])

    ax0 = fig.add_subplot(gs[0, 0])
    axs = [fig.add_subplot(gs[i, j], sharex=ax0, sharey=ax0) if (i != 0 or j != 0) else ax0 for j in range(4) for i in range(3)]

    normed_min = 10**-6
    exponential_constant = np.exp(np.log(normed_min) / np.min(log_probability_mesh))
    norm = mpl.colors.FuncNorm((lambda x: exponential_constant**x, lambda x: np.log(x) / np.log(exponential_constant)), vmin=np.min(log_probability_mesh), vmax=np.max(log_probability_mesh))

    for tip_index, ax in enumerate(axs):
        num_levels = 100
        levels = norm.inverse(np.linspace(norm(norm.vmin), norm(norm.vmax), num_levels))
        # ax.contourf(sense_resistance_range, sense_resistance_range, log_probability_mesh[tip_index], cmap='viridis', norm=norm, levels=100)
        ax.pcolormesh(sense_resistance_range, sense_resistance_range, log_probability_mesh[tip_index], cmap='viridis', norm=norm)
        r_sense_1 = sense_resistance_range[np.argmax(log_probability_mesh[tip_index]) % sense_resistance_range.size]
        r_sense_2 = sense_resistance_range[np.argmax(log_probability_mesh[tip_index]) // sense_resistance_range.size]
        ax.scatter(r_sense_1, r_sense_2, color='red', marker='x')
        ax.set_aspect('equal')

        if tip_index < 3:
            ax.set_ylabel(r'$R_{sense, 2}$ [$\Omega$]')
        if (tip_index + 1) % 3 == 0:
            ax.set_xlabel(r'$R_{sense, 1}$ [$\Omega$]')
        ax.set_xlim(sense_resistance_range[0], sense_resistance_range[-1])
        ax.set_ylim(sense_resistance_range[0], sense_resistance_range[-1])
        bbox_dict = dict(facecolor='white', alpha=0.5, edgecolor='white')
        ax.annotate(f'Tip #{tip_index + 1}', (.05, .2), xycoords='axes fraction', bbox=bbox_dict)
        ax.annotate(f'({r_sense_1:.2f}, {r_sense_2:.2f})', (.05, .1), xycoords='axes fraction', bbox=bbox_dict)

    # Make a colorbar for the probability mesh.
    colorbar_ax = fig.add_subplot(gs[:, 4])
    colorbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap='viridis'), cax=colorbar_ax, label='Log Probability Density')

    fig.suptitle('Log Probability of Sense Resistances')

plt.show()

