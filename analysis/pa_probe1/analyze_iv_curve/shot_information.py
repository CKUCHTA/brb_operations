# Name ordering is `helmholtz (G), drive (kV), middle_coil (bool), snubber (bool), tf (%), z (cm), clocking (deg)`
def _to_list(*shot_start_end_tuples, bad_shots=[]):
    all_shots = sum([list(range(*shot_tuple)) for shot_tuple in shot_start_end_tuples], [])
    return [shot for shot in all_shots if shot not in bad_shots]

helmholtz_30_drive_0_middle_False_snubber_False_tf_None_z_0_clocking_0 = _to_list((57332, 57350))

helmholtz_70_drive_5_middle_False_snubber_False_tf_None_z_0_clocking_0 = _to_list((58657, 58696))

helmholtz_100_drive_5_middle_False_snubber_False_tf_0_z_6_clocking_0 = [59039, 59042, 59043, 59044, 59045, 59047] + _to_list((59049, 59055))
helmholtz_100_drive_5_middle_False_snubber_False_tf_0_z_6_clocking_90 = [59057, 59059, 59061, 59063, 59064]
helmholtz_100_drive_5_middle_False_snubber_False_tf_0_z_6_clocking_180 = [59066, 59067, 59069, 59070, 59071]
helmholtz_100_drive_5_middle_False_snubber_False_tf_0_z_6_clocking_270 = _to_list((59073, 59078), (59081, 59084)) + [59081, 59082, 59083]

helmholtz_100_drive_5_middle_True_snubber_False_tf_0_z_30_clocking_0 = _to_list((59199, 59203), (59206, 59212), (59213, 59216))
helmholtz_100_drive_5_middle_True_snubber_False_tf_0_z_30_clocking_90 = _to_list((59216, 59219), (59221, 59233), (59249, 59252), (59253, 59259), (59260, 59263))
helmholtz_100_drive_5_middle_True_snubber_False_tf_0_z_30_clocking_270 = [59264] + _to_list((59269, 59273), (59275, 59277), (59278, 59279), (59280, 59283), (59288, 59293), (59296, 59298), (59300, 59305))

helmholtz_100_drive_6_middle_True_snubber_True_tf_50_z_43_clocking_0 = _to_list((59724, 59766), bad_shots=[59728, 59730, 59737, 59744, 59751, 59758, 59759])
helmholtz_100_drive_6_middle_True_snubber_True_tf_50_z_43_clocking_45 = _to_list((59767, 59773), (59775, 59787), (59788, 59794), (59795, 59801))

helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_0_clocking_45 = _to_list((59850, 59874), bad_shots=[59852, 59858, 59859, 59864, 59866, 59868, 59869, 59870])
helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_0_clocking_0 = _to_list((59877, 59905), (59911, 59915), bad_shots=[59882, 59883, 59884, 59888, 59892, 59895, 59898, 59899])
helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_0_clocking_90 = _to_list((59916, 59923), bad_shots=[59921])

helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_10_clocking_0 = _to_list((59928, 59939), bad_shots=[59930])
helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_10_clocking_315 = _to_list((59940, 59960), bad_shots=[59941, 59943])

helmholtz_100_drive_6_middle_False_snubber_True_tf_0_z_10_clocking_315 = _to_list((59960, 59972), (59975, 59978), (59982, 59990), bad_shots=[59963, 59966, 59988])

helmholtz_100_drive_6_middle_False_snubber_True_tf_0_z_5_clocking_315 = _to_list((59993, 60000), (60002, 60019), (60068, 60072), bad_shots=[60004, 60006, 60010, 60011, 60014])
helmholtz_100_drive_6_middle_False_snubber_True_tf_0_z_5_clocking_0 = _to_list((60073, 60092), bad_shots=[60076, 60079, 60085])

helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_0 = _to_list((60093, 60106), (60164, 60172), bad_shots=[60099, 60103])
helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_45 = _to_list((60173, 60195), bad_shots=[60175, 60179, 60186]) # All of these shots have negative vbias.
helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_90 = _to_list((60196, 60218), bad_shots=[60202, 60209])
helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_135 = _to_list((60219, 60240), bad_shots=[60220, 60222, 60225])
helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_180 = _to_list((60241, 60257), (60262, 60268), bad_shots=[60246, 60253])
helmholtz_100_drive_6_middle_True_snubber_True_tf_0_z_5_clocking_225 = _to_list((60269, 60271))

helmholtz_100_drive_6_middle_False_snubber_True_tf_0_z_5_clocking_225 = _to_list((60364, 60381), bad_shots=[60365])
helmholtz_100_drive_6_middle_False_snubber_True_tf_0_z_5_clocking_270 = _to_list((60383, 60385), (60388, 60403))
helmholtz_100_drive_6_middle_False_snubber_True_tf_0_z_5_clocking_315 = _to_list((60404, 60406))

helmholtz_50_4_coil_5_snubber_True_tf_0_z_36_clocking_180 = _to_list((62315, 62332))
helmholtz_50_4_coil_5_snubber_True_tf_0_z_36_clocking_180_alt_connection = _to_list((62332, 62366), bad_shots=[62344])
helmholtz_50_4_coil_5_snubber_True_tf_0_z_36_clocking_225_alt_connection = _to_list((62366, 62390))

helmholtz_50_3_coil_5_snubber_True_tf_0_z_36_clocking_0 = _to_list((62916, 62961))
helmholtz_50_3_coil_5_snubber_True_tf_0_z_36_clocking_45 = _to_list((62961, 62976), (63074, 63099), (63532, 63550))
helmholtz_50_3_coil_5_snubber_True_tf_0_z_36_clocking_90 = _to_list((63005, 63025), (63036, 63074))
helmholtz_50_3_coil_5_snubber_True_tf_0_z_36_clocking_135 = _to_list((63099, 63137), (63515, 63532), bad_shots=[63523])
helmholtz_50_3_coil_5_snubber_True_tf_0_z_36_clocking_180 = _to_list((63137, 63185), (63499, 63515), bad_shots=[63147, 63150, 63151, 63152, 63160, 63161, 63168, 63171, 63176, 63177, 63178])
helmholtz_50_3_coil_5_snubber_True_tf_0_z_36_clocking_225 = _to_list((63186, 63222), (63483, 63499))
helmholtz_50_3_coil_5_snubber_True_tf_0_z_36_clocking_270 = _to_list((63222, 63259), (63450, 63452), (63461, 63462), (63472, 63474), (63479, 63483))
helmholtz_50_3_coil_5_snubber_True_tf_0_z_36_clocking_315 = _to_list((63259, 63297), (63394, 63396), (63405, 63407), (63416, 63418), (63427, 63429), (63438, 63441), bad_shots=[63439])
