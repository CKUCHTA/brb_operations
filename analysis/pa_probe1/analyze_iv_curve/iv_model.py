"""The model for the PA probe IV curve"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from astropy import units as u
from astropy import constants as c
from astropy.visualization import quantity_support
from plasmapy.formulary import ion_sound_speed
from scipy.integrate import quad, nquad
from pa_probe1.get_data import PA_Probe1_Data
from pa_probe1.projected_area import _cap_area_func, _side_area_func, max_theta, projected_area, cap_projected_area, total_area
import time
quantity_support()


def drifting_bimaxwellian(
        v_vector, 
        n, 
        T_parallel, 
        T_perpendicular, 
        v_drift, 
        magnetic_field_direction,
        particle_mass=c.m_e
    ):
    """The distribution function for a drifting bimaxwellian with a parallel and perpendicular temperature.
    
    Parameters
    ----------
    v_vector : `astropy.Quantity[numpy.ndarray[float]]`
        Velocity vector to find distribution function at. Any number of 
        dimensions. The shape should be `(..., 3)`. Units of `m / s`.
    n : `astropy.Quantity[float]`
        Number density of plasma. Units of `m^-3`.
    T_parallel, T_perpendicular : `astropy.Quantity[float]`
        Temperature in the parallel and perpendicular directions. Units 
        of `eV`.
    v_drift : `astropy.Quantity[numpy.ndarray[float]]`
        The plasma drift/flow velocity. 1D array of shape `(3,)`. Units of `m / s`.
    magnetic_field_direction : `numpy.ndarray[float]`
        A unit vector pointing in the magnetic field direction which is the 
        parallel direction. 1D array of shape `(3,)`. No units.
    particle_mass : `float`, optional
        Mass of distribution function particle. Default is `astropy.constants.m_e`.

    Returns
    -------
    f : `astropy.Quantity[float]` or `astropy.Quantity[numpy.ndarray[float]]`
        The value of the distribution function at the `v_vector` point(s).
    """
    initial_shape = v_vector.shape
    if len(v_vector.shape) > 2:
        # Reshape the array into a 2D array.
        v_vector = v_vector.reshape((-1, 3))
    
    if len(v_vector.shape) > 1:
        v_vector = v_vector.transpose()
    else:
        v_vector = v_vector[:, None]

    parallel_shifted_velocity = np.dot((v_vector - v_drift[:, np.newaxis]).T, magnetic_field_direction)
    perpendicular_shifted_velocity = np.linalg.norm((v_vector - v_drift[:, np.newaxis]) - parallel_shifted_velocity[np.newaxis, :] * magnetic_field_direction[:, np.newaxis], axis=0)
    f = (particle_mass / (2 * np.pi))**1.5 * n / (T_parallel * T_perpendicular**2)**0.5 * np.exp(-particle_mass / 2 * (perpendicular_shifted_velocity**2 / T_perpendicular + parallel_shifted_velocity**2 / T_parallel))

    if len(initial_shape) == 1:
        return f[0]
    else:
        return f.reshape(initial_shape[:-1])
    
def visualize_drifting_bimaxwellian(
        n, 
        T_parallel, 
        T_perpendicular, 
        v_drift, 
        magnetic_field_direction,
        particle_mass,
    ):
    """Visualize the drifting bimaxwellian distribution.
    
    Returns
    -------
    fig, (velocity_ax, energy_ax)
    """
    T_multiples = 3
    v_drift_parallel = np.dot(v_drift, magnetic_field_direction)
    v_drift_perpendicular = np.linalg.norm(v_drift - v_drift_parallel * magnetic_field_direction)
    if v_drift_perpendicular == 0:
        unit_vector = np.array([1, 0, 0], dtype=float)
        if np.isclose(np.linalg.norm(np.cross(magnetic_field_direction, unit_vector)), 0):
            unit_vector = np.array([0, 1, 0], dtype=float)

        cross_vector = np.cross(magnetic_field_direction, unit_vector)
        perpendicular_direction = cross_vector / np.linalg.norm(cross_vector)
    else:
        perpendicular_direction = (v_drift - v_drift_parallel * magnetic_field_direction) / v_drift_perpendicular

    v_parallel_bounds = v_drift_parallel + (2 * T_multiples * T_parallel / particle_mass)**0.5 * np.array([-1, 1])
    v_parallel_bounds = np.array([min(v_parallel_bounds[0].to(u.m / u.s).value, 0), max(v_parallel_bounds[1].to(u.m / u.s).value, 0)]) * u.m / u.s
    v_perpendicular_bounds = v_drift_perpendicular + (2 * T_multiples * T_perpendicular / particle_mass)**0.5 * np.array([-1, 1])
    v_perpendicular_bounds = np.array([min(v_perpendicular_bounds[0].to(u.m / u.s).value, 0), max(v_perpendicular_bounds[1].to(u.m / u.s).value, 0)]) * u.m / u.s

    num_points_per_axis = 100
    v_parallel_factors = np.linspace(*v_parallel_bounds, num_points_per_axis)
    v_perpendicular_factors = np.linspace(*v_perpendicular_bounds, num_points_per_axis)

    v_parallel_meshgrid, v_perpendicular_meshgrid = np.meshgrid(v_parallel_factors, v_perpendicular_factors)
    v_vectors = (
        v_parallel_meshgrid.flatten()[:, np.newaxis] * magnetic_field_direction[np.newaxis, :] + 
        v_perpendicular_meshgrid.flatten()[:, np.newaxis] * perpendicular_direction[np.newaxis, :]
    )
    distribution_function_values = drifting_bimaxwellian(v_vectors, n, T_parallel, T_perpendicular, v_drift, magnetic_field_direction, particle_mass).reshape(v_parallel_meshgrid.shape)

    fig, (velocity_ax, energy_ax) = plt.subplots(1, 2)
    
    # Velocity axis
    contour_plot = velocity_ax.contourf(v_parallel_factors, v_perpendicular_factors, distribution_function_values)
    velocity_ax.scatter(v_drift_parallel, v_drift_perpendicular, color='red', marker='x')

    velocity_ax.set_aspect('equal')
    velocity_ax.axvline(0, color='grey', linestyle='dashed')
    velocity_ax.axhline(0, color='grey', linestyle='dashed')
    velocity_ax.set_xlabel(r'$v_\parallel \,[\mathrm{m} / \mathrm{s}]$')
    velocity_ax.set_ylabel(r'$v_\perp \,[\mathrm{m} / \mathrm{s}]$')
    fig.colorbar(contour_plot, ax=velocity_ax, label=r'$f(v_\parallel, v_\perp)$')

    # Energy axis
    velocity_to_energy = lambda v: (particle_mass / 2 * v**2 * np.sign(v)).to('eV')
    energy_to_velocity = lambda E: (2 * np.abs(E) / particle_mass)**0.5 * np.sign(E)
    e_parallel_factors = np.linspace(velocity_to_energy(v_parallel_bounds[0]), velocity_to_energy(v_parallel_bounds[1]), num_points_per_axis)
    e_perpendicular_factors = np.linspace(velocity_to_energy(v_perpendicular_bounds[0]), velocity_to_energy(v_perpendicular_bounds[1]), num_points_per_axis)
    new_v_parallel_meshgrid, new_v_perpendicular_meshgrid = np.meshgrid(energy_to_velocity(e_parallel_factors), energy_to_velocity(e_perpendicular_factors))
    new_v_vectors = (
        new_v_parallel_meshgrid.flatten()[:, np.newaxis] * magnetic_field_direction[np.newaxis, :] + 
        new_v_perpendicular_meshgrid.flatten()[:, np.newaxis] * perpendicular_direction[np.newaxis, :]
    )
    new_distribution_function_values = drifting_bimaxwellian(new_v_vectors, n, T_parallel, T_perpendicular, v_drift, magnetic_field_direction, particle_mass).reshape(v_parallel_meshgrid.shape)

    contour_plot = energy_ax.contourf(e_parallel_factors, e_perpendicular_factors, new_distribution_function_values)
    energy_ax.scatter(velocity_to_energy(v_drift_parallel), velocity_to_energy(v_drift_perpendicular), color='red', marker='x')

    energy_ax.set_aspect('equal')
    energy_ax.axvline(0, color='grey', linestyle='dashed')
    energy_ax.axhline(0, color='grey', linestyle='dashed')
    energy_ax.set_xlabel(r'$E_\parallel \,[\mathrm{eV}]$')
    energy_ax.set_ylabel(r'$E_\perp \,[\mathrm{eV}]$')
    fig.colorbar(contour_plot, ax=energy_ax, label=r'$f(E_\parallel, E_\perp)$')

    return fig, (velocity_ax, energy_ax)


def minimum_velocity(theta, V_p, V_0, cap=True, x=None, core_radius=None, particle_charge=-c.e.si, particle_mass=c.m_e):
    """Get the minimum velocity particles need to hit the cap or side of the core.
    
    Parameters
    ----------
    theta : `numpy.ndarray[float]`
        1D-array of angles measured from the tip direction. Of shape 
        `(NUM_THETA,)`.
    V_p, V_0 : `astropy.Quantity[float]`
        The probe and plasma potential.
    cap : `bool`, optional
        Whether the minimum velocity is needed for the cap or side of the tip. 
        Default is `True`. If `False` then both `x` and `core_radius` are 
        needed.
    x : `astropy.Quantity[numpy.ndarray[float]]`, optional
        1D-array of positions across the probe between `+/- core_radius`. Of 
        shape `(NUM_X,)`.
    core_radius : `astropy.Quantity[float]`, optional

    Returns
    -------
    v_min : `astropy.Quantity[numpy.ndarray[float]]`
        Minimum velocity to evaluate the velocity integral from. If 
        `cap == True` then this is the minimum velocities for electrons to hit 
        the cap of the tip and is a 1D-array of shape `(NUM_THETA,)`. If 
        `cap == False` then the minimum velocity depends on `x` so a 2D-array 
        of shape `(NUM_THETA, NUM_X)` is returned.
    """
    if cap:
        v_min_cap = (2 * particle_charge / particle_mass * (V_p - V_0))**0.5 / np.cos(theta)
        return v_min_cap
    else:
        v_min_side = (2 * particle_charge / particle_mass * (V_p - V_0))**0.5 / (np.sin(theta) * (1 - x**2 / core_radius**2)**0.5)
        return v_min_side


def machine_to_tip_coordinate_matrix(
        v_drift, magnetic_field_direction, tip_direction
    ):
    """
    Create a matrix that transforms a vector in machine cartesian to tip cartesian coordinates.
    
    The tip coordinates are made of three orthonormal unit vectors: `t1`, `t2`, 
    and `t3`. `t3` is along the tip direction. `t1` is chosen such that 
    `v_drift` lies in the `t1`-`t3` plane unless `v_drift` is along `t3` or 
    `v_drift = 0`. If this is the case then `t1` is instead chosen to align the 
    with the field direction.
    """
    transformation_matrix = np.zeros((3, 3))
    transformation_matrix[2] = tip_direction

    if not np.allclose(np.cross(tip_direction, v_drift), 0):
        transformation_matrix[0] = v_drift - np.dot(v_drift, tip_direction) * tip_direction
    elif not np.allclose(np.cross(tip_direction, magnetic_field_direction), 0):
        transformation_matrix[0] = magnetic_field_direction - np.dot(magnetic_field_direction, tip_direction) * tip_direction
    elif np.isclose(tip_direction[0], 1):
        transformation_matrix[0] = np.array([0, 1, 0])
    else:
        transformation_matrix[0] = np.cross(np.array([1, 0, 0]), tip_direction)
    transformation_matrix[0] /= np.linalg.norm(transformation_matrix[0])

    transformation_matrix[1] = np.cross(transformation_matrix[2], transformation_matrix[0])

    return transformation_matrix


def _cap_particle_flux_cylindrical_converted_v(
        v_parallel_to_tip,
        d_s_parallel,
        v_perpendicular_to_tip,
        d_s_perpendicular,
        phi_T,
        d_phi,
        d_v_d_s_parallel,
        d_v_d_s_perpendicular,
        transformation_matrix,
        pa_data,
        n, 
        T_parallel, 
        T_perpendicular, 
        V_probe,
        V_plasma,
        v_drift, 
        magnetic_field_direction,
        return_cumulative_fluxes=False
    ):
    """
    Parameters
    ----------
    v_parallel_to_tip, d_s_parallel, v_perpendicular_to_tip, d_s_perpendicular : `numpy.ndarray[float]`
        1D-array of velocities and the spacing between the converted velocities.
    phi_T, d_phi : `astropy.units.Quantity`
        1D-array of angles relative to the tip and the spacing between angles.
    d_v_d_s_parallel, d_v_d_s_perpendicular : `astropy.units.Quantity`
        1D-array of Jacobians for the converted velocities.
    transformation_matrix : `numpy.ndarray[float]`
        2D-array to transform vectors in machine coordinates to tip coordinates.
    pa_data : `pa_probe1.get_data.PA_Probe1_Data`
    n, T_parallel, T_perpendicular, V_probe, V_plasma : `astropy.units.Quantity`
        Plasma density and temperatures, probe potential, and plasma potential.
    v_drift : `astropy.units.Quantity`
        Plasma drift velocity as a 1D-array of shape `(3,)` in machine coordinates.
    magnetic_field_direction : `numpy.ndarray[float]`
        1D-array of shape `(3,)` in machine coordinates of the local magnetic field direction.
    return_cumulative_fluxes : bool
        Whether to return an array of fluxes and corresponding voltages instead of just a single flux value.
    """
    v_parallel_to_tip_value = v_parallel_to_tip.to(u.m / u.s).value
    d_s_parallel_value = d_s_parallel.to('').value
    v_perpendicular_to_tip_value = v_perpendicular_to_tip.to(u.m / u.s).value
    d_s_perpendicular_value = d_s_perpendicular.to('').value
    phi_T_value = phi_T.to(u.rad).value
    d_phi_value = d_phi.to(u.rad).value

    d_v_d_s_parallel_value = d_v_d_s_parallel.to(u.m / u.s).value
    d_v_d_s_perpendicular_value = d_v_d_s_perpendicular.to(u.m / u.s).value

    # `[v_parallel, v_perpendicular, phi]`
    v_tip_coords_array = np.zeros((v_parallel_to_tip_value.size, v_perpendicular_to_tip_value.size, phi_T_value.size, 3))
    v_tip_coords_array[:, :, :, 0] = v_perpendicular_to_tip_value[None, :, None] * np.cos(phi_T_value[None, None, :])
    v_tip_coords_array[:, :, :, 1] = v_perpendicular_to_tip_value[None, :, None] * np.sin(phi_T_value[None, None, :])
    v_tip_coords_array[:, :, :, 2] = v_parallel_to_tip_value[:, None, None]
    distribution_function = drifting_bimaxwellian(
        v_tip_coords_array,
        n.to(u.m**-3).value, T_parallel.to(u.J).value, T_perpendicular.to(u.J).value, 
        transformation_matrix @ v_drift.to(u.m / u.s).value, transformation_matrix @ magnetic_field_direction, particle_mass=c.m_e.to(u.kg).value
    )
    # `[v_parallel, v_perpendicular]`
    cap_area = cap_projected_area(
        pa_data.core_tip_radius, pa_data.shell_tip_radius, 
        pa_data.core_tip_height, pa_data.shell_tip_height,
        np.arctan2(v_perpendicular_to_tip_value[None, :], v_parallel_to_tip_value[:, None])
    )
    # `[v_parallel]`
    valid_velocities = v_parallel_to_tip_value >= minimum_velocity(
        0, V_probe.to(u.V).value, V_plasma.to(u.V).value, cap=True, particle_charge=-c.e.si.to(u.C).value, particle_mass=c.m_e.to(u.kg).value
    )

    # First integrate over `phi_T` as the distribution function is the only one containing it.
    phi_integral = np.sum(distribution_function * d_phi_value, axis=-1)
    # The x integral is only part of the projected area.
    x_and_phi_integral = phi_integral * cap_area
    # The v_perp integral has to take into account the cylindrical integration.
    x_phi_and_perp_integral = np.sum(x_and_phi_integral * (v_perpendicular_to_tip_value * d_v_d_s_perpendicular_value * d_s_perpendicular_value)[None, :], axis=-1)
    # The v_parallel integral has to take into account the `v * f` velocity of the flux calculation.
    if return_cumulative_fluxes:
        cap_flux_sum = np.cumsum((v_parallel_to_tip_value * d_v_d_s_parallel_value * d_s_parallel_value * x_phi_and_perp_integral)[::-1])[::-1]
    else:
        cap_flux_sum = np.sum(v_parallel_to_tip_value * d_v_d_s_parallel_value * d_s_parallel_value * x_phi_and_perp_integral, where=valid_velocities)
    return cap_flux_sum / u.s


def _cap_particle_flux_cylindrical(
        v_parallel_to_tip,
        d_v_parallel,
        v_perpendicular_to_tip,
        d_v_perpendicular,
        phi_T,
        d_phi,
        transformation_matrix,
        pa_data,
        n, 
        T_parallel, 
        T_perpendicular, 
        V_probe,
        V_plasma,
        v_drift, 
        magnetic_field_direction,
        return_cumulative_fluxes=False
    ):
    """
    Parameters
    ----------
    v_parallel_to_tip, d_v_parallel, v_perpendicular_to_tip, d_v_perpendicular : `astropy.units.Quantity`
        1D-array of velocities and the spacing between the velocities.
    phi_T, d_phi : `astropy.units.Quantity`
        1D-array of angles relative to the tip and the spacing between angles.
    transformation_matrix : `numpy.ndarray[float]`
        2D-array to transform vectors in machine coordinates to tip coordinates.
    pa_data : `pa_probe1.get_data.PA_Probe1_Data`
    n, T_parallel, T_perpendicular, V_probe, V_plasma : `astropy.units.Quantity`
        Plasma density and temperatures, probe potential, and plasma potential.
    v_drift : `astropy.units.Quantity`
        Plasma drift velocity as a 1D-array of shape `(3,)` in machine coordinates.
    magnetic_field_direction : `numpy.ndarray[float]`
        1D-array of shape `(3,)` in machine coordinates of the local magnetic field direction.
    return_cumulative_fluxes : bool
        Whether to return an array of fluxes and corresponding voltages instead of just a single flux value.
    """
    v_parallel_to_tip_value = v_parallel_to_tip.to(u.m / u.s).value
    d_v_parallel_value = d_v_parallel.to(u.m / u.s).value
    v_perpendicular_to_tip_value = v_perpendicular_to_tip.to(u.m / u.s).value
    d_v_perpendicular_value = d_v_perpendicular.to(u.m / u.s).value
    phi_T_value = phi_T.to(u.rad).value
    d_phi_value = d_phi.to(u.rad).value

    # `[v_parallel, v_perpendicular, phi]`
    v_tip_coords_array = np.zeros((v_parallel_to_tip_value.size, v_perpendicular_to_tip_value.size, phi_T_value.size, 3))
    v_tip_coords_array[:, :, :, 0] = v_perpendicular_to_tip_value[None, :, None] * np.cos(phi_T_value[None, None, :])
    v_tip_coords_array[:, :, :, 1] = v_perpendicular_to_tip_value[None, :, None] * np.sin(phi_T_value[None, None, :])
    v_tip_coords_array[:, :, :, 2] = v_parallel_to_tip_value[:, None, None]
    distribution_function = drifting_bimaxwellian(
        v_tip_coords_array,
        n.to(u.m**-3).value, T_parallel.to(u.J).value, T_perpendicular.to(u.J).value, 
        transformation_matrix @ v_drift.to(u.m / u.s).value, transformation_matrix @ magnetic_field_direction, particle_mass=c.m_e.to(u.kg).value
    )
    # `[v_parallel, v_perpendicular]`
    cap_area = cap_projected_area(
        pa_data.core_tip_radius, pa_data.shell_tip_radius, 
        pa_data.core_tip_height, pa_data.shell_tip_height,
        np.arctan2(v_perpendicular_to_tip_value[None, :], v_parallel_to_tip_value[:, None])
    )
    # `[v_parallel]`
    valid_velocities = v_parallel_to_tip_value >= minimum_velocity(
        0, V_probe.to(u.V).value, V_plasma.to(u.V).value, cap=True, particle_charge=-c.e.si.to(u.C).value, particle_mass=c.m_e.to(u.kg).value
    )

    # First integrate over `phi_T` as the distribution function is the only one containing it.
    phi_integral = np.sum(distribution_function * d_phi_value, axis=-1)
    # The x integral is only part of the projected area.
    x_and_phi_integral = phi_integral * cap_area
    # The v_perp integral has to take into account the cylindrical integration.
    x_phi_and_perp_integral = np.sum(x_and_phi_integral * (v_perpendicular_to_tip_value * d_v_perpendicular_value)[None, :], axis=-1)
    # The v_parallel integral has to take into account the `v * f` velocity of the flux calculation.
    if return_cumulative_fluxes:
        cap_flux_sum = np.cumsum((v_parallel_to_tip_value * d_v_parallel_value * x_phi_and_perp_integral)[::-1])[::-1]
    else:
        cap_flux_sum = np.sum(v_parallel_to_tip_value * d_v_parallel_value * x_phi_and_perp_integral, where=valid_velocities)
    return cap_flux_sum / u.s


def _cap_particle_flux(
        v,
        d_v,
        theta_T,
        d_theta,
        phi_T,
        d_phi,
        transformation_matrix,
        pa_data,
        n, 
        T_parallel, 
        T_perpendicular, 
        V_probe,
        V_plasma,
        v_drift, 
        magnetic_field_direction,
    ):
    """
    Parameters
    ----------
    v, theta_T, phi_T : `astropy.units.Quantity`
        1D-array of velocities and angles relative to the tip.
    transformation_matrix : `numpy.ndarray[float]`
        2D-array to transform vectors in machine coordinates to tip coordinates.
    pa_data : `pa_probe1.get_data.PA_Probe1_Data`
    n, T_parallel, T_perpendicular, V_probe, V_plasma : `astropy.units.Quantity`
        Plasma density and temperatures, probe potential, and plasma potential.
    v_drift : `astropy.units.Quantity`
        Plasma drift velocity as a 1D-array of shape `(3,)` in machine coordinates.
    magnetic_field_direction : `numpy.ndarray[float]`
        1D-array of shape `(3,)` in machine coordinates of the local magnetic field direction.
    """
    v_value = v.to(u.m / u.s).value
    d_v_value = d_v.to(u.m / u.s).value
    theta_T_value = theta_T.to(u.rad).value
    d_theta_value = d_theta.to(u.rad).value
    phi_T_value = phi_T.to(u.rad).value
    d_phi_value = d_phi.to(u.rad).value

    # Ordered as `[v, theta, phi]`
    distribution_function = drifting_bimaxwellian(
         np.moveaxis(
            v_value[None, :, None, None] * np.array([
                np.cos(phi_T_value[None, :]) * np.sin(theta_T_value[:, None]), 
                np.sin(phi_T_value[None, :]) * np.sin(theta_T_value[:, None]), 
                np.ones_like(phi_T_value)[None, :] * np.cos(theta_T_value[:, None])
            ])[:, None, :, :], 
            0, -1
        ),
        n.to(u.m**-3).value, T_parallel.to(u.J).value, T_perpendicular.to(u.J).value, 
        transformation_matrix @ v_drift.to(u.m / u.s).value, transformation_matrix @ magnetic_field_direction, particle_mass=c.m_e.to(u.kg).value
    )
    # `[theta,]`
    cap_area = cap_projected_area(
        pa_data.core_tip_radius, pa_data.shell_tip_radius, 
        pa_data.core_tip_height, pa_data.shell_tip_height,
        theta_T_value
    )
    # `[v, theta]`
    valid_velocities = v_value[:, None] >= minimum_velocity(
        theta_T_value, V_probe.to(u.V).value, V_plasma.to(u.V).value, cap=True, particle_charge=-c.e.si.to(u.C).value, particle_mass=c.m_e.to(u.kg).value
    )[None, :]

    # First integrate over `phi_T` as the distribution function is the only one containing it.
    # `[v, theta]`
    phi_integral = np.sum(distribution_function * d_phi_value, axis=-1)
    # The x integral is only part of the projected area.
    # `[v, theta]`
    x_and_phi_integral = phi_integral * cap_area[None, :]
    # Now do the big integral over `v` and `theta`.
    # We can combine the v^2 sin(theta) and `v_perp = v cos(theta)` terms into v^3 and 1/2 * sin(2 theta)
    cap_flux_sum = np.sum(
        (v_value**3 * d_v_value)[:, None] * (0.5 * np.sin(2 * theta_T_value) * d_theta_value)[None, :] * x_and_phi_integral,
        where=valid_velocities
    )
    return cap_flux_sum / u.s


def _side_particle_flux(
        v,
        theta_T,
        phi_T,
        x,
        transformation_matrix,
        pa_data,
        n, 
        T_parallel, 
        T_perpendicular, 
        V_probe,
        V_plasma,
        v_drift, 
        magnetic_field_direction,
    ):
    # First integrate over `phi_T` as the distribution function is the only one containing it.
    distribution_function = drifting_bimaxwellian(
        np.moveaxis(
            v[None, :, None, None] * np.array([
                np.cos(phi_T[None, :]) * np.sin(theta_T[:, None]), 
                np.sin(phi_T[None, :]) * np.sin(theta_T[:, None]), 
                np.ones_like(phi_T)[None, :] * np.cos(theta_T[:, None])
            ])[:, None, :, :], 
            0, -1
        ),
        n, T_parallel, T_perpendicular, transformation_matrix @ v_drift, transformation_matrix @ magnetic_field_direction
    ) # Ordered as `[v, theta, phi]`
    phi_integral = np.sum(distribution_function, axis=-1)
    side_area = _side_area_func(
        x[None, :],
        pa_data.core_tip_radius * u.m, pa_data.shell_tip_radius * u.m, 
        pa_data.core_tip_height * u.m, pa_data.shell_tip_height * u.m,
        theta_T[:, None]
    )
    r = pa_data.core_tip_radius * u.m
    perp_velocity_component = np.sin(theta_T)[:, None] * (1 - x**2 / r**2)[None, :]
    # Now do the big integral over `v` and `theta`.
    return np.sum(
        ((v**3)[:, None] * np.sin(theta_T)[None, :] * phi_integral)[:, :, None] * (side_area * perp_velocity_component)[None, :, :],
        where=v[:, None, None] >= minimum_velocity(theta_T[:, None], V_probe, V_plasma, cap=False, x=x[None, :], core_radius=r)[None, :, :]
    )


def core_electron_particle_flux(
        n, 
        T_parallel, 
        T_perpendicular, 
        V_probe,
        V_plasma,
        v_drift, 
        magnetic_field_direction,
        tip_direction,
        num_phi_points=16,
        num_theta_points=32,
        num_x_points=20,
        num_v_points=32,
        num_v_parallel_points=32,
        num_v_perpendicular_points=32,
        calculate_side_flux=True,
        example_pa_data=None,
        cylindrical_coords=False,
        cumulative_fluxes=False,
    ):
    """Get the electron particle flux to the repelling core.

    Parameters
    ----------
    n, T_parallel, T_perpendicular, V_probe, V_plasma : `astropy.units.Quantity` or `float`
        Plasma density (m**-3), temperatures (eV), probe potential (V), and plasma potential (V).
    v_drift : `astropy.units.Quantity` or `numpy.ndarray`
        Plasma drift velocity (m/s) as a 1D-array of shape `(3,)` in machine coordinates.
    magnetic_field_direction, tip_direction : `numpy.ndarray[float]`
        1D-array of shape `(3,)` in machine coordinates of the local magnetic field direction and tip direction.

    Returns
    -------
    cap_particle_flux, side_particle_flux : `astropy.units.Quantity`
    """
    if example_pa_data is None:
        example_pa_data = PA_Probe1_Data(0)
    transformation_matrix = machine_to_tip_coordinate_matrix(v_drift, magnetic_field_direction, tip_direction)

    phi_T_limits = (0, 2 * np.pi * u.rad)
    # We want to skip theta = 0 and theta = theta_max as there is no contribution to the flux at those points.
    cap_theta_step_size = max_theta(example_pa_data.core_tip_radius, example_pa_data.shell_tip_radius, example_pa_data.core_tip_height, example_pa_data.shell_tip_height, True) * u.rad / num_theta_points
    cap_theta_limits = (
        0.5 * cap_theta_step_size, cap_theta_step_size * (num_theta_points - 0.5)
    )

    T_multiples = 5
    if isinstance(T_parallel, u.Quantity):
        if V_probe.size > 1:
            min_velocity = minimum_velocity(0, np.max(V_probe), V_plasma, cap=True)
            max_velocity = minimum_velocity(0, np.min(V_probe), V_plasma, cap=True) + max(np.dot(v_drift, tip_direction), 0) + (2 * T_multiples * max(T_parallel, T_perpendicular) / c.m_e)**0.5
        else:
            min_velocity = minimum_velocity(0, V_probe, V_plasma, cap=True)
            max_velocity = min_velocity + max(np.dot(v_drift, tip_direction), 0) + (2 * T_multiples * max(T_parallel, T_perpendicular) / c.m_e)**0.5
        cap_v_limits = (min_velocity, max_velocity)
    else:
        if isinstance(V_probe, np.ndarray):
            min_velocity = minimum_velocity(0, np.max(V_probe * u.V), V_plasma * u.V, cap=True)
            max_velocity = minimum_velocity(0, np.min(V_probe * u.V), V_plasma * u.V, cap=True) + max(np.dot(v_drift, tip_direction), 0) * u.m / u.s + (2 * T_multiples * max(T_parallel, T_perpendicular) * u.eV / c.m_e)**0.5
        else:
            min_velocity = minimum_velocity(0, V_probe * u.V, V_plasma * u.V, cap=True)
            max_velocity = min_velocity + max(np.dot(v_drift, tip_direction), 0) * u.m / u.s + (2 * T_multiples * max(T_parallel, T_perpendicular) * u.eV / c.m_e)**0.5
        cap_v_limits = (min_velocity, max_velocity)


    # num_phi_points = 5
    # num_theta_points = 7
    # num_x_points = 11
    # num_v_points = 13
    # num_v_parallel_points = 17
    # num_v_perpendicular_points = 19

    phi_points, dphi = np.linspace(*phi_T_limits, num_phi_points, endpoint=False, retstep=True)
    
    # [n, T_parallel, T_perpendicular, V_probe, V_plasma, v_drift]
    if isinstance(T_parallel, u.Quantity):
        factors = [1, 1, 1, 1, 1, 1]
    else:
        factors = [u.m**-3, u.eV, u.eV, u.V, u.V, u.m / u.s]

    if not cylindrical_coords:
        cap_theta_points, dtheta = np.linspace(*cap_theta_limits, num_theta_points, retstep=True)
        cap_v_points, dv = np.linspace(*cap_v_limits, num_v_points, endpoint=False, retstep=True)
        
        cap_particle_flux = _cap_particle_flux(
            cap_v_points, dv, cap_theta_points, dtheta, phi_points, dphi, transformation_matrix,
            example_pa_data, n * factors[0], T_parallel * factors[1], T_perpendicular * factors[2], 
            V_probe * factors[3], V_plasma * factors[4], 
            v_drift * factors[5], magnetic_field_direction
        )
    else:
        angle_between_magnetic_field_and_tip = np.arccos(np.dot(magnetic_field_direction, tip_direction))
        temperature_in_tip_direction = (T_parallel * T_perpendicular) / (
            T_parallel * np.sin(angle_between_magnetic_field_and_tip)**2 + 
            T_perpendicular * np.cos(angle_between_magnetic_field_and_tip)**2
        ) * factors[1]

        if isinstance(V_probe, np.ndarray) and V_probe.size > 1:
            sorted_unique_V_probe, inverse_indeces = np.unique(-V_probe, return_inverse=True)
            sorted_unique_V_probe *= -1
            v_parallel_break_points = minimum_velocity(0, sorted_unique_V_probe * factors[3], V_plasma * factors[4])
            # sorted_V_probe_indeces = np.argsort(-V_probe)
            # v_parallel_break_points = minimum_velocity(0, (V_probe * factors[3])[sorted_V_probe_indeces], V_plasma * factors[4])

            v_parallel_list = []
            d_s_parallel_list = []
            d_v_d_s_parallel_list = []
            for i in range(v_parallel_break_points.size - 1):
                v, d_s, d_v_d_s = convert_v_to_s(v_parallel_break_points[i], v_parallel_break_points[i + 1], temperature_in_tip_direction, num=num_v_parallel_points)
                v_parallel_list.append(v)
                d_s_parallel_list.append(d_s * np.ones(num_v_parallel_points))
                d_v_d_s_parallel_list.append(d_v_d_s)
            v, d_s, d_v_d_s = convert_v_to_s(v_parallel_break_points[-1], max_velocity, temperature_in_tip_direction, num=num_v_parallel_points)
            v_parallel_list.append(v)
            d_s_parallel_list.append(d_s * np.ones(num_v_parallel_points))
            d_v_d_s_parallel_list.append(d_v_d_s)

            cap_v_parallel_points = np.concatenate(v_parallel_list)
            d_s_parallel = np.concatenate(d_s_parallel_list)
            d_v_d_s_parallel = np.concatenate(d_v_d_s_parallel_list)
        else:
            cap_v_parallel_points, d_s_parallel, d_v_d_s_parallel = convert_v_to_s(*cap_v_limits, temperature_in_tip_direction, num=num_v_parallel_points)
        cap_v_perpendicular_points, d_s_perpendicular, d_v_d_s_perpendicular = convert_v_to_s(0 * u.m / u.s, cap_v_limits[1], average_temperature(T_parallel, T_perpendicular) * factors[1], num=num_v_perpendicular_points)

        cap_particle_flux = _cap_particle_flux_cylindrical_converted_v(
            cap_v_parallel_points, d_s_parallel, cap_v_perpendicular_points, d_s_perpendicular, phi_points, dphi, 
            d_v_d_s_parallel, d_v_d_s_perpendicular, transformation_matrix,
            example_pa_data, n * factors[0], T_parallel * factors[1], T_perpendicular * factors[2], 
            sorted_unique_V_probe[0] * factors[3], V_plasma * factors[4], 
            # np.max(V_probe) * factors[3], V_plasma * factors[4], 
            v_drift * factors[5], magnetic_field_direction, 
            return_cumulative_fluxes=cumulative_fluxes
        )
        if cumulative_fluxes and isinstance(V_probe, np.ndarray) and V_probe.size > 1:
            unsorted_cap_particle_fluxes = np.zeros(V_probe.size) / u.s
            unsorted_cap_particle_fluxes = cap_particle_flux[::num_v_parallel_points][inverse_indeces]
            # unsorted_cap_particle_fluxes[sorted_V_probe_indeces] = cap_particle_flux[::num_v_parallel_points]
            cap_particle_flux = unsorted_cap_particle_fluxes

    if calculate_side_flux:
        # We only do (0, r) since x is symmetric. So as to not double-count at 
        # `x = 0` and have a better `x` spread our start and end points will be 
        # a half step in.
        x_step_size = example_pa_data.core_tip_radius * u.m / num_x_points
        x_limits = (0.5 * x_step_size, (num_x_points - 0.5) * x_step_size)
        side_theta_step_size = max_theta(example_pa_data.core_tip_radius, example_pa_data.shell_tip_radius, example_pa_data.core_tip_height, example_pa_data.shell_tip_height, True) * u.rad / num_theta_points
        side_theta_limits = (
            0.5 * side_theta_step_size, side_theta_step_size * (num_theta_points - 0.5)
        )
        side_v_limits = (
            minimum_velocity(side_theta_limits[1], V_probe, V_plasma, cap=False, x=0, core_radius=example_pa_data.core_tip_radius),
            max(np.linalg.norm(np.cross(v_drift, tip_direction)), 0) + (2 * T_multiples * max(T_parallel, T_perpendicular) / particle_mass)**0.5
        )

        x_points, dx = np.linspace(*x_limits, num_x_points, retstep=True)
        side_theta_points, dtheta = np.linspace(*side_theta_limits, num_theta_points, retstep=True)
        side_v_points, dv = np.linspace(*side_v_limits, num_v_points, endpoint=False, retstep=True)
        # side_particle_flux = np.sum(integrand(
        #     side_v_points, x_points, side_theta_points, phi_points, False
        # )) * dphi * dx * dtheta * dv
        side_particle_flux = _side_particle_flux(
            side_v_points, side_theta_points * u.rad, phi_points, x_points, transformation_matrix,
            example_pa_data, n, T_parallel, T_perpendicular, V_probe, V_plasma, 
            v_drift, magnetic_field_direction
        ) * dphi * dx * dtheta * dv
    else:
        side_particle_flux = np.zeros_like(cap_particle_flux)

    if isinstance(T_parallel, u.Quantity):
        if cumulative_fluxes:
            return cap_particle_flux, side_particle_flux, V_probe
        else:
            return cap_particle_flux, side_particle_flux
    else:
        if cumulative_fluxes:
            return cap_particle_flux.to(u.s**-1).value, side_particle_flux.to(u.s**-1).value, V_probe
        else:
            return cap_particle_flux.to(u.s**-1).value, side_particle_flux.to(u.s**-1).value
        

def convert_v_to_s(v_initial, v_final, temperature, particle_mass=c.m_e, num=50):
    """Convert the velocity to a better spacing of points so that integration is more accurate.
    
    Returns
    -------
    v_points : `numpy.ndarray[float]`
    d_s : `float`
    d_v_d_s : `numpy.ndarray[float]`
    """
    # We assume the integrand goes like `v * exp(-const * v**2)`
    const = particle_mass / (2 * temperature)
    s_initial = 1 - np.exp(-const * v_initial**2)
    s_final = 1 - np.exp(-const * v_final**2)

    if np.isclose(s_initial, 1) and np.isclose(s_initial, s_final):
        # Instead of using the converted velocities just use the velocity.
        d_v = (v_final - v_initial) / num
        d_s = const**0.5 * d_v
        v_points = np.linspace(v_initial + d_v / 2, v_final - d_v / 2, num=num)
        d_v_d_s = const**-0.5 * np.ones(num)

        return v_points, d_s, d_v_d_s
    else:
        d_s = (s_final - s_initial) / num
        s_points = np.linspace(s_initial + d_s / 2, s_final - d_s / 2, num=num)
        v_points = (-1 / const * np.log(1 - s_points))**0.5

        d_v_d_s = (2 * (1 - s_points) * const**0.5 * (-np.log(1 - s_points))**0.5)**-1

        return v_points, d_s, d_v_d_s


def average_temperature(T_parallel, T_perpendicular):
    """Average the temperature over all solid angle."""
    if np.isclose(T_parallel, T_perpendicular):
        return T_parallel
    elif T_parallel < T_perpendicular:
        angle = np.arccos((T_parallel / T_perpendicular)**0.5)
        if isinstance(T_parallel, u.Quantity):
            angle = angle.value
        return T_parallel * T_perpendicular * angle / (T_parallel * (T_perpendicular - T_parallel))**0.5
    else:
        angle = np.arccosh((T_parallel / T_perpendicular)**0.5)
        if isinstance(T_parallel, u.Quantity):
            angle = angle.value
        return T_parallel * T_perpendicular * angle / (T_parallel * (T_parallel - T_perpendicular))**0.5
    

def drift_velocity_normalization(
        average_T_e,
        particle_mass,
        charge_number=1
    ):
    """
    Get the normalization for velocity.
    
    Parameters
    ----------
    average_T_e, particle_mass : `astropy.units.Quantity` or `Float`
        Average plasma temperature (J) and particle mass (kg).
    """
    return (charge_number * average_T_e / particle_mass)**0.5


def core_ion_particle_flux(
        n,
        T_parallel, 
        T_perpendicular, 
        v_drift, 
        tip_direction,
        particle_mass,
        charge_number=1,
        example_pa_data=None,
    ):
    """Get the ion particle flux to the probe.

    Parameters
    ----------
    n, T_parallel, T_perpendicular : `astropy.units.Quantity` or `float`
        Plasma density (m**-3) and temperatures (eV).
    v_drift : `astropy.units.Quantity` or `numpy.ndarray`
        Plasma drift velocity (m/s) as a 1D-array of shape `(3,)` in machine coordinates.
    tip_direction : `numpy.ndarray[float]`
        1D- or 2D-array of shape `(3,)` or `(NUM_DIRECTIONS, 3)`.
    
    Returns
    -------
    ion_particle_flux : `float` of `numpy.ndarray[float]`
        Flux of ions to probe. If `tip_direction` is of shape `(3,)` then return 
        a `float`. Otherwise return a 1D-array of shape `(NUM_DIRECTIONS,)`.

    Notes
    -----
    This follows the analytic formula given by I. H. Hutchinson, “Ion collection 
    by a sphere in a flowing plasma: I. Quasineutral,” Plasma Phys. Control. 
    Fusion, vol. 44, no. 9, pp. 1953-1977, Sep. 2002, doi: 10.1088/0741-3335/44/9/313.
    """
    if example_pa_data is None:
        example_pa_data = PA_Probe1_Data(0)
    tip_area = total_area(example_pa_data.core_tip_radius, example_pa_data.shell_tip_radius, example_pa_data.core_tip_height, example_pa_data.shell_tip_height)
    if isinstance(T_parallel, u.Quantity):
        tip_area *= u.m**2
        T_parallel_Joule = T_parallel.to('J')
        T_perpendicular_Joule = T_perpendicular.to('J')
    else:
        T_parallel_Joule = T_parallel * c.e.to('C').value
        T_perpendicular_Joule = T_perpendicular * c.e.to('C').value
    
    average_T_e = average_temperature(T_parallel_Joule, T_perpendicular_Joule)
    unperturbed_random_flux_density = n * (average_T_e / particle_mass)**0.5
    velocity_normalization = drift_velocity_normalization(average_T_e, particle_mass, charge_number)

    # Equation (13).
    gamma_0 = 0.62
    K_u = 0.64
    K_d = 0.70
    drift_speed = np.linalg.norm(v_drift)
    if np.linalg.norm(v_drift) == 0:
        if len(tip_direction.shape) == 1:
            theta = 0
        else:
            theta = np.zeros(tip_direction.shape[0])
    else:
        theta = np.arccos(np.dot(tip_direction, v_drift / drift_speed))
    v_flow = drift_speed / velocity_normalization

    normalized_flux_density = gamma_0 * np.exp(0.5 * v_flow * ((1 - np.cos(theta)) * K_u - (1 + np.cos(theta)) * K_d))
    return unperturbed_random_flux_density * normalized_flux_density * tip_area


def visualize_core_particle_flux(
        n, 
        T_parallel, 
        T_perpendicular, 
        V_probe,
        V_plasma,
        v_drift, 
        magnetic_field_direction,
        tip_direction,
        particle_mass,
    ):
    transformation_matrix = machine_to_tip_coordinate_matrix(v_drift, magnetic_field_direction, tip_direction)

    pa_data = PA_Probe1_Data(0)
    fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})

    cap_max_theta = max_theta(pa_data.core_tip_radius, pa_data.shell_tip_radius, pa_data.core_tip_height, pa_data.shell_tip_height, cap=True)
    side_max_theta = max_theta(pa_data.core_tip_radius, pa_data.shell_tip_radius, pa_data.core_tip_height, pa_data.shell_tip_height, cap=False)

    all_colors = plt.get_cmap('tab20')(np.arange(8))
    shell_colors = all_colors[:2]
    core_colors = all_colors[2:4]
    side_color = core_colors[0]
    cap_color = core_colors[1]
    insulation_color = all_colors[5]
    side_min_v_color = all_colors[6]
    cap_min_v_color = all_colors[7]

    T_multiples = 5
    v_max = np.linalg.norm(v_drift) + (2 * T_multiples * T_parallel / particle_mass)**0.5
    v_values = np.linspace(0, v_max)
    theta_values = np.linspace(0, 2 * np.pi, num=100)
    
    area_theta_values = np.linspace(0, cap_max_theta)
    cap_areas = np.array(
        [projected_area(pa_data.core_tip_radius, pa_data.shell_tip_radius, pa_data.core_tip_height, pa_data.shell_tip_height, theta, area_function=_cap_area_func) for theta in area_theta_values]
    ) * u.m**2
    side_areas = np.array(
        [projected_area(pa_data.core_tip_radius, pa_data.shell_tip_radius, pa_data.core_tip_height, pa_data.shell_tip_height, theta, area_function=_side_area_func) for theta in area_theta_values]
    ) * u.m**2
    stack_to_v_ratio = 5 * np.max(cap_areas + side_areas) / v_max
    mirror_array = lambda arr, factor=1: np.concatenate((factor * np.flip(arr), arr))
    
    ax.stackplot(
        mirror_array(area_theta_values, -1), 
        mirror_array(side_areas / stack_to_v_ratio + v_max), 
        mirror_array(cap_areas / stack_to_v_ratio), 
        labels=['Side', 'Cap'], colors=(side_color, cap_color)
    )

    v_mesh, theta_mesh = np.meshgrid(v_values, theta_values)
    v_vec3 = transformation_matrix[2]
    v_vec1 = transformation_matrix[0]
    vector_mesh = v_mesh[:, :, np.newaxis] * (np.cos(theta_mesh)[:, :, np.newaxis] * v_vec3[np.newaxis, np.newaxis, :] + np.sin(theta_mesh)[:, :, np.newaxis] * v_vec1[np.newaxis, np.newaxis, :])
    distribution_function_mesh = drifting_bimaxwellian(vector_mesh, n, T_parallel, T_perpendicular, v_drift, magnetic_field_direction, particle_mass)
    cmesh = ax.contourf(theta_mesh, v_mesh, distribution_function_mesh)

    side_min_v = minimum_velocity(np.pi / 2, V_probe, V_plasma, cap=False, x=0 * u.m, core_radius=pa_data.core_tip_radius * u.m)
    side_theta_values = np.linspace(np.arcsin(side_min_v / v_max), np.pi * u.rad - np.arcsin(side_min_v / v_max))
    ax.plot(side_theta_values, side_min_v / np.sin(side_theta_values), color=side_min_v_color, label=r"$v_{\mathrm{min}}$ for side")
    ax.plot(-side_theta_values, side_min_v / np.sin(side_theta_values), color=side_min_v_color)
    
    cap_min_v = minimum_velocity(0, V_probe, V_plasma, cap=True)
    cap_theta_values = np.arccos(cap_min_v / v_max) * np.linspace(-1, 1)
    ax.plot(cap_theta_values, cap_min_v.to('m/s') / np.cos(cap_theta_values), color=cap_min_v_color, label=r"$v_{\mathrm{min}}$ for cap")

    ax.plot(theta_values, v_max * np.ones_like(theta_values), color='black')

    ax.scatter(np.arctan2(np.dot(v_vec1, v_drift), np.dot(v_vec3, v_drift)), np.linalg.norm(v_drift), color='red', marker='x')

    # ax.axvline(side_max_theta, color=side_color, linestyle='dotted')
    # ax.axvline(-side_max_theta, color=side_color, linestyle='dotted')
    # ax.axvline(cap_max_theta, color=cap_color, linestyle='dotted')
    # ax.axvline(-cap_max_theta, color=cap_color, linestyle='dotted')

    ax.set_ylabel('')
    ax.set_xlabel('')
    ax.set_yticks([])
    ax.set_xticks([])
    ax.set_theta_offset(np.pi / 2)
    # ax.set_ylim(0, v_max)

    # ax.legend()

    # ax.set_title(f"Max v: {v_max:.2e}, angle between tip and b: {np.rad2deg(np.arccos(np.dot(magnetic_field_direction, tip_direction))):.1f}")
    fig.colorbar(cmesh, ax=ax, label=r'$f_e(v)$')
    fig.patch.set_alpha(0.0)
    # fig.savefig('./analysis/pa_probe1/distribution_function.png', dpi=300)
    return fig, ax


if __name__ == "__main__":
    import tqdm
    particle_type = 'e-'
    particle_mass = c.m_e if particle_type == 'e-' else c.m_p
    n = 10**15 * u.m**-3
    T_parallel = 10 * u.eV
    T_perpendicular = 10 * u.eV
    drift_direction = np.array([0, 1, 0], dtype=float)
    drift_direction /= np.linalg.norm(drift_direction)
    v_drift = 0 * 10**5 * u.m / u.s * drift_direction
    magnetic_field_direction = np.array([1, 1, 0], dtype=float)
    magnetic_field_direction /= np.linalg.norm(magnetic_field_direction)
    tip_direction = np.array([1, 0, 0], dtype=float)
    V_plasma = 0 * u.V

    # fig, ax = plt.subplots(1, 1)
    # V_probe = np.linspace(0, -100, num=20) * u.V
    # cap_flux, side_flux, alt_V_probe = core_electron_particle_flux(
    #     n, T_parallel, T_perpendicular, V_probe, V_plasma, v_drift, magnetic_field_direction, np.array([1, 0, 0]), cylindrical_coords=True, cumulative_fluxes=True, calculate_side_flux=False
    # )
    # ax.plot(V_probe, cap_flux + side_flux, label='Cumulative')
    # cylindrical_electron_flux = np.zeros(V_probe.shape) / u.s
    # for i, V_p in enumerate(V_probe):
    #     cap_flux, side_flux = core_electron_particle_flux(
    #         n, T_parallel, T_perpendicular, V_p, V_plasma, v_drift, magnetic_field_direction, np.array([1, 0, 0]), cylindrical_coords=True, calculate_side_flux=False,
    #     )
    #     cylindrical_electron_flux[i] = cap_flux + side_flux
    # ax.plot(V_probe, cylindrical_electron_flux, label='Cylindrical')
    # spherical_electron_flux = np.zeros(V_probe.shape) / u.s
    # for i, V_p in enumerate(V_probe):
    #     cap_flux, side_flux = core_electron_particle_flux(
    #         n, T_parallel, T_perpendicular, V_p, V_plasma, v_drift, magnetic_field_direction, np.array([1, 0, 0]), calculate_side_flux=False
    #     )
    #     spherical_electron_flux[i] = cap_flux + side_flux
    # ax.plot(V_probe, spherical_electron_flux, label='Spherical')

    # ax.legend()
    # # ax.set_yscale('log')
    # plt.show()
    # exit()

    plot_particle_flux = False
    plot_electron_flux = False
    plot_ion_flux = False
    plot_current_vs_theta = False
    plot_voltage_vs_theta = True

    # visualize_core_particle_flux(n, T_parallel, T_perpendicular, -0 * u.V, 0 * u.V, v_drift, magnetic_field_direction, tip_direction, particle_mass)
    # visualize_drifting_bimaxwellian(n, T_parallel, T_perpendicular, v_drift, magnetic_field_direction, particle_mass)
    # plt.show()
    # exit()
    # import cProfile
    # cProfile.run('core_electron_particle_flux(n, T_parallel, T_perpendicular, -10 * u.V, 0 * u.V, v_drift, magnetic_field_direction, tip_direction, calculate_side_flux=False, cylindrical_coords=True)', './analysis/pa_probe1/analyze_iv_curve/particle_flux_stats_cylindrical')
    # exit()

    tip_angles = np.linspace(0, 2 * np.pi, num=64, endpoint=False)
    probe_voltages = np.linspace(-5 * max(T_parallel, T_perpendicular).value, 0, num=10) * u.V + V_plasma
    if plot_particle_flux:
        visualize_core_particle_flux(n, T_parallel, T_perpendicular, 0 * u.V, V_plasma, v_drift, magnetic_field_direction, tip_direction, particle_mass)

    colormap_name = 'viridis'
    colormap = mpl.cm.get_cmap(colormap_name)
    electron_fluxes = np.zeros((tip_angles.size + 1, probe_voltages.size)) * u.s**-1
    if plot_electron_flux or plot_current_vs_theta or plot_voltage_vs_theta:
        for i, tip_angle in enumerate(tqdm.tqdm(tip_angles)):
            tip_direction = np.array([np.cos(tip_angle), np.sin(tip_angle), 0])
            cap_fluxes, side_fluxes, _ = core_electron_particle_flux(
                n, T_parallel, T_perpendicular, probe_voltages, V_plasma, v_drift, 
                magnetic_field_direction, tip_direction, calculate_side_flux=False, 
                cylindrical_coords=True, cumulative_fluxes=True, num_v_perpendicular_points=8
            )
            electron_fluxes[i, :] = cap_fluxes + side_fluxes
            continue
            for j, V_probe in enumerate(probe_voltages):
                # cap_flux, side_flux = core_electron_particle_flux(n, T_parallel, T_perpendicular, V_probe, V_plasma, v_drift, magnetic_field_direction, tip_direction, num_phi_points=32, calculate_side_flux=False)
                cap_flux, side_flux = core_electron_particle_flux(
                    n, T_parallel, T_perpendicular, V_probe, V_plasma, v_drift, 
                    magnetic_field_direction, tip_direction, num_v_parallel_points=8,
                    calculate_side_flux=False, cylindrical_coords=True
                )
                electron_fluxes[i, j] = cap_flux + side_flux
                # visualize_core_particle_flux(n, T_parallel, T_perpendicular, V_probe, 0 * u.V, v_drift, magnetic_field_direction, tip_direction, particle_mass)
        electron_fluxes[-1] = electron_fluxes[0]
    
    drift_speeds = 10**np.linspace(0, np.log10(max(np.linalg.norm(v_drift).value, 10)), num=20) * u.m / u.s
    ion_fluxes = np.zeros((tip_angles.size + 1, drift_speeds.size)) * u.s**-1
    if plot_ion_flux:
        for i, tip_angle in enumerate(tqdm.tqdm(tip_angles)):
            tip_direction = np.array([np.cos(tip_angle), np.sin(tip_angle), 0])
            for j, drift_speed in enumerate(drift_speeds):
                ion_fluxes[i, j] = core_ion_particle_flux(n, T_parallel, T_perpendicular, drift_speed * v_drift / np.linalg.norm(v_drift), tip_direction, c.m_p)
        ion_fluxes[-1] = ion_fluxes[0]
    
    ion_flux = np.zeros(tip_angles.size + 1) * u.s**-1
    if plot_current_vs_theta or plot_voltage_vs_theta:
        for i, tip_angle in enumerate(tqdm.tqdm(tip_angles)):
            tip_direction = np.array([np.cos(tip_angle), np.sin(tip_angle), 0])
            ion_flux[i] = core_ion_particle_flux(n, T_parallel, T_perpendicular, v_drift, tip_direction, c.m_p)
        ion_flux[-1] = ion_flux[0]
    
    tip_angles = np.append(tip_angles, tip_angles[0] + 2 * np.pi)

    if plot_ion_flux:
        fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
        norm = mpl.colors.Normalize(np.log10(np.min(drift_speeds.value)), np.log10(np.max(drift_speeds.value)))
        for j, drift_speed in enumerate(drift_speeds):
            ax.plot(
                tip_angles, ion_fluxes[:, j], 
                label=f'Drift speed = {drift_speed:2.0e} m / s, $v_f$ = {(drift_speed / (average_temperature(T_parallel, T_perpendicular) / c.m_p)**0.5).to(""):.2f}',
                color=colormap(norm(np.log10(drift_speed.value)))
            )
        ax.set_xlabel(r'$\theta_T$')
        ax.set_ylabel(r'$\Gamma_i$')
        ax.set_title("Ion flux to probe")
        # ax.legend()
        fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=colormap), ax=ax, label='$\log_{10}(v_D)$')
    
    if plot_electron_flux:
        fig, (theta_vs_v_ax, theta_vs_i_ax) = plt.subplots(1, 2, subplot_kw={'projection': 'polar'})
        contourf_plot = theta_vs_v_ax.contourf(tip_angles, probe_voltages, electron_fluxes.T, cmap=colormap)
        # ax.scatter(*np.meshgrid(tip_angles, probe_voltages), color='red', marker='.')
        theta_vs_v_ax.set_ylim(np.min(probe_voltages), np.max(probe_voltages))
        theta_vs_v_ax.set_title(r"$\Gamma_e \mathrm{ vs. } \theta_T, V_p$")
        fig.colorbar(contourf_plot, ax=theta_vs_v_ax)
        
        for constant_v_flux, v_probe in zip(electron_fluxes.T, probe_voltages):
            theta_vs_i_ax.plot(tip_angles, constant_v_flux, color=colormap((v_probe - np.min(probe_voltages)) / (np.max(probe_voltages) - np.min(probe_voltages))))
        theta_vs_i_ax.set_title(r"$\Gamma_e \mathrm{ vs. } \theta_T$")

    if plot_current_vs_theta:
        fig = plt.figure()
        fig.suptitle("Total current to probe vs. direction")

        gs = mpl.gridspec.GridSpec(1, 3, width_ratios=(1, 1, 0.1))
        positive_current_ax = fig.add_subplot(gs[0], projection='polar')
        negative_current_ax = fig.add_subplot(gs[1], projection='polar')
        colorbar_ax = fig.add_subplot(gs[2])
        
        total_currents = (c.e.si * (ion_flux[None, :] - electron_fluxes.T)).to('A')

        norm = mpl.colors.Normalize(np.min(probe_voltages).value, np.max(probe_voltages).value)
        for i in range(probe_voltages.size - 1):
            color = colormap(norm((probe_voltages[i] + probe_voltages[i + 1]).value / 2))
            positive_current_ax.fill_between(
                tip_angles, np.maximum(total_currents[i], 0), np.maximum(total_currents[i + 1], 0),
                color=color
            )
            negative_current_ax.fill_between(
                tip_angles, np.maximum(-total_currents[i], 0), np.maximum(-total_currents[i + 1], 0),
                color=color
            )

        positive_current_ax.plot(tip_angles, c.e.si * ion_flux, color='grey', linestyle='dashed')
        positive_current_ax.plot(tip_angles, total_currents[probe_voltages == V_plasma][0], color='grey', linestyle='dotted')
        negative_current_ax.plot(tip_angles, -total_currents[probe_voltages == V_plasma][0], color='grey', linestyle='dotted')
        
        positive_current_ax.set_ylim(0, np.max(total_currents) * 1.1)
        negative_current_ax.set_ylim(0, -np.min(total_currents) * 1.1)
        positive_current_ax.set_ylabel('')
        negative_current_ax.set_ylabel('')
        positive_current_ax.set_title(r"$I_p > 0$ [A]")
        negative_current_ax.set_title(r"$I_p < 0$ [A]")

        fig.colorbar(mpl.cm.ScalarMappable(norm, colormap_name), cax=colorbar_ax, label=r'$V_p$ [V]')

    if plot_voltage_vs_theta:
        total_currents = (c.e.si * (ion_flux[None, :] - electron_fluxes.T)).to('A')

        fig = plt.figure()

        ax = fig.add_subplot(projection='polar')
        contourf_plot = ax.contourf(tip_angles, probe_voltages, total_currents, cmap='PiYG', norm=mpl.colors.CenteredNorm())
        fig.colorbar(contourf_plot, ax=ax)
    
    plt.show()
