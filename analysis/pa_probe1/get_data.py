"""Get data about the pa probe from the MDSplus tree."""
from modules.generic_get_data import Get, Data, Port, lazy_get
import numpy as np


class PA_Probe1_Data(Data):
    NUM_TIPS = 12

    def __init__(self, tree, *args, **kwargs):
        super().__init__(tree, [], [], *args, **kwargs)
        
    @lazy_get
    def vbias(self):
        """
        Returns
        -------
        float
            The bias voltage of the probe.
        """
        if self._static_vbias(self.shot_number) is not None:
            return self._static_vbias(self.shot_number)
        else:
            return self.get(Get("\\pa1_vbias", signal=False))
    
    def _static_vbias(self, shot_number):
        """
        Get the bias voltage from a static list.

        Returns
        -------
        float or None
            The bias voltage of the probe.
        """
        if shot_number < 66011 or shot_number > 66056:
            return None
        
        if shot_number < 66015:
            return 100
        elif shot_number < 66019:
            return 80
        elif shot_number < 66023:
            return 60
        elif shot_number < 66028:
            return 40
        elif shot_number < 66032:
            return 20
        elif shot_number < 66036:
            return 0
        elif shot_number < 66041:
            return -20
        elif shot_number < 66045:
            return -40
        elif shot_number < 66049:
            return -60
        elif shot_number < 66053:
            return -80
        else:
            return -100

    @lazy_get
    def vbias_curve(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of the bias voltage curve. Of shape `(NUM_TIMES,)`.
        """
        return self.get(Get("(\\pa1_vbias_raw - \\pa1_vbias_off) / \\pa1_vbias_div"))

    @lazy_get
    def bdot_raw(self):
        """
        Returns
        -------
        np.array[float]
            3D-array of raw Bdot values measured by the digitizer. Of shape 
            `(3, 2, NUM_TIMES)` where the second axis is for the two coil 
            directions.
        """
        return self.get(Get("[ " + ", ".join(
            "[ " + ", ".join(f'DATA(\\pa1_bdot_ax{ax_number}.raw_{coil_direction})' for coil_direction in ['cw', 'ccw']) + " ]" for ax_number in range(1, 4)
        ) + " ]", name='pa1_bdot_raw'))

    @lazy_get
    def bdot_probe(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of Bdot values in probe coordinates ordered as 
            `(x, y, z)`. Of shape `(3, NUM_TIMES)`.
        """
        return self.get(Get("[" + ", ".join('DATA(\\pa1_bdot_ax{})'.format(i) for i in range(1, 4)) + "]", name='pa1_bdot_probe'))

    @lazy_get
    def bdot_machine(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of Bdot values in machine coordinates ordered as 
            `(r, phi, z)`. Of shape `(3, NUM_TIMES)`.
        """
        return self.get(Get("[" + ", ".join('DATA(\\pa1_bdot_ax{})'.format(dir) for dir in ['r', 'phi', 'z']) + "]", name='pa1_bdot_machine'))

    @lazy_get
    def direction(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of shape `(NUM_PROBES, 3)` of machine directions of each 
            tip where the coordinates are ordered as `[r, phi, z]`.
        """
        return self.get(Get("\\pa1_direction", signal=False))

    @lazy_get
    def icore(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of core currents. Of shape `(NUM_TIPS, NUM_TIMES)`.
        """
        return self.get(Get("\\pa1_icore"))

    @lazy_get
    def ishell(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of shell currents. Of shape `(NUM_TIPS, NUM_TIMES)`.
        """
        return self.get(Get("\\pa1_ishell"))

    @lazy_get
    def vcore(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of core voltages. Of shape `(NUM_TIPS, NUM_TIMES)`.
        """
        return self.get(Get("\\pa1_vcore"))

    @lazy_get
    def vshell(self):
        """
        Returns
        -------
        np.array[float]
            2D-array of shell voltages. Of shape `(NUM_TIPS, NUM_TIMES)`.
        """
        return self.get(Get("\\pa1_vshell"))

    @lazy_get
    def working(self):
        """
        Returns
        -------
        np.array[bool]
            1D-array of shape `(NUM_TIPS,)` containing the status of each tip.
        """
        return self.get(Get("\\pa1_working", signal=False), np_data_type=bool)
    
    @lazy_get
    def v_core_raw_signal(self):
        """
        The raw signal for the core signal measurement. This is corrected for the relay state.

        Returns
        -------
        np.array[float]
            2D-array of core signal voltages. Of shape `(NUM_TIPS, NUM_TIMES)`.
        """
        return self.get(Get(
            "[" + ", ".join('DATA(\\pa1_{}_csv)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 13)) + "]", name='pa1_v_core_raw_signal'
        ))
    
    @lazy_get
    def v_core_raw_noise(self):
        """
        The raw signal for the core noise measurement. This is corrected for the relay state.

        Returns
        -------
        np.array[float]
            2D-array of core noise voltages. Of shape `(NUM_TIPS, NUM_TIMES)`.
        """
        return self.get(Get(
            "[" + ", ".join('DATA(\\pa1_{}_cnv)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 13)) + "]", name='pa1_v_core_raw_noise'
        ))

    @lazy_get
    def v_shell_raw_signal(self):
        """
        The raw signal for the shell signal measurement.

        Returns
        -------
        np.array[float]
            2D-array of shell signal voltages. Of shape `(NUM_TIPS, NUM_TIMES)`.
        """
        return self.get(Get(
            "[" + ", ".join('DATA(\\pa1_{}_ssv)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 13)) + "]", name='pa1_v_shell_raw_signal'
        ))

    @lazy_get
    def v_shell_raw_noise(self):
        """
        The raw signal for the shell noise measurement.

        Returns
        -------
        np.array[float]
            2D-array of shell signal voltages. Of shape `(NUM_TIPS, NUM_TIMES)`.
        """
        return self.get(Get(
            "[" + ", ".join('DATA(\\pa1_{}_snv)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 13)) + "]", name='pa1_v_shell_raw_noise'
        ))

    @lazy_get
    def r_diggnd(self):
        """
        Returns
        -------
        np.array[float]
            The resistance from the digitizer input to ground.
        """
        return self.get(Get(
            "[" + ", ".join('DATA(\\pa1_{}_rdiggnd)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 13)) + "]", name='pa1_r_diggnd', signal=False
        ))

    @lazy_get
    def r_div(self):
        """
        Returns
        -------
        np.array[float]
            The resistance across the divider from the low pass capacitor to ground.
        """
        return self.get(Get(
            "[" + ", ".join('DATA(\\pa1_{}_rdiv)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 13)) + "]", name='pa1_r_div', signal=False
        ))

    @lazy_get
    def r_sense(self):
        """
        Returns
        -------
        np.array[float]
            The resistance of the sense resistor.
        """
        return self.get(Get(
            "[" + ", ".join('DATA(\\pa1_{}_rsense)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 13)) + "]", name='pa1_r_sense', signal=False
        ))

    @lazy_get
    def noise_relay(self):
        """
        Returns
        -------
        bool
            Whether the noise relay is on or off.
        """
        return self.get(Get('\\pa1_noise_relay', signal=False), np_data_type=bool)
    
    @lazy_get
    def tip_phi_deg(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of tip phi directions in degrees where phi is the 
            azimuthal angle. Of shape `(NUM_TIPS,)`.
        """
        return self.get(Get("[" + ", ".join('DATA(\\pa1_{}_phi)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 13)) + "]", name='pa1_phi_deg', signal=False))
    
    @lazy_get
    def tip_phi_rad(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of tip phi directions in radians where phi is the 
            azimuthal angle. Of shape `(NUM_TIPS,)`.
        """
        return np.deg2rad(self.tip_phi_deg)
    
    @lazy_get
    def tip_theta_deg(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of tip theta directions in degrees where theta is the 
            polar angle. Of shape `(NUM_TIPS,)`.
        """
        return self.get(Get("[" + ", ".join('DATA(\\pa1_{}_theta)'.format(str(tip_num).zfill(2)) for tip_num in range(1, 13)) + "]", name='pa1_theta_deg', signal=False))
    
    @lazy_get
    def tip_theta_rad(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of tip theta directions in radians where theta is the polar
            angle. Of shape `(NUM_TIPS,)`.
        """
        return np.deg2rad(self.tip_theta_deg)

    @lazy_get
    def time(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of time values in seconds. Of shape `(NUM_TIMES,)`.
        """
        return self.get(Get("dim_of(\\pa1_ishell)", name='pa1_time'))

    @lazy_get
    def position(self):
        """
        Returns
        -------
        np.array[float]
            1D-array of the position of the probe in machine coordinates, 
            `(r, phi, z)`. `r` and `z` are in meters and `phi` is in radians. 
            Of shape `(3,)`.
        """
        return self.get(Get("[" + ", ".join('DATA(\\pa1_{})'.format(dir) for dir in ['r', 'phi', 'z']) + ']', name='pa1_position', signal=False))
        
    @lazy_get
    def port(self) -> Port:
        """
        Returns
        -------
        Port
            Object that represents the port position of this probe.
        """
        return Port(self, 'pa1_')
    
    @lazy_get
    def shell_tip_radius(self):
        """
        Returns
        -------
        float
            The radius of the shell in meters
        """
        return 0.0008255 

    @lazy_get
    def core_tip_radius(self):
        """
        Returns
        -------
        float
            The radius of the core in meters.
        """
        return 0.0002921
    
    @lazy_get
    def shell_tip_height(self):
        """
        Returns
        -------
        float
            Height of shell above the inner insulation in meters.
        """
        # TODO: Figure out what this value should be.
        return 0.004
    
    @lazy_get
    def core_tip_height(self):
        """
        Returns
        -------
        float
            Height of core above the inner insulation in meters.
        """
        # TODO: Figure out what this value should be.
        return 0.003

    def extract_working(self, signal):
        return signal[np.nonzero(self.working)]

    def extract_not_working(self, signal):
        return signal[np.nonzero(1 - self.working)]
