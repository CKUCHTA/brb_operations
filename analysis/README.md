# analysis

This project contains the codes used for analyzing probes using a remote connection.

## Package dependencies

All packages needed can be found in `brb_operations/analysis/conda_environment.yml`. These packages are:

```yaml
  - python
  - jupyter
  - mpmath
  - numpy
  - scipy
  - matplotlib
  - rope
  - mdsplus
  - tqdm
  - configobj
  - pyyaml
  - sympy
```

To install these packages we will use `mamba`. To install `mamba`, follow [the install instructions](https://mamba.readthedocs.io/en/latest/installation.html).

Once installed, we can then install the required packages into a virtual environment. The default environment name is `research`. To install the environment with this name, run the following:

```shell
mamba env create -f <path>/conda_environment.yml
```

`<path>/conda_environment.yml` is the path to the `conda_environment.yml` file which is in the `brb_operations/analysis/` directory. If you want a different name, use the following:

```shell
mamba env create -f <path>/conda_environment.yml -n myenv
```

`myenv` is the name of the environment you want to create.

For the Mac silicon processor, MDSplus does not exist for the `arm64` architecture. Thus you should use the following which forces mamba to use the `x86` architecture instead.

```shell
CONDA_SUBDIR=osx-64 mamba env create -f <path>/conda_environment.yml
```

## Setup

In order to run this on your own computer make sure that your python install can find all folders in the `brb_operations/analysis` directory. To do this, we need to put a `.pth` file into the `site-packages` directory that python uses. To find the directory for our conda environment run the following:

```shell
conda activate research
python
```

Once in the python terminal, run the following two commands.

```python
>>> from distutils.sysconfig import get_python_lib
>>> print(get_python_lib())
```

`research` is the name of the conda environment previously created. If you called your environment something else, please change the first line. If you are using Mamba or Conda then there should be a directory in the path named `mambaforge` or something similar.

Now create a file called `brb_operations_analysis_modules.pth` in the directory that ends with `site-packages` from the prior commands. In the new file add a single line that is the path to the `analysis` directory in this repository. This pathname will end in `/brb_operations/analysis`.

To test that you have done this correctly activate python and try importing the following:

```python
from modules.shot_loader import get_remote_shot_tree
```

If this does not throw a `ModuleNotFoundError` then you should be all set!

### Installing MDSplus on Mac Silicon

Paul's MDSplus connection works so let's try to recreate that. When setting up Paul's computer, we used `mamba` and created an environment called `DriveCylinder`. This environment runs python version 3.10.12. We are able to run MDSplus from the python terminal. Paul's matlab points towards the python in the `DriveCylinder` environment. I've exported Paul's mamba environment into a `.yml` file and sent it to Mary.

When trying to create an environment from the sent `.yml`, it looks like mamba is not using the `CONDA_SUBDIR=osx-64` by default even though that is how those packages were installed. Thus we will prepend the creation command which looks like

```shell
CONDA_SUBDIR=osx-64 mamba env create -f <path>/conda_environment.yml
```

Now we have successfully created the same environment on Mary's computer. It looks like Paul did not install MDSplus using the `.pkg` file and instead may have used the `mdsplus-stable.tar`. I've untarred the `.tar` and have moved that `mdsplus-stable` directory into `~/Documents/`. Additionally, prior installs of MDSplus onto Mary's computer has added some stuff to the `/etc/profile` file and some other places. These shouldn't be an issue (hopefully).

We need to add the MDSplus matlab files to matlab so that they are accessible. To do so we run `addpath('\<path>/mdsplus-stable')` and then `savepath` in the matlab terminal. This will make the accessible whenever we open matlab.

Now we change the `pyenv` so that it points to the correct python. To find the correct python we start the mamba environment by running `mamba activate DriveCylinder` and then running `which python` (both should be run in terminal). Then we run `pyenv(Version="\<path-to-python>")` in the matlab terminal.

We also need to change the execution mode by running `pyenv(ExecutionMode="OutOfProcess")` in the matlab terminal.

Then we run `mdsUsePython(1)` in the matlab terminal to make it use python for connecting and then we can run `mdstest()`. If that returns `1` we did it! If not, let's debug :(.

You can add `mdsUsePython(1)` to the `startup.m` file for matlab to make it automatically use python to connect.

## Usage

Each directory in the `analysis` directory holds a `get_data.py` file. In these files is an object, which we'll call `Data_Object`, that gives us easy access to some data from MDSplus. They all follow the same usage:

### `variable = Data_Object(shot_number)`

More options can be found in `modules/generic_get_data.py` in the `Data` object as each of the `Data_Object`s inherit this object.

Let's use the `speed_bdot1` directory as an example for retreiving data. To import the object into your code, add a line that looks like

```python
from speed_bdot1.get_data import Speed_Bdot1_Data
```

Then we need only create a variable to hold the data and pass a shot number.

```python
speed_data = Speed_Bdot1_Data(54000)
```

From here we can access any of the attributes of the `Speed_Bdot1_Data` object (you can check them out by opening the respective `get_data.py` file).

Let's try plotting some stuff! We'll use the `matplotlib` `imshow` command to plot a heatmap of the changing magnetic field in the z direction.

```python
import matplotlib.pyplot as plt
plt.imshow(speed_data.db_z, aspect='auto')
plt.show()
```

We get a pretty boring image though! It's mostly just green. There's a small vertical sliver between 65000 and 66000 where interesting things are happening. To load only data in a certain region we can either use `time_range` or `time_index_range`.

- `time_range=(start_time, end_time)` uses time in seconds to select specific data (you can get a time array using `speed_data.time`). `
- `time_index_range=(start_index, end_index)` uses the indeces of the array to select a subset of data.

Now change the previous object to

```python
speed_data = Speed_Bdot1_Data(54000, time_index_range=(65000, 66000))
```

If you run the previous plotting command now you will see some much more interesting features!

### Offline usage

You can also download data if you don't want to connect to the servers each time. Below is an example script.

```python
from speed_bdot1.get_data import Speed_Bdot1_Data

speed_data = Speed_Bdot1_Data(54000)
# Call the attributes we want to save.
speed_data.time
speed_data.r
speed_data.db_r
speed_data.db_phi
speed_data.db_z

# Data is saved as `.mat` files.
save_filepath = "./speed_data_54000.mat"
speed_data.save(save_filepath)

# Load the data into a new object.
loaded_speed_data = Speed_Bdot1_Data(54000, load_filepath=save_filepath)

# Now we can call the previous attributes without calling the database!
print(loaded_speed_data.time)
# We can still call attributes that weren't saved.
print(loaded_speed.z)
```

## Contributing

When adding new probes/measurements we need to create a directory and file.

The directory should have the same name as the MDSplus tree and the file should be called `get_data.py`. This file should contain a class that inherits from the `Data` class found in `modules/generic_get_data.py`.
