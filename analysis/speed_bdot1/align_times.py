"""Fix the timing between the two digitizers used for the speed probe by getting a new time array for each set of 4 coils."""
from speed_bdot1.get_data import Speed_Bdot1_Data
import numpy as np
from scipy.fft import rfft, rfftfreq
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import logging


def damped_oscillator_model(t, magnitude_high, decay_time, phase, angular_frequency_high, magnitude_low, angular_frequency_low):
    return magnitude_high * np.exp(-t / decay_time) * np.cos(angular_frequency_high * t + phase) - magnitude_low * np.cos(angular_frequency_low * t)

def get_phase(frequency_array, intensity_array):
    """
    Get the phase for the high frequency signal.
    """
    max_intensity_index = np.argmax(np.where(frequency_array >= 10**6, np.abs(intensity_array), 0))
    return np.arctan2(np.imag(intensity_array[max_intensity_index]), np.real(intensity_array[max_intensity_index]))

def plot_results(time_array, r_position_arrays, signal_arrays, frequency_array, intensity_arrays, fit_parameters, index_bounds_of_interest, digitizer_names=["acq114", "acq129"]):
    """
    Parameters
    ----------
    time_array : np.array[float]
        1D-array of time values for the signals.
    r_position_arrays : list[np.array[float]]
        Length 2 list of 1D-arrays containing the radius at which each signal is taken.
    signal_arrays : list[list[np.array[float]]]
        Length 2 list of lists of 1D-arrays. The first index is for which digitizer it's connected to and the second is the probe index.
    frequency_array : np.array[float]
        1D-array of frequency values for the signals after the FFT.
    intensity_arrays : list[list[np.array[float]]]
        Length 2 list of lists of 1D-arrays. These are the intensities from the signal arrays after taking an FFT.
    fit_parameters : list[list[np.array[float]]]
        1D-arrays of model parameters for each signal.
    index_bounds_of_interest : tuple[int]
        Start and end indeces that are used in the FFT.
    digitizer_names : list[str], default=["acq114", "acq129"]
    """
    fig, ((colormesh_ax, signal_ax), (fft_ax, phase_ax)) = plt.subplots(2, 2)
    fig.suptitle("Time Difference Calculation Routine")
    colors = ['red', 'blue']

    logging.debug("Plotting colormesh")
    all_r_positions = np.concatenate(r_position_arrays)
    sorted_r_indeces = np.argsort(all_r_positions)
    all_signals = np.concatenate([np.vstack(signal_sublist) for signal_sublist in signal_arrays])
    colormesh_ax.pcolormesh(time_array, all_r_positions[sorted_r_indeces], all_signals[sorted_r_indeces])
    colormesh_ax.set_xlabel("Time (s)")
    colormesh_ax.set_ylabel("Radius (m)")
    colormesh_ax.set_yticks(all_r_positions[sorted_r_indeces])
    digitizer_indeces = np.concatenate([index * np.ones_like(r_position_array) for index, r_position_array in enumerate(r_position_arrays)]).astype(int)
    for tick_index, r_tick in enumerate(colormesh_ax.get_yticklabels()):
        r_tick.set_color(colors[digitizer_indeces[sorted_r_indeces][tick_index]])
    colormesh_ax.set_title("Speed Probe Colormesh")

    logging.debug("Plotting signals")
    for digitizer_index, (signal_sublist, param_sublist) in enumerate(zip(signal_arrays, fit_parameters)):
        for signal_index, (signal_array, param_array) in enumerate(zip(signal_sublist, param_sublist)):
            signal_label = None if signal_index != 0 else digitizer_names[signal_index]
            signal_ax.plot(time_array, signal_array, color=colors[digitizer_index], label=signal_label)
            model_label = None if signal_index != 0 else digitizer_names[signal_index] + " model"
            model_time_points = time_array[index_bounds_of_interest[0]:index_bounds_of_interest[1]]
            signal_ax.plot(
                model_time_points, damped_oscillator_model(model_time_points, *param_array), 
                color=colors[digitizer_index], label=model_label, linestyle='dotted'
            )

    signal_ax.axvspan(time_array[index_bounds_of_interest[0]], time_array[index_bounds_of_interest[1]], color='green', alpha=0.2)
    signal_ax.set_xlim(time_array[0], time_array[-1])

    signal_ax.set_title("Speed Probe Signals")
    signal_ax.set_xlabel(r"Time (s)")
    signal_ax.set_ylabel(r"$\partial_t B_z$ (T/s)")
    signal_ax.legend()

    logging.debug("Plotting fft")
    for digitizer_index, (intensity_sublist, param_sublist) in enumerate(zip(intensity_arrays, fit_parameters)):
        digitizer_name = digitizer_names[digitizer_index]
        for intensity_index, (intensity_array, param_array) in enumerate(zip(intensity_sublist, param_sublist)):
            intensity_label = None if intensity_index != 0 else digitizer_name
            color = colors[digitizer_index]
            fft_ax.plot(frequency_array, np.abs(intensity_array), color=color, label=intensity_label)
            model_label = None if intensity_index != 0 else digitizer_name + " model"
            model_points = damped_oscillator_model(time_array[index_bounds_of_interest[0]:index_bounds_of_interest[1]], *param_array)
            model_points -= np.mean(model_points)
            model_intensity_array = rfft(model_points)
            fft_ax.plot(frequency_array, np.abs(model_intensity_array), color=color, label=model_label, linestyle='dotted')
            fft_ax.scatter(param_array[3] / (2 * np.pi), param_array[0], color=color, label=None if intensity_index != 0 else digitizer_name + " model amplitude")
    
    fft_ax.set_title("Signal FFT and model amplitudes")
    fft_ax.set_xlabel("Frequency (Hz)")
    fft_ax.set_ylabel("Intensity")
    fft_ax.legend()

    logging.debug("Plotting phase")
    phase_ax.grid()
    for digitizer_index in range(len(r_position_arrays)):
        for intensity_array, param_array, r_position in zip(intensity_arrays[digitizer_index], fit_parameters[digitizer_index], r_position_arrays[digitizer_index]):
            color = colors[digitizer_index]
            phase_ax.scatter(get_phase(frequency_array, intensity_array), r_position, color=color)
            phase_ax.scatter(param_array[2], r_position, marker='x', color=color)
            # phase_ax.axvline(np.median(fft_phase_values[0]), color='red')
    phase_ax.set_xlabel("Phase (rad)")
    phase_ax.set_ylabel("Radius (m)")
    phase_ax.set_xticks(np.linspace(-2 * np.pi, 2 * np.pi, 5), [r'$-2\pi$', r'$-\pi$', r'$0$', r'$\pi$', r'$2\pi$'])
    phase_ax.legend()

    plt.show()


def get_bounds_of_interest(signal_arrays):
    """
    Parameters
    ----------
    signal_arrays : list[list[np.array[float]]]
        Length 2 list of lists of 1D-arrays. The first index is for which digitizer it's connected to and the second is the probe index.

    Notes
    -----
    This method works best for vacuum shots. If applying this to plasma shots, 
    the ringing persists for much less time so the end index should be changed.
    """
    # Get the index when the bdot signal is less than some value.
    start_of_wave_bdot = -1000
    start_indeces = []
    for signal_sublist in signal_arrays:
        for signal_array in signal_sublist:
            start_indeces.append(np.argmax(signal_array < start_of_wave_bdot))
    mean_start_index = int(np.mean(start_indeces))

    return (mean_start_index, mean_start_index + 60)


def calculate_time_difference(speed_data_vac_shot, probe_indeces=np.arange(0, 13), plot_routine=False):
    """
    Parameters
    ----------
    speed_data_vac_shot : Speed_Bdot1_Data
        Speed data for a vacuum shot.
    probe_indeces : np.array[int], default=np.arange(0, 13)
        Indeces of which probes to use to get the time difference. These should 
        be probes at radius less than the drive coil/cylinder radius.

    Returns
    -------
    time_difference : float
        Time when even probes start sampling - time when odd probes start sampling.
    """
    fft_phase_values = [[], []] # (even, odd)
    signal_arrays = [[], []]
    r_position_arrays = [[], []]
    intensity_arrays = [[], []]
    fit_parameters = [[], []]

    for probe_index in probe_indeces:
        r_position_arrays[probe_index % 2].append(speed_data_vac_shot.exact_coil_r[0, probe_index]) # db_1a
        r_position_arrays[probe_index % 2].append(speed_data_vac_shot.exact_coil_r[1, probe_index]) # db_1b

        for signal_data in [speed_data_vac_shot.db_1a[probe_index], speed_data_vac_shot.db_1b[probe_index]]:
            signal_arrays[probe_index % 2].append(signal_data)

    index_bounds_of_interest = get_bounds_of_interest(signal_arrays)

    for probe_index in probe_indeces:
        for signal_data in [speed_data_vac_shot.db_1a[probe_index], speed_data_vac_shot.db_1b[probe_index]]:
            fft_data = signal_data[index_bounds_of_interest[0]:index_bounds_of_interest[1]].copy()
            fft_data -= np.mean(fft_data)
            frequencies = rfftfreq(fft_data.size, speed_data_vac_shot.time[1] - speed_data_vac_shot.time[0])
            intensities = rfft(fft_data)
            intensity_arrays[probe_index % 2].append(intensities)

            fft_phase_values[probe_index % 2].append(get_phase(frequencies, intensities))

            time_points_of_interest = speed_data_vac_shot.time[index_bounds_of_interest[0]:index_bounds_of_interest[1]]
            signal_points_of_interest = signal_data[index_bounds_of_interest[0]:index_bounds_of_interest[1]]
            max_index = np.argmax(np.where(frequencies > 10**6, np.abs(intensities), 0))
            initial_guess = [
                (np.max(signal_points_of_interest) - np.min(signal_points_of_interest)) / 2, 
                3 * 10**-6, np.pi, 2 * np.pi * frequencies[max_index], -np.mean(signal_points_of_interest), 2 * 10**4
            ]
            try:
                fit_params, covariance = curve_fit(
                    damped_oscillator_model, time_points_of_interest, signal_points_of_interest, initial_guess
                )
            except RuntimeError:
                fit_params = initial_guess

            fit_parameters[probe_index % 2].append(fit_params)

    if plot_routine:
        plot_results(speed_data_vac_shot.time, r_position_arrays, signal_arrays, frequencies, intensity_arrays, fit_parameters, index_bounds_of_interest)

    phase_difference = np.median(fft_phase_values[0]) - np.median(fft_phase_values[1])
    oscillation_frequency = np.mean([np.median([fit_params[3] / (2 * np.pi) for fit_params in fit_parameters[i]]) for i in range(2)])
    time_difference = phase_difference / (2 * np.pi * oscillation_frequency)
    sample_difference = time_difference / (speed_data_vac_shot.time[1] - speed_data_vac_shot.time[0])
    logging.info("Phase difference: {}".format(phase_difference))
    logging.info("Oscillation frequency: {}".format(oscillation_frequency))
    logging.info("Time difference: {}".format(time_difference))
    logging.info("Sample difference: {}".format(sample_difference))

    return time_difference


def get_corrected_time_array(uncorrected_time_array, speed_data_vac_shot, probe_indeces=np.arange(0, 13), plot_routine=False):
    """
    Get an array of time values with the digitizer times corrected.
    
    Parameters
    ----------
    uncorrected_time_array : np.array[float]
        2D-array of time points of shape `(NUM_PROBES, NUM_TIMES)`.
    speed_data_vac_shot : Speed_Bdot1_Data
        Speed data for a vacuum shot.
    probe_indeces : np.array[int], default=np.arange(0, 13)
        Indeces of which probes to use to get the time difference. These should 
        be probes at radius less than the drive coil/cylinder radius.
    plot_routine : bool, default=False
        Whether to plot the method used for getting the new times.

    Returns
    -------
    corrected_time_array : np.array[float]
        2D-array of time points of shape `(NUM_PROBES, NUM_TIMES)`.
    """
    time_difference = calculate_time_difference(speed_data_vac_shot, probe_indeces, plot_routine)
    corrected_time_array = uncorrected_time_array.copy()
    corrected_time_array[::2] += time_difference

    return corrected_time_array


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    plt.rcParams['figure.figsize'] = (20,12)

    vac_shots = [59730, 59801, 59852, 59921, 60092, 60163, 60225, 60382]
    index_range = (65035, 65135)
    # speed_data = Speed_Bdot1_Data(vac_shots[-1], time_index_range=index_range)
    speed_data_vac_shot = Speed_Bdot1_Data(vac_shots[-1])

    calculate_time_difference(speed_data_vac_shot, plot_routine=True)
