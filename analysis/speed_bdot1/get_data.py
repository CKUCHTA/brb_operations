"""Get data about the speed probe from the MDSplus tree."""
from modules.generic_get_data import Get, Data, lazy_get, Port
import numpy as np
import logging


class Speed_Bdot1_Data(Data):
    """
    Hold all the data for the speed probe in one class for easier access and reduced network connections.
    
    To get data, call the data without the underscore treating it as an attribute.
    """
    NUM_PROBES = 16

    def __init__(self, tree, db_phi=False, db_r=False, db_z=False, positions=False, time=False, *args, **kwargs):
        """
        Load the data for some nodes from the tree.
        optional params:
        index_range = Tuple of two elements that contains the index range to pull from the data.
        """
        self.get_calls = [
            Get('\\speed_bdot1_dbphi'),
            Get('\\speed_bdot1_dbr'),
            Get('\\speed_bdot1_dbz'),
        ] + [
            Get('[' + ', '.join('DATA(\\speed_bdot1_pb{num}_{dir})'.format(num=str(probe).zfill(2), dir=direction) for probe in range(1, 17)) + ']', name='speed_{}_position'.format(direction), signal=False) for direction in ['phi', 'r', 'z']
        ] + [
            Get('dim_of(\\speed_bdot1_dbphi)', name='speed_bdot1_time')
        ]

        self._db_phi, self._db_r, self._db_z, self._phi, self._r, self._z, self._time = super().__init__(tree, [db_phi, db_r, db_z] + [positions] * 3 + [time], self.get_calls, *args, **kwargs)

    @lazy_get
    def db_phi(self):
        """
        dB_phi / dt for conglomerated coils of the speed probe.
        
        Returns
        -------
        np.array[float]
            Shape of `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(self.get_calls[0])

    @lazy_get
    def db_r(self):
        """
        dB_r / dt for conglomerated coils of the speed probe.
        
        Returns
        -------
        np.array[float]
            Shape of `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(self.get_calls[1])

    @lazy_get
    def db_z(self):
        """
        dB_z / dt for conglomerated coils of the speed probe.
        
        Returns
        -------
        np.array[float]
            Shape of `(NUM_PROBES, NUM_TIMES)`.
        """
        return self.get(self.get_calls[2])

    @lazy_get
    def db_1a(self):
        return self.get(Get('\\speed_bdot1_db1a'))
    
    @lazy_get
    def db_1a_working(self):
        # As the speed_bdot1 probe does not yet have a working attribute we manually set it.
        logging.warning("Using hard-coded `Speed_Bdot1.db_1a_working` attribute.")
        return np.ones(16, dtype=bool)

    @lazy_get
    def db_1b(self):
        return self.get(Get('\\speed_bdot1_db1b'))
    
    @lazy_get
    def db_1b_working(self):
        logging.warning("Using hard-coded `Speed_Bdot1.db_1b_working` attribute.")
        return np.ones(16, dtype=bool)

    @lazy_get
    def db_2(self):
        return self.get(Get('\\speed_bdot1_db2'))
    
    @lazy_get
    def db_2_working(self):
        logging.warning("Using hard-coded `Speed_Bdot1.db_2_working` attribute.")
        return np.array((0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1), dtype=bool)

    @lazy_get
    def db_3(self):
        return self.get(Get('\\speed_bdot1_db3'))
    
    @lazy_get
    def db_3_working(self):
        logging.warning("Using hard-coded `Speed_Bdot1.db_3_working` attribute.")
        return np.array((1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1), dtype=bool)

    @lazy_get
    def phi(self):
        return self.get(self.get_calls[3])

    @lazy_get
    def r(self):
        return self.get(self.get_calls[4])

    @lazy_get
    def z(self):
        return self.get(self.get_calls[5])

    @lazy_get
    def exact_coil_r(self):
        """
        Exact positions of each coil.

        Returns
        -------
        np.array[float]
            2D array of `r` positions in meters of shape `(4, # of probes)`. 
            Each row is for coil `db_1a`, `db_1b`, `db_2`, and `db_3`.
        """
        # Positions of each coil relative to the center of the probe.
        relative_coil_positions = np.array([0.00974767, 0.00453572, -0.00146792, -0.00885702])
        return relative_coil_positions[:, None] + self.r[None, :]

    @lazy_get
    def time(self):
        server_time_value = self.get(self.get_calls[6])
        if server_time_value.size == 0:
            logging.warning("Since the `speed_bdot1` time data doesn't work at the moment, I'm manually returning the timing.")
            return 10**-7 * np.arange(self.db_phi.shape[1])
        else:
            return server_time_value

    @lazy_get
    def port(self) -> Port:
        """
        Returns
        -------
        Port
            Object that represents the port position of this probe.
        """
        return Port(self, 'speed_bdot1_')

