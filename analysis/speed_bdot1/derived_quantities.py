from speed_bdot1.get_data import Speed_Bdot1_Data
from scipy.interpolate import interp1d
import numpy as np
from modules.bdot.interpolation import get_db_machine_coords
from modules.coordinates import Position, Vector


def baselined_db(speed_data: Speed_Bdot1_Data, nothing_happening_range=range(1000, 2000)):
    """
    Remove the offset from 0 that is an artifact from the digitizers.
    
    Parameters
    ----------
    speed_data : Speed_Bdot1_Data
    nothing_happening_range : range
        Range of indeces to use to zero out the bdot signals.

    Returns
    -------
    np.array[float]
        3D array of `db` values in T / s of shape `(4, NUM_PROBES, NUM_TIMES)`. 
        Each row is for coil `db_1a`, `db_1b`, `db_2`, and `db_3`.
    """
    baselined_db_1a = speed_data.db_1a - np.mean(speed_data.db_1a[:, nothing_happening_range], axis=-1)[:, None]
    baselined_db_1b = speed_data.db_1b - np.mean(speed_data.db_1b[:, nothing_happening_range], axis=-1)[:, None]
    baselined_db_2 = speed_data.db_2 - np.mean(speed_data.db_2[:, nothing_happening_range], axis=-1)[:, None]
    baselined_db_3 = speed_data.db_3 - np.mean(speed_data.db_3[:, nothing_happening_range], axis=-1)[:, None]

    return np.stack((baselined_db_1a, baselined_db_1b, baselined_db_2, baselined_db_3), axis=0)

def interp_bdot(speed_data: Speed_Bdot1_Data, baselined_db_vals, axis=0, time_arrays=None, **kwargs):
    """
    Create a set of interpolators for each coil direction that interpolate in either the `r` or `t` direction.

    Parameters
    ----------
    speed_data : Speed_Bdot1_Data
    baselined_db_vals : np.array[float]
        3D array of `db` values in T / s of shape `(4, NUM_PROBES, NUM_TIMES)`. 
        Each row is for coil `db_1a`, `db_1b`, `db_2`, and `db_3`.
    axis : int or str, default=0
        Axis along which to interpolate. `str` options are `'t'` or `'r'`.
    time_arrays : np.array[float], default=`None`
        Times for each probe of shape `(NUM_PROBES, NUM_TIMES)`. This is used 
        for correcting the difference in digitizer timing. If `None` then use 
        the `speed_data.time` array instead.
    kwargs : dict
        Keyword arguments to be passed to the interpolation.

    Returns
    -------
    db1_interp, db2_interp, db3_interp : func(float) -> np.array[float]
        Function that takes a float of `axis` units and return an array of 
        values for the other axis.
    """
    interpolation_kwargs = {'kind': 'cubic', 'fill_value': 'extrapolate'}
    for key in kwargs:
        interpolation_kwargs[key] = kwargs[key]

    if axis == 0 or axis == 'r':
        interpolation_kwargs['axis'] = 0
        db1_x_data = np.tile((speed_data.exact_coil_r[1] + speed_data.exact_coil_r[0]) / 2, (baselined_db_vals.shape[2], 1)).T
        db2_x_data = np.tile(speed_data.exact_coil_r[2], (baselined_db_vals.shape[2], 1)).T
        db3_x_data = np.tile(speed_data.exact_coil_r[3], (baselined_db_vals.shape[2], 1)).T
    elif axis == 1 or axis == 't':
        interpolation_kwargs['axis'] = 1
        if time_arrays is None:
            time_data = np.tile(speed_data.time, (baselined_db_vals.shape[1], 1))
        else:
            time_data = time_arrays

        db1_x_data = time_data
        db2_x_data = time_data
        db3_x_data = time_data
    else:
        raise ValueError("`axis` to interpolate over was not one of `(0, 'r', 1, 't')`.")

    db1_interp = interp1d(
        db1_x_data, (baselined_db_vals[0] + baselined_db_vals[1]) / 2, 
        **interpolation_kwargs
    )
    db2_interp = interp1d(
        db2_x_data, baselined_db_vals[2],
        **interpolation_kwargs
    )
    db3_interp = interp1d(
        db3_x_data, baselined_db_vals[3],
        **interpolation_kwargs
    )

    return db1_interp, db2_interp, db3_interp


def get_db(speed_data: Speed_Bdot1_Data, insert_used=False, tf_coil_lowered=False, interpolation_kind='linear', baseline_time_indeces=slice(10)):
    """
    Get the `r`, `phi`, and `z` components for the Bdot data.

    Parameters
    ----------
    light_data : Light_Bdot1_Data
    insert_used : bool, default=False
        Whether the insert was on the machine. If so, the positions need to be
        adjusted if the probe is not at 0 latitude.
    tf_coil_lowered : bool, default=False
        Whether the TF coil was lowered and the central axis of the machine is
        different than usual which requires corrections for position and 
        direction.
    interpolation_kind : str, default='linear'
        How we interpolate the magnitudes between coils.
    baseline_time_indeces : slice, default=slice(10)
        Slice of indeces along the time axis where we can remove the digitizer
        baseline offset and nothing should be happening.

    Returns
    -------
    interpolation_positions : np.array[float]
        2D-array of cylindrical positions,`(r, phi, z)`, where `phi` is in 
        radians. Shape of `(NUM_POSITIONS, 3)`.
    interpolation_db_values : np.array[float]
        3D-array of interpolated dB/dt components in cylindrical coordinates, 
        `(dB_r / dt, dB_phi / dt, dB_z / dt)`. Shape of `(NUM_POSITIONS, 3, NUM_TIMES)`.
    
    Notes
    -----
    This operates by taking three independent vectors and finding the magnitude 
    in the machine directions. Each coil is at a specific location and to get 3 
    independent vectors we must use a coil in the 1, 2, and 3 directions. We 
    interpolate between coils in each unique direction by interpolating in 
    angle and magnitude.
    """
    NUM_COILS = speed_data.r.size
    # Get the data from the working coils for each coil direction.
    db1a_values = speed_data.db_1a[np.nonzero(speed_data.db_1a_working)]
    db1a_positions = np.vstack((speed_data.exact_coil_r[0], speed_data.phi, speed_data.z)).T[np.nonzero(speed_data.db_1a_working)]
    db1a_orientations = np.tile([1, 0, 0], (NUM_COILS, 1))[np.nonzero(speed_data.db_1a_working)].astype(float)
    db1b_values = speed_data.db_1b[np.nonzero(speed_data.db_1b_working)]
    db1b_positions = np.vstack((speed_data.exact_coil_r[1], speed_data.phi, speed_data.z)).T[np.nonzero(speed_data.db_1b_working)]
    db1b_orientations = np.tile([1, 0, 0], (NUM_COILS, 1))[np.nonzero(speed_data.db_1b_working)].astype(float)
    db2_values = speed_data.db_2[np.nonzero(speed_data.db_2_working)]
    db2_positions = np.vstack((speed_data.exact_coil_r[2], speed_data.phi, speed_data.z)).T[np.nonzero(speed_data.db_2_working)]
    db2_orientations = np.tile([0, 1, 0], (NUM_COILS, 1))[np.nonzero(speed_data.db_2_working)].astype(float)
    db3_values = speed_data.db_3[np.nonzero(speed_data.db_3_working)]
    db3_positions = np.vstack((speed_data.exact_coil_r[3], speed_data.phi, speed_data.z)).T[np.nonzero(speed_data.db_3_working)]
    db3_orientations = np.tile([0, 0, 1], (NUM_COILS, 1))[np.nonzero(speed_data.db_3_working)].astype(float)

    # Now combine the 1 direction.
    db1_values = np.concatenate((db1a_values, db1b_values), axis=0)
    db1_positions = np.concatenate((db1a_positions, db1b_positions), axis=0)
    db1_orientations = np.concatenate((db1a_orientations, db1b_orientations), axis=0)

    all_values = [db1_values, db2_values, db3_values]
    all_positions = [db1_positions, db2_positions, db3_positions]
    all_orientations = [db1_orientations, db2_orientations, db3_orientations]

    # For each set of coils we need to remove the baseline.
    for dir_values, dir_orientations in zip(all_values, all_orientations):
        dir_values -= np.mean(dir_values[:, baseline_time_indeces], axis=1)[:, None]

    # Let's change the orientation of each coil to be in the machine cylindrical coordinates.
    # We also change the position depending on how the BRB is setup.
    for dir_positions, dir_orientations in zip(all_positions, all_orientations):
        for i in range(dir_orientations.shape[0]):
            position = Position()
            position.cylindrical = dir_positions[i]

            orientation = Vector(position)
            orientation.from_port(
                speed_data.port.long_rad, speed_data.port.lat_rad, speed_data.port.alpha_rad, speed_data.port.beta_rad, speed_data.port.gamma_rad,
                dir_orientations[i, 0], dir_orientations[i, 1], dir_orientations[i, 2], speed_data.port.clocking_rad
            )

            # TODO: Make the information that the insert is on and the TF coil is on a part of the MDSplus tree. Then remove the warnings.
            # We only need to adjust for the insert position if the probe is not on the equator.
            if insert_used and not np.isclose(speed_data.port.lat_rad, 0):
                hemisphere = 'n' if speed_data.port.lat_rad > 0 else 's'
                orientation.position.add_insert(hemisphere)
            
            # We then adjust for the lowered TF coil
            if tf_coil_lowered:
                orientation.position.to_tf_centered_coordinates()

            # Rewrite the position and orientation in the new cylindrical coordinates.
            dir_positions[i] = orientation.position.cylindrical
            dir_orientations[i] = orientation.cylindrical

            # Also renormalize the orientations. They can become slightly off.
            dir_orientations[i] /= np.linalg.norm(dir_orientations[i])

    return get_db_machine_coords(all_values, all_positions, all_orientations)

def plot_db(speed_data : Speed_Bdot1_Data, plot_time=True):
    fig, axs = plt.subplots(1, 3)
    positions, db_values = get_db(speed_data, insert_used=True, tf_coil_lowered=True, baseline_time_indeces=slice(10))
    min_radii_index = np.argmin(positions[:, 0])
    titles = [r'$\partial_t B_r$', r'$\partial_t B_\phi$', r'$\partial_t B_z$']

    x_axis = 10**6 * speed_data.time if plot_time else np.arange(speed_data.time_index_range[0], speed_data.time_index_range[1] + 1)

    manual_vabs = 20000
    for ax, title, db_vals in zip(axs, titles, np.swapaxes(db_values, 0, 1)):
        cmesh = ax.pcolormesh(
            x_axis, positions[min_radii_index:, 0], db_vals[min_radii_index:], 
            # vmin=-np.max(abs(db_vals)), vmax=np.max(abs(db_vals)), 
            vmin=-manual_vabs, vmax=manual_vabs,
            cmap='seismic', shading='nearest'
        )
        # cmesh = ax.pcolormesh(
        #     x_axis, -positions[:min_radii_index + 1, 0], db_vals[:min_radii_index + 1], 
        #     # vmin=-np.max(abs(db_vals)), vmax=np.max(abs(db_vals)), 
        #     vmin=-manual_vabs, vmax=manual_vabs,
        #     cmap='seismic', shading='nearest'
        # )

        ax.set_title(title)
        ax.axhspan(-positions[min_radii_index, 0], positions[min_radii_index, 0], color='grey')
        
        ax.set_xlabel(r'$t$ ($\mu$s)' if plot_time else 'time index')
        ax.set_ylabel(r'$r$ (m)')
        fig.colorbar(cmesh, ax=ax)

    fig.suptitle("Shot #{}: Speed Data".format(speed_data.shot_number))

    return fig, axs


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import logging
    # logging.basicConfig(level=logging.DEBUG)

    index_lims = (64980, 65260)
    index_range = range(*index_lims)
    time_index = 65112
    speed_data = Speed_Bdot1_Data(60030, time_index_range=index_lims)
    fig, axs = plot_db(speed_data)

    plot_mesh = False
    plot_interpolation = False
    plot_mach_db = False
    
    x_values = np.arange(index_lims[0], index_lims[1])
    y_values = speed_data.r
    if plot_mesh:
        baselined_db_values = baselined_db(speed_data, nothing_happening_range=range(65000, 65050))

        fig, axs = plt.subplots(2, 2, sharex=True, sharey=True)
        titles = ['db_1a', 'db_1b', 'db_2', 'db_3']
        for i, ax in enumerate(axs.flatten()):
            cmesh = ax.pcolormesh(
                x_values, y_values, baselined_db_values[i, :, index_range].T, 
                vmin=-np.max(abs(baselined_db_values[:, :, index_range])), vmax=np.max(abs(baselined_db_values[:, :, index_range])), cmap='seismic', shading='nearest'
            )

            ax.axvline(time_index, color='red')
            ax.set_title(titles[i])

            fig.colorbar(cmesh, ax=ax)
            
        fig.suptitle("Shot #{}: index = {}".format(speed_data.shot_number, time_index))

    if plot_interpolation:
        baselined_db_values = baselined_db(speed_data, nothing_happening_range=range(65000, 65050))
        interps = interp_bdot(speed_data, baselined_db_values)

        fig, axs = plt.subplots(1, 3)

        axs[0].set_title('db_1')
        axs[1].set_title('db_2')
        axs[2].set_title('db_3')

        r_points = np.linspace(np.min(speed_data.r), np.max(speed_data.r), num=100)

        axs[0].plot(r_points, interps[0](r_points)[:, time_index])
        axs[1].plot(r_points, interps[1](r_points)[:, time_index])
        axs[2].plot(r_points, interps[2](r_points)[:, time_index])

        axs[0].scatter(speed_data.exact_coil_r[1], baselined_db_vals[0, :, time_index], label='A')
        axs[0].scatter(speed_data.exact_coil_r[0], baselined_db_vals[1, :, time_index], label='B')
        axs[0].legend()
        axs[1].scatter(speed_data.exact_coil_r[2], baselined_db_vals[2, :, time_index])
        axs[2].scatter(speed_data.exact_coil_r[3], baselined_db_vals[3, :, time_index])

        fig.suptitle("Shot #{}: index = {}".format(speed_data.shot_number, time_index))

    if plot_mach_db:
        fig, axs = plt.subplots(1, 3)
        titles = [r'$\partial_t B_r$', r'$\partial_t B_\phi$', r'$\partial_t B_z$']
        for ax, title, db_vals in zip(axs, titles, [speed_data.db_r, speed_data.db_phi, speed_data.db_z]):
            cmesh = ax.pcolormesh(
                x_values, y_values, db_vals[:, index_range], 
                vmin=-np.max(abs(db_vals[:, index_range])), vmax=np.max(abs(db_vals[:, index_range])), cmap='seismic', shading='nearest'
            )
            ax.axvline(time_index, color='red')
            ax.set_title(title)
        fig.colorbar(cmesh, ax=axs[-1])
        
        fig.suptitle("Shot #{}: index = {}".format(speed_data.shot_number, time_index))
        fig.tight_layout()

    plt.show()
