"""Get data from the 2nd hook probe from the MDSplus tree."""
from modules.generic_get_data import Get, Data, lazy_get


class Hook_Bdot2_Data(Data):
    """
    Hold all the data for the 2nd hook probe in one class for easier access and reduced network connections.
    
    To get data, call the data without the underscore treating it as an attribute.
    """
    def __init__(self, tree, *args, **kwargs):
        """
        Load the data for some nodes from the tree.
        """
        super().__init__(tree, [], [], *args, **kwargs)

    @lazy_get
    def db_phi(self):
        return self.get(Get('\\hook_bdot2_dbphi'))

    @lazy_get
    def db_r(self):
        return self.get(Get('\\hook_bdot2_dbr'))

    @lazy_get
    def db_z(self):
        return self.get(Get('\\hook_bdot2_dbz'))

    @lazy_get
    def phi(self):
        return self.get(Get('\\hook_bdot2_phi', signal=False))

    @lazy_get
    def r(self):
        return self.get(Get('\\hook_bdot2_r', signal=False))

    @lazy_get
    def z(self):
        return self.get(Get('\\hook_bdot2_z', signal=False))

    @lazy_get
    def db_1(self):
        return self.get(Get('\\hook_bdot2_db1'))

    @lazy_get
    def db_2(self):
        return self.get(Get('\\hook_bdot2_db2'))

    @lazy_get
    def db_3a(self):
        return self.get(Get('\\hook_bdot2_db3a'))

    @lazy_get
    def db_3b(self):
        return self.get(Get('\\hook_bdot2_db3b'))

    @lazy_get
    def time(self):
        return self.get(Get('dim_of(\\hook_bdot2_db1)', name='hook_bdot2_time'))
