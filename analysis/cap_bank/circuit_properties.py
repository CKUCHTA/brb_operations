# Extract the resistance, inductance, and capacitance from a shot without flyback diodes.
from cap_bank.get_data import Capacitor_Bank_Data
from modules.shot_loader import get_remote_shot_tree
import numpy as np
from scipy.optimize import curve_fit, differential_evolution
from scipy.misc import derivative
import logging
import matplotlib.pyplot as plt


def model_function(t, magnitude, decay_rate, oscillation_rate, trigger_time, cutoff_time):
    """
    Simplified function to model current trace consisting of an exponentially decaying sin curve that starts at 'trigger_time' and ends at 'cutoff_time'.
    """
    return np.heaviside(t - trigger_time, 0) * np.heaviside(cutoff_time - t, 0) * \
        magnitude * np.exp(decay_rate * (t - trigger_time)) * np.sin(oscillation_rate * (t - trigger_time))

def model_to_circuit(magnitude, decay_rate, oscillation_rate, cap_voltage):
    """
    Change the parameters from the model function into circuit values.
    """
    common_divider = (magnitude**2 * (decay_rate**2 + oscillation_rate**2))**-0.5
    resistance = -2 * decay_rate * cap_voltage * common_divider
    inductance = cap_voltage * common_divider
    capacitance = magnitude**2 * common_divider / cap_voltage
    return [resistance, inductance, capacitance]

def solution_error(parameters, cap_data: Capacitor_Bank_Data):
    """
    Calculate the difference between the experimental and theoretical data.
    """
    return np.linalg.norm(model_function(cap_data.time, *parameters) - cap_data.icap)**2

def expected_current(t, resistance, inductance, capacitance, trigger_time, cutoff_time, cap_voltage):
    """
    Return the expected current at some point in time with some circuit values.
    """
    decay_rate = - resistance / (2 * inductance)
    if resistance**2 - 4 * inductance / capacitance > 0:
        logging.warn("Resistance too high. No oscillations will occur. Returning 0 as expected current.\n" + "\nr = {}\nl={}\nc={}".format(resistance, inductance, capacitance))
        return np.zeros_like(t)

    oscillation_rate = (1 / (inductance * capacitance) - (resistance / (2 * inductance))**2)**0.5
    magnitude = cap_voltage * capacitance * (resistance**2 + 4 * inductance**2 * oscillation_rate**2)**0.5 / (2 * inductance)
    return model_function(t, magnitude, decay_rate, oscillation_rate, trigger_time, cutoff_time)

def data_to_fit(cap_data: Capacitor_Bank_Data):
    """
    We want to only use data around when the cap bank fires so extract the slice that uses that.
    """
    # We can get the approximate start time by getting the index of a point half way between the start and end vcap.
    start_vcap = np.mean(cap_data.vcap[:1000])
    end_vcap = np.mean(cap_data.vcap[-1000:])
    mid_vcap = (start_vcap + end_vcap) / 2

    mid_index = np.nonzero(cap_data.vcap < mid_vcap)[0][0]
    mid_time = cap_data.time[mid_index]

    start_time = mid_time - 0.0005
    end_time = mid_time + 0.002

    start_index = np.nonzero(cap_data.time > start_time)[0][0]
    end_index = np.nonzero(cap_data.time > end_time)[0][0]
    return slice(start_index, end_index)

def get_params(cap_data: Capacitor_Bank_Data):
    """
    Get the values for the LRC circuit. This is the main function to use to get the fit parameters from the shot.

    params:
    cap_data    = Cap bank data object.

    returns:
    circuit_params  = Tuple of floats containing fit (resistance, inductance, capacitance, trigger time, cutoff time, cap bank voltage).
    """
    avg_cap_voltage = np.mean(cap_data.vcap[:1000])
    fit_slice = data_to_fit(cap_data)
    # Calculate the oscillation rate by calculating the width at half max of the first peak and then changing that to the oscillation rate.
    index_of_max = np.argmax(cap_data.icap)
    width_at_half_max = cap_data.time[index_of_max + np.nonzero(cap_data.icap[index_of_max:] < np.amax(cap_data.icap) / 2)[0][0]] - cap_data.time[np.nonzero(cap_data.icap[:index_of_max] < np.amax(cap_data.icap) / 2)[0][-1]]
    initial_oscillation_rate = 2 * np.pi / (3 * width_at_half_max)
    bounds = np.array([
        (np.amax(cap_data.icap) * 0.75, np.amax(cap_data.icap) * 1.25), (-50 / (cap_data.time[fit_slice.stop] - cap_data.time[fit_slice.start]), 0), (initial_oscillation_rate * 0.5, initial_oscillation_rate * 2),
        (cap_data.time[fit_slice.start], cap_data.time[fit_slice.stop]), (cap_data.time[fit_slice.start], cap_data.time[fit_slice.stop])
    ]).T
    result = differential_evolution(solution_error, bounds=bounds.T, args=tuple([cap_data]))
    params = result.x
    circuit_params = model_to_circuit(params[0], params[1], params[2], avg_cap_voltage) + list(params[3:]) + [avg_cap_voltage]

    param_names = ["Resistance", "Inductance", "Capacitance", "Trigger time", "Cutoff time", "Cap Bank Voltage"]
    param_units = ["ohm", "H", "F", "s", "s", "V"]
    logging.debug("Parameters found were:\n" + "\n".join(["{name}: {val} {units}".format(
        name=param_names[i], val=circuit_params[i], units=param_units[i]
    ) for i in range(len(param_names))]))

    return circuit_params

def plot_solution(cap_data: Capacitor_Bank_Data, solved_params):
    """
    Plot both the experimental current and the solved current.

    params:
    cap_data        = The cap bank data object.
    solved_params   = An iterable of the circuit parameters to be fed into the expected current function.
    """
    fig, ax = plt.subplots()

    ax.plot(cap_data.time, cap_data.icap, label='Experimental')
    matching_solution = expected_current(cap_data.time, *solved_params)
    ax.plot(cap_data.time, matching_solution, label='Matching Theory')
    new_cap_solution = expected_current(cap_data.time, solved_params[0], 0.592 * 10**-6, 2 * 5 * 14 * 10**-6, solved_params[3], solved_params[4], solved_params[5])
    ax.plot(cap_data.time, new_cap_solution, label='Drive Cyllinder Bank')
    fit_slice = data_to_fit(cap_data)
    ax.axvspan(cap_data.time[fit_slice.start], cap_data.time[fit_slice.stop], color='grey', alpha=0.1)

    # Calculate the slope of the curve at the start.
    dt = 10**-8
    matching_dIdt = derivative(lambda t: expected_current(t, *solved_params), solved_params[3] + 2*dt, dx=dt)
    new_dIdt = derivative(lambda t: expected_current(t, solved_params[0], 0.592 * 10**-6, 2 * 5 * 14 * 10**-6, solved_params[3], solved_params[4], solved_params[5]), solved_params[3] + 2*dt, dx=dt)

    # Calculate the rise times.
    matching_rise_time = cap_data.time[np.argmax(matching_solution)] - solved_params[3]
    new_rise_time = cap_data.time[np.argmax(new_cap_solution)] - solved_params[3]
    # Create the box for holding the circuit info.
    text_str = "\n".join((
        r'$\mathrm{{R}} = {:.2f}\, m\Omega$'.format(10**3 * solved_params[0]),
        r'$\mathrm{{L}} = {:.2f}\, \mu H$'.format(10**6 * solved_params[1]),
        r'$\mathrm{{C}} = {:.2f}\, \mu F$'.format(10**6 * solved_params[2]),
        r'$\frac{{dI}}{{dt}}_{{old}} = {:.2f}e9\, A s^{{-1}}$'.format(matching_dIdt / 10**9),
        r'$\frac{{dI}}{{dt}}_{{new}} = {:.2f}e9\, A s^{{-1}}$'.format(new_dIdt / 10**9),
        r'$t_{{rise,\, old}} = {:.2f}\, \mu s$'.format(10**6 * matching_rise_time, matching_rise_time / (2.5 * 10**-6)),
        r'$t_{{rise,\, new}} = {:.2f}\, \mu s$'.format(10**6 * new_rise_time, new_rise_time / (2.5 * 10**-6)),
    ))
    ax.text(0.7, 0.6, text_str, transform=ax.transAxes)
    ax.legend()

    fig.suptitle("Shot #{}".format(cap_data.shot_number))
    plt.show()


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    for shot_number in range(55495, 55558):
        tree = get_remote_shot_tree(shot_number)
        cap_data = Capacitor_Bank_Data(tree)
        params = get_params(cap_data)
        plot_solution(cap_data, params)
