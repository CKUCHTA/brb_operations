from modules.generic_get_data import Get, Data, lazy_get
from scipy.integrate import cumtrapz
import numpy as np


class Capacitor_Bank_Data(Data):
    def __init__(self, tree, *args, **kwargs):
        super().__init__(tree, [], [], *args, **kwargs)

    @lazy_get
    def vcap(self):
        return self.get(Get("\\trex_v_cap"))
    
    @lazy_get
    def irog(self):
        return self.get(Get("[" + ", ".join("DATA( \\trex_irog_{} )".format(i) for i in range(1, 5)) + "]"))
    
    @lazy_get
    def irog_combined(self):
        return self.get(Get(" + ".join("\\trex_irog_{}".format(i) for i in range(1, 5))))
    
    @lazy_get
    def time(self):
        return self.get(Get("dim_of(\\trex_v_cap)", name='cap_time'))

    @lazy_get
    def icap(self):
        # Correct the offset in irog and fix the integrator droop.
        integrator_r = 1000 # ohm
        integrator_c = 10**-6 # F

        icap = np.zeros_like(self.irog)
        for i, irog in enumerate(self.irog):
            baselined_irog = irog - np.mean(irog[:1000])
        
            icap[i] = baselined_irog + 1 / (integrator_c * integrator_r) * cumtrapz(baselined_irog, self.time, initial=0)
        return icap
