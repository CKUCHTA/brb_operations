﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="support" Type="Folder">
		<Item Name="build_mask_array.vi" Type="VI" URL="../../build_mask_array.vi"/>
		<Item Name="build_reset_DP_string.vi" Type="VI" URL="../../build_reset_DP_string.vi"/>
		<Item Name="build_reset_string.vi" Type="VI" URL="../../build_reset_string.vi"/>
		<Item Name="Cathode Motor IDs.ctl" Type="VI" URL="../../Cathode Motor VIs/Cathode Motor IDs.ctl"/>
		<Item Name="event_Message.vi" Type="VI" URL="../../event_Message.vi"/>
		<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
		<Item Name="map_motor_to_index.vi" Type="VI" URL="../../map_motor_to_index.vi"/>
		<Item Name="MotorControllerState.ctl" Type="VI" URL="../../MotorControllerState.ctl"/>
		<Item Name="MotorIDs.ctl" Type="VI" URL="../../MotorIDs.ctl"/>
		<Item Name="MP_array_abs_move_axes.vi" Type="VI" URL="../../Probe Motor VIs/MP_array_abs_move_axes.vi"/>
		<Item Name="MP_array_initialize_motors.vi" Type="VI" URL="../../Probe Motor VIs/MP_array_initialize_motors.vi"/>
		<Item Name="MP_array_move_axes.vi" Type="VI" URL="../../Probe Motor VIs/MP_array_move_axes.vi"/>
		<Item Name="MP_array_parse_TP_v2.vi" Type="VI" URL="../../Probe Motor VIs/MP_array_parse_TP_v2.vi"/>
		<Item Name="MP_array_parse_TS_v2.vi" Type="VI" URL="../../Probe Motor VIs/MP_array_parse_TS_v2.vi"/>
		<Item Name="MP_array_TP.vi" Type="VI" URL="../../Probe Motor VIs/MP_array_TP.vi"/>
		<Item Name="MP_array_TP_TS_v2.vi" Type="VI" URL="../../Probe Motor VIs/MP_array_TP_TS_v2.vi"/>
		<Item Name="MP_array_TS.vi" Type="VI" URL="../../Probe Motor VIs/MP_array_TS.vi"/>
		<Item Name="open_galil_connection.vi" Type="VI" URL="../../open_galil_connection.vi"/>
		<Item Name="send_string_to_motor.vi" Type="VI" URL="../../send_string_to_motor.vi"/>
	</Item>
	<Item Name="probe_variables.lvlib" Type="Library" URL="../probe_variables.lvlib"/>
	<Item Name="stepper_2D_scan_abs_pos.vi" Type="VI" URL="../stepper_2D_scan_abs_pos.vi"/>
</Library>
