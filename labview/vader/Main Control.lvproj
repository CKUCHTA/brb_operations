﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="varPersistentID:{0010EFF2-5515-470B-803A-2458D5FCC199}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO15</Property>
	<Property Name="varPersistentID:{001F263E-E50F-4803-8DBD-C87866270AF3}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_Network.lvlib/Dump CCDS</Property>
	<Property Name="varPersistentID:{011C9E78-7F77-4372-A020-F13B97DC02E6}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 9 PSID</Property>
	<Property Name="varPersistentID:{0198F7AF-D518-4A4E-A737-E2C9ADF89277}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPC_Radial_Pos</Property>
	<Property Name="varPersistentID:{019AC8F5-FB9F-4ADE-AA62-CA9F522F5BAE}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO19</Property>
	<Property Name="varPersistentID:{01CC36AD-DC63-4B6B-9D48-436E60926805}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI12</Property>
	<Property Name="varPersistentID:{0210E37B-AE99-445D-9131-A39E8CF3A6CF}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_Network.lvlib/Alpha1_voltage</Property>
	<Property Name="varPersistentID:{02313504-E5E6-402E-B266-B7B4F6170B5B}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO15</Property>
	<Property Name="varPersistentID:{023CD662-942F-41E2-A24D-3419E653A55B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 8 PSID</Property>
	<Property Name="varPersistentID:{0309ED5C-516C-466F-8DDE-37D2426026B5}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Puff Duration 1</Property>
	<Property Name="varPersistentID:{03127E3F-F12F-431D-9A5B-1CCA5B72677A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/Shot Setup Data/FP_Shot_Setup.lvlib/Color</Property>
	<Property Name="varPersistentID:{034757C8-2832-438B-A5C5-1C4F20515617}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO11</Property>
	<Property Name="varPersistentID:{03F25780-3DF3-4EB4-B791-9B29110B6A7F}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO13</Property>
	<Property Name="varPersistentID:{041038A6-3B24-4D01-9717-62F3094CAFB8}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO18</Property>
	<Property Name="varPersistentID:{0420BCB9-26EB-46CB-A1A4-08ED3BBD4B74}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO14</Property>
	<Property Name="varPersistentID:{0432EE90-5CF1-4F53-9C91-35E6724A679B}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO12</Property>
	<Property Name="varPersistentID:{049B30AF-5F0F-40E2-BB0B-AAC201A9393A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/DTACQ_variables.lvlib/DTACQ_arm_trigger</Property>
	<Property Name="varPersistentID:{05174A5B-EA75-4DC4-AF15-ABF64C8A671D}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha2_IO_Aliases.lvlib/2_Ready</Property>
	<Property Name="varPersistentID:{051F9A11-CF7F-4D74-AF6C-53A6FAC77302}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO17</Property>
	<Property Name="varPersistentID:{053CE65F-8943-4B4A-AE0B-5323783BAC58}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO0</Property>
	<Property Name="varPersistentID:{057368B5-32DB-47DB-8000-E5C6C05D10E2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS2 Armed</Property>
	<Property Name="varPersistentID:{05B5B034-B966-45B5-A3D1-4202F4AECE4F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Puff Voltage 2</Property>
	<Property Name="varPersistentID:{05E8D100-4D50-42AC-A64B-350F0412FA5F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Trigger Spectrum_2</Property>
	<Property Name="varPersistentID:{0634749B-673F-467A-A5BC-AA58A07A002A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_10</Property>
	<Property Name="varPersistentID:{06864382-3006-40D6-8E62-4ADC729D4CA6}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_IO_Aliases.lvlib/Remote Set</Property>
	<Property Name="varPersistentID:{06A39FD2-9B16-4F62-B1BE-5962815570C5}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP23_24</Property>
	<Property Name="varPersistentID:{06CECE32-674C-4E2F-9237-CEAFD83E3A55}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_5</Property>
	<Property Name="varPersistentID:{075CBC12-FE5F-4522-943A-DF7DE1DBCE7E}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO30</Property>
	<Property Name="varPersistentID:{07A8B348-7E40-4ED7-9F99-B61E641CB698}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Arm Spectrum_2</Property>
	<Property Name="varPersistentID:{07D99644-58F6-4B73-992B-D793F49EDE1F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Shot Number</Property>
	<Property Name="varPersistentID:{0809D40B-F357-454F-838A-42C45BA529A3}" Type="Ref">/My Computer/TREX Controls/Heater Cap Controls/HeaterCap_Network_Variables.lvlib/Interlock</Property>
	<Property Name="varPersistentID:{0815A53D-E6F7-4A5C-8F90-2F117CA4CC43}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC15</Property>
	<Property Name="varPersistentID:{087045E9-A9FF-4998-AA1D-3AF791AA3FFD}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_13</Property>
	<Property Name="varPersistentID:{0873568C-79D0-4654-9D8F-67C08167C259}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI3</Property>
	<Property Name="varPersistentID:{08EA37D6-D57A-4209-94CA-2CAEA1B6A591}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS3 Mode</Property>
	<Property Name="varPersistentID:{0902E182-BD41-473B-B304-79F5F439815B}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/S_CT_Trig</Property>
	<Property Name="varPersistentID:{0A1F5379-34CE-42C0-975E-CF2D349A809A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Phantom Delay</Property>
	<Property Name="varPersistentID:{0A51023D-8FDD-4EA9-ADD2-F0CA4ADF1769}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 7</Property>
	<Property Name="varPersistentID:{0AD910BA-92DC-48A5-BEE0-604303D60FA0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TC1 Voltage</Property>
	<Property Name="varPersistentID:{0B76FABF-6F24-40FE-8E13-60BE734CE2BF}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI17</Property>
	<Property Name="varPersistentID:{0BFC0010-06F1-48ED-8022-C8C399AB0FB1}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Discharge_trigger</Property>
	<Property Name="varPersistentID:{0C234567-04B5-47B4-B859-C4D5F8A59637}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_7</Property>
	<Property Name="varPersistentID:{0C9487F6-4369-4087-9839-498642F8AE57}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPH_Radial_Pos</Property>
	<Property Name="varPersistentID:{0CDB48DD-8FC6-47AE-82B7-799DCB9B0821}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_Network.lvlib/Alpha1_ext int</Property>
	<Property Name="varPersistentID:{0CF985E3-B114-4D61-B3BB-CBF1902BA645}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/N CT Relay Status</Property>
	<Property Name="varPersistentID:{0D25B592-2234-4676-ACFC-F707E4209C43}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_A1_Radius</Property>
	<Property Name="varPersistentID:{0D8B189A-6588-4041-8644-C8ECCDA5A4AE}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath9</Property>
	<Property Name="varPersistentID:{0DCD259E-2506-42F7-9E18-B20BDE76DA9E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS4 Cathode ID</Property>
	<Property Name="varPersistentID:{0DF9B7BE-9216-45BA-9DFF-00E2B9A1B8F5}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO21</Property>
	<Property Name="varPersistentID:{0E09E6CC-31C0-49C7-8B30-E2FF990ADD70}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO18</Property>
	<Property Name="varPersistentID:{0E4693E3-E487-4320-8B47-7AC0754C33DC}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS11 HeaterID</Property>
	<Property Name="varPersistentID:{0E9BF6F4-98D4-46F5-8399-20797E148046}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_ID_Motor_H</Property>
	<Property Name="varPersistentID:{0F6FBDF6-5BCA-49CC-A942-3D5B3E2742D8}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/RGA_active</Property>
	<Property Name="varPersistentID:{0F92AAF5-D44E-4DDC-BCEA-53C8C6031A64}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO12</Property>
	<Property Name="varPersistentID:{0FC6CB0F-257C-4826-B528-93509EBD6774}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI1</Property>
	<Property Name="varPersistentID:{1198FDDA-4676-4D61-9254-277472935FB7}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO22</Property>
	<Property Name="varPersistentID:{11D8EE6F-4A76-4A0E-B08F-EE8BCF20488E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS7 Mode</Property>
	<Property Name="varPersistentID:{1228ADFE-E2E4-4839-89C0-8A676CC501EF}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_9</Property>
	<Property Name="varPersistentID:{123E93BF-67DF-4F86-801F-A5D91901B75E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPG_Radial_Home</Property>
	<Property Name="varPersistentID:{1263C848-E618-4EFA-A349-64F48F1F2401}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO4</Property>
	<Property Name="varPersistentID:{12E03ACE-6464-45C3-8E0A-5DC83811AD5A}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO23</Property>
	<Property Name="varPersistentID:{136D8D51-9582-4845-9CEA-16C010726028}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_E_Home</Property>
	<Property Name="varPersistentID:{14B4038F-1AB0-4141-A205-4C2D11E96D53}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC2</Property>
	<Property Name="varPersistentID:{15D567B3-36FB-4CE4-A582-CDF3E3CDC0DF}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO8</Property>
	<Property Name="varPersistentID:{1600C577-B8B5-41BD-B9F7-B1F02BC50A31}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS9 Control Mode</Property>
	<Property Name="varPersistentID:{162EE5C1-48FF-435F-850A-299BA5A276C5}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/N CT Relay Enable</Property>
	<Property Name="varPersistentID:{1634A5AF-BC08-49DE-B77B-3D3FC8959E2A}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/S CT Inhibit PS</Property>
	<Property Name="varPersistentID:{16502B86-09D8-4CE0-8DF2-CB38EB0D8DEA}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP17_18</Property>
	<Property Name="varPersistentID:{166439A8-190B-47E7-9859-09BCE323CC28}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS2 Setpoint</Property>
	<Property Name="varPersistentID:{17476EA7-F20F-4658-B352-AB1CF57E7B51}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Puff Duration 3</Property>
	<Property Name="varPersistentID:{177DE6E9-10D1-44F4-A232-0728BACCBEF0}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI6</Property>
	<Property Name="varPersistentID:{17B61A47-B568-4209-A8D3-3342C47DE65C}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_D_Radius</Property>
	<Property Name="varPersistentID:{1861BF81-0D47-47DD-95F1-13270DCF1A2F}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO31</Property>
	<Property Name="varPersistentID:{1882847A-FE3F-4BBE-9E28-B55138275744}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha3_Network_Controls.lvlib/3_on</Property>
	<Property Name="varPersistentID:{18A9B2F7-6A20-4DE9-B0BF-70094B737CD9}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO3</Property>
	<Property Name="varPersistentID:{18B708E0-DAD0-4E36-95B2-2319D30C22F2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Encoder_CPR</Property>
	<Property Name="varPersistentID:{18D64FD4-F587-4AE1-ADEC-BD32F3DB2D53}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC2</Property>
	<Property Name="varPersistentID:{193B0970-9A6F-4207-81AD-15244AEAE2F2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Wavelength_2</Property>
	<Property Name="varPersistentID:{19AE879F-B765-40C3-AA83-DF8FC4C6CF62}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 6 PSID</Property>
	<Property Name="varPersistentID:{1A6DAD58-59D5-4D74-93CD-89710CC9691E}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha2_Network_Controls.lvlib/2_state</Property>
	<Property Name="varPersistentID:{1AAEE751-69FC-4DB8-922E-A75D178A06BE}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_Network.lvlib/Remote Stop</Property>
	<Property Name="varPersistentID:{1AFEE2F0-745D-4DBC-B1E6-3EB338158E54}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_2</Property>
	<Property Name="varPersistentID:{1B464299-2473-4249-8D0D-9275DEE2CD8A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Spectra Time_3</Property>
	<Property Name="varPersistentID:{1B65A6E5-CA2E-4E43-BD15-D5F60C8DF98C}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_B2_Radius</Property>
	<Property Name="varPersistentID:{1B83EBD1-7C3B-4AB4-9D29-E6E402E71383}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha2_Network_Controls.lvlib/2_bfield</Property>
	<Property Name="varPersistentID:{1BE12D83-5F0F-4B6A-8482-D8CEB29BBBE4}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO11</Property>
	<Property Name="varPersistentID:{1C2F8555-1A94-407B-8424-8EB705859DE9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS10 Setpoint</Property>
	<Property Name="varPersistentID:{1C3A0797-5D80-4B51-8FBA-16112327DF31}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS8 HeaterID</Property>
	<Property Name="varPersistentID:{1CF83DF7-7EBF-403B-A5D0-EAE1A7A1851F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS10 Delay</Property>
	<Property Name="varPersistentID:{1D9DAA65-DEDD-4BCC-8830-46F223E25FB7}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP4</Property>
	<Property Name="varPersistentID:{1E65E327-A530-4CA2-97B0-D9FDDDE93AC3}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO8</Property>
	<Property Name="varPersistentID:{1EE57A6B-BE81-4FD1-A681-6106C8662D00}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO2</Property>
	<Property Name="varPersistentID:{1F5191B1-2616-4929-91B5-FB9D1F22FACD}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPG_Radial_Pos</Property>
	<Property Name="varPersistentID:{1F7D3901-C5FF-4247-A6BE-5687E5B1017B}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_IO_Aliases.lvlib/3_CurrentOut</Property>
	<Property Name="varPersistentID:{1FAEBA00-AF3D-4248-8635-92D9ADB5682D}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO1</Property>
	<Property Name="varPersistentID:{1FB2568C-622A-48F4-9F10-5455AD3B0665}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/PA_Z</Property>
	<Property Name="varPersistentID:{1FE134BC-7F4D-44FA-BFD8-F8DA53B800F2}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/N_CT_Voltage</Property>
	<Property Name="varPersistentID:{20399D71-9FAB-460C-8BC5-7CC4D4194CC0}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO1</Property>
	<Property Name="varPersistentID:{209ED600-5B8F-4776-B2F6-48897BE889A3}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO10</Property>
	<Property Name="varPersistentID:{21D735D7-7D44-4C29-8CF0-8B328200EC20}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI24</Property>
	<Property Name="varPersistentID:{223799E5-D876-4F11-83EA-238D90F1CE55}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Spectrum</Property>
	<Property Name="varPersistentID:{227BABAD-AB7A-440F-BEA6-2B0C676ED136}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO27</Property>
	<Property Name="varPersistentID:{22F68792-72D1-4DAB-98B2-E066C7753634}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_15</Property>
	<Property Name="varPersistentID:{23B53C1F-48F1-466D-A89F-290D4BF76B55}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS3 Setpoint</Property>
	<Property Name="varPersistentID:{23F38CA2-A8E1-48B7-A958-33333374392D}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_14</Property>
	<Property Name="varPersistentID:{24077C36-DFF2-4F80-AD55-9090D5727C69}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO6</Property>
	<Property Name="varPersistentID:{240E0926-AEA6-4094-8044-C18F79E82927}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO18</Property>
	<Property Name="varPersistentID:{2415247E-4A92-4A02-847A-7BB948DEB8CB}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/TREX_enabled</Property>
	<Property Name="varPersistentID:{2420AFC8-E467-46E7-BAB6-D78357DF3AC4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS2 Mode</Property>
	<Property Name="varPersistentID:{246C5B11-D20F-4EB9-A7D0-F5B539309DE4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Suspend Heating PS10</Property>
	<Property Name="varPersistentID:{24ED7CE3-361A-4A73-9A82-3AED14B25252}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO23</Property>
	<Property Name="varPersistentID:{252722F2-10FC-4B52-A6D4-4B0E62A92582}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/N CT PS Load Fault</Property>
	<Property Name="varPersistentID:{255AAEB9-434B-4E48-B815-36B512DD6176}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO2</Property>
	<Property Name="varPersistentID:{257B0108-0629-416A-8769-D4EC4248CD28}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS9 Mode</Property>
	<Property Name="varPersistentID:{25C1B709-07F5-4DAB-870D-FB97A0F371C4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath12</Property>
	<Property Name="varPersistentID:{26196715-12DF-41BA-9C5D-C49744DDB5B7}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP2</Property>
	<Property Name="varPersistentID:{267DEEB9-0198-43A6-A508-EB2A4F999089}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO10</Property>
	<Property Name="varPersistentID:{271D434A-A5C1-40B5-82A4-3994E55775CC}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/Te_R</Property>
	<Property Name="varPersistentID:{2798EDF8-D91C-45A8-967F-86A485AD4B07}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO21</Property>
	<Property Name="varPersistentID:{280CD5F9-8824-44ED-B565-84E057F7AB8F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Suspend Heating PS3</Property>
	<Property Name="varPersistentID:{2869DC57-378E-4B94-A966-7CAA37047375}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_Network.lvlib/Alpha3_on</Property>
	<Property Name="varPersistentID:{286F0D5D-3774-49A2-A51C-F37C33F90C18}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Spectrum_2</Property>
	<Property Name="varPersistentID:{28885E41-43A2-4AFB-AC58-C8F94B26A57A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TC3 Voltage</Property>
	<Property Name="varPersistentID:{28D25EF3-5DFF-44C4-8CFD-696B09C01037}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Time Between Measurements</Property>
	<Property Name="varPersistentID:{293A1564-BDCD-4499-8DAD-5E88C1793A0C}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_E_Radius</Property>
	<Property Name="varPersistentID:{293CD8DF-8383-40C1-96EA-CB13157802CA}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_F1_Radius</Property>
	<Property Name="varPersistentID:{29949211-4F3C-4C58-B279-CB9C6C0F8289}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO3</Property>
	<Property Name="varPersistentID:{29B186C6-0BF9-4265-A0B3-FC87253B6E37}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS5 Control Mode</Property>
	<Property Name="varPersistentID:{29E399F7-5A0E-450A-96FA-9651BA45B8EE}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 1</Property>
	<Property Name="varPersistentID:{2B17D094-D98E-4C1C-9EA2-6A4A4FC35EA0}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI21</Property>
	<Property Name="varPersistentID:{2BB0B954-B1B9-4808-8790-15DF009239D6}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_E3_Radius</Property>
	<Property Name="varPersistentID:{2C41DB3F-5C56-4852-87D3-A33521553F56}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_RTFIFO.lvlib/1_voltage out</Property>
	<Property Name="varPersistentID:{2C897F97-D037-4194-A6FE-AA3343F4E506}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/Ax_Z</Property>
	<Property Name="varPersistentID:{2CD85930-A570-4972-ADE3-3E9D36EACB2B}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO13</Property>
	<Property Name="varPersistentID:{2D355167-C429-4CED-AE1A-221CFB121837}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_ID_Motor_C</Property>
	<Property Name="varPersistentID:{2D5A1BDC-0C4C-4413-B348-D37668A0BF26}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS3 Control Mode</Property>
	<Property Name="varPersistentID:{2E47CD35-577E-4D10-87B2-09BE06912B2B}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha2_IO_Aliases.lvlib/2_Off</Property>
	<Property Name="varPersistentID:{2EB60A31-DEC0-4C8C-ACA6-FA2691284F38}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath7</Property>
	<Property Name="varPersistentID:{2EE2270C-AD05-412F-9163-5197EB18A821}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 10</Property>
	<Property Name="varPersistentID:{2F9377CE-9171-4B20-A161-FE7D1DE1FAD0}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO12</Property>
	<Property Name="varPersistentID:{30056694-5067-4D48-B62D-45C30029E037}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_3</Property>
	<Property Name="varPersistentID:{30411B5F-F97D-4B3F-BB65-61D6052BB33F}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO24</Property>
	<Property Name="varPersistentID:{304F1C31-56CC-400D-8039-6909BC04AEA1}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Suspend Heating PS8</Property>
	<Property Name="varPersistentID:{30513901-4B39-45ED-90A6-7E76267B35B9}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha2_Network_Controls.lvlib/2_voltage</Property>
	<Property Name="varPersistentID:{30BFC0C5-26E1-45B5-B123-D681EE15A1CE}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/Ax_R</Property>
	<Property Name="varPersistentID:{30C79323-4DC3-4993-B6EA-6D7D263D83CD}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO21</Property>
	<Property Name="varPersistentID:{30EE677B-E71C-4612-8C01-B063A7592FEC}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS6 Armed</Property>
	<Property Name="varPersistentID:{310181E9-4E94-4657-A492-29924C72B069}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha3_Network_Controls.lvlib/3_on?</Property>
	<Property Name="varPersistentID:{313087DF-FD9F-4C4B-814A-5C29D5497B9D}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO3</Property>
	<Property Name="varPersistentID:{31425DE5-D38D-414E-BC71-E91FA6FDDD0A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Thermocouple array</Property>
	<Property Name="varPersistentID:{31C88CB5-3710-4197-8A1D-846529EE44F4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS9 Setpoint</Property>
	<Property Name="varPersistentID:{3286F41A-8682-4B81-82EA-5C6A158C5770}" Type="Ref">/My Computer/TREX Controls/CCDS Power Supply/CCDS_Control_Network.lvlib/cap state</Property>
	<Property Name="varPersistentID:{32A819AC-A87A-4C0C-AB09-A2E67A0C4548}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 8</Property>
	<Property Name="varPersistentID:{32C8609C-E4AD-49CC-A06C-A9FB2043ABFD}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO16</Property>
	<Property Name="varPersistentID:{330AF7C8-DB45-49A2-879F-2348AE08FE9F}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO6</Property>
	<Property Name="varPersistentID:{331EAE33-5C6D-42A1-89D1-7C876A496691}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI8</Property>
	<Property Name="varPersistentID:{332E913F-0B85-46E3-8A61-DE8D59900AB3}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_G_Home</Property>
	<Property Name="varPersistentID:{3364DC0C-9C85-49C3-9EE4-58A1050288F2}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO23</Property>
	<Property Name="varPersistentID:{3367492E-3D4B-4693-81CF-80A49094635C}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/S CT PS Summary Fault</Property>
	<Property Name="varPersistentID:{33DB33FA-894B-4104-81ED-50223B66658A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/DTACQ_variables.lvlib/DTACQ Sampling Rate</Property>
	<Property Name="varPersistentID:{343C19BD-96AC-40DD-BB22-AF733E57D668}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS9 Status</Property>
	<Property Name="varPersistentID:{3444AC71-AF2B-4098-B036-1C76497A09C6}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 5</Property>
	<Property Name="varPersistentID:{34709AB4-CD97-4C07-835E-7EA4C09ED886}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC8</Property>
	<Property Name="varPersistentID:{34822C4A-670D-4B9B-ABF1-D2BB324E5862}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_G_Radius</Property>
	<Property Name="varPersistentID:{34BECE5C-31DE-462F-81EC-FEDD7E3E917E}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha1_Network_Controls.lvlib/1_factor</Property>
	<Property Name="varPersistentID:{34EA8F76-D114-4174-985C-69C72D157B32}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS8 Status</Property>
	<Property Name="varPersistentID:{34FD910E-3AB3-434B-AB1C-A1383106DE03}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI16</Property>
	<Property Name="varPersistentID:{351E3D35-CA6D-4923-98A7-4A14541C8F0B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS6 Delay</Property>
	<Property Name="varPersistentID:{35551531-DFA9-406B-84E0-2D195DCFEDB5}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Suspend Heating PS5</Property>
	<Property Name="varPersistentID:{35828D7B-8C9A-4FDB-B332-E7088ABC6648}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_RTFIFO.lvlib/1_STOP</Property>
	<Property Name="varPersistentID:{35843777-4BF1-4BE7-AD2E-3A74D77D6C94}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO7</Property>
	<Property Name="varPersistentID:{366E7316-E0FE-4CFC-822A-B2EE6F0D9C36}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha3_Network_Controls.lvlib/3_voltage</Property>
	<Property Name="varPersistentID:{36CA69CD-236D-4337-B579-01DF314842D3}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO29</Property>
	<Property Name="varPersistentID:{372C093B-CA95-48B6-B0ED-A768A1DCBC8B}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha2_Network_Controls.lvlib/2_factor</Property>
	<Property Name="varPersistentID:{378119CB-40B6-4DCF-B8A4-6029797FA552}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO18</Property>
	<Property Name="varPersistentID:{37AE407D-4619-4D8A-B561-F8427963C9BB}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_E1_Radius</Property>
	<Property Name="varPersistentID:{37B36BDB-13C0-4BA2-A90E-B4DD62A299F2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 11 CathID</Property>
	<Property Name="varPersistentID:{37B4638B-84CB-411E-84F4-F4C673855A4C}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_IO_Aliases.lvlib/3_SetPt</Property>
	<Property Name="varPersistentID:{37D38D05-9414-4F59-836E-764D6F632FCF}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Spectra Time_2</Property>
	<Property Name="varPersistentID:{38185F88-61B3-47CB-A00B-80E026C43F7E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS5 Armed</Property>
	<Property Name="varPersistentID:{385D5D94-C4D7-4F39-B8E6-F51A92C5A465}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI22</Property>
	<Property Name="varPersistentID:{387E27F5-3E89-4189-BE41-9598B6BA74D8}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_A_Radius</Property>
	<Property Name="varPersistentID:{393EEFFF-53B6-41D8-AD1A-FF3A5E1B347B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_12</Property>
	<Property Name="varPersistentID:{397163E4-C152-49F3-BC78-BC067599124F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPE_Radial_Pos</Property>
	<Property Name="varPersistentID:{39E62115-BF16-4248-8B41-7BA97A827FEE}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_ID_Motor_A</Property>
	<Property Name="varPersistentID:{39F9288A-FCBD-4562-A3DC-31D52B8A4DA7}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_Network.lvlib/Alpha1_remote stop</Property>
	<Property Name="varPersistentID:{3A6FF1BC-7191-42FC-9B42-ACA6674EDC54}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC11</Property>
	<Property Name="varPersistentID:{3A761B56-2F6B-40D1-8A50-DA7A41E86319}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP9_10</Property>
	<Property Name="varPersistentID:{3ADA2FDE-F0DE-419B-BD26-74CC05825CF0}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO28</Property>
	<Property Name="varPersistentID:{3AF7DE86-FD03-4BFF-8811-DEA2CCC9E36F}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO0</Property>
	<Property Name="varPersistentID:{3B62A44C-BB32-41A8-AF2C-61239EF8DCFC}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS12 Control Mode</Property>
	<Property Name="varPersistentID:{3B7ACF95-F10B-4076-A1DE-DEE6D2DC4A70}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_B1_Radius</Property>
	<Property Name="varPersistentID:{3B91613F-5724-43BD-ADAC-B3D6C3C8B2D5}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO26</Property>
	<Property Name="varPersistentID:{3BBAC1B8-7C70-4C11-8ABD-375CB0F4D8F2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 3 CathID</Property>
	<Property Name="varPersistentID:{3BCE8676-8FEE-49ED-8BD1-EEBE6E74C8AD}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha2_IO_Aliases.lvlib/2_CurrentOut</Property>
	<Property Name="varPersistentID:{3BE378CA-0C99-4108-8685-5C5CCE49666B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Puff Duration 2</Property>
	<Property Name="varPersistentID:{3BFE934C-675E-40B5-A9D7-51BAF60FE6F3}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/S CT Relay Enable</Property>
	<Property Name="varPersistentID:{3C38A519-9E0D-4C9B-8CBD-0A13F85EE0D2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/DTACQ_variables.lvlib/DTACQ_status</Property>
	<Property Name="varPersistentID:{3DF630D1-ADE2-4BC3-A4C9-0E6C9C845D86}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP5_6_theta</Property>
	<Property Name="varPersistentID:{3F1523AD-6EFA-487C-BA8E-9AE212DC66A8}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Spectrum Delay_2</Property>
	<Property Name="varPersistentID:{402B4ED5-B03B-4ABE-B4DC-2943FC296770}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 5 CathID</Property>
	<Property Name="varPersistentID:{405BB326-EF90-43C0-93BA-CC40F4F4405B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Operator name</Property>
	<Property Name="varPersistentID:{40F6AA07-1EB8-4A57-9379-F1BE4798DCA7}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC10</Property>
	<Property Name="varPersistentID:{4154C474-1C2A-48C4-A0FB-31CE7EF3B26A}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO17</Property>
	<Property Name="varPersistentID:{41600E30-2534-4B98-AFFE-994339A82504}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS1 Status</Property>
	<Property Name="varPersistentID:{419B749B-860D-4DB9-8D35-C65CCD3456A7}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_D3_Radius</Property>
	<Property Name="varPersistentID:{419D5F12-6826-4478-9AB3-CD4CEAD12507}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Misc_variables.lvlib/IAW Charge Duration</Property>
	<Property Name="varPersistentID:{41D415B0-3F83-4E9B-A2D9-9621C52BF01D}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO26</Property>
	<Property Name="varPersistentID:{41DF40AF-9295-4E83-A169-6487B7F42737}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_Network.lvlib/EoC CCDS</Property>
	<Property Name="varPersistentID:{41E8D5A8-C542-4ABF-A65B-8F0A278A8D33}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO1</Property>
	<Property Name="varPersistentID:{4233FCDE-AF47-4AD5-B626-87B3724DD7AF}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/N CT Enable PS</Property>
	<Property Name="varPersistentID:{42743CBA-9223-4832-8124-13D756F95364}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/S CT PS End of Charge Fault</Property>
	<Property Name="varPersistentID:{42D3594C-8609-4360-ACA0-75BAE94E6D0A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_3</Property>
	<Property Name="varPersistentID:{430227E7-006F-461D-8FD2-F3D28F220379}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/Shot Setup Data/FP_Shot_Setup.lvlib/FP Notes</Property>
	<Property Name="varPersistentID:{4348381C-3C3F-41C0-8F00-9EA09C8A5AE7}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_IO_Aliases.lvlib/EOC Reset</Property>
	<Property Name="varPersistentID:{43BE2D7A-725D-4252-B5F5-0A9B39D6BBFC}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO9</Property>
	<Property Name="varPersistentID:{43C127B5-975B-4F11-B7BC-CF68A4897238}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO31</Property>
	<Property Name="varPersistentID:{440BE375-A161-49A9-AE0E-C3E3B46F2346}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI2</Property>
	<Property Name="varPersistentID:{449DD8EF-7FB4-4C66-959D-DBB7512A8A17}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO8</Property>
	<Property Name="varPersistentID:{44C5A846-6D62-4858-8933-37C92E023961}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_IO_Aliases.lvlib/1_SetPt</Property>
	<Property Name="varPersistentID:{45102EB1-0939-4C84-90FF-ADF42853740B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS10 Shotlength</Property>
	<Property Name="varPersistentID:{453A4D2C-5CDC-49E9-9A96-25A74DE8F077}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS9 Error</Property>
	<Property Name="varPersistentID:{4600F6DA-2F8E-4754-B36E-B1AA24114A48}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Fabry Perot Delay</Property>
	<Property Name="varPersistentID:{4664F475-091A-4E5F-978D-FD7FB715B2C9}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO20</Property>
	<Property Name="varPersistentID:{46C1874C-86F1-4CA0-8EF9-C9FB8DEA7BB2}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO7</Property>
	<Property Name="varPersistentID:{472C13FC-B41B-4D71-A2DB-AD47FE850D36}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_IO_Aliases.lvlib/3_ExtInt</Property>
	<Property Name="varPersistentID:{47557524-4575-48E1-B529-2E61C5792BC6}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC15</Property>
	<Property Name="varPersistentID:{476B975E-823D-42E0-8E4C-D1E53188A7FF}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/2Dprobe_Encoder_CPR</Property>
	<Property Name="varPersistentID:{47EE3423-5D00-42BB-AF81-42C1DDF3139A}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO31</Property>
	<Property Name="varPersistentID:{48136588-FA15-4D71-9F34-94B3633DB37D}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Wavelength</Property>
	<Property Name="varPersistentID:{48B9BBF5-EFC3-4924-9E5C-EAC5AE0AB8F2}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO29</Property>
	<Property Name="varPersistentID:{48BA5E69-1950-47C3-8BE7-CE5C1645EAB4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Number of Measurements</Property>
	<Property Name="varPersistentID:{48E77E1D-A44F-4E6C-8DA8-81169835EBF0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS2 Status</Property>
	<Property Name="varPersistentID:{494E1EC8-9EAB-4C23-8DFA-D383B3452A7B}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO5</Property>
	<Property Name="varPersistentID:{4974BCDA-FB1A-4E57-98F0-4017CD737670}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_4</Property>
	<Property Name="varPersistentID:{4982CBEC-F383-4ABF-8F3D-5B5FCEEB7597}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP19_20</Property>
	<Property Name="varPersistentID:{49C53ADC-C573-4F8F-BED0-AED17970C94D}" Type="Ref">/My Computer/TREX Controls/Heater Cap Controls/HeaterCap_Network_Variables.lvlib/state</Property>
	<Property Name="varPersistentID:{49F09601-5810-4639-8E54-56A37E556DE7}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_4</Property>
	<Property Name="varPersistentID:{49F275CC-F82A-4089-B4B5-72797D61C64D}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 4</Property>
	<Property Name="varPersistentID:{4A013371-3AB1-4688-AA4F-CA07B4D767B8}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_G3_Radius</Property>
	<Property Name="varPersistentID:{4A68A276-B940-4FC2-8C7B-3D6F94AEBC88}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO2</Property>
	<Property Name="varPersistentID:{4A8E1D87-23C3-4437-BFD1-56AE38DA2E6A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Integration Time_3</Property>
	<Property Name="varPersistentID:{4AA9584E-5E76-4AA9-86CD-AEF8DD6C3B04}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS4 Error</Property>
	<Property Name="varPersistentID:{4BF0F34F-2FC1-48A8-A918-E7517CD07878}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC3</Property>
	<Property Name="varPersistentID:{4C1FF378-A828-4B59-93B9-3CBEBD38C0C7}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/2Dprobe_Theta_Pos</Property>
	<Property Name="varPersistentID:{4D118B83-34FF-41FD-81D1-B2547CE49331}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_15</Property>
	<Property Name="varPersistentID:{4D2E6304-B555-4B20-BF7F-F373FA52BFB4}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/S_CT_Abort</Property>
	<Property Name="varPersistentID:{4D63BFE0-E1A8-4F96-BFA9-2DE42BB7E516}" Type="Ref">/My Computer/TREX Controls/Heater Cap Controls/HeaterCap_Network_Variables.lvlib/Charge</Property>
	<Property Name="varPersistentID:{4D77F5ED-3648-4EBC-A545-63AA55346225}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_C2_Radius</Property>
	<Property Name="varPersistentID:{4DE5DEA6-9C41-4DFF-A83E-5A7CD3C15D8F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_8</Property>
	<Property Name="varPersistentID:{4DF34B55-3969-475C-A792-B5BDD723050C}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_IO_Aliases.lvlib/3_On</Property>
	<Property Name="varPersistentID:{4E65700E-BEEA-45FA-89E2-FF6228DE45EB}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_F2_Radius</Property>
	<Property Name="varPersistentID:{4E6F4DDF-048B-40FE-8463-998B27EFB117}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha3_Network_Controls.lvlib/3_off</Property>
	<Property Name="varPersistentID:{4E71DAF7-E22D-4671-BF04-A57006E239AC}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_2</Property>
	<Property Name="varPersistentID:{4EDFE53C-755B-43E6-89D9-13853D154541}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO10</Property>
	<Property Name="varPersistentID:{4EEED3C9-A56E-4292-8ADC-688595568699}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha2_Network_Controls.lvlib/2_on</Property>
	<Property Name="varPersistentID:{4F3FD6B7-9DF8-4ABF-B04D-E7DCD7D26CFB}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha1_Network_Controls.lvlib/1_voltage</Property>
	<Property Name="varPersistentID:{4F8AAE60-653C-4371-ACDE-C5E2E63640B4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS6 Mode</Property>
	<Property Name="varPersistentID:{4FC33C06-724F-4F3A-A34F-5D9892AAE67A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_3</Property>
	<Property Name="varPersistentID:{4FDBFFB1-B839-44FE-A73D-6BAEC47349BD}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Trigger Spectrum_3</Property>
	<Property Name="varPersistentID:{5035B4CC-19C2-4F24-8CC0-1D8757F74FAE}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI27</Property>
	<Property Name="varPersistentID:{5061BB34-4A12-4E51-8D7A-06C05AEC37D0}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha1_Network_Controls.lvlib/1_setpt</Property>
	<Property Name="varPersistentID:{5137ABDA-13FD-4759-9D1A-BEDB52361EE3}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha3_Network_Controls.lvlib/3_setpt</Property>
	<Property Name="varPersistentID:{52273647-6097-49FD-B1E2-72A366D52494}" Type="Ref">/My Computer/TREX Controls/CCDS Power Supply/CCDS_Control_Network.lvlib/message</Property>
	<Property Name="varPersistentID:{52A3C338-2C24-45C5-B201-8EE184EDC6F2}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO16</Property>
	<Property Name="varPersistentID:{52BD905D-CA0E-467B-A17B-A39F056BBEDA}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha1_Network_Controls.lvlib/1_state</Property>
	<Property Name="varPersistentID:{538091AF-E74C-4DC2-AE6A-71CBBA9F864C}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI26</Property>
	<Property Name="varPersistentID:{53CB028E-B88F-4CCE-B65F-0D8683F57DFD}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_RTFIFO.lvlib/STOP</Property>
	<Property Name="varPersistentID:{542581F5-A9FA-45E8-BF52-F7195FEC1730}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_C_Home</Property>
	<Property Name="varPersistentID:{5447673E-03F3-4465-AA95-AB874E1EAAC9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS9 Cathode ID</Property>
	<Property Name="varPersistentID:{54C6EEB1-A712-4242-8C6C-767020BEA937}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI23</Property>
	<Property Name="varPersistentID:{54F92F78-7854-4F35-8999-526DE19F256E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP7_8</Property>
	<Property Name="varPersistentID:{5527990F-0D12-4168-9A21-317D899BEC43}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO8</Property>
	<Property Name="varPersistentID:{5532CFEB-F860-4558-B3A0-3A2AF307043E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Wavelength_3</Property>
	<Property Name="varPersistentID:{5557A665-044A-45FB-B66F-B8520232067C}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC4</Property>
	<Property Name="varPersistentID:{5558B793-1DE0-496E-8011-2012AD2DD67A}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI9</Property>
	<Property Name="varPersistentID:{557A0C51-71AD-411A-BEFC-43F9CD1F92E7}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS4 Setpoint</Property>
	<Property Name="varPersistentID:{565F2CF8-F8A1-459A-B5B4-03F7385767F4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_7</Property>
	<Property Name="varPersistentID:{56D1B9EB-9822-49A4-BABC-BB3F214C351E}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_IO_Aliases.lvlib/HV Connect</Property>
	<Property Name="varPersistentID:{56E5D906-57F4-4C85-9B54-EC46FBF72F41}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC5</Property>
	<Property Name="varPersistentID:{576C08EA-6562-4790-B104-14B7F178C1C9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_F_Radius</Property>
	<Property Name="varPersistentID:{577A3A49-DCDA-4FD7-90EC-9BFE436C71E9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS7 HeaterID</Property>
	<Property Name="varPersistentID:{57F0A7BE-B92D-455F-B163-8937C97E9955}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 9</Property>
	<Property Name="varPersistentID:{58364738-7B66-458C-9505-8F273ECEAF56}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS5 Status</Property>
	<Property Name="varPersistentID:{59921140-F94F-4B00-981D-182449C11C63}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO8</Property>
	<Property Name="varPersistentID:{5A0EFC3F-4B05-41D5-A284-8C69379F3421}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_14</Property>
	<Property Name="varPersistentID:{5A326630-C12B-4A71-9422-935E3FB84F4B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/mm-Wave Delay</Property>
	<Property Name="varPersistentID:{5AD44AAE-A1C5-4313-B65F-AC747939738D}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS9 Armed</Property>
	<Property Name="varPersistentID:{5AD8183B-0461-459B-B9BB-AB2E0EFEB8CE}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_RTFIFO.lvlib/3_STOP</Property>
	<Property Name="varPersistentID:{5BA9D6C5-3397-4742-BA69-2D704E02E243}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPD_Radial_Pos</Property>
	<Property Name="varPersistentID:{5C054113-B13D-4B22-8F4F-DA9FD0A3E22B}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_IO_Aliases.lvlib/HV On</Property>
	<Property Name="varPersistentID:{5C85B00A-88BD-40CB-9DE8-97B0C41323E2}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/Sh_R</Property>
	<Property Name="varPersistentID:{5C8FECE9-E207-4D05-A6A3-7616520FB711}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO0</Property>
	<Property Name="varPersistentID:{5CC27B98-89D8-4F60-902A-3E1F3249DEBA}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Suspend Heating PS4</Property>
	<Property Name="varPersistentID:{5D1AF210-9D6D-4B4E-A3C8-28F5538BAACA}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS5 Cathode ID</Property>
	<Property Name="varPersistentID:{5D5288FA-5155-4AFD-987C-CC21112312D4}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO9</Property>
	<Property Name="varPersistentID:{5DA5B5D8-EA8E-455E-B2C1-13F1277286E8}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS7 Armed</Property>
	<Property Name="varPersistentID:{5E0F19B1-127F-498F-8B53-98BFD80D50DD}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO24</Property>
	<Property Name="varPersistentID:{5E191C95-F79F-4F63-A4F2-21DEC389D407}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_ID_Motor_B</Property>
	<Property Name="varPersistentID:{5E82187E-5B80-465C-A8E6-05DD7289E80A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/mm-Wave Duration</Property>
	<Property Name="varPersistentID:{5E98CD6B-7295-44C9-B3EA-5D0F7B60B636}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Fire</Property>
	<Property Name="varPersistentID:{5F3BFD3B-4171-4049-81BE-DE4D7C08302A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS4 Shotlength</Property>
	<Property Name="varPersistentID:{5F659A6E-7D2D-48F8-BCD2-163A40DFBC44}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha1_Network_Controls.lvlib/1_on</Property>
	<Property Name="varPersistentID:{5F7025BD-D066-406E-8C58-98FF88A9348E}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_IO_Aliases.lvlib/Cap Voltage</Property>
	<Property Name="varPersistentID:{603D031C-67BA-40FF-8448-275E5787FD99}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO20</Property>
	<Property Name="varPersistentID:{60449F9F-7C2C-4BCD-908C-669AD26A04EB}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS5 Setpoint</Property>
	<Property Name="varPersistentID:{6143BF86-9466-4BF8-BDD6-18A35292D654}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_6</Property>
	<Property Name="varPersistentID:{6197C224-E1CB-4112-AC43-A4DB494A1DE1}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS8 Mode</Property>
	<Property Name="varPersistentID:{62B7C53D-5587-47ED-BBFC-DECE31489328}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI0</Property>
	<Property Name="varPersistentID:{6305DEF6-8A3F-4AD1-AFEA-4DD5B55B980D}" Type="Ref">/My Computer/TREX Controls/CCDS Power Supply/CCDS_Control_Network.lvlib/charge</Property>
	<Property Name="varPersistentID:{634A8B03-70EE-462F-AA03-E1E9E1CA7D0F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_ID_Motor_D</Property>
	<Property Name="varPersistentID:{63D227DD-CEE0-4880-932A-0872570C1AE0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPH_Radial_Home</Property>
	<Property Name="varPersistentID:{642EA9E9-B120-45B6-9A83-9D4BA5DF2100}" Type="Ref">/My Computer/Run Local.lvlib/stop</Property>
	<Property Name="varPersistentID:{65201D3F-7B1A-44B7-BEA7-6FD4923512FB}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO9</Property>
	<Property Name="varPersistentID:{655FB175-5548-42EB-872C-403A869717B5}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/DTACQ_variables.lvlib/DTACQ_uploaded</Property>
	<Property Name="varPersistentID:{65B02B16-783F-4993-AC4C-8800A26C047F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/DTACQ_variables.lvlib/Auto-arm D-TACQ</Property>
	<Property Name="varPersistentID:{6642E059-9BF0-4A0C-A3C3-FF49C669660F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPA_Radial_Home</Property>
	<Property Name="varPersistentID:{67536CD6-05EF-43F3-A85B-CA38D51F07F2}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI20</Property>
	<Property Name="varPersistentID:{67842963-2FEE-4B4B-B61E-28C74C447984}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS5 Shotlength</Property>
	<Property Name="varPersistentID:{6861BE46-0825-4694-B2A4-F8D7DB608C3E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 12 PSID</Property>
	<Property Name="varPersistentID:{69306CCF-F29B-4596-8365-D2778A32F3C2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 6</Property>
	<Property Name="varPersistentID:{69A43073-9905-4D3C-98B5-489897202C8A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath8</Property>
	<Property Name="varPersistentID:{69BCFC16-8B96-4767-A27C-8C07D021BC39}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 1 CathID</Property>
	<Property Name="varPersistentID:{69BD22AA-CFB9-43F1-BF7E-9427214AE95B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Shot Spectrum_2</Property>
	<Property Name="varPersistentID:{6A4A4FD4-7005-433F-AE9D-3DAC301DA159}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TC4 Voltage</Property>
	<Property Name="varPersistentID:{6B062699-EE49-48B4-B3AD-2D14815C0C65}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_2</Property>
	<Property Name="varPersistentID:{6B2BC004-2F58-454E-9EB4-B93A179864B5}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO14</Property>
	<Property Name="varPersistentID:{6CC1E30B-4FFF-4860-B4C2-82554940E950}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO25</Property>
	<Property Name="varPersistentID:{6D2A6E06-CDE5-401E-9AB5-6A3484D74F25}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_1</Property>
	<Property Name="varPersistentID:{6D2D133E-F526-48F4-81CF-29C5C3F4265F}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha2_IO_Aliases.lvlib/2_On</Property>
	<Property Name="varPersistentID:{6D634EB2-BCAD-43C6-86E0-8F535B7BA962}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS3 Status</Property>
	<Property Name="varPersistentID:{6DB08ADB-CCB2-488A-B522-4F53F1C0095B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_D_Home</Property>
	<Property Name="varPersistentID:{6DC2146B-FF18-487D-AD3D-6D6FF00A6C95}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_Network.lvlib/Alpha1_setpt</Property>
	<Property Name="varPersistentID:{6DC3C526-2533-4A0E-9700-EB6009A5CC56}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO12</Property>
	<Property Name="varPersistentID:{6DF5511D-7A90-43DB-89DF-5793C4AA27EE}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_D1_Radius</Property>
	<Property Name="varPersistentID:{6ECE6503-0200-42C4-8FCD-9C598EEA835B}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO4</Property>
	<Property Name="varPersistentID:{6F0BE700-994D-4C06-9D2F-6A9086E58287}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS5 Delay</Property>
	<Property Name="varPersistentID:{6F4A1143-1BDE-4B14-87DC-DF05B3D117CE}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO3</Property>
	<Property Name="varPersistentID:{6F4BB296-3FA9-4577-99F5-854EAECF8AE5}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS10 Cathode ID</Property>
	<Property Name="varPersistentID:{7035F171-4E23-4085-9C73-390A5A97A035}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS8 Setpoint</Property>
	<Property Name="varPersistentID:{706BD1AE-4CD7-4B60-AB28-83A34B3786C3}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI18</Property>
	<Property Name="varPersistentID:{709D1F31-9DF5-460C-AD8F-3D92D80A8191}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Log Complete</Property>
	<Property Name="varPersistentID:{70A7B637-2EDE-4610-A699-95C7F049949D}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO31</Property>
	<Property Name="varPersistentID:{71455CF1-6C67-4A80-BB3F-CEAA2546B6CB}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Puff Duration 4</Property>
	<Property Name="varPersistentID:{7154FE0D-A290-4150-8239-363CC1995092}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/mm-Wave Trigger</Property>
	<Property Name="varPersistentID:{71C61F5E-43CC-4C32-A004-28AA087C6010}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Heater Cap Bank/HeaterCap_Control_IO_Aliases.lvlib/Charge</Property>
	<Property Name="varPersistentID:{71C7C5FA-1722-4C1D-AA95-85C0ECF65895}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 9 CathID</Property>
	<Property Name="varPersistentID:{726E0C79-2D1C-4C68-973F-E4A782052B76}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS8 Shotlength</Property>
	<Property Name="varPersistentID:{72A41178-6769-484E-9C69-FBE919B5E316}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_Network.lvlib/Alpha1_off</Property>
	<Property Name="varPersistentID:{72A79149-DD90-4F88-BC4F-6E1813EBB2A8}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS1 Shotlength</Property>
	<Property Name="varPersistentID:{72B3D86A-78A2-4311-9324-4EAAD53B39FA}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Heater Cap Bank/HeaterCap_Control_IO_Aliases.lvlib/Interlock</Property>
	<Property Name="varPersistentID:{72D27CD3-08B9-4EAD-8F11-807A514FD808}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/FabryTakeCal</Property>
	<Property Name="varPersistentID:{73B9CD95-EB3C-4B96-8F20-A427F620CBB8}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_12</Property>
	<Property Name="varPersistentID:{73E533FA-955B-4124-BC5F-A4E117985A58}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS4 Status</Property>
	<Property Name="varPersistentID:{73E94703-11B7-45D0-9B6F-332417602BE8}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Magnetron Variables.lvlib/Magnetron1_SetPoint</Property>
	<Property Name="varPersistentID:{7518F471-9539-4729-8767-DDD146348F4D}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 6 CathID</Property>
	<Property Name="varPersistentID:{757FAE46-5399-4F0A-B501-567792B7109A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Suspend Heating PS6</Property>
	<Property Name="varPersistentID:{7588FEE9-2089-4799-8E87-4371800FD3C8}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO19</Property>
	<Property Name="varPersistentID:{75A05E07-5824-4657-9905-63D056E4385D}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_IO_Aliases.lvlib/1_CurrentOut</Property>
	<Property Name="varPersistentID:{7612ED2B-CF98-45A6-BFA8-B51AFF032A5E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 4 CathID</Property>
	<Property Name="varPersistentID:{763593A8-1430-4ECD-B934-B52812890812}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_IO_Aliases.lvlib/1_VoltageOut</Property>
	<Property Name="varPersistentID:{769BF091-6995-4D34-92B2-B1CE2738ED38}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Suspend Heating PS9</Property>
	<Property Name="varPersistentID:{770F7D6B-D465-4DA3-B2E5-2BCC7308067D}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO28</Property>
	<Property Name="varPersistentID:{77971F1F-146C-4C5E-BE97-E49092175AF0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS10 HeaterID</Property>
	<Property Name="varPersistentID:{7841EF8D-2FB4-499A-82D8-3472F8A6EFF9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS3 HeaterID</Property>
	<Property Name="varPersistentID:{78CFBFBE-A98A-45F9-B4E7-2024C3FA6AE0}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO5</Property>
	<Property Name="varPersistentID:{78E071ED-A8D3-45D0-9C30-3324FDD82976}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO1</Property>
	<Property Name="varPersistentID:{79146534-D135-4797-BBA1-49EAC1EE7BE6}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO19</Property>
	<Property Name="varPersistentID:{798360F9-767E-4F82-8197-618F27C10FA6}" Type="Ref">/My Computer/Motor VIs/compact_stage_controller.lvlib/probe_variables.lvlib/theta_vi</Property>
	<Property Name="varPersistentID:{79878279-559A-4773-93B8-F3ABF0403842}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO20</Property>
	<Property Name="varPersistentID:{7A535358-1B33-4D60-8140-BA75417AA42C}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC10</Property>
	<Property Name="varPersistentID:{7A6B1D01-6FEE-4D75-9D3A-2B60F6FCF9EC}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/RetrieveFabryData</Property>
	<Property Name="varPersistentID:{7AC15EF3-50CE-4A6D-A0FC-BE1FBA3B1B70}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS6 Shotlength</Property>
	<Property Name="varPersistentID:{7B6E5788-BCAE-4B06-8025-FB86B3A3FF13}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI11</Property>
	<Property Name="varPersistentID:{7B737C2B-4BBA-4504-8727-D3D33CA5995A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Time Between Measurements_3</Property>
	<Property Name="varPersistentID:{7B95E8F2-73EC-4082-9B0D-A306655FA227}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS4 HeaterID</Property>
	<Property Name="varPersistentID:{7BB5E50A-A004-4C86-B0E3-9FFDA19BDBC5}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Puff Duration</Property>
	<Property Name="varPersistentID:{7C9F2484-E24B-4CF2-8D8D-210762DEEA16}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Number of Measurements_2</Property>
	<Property Name="varPersistentID:{7D9D0FE7-007A-41E6-A493-318A32191605}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO13</Property>
	<Property Name="varPersistentID:{7DD5CD2E-9683-4DC6-9A2A-BD0708B16825}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO26</Property>
	<Property Name="varPersistentID:{7E541773-98A5-44A0-8A43-49241C9623AB}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_8</Property>
	<Property Name="varPersistentID:{7E800F18-E724-479E-A401-2FDEE8808A12}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS9 Shotlength</Property>
	<Property Name="varPersistentID:{7F06F8E7-5B63-4C8B-BBA7-9896CCF04AE2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS8 Delay</Property>
	<Property Name="varPersistentID:{7F1D440C-7E92-47CD-8072-2732E33D62F9}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/S_CT_Armed</Property>
	<Property Name="varPersistentID:{7F1EEFE3-1DD2-4447-88E2-CB494AA01EFC}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha2_Network_Controls.lvlib/2_setpt</Property>
	<Property Name="varPersistentID:{7F471FF6-4994-4ECD-B705-12F212DBF7F0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Shot Spectrum</Property>
	<Property Name="varPersistentID:{7FA049C6-0F32-4827-9F6C-B3EE2AFD784B}" Type="Ref">/My Computer/TREX Controls/Heater Cap Controls/HeaterCap_Network_Variables.lvlib/Discharge</Property>
	<Property Name="varPersistentID:{7FBEA1E9-9EF4-42D9-B85C-0A5EABAD903A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS12 HeaterID</Property>
	<Property Name="varPersistentID:{7FD702DD-4B7D-4372-A202-0AA40AD69086}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS1 Delay</Property>
	<Property Name="varPersistentID:{802EE282-FE39-4497-BB89-8F172488A49C}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/2Dprobe_Theta_Home</Property>
	<Property Name="varPersistentID:{805757BC-D27A-4044-B69D-01000A256A1F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_E2_Radius</Property>
	<Property Name="varPersistentID:{806A393F-CE62-48B9-9DCE-EB5A35735AC6}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO12</Property>
	<Property Name="varPersistentID:{80741D01-B75B-4B86-81BF-FD4E013ED816}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/N_CT_State</Property>
	<Property Name="varPersistentID:{81D37AF7-34C9-4BC7-85F6-9AD4BA9371A5}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/Cap_enabled</Property>
	<Property Name="varPersistentID:{81D957F5-DF2B-47C2-B497-E81567020EC0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS10 Status</Property>
	<Property Name="varPersistentID:{826867B7-1723-49FB-82C4-61491D8CA89C}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/DTACQ_variables.lvlib/Skip Record</Property>
	<Property Name="varPersistentID:{827A7B92-BB7E-4D7D-8CB6-6B9529F6B2D2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Breakdown Type</Property>
	<Property Name="varPersistentID:{83DC4426-58A8-43A2-9CC2-922755791579}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_C3_Radius</Property>
	<Property Name="varPersistentID:{84375F72-C6D2-47BE-99B8-DEF4BC998840}" Type="Ref">/My Computer/TREX Controls/Heater Cap Controls/HeaterCap_Network_Variables.lvlib/Voltage</Property>
	<Property Name="varPersistentID:{843E145E-12B4-42D6-A607-D7097B0B5812}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO15</Property>
	<Property Name="varPersistentID:{84422D8F-48C3-4358-9CA1-5223573ED24F}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO10</Property>
	<Property Name="varPersistentID:{8451F3FF-0463-4D62-B2DA-24F03AD6A05F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/ShotExposureTime</Property>
	<Property Name="varPersistentID:{84C3C87D-903D-45EB-8DC6-6844BF47C7AB}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO15</Property>
	<Property Name="varPersistentID:{8569D0A7-DFD2-43A5-9F9F-075ADA335E0E}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_RTFIFO.lvlib/setpt input</Property>
	<Property Name="varPersistentID:{8598D36A-AA4C-45FC-9EF8-0D2B7FBFF73D}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_G1_Radius</Property>
	<Property Name="varPersistentID:{862076B9-8717-4A98-961A-FBACCF0932CE}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_10</Property>
	<Property Name="varPersistentID:{8652D9E5-4A07-422B-9207-72D2A998F536}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_10</Property>
	<Property Name="varPersistentID:{86B62FA5-E4D7-405E-94BB-877303BBC7F6}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO5</Property>
	<Property Name="varPersistentID:{86E8E430-B14F-46D6-B746-F8A1D2585B9C}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC5</Property>
	<Property Name="varPersistentID:{86EDDDDF-9518-4A0D-93EE-24B8D34C2659}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/PlasmaTurnOffTime</Property>
	<Property Name="varPersistentID:{8725FEB6-5540-4AA0-8A9B-1CD97B660887}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_Network.lvlib/Alpha1_on</Property>
	<Property Name="varPersistentID:{872BEECA-0BDB-4915-9FB5-B89EEEE0480D}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/MFC Voltage</Property>
	<Property Name="varPersistentID:{87478E5F-2DB5-4957-ABA4-50B5A242DEB9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 1 PSID</Property>
	<Property Name="varPersistentID:{8853EA14-5F86-499A-A05A-32E2BDFAC914}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_B3_Radius</Property>
	<Property Name="varPersistentID:{89156B22-01B7-4C74-ACCE-D5052DD59369}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS5 Mode</Property>
	<Property Name="varPersistentID:{891B99A6-D416-4673-8CA9-BF0FCE58C9E4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_F3_Radius</Property>
	<Property Name="varPersistentID:{894FA28E-9CC2-49AF-9B31-2E719417A336}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/N_CT_Message</Property>
	<Property Name="varPersistentID:{8AD8D7FA-7118-4D05-B0C6-D780DD1B88F8}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS7 Cathode ID</Property>
	<Property Name="varPersistentID:{8AE7F1EE-A95C-4B1E-921E-9F563F8764BB}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI5</Property>
	<Property Name="varPersistentID:{8B3161DB-3920-4BD7-BD31-71CBA709F488}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI4</Property>
	<Property Name="varPersistentID:{8B43A152-9442-4D70-B077-7612339DD93F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_8</Property>
	<Property Name="varPersistentID:{8BA87B72-FD39-466F-A5E9-AC75D852345B}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO1</Property>
	<Property Name="varPersistentID:{8BD400B7-DDBA-450C-BC97-D9A2B44128E5}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_IO_Aliases.lvlib/1_Ready</Property>
	<Property Name="varPersistentID:{8BD847A6-E643-4F1D-8FC4-4048CAC347C6}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_ID_Motor_F</Property>
	<Property Name="varPersistentID:{8BFEE6F3-FD48-4A3C-8D3D-F0FCFEB2F681}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/N CT Cap Voltage</Property>
	<Property Name="varPersistentID:{8C757776-CBCA-49A7-937C-2B8CFA50067E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath4</Property>
	<Property Name="varPersistentID:{8D62CE9D-D9F5-43E9-8BC8-9310F20E1FBB}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO2</Property>
	<Property Name="varPersistentID:{8D7AE10B-1237-4607-A563-4B79F402180B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 10 CathID</Property>
	<Property Name="varPersistentID:{8DD4E465-6924-4527-AF19-F21B27C82F1D}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/Shot Setup Data/FP_Shot_Setup.lvlib/calib id</Property>
	<Property Name="varPersistentID:{8E4DEA6A-48C2-4B51-B1D2-A8AFF089C153}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO17</Property>
	<Property Name="varPersistentID:{8EB557A2-0756-43A8-AE2A-5BCDF77515F7}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_H2_Radius</Property>
	<Property Name="varPersistentID:{8F145D22-62CE-49D5-8A20-6A351FA09B10}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO2</Property>
	<Property Name="varPersistentID:{8F8CBF5C-0C57-4596-B866-95DDF0680CAD}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO17</Property>
	<Property Name="varPersistentID:{90679C05-22B5-4BCD-950F-293C42C11600}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO12</Property>
	<Property Name="varPersistentID:{913B911A-0A1F-4C49-BDB6-B5747C1694AB}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO0</Property>
	<Property Name="varPersistentID:{91A0FB99-AC30-40FE-AAB4-C6EE2D611F88}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Shot Spectrum_3</Property>
	<Property Name="varPersistentID:{91D7C314-A198-4473-98B3-0E07434AF50C}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO30</Property>
	<Property Name="varPersistentID:{91FEE76F-F06D-4B34-9050-056FA2607D2E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPE_Radial_Home</Property>
	<Property Name="varPersistentID:{9253CD70-1EB2-414C-9E3E-46B559E3252B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath6</Property>
	<Property Name="varPersistentID:{932DD58D-DE5B-4F87-910D-F619CD47C842}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/N_CT_Fault</Property>
	<Property Name="varPersistentID:{9346C659-26AF-4E9A-BF3C-8D8A00DFF0CF}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_Network.lvlib/Alpha3_off</Property>
	<Property Name="varPersistentID:{9362E7E6-809D-498D-9719-4D778CE7393D}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_H1_Radius</Property>
	<Property Name="varPersistentID:{936C8ACC-34DA-45A5-A7EC-1EB9288FFACF}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS10 Control Mode</Property>
	<Property Name="varPersistentID:{93FF7E20-4A0C-4601-B432-A1FB361E0C15}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 12 CathID</Property>
	<Property Name="varPersistentID:{94861688-8A4A-441D-98DA-1072EA4F53D1}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO0</Property>
	<Property Name="varPersistentID:{95144E4A-BF62-4189-AD16-00971352AC82}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_9</Property>
	<Property Name="varPersistentID:{95228A57-2455-47AC-991D-FC07097984BA}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS1 Setpoint</Property>
	<Property Name="varPersistentID:{954F89CD-08AE-4D86-8533-14A31DFCC618}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/S_CT_Voltage_Set</Property>
	<Property Name="varPersistentID:{95B8E373-7402-4694-9C17-3B886CB9811A}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO4</Property>
	<Property Name="varPersistentID:{95E25B00-355B-4D9E-A468-88CC2CDD1BCC}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS9 Delay</Property>
	<Property Name="varPersistentID:{95F656E3-BF9E-4329-BC6B-DBC5BED17457}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC3</Property>
	<Property Name="varPersistentID:{966DD123-B72B-4FFB-B285-486794663CF0}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO25</Property>
	<Property Name="varPersistentID:{96AE4EAC-1D16-406D-B3EB-6A161B71C691}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO25</Property>
	<Property Name="varPersistentID:{96C88447-1785-430A-9E2F-A69F7EDF4682}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha2_IO_Aliases.lvlib/2_VoltageOut</Property>
	<Property Name="varPersistentID:{9784F285-82A2-4310-BE4A-3B57B4272699}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 4 PSID</Property>
	<Property Name="varPersistentID:{97901F7B-B0B5-4843-A769-F833B64024BE}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO27</Property>
	<Property Name="varPersistentID:{982F099D-3B83-435E-9A87-02159BD990F0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP21_22</Property>
	<Property Name="varPersistentID:{991BB170-ADD9-497E-9508-8D21F00DADD6}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 2 CathID</Property>
	<Property Name="varPersistentID:{99D85466-BEE3-4D13-839C-E4BC2C3594D3}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/N_CT_Trig</Property>
	<Property Name="varPersistentID:{99E30479-F6DA-42BA-AD47-2CD7BD11334E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 16</Property>
	<Property Name="varPersistentID:{9A4DFB0C-6EC8-4EBA-8075-23F657B285DB}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Log_trigger</Property>
	<Property Name="varPersistentID:{9AAFBA2B-7822-4941-8CA6-7208B92B9551}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC6</Property>
	<Property Name="varPersistentID:{9ADF39BD-A79B-4B4A-91A5-B06BFE622236}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Suspend Heating PS7</Property>
	<Property Name="varPersistentID:{9AF2D6C3-5626-40F8-9963-3965BAF8CAC2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 3 PSID</Property>
	<Property Name="varPersistentID:{9B0E5497-59E7-4AAD-BF70-86266C56F733}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS9 HeaterID</Property>
	<Property Name="varPersistentID:{9C38548D-B1E8-431E-A227-077776BACE2C}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_Network.lvlib/Alpha1_Current</Property>
	<Property Name="varPersistentID:{9D1FBD63-CC7D-4281-82B1-BBAB4DCFEDA6}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Time Between Measurements_2</Property>
	<Property Name="varPersistentID:{9D2149CB-7FE4-4CFF-A989-DD5FE77A168D}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/S_CT_Message</Property>
	<Property Name="varPersistentID:{9D448C3E-9C59-4130-BB14-C0675AB8AD32}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_16</Property>
	<Property Name="varPersistentID:{9D701226-1983-4D57-B82C-80E48EFFEA4C}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 3</Property>
	<Property Name="varPersistentID:{9D94B87F-0C32-4A62-919D-EF6D9998F62F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath2</Property>
	<Property Name="varPersistentID:{9E2FC2D8-E243-4BB3-A75D-22685B6D3D50}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_RTFIFO.lvlib/3_setpt in</Property>
	<Property Name="varPersistentID:{9E3FE641-B492-42BE-BD7D-C7AC71BE7934}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 7 CathID</Property>
	<Property Name="varPersistentID:{9E86201B-7AEF-4E3B-8BEE-7AF1B4114DDB}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Shot Comment</Property>
	<Property Name="varPersistentID:{9FC7ED13-0EFF-4329-AECC-31CF645816D2}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC8</Property>
	<Property Name="varPersistentID:{9FCCDC2C-F71E-432C-A771-E34B45192B21}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/N CT PS Summary Fault</Property>
	<Property Name="varPersistentID:{A0124AE0-CC77-4BA7-9CFB-73FAAFC66939}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/2Dprobe_Radial_Home</Property>
	<Property Name="varPersistentID:{A01F264C-9F11-4325-A979-8CC3497D9328}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS8 Error</Property>
	<Property Name="varPersistentID:{A020144C-E05B-4EC9-9CC4-806EA6863B91}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_RTFIFO.lvlib/3_voltage out</Property>
	<Property Name="varPersistentID:{A0BA5762-B0A3-4961-A14D-03F9286CC108}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/RGA_trend_trigger</Property>
	<Property Name="varPersistentID:{A0D3C214-4AA2-44C8-A3FF-753FD9E1CD2A}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_RTFIFO.lvlib/voltage outputs</Property>
	<Property Name="varPersistentID:{A1B137FE-4F67-441A-980A-E4275F4F0250}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/Shot Setup Data/FP_Shot_Setup.lvlib/Wavelength</Property>
	<Property Name="varPersistentID:{A23780E0-F92E-4B87-B4A9-1BAF705BC593}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI29</Property>
	<Property Name="varPersistentID:{A2CD26A9-55C1-450C-A543-673AE190C805}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha3_Network_Controls.lvlib/3_bfield</Property>
	<Property Name="varPersistentID:{A32E167E-D18B-4C76-A01F-F623E6D8F29E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Wave Launch Delay</Property>
	<Property Name="varPersistentID:{A35CB41B-9289-457A-9C83-1EBBD7F768F1}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/S CT Set Voltage</Property>
	<Property Name="varPersistentID:{A36A4A3D-5A81-45B4-95A1-A069B73E55D6}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO11</Property>
	<Property Name="varPersistentID:{A3A8015D-EC70-4150-B1AE-651275E62241}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO28</Property>
	<Property Name="varPersistentID:{A49D3B2B-F7F4-4542-8020-40CF0EEF9C9F}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC4</Property>
	<Property Name="varPersistentID:{A4A515DB-83A0-473C-AA44-E1A513A7A49B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Trigger Variables.lvlib/Master Triggers</Property>
	<Property Name="varPersistentID:{A4AD72B8-0B61-4D6C-B272-1AE31906DC7A}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha1_Network_Controls.lvlib/1_bfield</Property>
	<Property Name="varPersistentID:{A4DFA84B-50C0-4427-A8FC-DEB71EE3FA51}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_H_Home</Property>
	<Property Name="varPersistentID:{A4F3A192-4EE5-44D7-AEEC-D2E0A24C5BDE}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS2 Shotlength</Property>
	<Property Name="varPersistentID:{A53D6B77-F257-49C5-9D6A-E76B769D501D}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/S CT Enable PS</Property>
	<Property Name="varPersistentID:{A54DE25A-1043-4D79-B650-446A26ACD26A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS6 Error</Property>
	<Property Name="varPersistentID:{A60E6947-88D8-450B-AF52-D18554E431C0}" Type="Ref">/My Computer/Motor VIs/compact_stage_controller.lvlib/probe_variables.lvlib/R_mach</Property>
	<Property Name="varPersistentID:{A84096A3-897D-4721-A13A-BFF6DDB2F448}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC9</Property>
	<Property Name="varPersistentID:{A85ECD2F-552A-48DB-A427-FDD075C26EBA}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Spectrum Delay</Property>
	<Property Name="varPersistentID:{A87957E2-13E7-4FDB-ABA2-D42F8ABAA45D}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_1</Property>
	<Property Name="varPersistentID:{A8B923BC-7CA1-4B8D-B699-BCB9FF64333B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 10 PSID</Property>
	<Property Name="varPersistentID:{A8D780C6-E701-48C7-A1B9-908918A97A30}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Trigger Variables.lvlib/Timed Trigger</Property>
	<Property Name="varPersistentID:{A8F06B49-DCE0-406F-86C2-6EA82877EADC}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO15</Property>
	<Property Name="varPersistentID:{A906EB8B-D09C-4906-B44D-6C4F80952F9E}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha2_IO_Aliases.lvlib/2_ExtInt</Property>
	<Property Name="varPersistentID:{AA9A201A-75AD-4E8D-9683-E7078B9F9237}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath11</Property>
	<Property Name="varPersistentID:{AAB60176-8A22-4CC3-A8C9-8813D0961A4B}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO22</Property>
	<Property Name="varPersistentID:{AB1C0651-BE94-4966-8138-5AB12395B051}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI28</Property>
	<Property Name="varPersistentID:{AB40D73D-0C91-4A3B-B3A1-B315C454CF02}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_Network.lvlib/Alpha3_ready</Property>
	<Property Name="varPersistentID:{AB75E116-13C6-4507-A84F-CB96DDE0DCC1}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/HallProbeVariables.lvlib/R0_3</Property>
	<Property Name="varPersistentID:{AB9B3C08-5C28-4D98-AC73-D3FC2E142012}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 7 PSID</Property>
	<Property Name="varPersistentID:{ABFD92E6-D875-4F8C-BCD4-A8546C148A17}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS10 Error</Property>
	<Property Name="varPersistentID:{AC287D14-365D-4C29-B4F2-D543026EEDA3}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC9</Property>
	<Property Name="varPersistentID:{AC2C9635-B7FB-4E3C-8064-5ECD3A75E2F0}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_RTFIFO.lvlib/1_bool in</Property>
	<Property Name="varPersistentID:{ACCC8775-E588-490B-9BDA-A3EB7050A7C4}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO19</Property>
	<Property Name="varPersistentID:{AD3D5AEA-D95F-4B24-B5BC-A509AD1C76BC}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO24</Property>
	<Property Name="varPersistentID:{ADBB34C9-4E59-4BF3-93A5-9275CEE08013}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_RTFIFO.lvlib/1_setpt in</Property>
	<Property Name="varPersistentID:{ADD902FE-14C4-4E18-A447-4696D799EB9A}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO0</Property>
	<Property Name="varPersistentID:{AE1C3623-D21B-4A27-9119-5D952486AD2F}" Type="Ref">/My Computer/TREX Controls/CCDS Power Supply/CCDS_Control_Network.lvlib/abort</Property>
	<Property Name="varPersistentID:{AE9FDD71-E5BA-423D-9EA3-9547088DF23D}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/S CT Cap Voltage</Property>
	<Property Name="varPersistentID:{AEB9B88D-7D3B-4AE3-A89F-D9FCA37137F6}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI19</Property>
	<Property Name="varPersistentID:{AECFC083-D8A8-4C60-9E95-5F8E3C4A51CA}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS6 Setpoint</Property>
	<Property Name="varPersistentID:{AF3A8DEF-D84C-4B2A-8E64-5428CB251AA7}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO4</Property>
	<Property Name="varPersistentID:{AF592E09-3E85-4C50-B366-7C0BE69F0D1A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_7</Property>
	<Property Name="varPersistentID:{B0044D7E-4BDD-49F5-BB38-5636D188084B}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO3</Property>
	<Property Name="varPersistentID:{B009396B-E765-424B-9CFD-ECC7641C81EF}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/PA_lat</Property>
	<Property Name="varPersistentID:{B030B4DC-4FBD-4F4A-AD87-EADDAD62D710}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 12</Property>
	<Property Name="varPersistentID:{B0468D10-330C-4A06-9E05-88865C04C4FC}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO9</Property>
	<Property Name="varPersistentID:{B0BDF4D7-A93C-4B47-9B8E-C6BB270E7E85}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS10 Mode</Property>
	<Property Name="varPersistentID:{B1233222-A16D-404D-9DEE-2EB8ECFF48F9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/HallProbeVariables.lvlib/Delta_Theta_3</Property>
	<Property Name="varPersistentID:{B12FF1B6-4CC0-45EE-88E5-07958E73563B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Arm Spectrum_3</Property>
	<Property Name="varPersistentID:{B1825637-0994-460C-BEC1-7013466629F1}" Type="Ref">/My Computer/TREX Controls/CCDS Power Supply/CCDS_Control_Network.lvlib/Cap Voltage</Property>
	<Property Name="varPersistentID:{B205CFEB-866C-43CD-B0D1-84993DBDACF9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP13_14</Property>
	<Property Name="varPersistentID:{B23A0ACB-31C1-4B99-951B-4D0A3B9DD1FD}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/CalibrationExposureTime</Property>
	<Property Name="varPersistentID:{B244DC23-D216-4039-9BAA-953C46712B89}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO1</Property>
	<Property Name="varPersistentID:{B25A9992-E774-4271-B8D5-1C2F49A62B5C}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS7 Error</Property>
	<Property Name="varPersistentID:{B2FFC867-6595-472C-A4EE-2EC95B2817B5}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI25</Property>
	<Property Name="varPersistentID:{B352071A-C368-4292-B743-A166D7E74B6C}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPF_Radial_Home</Property>
	<Property Name="varPersistentID:{B362E5FE-97ED-4742-88F0-9353B067394F}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_H_Radius</Property>
	<Property Name="varPersistentID:{B45806D1-300F-471D-8473-CA71F24598FA}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_11</Property>
	<Property Name="varPersistentID:{B49CA451-4538-4DFF-971A-E2C1F27D4180}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS3 Cathode ID</Property>
	<Property Name="varPersistentID:{B4A38F13-CD17-4928-86C5-FAEE4E91CA84}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_5</Property>
	<Property Name="varPersistentID:{B582A8C3-FFEE-401E-B14F-CB6A1DDA2BB5}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS7 Control Mode</Property>
	<Property Name="varPersistentID:{B58CB5F7-9C2A-4690-BC4F-16B6EC26B3AB}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP5_6</Property>
	<Property Name="varPersistentID:{B5AC3C6B-12E1-4AC2-8FA8-0C24FF90F555}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS11 Control Mode</Property>
	<Property Name="varPersistentID:{B5B33A4B-D0E0-4E15-9159-984835D860C9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 5 PSID</Property>
	<Property Name="varPersistentID:{B5E7E2FE-5B78-46EE-9F2F-18E474381D31}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/N CT PS End of Charge Fault</Property>
	<Property Name="varPersistentID:{B6143EAF-B446-4155-8F39-B6FDF0FD1AA4}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC6</Property>
	<Property Name="varPersistentID:{B61E7D89-E417-40F1-805B-E823B920C577}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS2 Control Mode</Property>
	<Property Name="varPersistentID:{B6776C84-1DD2-4BF9-95F9-DA703CA189F0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/RGA Trend Setting</Property>
	<Property Name="varPersistentID:{B6B339AD-3565-454C-8455-FF13E4E64228}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/N CT PS Inhibit Fault</Property>
	<Property Name="varPersistentID:{B6D1427A-0B2E-496A-9D40-4E4E621B92E3}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Discharge_state</Property>
	<Property Name="varPersistentID:{B6EDA17A-E6CA-4B60-95CF-C8CF984EB368}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/S_CT_Voltage</Property>
	<Property Name="varPersistentID:{B72F0E92-4F81-49E3-8B67-E9B4FA2E1D77}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS3 Shotlength</Property>
	<Property Name="varPersistentID:{B7DBB35C-7734-409B-AB33-A25B04F0C9F6}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS1 Armed</Property>
	<Property Name="varPersistentID:{B7F95795-0A80-49E7-BBB5-3DA806804CDC}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI30</Property>
	<Property Name="varPersistentID:{B821E6AD-1CBB-41DA-8082-3ADD925AA117}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI7</Property>
	<Property Name="varPersistentID:{B823A0E6-B2DE-48E1-99F3-94CC91CB24B7}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO8</Property>
	<Property Name="varPersistentID:{B825BF48-3891-45C2-A321-E55EAFDB21FB}" Type="Ref">/My Computer/TREX Controls/CCDS Power Supply/CCDS_Control_Network.lvlib/voltage set</Property>
	<Property Name="varPersistentID:{B82EFA28-4A6F-49F7-9552-0FF5333C3C8A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath5</Property>
	<Property Name="varPersistentID:{B8C271B3-A8CC-4C6B-BF02-BA59A2A0ADE3}" Type="Ref">/My Computer/TREX Controls/CCDS Power Supply/CCDS_Control_Network.lvlib/discharge event</Property>
	<Property Name="varPersistentID:{B8C8069F-C2F1-4EA3-B7A1-FBC04A51C0AA}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO30</Property>
	<Property Name="varPersistentID:{B8E869BA-3033-4B98-9046-BD475096DC60}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO11</Property>
	<Property Name="varPersistentID:{B934117C-1800-4C75-8F60-96C75825A54E}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO27</Property>
	<Property Name="varPersistentID:{B9D5A84A-C6D7-430A-BA4C-DDE4D097A23B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS3 Error</Property>
	<Property Name="varPersistentID:{BA548C4C-4022-4502-8FD6-BFDD25F8865B}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO16</Property>
	<Property Name="varPersistentID:{BAF30FFD-C279-4F93-8C25-6072217164C2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS1 HeaterID</Property>
	<Property Name="varPersistentID:{BB0BC01B-F7BC-472C-979C-B608B2B7ED66}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_IO_Aliases.lvlib/EoC</Property>
	<Property Name="varPersistentID:{BB0ED50B-CD16-45E5-BE00-69804F6EA42E}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO6</Property>
	<Property Name="varPersistentID:{BB70D339-1D97-4EA6-A29E-D63668F553B9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS1 Control Mode</Property>
	<Property Name="varPersistentID:{BBAA382F-F8EF-4CBF-838E-F3B6071BB6EE}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Puff Voltage 1</Property>
	<Property Name="varPersistentID:{BC2A8E2C-17B4-42B7-8E4D-A6143E131408}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/PA_R</Property>
	<Property Name="varPersistentID:{BCFEDC50-4BBF-49C6-AC31-E2A9299E2701}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_A_Home</Property>
	<Property Name="varPersistentID:{BD09B819-0C89-4FE6-8C1F-A8F49CBBDBD9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS4 Delay</Property>
	<Property Name="varPersistentID:{BD691F72-AD80-416D-BD59-D8A835B1C546}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/N CT Set Voltage</Property>
	<Property Name="varPersistentID:{BDE29012-2482-4AF5-8261-E4839A779EBE}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Integration Time</Property>
	<Property Name="varPersistentID:{BE058951-A695-4BF6-B411-6448E6E75E85}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/2Dprobe_Radial_Pos</Property>
	<Property Name="varPersistentID:{BE3BE15F-3DEE-4AF9-9260-2209321097B8}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO9</Property>
	<Property Name="varPersistentID:{BEE9CA16-FA47-4057-BF19-9DDD4E3A9D3A}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha1_Network_Controls.lvlib/1_on?</Property>
	<Property Name="varPersistentID:{BF7D16CE-93B2-4BD9-BCF1-97F406D3A0F2}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC12</Property>
	<Property Name="varPersistentID:{BF8C433E-337C-4EE8-BAB8-66A7CD414CDD}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO27</Property>
	<Property Name="varPersistentID:{BFC121C0-F8B2-4B23-A867-BDAF95E5F60D}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_15</Property>
	<Property Name="varPersistentID:{C02FED09-342E-4E17-967B-226C52CF8315}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO14</Property>
	<Property Name="varPersistentID:{C05FABC3-4C0C-4236-8608-61AB18D8511F}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_Network.lvlib/Remote Set CCDS</Property>
	<Property Name="varPersistentID:{C08D75C1-A6A4-45C1-AE4C-A148E5A5D988}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_Network.lvlib/HV On CCDS</Property>
	<Property Name="varPersistentID:{C09A087F-ECB1-448E-9BF8-D76B99FF2611}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_A2_Radius</Property>
	<Property Name="varPersistentID:{C1413D56-FE9A-48C2-BF4C-90A386C5711B}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO29</Property>
	<Property Name="varPersistentID:{C178D2CA-B82E-44D7-83AA-E7064BF7BAF5}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_11</Property>
	<Property Name="varPersistentID:{C23B6740-003E-47D8-A7B7-1F770264C192}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TC2 Voltage</Property>
	<Property Name="varPersistentID:{C2C00F3A-AE5A-47F8-8828-6CD54DD32A28}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath3</Property>
	<Property Name="varPersistentID:{C2F7279B-4B9D-4713-93C3-937F869EABD0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_13</Property>
	<Property Name="varPersistentID:{C306E53B-3332-41DB-A51E-DF7AEB2D85A6}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_14</Property>
	<Property Name="varPersistentID:{C3464895-9F9F-4B3B-BEDC-6BFC0CBD3309}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_16</Property>
	<Property Name="varPersistentID:{C3CD70B4-8C6A-4382-8ADD-C0BC50728263}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_IO_Aliases.lvlib/3_Ready</Property>
	<Property Name="varPersistentID:{C4298B5A-5A0F-4560-B6D9-95D661F5AB4E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPA_Radial_Pos</Property>
	<Property Name="varPersistentID:{C4597C8C-CF9E-4418-B0A9-621DFA34EAA1}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO4</Property>
	<Property Name="varPersistentID:{C47F90FD-FE51-4B81-B039-E51C3D8FE895}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO24</Property>
	<Property Name="varPersistentID:{C50FE9E1-574C-40D2-AA74-0835899E7E9F}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO14</Property>
	<Property Name="varPersistentID:{C52588CE-0DA5-454F-883B-83EF7E4553C8}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/EnableFabry</Property>
	<Property Name="varPersistentID:{C5470DEE-6F7E-4B37-AD2A-BE61712E8A0C}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS2 Cathode ID</Property>
	<Property Name="varPersistentID:{C5E2DDE5-39DB-4028-B050-882C76F43C50}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP1</Property>
	<Property Name="varPersistentID:{C5F460E7-1C67-4D2F-B196-8E1CA742F651}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/Alpha3_enabled</Property>
	<Property Name="varPersistentID:{C5F85D96-276F-4C30-B20F-EAF7D91B0537}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC0</Property>
	<Property Name="varPersistentID:{C60890C0-D8A5-415C-9498-F8C806563415}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha3_Network_Controls.lvlib/3_factor</Property>
	<Property Name="varPersistentID:{C6592AC4-6144-4485-8028-1E7CB3F9CB12}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO14</Property>
	<Property Name="varPersistentID:{C6AFA81B-D320-4C85-AE8E-DDEB3EC5DE5C}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPB_Radial_Pos</Property>
	<Property Name="varPersistentID:{C7793C16-6E82-48DA-B3CF-FE5E701DCF96}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 11 PSID</Property>
	<Property Name="varPersistentID:{C7A94E01-DBF3-46C5-9791-181A20FC89F4}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_Network.lvlib/Alpha3_Current</Property>
	<Property Name="varPersistentID:{C7C967EA-9AA6-42C0-BA58-3C83D8B7ACD0}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO6</Property>
	<Property Name="varPersistentID:{C86768F2-FD05-4362-8839-CB1D11872557}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO5</Property>
	<Property Name="varPersistentID:{C8AA77F0-6168-4A38-9C64-0707B4CDB1E0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_ID_Motor_E</Property>
	<Property Name="varPersistentID:{C900047C-2AB4-4F6E-9278-2A2EED6715BF}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO15</Property>
	<Property Name="varPersistentID:{C93EA0C5-D695-43FB-8392-16F73287C55C}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO22</Property>
	<Property Name="varPersistentID:{C9757C9E-4447-405C-AB32-D806F19B2A40}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_G2_Radius</Property>
	<Property Name="varPersistentID:{C9BB5C02-AD85-48FE-8C22-CCFB7D14A2AE}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO23</Property>
	<Property Name="varPersistentID:{C9C35244-73F4-4099-8C28-2E880BF548E8}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO3</Property>
	<Property Name="varPersistentID:{CA232D11-4DCF-4F66-87E6-5529F64AD9FC}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO2</Property>
	<Property Name="varPersistentID:{CA269E1C-FD0F-4A96-A6B9-A9860FF385E3}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_Network.lvlib/EoC Reset CCDS</Property>
	<Property Name="varPersistentID:{CA2C8BEF-6E3E-49EE-828E-C81AD2CA4125}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO30</Property>
	<Property Name="varPersistentID:{CA9109D6-398B-4EB5-B91C-63762D3A5B69}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS6 Status</Property>
	<Property Name="varPersistentID:{CAA6E027-1780-44C0-93CB-7A171445145C}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI31</Property>
	<Property Name="varPersistentID:{CAB99399-20CE-4AA7-A5F6-ABA9F676437B}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO13</Property>
	<Property Name="varPersistentID:{CAEE03E5-5ABE-4189-B8B0-0CED00946E90}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_Network.lvlib/Alpha3_ext int</Property>
	<Property Name="varPersistentID:{CB372AEB-A343-417F-A211-000E3E664A50}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_B_Radius</Property>
	<Property Name="varPersistentID:{CB3E9105-330E-470F-97CA-8882C8BE7B95}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS3 Delay</Property>
	<Property Name="varPersistentID:{CB6C2EEE-046C-4713-9005-74D0AFB6B6D5}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Integration Time_2</Property>
	<Property Name="varPersistentID:{CB7BF23E-81B0-4ECA-B48E-6599E60E0044}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/Shot Setup Data/FP_Shot_Setup.lvlib/Filter FWHM</Property>
	<Property Name="varPersistentID:{CC168E26-7490-4C35-81D4-5428DABBD7DA}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPD_Radial_Home</Property>
	<Property Name="varPersistentID:{CC86736F-0CA6-4127-8A6C-D501126E1CE2}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO11</Property>
	<Property Name="varPersistentID:{CC9FD1A4-5CFD-4759-9D43-BFDC45E88EF6}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/Alpha1_enabled</Property>
	<Property Name="varPersistentID:{CCCA9B0D-8F3C-4BED-9BB4-B56A798F9D7A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 11</Property>
	<Property Name="varPersistentID:{CCCB6796-E60B-4690-A9D1-0B3A688E0CA9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPC_Radial_Home</Property>
	<Property Name="varPersistentID:{CD264E1A-65FB-40B6-987A-B0683A6802FD}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO21</Property>
	<Property Name="varPersistentID:{CD4C05D2-5AB5-4CDC-95F4-9CA69681A4F4}" Type="Ref">/My Computer/TREX Controls/CCDS Power Supply/CCDS_Control_Network.lvlib/waiting</Property>
	<Property Name="varPersistentID:{CD4F75E2-17A5-4E08-994D-E969E87B5A10}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Suspend Heating PS2</Property>
	<Property Name="varPersistentID:{CDAF5DDE-BB49-4D29-87DC-AEF6D196D0A4}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_IO_Aliases.lvlib/3_Off</Property>
	<Property Name="varPersistentID:{CDCA8D40-0FBB-4A0A-BC16-CB90C556DB2F}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/S_CT_State</Property>
	<Property Name="varPersistentID:{CDDB3698-F04F-422E-88CA-0EF851BE2992}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS8 Control Mode</Property>
	<Property Name="varPersistentID:{CDE65626-478B-4B33-9766-2FA5926CB939}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Suspend Heating PS1</Property>
	<Property Name="varPersistentID:{CDE814FD-74E9-4D2F-888C-F939D04D7097}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_Network.lvlib/HV Connect CCDS</Property>
	<Property Name="varPersistentID:{CE13C65B-9C00-4C46-91C0-EFA7FA4FF05E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/TREX trigger delay</Property>
	<Property Name="varPersistentID:{CE676FF8-159E-4845-945A-C4A47EBCA382}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/S_CT_Charge</Property>
	<Property Name="varPersistentID:{CE6BE2A1-9376-4B29-A5CD-13D3AA20FAFC}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/S CT PS Load Fault</Property>
	<Property Name="varPersistentID:{CE6F4BDD-18AE-4F3F-827F-DBA4732022A4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/DTACQ_variables.lvlib/DTACQ_delay</Property>
	<Property Name="varPersistentID:{CF2A300F-7808-43E8-B017-D1FD056CC5D9}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO13</Property>
	<Property Name="varPersistentID:{CF4C1C0A-6831-418D-A118-11D1ACB45A5F}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_IO_Aliases.lvlib/3_VoltageOut</Property>
	<Property Name="varPersistentID:{CFE58583-C5F3-4B0D-9603-E27A351FAE45}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO7</Property>
	<Property Name="varPersistentID:{D19B0C75-F47A-499C-B4EC-927D1AB0E5F3}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_6</Property>
	<Property Name="varPersistentID:{D2178033-5562-4893-B483-E54E1441C87B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Spectrum Delay_3</Property>
	<Property Name="varPersistentID:{D251D1E5-DBB9-4D9E-BF87-4667391CA886}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 13</Property>
	<Property Name="varPersistentID:{D28E9A2F-9892-493A-AE4E-0B3AF3248AC3}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_C1_Radius</Property>
	<Property Name="varPersistentID:{D2C9298D-3C20-427B-981D-CA81BEB8A211}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Arm Complete</Property>
	<Property Name="varPersistentID:{D2F16BDE-9288-42F1-9F1C-524FAFF408A3}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha3_Network_Controls.lvlib/3_state</Property>
	<Property Name="varPersistentID:{D2FCCC7B-3390-4910-B6C6-EBFD92308B08}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/Shot Setup Data/FP_Shot_Setup.lvlib/Filter Center</Property>
	<Property Name="varPersistentID:{D324435F-B057-46DF-9A5D-13B09CE7A877}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS1 Error</Property>
	<Property Name="varPersistentID:{D3E572F6-8DA9-4CB4-8823-B985614A5991}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO5</Property>
	<Property Name="varPersistentID:{D3EFF6C2-BD43-4BE2-B4E9-E193F97D33B7}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC13</Property>
	<Property Name="varPersistentID:{D430BC1E-C0A2-460D-AE87-91DE91A47A69}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI13</Property>
	<Property Name="varPersistentID:{D43E4A50-6D65-42EF-89F3-4163782EA905}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO6</Property>
	<Property Name="varPersistentID:{D48CAE74-9874-4A86-8AB3-C3CB72FF568E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS1 Mode</Property>
	<Property Name="varPersistentID:{D4B3C841-89EE-495E-9119-D8E99C69C6BD}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO7</Property>
	<Property Name="varPersistentID:{D545328C-6A4F-4674-B638-C6A84A3E308A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_ID_Motor_G</Property>
	<Property Name="varPersistentID:{D55AED90-7117-4458-B131-07EF327C60CD}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS3 Armed</Property>
	<Property Name="varPersistentID:{D56DFC3D-C81B-4813-81A1-C959F25A0ED4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_6</Property>
	<Property Name="varPersistentID:{D5ADAC7E-1DA0-4D90-B6C3-DFDF29779A89}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Trigger Spectrum</Property>
	<Property Name="varPersistentID:{D5E8915A-EBA6-4473-8A10-05452DE3EE28}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS7 Setpoint</Property>
	<Property Name="varPersistentID:{D5E9FA77-CD2B-45DD-B823-2E8F4157E3C7}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO5</Property>
	<Property Name="varPersistentID:{D6B2009A-C7FA-4D76-A1E4-81DB9650AAE8}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO10</Property>
	<Property Name="varPersistentID:{D70A38A6-693B-4C79-BF3E-7AEC00E64384}" Type="Ref">/My Computer/TREX Controls/Heater Cap Controls/HeaterCap_Network_Variables.lvlib/HeaterCap Trigger</Property>
	<Property Name="varPersistentID:{D7FBF983-4E93-4B8B-A71B-07AE08DD5C35}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO6</Property>
	<Property Name="varPersistentID:{D824D257-818E-481B-9952-C41E620537C3}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_B_Home</Property>
	<Property Name="varPersistentID:{D8DD7FC5-A7A4-40E9-A4BF-82987562BF77}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC12</Property>
	<Property Name="varPersistentID:{D9B9B2E4-923D-45CA-A6A5-73E963EE1D5C}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC1</Property>
	<Property Name="varPersistentID:{D9C61390-D44B-432F-A72E-54E93E78AF91}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS7 Delay</Property>
	<Property Name="varPersistentID:{D9DC0155-E891-457F-A0AF-9335B4CCCEBE}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC11</Property>
	<Property Name="varPersistentID:{DB373DAB-67DE-440E-AB6E-90090863B09B}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/Sh_theta</Property>
	<Property Name="varPersistentID:{DB5C1018-0863-4DAC-8047-67AE789432E5}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha2_Network_Controls.lvlib/2_on?</Property>
	<Property Name="varPersistentID:{DBEF8073-8747-45B6-BCCD-2B15C5899020}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP11_12</Property>
	<Property Name="varPersistentID:{DC15AA35-55C0-4AF3-91F0-C8960A413ECE}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/CCG Pressure</Property>
	<Property Name="varPersistentID:{DC184700-3DE7-4594-ADD1-083A7F78C613}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO29</Property>
	<Property Name="varPersistentID:{DC257CE1-EB53-4B89-9444-E856FD309EE9}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Puff Voltage 4</Property>
	<Property Name="varPersistentID:{DC2EC051-F59E-4F75-910F-E9F48B3BDBDB}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC14</Property>
	<Property Name="varPersistentID:{DCD627A7-B40B-4C1A-BD38-BD4382E77B0F}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/N_CT_Abort</Property>
	<Property Name="varPersistentID:{DCFE3B3A-B50B-46FF-923D-A406BF404F0A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS4 Armed</Property>
	<Property Name="varPersistentID:{DDD1269B-2808-486F-859B-9A3F00D66279}" Type="Ref">/My Computer/Motor VIs/compact_stage_controller.lvlib/probe_variables.lvlib/r_vi</Property>
	<Property Name="varPersistentID:{DEA24BEA-B9DA-4D65-ACAB-B6A87B004C33}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS10 Armed</Property>
	<Property Name="varPersistentID:{DEB74833-460F-490E-84CD-84AE4350C781}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/Te_Z</Property>
	<Property Name="varPersistentID:{DEB83FEF-BC7C-43BB-90F1-BD08D71CF64F}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO10</Property>
	<Property Name="varPersistentID:{DEC396FE-4849-4231-8D54-602D59570E0B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPF_Radial_Pos</Property>
	<Property Name="varPersistentID:{DF742114-EB51-4D33-A353-F262B9832D38}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha2_IO_Aliases.lvlib/2_SetPt</Property>
	<Property Name="varPersistentID:{DF7C54FD-36C2-41E7-9F87-935C97C2433A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Puff Voltage 3</Property>
	<Property Name="varPersistentID:{DF7CBE97-9090-4C06-8A37-ADACAAAD7B19}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS5 HeaterID</Property>
	<Property Name="varPersistentID:{E01DF3BD-0B22-48E9-8133-6E93F08006A7}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_IO_Aliases.lvlib/1_ExtInt</Property>
	<Property Name="varPersistentID:{E0B22D4C-030A-4C02-8718-DEA7EEF2ECDF}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 2</Property>
	<Property Name="varPersistentID:{E0D03355-4E3B-4780-A454-C52888F93DF3}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_4</Property>
	<Property Name="varPersistentID:{E0DFCD9F-0960-4C36-BB7E-99F7AB955592}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_F_Home</Property>
	<Property Name="varPersistentID:{E0E48224-B76C-4899-BAF2-53A12D15B572}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_Network.lvlib/Alpha3_remote stop</Property>
	<Property Name="varPersistentID:{E13480A1-A24C-493F-8015-52A5612A9BE6}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI10</Property>
	<Property Name="varPersistentID:{E16A0F41-255D-408C-8E4D-8FCF295F21A2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS4 Mode</Property>
	<Property Name="varPersistentID:{E1C02D4E-1926-448C-B6A7-01BC0FBE0927}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_H3_Radius</Property>
	<Property Name="varPersistentID:{E217B810-52E3-4F11-9C32-2A48764505A0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 15</Property>
	<Property Name="varPersistentID:{E2728529-056A-4440-843E-B8BA64695F3A}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_9</Property>
	<Property Name="varPersistentID:{E2B7C665-8B75-49F5-B7A0-2F02731F3E9C}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/S CT PS Inhibit Fault</Property>
	<Property Name="varPersistentID:{E2C3598A-394D-4252-B65A-BDBE913B8BD5}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Wave Launch Duration</Property>
	<Property Name="varPersistentID:{E2EAB2E8-3B9C-4BA7-9559-0C38848B3705}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_13</Property>
	<Property Name="varPersistentID:{E30741B1-F05C-47CD-8015-4342DB243193}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/N CT Inhibit PS</Property>
	<Property Name="varPersistentID:{E353690B-E253-4061-B294-37EA1ED55A5D}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/N_CT_Voltage_Set</Property>
	<Property Name="varPersistentID:{E3BB3D0B-B61E-43A8-BF95-CC88CFB5B141}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS7 Status</Property>
	<Property Name="varPersistentID:{E493365C-380F-42F3-A3C3-0EB847B5C285}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_pow_12</Property>
	<Property Name="varPersistentID:{E4C8B37E-2518-4C0F-8437-A6CC8F9CA790}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO11</Property>
	<Property Name="varPersistentID:{E53239E9-FD2D-4FD6-BC01-E8F8C3B02515}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Arm Spectrum</Property>
	<Property Name="varPersistentID:{E554A903-6143-4E4F-9971-F1F7B689C122}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO4</Property>
	<Property Name="varPersistentID:{E565FAB5-D6D3-4E56-AAFF-B63E9B1DA15E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS2 Error</Property>
	<Property Name="varPersistentID:{E7440262-3019-4DAB-8C13-6E3856BBEF70}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CT Controls/Variables/CT Aliases.lvlib/S CT Relay Status</Property>
	<Property Name="varPersistentID:{E78FB491-A0AA-411D-8E2E-418150A4ABCA}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO7</Property>
	<Property Name="varPersistentID:{E80DE736-1D7C-4698-9BA9-92413B5EB4E4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath1</Property>
	<Property Name="varPersistentID:{E850BA8C-3DF6-4101-9900-78A4CF176D16}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO28</Property>
	<Property Name="varPersistentID:{E86C56CA-005A-4E19-9C52-EB02ECAB876C}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Time_log_variables.lvlib/Heater 8 CathID</Property>
	<Property Name="varPersistentID:{E87180F1-1410-4BC3-9832-5870498A1EA3}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/N_CT_Armed</Property>
	<Property Name="varPersistentID:{E8C3463C-474A-4654-A861-A0E50E77E8DB}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_IO_Aliases.lvlib/1_On</Property>
	<Property Name="varPersistentID:{E97F67AE-1C1E-4B17-99EF-59F9E9A3818E}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO26</Property>
	<Property Name="varPersistentID:{EA374066-8A68-4349-BE37-2FEFA0043BA2}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/DIOSwitch/DO25</Property>
	<Property Name="varPersistentID:{EACACDA7-27F7-4B4E-830F-83323B7BCE27}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP15_16</Property>
	<Property Name="varPersistentID:{EBA3DC6B-CF0C-4916-976A-A9F56BC538C4}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_IO_Aliases.lvlib/1_Off</Property>
	<Property Name="varPersistentID:{EC647A31-DBAA-4AB1-A7E3-6005D7A92EF1}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/TurnOff Heater 14</Property>
	<Property Name="varPersistentID:{EC6B80C8-751C-424A-A1B0-B8988FC33FF6}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO7</Property>
	<Property Name="varPersistentID:{ECC5C41D-5DD4-4B52-B15F-0D3C9FE624B3}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/DTACQ_variables.lvlib/Shot Duration</Property>
	<Property Name="varPersistentID:{ED199741-A63A-4E11-AC61-63258E27EEA5}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_IO_Aliases.lvlib/Dump</Property>
	<Property Name="varPersistentID:{ED3A4D47-AABB-4AC0-B10A-AC130656C9EF}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_RTFIFO.lvlib/bool inputs</Property>
	<Property Name="varPersistentID:{ED61F774-D90F-4A6A-9B23-21826BA69DCD}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS1 Cathode ID</Property>
	<Property Name="varPersistentID:{ED7C142C-B7E5-4736-A057-1D0DB1CCCD72}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS2 HeaterID</Property>
	<Property Name="varPersistentID:{ED7E9689-6342-4E9A-8637-065F3F333FAB}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC7</Property>
	<Property Name="varPersistentID:{EDF6437F-53A8-4C3D-BBC7-F1C2FB48FF52}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/S_CT_Fault</Property>
	<Property Name="varPersistentID:{EE054479-61C6-4A5F-BF93-B582FE9A919C}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod3/DIO22</Property>
	<Property Name="varPersistentID:{EE408C1D-9025-4142-8328-38A456E171E5}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_D2_Radius</Property>
	<Property Name="varPersistentID:{EEA27106-9710-4996-9903-8B0C0A0E436B}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Cathode 2 PSID</Property>
	<Property Name="varPersistentID:{EEDEC715-DAD2-4D56-AC44-1E485E7C035F}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_Network.lvlib/Alpha3_voltage</Property>
	<Property Name="varPersistentID:{EF76C69C-1721-41BC-AD41-EA6D022AFDA3}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_RTFIFO.lvlib/3_bool in</Property>
	<Property Name="varPersistentID:{EF78990A-63B6-4240-8821-F025708E1724}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_1</Property>
	<Property Name="varPersistentID:{EFB2703D-C1DB-4151-9EEB-92D260C785A2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Motor_A3_Radius</Property>
	<Property Name="varPersistentID:{F0194A70-C09B-40A9-8C09-1F5D22C65992}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/MPB_Radial_Home</Property>
	<Property Name="varPersistentID:{F02F021E-622F-4B5F-B97C-08A4C7F859D0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/Cathode_Motor_C_Radius</Property>
	<Property Name="varPersistentID:{F031BB8E-67AD-403C-A43E-517AD8D1B00B}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha2_Network_Controls.lvlib/2_off</Property>
	<Property Name="varPersistentID:{F069D870-2EDE-4700-AE7D-5AA4BBADDFE1}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_5</Property>
	<Property Name="varPersistentID:{F0861439-7896-4419-A719-EF4D4EC74651}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI14</Property>
	<Property Name="varPersistentID:{F0916164-6591-43C1-B874-904F429D475E}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod4/AO9</Property>
	<Property Name="varPersistentID:{F0946291-BD45-46C0-AE6E-C6AF1AA2D93D}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC0</Property>
	<Property Name="varPersistentID:{F0C0E3CD-32C0-426E-B681-8211B9D7B1AD}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/PS6 HeaterID</Property>
	<Property Name="varPersistentID:{F1C3E0F2-5E06-4E48-8FD3-ABA31FD7EB09}" Type="Ref">/My Computer/TREX Controls/Alpha Power Supplies/Alpha1_Network_Controls.lvlib/1_off</Property>
	<Property Name="varPersistentID:{F226ED32-8BCB-4ED8-A0DC-AF3D139D10C2}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Fabry libraries/FabryVariables.lvlib/Shot Setup Data/FP_Shot_Setup.lvlib/FP Camera</Property>
	<Property Name="varPersistentID:{F22DC302-A3FD-4960-A58D-2C892F2ECB6E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Magnetron Variables.lvlib/Magnetron2_SetPoint</Property>
	<Property Name="varPersistentID:{F289AB2B-0F6A-4DB0-950C-E7977F4B1D0D}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AI1/AI15</Property>
	<Property Name="varPersistentID:{F2BDDD1B-0F86-49EA-BE4B-9E34EB405B19}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS4 Control Mode</Property>
	<Property Name="varPersistentID:{F2F8A621-8883-4B5F-826C-2997A8004404}" Type="Ref">/My Computer/CT Injector Control/CT_global_variables.lvlib/N_CT_Charge</Property>
	<Property Name="varPersistentID:{F369CA24-64E9-4371-833E-4E4777104BD0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Probe_motor_variables.lvlib/motorID_MP3</Property>
	<Property Name="varPersistentID:{F38508EA-07DE-40F1-80B9-7FBE703EBB27}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_vol_16</Property>
	<Property Name="varPersistentID:{F3895EAE-6021-414D-8AD4-A6D1EFE398A1}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO14</Property>
	<Property Name="varPersistentID:{F3FEB0D9-9FB5-489D-9002-36ADBEF870E6}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS/Variables/CCDS_PS_Network.lvlib/Cap Voltage CCDS</Property>
	<Property Name="varPersistentID:{F4204BDD-9721-4E43-B7E7-31B11B0C9B16}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha3_Network.lvlib/Alpha3_setpt</Property>
	<Property Name="varPersistentID:{F4505DF3-CE66-4199-BE9C-CA593EAF61C0}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas/Variables/Alpha1_Network.lvlib/Alpha1_ready</Property>
	<Property Name="varPersistentID:{F46CB789-7FDC-4BBA-933F-52D9C8DAB7C3}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC14</Property>
	<Property Name="varPersistentID:{F592C88C-B151-426B-A3E9-2B604E8E7F77}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Spectrum_3</Property>
	<Property Name="varPersistentID:{F66E5637-218F-4CD5-A173-D7DFC15A97F0}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/DTACQ_variables.lvlib/DTACQ_duration</Property>
	<Property Name="varPersistentID:{F73C45E3-5A97-4FAC-B5C1-5E9B50C51F22}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod6/TC13</Property>
	<Property Name="varPersistentID:{F78F43BF-4A28-4C9E-9DA3-C58BCDCDA92E}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC1</Property>
	<Property Name="varPersistentID:{F7C83238-99DA-45BB-A8D6-68015D3A2B1E}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Mod6-DIO/DIO20</Property>
	<Property Name="varPersistentID:{F812BF3C-3B1F-4514-9283-5EF2CED840B4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS6 Cathode ID</Property>
	<Property Name="varPersistentID:{F83F2E21-8D96-48F9-9984-5554D510E010}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Spectra Time</Property>
	<Property Name="varPersistentID:{F8D86EE2-D6AE-4FFF-888A-B69E17969FC4}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Discharge_variables.lvlib/Gastype</Property>
	<Property Name="varPersistentID:{F8E9CC7E-33D4-4283-AFA2-20F38C051A95}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/AO/AO13</Property>
	<Property Name="varPersistentID:{F8EE885C-E0FB-4B43-9101-1C364D553332}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS8 Cathode ID</Property>
	<Property Name="varPersistentID:{F9292ED4-0B3D-4ED5-992F-14F9C53C39DB}" Type="Ref">/My Computer/Run Local.lvlib/SetPt</Property>
	<Property Name="varPersistentID:{F95DE5B3-4E3E-4F1F-A621-BFE3FF599DFA}" Type="Ref">/My Computer/TREX Controls/ShotLog TREX/TREX_shotlog_variables.lvlib/Alpha2_enabled</Property>
	<Property Name="varPersistentID:{FA19EB9F-221C-463A-BDB9-18CA1BF2DFB3}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS7 Shotlength</Property>
	<Property Name="varPersistentID:{FB9C3369-43A4-430F-A519-5721F977AA63}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Chassis/Real-Time Scan Resources/Thermocouple/TC7</Property>
	<Property Name="varPersistentID:{FBF5F07A-5E35-445A-9306-AD5C092EDA35}" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Heater Cap Bank/HeaterCap_Control_IO_Aliases.lvlib/Voltage</Property>
	<Property Name="varPersistentID:{FC27168B-C010-4CC2-A53C-63E0195E0076}" Type="Ref">/NI-cRIO9074-016E5818/Chassis/Real-Time Scan Resources/Mod5/DO16</Property>
	<Property Name="varPersistentID:{FC8137CC-3AF0-45C0-96A2-03FA3D845C1E}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Cathode_motor_variables.lvlib/motorID_cath10</Property>
	<Property Name="varPersistentID:{FCA1203F-BECE-4FAC-90D8-C2B8EABFF4DF}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS5 Error</Property>
	<Property Name="varPersistentID:{FCADAB34-CBED-4C77-AF0A-98C65E8AA578}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS6 Control Mode</Property>
	<Property Name="varPersistentID:{FCB23DE4-9193-493A-871E-81B6D8464075}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Spectrum_variables.lvlib/Number of Measurements_3</Property>
	<Property Name="varPersistentID:{FDAEEC3E-5DAF-4458-8C5C-4ABC5A821CEB}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS8 Armed</Property>
	<Property Name="varPersistentID:{FDF12475-2B2F-4982-99D1-234E9F86D7C8}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/PS_variables.lvlib/PS2 Delay</Property>
	<Property Name="varPersistentID:{FE89B917-7401-458D-BB9D-D5B67B426ABA}" Type="Ref">/My Computer/Motor VIs/compact_stage_controller.lvlib/probe_variables.lvlib/Z_mach</Property>
	<Property Name="varPersistentID:{FF22D137-8FA9-4A04-9C64-D6B6A77D7958}" Type="Ref">/NI-cRIO9074-016E5818/Global Variables/Heater_streaming_variables.lvlib/heater_cur_11</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="CT Injector Control" Type="Folder" URL="../CT Injector Control">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Custom Controls" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="acq196_control.ctl" Type="VI" URL="../Main Control VIs/acq196_control.ctl"/>
			<Item Name="acq_196_lights.ctl" Type="VI" URL="../Main Control VIs/acq_196_lights.ctl"/>
			<Item Name="acq2106_control.ctl" Type="VI" URL="../Main Control VIs/acq2106_control.ctl"/>
			<Item Name="mach_triple_sql.ctl" Type="VI" URL="../Shot Log VIs/mach_triple_sql.ctl"/>
			<Item Name="magnetron_shotlog.ctl" Type="VI" URL="../Main Control VIs/magnetron_shotlog.ctl"/>
			<Item Name="special_anode_sql.ctl" Type="VI" URL="../Shot Log VIs/special_anode_sql.ctl"/>
			<Item Name="trex_cell_control.ctl" Type="VI" URL="../Main Control VIs/trex_cell_control.ctl"/>
		</Item>
		<Item Name="Fabry-Perot" Type="Folder">
			<Item Name="Fabry_Calibration Ver. 2.vi" Type="VI" URL="../FabryPerot/Fabry_Calibration Ver. 2.vi"/>
			<Item Name="fp_calibration_db.vi" Type="VI" URL="../FabryPerot/fp_calibration_db.vi"/>
		</Item>
		<Item Name="Main Control VIs" Type="Folder">
			<Item Name="Main Control.vi" Type="VI" URL="../Main Control VIs/Main Control.vi"/>
			<Item Name="Main Control_updating_by_JO_DONOTUSE.vi" Type="VI" URL="../Main Control VIs/Main Control_updating_by_JO_DONOTUSE.vi"/>
		</Item>
		<Item Name="Motor VIs" Type="Folder">
			<Item Name="Cathode_Motor_Control.vi" Type="VI" URL="../Motor VIs/Cathode Motor VIs/Cathode_Motor_Control.vi"/>
			<Item Name="Cathode_Motor_Control_corotating.vi" Type="VI" URL="../Motor VIs/Cathode Motor VIs/Cathode_Motor_Control_corotating.vi"/>
			<Item Name="compact_stage_controller.lvlib" Type="Library" URL="../Motor VIs/compact_stage_controller/2D Scan/compact_stage_controller.lvlib"/>
			<Item Name="MP_array_Motor_Control.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_array_Motor_Control.vi"/>
		</Item>
		<Item Name="Shot Log VIs" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="shot log.vi" Type="VI" URL="../Shot Log VIs/shot log.vi"/>
		</Item>
		<Item Name="Sub VIs" Type="Folder">
			<Item Name="bolometer_logging(SubVI).vi" Type="VI" URL="../Shot Log VIs/bolometer_logging(SubVI).vi"/>
			<Item Name="build_acq196_string(SubVI).vi" Type="VI" URL="../Main Control VIs/build_acq196_string(SubVI).vi"/>
			<Item Name="build_acq2106_string(SubVI).vi" Type="VI" URL="../Main Control VIs/build_acq2106_string(SubVI).vi"/>
			<Item Name="build_trex_cell_string(SubVI).vi" Type="VI" URL="../Main Control VIs/build_trex_cell_string(SubVI).vi"/>
			<Item Name="check_trex_params.vi" Type="VI" URL="../Main Control VIs/check_trex_params.vi"/>
			<Item Name="get_acq196_states.vi" Type="VI" URL="../Main Control VIs/get_acq196_states.vi"/>
			<Item Name="get_acq2106_states.vi" Type="VI" URL="../Main Control VIs/get_acq2106_states.vi"/>
			<Item Name="Load Params.vi" Type="VI" URL="../Save Run Parameters/Load Params.vi"/>
			<Item Name="mach_triple_logging.vi" Type="VI" URL="../Shot Log VIs/mach_triple_logging.vi"/>
			<Item Name="magnetron_logging(SubVI).vi" Type="VI" URL="../Shot Log VIs/magnetron_logging(SubVI).vi"/>
			<Item Name="putty_comm_acq2106_state.vi" Type="VI" URL="../Main Control VIs/putty_comm_acq2106_state.vi"/>
			<Item Name="Putty_communication_acq2106(SubVI).vi" Type="VI" URL="../Main Control VIs/Putty_communication_acq2106(SubVI).vi"/>
			<Item Name="Putty_communication_dtacqs(SubVI).vi" Type="VI" URL="../Main Control VIs/Putty_communication_dtacqs(SubVI).vi"/>
			<Item Name="Run acq2106.vi" Type="VI" URL="../Main Control VIs/Run acq2106.vi"/>
			<Item Name="Run Digitizers.vi" Type="VI" URL="../Main Control VIs/Run Digitizers.vi"/>
			<Item Name="Run_Process_Scripts(SubVI).vi" Type="VI" URL="../Main Control VIs/Run_Process_Scripts(SubVI).vi"/>
			<Item Name="Save Params.vi" Type="VI" URL="../Save Run Parameters/Save Params.vi"/>
			<Item Name="special_anode_logging.vi" Type="VI" URL="../Shot Log VIs/special_anode_logging.vi"/>
		</Item>
		<Item Name="Time Log VIs" Type="Folder">
			<Item Name="time log.vi" Type="VI" URL="../Time Log VIs/time log.vi"/>
		</Item>
		<Item Name="TREX Controls" Type="Folder" URL="../TREX Controls">
			<Property Name="NI.DISK" Type="Bool">true</Property>
			<Property Name="NI.SortType" Type="Int">0</Property>
		</Item>
		<Item Name="WiPAL Classes" Type="Folder">
			<Item Name="Logbook Item.lvclass" Type="LVClass" URL="../WiPAL Classes/Logbook Item.lvclass"/>
		</Item>
		<Item Name="X Controls" Type="Folder">
			<Item Name="testingXcontrols.vi" Type="VI" URL="../X control test/testingXcontrols.vi"/>
			<Item Name="XControl 2.xctl" Type="XControl" URL="../X control test/XControl 2.xctl"/>
		</Item>
		<Item Name="2d_mach_te_probe_logging(SubVI).vi" Type="VI" URL="../Shot Log VIs/2d_mach_te_probe_logging(SubVI).vi"/>
		<Item Name="acq2106_state.ctl" Type="VI" URL="../Main Control VIs/acq2106_state.ctl"/>
		<Item Name="anode_cluster.ctl" Type="VI" URL="../Main Control VIs/anode_cluster.ctl"/>
		<Item Name="anode_control.ctl" Type="VI" URL="../Main Control VIs/anode_control.ctl"/>
		<Item Name="digitizer_id.ctl" Type="VI" URL="../Main Control VIs/digitizer_id.ctl"/>
		<Item Name="ExpType.ctl" Type="VI" URL="../../../Documents/LabView/ExpType.ctl"/>
		<Item Name="FPGA Timing Front End.vi" Type="VI" URL="../Timing VI/FPGA Timing Front End.vi"/>
		<Item Name="FPGA_chan_out.ctl" Type="VI" URL="../Custom Controls/FPGA_chan_out.ctl"/>
		<Item Name="FPGA_clk_out.ctl" Type="VI" URL="../Custom Controls/FPGA_clk_out.ctl"/>
		<Item Name="FPGA_input.ctl" Type="VI" URL="../Custom Controls/FPGA_input.ctl"/>
		<Item Name="FPGABank33-64.ctl" Type="VI" URL="../Custom Controls/FPGABank33-64.ctl"/>
		<Item Name="FPGABank65-96.ctl" Type="VI" URL="../Custom Controls/FPGABank65-96.ctl"/>
		<Item Name="FPGABank97-128.ctl" Type="VI" URL="../Custom Controls/FPGABank97-128.ctl"/>
		<Item Name="FPGABankCLKs.ctl" Type="VI" URL="../Custom Controls/FPGABankCLKs.ctl"/>
		<Item Name="get_process_state.vi" Type="VI" URL="../Main Control VIs/get_process_state.vi"/>
		<Item Name="get_shot_num.vi" Type="VI" URL="../Main Control VIs/get_shot_num.vi"/>
		<Item Name="hall_probe_logging(SubVI).vi" Type="VI" URL="../Shot Log VIs/hall_probe_logging(SubVI).vi"/>
		<Item Name="HallArrayCluster.ctl" Type="VI" URL="../Main Control VIs/HallArrayCluster.ctl"/>
		<Item Name="HH Control.vi" Type="VI" URL="../TREX HH Control/HH Control.vi"/>
		<Item Name="Latitude.ctl" Type="VI" URL="../../../Documents/TREX/Latitude.ctl"/>
		<Item Name="log_anodes.vi" Type="VI" URL="../Shot Log VIs/log_anodes.vi"/>
		<Item Name="Longitude.ctl" Type="VI" URL="../../../Documents/TREX/Longitude.ctl"/>
		<Item Name="MachTripleCluster.ctl" Type="VI" URL="../Main Control VIs/MachTripleCluster.ctl"/>
		<Item Name="MotorController.ctl" Type="VI" URL="../../../Documents/TREX/MotorController.ctl"/>
		<Item Name="new_shot_log.vi" Type="VI" URL="../Shot Log VIs/new_shot_log.vi"/>
		<Item Name="NewMotorCtlTesti.vi" Type="VI" URL="../Main Control VIs/NewMotorCtlTesti.vi"/>
		<Item Name="ProbeGeometry.ctl" Type="VI" URL="../../../Documents/TREX/ProbeGeometry.ctl"/>
		<Item Name="process_lights.ctl" Type="VI" URL="../Main Control VIs/process_lights.ctl"/>
		<Item Name="ref_testing.vi" Type="VI" URL="../Main Control VIs/ref_testing.vi"/>
		<Item Name="Run Local.lvlib" Type="Library" URL="../TREX HH Control/Run Local.lvlib"/>
		<Item Name="Universal Probe Controller.vi" Type="VI" URL="../../../Documents/TREX/Universal Probe Controller.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Dynamic To Waveform Array.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/transition.llb/Dynamic To Waveform Array.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GOOP Object Repository Method.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Method.ctl"/>
				<Item Name="GOOP Object Repository Statistics.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Statistics.ctl"/>
				<Item Name="GOOP Object Repository.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_Database_API.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/database/NI_Database_API.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Read Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (I64).vi"/>
				<Item Name="Read Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (string).vi"/>
				<Item Name="Read Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read Lines From File (with error IO).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File (with error IO).vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subBuildXYGraph.vi" Type="VI" URL="/&lt;vilib&gt;/express/express controls/BuildXYGraphBlock.llb/subBuildXYGraph.vi"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="Version To Dotted String.vi" Type="VI" URL="/&lt;vilib&gt;/_xctls/Version To Dotted String.vi"/>
				<Item Name="Waveform Array To Dynamic.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/transition.llb/Waveform Array To Dynamic.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="XControlSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/_xctls/XControlSupport.lvlib"/>
			</Item>
			<Item Name="2Dprobe_Encoder_position_conversion.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/2Dprobe_Encoder_position_conversion.vi"/>
			<Item Name="Anode_logging(SubVI).vi" Type="VI" URL="../Shot Log VIs/Anode_logging(SubVI).vi"/>
			<Item Name="AnodeILEM.ctl" Type="VI" URL="../Shot Log VIs/AnodeILEM.ctl"/>
			<Item Name="calibration_cluster.ctl" Type="VI" URL="../FabryPerot/calibration_cluster.ctl"/>
			<Item Name="calibration_sql_insert.vi" Type="VI" URL="../FabryPerot/calibration_sql_insert.vi"/>
			<Item Name="Cath_array_get_init.vi" Type="VI" URL="../Motor VIs/Cathode Motor VIs/Cath_array_get_init.vi"/>
			<Item Name="Cath_array_get_motorID.vi" Type="VI" URL="../Motor VIs/Cathode Motor VIs/Cath_array_get_motorID.vi"/>
			<Item Name="Cath_array_order_Axis_IDs.vi" Type="VI" URL="../Motor VIs/Cathode Motor VIs/Cath_array_order_Axis_IDs.vi"/>
			<Item Name="Cath_array_parse_TP.vi" Type="VI" URL="../Motor VIs/Cathode Motor VIs/Cath_array_parse_TP.vi"/>
			<Item Name="Cath_array_parse_TS.vi" Type="VI" URL="../Motor VIs/Cathode Motor VIs/Cath_array_parse_TS.vi"/>
			<Item Name="Cath_array_TP_TS.vi" Type="VI" URL="../Motor VIs/Cathode Motor VIs/Cath_array_TP_TS.vi"/>
			<Item Name="Cath_ID_to_Radial_Pos.vi" Type="VI" URL="../Motor VIs/Cathode Motor VIs/Cath_ID_to_Radial_Pos.vi"/>
			<Item Name="CathID to HeaterID.vi" Type="VI" URL="../Shot Log VIs/CathID to HeaterID.vi"/>
			<Item Name="Cathode ILEM.ctl" Type="VI" URL="../Shot Log VIs/Cathode ILEM.ctl"/>
			<Item Name="Cathode Motor IDs.ctl" Type="VI" URL="../Motor VIs/Cathode Motor VIs/Cathode Motor IDs.ctl"/>
			<Item Name="Cathode_IDs.ctl" Type="VI" URL="../Motor VIs/Cathode Motor VIs/Cathode_IDs.ctl"/>
			<Item Name="Cathode_logging(SubVI).vi" Type="VI" URL="../Shot Log VIs/Cathode_logging(SubVI).vi"/>
			<Item Name="Control 1.ctl" Type="VI" URL="../Main Control VIs/Control 1.ctl"/>
			<Item Name="Control 2.ctl" Type="VI" URL="../Main Control VIs/Control 2.ctl"/>
			<Item Name="event_Message.vi" Type="VI" URL="../Motor VIs/event_Message.vi"/>
			<Item Name="EWhemi.ctl" Type="VI" URL="../../../Documents/TREX/EWhemi.ctl"/>
			<Item Name="Heater CathID to PS IDs.vi" Type="VI" URL="../Main Control VIs/Heater CathID to PS IDs.vi"/>
			<Item Name="Heater ID wNA.ctl" Type="VI" URL="../Main Control VIs/Heater ID wNA.ctl"/>
			<Item Name="HeatercathID to INT.vi" Type="VI" URL="../Time Log VIs/HeatercathID to INT.vi"/>
			<Item Name="langmuir_probe_logging(SubVI).vi" Type="VI" URL="../Shot Log VIs/langmuir_probe_logging(SubVI).vi"/>
			<Item Name="mach_probe_logging(SubVI).vi" Type="VI" URL="../Shot Log VIs/mach_probe_logging(SubVI).vi"/>
			<Item Name="Make_DE_reset_string.vi" Type="VI" URL="../Motor VIs/Make_DE_reset_string.vi"/>
			<Item Name="Motor Control 1.ctl" Type="VI" URL="../Motor VIs/Motor Control 1.ctl"/>
			<Item Name="MP_array_get_init.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_array_get_init.vi"/>
			<Item Name="MP_array_get_motorID.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_array_get_motorID.vi"/>
			<Item Name="MP_array_initialize_motors.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_array_initialize_motors.vi"/>
			<Item Name="MP_array_move_axes.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_array_move_axes.vi"/>
			<Item Name="MP_array_order_Axis_IDs.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_array_order_Axis_IDs.vi"/>
			<Item Name="MP_array_parse_TP.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_array_parse_TP.vi"/>
			<Item Name="MP_array_parse_TS.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_array_parse_TS.vi"/>
			<Item Name="MP_array_TP.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_array_TP.vi"/>
			<Item Name="MP_array_TP_TS.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_array_TP_TS.vi"/>
			<Item Name="MP_array_TS.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_array_TS.vi"/>
			<Item Name="MP_ID.ctl" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_ID.ctl"/>
			<Item Name="MP_ID_to_Radial_Pos.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_ID_to_Radial_Pos.vi"/>
			<Item Name="MP_orientation.ctl" Type="VI" URL="../Motor VIs/Probe Motor VIs/MP_orientation.ctl"/>
			<Item Name="NShemi.ctl" Type="VI" URL="../../../Documents/TREX/NShemi.ctl"/>
			<Item Name="open_galil_connection.vi" Type="VI" URL="../Motor VIs/open_galil_connection.vi"/>
			<Item Name="Output Type.ctl" Type="VI" URL="../Main Control VIs/Output Type.ctl"/>
			<Item Name="power_supply_logging(SubVI).vi" Type="VI" URL="../Shot Log VIs/power_supply_logging(SubVI).vi"/>
			<Item Name="PS_ID_enum.ctl" Type="VI" URL="../Shot Log VIs/PS_ID_enum.ctl"/>
			<Item Name="PS_mode.ctl" Type="VI" URL="../Main Control VIs/PS_mode.ctl"/>
			<Item Name="Putty_communication(SubVI).vi" Type="VI" URL="../Main Control VIs/Putty_communication(SubVI).vi"/>
			<Item Name="Putty_communication_fabry(SubVI).vi" Type="VI" URL="../Main Control VIs/Putty_communication_fabry(SubVI).vi"/>
			<Item Name="Putty_fabry_calibration.vi" Type="VI" URL="../FabryPerot/Putty_fabry_calibration.vi"/>
			<Item Name="Putty_fabry_delete_photos.vi" Type="VI" URL="../FabryPerot/Putty_fabry_delete_photos.vi"/>
			<Item Name="RGA_trend_setting.ctl" Type="VI" URL="../Main Control VIs/RGA_trend_setting.ctl"/>
			<Item Name="send_string_to_motor.vi" Type="VI" URL="../Motor VIs/send_string_to_motor.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="update_2Dprobe_encoder_positions.vi" Type="VI" URL="../Motor VIs/Probe Motor VIs/update_2Dprobe_encoder_positions.vi"/>
			<Item Name="USER 24 Channel.ctl" Type="VI" URL="../Main Control VIs/USER 24 Channel.ctl"/>
			<Item Name="USER Channel.ctl" Type="VI" URL="../Main Control VIs/USER Channel.ctl"/>
			<Item Name="VLEM.ctl" Type="VI" URL="../Shot Log VIs/VLEM.ctl"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="NI-cRIO9074-016E5818" Type="RT CompactRIO">
		<Property Name="alias.name" Type="Str">NI-cRIO9074-016E5818</Property>
		<Property Name="alias.value" Type="Str">192.168.113.20</Property>
		<Property Name="CCSymbols" Type="Str">OS,VxWorks;CPU,PowerPC;TARGET_TYPE,RT;DeviceCode,729D;</Property>
		<Property Name="crio.ControllerPID" Type="Str">729D</Property>
		<Property Name="crio.family" Type="Str">901x</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">2</Property>
		<Property Name="host.TargetOSID" Type="UInt">14</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str">1.0,0;</Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">2000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">false</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/c/ni-rt/startup</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str"></Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.viscripting.showScriptingOperationsInContextHelp" Type="Bool">false</Property>
		<Property Name="target.server.viscripting.showScriptingOperationsInEditor" Type="Bool">false</Property>
		<Property Name="target.WebServer.Config" Type="Str"># Web server configuration file.
# Generated by LabVIEW 11.0
# 9/21/2015 6:15:17 PM
#
# Global Directives
#
NI.AddLVRouteVars
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
LimitWorkers 10
LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule
Listen 8000
#
# Directives that apply to the default server
#
NI.ServerName LabVIEW
DocumentRoot "$LVSERVER_DOCROOT"
InactivityTimeout 60
SetConnector netConnector
AddHandler LVAuth
AddHandler LVRFP
AddHandler fileHandler ""
AddOutputFilter chunkFilter
DirectoryIndex index.htm
</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Property Name="TargetOSID" Type="Str">VxWorks-PPC603</Property>
		<Item Name="Discharge VIs" Type="Folder">
			<Item Name="cRIO Run Discharge.vi" Type="VI" URL="../cRIO VIs/Discharge VIs/cRIO Run Discharge.vi"/>
		</Item>
		<Item Name="Global Variables" Type="Folder">
			<Item Name="Fabry libraries" Type="Folder">
				<Item Name="FabryVariables.lvlib" Type="Library" URL="../Global Variables/FabryVariables.lvlib"/>
			</Item>
			<Item Name="Cathode_motor_variables.lvlib" Type="Library" URL="../Global Variables/Motor control variables/Cathode_motor_variables.lvlib"/>
			<Item Name="Discharge_variables.lvlib" Type="Library" URL="../Global Variables/Discharge_variables.lvlib"/>
			<Item Name="DTACQ_variables.lvlib" Type="Library" URL="../Global Variables/DTACQ_variables.lvlib"/>
			<Item Name="HallProbeVariables.lvlib" Type="Library" URL="../Main Control VIs/HallProbeVariables.lvlib"/>
			<Item Name="Heater_streaming_variables.lvlib" Type="Library" URL="../Global Variables/Heater_streaming_variables.lvlib"/>
			<Item Name="Magnetron Variables.lvlib" Type="Library" URL="../Global Variables/Magnetron Variables.lvlib"/>
			<Item Name="Misc_variables.lvlib" Type="Library" URL="../Global Variables/Misc_variables.lvlib"/>
			<Item Name="Probe_motor_variables.lvlib" Type="Library" URL="../Global Variables/Motor control variables/Probe_motor_variables.lvlib"/>
			<Item Name="PS_variables.lvlib" Type="Library" URL="../Global Variables/PS_variables.lvlib"/>
			<Item Name="Spectrum_variables.lvlib" Type="Library" URL="../Global Variables/Spectrum_variables.lvlib"/>
			<Item Name="Time_log_variables.lvlib" Type="Library" URL="../Global Variables/Time_log_variables.lvlib"/>
			<Item Name="Trigger Variables.lvlib" Type="Library" URL="../Global Variables/Trigger Variables.lvlib"/>
		</Item>
		<Item Name="Heater VIs" Type="Folder">
			<Item Name="FPGA AI Stream.vi" Type="VI" URL="../cRIO VIs/Heater VIs/FPGA AI Stream.vi"/>
		</Item>
		<Item Name="Chassis" Type="cRIO Chassis">
			<Property Name="crio.ProgrammingMode" Type="Str">fpga</Property>
			<Property Name="crio.ResourceID" Type="Str">RIO0</Property>
			<Property Name="crio.Type" Type="Str">cRIO-9074</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="FPGA Target" Type="FPGA Target">
				<Property Name="AutoRun" Type="Bool">false</Property>
				<Property Name="configString.guid" Type="Str">{099168ED-9C6D-4D76-88F1-D662B3B630F1}resource=/crio_Mod2/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{128D0CAF-E835-4887-9DD8-F14DFD66DE96}resource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{17FB1D5B-2438-4202-A5D3-0D62773E88D2}resource=/crio_Mod2/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1832806D-7730-4D5F-A9FF-556DB7EDA37C}resource=/crio_Mod2/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1A131E11-7480-4A1C-800E-3AB97B53CC20}resource=/crio_Mod2/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1C5BC80E-9723-40F0-BE35-EFDC791C8EA4}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{23D2B7DD-CDDA-49A8-B671-C66C4B626BAB}resource=/crio_Mod2/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{284CC242-3D21-4EE7-871E-744F57496C60}resource=/crio_Mod2/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{2BA30314-653D-45BF-89B5-6F0B3EE1F6E1}resource=/crio_Mod2/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{2FDA46B4-45B7-40E1-B276-88F6FE8CB507}resource=/crio_Mod2/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{319FF8A5-3174-4F3B-82B8-725858E014F6}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{3686DAE7-5D3B-45D5-A228-41B83AEDA47E}resource=/crio_Mod2/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{36AFAE48-7AAE-42D0-8A9E-E36682207EDF}resource=/crio_Mod2/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{3A849AC3-81EF-4949-B3BA-9C43443DFE19}resource=/crio_Mod2/DI0;0;ReadMethodType=bool{527FE5D9-A0CC-43BC-AE03-A96B2D66F09D}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=1,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=1,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=1,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=1,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=1,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=1,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=1,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=1,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=1,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=1,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=1,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=1,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=1,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=1,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=1,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=1,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=1,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=1,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=1,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=1,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=1,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=1,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=1,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=1,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=1,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=1,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=1,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=1,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=1,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=1,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=1,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=1,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=4.000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]{617CA2F1-0ED5-4335-B186-A5DEFCD22A59}resource=/crio_Mod2/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{66249514-528A-4348-82FC-3CCDD52C6C56}resource=/crio_Mod2/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6890C498-2A89-4034-808D-2FD04654A18A}resource=/crio_Mod2/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6B0A1C92-52DB-4CD6-ADAF-1E68B4FE9AEA}resource=/crio_Mod2/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{748BE57C-7E5D-41B3-9017-429D4CEBF63C}resource=/crio_Mod2/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{76F79025-5F79-4CCF-92DD-6405257DE7A6}resource=/crio_Mod2/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7A02CF0A-1459-4369-8E05-9983E74E70EB}resource=/crio_Mod2/DO0;0;WriteMethodType=bool{7D9B52D3-DB42-4B0D-B928-F104D2F19C0F}resource=/Scan Clock;0;ReadMethodType=bool{953F7D19-437C-4E8C-9C44-DF44F8A1131C}[crioConfig.Begin]crio.Location=Slot 8,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{97CF5C37-B1F8-4C70-8055-19B54795B425}resource=/crio_Mod2/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{9C34E5F4-4536-4883-A683-3BE80CF491B6}resource=/crio_Mod2/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{9F2E5BD1-47B2-4969-9B43-756A4CAFB28F}[crioConfig.Begin]crio.Location=Slot 5,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{A230512D-43EC-4B6B-87E1-05338E13B289}resource=/crio_Mod2/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B00BA298-69E0-46BB-8077-126CD7C6D9C0}resource=/crio_Mod2/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B339B08B-1256-4E4E-B64A-33CBB98C6C66}resource=/crio_Mod2/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B3DE80B4-404C-43F9-9EE3-625338B87470}resource=/crio_Mod2/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B3EE150E-C1DE-46A5-B0B7-BD0D0918A03C}resource=/Chassis Temperature;0;ReadMethodType=i16{B444ABFE-F731-4F5F-BFC7-4A3A4507FA18}resource=/crio_Mod2/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B46B682B-9336-474A-AD69-61AA4B26D069}[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{BA98491F-6054-4773-A34F-32EC5B16C981}resource=/crio_Mod2/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{BC2E8987-4499-48F2-8AB6-14C8ED0DF236}resource=/crio_Mod2/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{BC48ABEB-5DC6-478F-9AA9-D691C11482EC}resource=/crio_Mod2/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C4F973D7-C321-43D8-9D0D-CB48757F6FC6}[crioConfig.Begin]crio.Location=Slot 3,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{CB171875-ADCB-448B-85AA-86DA127CFA1D}resource=/crio_Mod2/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{CE42CD00-3AA9-488A-8E33-B6A7ABF66675}resource=/crio_Mod2/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{CFE57B79-956F-4D20-9E3E-BE90ABE52D89}resource=/crio_Mod2/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{D2AB409B-64D1-4961-A907-79A8A7E20CB4}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{D571D56E-1BEF-4769-8DD0-142900F3B303}[crioConfig.Begin]crio.Location=Slot 4,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{D71B068E-D66A-4625-9F84-EA060C02BDCD}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;DataU32;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{DCA1D790-FA50-427B-876C-6546BA9D74BD}resource=/crio_Mod2/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E17E2BC5-BF77-4F4B-AA61-022F00E64308}resource=/crio_Mod2/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E9F74B2C-D23F-4DB5-A536-688D43CACF7E}resource=/crio_Mod2/Trig;0;ReadMethodType=bool{EFC85E0D-1B0B-47FF-AFB4-53DC8A76BE7A}[crioConfig.Begin]crio.Location=Slot 1,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{F2068688-0B67-4E71-B87E-99BE277002A3}[crioConfig.Begin]crio.Location=Slot 7,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{F879435F-6ED9-4667-8868-823F35F85D56}resource=/crio_Mod2/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlcRIO-9074/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9074FPGA_TARGET_FAMILYSPARTAN3TARGET_TYPEFPGA</Property>
				<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EChassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9074/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9074FPGA_TARGET_FAMILYSPARTAN3TARGET_TYPEFPGADataU32"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;DataU32;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FPGA LEDresource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolMod1[crioConfig.Begin]crio.Location=Slot 1,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod2/AI0resource=/crio_Mod2/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI10resource=/crio_Mod2/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI11resource=/crio_Mod2/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI12resource=/crio_Mod2/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI13resource=/crio_Mod2/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI14resource=/crio_Mod2/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI15resource=/crio_Mod2/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI16resource=/crio_Mod2/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI17resource=/crio_Mod2/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI18resource=/crio_Mod2/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI19resource=/crio_Mod2/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI1resource=/crio_Mod2/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI20resource=/crio_Mod2/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI21resource=/crio_Mod2/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI22resource=/crio_Mod2/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI23resource=/crio_Mod2/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI24resource=/crio_Mod2/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI25resource=/crio_Mod2/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI26resource=/crio_Mod2/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI27resource=/crio_Mod2/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI28resource=/crio_Mod2/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI29resource=/crio_Mod2/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI2resource=/crio_Mod2/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI30resource=/crio_Mod2/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI31resource=/crio_Mod2/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI3resource=/crio_Mod2/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI4resource=/crio_Mod2/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI5resource=/crio_Mod2/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI6resource=/crio_Mod2/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI7resource=/crio_Mod2/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI8resource=/crio_Mod2/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI9resource=/crio_Mod2/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/DI0resource=/crio_Mod2/DI0;0;ReadMethodType=boolMod2/DO0resource=/crio_Mod2/DO0;0;WriteMethodType=boolMod2/Trigresource=/crio_Mod2/Trig;0;ReadMethodType=boolMod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=1,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=1,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=1,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=1,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=1,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=1,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=1,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=1,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=1,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=1,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=1,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=1,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=1,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=1,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=1,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=1,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=1,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=1,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=1,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=1,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=1,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=1,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=1,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=1,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=1,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=1,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=1,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=1,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=1,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=1,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=1,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=1,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=4.000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]Mod3[crioConfig.Begin]crio.Location=Slot 3,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod4[crioConfig.Begin]crio.Location=Slot 4,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod5[crioConfig.Begin]crio.Location=Slot 5,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod6[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod7[crioConfig.Begin]crio.Location=Slot 7,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod8[crioConfig.Begin]crio.Location=Slot 8,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool</Property>
				<Property Name="Mode" Type="Int">0</Property>
				<Property Name="NI.LV.FPGA.CLIPDeclarationsArraySize" Type="Int">0</Property>
				<Property Name="NI.LV.FPGA.CLIPDeclarationSet" Type="Xml">
<CLIPDeclarationSet>
</CLIPDeclarationSet></Property>
				<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">cRIO-9074/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9074FPGA_TARGET_FAMILYSPARTAN3TARGET_TYPEFPGA</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Property Name="Resource Name" Type="Str">RIO0</Property>
				<Property Name="SWEmulationSubMode" Type="UInt">0</Property>
				<Property Name="SWEmulationVIPath" Type="Path"></Property>
				<Property Name="Target Class" Type="Str">cRIO-9074</Property>
				<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
				<Item Name="Chassis I/O" Type="Folder">
					<Item Name="Chassis Temperature" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Chassis Temperature</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B3EE150E-C1DE-46A5-B0B7-BD0D0918A03C}</Property>
					</Item>
					<Item Name="FPGA LED" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/FPGA LED</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{128D0CAF-E835-4887-9DD8-F14DFD66DE96}</Property>
					</Item>
					<Item Name="Scan Clock" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Scan Clock</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7D9B52D3-DB42-4B0D-B928-F104D2F19C0F}</Property>
					</Item>
					<Item Name="Sleep" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Sleep</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{319FF8A5-3174-4F3B-82B8-725858E014F6}</Property>
					</Item>
					<Item Name="System Reset" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/System Reset</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D2AB409B-64D1-4961-A907-79A8A7E20CB4}</Property>
					</Item>
				</Item>
				<Item Name="Mod2" Type="Folder">
					<Item Name="Mod2/AI0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{66249514-528A-4348-82FC-3CCDD52C6C56}</Property>
					</Item>
					<Item Name="Mod2/AI1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B444ABFE-F731-4F5F-BFC7-4A3A4507FA18}</Property>
					</Item>
					<Item Name="Mod2/AI2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9C34E5F4-4536-4883-A683-3BE80CF491B6}</Property>
					</Item>
					<Item Name="Mod2/AI3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E17E2BC5-BF77-4F4B-AA61-022F00E64308}</Property>
					</Item>
					<Item Name="Mod2/AI4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1A131E11-7480-4A1C-800E-3AB97B53CC20}</Property>
					</Item>
					<Item Name="Mod2/AI5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{099168ED-9C6D-4D76-88F1-D662B3B630F1}</Property>
					</Item>
					<Item Name="Mod2/AI6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1832806D-7730-4D5F-A9FF-556DB7EDA37C}</Property>
					</Item>
					<Item Name="Mod2/AI7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{CB171875-ADCB-448B-85AA-86DA127CFA1D}</Property>
					</Item>
					<Item Name="Mod2/AI8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2FDA46B4-45B7-40E1-B276-88F6FE8CB507}</Property>
					</Item>
					<Item Name="Mod2/AI9" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6890C498-2A89-4034-808D-2FD04654A18A}</Property>
					</Item>
					<Item Name="Mod2/AI10" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{17FB1D5B-2438-4202-A5D3-0D62773E88D2}</Property>
					</Item>
					<Item Name="Mod2/AI11" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B3DE80B4-404C-43F9-9EE3-625338B87470}</Property>
					</Item>
					<Item Name="Mod2/AI12" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BA98491F-6054-4773-A34F-32EC5B16C981}</Property>
					</Item>
					<Item Name="Mod2/AI13" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{748BE57C-7E5D-41B3-9017-429D4CEBF63C}</Property>
					</Item>
					<Item Name="Mod2/AI14" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6B0A1C92-52DB-4CD6-ADAF-1E68B4FE9AEA}</Property>
					</Item>
					<Item Name="Mod2/AI15" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{CE42CD00-3AA9-488A-8E33-B6A7ABF66675}</Property>
					</Item>
					<Item Name="Mod2/AI16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{617CA2F1-0ED5-4335-B186-A5DEFCD22A59}</Property>
					</Item>
					<Item Name="Mod2/AI17" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2BA30314-653D-45BF-89B5-6F0B3EE1F6E1}</Property>
					</Item>
					<Item Name="Mod2/AI18" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{36AFAE48-7AAE-42D0-8A9E-E36682207EDF}</Property>
					</Item>
					<Item Name="Mod2/AI19" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BC48ABEB-5DC6-478F-9AA9-D691C11482EC}</Property>
					</Item>
					<Item Name="Mod2/AI20" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B339B08B-1256-4E4E-B64A-33CBB98C6C66}</Property>
					</Item>
					<Item Name="Mod2/AI21" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{CFE57B79-956F-4D20-9E3E-BE90ABE52D89}</Property>
					</Item>
					<Item Name="Mod2/AI22" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{76F79025-5F79-4CCF-92DD-6405257DE7A6}</Property>
					</Item>
					<Item Name="Mod2/AI23" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F879435F-6ED9-4667-8868-823F35F85D56}</Property>
					</Item>
					<Item Name="Mod2/AI24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{23D2B7DD-CDDA-49A8-B671-C66C4B626BAB}</Property>
					</Item>
					<Item Name="Mod2/AI25" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BC2E8987-4499-48F2-8AB6-14C8ED0DF236}</Property>
					</Item>
					<Item Name="Mod2/AI26" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{97CF5C37-B1F8-4C70-8055-19B54795B425}</Property>
					</Item>
					<Item Name="Mod2/AI27" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI27</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B00BA298-69E0-46BB-8077-126CD7C6D9C0}</Property>
					</Item>
					<Item Name="Mod2/AI28" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A230512D-43EC-4B6B-87E1-05338E13B289}</Property>
					</Item>
					<Item Name="Mod2/AI29" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DCA1D790-FA50-427B-876C-6546BA9D74BD}</Property>
					</Item>
					<Item Name="Mod2/AI30" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3686DAE7-5D3B-45D5-A228-41B83AEDA47E}</Property>
					</Item>
					<Item Name="Mod2/AI31" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/AI31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{284CC242-3D21-4EE7-871E-744F57496C60}</Property>
					</Item>
					<Item Name="Mod2/DI0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3A849AC3-81EF-4949-B3BA-9C43443DFE19}</Property>
					</Item>
					<Item Name="Mod2/DO0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7A02CF0A-1459-4369-8E05-9983E74E70EB}</Property>
					</Item>
					<Item Name="Mod2/Trig" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/Trig</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E9F74B2C-D23F-4DB5-A536-688D43CACF7E}</Property>
					</Item>
				</Item>
				<Item Name="Mod2" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 2</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9205</Property>
					<Property Name="cRIOModule.AI0.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI0.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI1.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI1.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI10.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI10.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI11.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI11.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI12.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI12.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI13.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI13.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI14.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI14.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI15.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI15.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI16.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI16.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI17.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI17.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI18.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI18.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI19.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI19.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI2.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI2.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI20.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI20.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI21.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI21.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI22.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI22.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI23.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI23.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI24.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI24.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI25.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI25.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI26.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI26.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI27.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI27.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI28.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI28.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI29.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI29.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI3.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI3.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI30.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI30.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI31.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI31.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI4.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI4.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI5.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI5.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI6.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI6.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI7.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI7.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI8.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI8.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI9.TerminalMode" Type="Str">1</Property>
					<Property Name="cRIOModule.AI9.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableCalProperties" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.MinConvTime" Type="Str">4.000000E+0</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{527FE5D9-A0CC-43BC-AE03-A96B2D66F09D}</Property>
				</Item>
				<Item Name="DataU32" Type="FPGA FIFO">
					<Property Name="Actual Number of Elements" Type="UInt">1023</Property>
					<Property Name="Arbitration for Read" Type="UInt">1</Property>
					<Property Name="Arbitration for Write" Type="UInt">1</Property>
					<Property Name="Control Logic" Type="UInt">0</Property>
					<Property Name="Data Type" Type="UInt">7</Property>
					<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
					<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;DataU32;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
					<Property Name="fifo.configured" Type="Bool">true</Property>
					<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
					<Property Name="fifo.valid" Type="Bool">true</Property>
					<Property Name="fifo.version" Type="Int">12</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D71B068E-D66A-4625-9F84-EA060C02BDCD}</Property>
					<Property Name="Local" Type="Bool">false</Property>
					<Property Name="Memory Type" Type="UInt">2</Property>
					<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
					<Property Name="Requested Number of Elements" Type="UInt">1023</Property>
					<Property Name="Type" Type="UInt">2</Property>
					<Property Name="Type Descriptor" Type="Str">1000800000000001000940070003553332000100000000000000000000</Property>
				</Item>
				<Item Name="[FPGA] SAR Acq Main.vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/_exampleProjects/SAR Acquisition/FPGA Main (SAR)/[FPGA] SAR Acq Main.vi">
					<Property Name="BuildSpec" Type="Str">{EFEB3738-2375-4FC5-BCBF-F06C7A416927}</Property>
					<Property Name="configString.guid" Type="Str">{099168ED-9C6D-4D76-88F1-D662B3B630F1}resource=/crio_Mod2/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{128D0CAF-E835-4887-9DD8-F14DFD66DE96}resource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{17FB1D5B-2438-4202-A5D3-0D62773E88D2}resource=/crio_Mod2/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1832806D-7730-4D5F-A9FF-556DB7EDA37C}resource=/crio_Mod2/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1A131E11-7480-4A1C-800E-3AB97B53CC20}resource=/crio_Mod2/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1C5BC80E-9723-40F0-BE35-EFDC791C8EA4}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{23D2B7DD-CDDA-49A8-B671-C66C4B626BAB}resource=/crio_Mod2/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{284CC242-3D21-4EE7-871E-744F57496C60}resource=/crio_Mod2/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{2BA30314-653D-45BF-89B5-6F0B3EE1F6E1}resource=/crio_Mod2/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{2FDA46B4-45B7-40E1-B276-88F6FE8CB507}resource=/crio_Mod2/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{319FF8A5-3174-4F3B-82B8-725858E014F6}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{3686DAE7-5D3B-45D5-A228-41B83AEDA47E}resource=/crio_Mod2/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{36AFAE48-7AAE-42D0-8A9E-E36682207EDF}resource=/crio_Mod2/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{3A849AC3-81EF-4949-B3BA-9C43443DFE19}resource=/crio_Mod2/DI0;0;ReadMethodType=bool{527FE5D9-A0CC-43BC-AE03-A96B2D66F09D}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=1,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=1,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=1,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=1,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=1,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=1,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=1,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=1,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=1,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=1,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=1,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=1,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=1,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=1,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=1,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=1,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=1,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=1,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=1,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=1,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=1,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=1,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=1,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=1,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=1,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=1,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=1,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=1,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=1,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=1,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=1,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=1,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=4.000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]{617CA2F1-0ED5-4335-B186-A5DEFCD22A59}resource=/crio_Mod2/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{66249514-528A-4348-82FC-3CCDD52C6C56}resource=/crio_Mod2/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6890C498-2A89-4034-808D-2FD04654A18A}resource=/crio_Mod2/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6B0A1C92-52DB-4CD6-ADAF-1E68B4FE9AEA}resource=/crio_Mod2/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{748BE57C-7E5D-41B3-9017-429D4CEBF63C}resource=/crio_Mod2/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{76F79025-5F79-4CCF-92DD-6405257DE7A6}resource=/crio_Mod2/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7A02CF0A-1459-4369-8E05-9983E74E70EB}resource=/crio_Mod2/DO0;0;WriteMethodType=bool{7D9B52D3-DB42-4B0D-B928-F104D2F19C0F}resource=/Scan Clock;0;ReadMethodType=bool{953F7D19-437C-4E8C-9C44-DF44F8A1131C}[crioConfig.Begin]crio.Location=Slot 8,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{97CF5C37-B1F8-4C70-8055-19B54795B425}resource=/crio_Mod2/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{9C34E5F4-4536-4883-A683-3BE80CF491B6}resource=/crio_Mod2/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{9F2E5BD1-47B2-4969-9B43-756A4CAFB28F}[crioConfig.Begin]crio.Location=Slot 5,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{A230512D-43EC-4B6B-87E1-05338E13B289}resource=/crio_Mod2/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B00BA298-69E0-46BB-8077-126CD7C6D9C0}resource=/crio_Mod2/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B339B08B-1256-4E4E-B64A-33CBB98C6C66}resource=/crio_Mod2/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B3DE80B4-404C-43F9-9EE3-625338B87470}resource=/crio_Mod2/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B3EE150E-C1DE-46A5-B0B7-BD0D0918A03C}resource=/Chassis Temperature;0;ReadMethodType=i16{B444ABFE-F731-4F5F-BFC7-4A3A4507FA18}resource=/crio_Mod2/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B46B682B-9336-474A-AD69-61AA4B26D069}[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{BA98491F-6054-4773-A34F-32EC5B16C981}resource=/crio_Mod2/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{BC2E8987-4499-48F2-8AB6-14C8ED0DF236}resource=/crio_Mod2/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{BC48ABEB-5DC6-478F-9AA9-D691C11482EC}resource=/crio_Mod2/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C4F973D7-C321-43D8-9D0D-CB48757F6FC6}[crioConfig.Begin]crio.Location=Slot 3,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{CB171875-ADCB-448B-85AA-86DA127CFA1D}resource=/crio_Mod2/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{CE42CD00-3AA9-488A-8E33-B6A7ABF66675}resource=/crio_Mod2/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{CFE57B79-956F-4D20-9E3E-BE90ABE52D89}resource=/crio_Mod2/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{D2AB409B-64D1-4961-A907-79A8A7E20CB4}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{D571D56E-1BEF-4769-8DD0-142900F3B303}[crioConfig.Begin]crio.Location=Slot 4,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{D71B068E-D66A-4625-9F84-EA060C02BDCD}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;DataU32;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{DCA1D790-FA50-427B-876C-6546BA9D74BD}resource=/crio_Mod2/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E17E2BC5-BF77-4F4B-AA61-022F00E64308}resource=/crio_Mod2/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E9F74B2C-D23F-4DB5-A536-688D43CACF7E}resource=/crio_Mod2/Trig;0;ReadMethodType=bool{EFC85E0D-1B0B-47FF-AFB4-53DC8A76BE7A}[crioConfig.Begin]crio.Location=Slot 1,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{F2068688-0B67-4E71-B87E-99BE277002A3}[crioConfig.Begin]crio.Location=Slot 7,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{F879435F-6ED9-4667-8868-823F35F85D56}resource=/crio_Mod2/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlcRIO-9074/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9074FPGA_TARGET_FAMILYSPARTAN3TARGET_TYPEFPGA</Property>
					<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EChassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9074/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9074FPGA_TARGET_FAMILYSPARTAN3TARGET_TYPEFPGADataU32"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;DataU32;DataType=1000800000000001000940070003553332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"FPGA LEDresource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolMod1[crioConfig.Begin]crio.Location=Slot 1,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod2/AI0resource=/crio_Mod2/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI10resource=/crio_Mod2/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI11resource=/crio_Mod2/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI12resource=/crio_Mod2/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI13resource=/crio_Mod2/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI14resource=/crio_Mod2/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI15resource=/crio_Mod2/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI16resource=/crio_Mod2/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI17resource=/crio_Mod2/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI18resource=/crio_Mod2/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI19resource=/crio_Mod2/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI1resource=/crio_Mod2/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI20resource=/crio_Mod2/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI21resource=/crio_Mod2/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI22resource=/crio_Mod2/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI23resource=/crio_Mod2/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI24resource=/crio_Mod2/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI25resource=/crio_Mod2/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI26resource=/crio_Mod2/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI27resource=/crio_Mod2/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI28resource=/crio_Mod2/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI29resource=/crio_Mod2/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI2resource=/crio_Mod2/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI30resource=/crio_Mod2/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI31resource=/crio_Mod2/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI3resource=/crio_Mod2/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI4resource=/crio_Mod2/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI5resource=/crio_Mod2/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI6resource=/crio_Mod2/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI7resource=/crio_Mod2/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI8resource=/crio_Mod2/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/AI9resource=/crio_Mod2/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod2/DI0resource=/crio_Mod2/DI0;0;ReadMethodType=boolMod2/DO0resource=/crio_Mod2/DO0;0;WriteMethodType=boolMod2/Trigresource=/crio_Mod2/Trig;0;ReadMethodType=boolMod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=1,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=1,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=1,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=1,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=1,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=1,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=1,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=1,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=1,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=1,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=1,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=1,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=1,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=1,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=1,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=1,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=1,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=1,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=1,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=1,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=1,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=1,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=1,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=1,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=1,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=1,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=1,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=1,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=1,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=1,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=1,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=1,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=4.000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]Mod3[crioConfig.Begin]crio.Location=Slot 3,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod4[crioConfig.Begin]crio.Location=Slot 4,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod5[crioConfig.Begin]crio.Location=Slot 5,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod6[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod7[crioConfig.Begin]crio.Location=Slot 7,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Mod8[crioConfig.Begin]crio.Location=Slot 8,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool</Property>
					<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\Users\MPDX\mpdx_scripts\LabView\FPGA Bitfiles\MainControl_FPGATarget_[FPGA]SARAcqMain_kjCEpPEpuIk.lvbitx</Property>
				</Item>
				<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
					<Property Name="FPGA.PersistentID" Type="Str">{1C5BC80E-9723-40F0-BE35-EFDC791C8EA4}</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
					<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
				</Item>
				<Item Name="Dependencies" Type="Dependencies">
					<Item Name="vi.lib" Type="Folder">
						<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
					</Item>
				</Item>
				<Item Name="Build Specifications" Type="Build">
					<Item Name="[FPGA] SAR Acq Main" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">[FPGA] SAR Acq Main</Property>
						<Property Name="Comp.BitfileName" Type="Str">MainControl_FPGATarget_[FPGA]SARAcqMain_kjCEpPEpuIk.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">0</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.OptDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.PhysOptDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.PlaceDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.RouteDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">timing</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">high</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">high</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/Users/MPDX/mpdx_scripts/LabView/FPGA Bitfiles/MainControl_FPGATarget_[FPGA]SARAcqMain_kjCEpPEpuIk.lvbitx</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/MainControl_FPGATarget_[FPGA]SARAcqMain_kjCEpPEpuIk.lvbitx</Property>
						<Property Name="ProjectPath" Type="Path">/C/Users/MPDX/mpdx_scripts/LabView/Main Control.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">true</Property>
						<Property Name="RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
						<Property Name="TargetName" Type="Str">FPGA Target</Property>
						<Property Name="TopLevelVI" Type="Ref">/NI-cRIO9074-016E5818/Chassis/FPGA Target/[FPGA] SAR Acq Main.vi</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Real-Time Scan Resources" Type="Module Container">
				<Property Name="crio.ModuleContainerType" Type="Str">crio.RSIModuleContainer</Property>
				<Item Name="Mod1" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 1</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9870</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.kBaudRateDivider1" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRateDivider2" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRateDivider3" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRateDivider4" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler1" Type="Str">1</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler2" Type="Str">1</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler3" Type="Str">1</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler4" Type="Str">1</Property>
					<Property Name="cRIOModule.kDataBits1" Type="Str">4</Property>
					<Property Name="cRIOModule.kDataBits2" Type="Str">4</Property>
					<Property Name="cRIOModule.kDataBits3" Type="Str">4</Property>
					<Property Name="cRIOModule.kDataBits4" Type="Str">4</Property>
					<Property Name="cRIOModule.kDesiredBaudRate1" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kDesiredBaudRate2" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kDesiredBaudRate3" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kDesiredBaudRate4" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kFlowControl1" Type="Str">1</Property>
					<Property Name="cRIOModule.kFlowControl2" Type="Str">1</Property>
					<Property Name="cRIOModule.kFlowControl3" Type="Str">1</Property>
					<Property Name="cRIOModule.kFlowControl4" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity1" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity2" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity3" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity4" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits1" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits2" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits3" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits4" Type="Str">1</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{EFC85E0D-1B0B-47FF-AFB4-53DC8A76BE7A}</Property>
				</Item>
				<Item Name="Mod3" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 3</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9403</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.Initial Line Direction" Type="Str">10011000000100000000000001111000</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C4F973D7-C321-43D8-9D0D-CB48757F6FC6}</Property>
					<Item Name="DIO0" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO0</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO1" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO1</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO2" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO2</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO3" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO3</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO4" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO4</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO5" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO5</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO6" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO6</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO7" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO7</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO8" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO8</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO9" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO9</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO10" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO10</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO11" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO11</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO12" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO12</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO13" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO13</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO14" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO14</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO15" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO15</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO16" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO16</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO17" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO17</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO18" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO18</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO19" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO19</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO20" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO20</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO21" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">16</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO21</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO22" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">17</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO22</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO23" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">18</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO23</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO24" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">19</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO24</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO25" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">20</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO25</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO26" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">21</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO26</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO27" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO27</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO28" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO28</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO29" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">22</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO29</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO30" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">23</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO30</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO31" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO31</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
				</Item>
				<Item Name="Mod4" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 4</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9264</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.HotSwapMode" Type="Str">0</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D571D56E-1BEF-4769-8DD0-142900F3B303}</Property>
					<Item Name="AO0" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO0</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO1" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO1</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO2" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO2</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO3" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO3</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO4" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO4</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO5" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO5</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO6" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO6</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO7" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO7</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO8" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO8</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO9" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO9</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO10" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO10</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO11" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO11</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO12" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO12</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO13" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO13</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO14" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO14</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO15" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO15</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
				</Item>
				<Item Name="Mod5" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 5</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9477</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9F2E5BD1-47B2-4969-9B43-756A4CAFB28F}</Property>
					<Item Name="DO0" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO0</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO1" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO1</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO2" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO2</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO3" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO3</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO4" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO4</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO5" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO5</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO6" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO6</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO7" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO7</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO8" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO8</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO9" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO9</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO10" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO10</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO11" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO11</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO12" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO12</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO13" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO13</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO14" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO14</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO15" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO15</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO16" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">16</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO16</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO17" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">17</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO17</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO18" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">18</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO18</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO19" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">19</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO19</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO20" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">20</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO20</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO21" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">21</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO21</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO22" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">22</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO22</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO23" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">23</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO23</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO24" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">24</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO24</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO25" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">25</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO25</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO26" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">26</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO26</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO27" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">27</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO27</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO28" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">28</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO28</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO29" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">29</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO29</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO30" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">30</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO30</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO31" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">31</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO31</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
				</Item>
				<Item Name="Mod6" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 6</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9213</Property>
					<Property Name="cRIOModule.AI0.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI0.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI1.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI1.TCoupleType" Type="Str">1</Property>
					<Property Name="cRIOModule.AI10.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI10.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI11.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI11.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI12.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI12.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI13.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI13.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI14.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI14.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI15.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI15.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI2.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI2.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI3.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI3.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI4.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI4.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI5.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI5.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI6.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI6.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI7.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI7.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI8.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI8.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI9.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI9.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.Conversion Time" Type="Str">1</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.Enable Open TC Detection" Type="Str">true</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B46B682B-9336-474A-AD69-61AA4B26D069}</Property>
					<Item Name="TC0" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC0</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC1" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC1</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC2" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC2</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC3" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC3</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC4" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC4</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC5" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC5</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC6" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC6</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC7" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC7</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC8" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC8</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC9" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC9</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC10" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC10</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC11" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC11</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC12" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC12</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC13" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC13</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC14" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC14</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC15" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC15</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
				</Item>
				<Item Name="Mod7" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 7</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9870</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.kBaudRateDivider1" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRateDivider2" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRateDivider3" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRateDivider4" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler1" Type="Str">1</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler2" Type="Str">1</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler3" Type="Str">1</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler4" Type="Str">1</Property>
					<Property Name="cRIOModule.kDataBits1" Type="Str">4</Property>
					<Property Name="cRIOModule.kDataBits2" Type="Str">4</Property>
					<Property Name="cRIOModule.kDataBits3" Type="Str">4</Property>
					<Property Name="cRIOModule.kDataBits4" Type="Str">4</Property>
					<Property Name="cRIOModule.kDesiredBaudRate1" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kDesiredBaudRate2" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kDesiredBaudRate3" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kDesiredBaudRate4" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kFlowControl1" Type="Str">1</Property>
					<Property Name="cRIOModule.kFlowControl2" Type="Str">1</Property>
					<Property Name="cRIOModule.kFlowControl3" Type="Str">1</Property>
					<Property Name="cRIOModule.kFlowControl4" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity1" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity2" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity3" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity4" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits1" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits2" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits3" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits4" Type="Str">1</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F2068688-0B67-4E71-B87E-99BE277002A3}</Property>
				</Item>
				<Item Name="Mod8" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 8</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9870</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.kBaudRateDivider1" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRateDivider2" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRateDivider3" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRateDivider4" Type="Str">384</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler1" Type="Str">1</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler2" Type="Str">1</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler3" Type="Str">1</Property>
					<Property Name="cRIOModule.kBaudRatePrescaler4" Type="Str">1</Property>
					<Property Name="cRIOModule.kDataBits1" Type="Str">4</Property>
					<Property Name="cRIOModule.kDataBits2" Type="Str">4</Property>
					<Property Name="cRIOModule.kDataBits3" Type="Str">4</Property>
					<Property Name="cRIOModule.kDataBits4" Type="Str">4</Property>
					<Property Name="cRIOModule.kDesiredBaudRate1" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kDesiredBaudRate2" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kDesiredBaudRate3" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kDesiredBaudRate4" Type="Str">9.600000E+3</Property>
					<Property Name="cRIOModule.kFlowControl1" Type="Str">1</Property>
					<Property Name="cRIOModule.kFlowControl2" Type="Str">1</Property>
					<Property Name="cRIOModule.kFlowControl3" Type="Str">1</Property>
					<Property Name="cRIOModule.kFlowControl4" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity1" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity2" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity3" Type="Str">1</Property>
					<Property Name="cRIOModule.kParity4" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits1" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits2" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits3" Type="Str">1</Property>
					<Property Name="cRIOModule.kStopBits4" Type="Str">1</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{953F7D19-437C-4E8C-9C44-DF44F8A1131C}</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="cRIO_Fabry_Calibration.vi" Type="VI" URL="../FabryPerot/cRIO_Fabry_Calibration.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Arm PS (SubVI).vi" Type="VI" URL="../cRIO VIs/Discharge VIs/Arm PS (SubVI).vi"/>
			<Item Name="Cathode_ID.ctl" Type="VI" URL="../cRIO VIs/Heater VIs/Cathode_ID.ctl"/>
			<Item Name="decimate_and_RMS.vi" Type="VI" URL="../cRIO VIs/Heater VIs/decimate_and_RMS.vi"/>
			<Item Name="Discharge_state.ctl" Type="VI" URL="../cRIO VIs/Discharge VIs/Discharge_state.ctl"/>
			<Item Name="Get Sample Rate (50k base).vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/Sample Rate Utility/Get Sample Rate (50k base).vi"/>
			<Item Name="Get Sample Rate (51.2k base).vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/Sample Rate Utility/Get Sample Rate (51.2k base).vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Output Type.ctl" Type="VI" URL="../Main Control VIs/Output Type.ctl"/>
			<Item Name="plot_heaters.vi" Type="VI" URL="../cRIO VIs/Heater VIs/plot_heaters.vi"/>
			<Item Name="PS_mode.ctl" Type="VI" URL="../Main Control VIs/PS_mode.ctl"/>
			<Item Name="pulse_stay.ctl" Type="VI" URL="../Main Control VIs/pulse_stay.ctl"/>
			<Item Name="rwfm_AcqConfig(50k).vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/Poly VIs SubVIs/rwfm_AcqConfig(50k).vi"/>
			<Item Name="rwfm_AcqConfig(51_2k).vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/Poly VIs SubVIs/rwfm_AcqConfig(51_2k).vi"/>
			<Item Name="rwfm_AcqConfig(SAR).vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/Poly VIs SubVIs/rwfm_AcqConfig(SAR).vi"/>
			<Item Name="rwfm_AcqRead(1D_EncodedRaw).vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/Poly VIs SubVIs/rwfm_AcqRead(1D_EncodedRaw).vi"/>
			<Item Name="rwfm_AcqRead(2D_Scaled).vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/Poly VIs SubVIs/rwfm_AcqRead(2D_Scaled).vi"/>
			<Item Name="rwfm_AcqRead(Wfm).vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/Poly VIs SubVIs/rwfm_AcqRead(Wfm).vi"/>
			<Item Name="rwfm_BufferCfg.vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/rwfm_BufferCfg.vi"/>
			<Item Name="rwfm_ChannelConfig.ctl" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/RIO Wfm Cust Ctrls/rwfm_ChannelConfig.ctl"/>
			<Item Name="rwfm_ConfigTiming(poly).vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/rwfm_ConfigTiming(poly).vi"/>
			<Item Name="rwfm_CreateChan.vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/rwfm_CreateChan.vi"/>
			<Item Name="rwfm_CustomErrors.vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/Poly VIs SubVIs/rwfm_CustomErrors.vi"/>
			<Item Name="rwfm_DynamicFPGAref.ctl" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/RIO Wfm Cust Ctrls/rwfm_DynamicFPGAref.ctl"/>
			<Item Name="rwfm_Read(Poly).vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/rwfm_Read(Poly).vi"/>
			<Item Name="rwfm_SampleRate50k.ctl" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/RIO Wfm Cust Ctrls/rwfm_SampleRate50k.ctl"/>
			<Item Name="rwfm_SampleRate51_2k.ctl" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/RIO Wfm Cust Ctrls/rwfm_SampleRate51_2k.ctl"/>
			<Item Name="rwfm_SamplingInfo.ctl" Type="VI" URL="../cRIO VIs/cRIO Wfm/_subVIs/RIO Wfm Cust Ctrls/rwfm_SamplingInfo.ctl"/>
			<Item Name="rwfm_Start.vi" Type="VI" URL="../cRIO VIs/cRIO Wfm/rwfm_Start.vi"/>
			<Item Name="USER 24 Channel.ctl" Type="VI" URL="../Main Control VIs/USER 24 Channel.ctl"/>
			<Item Name="USER Channel.ctl" Type="VI" URL="../Main Control VIs/USER Channel.ctl"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="NI-cRIO9074-01948EFE-Alphas" Type="RT CompactRIO">
		<Property Name="alias.name" Type="Str">NI-cRIO9074-01948EFE-Alphas</Property>
		<Property Name="alias.value" Type="Str">192.168.113.24</Property>
		<Property Name="CCSymbols" Type="Str">OS,VxWorks;CPU,PowerPC;DeviceCode,729D;TARGET_TYPE,RT;</Property>
		<Property Name="crio.ControllerPID" Type="Str">729D</Property>
		<Property Name="crio.family" Type="Str">901x</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">2</Property>
		<Property Name="host.TargetOSID" Type="UInt">14</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str">1.0,0;</Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="UInt">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">false</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/c/ni-rt/startup</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*;+192.168.113.24;+192.168.113.50</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str"></Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.viscripting.showScriptingOperationsInContextHelp" Type="Bool">false</Property>
		<Property Name="target.server.viscripting.showScriptingOperationsInEditor" Type="Bool">false</Property>
		<Property Name="target.WebServer.Config" Type="Str"># Web server configuration file.
# Generated by LabVIEW 13.0.1f2
# 11/21/2016 9:39:31 AM

#
# Global Directives
#
NI.AddLVRouteVars
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
LimitWorkers 10
LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule
Listen 8000

#
# Directives that apply to the default server
#
NI.ServerName LabVIEW
DocumentRoot "$LVSERVER_DOCROOT"
InactivityTimeout 60
SetConnector netConnector
AddHandler LVAuth
AddHandler LVRFP
AddHandler fileHandler ""
AddOutputFilter chunkFilter
DirectoryIndex index.htm
</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="CCDS PS" Type="Folder">
			<Item Name="Variables" Type="Folder">
				<Item Name="CCDS_PS_Network.lvlib" Type="Library" URL="../cRIO VIs/CCDS Power Supply/Variable Libraries/CCDS_PS_Network.lvlib"/>
				<Item Name="CCDS_PS_IO_Aliases.lvlib" Type="Library" URL="../cRIO VIs/CCDS Power Supply/Variable Libraries/CCDS_PS_IO_Aliases.lvlib"/>
				<Item Name="CCDS_PS_RTFIFO.lvlib" Type="Library" URL="../cRIO VIs/CCDS Power Supply/Variable Libraries/CCDS_PS_RTFIFO.lvlib"/>
			</Item>
			<Item Name="VIs" Type="Folder">
				<Item Name="CCDS_Sync.vi" Type="VI" URL="../cRIO VIs/CCDS Power Supply/VIs/CCDS_Sync.vi"/>
				<Item Name="CCDS_Sync_Initialize.vi" Type="VI" URL="../cRIO VIs/CCDS Power Supply/VIs/CCDS_Sync_Initialize.vi"/>
			</Item>
		</Item>
		<Item Name="Alphas" Type="Folder">
			<Item Name="Variables" Type="Folder">
				<Item Name="Alpha1_IO_Aliases.lvlib" Type="Library" URL="../cRIO VIs/Alpha Power Supplies/Variable Libraries/Alpha1_IO_Aliases.lvlib"/>
				<Item Name="Alpha1_Network.lvlib" Type="Library" URL="../cRIO VIs/Alpha Power Supplies/Variable Libraries/Alpha1_Network.lvlib"/>
				<Item Name="Alpha1_RTFIFO.lvlib" Type="Library" URL="../cRIO VIs/Alpha Power Supplies/Variable Libraries/Alpha1_RTFIFO.lvlib"/>
				<Item Name="Alpha3_Network.lvlib" Type="Library" URL="../cRIO VIs/Alpha Power Supplies/Variable Libraries/Alpha3_Network.lvlib"/>
				<Item Name="Alpha3_RTFIFO.lvlib" Type="Library" URL="../cRIO VIs/Alpha Power Supplies/Variable Libraries/Alpha3_RTFIFO.lvlib"/>
				<Item Name="Alpha3_IO_Aliases.lvlib" Type="Library" URL="../cRIO VIs/Alpha Power Supplies/Variable Libraries/Alpha3_IO_Aliases.lvlib"/>
				<Item Name="Alpha2_IO_Aliases.lvlib" Type="Library" URL="../cRIO VIs/Alpha Power Supplies/Variable Libraries/Alpha2_IO_Aliases.lvlib"/>
			</Item>
			<Item Name="VIs" Type="Folder">
				<Item Name="Alpha1_Sync.vi" Type="VI" URL="../cRIO VIs/Alpha Power Supplies/VIs/Alpha1_Sync.vi"/>
				<Item Name="Alpha1_Initialize.vi" Type="VI" URL="../cRIO VIs/Alpha Power Supplies/VIs/Alpha1_Initialize.vi"/>
				<Item Name="Alpha3_Sync.vi" Type="VI" URL="../cRIO VIs/Alpha Power Supplies/VIs/Alpha3_Sync.vi"/>
				<Item Name="Alpha3_Initialize.vi" Type="VI" URL="../cRIO VIs/Alpha Power Supplies/VIs/Alpha3_Initialize.vi"/>
			</Item>
		</Item>
		<Item Name="Heater Cap Bank" Type="Folder">
			<Item Name="HeaterCap_Control_IO_Aliases.lvlib" Type="Library" URL="../cRIO VIs/Heater Coil Cap Bank/HeaterCap_Control_IO_Aliases.lvlib"/>
			<Item Name="test.vi" Type="VI" URL="../cRIO VIs/Heater Coil Cap Bank/test.vi"/>
		</Item>
		<Item Name="CT Controls" Type="Folder">
			<Item Name="Variables" Type="Folder">
				<Item Name="CT Aliases.lvlib" Type="Library" URL="../../../Documents/Endrizzi/CT Aliases.lvlib"/>
			</Item>
		</Item>
		<Item Name="Chassis" Type="cRIO Chassis">
			<Property Name="crio.ProgrammingMode" Type="Str">express</Property>
			<Property Name="crio.ResourceID" Type="Str">RIO0</Property>
			<Property Name="crio.Type" Type="Str">cRIO-9074</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Real-Time Scan Resources" Type="Module Container">
				<Property Name="crio.ModuleContainerType" Type="Str">crio.RSIModuleContainer</Property>
				<Item Name="AI1" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 1</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9205</Property>
					<Property Name="cRIOModule.AI0.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI0.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI1.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI1.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI10.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI10.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI11.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI11.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI12.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI12.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI13.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI13.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI14.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI14.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI15.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI15.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI16.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI16.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI17.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI17.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI18.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI18.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI19.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI19.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI2.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI2.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI20.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI20.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI21.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI21.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI22.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI22.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI23.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI23.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI24.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI24.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI25.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI25.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI26.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI26.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI27.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI27.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI28.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI28.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI29.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI29.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI3.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI3.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI30.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI30.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI31.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI31.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI4.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI4.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI5.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI5.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI6.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI6.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI7.TerminalMode" Type="Str">2</Property>
					<Property Name="cRIOModule.AI7.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI8.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI8.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.AI9.TerminalMode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI9.VoltageRange" Type="Str">0</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableCalProperties" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.MinConvTime" Type="Str">8.000000E+0</Property>
					<Item Name="AI0" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI0</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI1" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI1</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI2" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI2</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI3" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI3</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI4" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI4</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI5" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI5</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI6" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI6</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI7" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI7</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI8" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI8</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI9" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI9</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI10" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI10</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI11" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI11</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI12" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI12</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI13" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI13</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI14" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI14</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI15" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI15</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI16" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">16</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI16</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI17" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">17</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI17</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI18" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">18</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI18</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI19" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">19</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI19</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI20" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">20</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI20</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI21" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">21</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI21</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI22" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">22</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI22</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI23" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">23</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI23</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI24" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">24</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI24</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI25" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">25</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI25</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI26" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">26</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI26</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI27" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">27</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI27</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI28" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">28</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI28</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI29" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">29</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI29</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI30" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">30</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI30</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AI31" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">31</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AI31</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
				</Item>
				<Item Name="AO" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 2</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9264</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.HotSwapMode" Type="Str">0</Property>
					<Item Name="AO0" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO0</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO1" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO1</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO2" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO2</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO3" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO3</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO4" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO4</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO5" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO5</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO6" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO6</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO7" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO7</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO8" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO8</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO9" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO9</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO10" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO10</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO11" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO11</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO12" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO12</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO13" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO13</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO14" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO14</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="AO15" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">AO15</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
				</Item>
				<Item Name="DIOSwitch" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 3</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9477</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Item Name="DO0" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO0</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!</Property>
					</Item>
					<Item Name="DO1" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO1</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO2" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO2</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO3" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO3</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO4" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO4</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO5" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO5</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO6" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO6</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO7" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO7</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO8" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO8</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO9" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO9</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO10" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO10</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO11" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO11</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO12" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO12</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO13" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO13</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO14" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO14</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO15" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO15</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO16" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">16</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO16</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO17" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">17</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO17</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO18" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">18</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO18</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO19" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">19</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO19</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO20" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">20</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO20</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO21" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">21</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO21</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO22" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">22</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO22</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO23" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">23</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO23</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO24" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">24</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO24</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO25" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">25</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO25</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO26" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">26</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO26</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO27" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">27</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO27</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO28" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">28</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO28</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO29" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">29</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO29</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO30" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">30</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO30</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DO31" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">31</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DO31</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
				</Item>
				<Item Name="Mod6-DIO" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 6</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9403</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.Initial Line Direction" Type="Str">00000000000000000000110000000000</Property>
					<Item Name="DIO0" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO0</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO1" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO1</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO2" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO2</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO3" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO3</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO4" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO4</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO5" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO5</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO6" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO6</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO7" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO7</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO8" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO8</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO9" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO9</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO10" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO10</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO11" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Output</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO11</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO12" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO12</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO13" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO13</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO14" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO14</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO15" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO15</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO16" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO16</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO17" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO17</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO18" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">16</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO18</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO19" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">17</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO19</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO20" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">18</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO20</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO21" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">19</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO21</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO22" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">20</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO22</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO23" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">21</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO23</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO24" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">22</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO24</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO25" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">23</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO25</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO26" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">24</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO26</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO27" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">25</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO27</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO28" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">26</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO28</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO29" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">27</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO29</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO30" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">28</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO30</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
					<Item Name="DIO31" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">29</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">DIO31</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
					</Item>
				</Item>
				<Item Name="Thermocouple" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 8</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9213</Property>
					<Property Name="cRIOModule.AI0.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI0.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI0.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI1.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI1.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI1.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI10.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI10.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI10.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI11.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI11.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI11.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI12.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI12.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI12.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI13.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI13.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI13.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI14.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI14.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI14.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI15.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI15.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI15.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI16.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI17.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI18.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI19.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI2.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI2.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI2.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI20.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI21.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI22.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI23.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI24.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI25.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI26.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI27.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI28.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI29.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI3.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI3.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI3.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI30.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI31.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI4.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI4.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI4.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI5.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI5.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI5.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI6.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI6.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI6.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI7.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI7.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI7.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI8.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI8.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI8.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.AI9.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI9.TCoupleType" Type="Str">0</Property>
					<Property Name="cRIOModule.AI9.Terminal Mode" Type="Str">0</Property>
					<Property Name="cRIOModule.Conversion Time" Type="Str">1</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.Enable Open TC Detection" Type="Str">true</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Item Name="TC0" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC0</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC1" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC1</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC2" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC2</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC3" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC3</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC4" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC4</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC5" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC5</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC6" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC6</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC7" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC7</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC8" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC8</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC9" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC9</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC10" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC10</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC11" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC11</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC12" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC12</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC13" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC13</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC14" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC14</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
					<Item Name="TC15" Type="Variable">
						<Property Name="featurePacks" Type="Str">Industrial</Property>
						<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
						<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
						<Property Name="Industrial:EnableTimestamp" Type="Str">False</Property>
						<Property Name="Industrial:IODirection" Type="Str">Input</Property>
						<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
						<Property Name="Industrial:Mode" Type="Str">1</Property>
						<Property Name="Industrial:PhysicalName" Type="Str">TC15</Property>
						<Property Name="Industrial:PublishedReadOnly" Type="Str">False</Property>
						<Property Name="Industrial:RSI" Type="Str">False</Property>
						<Property Name="Network:UseBinding" Type="Str">False</Property>
						<Property Name="Network:UseBuffering" Type="Str">False</Property>
						<Property Name="numTypedefs" Type="UInt">0</Property>
						<Property Name="type" Type="Str">Industrial</Property>
						<Property Name="typeDesc" Type="Bin">(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
					</Item>
				</Item>
			</Item>
		</Item>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Source Distribution" Type="Source Distribution">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{C51A1A81-9934-4665-A241-950C1175A971}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Source Distribution</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/NI_AB_TARGETNAME/Source Distribution</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{959A617D-26D6-4711-8752-D164ED691BE2}</Property>
				<Property Name="Bld_targetDestDir" Type="Path">/c/ni-rt/startup/remote</Property>
				<Property Name="Bld_userLogFile" Type="Path">../builds/Main Control/NI-cRIO9074-01948EFE-Alphas/Source Distribution/c/ni-rt/startup/remote/Main Control_Source Distribution_log.txt</Property>
				<Property Name="Bld_userLogFile.pathType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">/c/ni-rt/startup/remote</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/c/ni-rt/startup/remote/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{10C8252D-803D-421B-B108-B756E53D746C}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/CCDS PS</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/NI-cRIO9074-01948EFE-Alphas/Alphas</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
			</Item>
		</Item>
	</Item>
</Project>
