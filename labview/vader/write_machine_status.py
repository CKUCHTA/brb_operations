import paramiko
import sys
import socket

ip1="128.104.166.142"
ip2="128.104.166.148"
ip3="128.104.166.150"
ip4="128.104.166.154"
# ip4 no longer in use
ips = [ip1,ip2,ip3]


if len(sys.argv)>1:
    for ip in ips:
        print ip
        try:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(ip,username='pi',password='raspberry')
        
            str_cmd = "echo {0:s} | cat > ~/machine_status/status.txt".format(sys.argv[1])
            stdin,stdout,stderr = client.exec_command(str_cmd)

            if client:
                client.close()
	except socket.error:
            print "Unable to connect to {0:s}".format(ip)




