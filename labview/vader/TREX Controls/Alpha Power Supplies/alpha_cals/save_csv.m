
load('alpha_cal_facs.mat')

out=zeros(2,5);

fldName={'dtacq', 'crio', 'crioSetpt'};
calNameJJ={'I', 'V'};
calNameKK={'cal', 'off'};

for II=1:3
    for JJ=1:2
        for KK=1:2
            
            fname=fldName{II};
            
            if II==3 && JJ==2
            else
            out(KK,(II-1)*2+JJ)=A3.(fname).([calNameJJ{JJ} calNameKK{KK}]);
            end
            
        end
        
    end
    
end

T=array2table(out,'VariableNames',{'dtacq_I','dtacq_V','crio_I','crio_V','setpt_I'});

writetable(T,'A3cals.csv')

