﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.FriendGUID" Type="Str">da505c9f-9a60-458b-91c8-d49473107faf</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"IR5F.31QU+!!.-6E.$4%*76Q!!&amp;&lt;Q!!!2V!!!!)!!!&amp;:Q!!!!:!!!!!22-&lt;W&gt;C&lt;W^L)%FU:7UO&lt;(:D&lt;'&amp;T=Q!!!!!!!+!8!)!!!$!!!#A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!AB4T:D?^U5K5&lt;XV8LP@HT!!!!!Q!!!!1!!!!!&amp;TZ_)KJQU*.NO?@N!"6$?85(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!#0:^A-!"^P1&lt;6^GBV1\5]I!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%,0EX2&lt;^?"(NHRD'@!X0X:Q!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!G!!!!)(C=9_"A9'JAO-!!R)Q/4!V=1":4"A/9:PD!%-!!!(VL#%Q!!!!!!"1!!!!-?*RD9'(A9O!!1Q9!!1I!*Q!!!%M!!!%9?*RD9-!%`Y%!3$%S-$"^!^*M;/*A'M;G*M"F,C[\I/,-1-Q#R+QQ9;#\^Q"J*J!Y6)UI2)LJ!2#@1$?((UI`1")$!,CB+6M!!!!!$!!"6EF%5Q!!!!!!!Q!!!;!!!!.U?*T&lt;Q-D!E'FM9=&lt;!R-$!$'3,-T1Q*/?HJ0)S!0E-%+!$9V!!!K$G;;'*'RYYH!9%?PTS,7"_]RO?&lt;B=6A?9;&amp;1GG5J&amp;O(R724B]6FEY7F2&gt;``P``XXS%ZX#X2]ZR2RO1WGY/I0BR&amp;R5/%!&gt;)MY$I`Y%:)&amp;7IZMFU!G7"N!33"LC"+09(!&amp;6R.&amp;1I-Z3Q'"[)/HS]Q912YF#9%[+QO:&gt;Y]ZP@=!!^*8$Q)5NXIQ;1XTM22!+&amp;?$J$/#3/OX$IC!(ZD#&gt;!"H&lt;SQ(T.!@&gt;0'-C!%B7"4B/123S--)O[W9Y\;)$$Q5%%1G6!K!I)61#C&gt;I"&gt;=)1D\D!]`.?_PL?,&amp;5CT)=7*!R!XA"B-K&amp;C0A:'"%=RE:&amp;A,67M$:$."R7"R#W)L1)/.E=%?LO=W6&amp;Y$S2Q82JA?B,JK*(=QA=VA:0D$!$-0;"^54Q05X3!R8[$9!3A\"-C?!'6(!^E@I/QE)&amp;M!SMY%MAU9)?Q]+"NM'1.OWNH@R25JG-$Z!J9VB)%Y/&lt;@!Q%$04[][T55H7!&gt;)/.7#\5MO3#Z$&amp;A1!K&amp;[5/1!!!2Q!!!'5?*RT9'"AS$3W-$.A:'"A"G*RBA;'Z0S56!9EE-,)A"/%&gt;DY.$WN_)^$NS^(J_[5\F+=T^--H@A&amp;')/:+Y^=\7=U'*!^5+;4R;VTYR(`I)E-;@^W&amp;ZA?-1.'$`+UTA3&lt;Q&gt;UU'ELW&amp;(*WF8XKL/$JL0X1\=H2\=84[K,"UMKC]_00````G(YT]5Q[5:P8[M1"N[1VHA&gt;DCRA)E`&amp;G1\*(!:E_J;:6RLS.(J_O88C=A_;'BCI/"@_K"BEI1&gt;&lt;$DF-%B`GW8GD]R'BT=!@)JP_OBV]T]WQYU(R')CY]^D/L@N;`P\1)J1AY3"S"79J!!CT%"=2+3/!AY_\OYII=H3+UQ%#@H&amp;BA9[0HJ6;?Z[!4L!!GH7A9!U&amp;JJC1!!!.!!!!%A?*RT9'"AS$3W-$M!J*E:'2D%'2I9EP.45BG1Q"='X##U]WFY70-&lt;A7Z8DE\8,^W_0*W_(T\R#T"]YB&gt;E\(&lt;^UFP!U6H[J&lt;PW1`.RDM\;$^U/(*UV+CS&gt;,#IP`PT``\`V1+F"LRM,5'/P/Z!%;82D!?JU:Q&amp;J^?$I^!7;_+%XE+-T^%/8!U?()Y@BA?9D!H(RM9&gt;B^K^^@7]8U.E-D%BO=A$C,+!)3)Q*C#W2R%(!W&gt;`&amp;&amp;&gt;V`),5#1*R=E&amp;SG6ZXGIB/M!S3=;A(XFE:2!!!!$B="A!9!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A!9!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A!9!!!9R.SYQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!&amp;"1!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!&amp;L6G$L15!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!&amp;L6EP,S]PA[U&amp;!!!!!!!!!!!!!!!!!!!!!0``!!!&amp;L6EP,S]P,S]P,Y/N"1!!!!!!!!!!!!!!!!!!``]!AVEP,S]P,S]P,S]P,S_$L1!!!!!!!!!!!!!!!!$``Q":73]P,S]P,S]P,S]P,`[$!!!!!!!!!!!!!!!!!0``!&amp;G$AVEP,S]P,S]P,`\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY.:,S]P,`\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$7;X_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY/$`P\_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AY0_`P\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$A`\_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY/$`P\_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AY0_`P\_`P\_71!!!!!!!!!!!!!!!!$``Q#$AY/$AY/$A`\_`P\_`I/$!!!!!!!!!!!!!!!!!0``!!":79/$AY/$`P\_`I/N71!!!!!!!!!!!!!!!!!!``]!!!!!79/$AY0_`I/$71!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!&amp;G$AY/$,Q!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!":,Q!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!!A!"!!!!!!!-!!&amp;'5%B1!!!!!!!$!!!&amp;=Q!!%*6YH-698UQ&lt;&gt;2T`XP8!;]P#&amp;?B9FUF&lt;&gt;KU%93.%92+9O.UE64;X!20WY$9I!&gt;R=$16D?"C9.#3%]+"MT#R:MC&gt;]^)%(%Y/*-1R.\M(NQ35SET*?V71GEB('^@T_@M@VLN@3VI'2BV^_/&lt;[@\^`0\X/`+Y$QO8#!4=!."2BB(4@N#DD#-A/Q6-P$^F^Q%I1OZDEQ:2Z'A6;_3XD-*JB$#B3&amp;Z3"@*UX"5\276^5\\&amp;8&lt;N0!%41M&amp;$TJT+&amp;!=FMN&gt;\YBR1@T[E$B6I(MN";]QQS49&lt;N(\D*_*24!AR+L)[KJF%M")@I[,6:[^=LU`*J+H^FL?1VX;&amp;2!E?&gt;_Q'(]&amp;07,I([B,WSI\:PN,&gt;QHIMAI7&amp;B9-E%M$"7E;RR$$DA'QU\&lt;6,*A338YZ+M:L+-:"-2BH7I]D?2/T*(=#ME*,*6F!+/)_X3[:9(7=UEJR;WNLC-.V'T?I1*E90]&amp;\_#@BF9*^ZW,@!!0-UC6?\66J&lt;.=:-A6K7Y+$#$1R1A@O/R3IDMHM:?$U^PY#%X1-H$[':BQ$]T9&gt;AVW;%M#9QU4W/@A+1@*,X&amp;*F78NEI$=3O?I,D@2`Z$NZ&lt;41[UD_=0JQ73&lt;9XEM**(-I,+!=/(M+AO&gt;%2G*_@R^JR.;$(%6IORJ/Y5FL)N73TQU;T363D;7^CU[3.TA(3O%!4K^/6IX1^4&gt;=:OPZ.6K&lt;0)(!^%NAF7"..)@$L?U`A"O46$1O")1T@QGA7-D:K))0!;!]A1TA,ZBBCRMU%2EQ9-;/Z#@R''I%*VE,AO&lt;GZ&amp;"T/ICF*9)ZB.!,((KB&lt;[B;B]30V&gt;`A$0K-UVKDZ(I[$_)_C`[_A/;8VT"?%N'WU^7[$N!89_V@T*'VBR]C6E&gt;'I:12&amp;#D/,8+M8YR?IZW,KW1O(I2LKM;S+&gt;^%&gt;76V?,6%G9@-N?&lt;5O/+%YZK-G@L)+F&gt;O&gt;`*#P*/\44=-K-=5VN_HOP&gt;K^2IUXM=&lt;^9NSNU`MANOYI.'+"CYO,7##OG1JU9Q^]V-20VK4`I@25.&amp;0#!D^:=ZPOXKOZQ&amp;O;VP39BEBK@!W;T:T/6/."&lt;)/0GPD*GAQBJ7?4U:3=NPQM`\V4=Y6T7I8&gt;JAL&gt;#-^5):-;Q9MGG:):4!ERK$#X]=!'?C"Q%;3.]X:.1GUII:-@E%&amp;MWZV&amp;\9T*L96QAK41BP[DU%)0[UO[4G\O_):JS@/Q/C\[0IZ%BU;')N=N"X:6A?&gt;BG3-HVG&amp;ZM62"P3Z+KIO+UO&lt;G*L9&amp;6]U`VD`"M4'20"%#?K-!R#I'CQX9;W-DA=/8"J)^BSV*^BQ8YSQ.6%@L+&amp;(8I1*&gt;75]/G^LT)CD+S0'_&gt;!*IJOE=\\-Q!"4-"D86:ML'B^H5P(AWP@FHUZO3D6/"B*:.M4&lt;D\YUB/$#&lt;8V&gt;_QWR'*_`2&lt;!J)#/*FIUY,2`&gt;(4@MDJHWNM8`WU"14XU2KBGN"$44EP"9Q9';W5W-W&amp;ZREFXW]_F3\W3\\_8&amp;^7ZG]\W[\/)@8"B,1AY`;].W8=CW9X:(O*`/E?Z%U.)"E(_M@^I5E#_'RV4&gt;47MVLYM:"-%/LO74L@D+V]5@4@NGUPZ][UFN;H%]M)[WG=4$#HM4"BMVJ&lt;VSH[@R[;*T,?:R@,MPZ.:BSW])5)J6(D%P,TB@),UV-[&lt;SP-;5A/'ETV/^U7.&lt;%4`U4E2Y)J9J@2U9W/*%.I2RM9(8R/Z^&amp;`$ITCF]RHLO702;`,GRBAU8]-*E+&amp;)D`1@QO;*R*%4^[_F]YG^W)X`O:R)]-Q@X@C6^XGPAZM@SX=F/[*Y0Y&amp;?9H@P9TJE]+`P(SK0'`$F&amp;3MRE)JT$[O!*O[74*TU))/V(%B`B4QFX]X-,PL`X]88Z^[6(SJY+F^C1S^I":M8UHXP(S"`Y"9Z32\Q!!!!!%!!!!2A!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!-P!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!#-&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!"Q!!!!"1!21!I!#F)A='^T;82J&lt;WY!!":!-0````]-2'FH;82J?G6S)%F%!!!21!I!#FIA='^T;82J&lt;WY!!!R!)1:4&gt;'&amp;U&gt;8-!!#2!5!!%!!!!!1!#!!-54'^H9G^P;S"*&gt;'6N,GRW9WRB=X-!!!%!"!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!V&amp;Q#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!%!!!!!!!!!!%!!!!#!!!!!Q!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!V&amp;??MQ!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$56Z[T!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!)Q8!)!!!!!!!1!)!$$`````!!%!!!!!!(!!!!!&amp;!"&amp;!#A!+5C"Q&lt;X.J&gt;'FP&lt;A!!&amp;E!Q`````QR%;7&gt;J&gt;'F[:8)A351!!"&amp;!#A!+7C"Q&lt;X.J&gt;'FP&lt;A!!$%!B"F.U982V=Q!!*%"1!!1!!!!"!!)!!R2-&lt;W&gt;C&lt;W^L)%FU:7UO&lt;(:D&lt;'&amp;T=Q!!!1!%!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;Q#!!!!!!!%!"1!$!!!"!!!!!!!6!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!)U8!)!!!!!!"1!21!I!#F)A='^T;82J&lt;WY!!":!-0````]-2'FH;82J?G6S)%F%!!!21!I!#FIA='^T;82J&lt;WY!!!R!)1:4&gt;'&amp;U&gt;8-!!#2!5!!%!!!!!1!#!!-54'^H9G^P;S"*&gt;'6N,GRW9WRB=X-!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!'!!Y!!!!%!!!"FQ!!!#A!!!!#!!!%!!!!!!)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+1!!!&gt;ZYH)V03UI$123M37@S'5?&gt;@)T@R13S=#7#&amp;WAQ#!%8)9+#+_0]'"RNS@3)O0)E8M+L?"!^A6:[)BL=W%7`\H[P8F=^!(NI31@/R,^8?;J4&gt;1@UZ/%HFTN-%W;?IJE`'K+E8@[CO&lt;*@/^.48?4!/4\?,FZ?!63[JSKZ6OL'(_HI^C"\#,*JHHN,S5"HW*&gt;D6-GX5)&amp;9[P'0MS,8U1SQ#4A)S8IX$\&amp;YYABV.'5&gt;)MY3./3T5".&gt;*C)6Q_0@!EVS6U12RGRUA!&lt;-6"4NIWX%6\('P3\U9UD#G%:=?,Q&amp;&amp;"!U.PC(R:*N%4::$,6ZM/:S6SR5==+0@7SA:Q;QM5HHX\!8_*PZK7Q:F&amp;.&lt;W$;'/IQV+L41ZLW,(?S;KO$:)13NU-)8")V,NQ!!!!!!!(=!!1!#!!-!"1!!!&amp;A!$Q1!!!!!$Q$9!.5!!!"B!!]%!!!!!!]!W!$6!!!!;A!0"!!!!!!0!.A!V1!!!(-!%91!A!!!%1$P!/1!!!"VA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!"-!"35V*$$1I!!UR71U.-1F:8!!!6P!!!"(5!!!!A!!!6H!!!!!!!!!!!!!!!)!!!!$1!!!2E!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!"!!!"W%2'2&amp;-!!!!!!!!#!%R*:(-!!!!!!!!#&amp;&amp;:*1U1!!!!#!!!#+(:F=H-!!!!%!!!#:&amp;.$5V)!!!!!!!!#S%&gt;$5&amp;)!!!!!!!!#X%F$4UY!!!!!!!!#]'FD&lt;$A!!!!!!!!$"%.11T)!!!!!!!!$'%R*:H!!!!!!!!!$,%:13')!!!!!!!!$1%:15U5!!!!!!!!$6&amp;:12&amp;!!!!!!!!!$;%R*9G1!!!!!!!!$@%*%3')!!!!!!!!$E%*%5U5!!!!!!!!$J&amp;:*6&amp;-!!!!!!!!$O%253&amp;!!!!!!!!!$T%V6351!!!!!!!!$Y%B*5V1!!!!!!!!$^&amp;:$6&amp;!!!!!!!!!%#%:515)!!!!!!!!%(!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!`````Q!!!!!!!!$%!!!!!!!!!!$`````!!!!!!!!!.A!!!!!!!!!!0````]!!!!!!!!!Y!!!!!!!!!!!`````Q!!!!!!!!%-!!!!!!!!!!$`````!!!!!!!!!21!!!!!!!!!!P````]!!!!!!!!"1!!!!!!!!!!!`````Q!!!!!!!!&amp;9!!!!!!!!!!$`````!!!!!!!!!;A!!!!!!!!!!0````]!!!!!!!!"O!!!!!!!!!!"`````Q!!!!!!!!.=!!!!!!!!!!,`````!!!!!!!!"(Q!!!!!!!!!"0````]!!!!!!!!&amp;5!!!!!!!!!!(`````Q!!!!!!!!6E!!!!!!!!!!D`````!!!!!!!!"81!!!!!!!!!#@````]!!!!!!!!&amp;C!!!!!!!!!!+`````Q!!!!!!!!79!!!!!!!!!!$`````!!!!!!!!";Q!!!!!!!!!!0````]!!!!!!!!&amp;R!!!!!!!!!!!`````Q!!!!!!!!89!!!!!!!!!!$`````!!!!!!!!"FQ!!!!!!!!!!0````]!!!!!!!!+9!!!!!!!!!!!`````Q!!!!!!!!JI!!!!!!!!!!$`````!!!!!!!!#HA!!!!!!!!!!0````]!!!!!!!!0]!!!!!!!!!!!`````Q!!!!!!!!`Y!!!!!!!!!!$`````!!!!!!!!%!!!!!!!!!!!!0````]!!!!!!!!1%!!!!!!!!!!!`````Q!!!!!!!""Y!!!!!!!!!!$`````!!!!!!!!%)!!!!!!!!!!!0````]!!!!!!!!4N!!!!!!!!!!!`````Q!!!!!!!"/]!!!!!!!!!!$`````!!!!!!!!%]1!!!!!!!!!!0````]!!!!!!!!4]!!!!!!!!!#!`````Q!!!!!!!"5A!!!!!""-&lt;W&gt;C&lt;W^L)%FU:7UO9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!22-&lt;W&gt;C&lt;W^L)%FU:7UO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!!!!-!!1!!!!!!!!!!!!!"!!9!5!!!!!%!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!%Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!%!!!!!"A!.1!I!"UZV&lt;76S;7-!%%!Q`````Q:4&gt;(*J&lt;G=!!!R!)1&gt;#&lt;W^M:7&amp;O!!Z!-P````]%5'&amp;U;!!!$U!+!!F/&gt;7VF=GFD)$)!7!$RV&amp;4.YQ!!!!)54'^H9G^P;S"*&gt;'6N,GRW9WRB=X-14'^H9G^P;S"*&gt;'6N,G.U&lt;!!K1&amp;!!"1!!!!%!!A!$!!154'^H9G^P;S"*&gt;'6N)%.M&gt;8.U:8)!!!%!"1!!!!8``````````````````````````Q!!!!!!!!!!!!!!!!"16%AQ!!!!"!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"-!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!!!!!!5!%5!+!!J3)("P=WFU;7^O!!!71$$`````$%2J:WFU;8JF=C"*2!!!%5!+!!J;)("P=WFU;7^O!!!-1#%'5X2B&gt;(6T!!"7!0(56Z[T!!!!!B2-&lt;W&gt;C&lt;W^L)%FU:7UO&lt;(:D&lt;'&amp;T=R"-&lt;W&gt;C&lt;W^L)%FU:7UO9X2M!#B!5!!%!!!!!1!#!!-54'^H9G^P;S"*&gt;'6N)%.M&gt;8.U:8)!!!%!"!!!!!1!!!!!!!!!!1!!!!4`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!%Q#!!!!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.2</Property>
	<Item Name="Friends List" Type="Friends List">
		<Item Name="XControl 2.xctl" Type="Friended Library" URL="../../X control test/XControl 2.xctl"/>
		<Item Name="testingXcontrols.vi" Type="Friended VI" URL="../../X control test/testingXcontrols.vi"/>
	</Item>
	<Item Name="Logbook Item.ctl" Type="Class Private Data" URL="Logbook Item.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="LogbookItemInit.vi" Type="VI" URL="../LogbookItemInit.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;\!!!!$1!Q1(!!(A!!&amp;B2-&lt;W&gt;C&lt;W^L)%FU:7UO&lt;(:D&lt;'&amp;T=Q!!$URP:W*P&lt;WMA382F&lt;3"J&lt;A!21!I!#FIA='^T;82J&lt;WY!!"&amp;!#A!+5C"Q&lt;X.J&gt;'FP&lt;A!!"!!!!$*!=!!?!!!7&amp;%RP:W*P&lt;WMA382F&lt;3ZM&gt;G.M98.T!!!14'^H9G^P;S"*&gt;'6N)'^V&gt;!!!&amp;E!Q`````QR%;7&gt;J&gt;'F[:8)A351!!!R!)1:4&gt;'&amp;U&gt;8-!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!=!#!!*%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"Q!)!!E*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!#!!-!"!!&amp;!!-!"A!$!!-!!Q!+!!-!!Q!$!!M#!!%)!!#3!!!!#A!!!!I!!!!!!!!!D1!!!1I!!!!!!!!!#A!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!$!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
	</Item>
	<Item Name="Digitizer ID" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Digitizer ID</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Digitizer ID</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Read Digitizer ID.vi" Type="VI" URL="../Read Digitizer ID.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%V!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````]-2'FH;82J?G6S)%F%!!!S1(!!(A!!&amp;B2-&lt;W&gt;C&lt;W^L)%FU:7UO&lt;(:D&lt;'&amp;T=Q!!%%RP:W*P&lt;WMA382F&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!-%"Q!"Y!!"954'^H9G^P;S"*&gt;'6N,GRW9WRB=X-!!!^-&lt;W&gt;C&lt;W^L)%FU:7UA;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Write Digitizer ID.vi" Type="VI" URL="../Write Digitizer ID.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%V!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%RP:W*P&lt;WMA382F&lt;3ZM&gt;G.M98.T!!!14'^H9G^P;S"*&gt;'6N)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$%2J:WFU;8JF=C"*2!!!-%"Q!"Y!!"954'^H9G^P;S"*&gt;'6N,GRW9WRB=X-!!!^-&lt;W&gt;C&lt;W^L)%FU:7UA;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!*)!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
	</Item>
	<Item Name="R position" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">R position</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">R position</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Read R position.vi" Type="VI" URL="../Read R position.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Q!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!#A!+5C"Q&lt;X.J&gt;'FP&lt;A!!-E"Q!"Y!!"954'^H9G^P;S"*&gt;'6N,GRW9WRB=X-!!""-&lt;W&gt;C&lt;W^L)%FU:7UA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$"!=!!?!!!7&amp;%RP:W*P&lt;WMA382F&lt;3ZM&gt;G.M98.T!!!04'^H9G^P;S"*&gt;'6N)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Write R position.vi" Type="VI" URL="../Write R position.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Q!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%RP:W*P&lt;WMA382F&lt;3ZM&gt;G.M98.T!!!14'^H9G^P;S"*&gt;'6N)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!I!#F)A='^T;82J&lt;WY!!$"!=!!?!!!7&amp;%RP:W*P&lt;WMA382F&lt;3ZM&gt;G.M98.T!!!04'^H9G^P;S"*&gt;'6N)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
	</Item>
	<Item Name="Status" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Status</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Status</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Read Status.vi" Type="VI" URL="../Read Status.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%L!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1:4&gt;'&amp;U&gt;8-!!$*!=!!?!!!7&amp;%RP:W*P&lt;WMA382F&lt;3ZM&gt;G.M98.T!!!14'^H9G^P;S"*&gt;'6N)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Q1(!!(A!!&amp;B2-&lt;W&gt;C&lt;W^L)%FU:7UO&lt;(:D&lt;'&amp;T=Q!!$URP:W*P&lt;WMA382F&lt;3"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Write Status.vi" Type="VI" URL="../Write Status.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%L!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%RP:W*P&lt;WMA382F&lt;3ZM&gt;G.M98.T!!!14'^H9G^P;S"*&gt;'6N)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!-1#%'5X2B&gt;(6T!!!Q1(!!(A!!&amp;B2-&lt;W&gt;C&lt;W^L)%FU:7UO&lt;(:D&lt;'&amp;T=Q!!$URP:W*P&lt;WMA382F&lt;3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
	</Item>
	<Item Name="Z position" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Z position</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Z position</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Read Z position.vi" Type="VI" URL="../Read Z position.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Q!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!#A!+7C"Q&lt;X.J&gt;'FP&lt;A!!-E"Q!"Y!!"954'^H9G^P;S"*&gt;'6N,GRW9WRB=X-!!""-&lt;W&gt;C&lt;W^L)%FU:7UA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$"!=!!?!!!7&amp;%RP:W*P&lt;WMA382F&lt;3ZM&gt;G.M98.T!!!04'^H9G^P;S"*&gt;'6N)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Write Z position.vi" Type="VI" URL="../Write Z position.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Q!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%RP:W*P&lt;WMA382F&lt;3ZM&gt;G.M98.T!!!14'^H9G^P;S"*&gt;'6N)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!I!#FIA='^T;82J&lt;WY!!$"!=!!?!!!7&amp;%RP:W*P&lt;WMA382F&lt;3ZM&gt;G.M98.T!!!04'^H9G^P;S"*&gt;'6N)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
	</Item>
</LVClass>
