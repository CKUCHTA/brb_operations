﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="CCG Pressure" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="Real-Time Features:ApplyNetworkConfig" Type="Str">False</Property>
		<Property Name="Real-Time Features:BufferLength" Type="Str">1</Property>
		<Property Name="Real-Time Features:UseBuffering" Type="Str">False</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 1 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 2 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 3 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 4 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 5 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 6 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 7 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 8 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 9 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 10 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 11 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Heater 12 CathID" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;G1!!!"=!A!!!!!!"!)=!&amp;A!.#5.B&gt;'BP:'5A-1F$982I&lt;W2F)$)*1W&amp;U;'^E:3!T#5.B&gt;'BP:'5A.!F$982I&lt;W2F)$5*1W&amp;U;'^E:3!W#5.B&gt;'BP:'5A.QF$982I&lt;W2F)$A*1W&amp;U;'^E:3!Z#E.B&gt;'BP:'5A-4!+1W&amp;U;'^E:3!R-1J$982I&lt;W2F)$%S!UYP11!!!!%!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="RGA Trend Setting" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!P+Q!!!"=!A!!!!!!"!"E!&amp;A!##&amp;2J&lt;75A&lt;'^H#&amp;.I&lt;X1A&lt;'^H!!!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Thermocouple array" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">2</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Time_log_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""01!!!"=!A!!!!!!#!!V!#A!(4H6N:8*J9Q!=1%!!!@````]!!!^"=H*B?3"P:C"%&lt;X6C&lt;'5!!1!"!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
