﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="2Dprobe_Encoder_CPR" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="2Dprobe_Radial_Home" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="2Dprobe_Radial_Pos" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="2Dprobe_Theta_Home" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="2Dprobe_Theta_Pos" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP1" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP2" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP3" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP4" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP5_6" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP5_6_theta" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP7_8" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP9_10" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP11_12" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP13_14" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP15_16" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP17_18" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP19_20" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP21_22" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="motorID_MP23_24" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`_Q!!!"=!A!!!!!!"!/E!&amp;A!:#%ZP)'VP&gt;'^S#%VP&gt;'^S)%%R#%VP&gt;'^S)%)R#%VP&gt;'^S)%-R#%VP&gt;'^S)%1R#%VP&gt;'^S)%5R#%VP&gt;'^S)%9R#%VP&gt;'^S)%=R#%VP&gt;'^S)%AR#%VP&gt;'^S)%%S#%VP&gt;'^S)%)S#%VP&gt;'^S)%-S#%VP&gt;'^S)%1S#%VP&gt;'^S)%5S#%VP&gt;'^S)%9S#%VP&gt;'^S)%=S#%VP&gt;'^S)%AS#%VP&gt;'^S)%%T#%VP&gt;'^S)%)T#%VP&gt;'^S)%-T#%VP&gt;'^S)%1T#%VP&gt;'^S)%5T#%VP&gt;'^S)%9T#%VP&gt;'^S)%=T#%VP&gt;'^S)%AT!!!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPA_Radial_Home" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPA_Radial_Pos" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPB_Radial_Home" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPB_Radial_Pos" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPC_Radial_Home" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPC_Radial_Pos" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPD_Radial_Home" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPD_Radial_Pos" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPE_Radial_Home" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPE_Radial_Pos" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPF_Radial_Home" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPF_Radial_Pos" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPG_Radial_Home" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPG_Radial_Pos" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPH_Radial_Home" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MPH_Radial_Pos" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/Main Control.lvproj/My Computer/Global Variables/Probe_motor_variables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
