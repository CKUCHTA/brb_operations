import MDSplus as mds

shot = 55715
tr = mds.Tree('wipal',shot)
dir = 'db1'
pbs = []

for pb in range(1,17):
    ndName = '\\hook_bdot2_pb{0:02d}_{1}'.format(pb,dir)
    nd = tr.getNode(ndName)
    data = str(nd.getData())
    last = data[-9:]
    pbs.append(last)
    print(last)

tr.close()
