function hook_bdot2_run_proc(shots)
% takes shot number or array of CONTINUOUS shot numbers and uploads raw data to
% MDSplus


%% make diary log file

funcfpath = fileparts(mfilename('fullpath'));
logfile = fullfile(funcfpath, 'putlog.txt');
logid = fopen(logfile, 'wt');



%% shot info

% login credentials
mdsip = 'localhost';
sql_dbase = 'skywalker';
sql_user = 'public';
sql_pswd = 'public';
% mdsip = 'skywalker.physics.wisc.edu';
% sql_dbase = 'skywalker';
% sql_user = 'olson';
% sql_pswd = 'dark$ide';


try
    conn = database(sql_dbase, sql_user, sql_pswd);
    sqlgood = 1;
    % radial location and clocking of shaft from sql database
    sqlquery = sprintf('select shot_num,te_r,te_z from TREX_data where shot_num between %d and %d',shots(1),shots(end));
    parameters = select(conn, sqlquery);
    close(conn);
catch
    fprintf(logid,'Error connecting to SQL database. Continuing to populate raw data nodes\n');
    sqlgood = 0;
end


try
    mdsconnect(mdsip);
catch
    fprintf(logid,'Error connecting to MDSplus\n');
    return
end


%% Define mdsplus tree name
tree = 'hook_bdot2';



%% grab config file data

config = fullfile(funcfpath, 'config', [tree '_config.ini']);
I = INI('File',config);
I.read();


%% upload data to MDSplus

nshots = length(shots);
for nni = 1:nshots
    
    shotnum = shots(nni);
    fprintf(1, 'Shot %d of %d\n', nni, nshots);
    
    fprintf(logid, 'Uploading %s data: Shot %d\n', tree, shotnum);
    mdsopen('wipal',shotnum);
    
    %% db nodes
    axnames = {'db1', 'db2', 'db3a', 'db3b'};
    numPbs = 8;

    for pbnum1 = 1:numPbs
        
        pcb = sprintf('pcb%02d', pbnum1);
        fprintf(logid, '    Adding pcb %02d raw data\n', pbnum1);
        
        for axn = 1:4
            
            ax = axnames{axn};
            err = []; % fail strings
            
            % gain data
            gainNdName = sprintf('magnetics.%s.pcbs.%s.%s.gain', tree, pcb, ax);
            gainData = I.(pcb).(ax).gain;
            gainUnit = I.(pcb).(ax).gainunit;
            gainStr = 'build_with_units($1,$2)';

            err = mdsputWrapper(err, gainNdName, gainStr, gainData, gainUnit);
            
            % raw data
            rawNdName = sprintf('magnetics.%s.pcbs.%s.%s.raw', tree, pcb, ax);
            rawData = I.(pcb).(ax).raw;

            err = mdsputWrapper(err, rawNdName, rawData);

        end
        
        if ~isempty(err)
            fprintf(logid,'        Failure on nodes: %s\n',err);
        end 
    end
    
    %% Fix bad channels
    
    if sqlgood
        %% port configuration data nodes
        fprintf(logid,'    Adding port configuration data\n');
        
        % port info
        rport = I.port.rport;
        lat = I.port.lat;
        long = I.port.long;
        alpha = I.port.alpha;
        beta = I.port.beta;
        gamma = I.port.gamma;
        clock = I.port.clock;
        pii = find(parameters.shot_num==shotnum);
        rpivot = rport*cosd(lat); %128.3e-2; % m
        rbend = round((parameters.te_z(pii)), 2)*1e-2; % from motor control VI
        insert = rpivot-rbend; % insertion depth (will come from motor control VI)
        
        ptNames = {'rport', 'lat', 'long', 'alpha', 'beta', 'gamma', 'clock', 'insert'};
        ptUnits = {'m', 'deg', 'deg', 'deg', 'deg', 'deg', 'deg', 'm'};
        matDats = {rport, lat, long, alpha, beta, gamma, clock, insert};

        err = []; % fail strings

        for porti = 1:length(ptNames)
            
            nms = ptNames{porti};
            un = ptUnits{porti};
            
            ndName = ['\' tree '_' nms];
            putStr = sprintf( 'BUILD_WITH_UNITS($1,"%s")', un);
            matData = matDats{porti};

            err = mdsputWrapper(err, ndName, putStr, matData);

        end
        
        if ~isempty(err)
            fprintf(logid,'        Failure on nodes: %s\n',err);
        end
        
        
        %% probe coordinates (PCBs)
        fprintf(logid,'    Adding PCB coordinate data\n');
        
        [z1, z2, z3a, z3b] = hook2_probe_offsets;
        zs = [z1; z2; z3a; z3b];

        Xall = zeros(3,numPbs,size(zs,1));


        for dirIndex = 1:size(zs,1)
            probeZ = zs(dirIndex,:);
            [r, phi, z, x, y] = port_to_mach_positions_hook(probeZ, insert, clock, rport, lat, long, alpha, beta, gamma);
            axStr = axnames{dirIndex};
            
            for pcbNum = 1:numPbs
                coords = {'r','phi','z'};
                coordUnits = {'m','deg','m'};
                coordDataCell = {r(pcbNum), phi(pcbNum), z(pcbNum)};
                err = []; % fail strings

                for coordII = 1:length(coords)
                    coordStr = coords{coordII};
                    coordUnitStr = coordUnits{coordII};
                    coordData = coordDataCell{coordII};

                    ndName = sprintf('\\%s_pcb%02d_%s.%s',tree,pcbNum,axStr,coordStr);
                    ndput = sprintf('BUILD_WITH_UNITS($1,"%s")',coordUnitStr);

                    err = mdsputWrapper(err, ndName, ndput, coordData);

                end

                if ~isempty(err)
                    fprintf(logid, '        Failure on nodes: %s\n',err);
                end

            end

            % congregate coordinates for all probes
            Xall(1,:,dirIndex) = x;
            Xall(2,:,dirIndex) = y;
            Xall(3,:,dirIndex) = z;
        end


        %% Probe Signals and Coordinates (pseudo-probe positions)
        % defines probe positions, effectively doubling locations to be
        % used for transformations
        % pb01: 1-3a, 1-2, 1-1
        % pb02: 1-2, 1-3b, 1-1
        % pb03: 1-1, 2-3a, 2-2
        % pb04: 2-2, 2-3b, 2-1
        % pb05: 2-1, 3-3a, 3-2 ...
        
        fprintf(logid,'    Converting PCB data to probe data\n');

        % PCB ## for each db1,db2, db3
        db2_pcb = repelem(1:numPbs,2);
        db1_pcb = [1 db2_pcb(1:15)];
        db3_pcb = repelem(1:numPbs,2);
        db3_aorb = repmat({'a','b'},1,numPbs);
        db3_aorb_index = repmat([3,4],1,numPbs);
        db_pcbs = [db1_pcb; db2_pcb; db3_pcb];

        phiAll = zeros(1,numPbs*2);
        
        % assign PCBs to Probe values
        for pb_index = 1:numPbs*2

            err = [];

            Xpcbs = zeros(3,3);

            for dir = 1:3
                probe_node_name = sprintf('\\%s_pb%02d_db%d',tree,pb_index,dir);

                % add data
                pcb_num = db_pcbs(dir,pb_index);
                if dir==3
                    db_name = ['db' num2str(dir) db3_aorb{pb_index}];
                else
                    db_name = ['db' num2str(dir)];
                end
                pcb_node_name = sprintf('\\%s_pcb%02d_%s',tree,pcb_num,db_name);
                
                err = mdsputWrapper(err, probe_node_name, pcb_node_name);


                % average positions
                if dir == 3
                    dir_index = db3_aorb_index(pb_index);
                else
                    dir_index = dir;
                end
                Xpcbs(:,dir) = Xall(:,db_pcbs(dir,pb_index), dir_index);
            end

            aveXpcbs = mean(Xpcbs,2);
            xave = aveXpcbs(1);
            yave = aveXpcbs(2);
            zave = aveXpcbs(3);
            rave = sqrt(xave^2 + yave^2);
            phiave = atan2d(yave,xave);
            phiAll(pb_index) = phiave;

            % add positions
            posCell = {'r', 'phi', 'z'};
            posUnits = {'m','deg','m'};
            posDataCell = {rave, phiave, zave};

            for posIndex = 1:length(posCell)
                pos = posCell{posIndex};
                unit = posUnits{posIndex};
                data = posDataCell{posIndex};

                pb_pos_node = sprintf('\\%s_pb%02d_%s',tree,pb_index,pos);
                pb_pos_put = sprintf('BUILD_WITH_UNITS($1,"%s")',unit);

                err = mdsputWrapper(err, pb_pos_node, pb_pos_put, data);
            end

            if ~isempty(err)
                fprintf(logid, '        Failure on nodes: %s\n',err);
            end
            
        end

        %% Probe signal transformations
        fprintf(logid,'    Applying probe signal transformations\n');

        
        % grab signal transformation matrix
        Msig = hook_signal_matrix(phiAll, clock, lat, long, alpha, beta, gamma);


        poscell = {'r','phi','z'};

        for pb_index = 1:numPbs*2

            err = [];

            for pos_index = 1:length(poscell)
                pos = poscell{pos_index};
                pb_node = sprintf('\\%s_pb%02d_db%s',tree,pb_index,pos);

                raw_pb_cell = cell(1,3);
                for dir = 1:3
                    raw_pb_cell{dir} = sprintf('\\%s_pb%02d_db%d',tree,pb_index,dir);
                end

                % equation: [dbr dbphi dbz]' = Msig*[db1 db2 db3]'
                sig = sprintf('BUILD_SIGNAL(BUILD_WITH_UNITS($1*%1$s+$2*%2$s+$3*%3$s,"T/s"),,DIM_OF(%1$s))',raw_pb_cell{1},raw_pb_cell{2},raw_pb_cell{3});

                err = mdsputWrapper(err, pb_node, sig, Msig(pos_index,1,pb_index), Msig(pos_index,2,pb_index), Msig(pos_index,3,pb_index));
            end

            if ~isempty(err)
                fprintf(logid, '        Failure on nodes: %s\n',err);
            end

        end
    end
    
    mdsclose;
    
end



%% clean up

mdsdisconnect;
fclose(logid);



function [errorOut] = mdsputWrapper(errorIn, node, expression, varargin)

mdsout = mdsput(node, expression, varargin{:});

if ~mod(mdsout,2)
    errorOut = [errorIn ', ' node];
else
    errorOut = errorIn;
end


function [z1, z2, z3a, z3b] = hook2_probe_offsets

tip_offset = 47.5e-2; % m
to_first_probe = 2.5e-2; % m

z3a = tip_offset - to_first_probe - 2*2.54e-2*(0:7);
z2 = z3a - 10e-3;
z3b = z3a - 25e-3;
z1 = z3a - 34e-3;


function Xpb_hook = hook_pos_in_port_coords(deltaZ, insert, clocking)

% check if row or column vector. Change to row vector
if iscolumn(deltaZ)
    deltaZ = deltaZ';
end

Rbend = hook_bend_matrix(clocking);

% position of probes (in port coordinates)
Xpb_hook = Rbend*[zeros(2, numel(deltaZ)); deltaZ] + [0; 0; insert];


function Rbend = hook_bend_matrix(clocking)

% rotation matrix for 90 deg bend (defined as CCW rotation)
Ry90 = [cosd(-90)   0   -sind(-90); ...
        0           1          0; ...
        sind(-90)   0   cosd(-90)];
RxRot = [1  0              0; ...
         0  cosd(clocking) -sind(clocking); ...
         0  sind(clocking) cosd(clocking)];
Rbend = Ry90*RxRot;


function Mport = port_to_mach_matrix(latitude, longitude, alpha, beta, gamma)

% rotation matrix of port
Rx = [1 0 0; 0 cosd(beta) -sind(beta); 0 sind(beta) cosd(beta)];
Ry = [cosd(alpha) 0 -sind(alpha); 0 1 0; sind(alpha) 0 cosd(alpha)];
Rz = [cosd(gamma) -sind(gamma) 0; sind(gamma) cosd(gamma) 0; 0 0 1];
Rport = Rx*Ry*Rz;

% NED transformation
Rned = [-sind(latitude)*cosd(longitude) -sind(longitude) -cosd(latitude)*cosd(longitude); ...
        -sind(latitude)*sind(longitude) cosd(longitude)  -cosd(latitude)*sind(longitude); ...
        cosd(latitude)                  0                -sind(latitude)];

% combined matrices
Mport = Rned*Rport;


function [r, phi, z, x, y] = port_to_mach_positions_hook(deltaZ, insert, clocking, radiusPort, latitude, longitude, alpha, beta, gamma)

% grab probe vector in port coordinates
Xpb_hook = hook_pos_in_port_coords(deltaZ, insert, clocking);

% grab port rotation matrix
Mport = port_to_mach_matrix(latitude, longitude, alpha, beta, gamma);

% port location in machine coordinates
Xport = [radiusPort*cosd(latitude)*cosd(longitude); ...
        radiusPort*cosd(latitude)*sind(longitude); ...
        radiusPort*sind(latitude)];

% Probe locations in machine coordinates (vector)
X_mach = Mport*Xpb_hook + Xport;

% probe locations in machine coordinates (machine cylindrical coordinates)
x = X_mach(1,:);
y = X_mach(2,:);
z = X_mach(3,:);
r = sqrt(x.^2 + y.^2);
phi = atan2d(y,x);


function Msig = hook_signal_matrix(phi, clocking, latitude, longitude, alpha, beta, gamma)

% phi = average phi location of 3-axis probe set

% grab bend matrix
Rbend = hook_bend_matrix(clocking);

% grab port rotation matrix
Mport = port_to_mach_matrix(latitude, longitude, alpha, beta, gamma);

% calculate full transformation matrix for signals
Msig = zeros(3, 3, length(phi));
for pbnum = 1:length(phi)
    phin = phi(pbnum);
    Rpol = [ cosd(phin) sind(phin) 0; ...
            -sind(phin) cosd(phin) 0; ...
            0           0          1];
    Msig(:,:,pbnum) = Rpol*Mport*Rbend;
end




























