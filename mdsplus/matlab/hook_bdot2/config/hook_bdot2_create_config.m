% build config file for linear probe data
% J. Olson on 09/03/2021

% port configuration data will eventually come from motor control vi and
% sql database


%% Initialize ini file

I = INI();
config = 'hook_bdot2_config.ini';



%% Channel map and calibration factors

numPbs = 8;

% board mapping
bd1 = struct('dig', 114, 'ch', 1:16);
bd2 = struct('dig', 114, 'ch', 17:32);

% create zero arrays
ax3a = struct('dig', zeros(1,numPbs), 'ch', zeros(1,numPbs));
ax3b = struct('dig', zeros(1,numPbs), 'ch', zeros(1,numPbs));
ax2= struct('dig', zeros(1,numPbs), 'ch', zeros(1,numPbs));
ax1 = struct('dig', zeros(1,numPbs), 'ch', zeros(1,numPbs));

% bd1
  ax1.ch(1:4) = bd1.ch(1:4);
  ax2.ch(1:4) = bd1.ch(5:8);
 ax3a.ch(1:4) = bd1.ch(9:2:16);
 ax3b.ch(1:4) = bd1.ch(10:2:16);
 ax1.dig(1:4) = bd1.dig;
 ax2.dig(1:4) = bd1.dig;
ax3a.dig(1:4) = bd1.dig;
ax3b.dig(1:4) = bd1.dig;
% bd2
  ax1.ch(5:8) = bd2.ch(1:4);
  ax2.ch(5:8) = bd2.ch(5:8);
 ax3a.ch(5:8) = bd2.ch(9:2:16);
 ax3b.ch(5:8) = bd2.ch(10:2:16);
 ax1.dig(5:8) = bd2.dig;
 ax2.dig(5:8) = bd2.dig;
ax3a.dig(5:8) = bd2.dig;
ax3b.dig(5:8) = bd2.dig;

digs = [ax3a.dig; ax3b.dig; ax2.dig; ax1.dig];
chans = [ax3a.ch; ax3b.ch; ax2.ch; ax1.ch];

% probe names
axnames = {'db3a' 'db3b' 'db2' 'db1'};
gainunit = 'T/s/V';
rawunit = 'V';

% calibration factors (V to T/s);
% based off trex z direction calibration
cal3a = 32500;
cal3b = cal3a;
cal2 = 23544;
cal1 = 33238;

pbdirs = [-1 1 1 -1]; % directions correspond to definition of local probe coordinates
cals = [cal3a cal3b cal2 cal1].*pbdirs; % includes direction




%% Parse channel mapping and 

pbst = struct();

for pb = 1:numPbs
    
    pbname = ['pcb' num2str(pb, '%02d')];
    
    for axnum = 1:length(axnames)
        
        axname = axnames{axnum};
        axst = struct();
        
        % gain info
        axst.gain = cals(axnum);
        axst.gainunit = gainunit;
        
        % tree data info
        serial = digs(axnum,pb);
        if isnan(serial)
            node = '';
        else
            chan = chans(axnum,pb);
            node = sprintf('\\ta%d_%02d', serial, chan);
        end
        axst.raw = node;
        axst.rawunit = rawunit; % actually set by referenced node
        
        % add individual axis structure to probe structure
        pbst.(axname) = axst;
        
    end
    
    % Add sections to ini file
    I.add(pbname, pbst);
    
end


%% Port configuration data 
% will be depricated once data is available on SQL database


rport = 1.555;
lat = -34.39;
long = 45;
alpha = -lat;
beta = 0;
gamma = 0;
clock = 0;

portnames = {'rport', 'lat', 'long', 'alpha', 'beta', 'gamma', 'clock'};
portvals = {rport, lat, long, alpha, beta, gamma, clock};

port = struct();
for numval = 1:length(portnames)
    nme = portnames{numval};
    val = portvals{numval};
    
    port.(nme) = val;    
end

% add values
I.add('port', port);



%% Write ini file

I.write(config);

% winopen(config);


 



























