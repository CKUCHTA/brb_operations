function test_batch(shotnum)

scripts = {'linear_bdot1_run_proc', ...
           'hook_bdot1_run_proc', ...
           'hook_bdot2_run_proc', ...
           'speed_bdot1_run_proc'};

% for ss = 1:numel(scripts)
%     ss
%     scr = sprintf('%s(%d)', scripts{ss}, shotnum);
%     job(ss) = batch(scr);
% end

scr = strcat(scripts, ['(' num2str(shotnum) ')']);
scr{1}
% job = batch(scr{1});

% wait(job);

disp('Success!')