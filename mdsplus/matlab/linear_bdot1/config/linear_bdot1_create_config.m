% build config file for linear probe data
% J. Olson on 09/03/2021

% port configuration data will eventually come from motor control vi and
% sql database


%% Initialize ini file

I = INI();
config = 'linear_bdot1_config.ini';



%% Channel map and calibration factors

numPbs = 24;

% board mapping
bd1 = struct('dig', 116, 'ch', 1:16);
bd2 = struct('dig', 116, 'ch', 17:32);
bd3 = struct('dig', 128, 'ch', 1:16);
bd4 = struct('dig', 128, 'ch', 17:32);
bd5 = struct('dig', 320, 'ch', 1:16);
bd6 = struct('dig', 320, 'ch', 17:32);

% create zero arrays
ax3a = struct('dig', zeros(1,numPbs), 'ch', zeros(1,numPbs));
ax3b = struct('dig', zeros(1,numPbs), 'ch', zeros(1,numPbs));
ax2= struct('dig', zeros(1,numPbs), 'ch', zeros(1,numPbs));
ax1 = struct('dig', zeros(1,numPbs), 'ch', zeros(1,numPbs));

% bd1
ax3a.ch(1:2:23) = bd1.ch(1:12);
ax3a.dig(1:2:23) = bd1.dig;
ax1.ch(1:2:7) = bd1.ch(13:16);
ax1.dig(1:2:7) = bd1.dig;
% bd2
ax1.ch(9:2:23) = bd2.ch(1:8);
ax1.dig(9:2:23) = bd2.dig;
ax2.ch(1:2:15) = bd2.ch(9:16);
ax2.dig(1:2:15) = bd2.dig;
% bd3
ax2.ch(17:2:23) = bd3.ch(1:4);
ax2.dig(17:2:23) = bd3.dig;
ax3a.ch(2:2:24) = bd3.ch(5:16);
ax3a.dig(2:2:24) = bd3.dig;
%bd4
ax1.ch(2:2:24) = bd4.ch(1:12);
ax1.dig(2:2:24) = bd4.dig;
ax2.ch(2:2:8) = bd4.ch(13:16);
ax2.dig(2:2:8) = bd4.dig;
% bd5
ax2.ch(10:2:24) = bd5.ch(1:8);
ax2.dig(10:2:24) = bd5.dig;
ax3b.ch(1:2:15) = bd5.ch(9:16);
ax3b.dig(1:2:15) = bd5.dig;
% bd6
ax3b.ch(17:2:23) = bd6.ch(1:4);
ax3b.dig(17:2:23) = bd6.dig;
ax3b.ch(2:2:24) = bd6.ch(5:16);
ax3b.dig(2:2:24) = bd6.dig;

digs = [ax3a.dig; ax3b.dig; ax2.dig; ax1.dig];
chans = [ax3a.ch; ax3b.ch; ax2.ch; ax1.ch];

% probe names
axnames = {'db3a' 'db3b' 'db2' 'db1'};
gainunit = 'T/s/V';
rawunit = 'V';

% calibration factors (V tp T/s);
% based off trex z direction calibration
cal3a = 32500;
cal3b = cal3a;
cal2 = 23544;
cal1 = 33238;

pbdirs = [-1 1 1 -1]; % directions correspond to definition of local probe coordinates
cals = [cal3a cal3b cal2 cal1].*pbdirs; % includes direction




%% Parse channel mapping and 

pbst = struct();

for pb = 1:numPbs
    
    pbname = ['pcb' num2str(pb, '%02d')];
    
    for axnum = 1:length(axnames)
        
        axname = axnames{axnum};
        axst = struct();
        
        % gain info
        axst.gain = cals(axnum);
        axst.gainunit = gainunit;
        
        % tree data info
        serial = digs(axnum,pb);
        if isnan(serial)
            node = '';
        else
            chan = chans(axnum,pb);
            node = sprintf('\\ta%d_%02d', serial, chan);
        end
        axst.raw = node;
        axst.rawunit = rawunit; % actually set by referenced node
        
        % add individual axis structure to probe structure
        pbst.(axname) = axst;
        
    end
    
    % Add sections to ini file
    I.add(pbname, pbst);
    
end


%% Port configuration data 
% will be depricated once data is available on SQL database

% % For boxport location
% rport = 1.6238;
% lat = -75.92;
% long = -96.45;
% alpha = -14.08;
% beta = 0;
% gamma = 0; % alignment with r/phi/z (use 6.45 to align with x/y/z);
% clock = 0;

% 5 deg port
rport = 1.52;
lat = -5;
long = -30;
alpha = 0;
beta = 0;
gamma = 0; % alignment with r/phi/z (use 6.45 to align with x/y/z);
clock = 0;

portnames = {'rport', 'lat', 'long', 'alpha', 'beta', 'gamma', 'clock'};
portvals = {rport, lat, long, alpha, beta, gamma, clock};

port = struct();
for numval = 1:length(portnames)
    nme = portnames{numval};
    val = portvals{numval};
    
    port.(nme) = val;    
end

% add values
I.add('port', port);



%% Write ini file

I.write(config);

% winopen(config);


 



























