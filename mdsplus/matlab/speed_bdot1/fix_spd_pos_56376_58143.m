% J. Olson - 9/27/2022
% fix speed probe position for TREX drive cylinder placement (shots
% 56376-58143)

% turn on logging
diary fix_shots.txt

r = [-13.1 -7.8 -2.5 2.9 8.2 13.5 18.9 24.2 ...
            29.5 34.9 40.2 45.5 50.9 56.2 61.5 66.9];
r = r + 6.5;
        
z = 0*ones(size(r));

% convert to m
r = r/100; % m
z = z/100; % m

lat = 0;
long = 180 - 42.5;
phi = long*ones(size(r));


shotrange = 56376:58143;

mdsconnect('skywalker.physics.wisc.edu');

for shot = shotrange
    try 
        mdsopen('wipal', shot)
        % update probe position nodes
        for pbnum2 = 1:16
            crds = {'r','phi','z'};
            crdUnits = {'m','deg','m'};
            crdDat = {r(pbnum2), phi(pbnum2), z(pbnum2)};

            for crdi = 1:3
                ndName = sprintf('\\speed_bdot1_pb%02d_%s',pbnum2,crds{crdi});
                ndput = sprintf('BUILD_WITH_UNITS($1,"%s")',crdUnits{crdi});
                mdsput(ndName, ndput, crdDat{crdi});
            end
        end
        % update port location
        mdsput('\speed_bdot1_lat', 'BUILD_WITH_UNITS($1,"deg")', lat);
        mdsput('\speed_bdot1_long', 'BUILD_WITH_UNITS($1,"deg")', long);

        mdsclose;
    catch ME
        disp(shot)
        errortxt = getReport(ME);
        disp(errortxt)
    end
end

mdsdisconnect;
diary off

