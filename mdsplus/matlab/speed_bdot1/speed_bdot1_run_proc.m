function speed_bdot1_run_proc(shots)
% takes shot number or array of CONTINUOUS shot numbers and uploads raw data to
% MDSplus


%% make diary log file

funcfpath = fileparts(mfilename('fullpath'));
logfile = fullfile(funcfpath, 'putlog.txt');
logid = fopen(logfile, 'wt');



%% shot info

% No need for SQL query
mdsip = 'localhost';
% mdsip = 'skywalker.physics.wisc.edu';
sqlgood = 1;


try
    mdsconnect(mdsip);
catch
    fprintf(logid,'Error connecting to MDSplus\n');
    return
end



%% grab config file data

% config = './config/speed_bdot1_config.ini';
config = fullfile(funcfpath, 'config', 'speed_bdot1_config.ini');
I = INI('File',config);
I.read();


%% upload data to MDSplus

nshots = length(shots);
for nni = 1:nshots
    
    shotnum = shots(nni);
    fprintf(1, 'Shot %d of %d\n', nni, nshots);
    
    fprintf(logid, 'Uploading speed probe data: Shot %d\n', shotnum);
    mdsopen('wipal',shotnum);
    
    %% db nodes
    axnames = {'db2', 'db1a', 'db1b', 'db3'};
    numPbs = 16;
    for pbnum1 = 1:numPbs
        
        pb = sprintf('pb%02d', pbnum1);
        fprintf(logid, '    Adding probe %02d raw data\n', pbnum1);
        
        for axn = 1:4
            
            ax = axnames{axn};
            fail = []; % fail strings
            
            % gain data
            gainNdName = sprintf('magnetics.speed_bdot1.%s.%s.gain', pb, ax);
            gainData = I.(pb).(ax).gain;
            gainUnit = I.(pb).(ax).gainunit;
            gainStr = 'build_with_units($1,$2)';
            gainout = mdsput(gainNdName, gainStr, gainData, gainUnit);
            if ~mod(gainout,2)
                failold = fail;
                fail = [failold ', ' gainNdName];
            end
            
            % raw data
            rawNdName = sprintf('magnetics.speed_bdot1.%s.%s.raw', pb, ax);
            %         rawStr = 'build_signal($1,*,dim_of($1))';
            rawData = I.(pb).(ax).raw;
            rawout = mdsput(rawNdName, rawData);
            if ~mod(rawout,2)
                failold = fail;
                fail = [failold ', ' rawNdName];
            end
        end
        
        if ~isempty(fail)
            fprintf(logid,'        Failure on nodes: %s\n',fail);
        end 
    end
    
    %% Fix bad channels
    % db1b(10) --> db1a(10)
    % db1a(14) --> db1b(14)
    % db1b(16) --> db1a(16)
    fprintf(logid,'    Adding probe fixes\n');
    
    fail = []; % fail strings
    
    % pb10
    badNd = '\speed_bdot1_pb10_db1b';
    rep1 = '\speed_bdot1_pb10_db1a';
    fixput = mdsput(badNd, rep1);
    if ~mod(fixput,2)
        failold = fail;
        fail = [failold ', ' badNd];
    end
    
    % pb14
    badNd = '\speed_bdot1_pb14_db1a';
    rep1 = '\speed_bdot1_pb14_db1b';
    fixput = mdsput(badNd, rep1);
    if ~mod(fixput,2)
        failold = fail;
        fail = [failold ', ' badNd];
    end
    
    % pb16
    badNd = '\speed_bdot1_pb16_db1b';
    rep1 = '\speed_bdot1_pb16_db1a';
    fixput = mdsput(badNd, rep1);
    if ~mod(fixput,2)
        failold = fail;
        fail = [failold ', ' badNd];
    end
    
    if ~isempty(fail)
        fprintf(logid,'        Failure on nodes: %s\n',fail);
    end
    
    
    
    if sqlgood
        %% port configuration data nodes
        fprintf(logid,'    Adding port configuration data\n');
        
        % port info
        rport = I.port.rport;
        lat = I.port.lat;
        long = I.port.long;
        alpha = I.port.alpha;
        beta = I.port.beta;
        gamma = I.port.gamma;
        
        ptNames = {'rport', 'lat', 'long', 'alpha', 'beta', 'gamma'}; % not using: 'clock', 'insert'
        ptUnits = {'m', 'deg', 'deg', 'deg', 'deg', 'deg'};
        matDats = {rport, lat, long, alpha, beta, gamma};
        for porti = 1:length(ptNames)
            
            nms = ptNames{porti};
            un = ptUnits{porti};
            
            ndName = ['\speed_bdot1_' nms];
            putStr = sprintf( 'BUILD_WITH_UNITS($1,"%s")', un);
            matData = matDats{porti};
            
            out = mdsput(ndName, putStr, matData);
            fail = []; % fail strings
            if ~mod(out,2)
                failold = fail;
                fail = [failold ', ' ndName];
            end
        end
        
        if ~isempty(fail)
            fprintf(logid,'        Failure on nodes: %s\n',fail);
        end
        
        
        %% probe coordinates (adapted from HookCoordTrans)
        fprintf(logid,'    Adding probe coordinate data\n');
        
        [z, r] = getSpeedPos;
        phi = long*ones(size(z));
        
        for pbnum2 = 1:numPbs
            crds = {'r','phi','z'};
            crdUnits = {'m','deg','m'};
            crdDat = {r(pbnum2), phi(pbnum2), z(pbnum2)};
            fail = []; % fail strings
            for crdi = 1:3
                ndName = sprintf('\\speed_bdot1_pb%02d_%s',pbnum2,crds{crdi});
                ndput = sprintf('BUILD_WITH_UNITS($1,"%s")',crdUnits{crdi});
                ndout = mdsput(ndName, ndput, crdDat{crdi});
                if ~mod(ndout,2)
                    failold = fail;
                    fail = [failold ', ' ndName];
                end
            end
            if ~isempty(fail)
                fprintf(logid,'        Failure on nodes: %s\n',fail);
            end
        end
        
        
        
        %% probe signal transformations (adapted from HookCoordTrans)
        fprintf(logid,'    Adding transformed probe signals\n');
        
        % cart to polar transformation matrix based on probe position
        for pbnum3 = 1:numPbs
            
            % probe coords to machine coords:
            % za,zb --> 1a/1b
            % phi --> 2
            % r --> -3
            
            rndName = sprintf('\\speed_bdot1_pb%02d_dbr',pbnum3);
            phindName = sprintf('\\speed_bdot1_pb%02d_dbphi',pbnum3);
            zndName = sprintf('\\speed_bdot1_pb%02d_dbz',pbnum3);
            dirndNames = {zndName, phindName, rndName};
            
            db1a = sprintf('\\speed_bdot1_pb%02d_db1a',pbnum3);
            db1b = sprintf('\\speed_bdot1_pb%02d_db1b',pbnum3);
            db2 = sprintf('\\speed_bdot1_pb%02d_db2',pbnum3);
            db3 = sprintf('\\speed_bdot1_pb%02d_db3',pbnum3);
            
            sigz = sprintf('BUILD_SIGNAL(BUILD_WITH_UNITS($1*(%1$s+%2$s)/2,"T/s"),,DIM_OF(%1$s))',db1a,db1b);
            sigphi = sprintf('BUILD_SIGNAL(BUILD_WITH_UNITS($1*%1$s,"T/s"),,DIM_OF(%1$s))',db2);
            sigr = sprintf('BUILD_SIGNAL(BUILD_WITH_UNITS($1*%1$s,"T/s"),,DIM_OF(%1$s))',db3);
            sigs = {sigz, sigphi, sigr};
            facs = [1 1 -1]; 
            
            
            fail = []; % fail strings
            for dirii = 1:3
                dirndName = dirndNames{dirii};
                sig = sigs{dirii};
                fac = facs(dirii);
                dirout = mdsput(dirndName, sig, fac);
                if ~mod(dirout,2)
                    failold = fail;
                    fail = [failold ', ' dirndName];
                end
            end
            if ~isempty(fail)
                fprintf(logid,'        Failure on nodes: %s\n',fail);
            end
        end
    end
    
    mdsclose;
    
end



%% clean up

mdsdisconnect;
fclose(logid);


end




% grab speed probe positions
function [z_spd, r_spd] = getSpeedPos

% positions come from Alex (12/1/2020)
r_spd = [-13.1 -7.8 -2.5 2.9 8.2 13.5 18.9 24.2 ...
            29.5 34.9 40.2 45.5 50.9 56.2 61.5 66.9];
        
z_spd = 0*ones(size(r_spd));

% % Mar 3, 2022 - moved speed probe to 135 deg long and 5 deg lat. Also
% % retraced the probe ~25.7 cm out (along spherical radius).
% r_spd = r_spd + 25.6;
% z_spd = z_spd + 2.24;

% Shot 56376: Speed probe on TREX insert (drive cylinder) Z = 0(modified by
% insert). R(1) = -6.6 cm. Phi = 180-42.5 = 137.5 set in config
r_spd = r_spd + 6.5;
z_spd = z_spd + 0;

r_spd = r_spd/100; % m
z_spd = z_spd/100; % m

end

