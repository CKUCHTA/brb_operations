% build config file for hook probe data
% J. Olson on 04/14/2021

% 4/28/2016
% port configuration data will eventually be grabbed from sql database


%% Initialize ini file

I = INI();
config = 'hook_bdot1_config.ini';



%% Channel map and calibration factors

% channel map
digs_used = [113 115]; % digitizers used
% digitizer to probe
dig_ids = [1 2 1 2 1 2 1 2 1 2 1 2 1 2 1];
ax2a_dig = digs_used(dig_ids);
ax2b_dig = ax2a_dig; % same as y1_dig
ax1_dig = ax2a_dig; % same as y1_dig
ax3_dig = ax2a_dig; % same as y1_dig
digs = [ax2a_dig; ax2b_dig; ax1_dig; ax3_dig];
% raw channel to probe
ax2a_chan = [1 1 3 3 5 5 7 7 9 9 11 11 13 13 15];
ax2b_chan = [2 2 4 4 6 6 8 8 10 10 12 12 14 14 16];
ax1_chan = ax2a_chan+16;
ax3_chan = ax2b_chan+16;
chans = [ax2a_chan; ax2b_chan; ax1_chan; ax3_chan];
% probe names
axnames = {'db2a' 'db2b' 'db1' 'db3'};
gainunit = 'T/s/V';
rawunit = 'V';

% calibration factors (V tp T/s);
% based off approximate probe areas
% cal1 = 26080;
% cal2a = 18473;
% cal2b = cal2a;
% cal3 = 25501;
% based off trex z direction calibration
calfac = 32500/25501;
cal1 = 26080*calfac;
cal2a = 18473*calfac;
cal2b = cal2a;
cal3 = 25501*calfac;

pbdirs = [1 -1 1 -1]; % directions correspond to definition of local probe coordinates
cals = [cal2a cal2b cal1 cal3].*pbdirs; % includes direction




%% Parse channel mapping and 

numPbs = 15;
pbst = struct();

for pb = 1:numPbs
    
    pbname = ['pb' num2str(pb, '%02d')];
    
    for axnum = 1:length(axnames)
        
        axname = axnames{axnum};
        axst = struct();
        
        % gain info
        axst.gain = cals(axnum);
        axst.gainunit = gainunit;
        
        % tree data info
        serial = digs(axnum,pb);
        chan = chans(axnum,pb);
        node = sprintf('\\ta%d_%02d', serial, chan);
        axst.raw = node;
        axst.rawunit = rawunit; % actually set by referenced node
        
        % add individual axis structure to probe structure
        pbst.(axname) = axst;
        
    end
    
    % Add sections to ini file
    I.add(pbname, pbst);
    
%     disp(pbname)
%     disp(pbst.db1)
%     disp(pbst.db2a)
%     disp(pbst.db2b)
%     disp(pbst.db3)
    
end


%% Port configuration data 
% will be depricated once data is available on SQL database


rport = 155.6*1e-2;
lat = -10;
long = 45;
alpha = 10;
beta = 0;
gamma = 0;

portnames = {'rport', 'lat', 'long', 'alpha', 'beta', 'gamma'};
portvals = {rport, lat, long, alpha, beta, gamma};

port = struct();
for numval = 1:length(portnames)
    nme = portnames{numval};
    val = portvals{numval};
    
    port.(nme) = val;    
end

% add values
I.add('port', port);



%% Write ini file

I.write(config);

% winopen(config);


 



























