#!/usr/bin/python3
import MDSplus as mds
import sys
import logging

def traverse_tree(node):
    desc = node.getDescendants()
    if len(desc) == 0:
        logging.info(node.getPath(), node.isWriteOnce())
        return
    else:
        for n in desc:
            traverse_tree(n)

if __name__ == "__main__":
    tree_name = str(sys.argv[1])
    shot = int(sys.argv[2])
    tree = mds.Tree(tree_name,shot)
    top = tree.getNode("\\top")
    traverse_tree(top)

        
