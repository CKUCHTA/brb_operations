dispatch/close
set tree 'p1'/shot=-1
create pulse 'p2'
set tree 'p1'/shot='p2'
dispatch/build
dispatch/phase/log INIT
dispatch/close
close 'p1'
