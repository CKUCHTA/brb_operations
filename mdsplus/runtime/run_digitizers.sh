#!/bin/bash
################################################################################
## Written by: Ethan Peterson
## Contact Info: ethan.peterson@wisc.edu
## Date: 08/4/2016
##
## Main shell script for running data acquisition/processing sequence
##
## Creates new MDSplus raw trees and sets up digitizers in parallel, once the 
## digitizers write to the trees, processing scripts can be run. Logs output
## to file.
##
################################################################################

main () {
    # TODO: Add correct modules path to '.bash_profile' before running 'run_digitizers.sh'.
    ## Get current shot number and increment by 1 for new pulse
    SHOTNUM=$((16#$(hexdump -e '16 "%02x " "\n"' /data/wipal_model/top/shotid.sys)+1))
    LOGFILE=/data/raw/acq_hosts/run_digitizers_logfile.txt
    ERRFILE=/data/raw/acq_hosts/run_digitizers_errorfile.txt
    PROC_ERRFILE=/data/raw/acq_hosts/process_errorfile.txt
    PROC_STATEFILE=/data/raw/acq_hosts/process_state.txt
    ## Rewrite logfile
    > "$LOGFILE"
    > "$ERRFILE"
    > "$PROC_ERRFILE"

    exec >>"$LOGFILE" 2>&1 ## Send stdout and stderr to logfile
    exec 2>>"$ERRFILE"
    echo "run_digitzers.sh logfile for Shot #: $SHOTNUM Timestamp: $(date +%F_%T)"
    echo "run_digitzers.sh errorfile for Shot #: $SHOTNUM Timestamp: $(date +%F_%T)" >>"$ERRFILE"
    echo "IDLE" >"$PROC_STATEFILE"
    
    ## Create new mdsplus pulses
    python /home/WIPALdata/brb_operations/mdsplus/runtime/mdsCreateTrees.py "wipal" $SHOTNUM
    
    # [Deprecated]
    # Launch actions
    # mdstcl @/home/WIPALdata/brb_operations/mdsplus/run_actions.tcl linear_hall3 $SHOTNUM
    # mdstcl @/home/WIPALdata/brb_operations/mdsplus/run_actions.tcl surface_hall $SHOTNUM
    # mdstcl @/home/WIPALdata/brb_operations/mdsplus/run_actions.tcl linear_hall2 $SHOTNUM
    # mdstcl @/home/WIPALdata/brb_operations/mdsplus/run_actions.tcl plasma_guns $SHOTNUM
    # [Deprecated] 
    
    # Run load config scripts
    python /home/WIPALdata/brb_operations/mdsplus/tree_configs/plasma_guns/plasma_guns_load_config.py $SHOTNUM
    python /home/WIPALdata/brb_operations/mdsplus/tree_configs/linear_hall3/linear_hall3_load_config.py $SHOTNUM
    
    ## Loop through all arguments and run appropriate digitizer functions
    for arg in $@;do
        choose_digitizer $arg
    done
    wait

    # data is posted
    # run python sql stuff here
    # wait
    
    # SQL load scripts
    # python /home/WIPALdata/brb_operations/mdsplus/tree_configs/mach_triple/mach_triple_load_sql.py $SHOTNUM &
    # python /home/WIPALdata/brb_operations/mdsplus/tree_configs/spiral_probe/upload_spiral_probe_data.py $SHOTNUM &
    # spectrometer scripts
    # python /home/WIPALdata/brb_operations/mdsplus/tree_configs/spec1/spec1_sql_load.py $SHOTNUM
    # python /home/WIPALdata/brb_operations/mdsplus/tree_configs/spec1/time_parser.py $SHOTNUM
    # python /home/WIPALdata/brb_operations/mdsplus/tree_configs/spec1/radiance.py $SHOTNUM
    # python emit_done_event.py oes_ready
    
    python /home/WIPALdata/brb_operations/mdsplus/tree_configs/cathodes/cathode_sql_load.py $SHOTNUM &
    python /home/WIPALdata/brb_operations/mdsplus/tree_configs/anodes/anode_sql_load.py $SHOTNUM &
    wait
    
    # Move to Post Processing state
    exec 2>>"$PROC_ERRFILE"
    echo "PROCESS" >"$PROC_STATEFILE"
    echo "shot_processing.sh errorfile for Shot #: $SHOTNUM Timestamp: $(date +%F_%T)" >>"$PROC_ERRFILE"
    echo "Post Processing"

    # Run post processing scripts
    # matlab -batch runs matlab without most features but still loads all of matlab. To remove this we use a single matlab line but splits it into 'batch' calls which parallelizes the running.
    # ssh WIPALdata@192.168.113.1 "cd ~/wippl_matlab/ ; matlab -batch \"hook_bdot1_run_proc($SHOTNUM); speed_bdot1_run_proc($SHOTNUM); linear_bdot1_run_proc($SHOTNUM); \"" &
    
    # ssh WIPALdata@192.168.113.1 "cd ~/wippl_matlab/ ; matlab -batch \"hook_bdot1_run_proc($SHOTNUM); \"" &

    python /home/WIPALdata/brb_operations/mdsplus/tree_configs/linear_hall3/linear_hall3_run_proc.py $SHOTNUM &
    python /home/WIPALdata/brb_operations/mdsplus/tree_configs/te_probe1/run_proc.py $SHOTNUM &
    python /home/WIPALdata/brb_operations/mdsplus/tree_configs/te_probe3/run_proc.py $SHOTNUM &
    python /home/WIPALdata/brb_operations/mdsplus/tree_configs/pa_probe1/run_proc.py $SHOTNUM &
    # wait
    
    # [Deprecated]
    # ssh MPDXdata@192.168.113.64 "cd /home/MPDXdata/mpdx_scripts/PythonModules/processing; ./shot_processing.sh $SHOTNUM" &
    # [Deprecated]
    
    # [Deprecated
    # Run store actions
    # mdstcl @/home/WIPALdata/brb_operations/mdsplus/dispatch_store.tcl linear_hall3 $SHOTNUM &
    # mdstcl @/home/WIPALdata/brb_operations/mdsplus/dispatch_store.tcl linear_hall2 $SHOTNUM &
    # mdstcl @/home/WIPALdata/brb_operations/mdsplus/dispatch_store.tcl magnetrons $SHOTNUM &
    # [Deprecated

    # if you want to write mach offsets, enable this next line
    # python write_mach_offsets.py $SHOTNUM &
    # wait

    # Return to idle state
    echo "IDLE" >"$PROC_STATEFILE"
    sleep 5

    echo -e "\nEmit events"
    python emit_done_event.py raw_data_ready
    python emit_done_event.py raw_data_ready2
    python emit_done_event.py mach_data_ready
    python emit_done_event.py gun_data_ready
    python emit_done_event.py mag_data_ready
    # python emit_done_event.py "proc_done"

    sleep 20
    # Emit a second event to get the data from digitizers that take a while to upload like the acq2106.
    python emit_done_event.py raw_data_ready
}

choose_digitizer () {
    if [ -n "$1" ]; then
        IFS=',' read -ra INPUTARR <<< "$1"
        TARGET="${INPUTARR[0]}"
        case $TARGET in
            192.168.113.9*) run_trex_cell $1;;
            192.168.120.*) run_acq_196 $1;;
        esac
    else
        run_default 
    fi
}

run_trex_cell () {
    IFS=',' read -ra INPUTARR <<< "$1"
    local TARGET="${INPUTARR[0]}"
    local CELL_ON="${INPUTARR[1]}"
    local OVERSAMPLE="${INPUTARR[2]}"
    local CHANNELS="${INPUTARR[3]}"
    local NUMSAMPLES="${INPUTARR[4]}"
    local SAMPLERATE="${INPUTARR[5]}"
    local EXTERNALTRIG="${INPUTARR[6]}"
    local NUMBOARDS="${INPUTARR[7]}"
    local DELAY="${INPUTARR[8]}"
    local MASTERCLOCK="${INPUTARR[9]}"
    local WAITTIME="${INPUTARR[10]}"
    local ISICSB="${INPUTARR[11]}"
    local PATHOUT="${INPUTARR[12]}"
    local QUIT=1
    case $TARGET in
        192.168.113.93) local TREENAME="trex_cell_1";
                        local FILEOUT=$PATHOUT"c01_$(printf "%06d" $SHOTNUM)";;
        192.168.113.94) local TREENAME="trex_cell_2";
                        local FILEOUT=$PATHOUT"c02_$(printf "%06d" $SHOTNUM)";;
        192.168.113.95) local TREENAME="trex_cell_3";
                        local FILEOUT=$PATHOUT"c03_$(printf "%06d" $SHOTNUM)";;
        192.168.113.96) local TREENAME="trex_cell_4";
                        local FILEOUT=$PATHOUT"c04_$(printf "%06d" $SHOTNUM)";;
        192.168.113.97) local TREENAME="trex_cell_5";
                        local FILEOUT=$PATHOUT"c05_$(printf "%06d" $SHOTNUM)";;
        192.168.113.98) local TREENAME="trex_cell_6";
                        local FILEOUT=$PATHOUT"c06_$(printf "%06d" $SHOTNUM)";;
        192.168.113.99) local TREENAME="trex_cell_7";
                        local FILEOUT=$PATHOUT"c07_$(printf "%06d" $SHOTNUM)";;
        *) echo ERROR;;
    esac

    local CMD="matlab -r -wait -nosplash -nojvm -nodesktop runICS($OVERSAMPLE,$CHANNELS,$NUMSAMPLES,$SAMPLERATE,$EXTERNALTRIG,$NUMBOARDS,$WAITTIME,$ISICSB,'$FILEOUT',$QUIT) & python trex_mdsplus_put.py $TREENAME $SHOTNUM $FILEOUT --t0 $DELAY"

    if [ "$CELL_ON" != 0 ]; then
        echo =======================================================================
        echo Cell Name: $TREENAME
        echo IP Address: $TARGET
        echo Oversampling: $OVERSAMPLE
        echo Number of Channels: $CHANNELS
        echo Number of Samples: $NUMSAMPLES
        echo "Sample Rate (MHz): $SAMPLERATE"
        echo "Delay (s): $DELAY"
        echo External Trigger: $EXTERNALTRIG
        echo Number of Boards: $NUMBOARDS
        echo "Timeout (s): $WAITTIME"
        echo isICSB: $ISICSB
        echo Output File Data Path: $FILEOUT
        echo Quit matlab on completion: $QUIT
        echo SSH Command: $CMD
        echo =======================================================================
        echo
        ssh TREX@$TARGET $CMD &
    fi
}

run_acq_196 () {
    IFS=',' read -ra INPUTARR <<< "$1"
    local TARGET="${INPUTARR[0]}"
    local ON="${INPUTARR[1]}"
    local CMDSUFFIX="direct_setup.sh"
    if [ "${INPUTARR[2]}" == 0 ]; then
        CMDSUFFIX="direct_fire.sh"
    fi
    local NUMSAMPLES="${INPUTARR[3]}"
    local SAMPLERATE="${INPUTARR[4]}"
    local CHANNELBLOCKMASK="${INPUTARR[5]}"
    local MODE="${INPUTARR[6]}"
    local MASTERCLOCK="${INPUTARR[7]}"
    local CLOCKDIV="${INPUTARR[8]}"
    local DELAY="${INPUTARR[9]}"
    case $TARGET in
        192.168.120.23 ) local CMD="./a368_"$CMDSUFFIX;;
        192.168.120.22 ) local CMD="./a223_"$CMDSUFFIX;;
        192.168.120.21 ) local CMD="./a220_"$CMDSUFFIX;;
        192.168.120.24 ) local CMD="./a371_"$CMDSUFFIX;;
        192.168.120.25 ) local CMD="./a372_"$CMDSUFFIX;;
        192.168.120.26 ) local CMD="./a373_"$CMDSUFFIX;;
        192.168.120.27 ) local CMD="./a470_"$CMDSUFFIX;;
    esac

    if [ "$ON" != 0 ]; then
        echo =======================================================================
        echo ACQ196 Board: ${CMD:2:4}
        echo IP Address: $TARGET
        echo Number of Samples: $NUMSAMPLES
        echo "Sample Rate (Hz): $SAMPLERATE"
        echo "Delay (s): $DELAY"
        echo Channel Block Mask: $CHANNELBLOCKMASK
        echo Operation Mode: $MODE
        echo "Master Clock Rate (Hz): $MASTERCLOCK"
        echo Master Clock Divisor: $CLOCKDIV
        echo SSH Command: "$CMD $NUMSAMPLES $SAMPLERATE $CHANNELBLOCKMASK $MODE $MASTERCLOCK $CLOCKDIV $DELAY $SHOTNUM"
        echo =======================================================================
        echo
        
        ssh root@$TARGET "$CMD $NUMSAMPLES $SAMPLERATE $CHANNELBLOCKMASK $MODE $MASTERCLOCK $CLOCKDIV $DELAY $SHOTNUM" &
    fi
}

run_default () {
    echo Need to complete this
}

main "$@"
