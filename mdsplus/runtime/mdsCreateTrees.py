import MDSplus as mds
import sys


def main(treeList, shotnum):
    """ Create MDSplus tree pulses for given trees and shot number

    Args:
        treeList (list of str): MDSplus tree names to create pulses for
        shotnum (int): desired shot number

    Returns:
        None

    Raises:

    Examples:
    """
    for treeName in treeList:
        try:                                                            
            myTree = mds.Tree(treeName, shotnum)
        except mds.MDSplusException:
            try:
                print("creating new {0} pulse: shot {1}...".format(treeName, shotnum))
                modelTree = mds.Tree(treeName, -1)
                modelTree.createPulse(shotnum)
                mds.tree.Tree.setCurrent(treeName, shotnum)
                print("success")
            except mds.MDSplusException as e:
                print("Create {0} pulse failed".format(treeName))
                sys.exit()
        else:
            print("{0} shot {1} already exists".format(treeName, shotnum))

if __name__ == "__main__":
    ## treeList should be input from command line as "tree1 tree2 tree3..."
    treeList = sys.argv[1].split()
    shotnum = int(sys.argv[2])
    main(treeList, shotnum)
