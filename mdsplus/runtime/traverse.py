import MDSplus as mds
from modules.mds_builders import traverse_tree
import sys

tree_name = str(sys.argv[1])
shot = int(sys.argv[2])
tree = mds.Tree(tree_name,shot)
top = tree.getNode("\\top")
traverse_tree(top)
