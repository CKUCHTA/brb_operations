"""
Create a connection to the MySQL database.
"""
import logging
from sqlalchemy import create_engine
import os.path
import configparser

mysql_engine = None

def get_engine(sql_config_path="/home/WIPALdata/sql_config.ini"):
    global mysql_engine
    if mysql_engine is None:
        # Read in the sql config file.
        sql_config = configparser.ConfigParser()
        if os.path.exists(sql_config_path):
            logging.debug("Found SQL config file at '{path}'. Reading config file.".format(path=sql_config_path))
            sql_config.read(sql_config_path)
            logging.info("Read SQL config file.")
        else:
            logging.error("Could not find config file for accessing MySQL database to get te probe position. File path '{path}' does not lead to the config file.".format(path=sql_config_path))
            raise FileNotFoundError("Could not find sql config file at '{path}'.".format(path=sql_config_path))

        # Get the values needed for connecting to database.
        database_type = 'mysql'
        database_config = sql_config[database_type]
        host = database_config['host']
        database = database_config['database']
        user = database_config['user']
        password = database_config['password']
        if 'drivername' in database_config.keys():
            drivername = database_config['drivername']
        else:
            drivername = None
        logging.debug("Found SQL connection info.")

        # Create the database url.
        if drivername is None or len(drivername.strip()) == 0:
            database_url = '{t}://{u}:{p}@{h}/{d}'.format(
                t=database_type, u=user, p=password, h=host, d=database
            )
            no_password_url = '{t}://{u}:{p}@{h}/{d}'.format(
                t=database_type, u=user, p='<password>', h=host, d=database
            )
        else:
            database_url = '{t}+{n}://{u}:{p}@{h}/{d}'.format(
                t=database_type, n=drivername, u=user, p=password, h=host, d=database
            )
            no_password_url = '{t}+{n}://{u}:{p}@{h}/{d}'.format(
                t=database_type, n=drivername, u=user, p='<password>', h=host, d=database
            )
        logging.debug("Created database url (password hidden): {}".format(no_password_url))
    
        # Create the new engine.
        mysql_engine = create_engine(database_url)

    return mysql_engine

