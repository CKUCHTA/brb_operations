# TODO: Change configobj to something else.
from configobj import ConfigObj, flatten_errors
from validate import Validator, ValidateError, VdtTypeError,VdtValueError
import numpy as np
import logging

def flatten(value):
    if not isinstance(value, list):
        raise VdtTypeError("Value must be of type list")
    for i in value:
        if isinstance(i,(list,tuple)):
            for j in flatten(i):
                yield j
            else:
                yield i

def nested_list_check(value, atom_type=None):
    flat_val = flatten(value)
    if atom_type is not None:
        if False in [isinstance(i, atom_type) for i in flat_val]:
            raise VdtTypeError("All items in value must be of type {0}".format(atom_type))
        return value

def ndarray_check(value):
    logging.info("1: {}".format(value))
    shape = None
    if not isinstance(value, list):
        raise VdtTypeError("Value must be of type list to start")
    try:
        value = np.array(value,dtype=np.float64)
        logging.info("2: {}".format(value))
        if shape is not None:
            if value.shape != shape:
                raise VdtValueError("Value must be of the specified shape: {0}".format(shape))
        logging.info("3: {}".format(value))
        return value
    except:
        raise VdtTypeError("Could not convert input value to numpy array of dtype: {0}".format(dtype))

class NPConfigObj(ConfigObj):

    def __init__(self, infile=None, options=None, configspec=None, encoding=None,
                 interpolation=True, raise_errors=False, list_values=True,
                 create_empty=False, file_error=False, stringify=True,
                 indent_type=None, default_encoding=None, unrepr=True,
                 write_empty_values=False, _inspec=False, mode="r", 
                 val_on_init=True,val_on_write=True,npify=True,backup=False):

        if infile:
            self.mode = mode.lower()
        else:
            self.mode = None
            if backup:
                raise IOError("There is no infile to create a backup of")

        self.isnp = npify
        self.val_on_write = val_on_write

        super(NPConfigObj, self).__init__(infile=infile, options=options, configspec=configspec, encoding=encoding,
                 interpolation=interpolation, raise_errors=raise_errors, list_values=list_values,
                 create_empty=create_empty, file_error=file_error, stringify=stringify,
                 indent_type=indent_type, default_encoding=default_encoding, unrepr=unrepr,
                 write_empty_values=write_empty_values, _inspec=_inspec)

        self.validator = Validator()
        if val_on_init:
            if configspec is None:
                raise ValueError("Cannot validate on instantiation without a configspec")
            logging.info("Validating config against the configspec on init...")
            val_res = self.validate(self.validator,preserve_errors=True)
            self.check_validation(val_res,raise_errors=True)

        if backup:
            self.backup()

        if self.isnp:
            self.npify()

    def check_validation(self,val_res,raise_errors=False):
        logging.info("Checking validation results...")
        if val_res is True:
            logging.info("Validation Successful!")
            return

        for entry in flatten_errors(self,val_res):
            section_list, key, error = entry
            if key is not None:
                section_list.append(key)
            else:
                section_list.append("[missing section]")
            section_string = ", ".join(section_list)
            if error == False:
                error = "Missing value or section."
            logging.info("{sec} = {err}".format(sec=section_string, err=error))

        if raise_errors:
            logging.error("Validation Failed...")
            raise ValidateError("There were errors in validating against the spec")
        else:
            logging.warn("Warning: Validation failed, but errors were supressed")

    def backup(self,ext="bak"):
        if self.filename is not None:
            with open(self.filename+"."+ext,"w") as f:
                self.write(outfile=f)
        else:
            raise IOError("Unable to find appropriate filename on which to base backup")

    def npify(self):
        logging.info("Converting desired entries to np.ndarray...")
        return self.walk(self._npify)

    def un_npify(self):
        logging.info("Converting np.ndarry types back to lists...")
        return self.walk(self._un_npify)

    def _npify(self, section, key, **kwargs):
        val = section[key]
        if not isinstance(val,list):
            return
        try:
            section[key] = np.array(val,dtype=np.float64)
        except TypeError:
            pass

    def _un_npify(self, section, key):
        val = section[key]
        if not isinstance(val,np.generic):
            return
        else:
            if isinstance(val,np.ndarray):
                section[key] = val.tolist()
            elif isinstance(val,np.floating):
                section[key] = float(val)
            elif isinstance(val,np.integer):
                section[key] = int(val)

    def write(self, outfile=None, section=None):
        # if outfile is not none write to file object
        # else open self.filename with write privilege and write
        # section is always not None on recursion
        # outfile is always None on recursion
        # section should only be None on first entry
        if (section is None):
            if outfile is None:
                if self.mode not in ["edit", "w",None]:
                    raise IOError("Config file is open in read-only mode")
            # Do stuff on first entry of write function
            if self.isnp:
                self.un_npify()
            if self.val_on_write:
                logging.info("Validating against the configspec before writing...")
                self.validate(self.validator)
                val_res = self.validate(self.validator,preserve_errors=True)
                self.check_validation(val_res,raise_errors=True)
            super(NPConfigObj,self).write(outfile=outfile,section=section)
            if self.isnp:
                self.npify()
        else:
            return super(NPConfigObj,self).write(outfile=outfile,section=section)


