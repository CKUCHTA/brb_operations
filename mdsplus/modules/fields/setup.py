from distutils.core import setup
from Cython.Build import cythonize

setup(name="greens_c2", ext_modules=cythonize("greens_c2.pyx"))
