from numpy import pi,linspace,meshgrid,sin,cos,sqrt,sum,array,ones,zeros,hstack,vstack,sign,mod,isfinite,ceil,isclose
from scipy.special import ellipk, ellipe
from multiprocessing import Process, Queue, cpu_count
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib.transforms import Affine2D
from numbers import Number
import importlib
import warnings
import sys


class MyDict(dict):
    validattrs = ("x","y","z")
    def __init__(self,*args,**kwargs):
        for key,value in kwargs:
            self.__setattr__(key,value)

    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __setattr__(self, name, value):
        if name in self.validattrs:
            self[name] = value
            super(MyDict,self).__setattr__(name,value)
        else:
            raise AttributeError("No such attribute: " + name)

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    @property
    def x(self):
        return " PROPERTY!"

class Sub(MyDict):
    validattrs = MyDict.validattrs + ("t","u","v")
    def __init__(self,*args,**kwargs):
        super(MyDict,self).__init__(*args,**kwargs)

class Current(object):
    """Represent an axisymmetric current for magnetic field modeling.
    Attributes:
        _loc
        _current
        _marker
    Properties:
        loc
        current
        marker
    Methods:
        plot(ax,**kwargs)
        to_dict()
        from_dict(cls_dict) (class method)
    """
    def __init__(self, position,current):
        self.current = current
        self.loc = position

    @property
    def loc(self):
        return self._loc

    @loc.setter
    def loc(self, position):
        r, z = position
        if r <= 0:
            warnings.warn("Current r location found in left half plane, setting it to zero",UserWarning)
            r = 0
            self.current = 0
        else:
            self.current = self._current
        self._loc = (float(r),float(z))

    @property
    def current(self):
        return self._current

    @current.setter
    def current(self,newcurrent):
        """Set current value in Amps + for ZxR direction - for RxZ"""
        self._current = float(newcurrent)
        if self._current == 0:
            self._marker = ""
        elif self._current < 0:
            self._marker = "ko"
        else:
            self._marker = "kx"

    @property
    def marker(self):
        return self._marker
    
    def plot(self,ax):
        """Plot current locations with markers for +/-"""
        r0,z0 = self._loc
        ax.plot(r0,z0,self._marker)

    def to_dict(self):
        cls = str(self.__class__).split("'")[1]
        return {"cls":cls,"loc":self._loc,"current":self._current}

    @classmethod
    def from_dict(cls,cls_dict):
        """Create Current instance from a dictionary"""
        loc = cl_dict.pop("loc")
        current = cls_dict.pop("current")
        return cls(loc,current)

class CurrentGroup(object):
    """ Grouping of Current objects
    Attributes:
        _obj_list
        _current
        _patchcls
        _patchargs_dict
        _patchkwargs
        _patch
    Properties:
        obj_list
        current
        rzdir
        patchcls
        patchargs_dict
        patchkwargs
        patch
    Methods:
        build_patchargs(**kwargs)
        update_patch()
        plot_currents(ax)
        to_dict()
        from_dict(cls_dict) (class method)
    """
    def __init__(self,**kwargs):
        CurrentGroup.reset(self,**kwargs)

    def reset(self,**kwargs):
        rz_pts = array(kwargs.pop("rz_pts",[(1,1)]))
        current = float(kwargs.pop("current",1))
        self._patchcls = kwargs.pop("patchcls",None)
        self._patchargs_dict = kwargs.pop("patchargs_dict",{})
        n,d = rz_pts.shape
        if not (d == 2 and n >= 1):
            raise ValueError("rz_pts shape: {0} is invalid, must be Nx2".format(rz_pts.shape))
        self._current = current
        self._obj_list = [Current((r,z),current) for r,z in rz_pts]
        self._patchkwargs = {"fc":"w","ec":"k","zorder":3}
        self._patchkwargs.update(kwargs)
        self.update_patch()

    @property
    def current(self):
        return self._current

    @current.setter
    def current(self,new_current):
        self._current = new_current
        for c_obj in self._obj_list:
            c_obj.current = new_current

    @property
    def obj_list(self):
        return self._obj_list

    @obj_list.setter
    def obj_list(self,new_obj_list):
        assert all([type(c_obj) == Current for c_obj in new_obj_list]),"All objects must be of type fields.core.Current"
        self._obj_list = new_obj_list

    @property
    def rzdir(self):
        return array([c_obj.loc+(1,) for c_obj in self._obj_list],dtype="float32")

    @property
    def patchcls(self):
        return self._patchcls

    @patchcls.setter
    def patchcls(self,new_cls):
        self._patchcls = new_cls

    @property
    def patchargs_dict(self):
        return self._patchargs_dict

    @patchargs_dict.setter
    def patchargs_dict(self,new_arg_dict):
        self._patchargs_dict = new_arg_dict

    @property
    def patchkwargs(self):
        return self._patchkwargs

    @patchkwargs.setter
    def patchkwargs(self,new_kwargs):
        self._patchkwargs = new_kwargs

    @property
    def patch(self):
        return self._patch

    def translate(self, position_change):
        dr, dz = position_change
        for c_obj in self._obj_list:
            r,z = c_obj.loc
            c_obj.loc = r+dr,z+dz
        self.update_patch()

    def rotate(self, position,angle):
        r0, z0 = position
        angle = pi/180.0*angle
        cost = cos(angle)
        sint = sin(angle)
        for c_obj in self._obj_list:
            r,z = c_obj.loc
            newr = cost*(r-r0) + sint*(z-z0) + r0
            newz = -sint*(r-r0) + cost*(z-z0) + z0
            c_obj.loc = (newr,newz)
        self.update_patch()

    def build_patchargs(self,**kwargs):
        # build arg tuple for patchcls for patchargs_dict
        # This should be the only method necessary to override in child class
        # for proper patch performance
        raise NotImplementedError("This method should be overridden in the child class")

    def rebuild(self,key,value):
        cls_dict = self.to_dict()
        cls_dict.pop("cls",None)
        if key not in cls_dict.keys():
            raise KeyError("key {0} not found in dictionary representation of {1}".format(key,self.__class__))
        cls_dict[key] = value
        self.reset(**cls_dict)

    def update_patch(self):
        try:
            patchargs = self.build_patchargs(**self._patchargs_dict)
            self._patch = self._patchcls(*patchargs,**self._patchkwargs)
        except NotImplementedError:
            self._patch = None

    def plot_currents(self,ax):
        for c_obj in self._obj_list:
           c_obj.plot(ax)

    def plot(self,ax):
        try:
            ax.plot(self.loc[0],self.loc[1],"co")
        except AttributeError:
            pass
        if self._patch:
            ax.add_collection(PatchCollection([self.patch],match_original=True))
        self.plot_currents(ax)

    def to_dict(self):
        cls_dict = {key.strip("_"):value for key,value in self.__dict__.items()}
        cls_dict.pop("obj_list")
        cls_dict.pop("patch")
        cls = str(self.__class__).split("'")[1]
        cls_dict.update({"rz_pts":self.rzdir[:,0:2],"cls":cls})
        cls_dict.update(cls_dict.pop("patchkwargs"))
        return cls_dict

    @classmethod
    def from_dict(cls,cls_dict):
        return cls(**cls_dict)

