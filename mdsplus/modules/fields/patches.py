import matplotlib as mpl
from matplotlib.patches import Polygon,Wedge

class MagnetPatch(Polygon):
    def __init__(self,xy,closed=True,**kwargs):
        self.xy = xy
        super(MagnetPatch,self).__init__(xy,closed=closed,**kwargs)
    
    def to_dict(self):
        return {"xy":self.xy}

    @classmethod
    def from_dict(cls,cls_dict):
        xy = cls_dict.pop("xy")
        closed = cls_dict.pop("closed")
        return cls(xy,closed=closed,**cls_dict)
