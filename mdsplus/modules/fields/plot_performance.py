import numpy as np
import matplotlib.pyplot as plt

g_pts = []
n_procs = []
t = []
with open("test.out","r") as f:
    for i,line in enumerate(f):
        tmp = line.split()
        g_pts.append(tmp[9])
        n_procs.append(tmp[13])
        t.append(tmp[15])
t = np.array(t).reshape(10,10)
g_pts = np.array(g_pts).reshape(10,10)
n_procs = np.array(n_procs).reshape(10,10)
plt.plot(g_pts,t)
plt.legend(range(1,11))
plt.show()
