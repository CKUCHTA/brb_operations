import numpy as np

class spline:
    '''
    Master Spline Class
    defines basic data structures and basic functions
    '''
    def __init__(self, x_data, y_data):
        x_data = np.array(x_data,dtype=float)
        y_data = np.array(y_data,dtype=float)
        if len(x_data) != len(y_data):
            raise ValueError('Vectors to make splines for are not of equal lengths')
        self.x_data = x_data
        self.y_data = y_data
        self.dimension = len(x_data)
        self.d_x = (x_data[1:] - x_data[0:-1])  # length = dimension-1
        self.d_y = (y_data[1:] - y_data[0:-1])  # length = dimension-1
        
    def __call__(self, x_value, degree=0):
        return self._interpolate(x_value, degree)
        
    def _interpolate(self, x_value, deriv=0):
        return
        
class CubicSpline(spline):
    '''
    Spline Class for Cubic Splines
    Includes Cubic Spline _interpolate function
    '''
    def __init__(self, x_data, y_data):
        spline.__init__(self, x_data, y_data)
        
    def _interpolate(self,x_value,deriv=0):
        '''Interpolate to get the functional value to x_value'''
        x_value=np.array(x_value,dtype=float)
        y_int=np.zeros_like(x_value)
        for i in range(self.k.size-1):
            if i==0:
                tmploc=(x_value<=self.x_data[i+1])*(x_value>=self.x_data[i])
            else:
                tmploc=(x_value<=self.x_data[i+1])*(x_value>self.x_data[i])
            xs=x_value[tmploc]
            if xs.size==0:continue
            x_l, x_u = self.x_data[i], self.x_data[i + 1]
            y_l, y_u = self.y_data[i], self.y_data[i + 1]
            k_l, k_u = self.k[i], self.k[i + 1]
            d_x = (x_u - x_l) 
            t=(xs-x_l)/(d_x)
            a=k_l*d_x-(y_u-y_l)
            b=-k_u*d_x+(y_u-y_l)
            if deriv == 1:
                y_int[tmploc]=(y_u-y_l)/d_x+(1.-2.*t)*(a*(1-t)+b*t)/d_x+\
                            t*(1-t)*(b-a)/d_x
            elif deriv == 2:
                y_int[tmploc]=2.*(b-2.*a+(a-b)*3.*t)/d_x**2
            else:
                y_int[tmploc]=(1.-t)*y_l+t*y_u+t*(1.-t)*(a*(1.-t)+b*t)
        return y_int 
        
class NaturalCubicSpline(CubicSpline):
    def __init__(self, x_data, y_data):
        CubicSpline.__init__(self, x_data, y_data)
        '''Convience Pointers'''
        dimension=self.dimension
        d_x=self.d_x
        d_y=self.d_y
        
        '''Define Matrix'''
        A=np.matrix(np.zeros((dimension,dimension)))
        for i in range(0,dimension-1):
            A[i,i]=2*(1/d_x[i]+1/d_x[i-1])
            A[i+1,i]=1/d_x[i]
            A[i,i+1]=1/d_x[i]
        A[0,0]=2/d_x[0]
        A[-1,-1]=2/d_x[-1]
        
        '''Define the b vector'''
        b=np.matrix(np.zeros((dimension))).T
        b[0]=3*d_y[0]/d_x[0]**2
        b[-1]=3*d_y[-1]/d_x[-1]**2
        for i in range(1,dimension-1):
            b[i]=3*(d_y[i]/d_x[i]**2+d_y[i-1]/d_x[i-1]**2)
            
        '''Solve for Slopes'''
        k=np.linalg.solve(A,b)
        self.k=np.array(k)
        
class ClampedCubicSpline(CubicSpline):
    def __init__(self, x_data, y_data,yp=[0,0]):
        CubicSpline.__init__(self, x_data, y_data)
        '''Data check'''
        assert len(yp)==2,'yp must be a vector of length 2'
        '''Convience Pointers'''
        dimension=self.dimension
        d_x=self.d_x
        d_y=self.d_y
        '''Define Matrix'''
        A=np.matrix(np.zeros((dimension-2,dimension-2)))
        for i in range(0,dimension-2):
            A[i,i]=2*(1./d_x[i]+1./d_x[i-1])
            try:
                A[i+1,i]=1/d_x[i]
                A[i,i+1]=1/d_x[i]
            except: pass
        '''Define the b vector'''
        b=np.matrix(np.zeros((dimension-2))).T
        for i in range(0,dimension-2):
            b[i]=3.*(d_y[i]/d_x[i]**2+d_y[i+1]/d_x[i+1]**2)
        b[0]+=-1.*yp[0]/d_x[0]
        b[-1]+=-1.*yp[-1]/d_x[-1]
        '''Solve for Slopes and add clamped slopes'''
        k=np.linalg.solve(A,b)
        ktmp=np.zeros(dimension)
        ktmp[0]=yp[0]
        ktmp[-1]=yp[-1]
        ktmp[1:-1]=k.T
        k=ktmp
        self.k=np.array(k)
        
class FirstClampedCubicSpline(CubicSpline):
    '''
    Class for doing special clamped splines for 
    BC's on Cylindrical Magnetic Fitting Problem
    '''
    def __init__(self, x_data, y_data,yp=0,m=0):
        CubicSpline.__init__(self, x_data, y_data)
        '''Convience Pointers'''
        dimension=self.dimension
        d_x=self.d_x
        d_y=self.d_y
        x_data=self.x_data
        y_data=self.y_data
        A=np.matrix(np.zeros((dimension-1,dimension-1)))
        for i in range(0,dimension-2):
            A[i,i]=2*(1/d_x[i]+1/d_x[i-1])
            try:
                A[i+1,i]=1/d_x[i]
                A[i,i+1]=1/d_x[i]
            except: pass
        A[-1,-2]=2/d_x[-1]
        A[-1,-1]=4/d_x[-1]+1/x_data[-1]
        b=np.matrix(np.zeros((dimension-1))).T
        for i in range(0,dimension-2):
            b[i]=3*(d_y[i]/d_x[i]**2+d_y[i+1]/d_x[i+1]**2)
        b[0]+=-1*yp/d_x[0]
        b[-1]+=6*d_y[-1]/d_x[-1]**2+m**2*y_data[-1]/x_data[-1]**2
        k=np.linalg.solve(A,b)
        ktmp=np.zeros(dimension)
        ktmp[0]=yp
        ktmp[1:]=k.T
        k=ktmp
        self.k=np.array(k)
        
class SecondClampedCubicSpline(CubicSpline):       
     def __init__(self, x_data, y_data,m=1):
        CubicSpline.__init__(self, x_data, y_data)
        '''Convience Pointers'''
        dimension=self.dimension
        d_x=self.d_x
        d_y=self.d_y
        x_data=self.x_data
        y_data=self.y_data
        A=np.matrix(np.zeros((dimension,dimension)))
        for i in range(0,dimension-1):
            A[i,i]=2*(1/d_x[i]+1/d_x[i-1])
            A[i+1,i]=1/d_x[i]
            A[i,i+1]=1/d_x[i]
        A[0,0]=2
        A[0,1]=1
        A[-1,-2]=2/d_x[-1]
        A[-1,-1]=4/d_x[-1]+1/x_data[-1]
        b=np.matrix(np.zeros((dimension))).T
        for i in range(1,dimension-1):
            b[i]=3*(d_y[i]/d_x[i]**2+d_y[i-1]/d_x[i-1]**2)
        b[0]+=3*y_data[1]/x_data[1]
        b[-1]+=6*d_y[-1]/d_x[-1]**2+m**2*y_data[-1]/x_data[-1]**2
        k=np.linalg.solve(A,b)
        self.k=np.array(k)

class QuarticSpline(spline):
    '''
    Spline Class for Quartic Splines
    Includes Quartic Spline _interpolate function
    '''
    def __init__(self, x_data, y_data):
        spline.__init__(self, x_data, y_data)
        assert self.d_x.std()<1e-12, \
            'x_data must be equally spaced for Quartic Splines'
        self.d_x=self.d_x.mean()
    def _interpolate(self,x_value,deriv=0):
        deriv=int(deriv)
        '''Interpolate to get the functional value to x_value'''
        x_value=np.array(x_value,dtype=float)
        y_int=np.zeros_like(x_value)
        for i in range(1,self.z.size):
            if i==1:
                tmploc=(x_value<=self.x_data[i])*(x_value>=self.x_data[i-1])
            else:
                tmploc=(x_value<=self.x_data[i])*(x_value> self.x_data[i-1])
            xs=x_value[tmploc]
            if xs.size==0:continue
            x_l, x_u = self.x_data[i-1], self.x_data[i]
            y_l, y_u = self.y_data[i-1], self.y_data[i]
            z_l, z_u = self.z[i-1], self.z[i]
            C        = self.C[i-1]
            d_x = self.d_x
            if deriv == 0:
                y_int[tmploc]=(z_u/(24*d_x)*(xs-x_l)**4
                              -z_l/(24*d_x)*(x_u-xs)**4
                              +(-z_u/24*d_x**2+y_u/d_x)*(xs-x_l)
                              +( z_l/24*d_x**2+y_l/d_x)*(x_u-xs)
                              +C*(xs-x_l)*(x_u-xs))
            elif deriv == 1:
                y_int[tmploc]=(z_u/(6*d_x)*(xs-x_l)**3
                              +z_l/(6*d_x)*(x_u-xs)**3
                              +(-z_u/24*d_x**2+y_u/d_x)
                              -( z_l/24*d_x**2+y_l/d_x)
                              +C*(x_l+x_u-2*xs))
            elif deriv == 2:
                y_int[tmploc]=(z_u/(2*d_x)*(xs-x_l)**2
                              -z_l/(2*d_x)*(x_u-xs)**2
                              -2*C)
            elif deriv == 3:
                y_int[tmploc]=(z_u/(d_x)*(xs-x_l)
                              +z_l/(d_x)*(x_u-xs))
            elif deriv == 4:
                y_int[tmploc]=(z_u/(d_x)-z_l/(d_x))
        return y_int 

class AzFitQuarticSpline(QuarticSpline):
    def __init__(self, x_data, y_data,m=0):
        QuarticSpline.__init__(self, x_data, y_data)
        np.set_printoptions(3)
        A,b=self._gen_Matrix(m=m)
        h=1.0*self.d_x
        xn=1.0*self.x_data[-1]
        if m<3:
            A=A[:,1:]
            A=np.delete(A,0,axis=0)
            b=np.delete(b,0)
            if m!=1:
                A[0:-1,0]+=-h**2/12.
                A[-1,0]+=-(2+h/xn)*h/24
                b[0:-1]+=-2*self.d_y[0]/h
                b[-1]+=-(2+h/xn)*self.d_y[0]/h**2
        else:
            A[1:-1,0]+=h**2/2.
            A[-1,0]+=(2+h/xn)*h/4.
            A[0,0]=h**2/8
            A[0,1]=h**2/24.
            b[0]=self.d_y[0]/h
        
        k=np.linalg.solve(A,b)
        self.z=np.zeros(self.dimension)
        self.C=np.zeros(self.dimension-1)
        if m<3:
            self.z[1:]=k
            if m!=1:
                self.C[0]=h/24*self.z[1]-self.d_y[0]/h**2
        else:
            self.z=k
            self.C[0]=-self.z[0]*h/4
        for i in range(1,len(self.C)):
            self.C[i]=self.C[i-1]-self.z[i]/2*h

    def _gen_Matrix(self,m=0):
        '''
        Makes the solving Matrix for the Magnetic Fitting problem with
        conditions for $x_i>x_0$
        '''
        h=1.0*self.d_x
        xn=1.0*self.x_data[-1]
        '''Define Matrix'''        
        A=np.zeros((self.dimension,self.dimension))
        for i in range(1,self.dimension-1):
            A[i,i]+=h**2/2.
            A[i,i-1]+=-h**2/24.
            A[i,i+1]+=h**2/24.
            for j in range(1,i):
                A[i,j]+=h**2
        A[-1,-1]+=h/2.+h**2/(8*xn)
        A[-1,-2]+=-h**2/(24*xn)
        for j in range(1,self.dimension-1):
            A[-1,j]+=(2+h/xn)*h/2
        
        '''Define the b vector'''
        b=np.zeros((self.dimension))
        b[1:-1]=(self.y_data[2:]
                -2*self.y_data[1:-1]
                +self.y_data[0:-2])/h
        b[-1]=(m**2*self.y_data[-1]/xn**2
              -(self.d_y[-1])/(h*xn))
        return A,b


if __name__=='__main__':
    n=6
    m=1
    x1=np.linspace(0,10,n)
    y1=np.zeros(n)
    y1[-1]=1
    x2=np.linspace(0,10,500)
    for ns in range(n):
        y1=np.zeros(n)
        y1[ns]=1
        for m in [0,1,2,3]:
            print(ns,m)
            s=AzFitQuarticSpline(x1,y1,m)
            print((s(x2,2)+1./x2*s(x2,1)-(1.*m)**2/x2**2*s(x2))[0])
            print((s(x2,2)+1./x2*s(x2,1)-(1.*m)**2/x2**2*s(x2))[-1])
    #    s=SecondClampedCubicSpline(x1,y1,m)
    import matplotlib.pyplot as plt
    plt.close('all')
    plt.plot(x1,y1,'o')
    plt.plot(x1,0*x1,'--k')
    plt.plot(x2,s(x2),label='s')
    plt.plot(x2,s(x2,1),label='ds')
    plt.plot(x2,s(x2,2),label="dds")
    plt.plot(x2,s(x2,3),label="ddds")
    plt.plot(x2,s(x2,2)+1./x2*s(x2,1)-(1.*m)**2/x2**2*s(x2),label='grad s')
    print((s(x2,2)+1./x2*s(x2,1)-(1.*m)**2/x2**2*s(x2))[-1])
    plt.legend(loc=0)
    plt.show()
