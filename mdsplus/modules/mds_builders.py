import MDSplus as mds
import logging


def put_wrapper(node, data, err=None, units=None):
    '''
    Wrapper for mds.putData calls when setting units or error fields.
    
    params:
    node    = The MDSplus node to insert data into.
    data    = The compiled data to insert into the node.
    err     = The compiled error to insert into the node.
    units   = The units of the error and data.
    '''
    if not isinstance(node, mds.TreeNode):
        raise TypeError("Node {node} is not an MDSplus TreeNode.".format(node=str(node)))
    
    data.setUnits(units)
    if err is not None:
        err.setUnits(units)
    data.setError(err)
    node.putData(data)

def get_error_refs(*nodes):
    '''
    Return TDI data instances that get the error of a specified node.

    params:
    nodes   = Any number of node objects that we want the error of.

    returns:
    errors  = Either a tuple or just the compiled TDI data call to get the error of a node.
    '''
    for node in nodes:
        if type(node) != mds.TreeNode:
            raise TypeError("Node {node} is not an MDSplus TreeNode.".format(node=str(node)))
    
    errors = tuple([mds.Data.compile("ERROR_OF($1)", nodes) for nodes in nodes])
    if len(errors) == 1:
        return errors[0]
    else:
        return errors

def get_node_refs(ref_node, *node_names):
    '''
    Return TreeNode objects for specified node names in *args, with respect to ref_node.

    Parameters
    ----------
    ref_node : mds.TreeNode
        Ancestor node for all nodes in node_names.
    node_names : *str
        Any number of strings that refer to nodes under the ancestor.

    Returns
    -------
    nodes : str or tuple[str]
        The nodes referenced by the strings.
    '''
    logging.debug("Getting nodes called {} under node {}.".format(list(node_names), ref_node))
    if not isinstance(ref_node, mds.TreeNode):
        raise TypeError("Node {node} is not an MDSplus TreeNode.".format(node=str(ref_node)))
    for node_name in node_names:
        if not isinstance(node_name, str):
            raise TypeError("Type of node name '{name}' is not str.".format(name=str(node_name)))
    
    nodes = tuple([ref_node.getNode(node_name) for node_name in node_names])
    if len(nodes) == 1:
        return nodes[0]
    else:
        return nodes

def traverse_tree(node, ntabs=0):
    """
    Print out all nodes that are descendents by recursively calling this and tabbing in each time.

    params:
    node    = An MDS node to print descendents of.
    ntabs   = Number of tabs to move the printed node over.
    """
    print("\n")
    desc = node.getDescendants()
    print(ntabs*"\t"+node.getPath())
    print(ntabs*"\t"+ "NODE DTYPE: {0}".format(node.getDtype()))
    print(ntabs*"\t"+ "NODE WRITE ONCE: {0}".format(node.isWriteOnce()))
    print(ntabs*"\t"+ "NODE COMPRESS ON PUT: {0}".format(node.isCompressOnPut()))
    if len(desc) == 0:
        return
    else:
        for n in desc:
            traverse_tree(n,ntabs=ntabs+1)

def set_write_once(node, is_write_once=True):
    """
    Change a node and all it's descendents write once attribute.

    params:
    node    = An MDS node to change to write once including descendents.
    """
    desc = node.getDescendants()
    try:
        node.setWriteOnce(is_write_once)
    except mds.mdsExceptions.MDSplusException as e:
        logging.exception("Error trying to set writability of node '{}' to value '{}'.".format(str(node), is_write_once))
        raise

    if len(desc) == 0:
        return
    else:
        for n in desc:
            set_write_once(n,is_write_once=is_write_once)

def make_tree_write_once(tree):
    """
    Set all nodes in a tree to write once.

    params:
    tree    = Tree to set to write once.
    """
    top = tree.getNode("\\top")
    set_write_once(top)
    return

def unlock_tree(tree):
    """
    Set write once of all nodes in tree to false.

    params:
    tree    = Tree to remove write once from.
    """
    top = tree.getNode("\\top")
    set_write_once(top,is_write_once=False)
    return

def unlock_nodes(nodes):
    """
    Set a list of nodes to write once.

    params:
    nodes   = List of nodes to unlock.
    """
    for node in nodes:
        node.setWriteOnce(False)
    return

def lock_nodes(nodes):
    for node in nodes:
        node.setWriteOnce(True)
    return

def to_mds_datatype(data):
    """
    Change some data to the MDSplus datatype that corresponds to it.

    params:
    data    = Variable to change to MDSplus datatype.
    """
    if isinstance(data, str):
        logging.debug("Changing {d} from type 'str' to compiled string using MDSplus.Data.Compile.".format(d=data))
        return mds.Data.compile(data)
    elif isinstance(data, float):
        logging.debug("Changing {d} from type 'float' to 'FLOAT32'.".format(d=data))
        return mds.Float32(data)
    elif isinstance(data, int):
        logging.debug("Changing {d} from type 'int' to 'INT32'.".format(d=data))
        return mds.Int32(data)
    else:
        raise TypeError("Can't change {d} of type '{t}' to MDSplus datatype.".format(d=data, t=type(data)))

