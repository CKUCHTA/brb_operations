#!/usr/bin/python
import numpy as np
import scipy.signal as sg
from scipy.interpolate import griddata
from scipy.ndimage import gaussian_filter
from scipy.integrate import cumtrapz
import splines
import matplotlib.pyplot as plt
from scipy.spatial import Delaunay, ConvexHull

"""data processing section"""
def smooth(array,pts,axis=0,win_type="boxcar",mode="reflect"):
    """Smooth an ndarray of signals over any axis.
    pts is (window width - 1) /2 so window is 2*pts+1 wide
    """
    ## Make sure the desired axis is in bounds
    shape_in = array.shape
    assert axis < len(shape_in), "axis value greater than input array dimensions"
    ## build kernel and broadcast to match input dimensions
    if win_type == "gaussian":
        window = np.exp(-3*(np.linspace(-pts,pts,2*pts+1,endpoint=True)/(2*pts+1))**2)
        window = window/np.sum(window)
    else:
        window = np.ones(2*pts+1)/float(2*pts+1)
    for dim,length in enumerate(shape_in):
        if dim == axis:
            continue
        else:
            window = np.expand_dims(window,dim)
    if mode == "reflect":
        ## build slices to reflect signals along proper axis
        ## also build final_slice to remove these reflections in the end
        length = shape_in[axis]
        rev_slice = tuple([slice(2*pts,None,-1) if i == axis else slice(None) for i,j in enumerate(shape_in)])
        tack_slice = tuple([slice(-1,-(2*pts+2),-1) if i == axis else slice(None) for i,j in enumerate(shape_in)])
        final_slice = tuple([slice(2*pts+1,2*pts+1+length) if i == axis else slice(None) for i,j in enumerate(shape_in)])
        ## concatenate reflections with input array
        tmp = np.concatenate((array[rev_slice],array,array[tack_slice]),axis=axis)
        return sg.fftconvolve(tmp,window,'same')[final_slice]
    else: 
        return sg.fftconvolve(array,window,mode)

def smooth2D(*args,**kwargs):
    """*args should be any number of 2D regular gridded numpy arrays to smooth"""
    sigma = float(kwargs.pop("sigma",2.0))
    truncate = float(kwargs.pop("truncate",4.0))

    qts_filt = []
    for q in args:
        v = q.copy()
        v[~np.isfinite(q)] = 0
        vv = gaussian_filter(v,sigma,mode="constant",truncate=truncate)
        w = np.ones_like(q)
        w[~np.isfinite(q)] = 0
        ww = gaussian_filter(w,sigma,truncate=truncate)
        q_filter = vv/ww
        q_filter[~np.isfinite(q)] = np.nan
        qts_filt.append(q_filter)

    if len(qts_filt) == 1:
        return qts_filt[0]
    else:
        return tuple(qts_filt)

# need to remove groupby in this function
#def spikeoff(sigin,threshold,ns=11,plotROI=False):
#    tmpsig = np.copy(sigin)
#    threshold = threshold**2
#    last = 5
#    for k in range(1,last+1):
#        sigfilt = smooth(tmpsig,ns);
#        sch = (tmpsig-sigfilt)**2/np.mean(sigfilt**2)
#        sch[0] = 0
#        sch[-1] = 0
#        fac = (5**last)/5**k
#        spike_idx = np.where(sch>=threshold*fac)[0]
#        good_idx = np.where(sch<threshold*fac)[0]
#        sigout = np.copy(tmpsig)
#        if not spike_idx.size:
#            pass
#        else:
#            sigout[spike_idx] = np.interp(spike_idx,good_idx,tmpsig[good_idx])
#            if plotROI:
#                fig,ax = plt.subplots()
#                ax.plot(tmpsig,"b")
#                ax.plot(sigfilt,"g")
#                #ax.plot(sch,"r")
#                roi_bot,roi_top = ax.get_ylim()
#                for key,g in groupby(enumerate(spike_idx),lambda (i,x): i-x):
#                    roi = map(itemgetter(1),g)
#                    roi_left,roi_right = min(roi)-1,max(roi)+1
#                    roi = Rectangle((roi_left,roi_bot),roi_right-roi_left,roi_top-roi_bot,fc="y",ec="none",alpha=.35)
#                    ax.add_patch(roi)
#                plt.show()
#        tmpsig = np.copy(sigout)
#    return sigout

def rebin(data,pts=100,axis=0,data_sd=None):
    inshape = data.shape
    axpts = inshape[axis]
    m,pad = divmod(axpts,pts)
    if pad != 0:
        m += 1
        pad = m*pts % axpts
        pad_width = tuple([(0,pad) if i == axis else (0,0) for i in range(len(inshape))])
        output = np.pad(data,pad_width,"constant",constant_values = np.nan)
        if data_sd is not None:
            output_sd = np.pad(data_sd,pad_width,"constant",constant_values = np.nan)
    else:
        output = data
        if data_sd is not None:
            output_sd = data_sd
    outshape = list(inshape)
    outshape[axis] = pts
    outshape.insert(axis+1,m)
    outshape = tuple(outshape)
    output = output.reshape(outshape)
    if data_sd is not None:
        output_sd = output_sd.reshape(outshape)
        return np.nanmean(output,axis=axis+1),np.sqrt(np.nansum(output_sd**2,axis=axis+1))
    else:
        return np.nanmean(output,axis=axis+1),np.nanstd(output,axis=axis+1)

class Fit(object):
    """convenient fit class"""
    def __init__(self,x,y,basis="poly",num_fns=4,nodes=4,plotit=False):
        self.x = x
        self.y = y
        self.linCom = svd_fit(self.x,self.y,basis=basis,num_fns=num_fns,nodes=nodes,plotit=plotit)
        self.basis_fns = self.linCom.basis_fns
        self.coeffs = self.linCom.coeffs
        self.chisq = get_chisq(self.x,self.y,self.linCom)
        self.redchisq = get_reducedchisq(self.x,self.y,self.linCom)

class LinearCombination(object):
    """Linear combination of functions and coefficients for simple, callable interpolation"""
    def __init__(self,basis_fns,coeffs):
        self.basis_fns = basis_fns
        self.coeffs = coeffs
    def __call__(self,x,deriv=0):
        if isinstance(self.basis_fns[0],(splines.NaturalCubicSpline,splines.FirstClampedCubicSpline,splines.SecondClampedCubicSpline)):
            y_int = np.zeros_like(x)
            for i in range(len(self.basis_fns)):
                y_int += self.coeffs[i]*self.basis_fns[i](x,deriv)
            return y_int        
        else:
            y_int = np.zeros_like(x)
            for i in range(len(self.basis_fns)):
                y_int += self.coeffs[i]*self.basis_fns[i](x)
            return y_int        

def poly_fit(x,y,num_fns=4):
    """Perform linear least square fit to data x,y using num_fns polynomial
    basis functions.
    """
    n = num_fns
    m = len(y)
    basis_fns = [(lambda z,i=i: z**i) for i in range(n)]
    A,c = _svd_comp(x,y,basis_fns)
    return LinearCombination(basis_fns,c)

def spline_fit(x,y,nodes=4,spline_type="natural"):
    """Perform linear least square fit to data x,y using splines.
    nodes specifies number of nodes or specific node locations
    """
    assert spline_type in ["natural","clamped1","clamped2"], "Unsupported spline type"
    spline_dict = {"natural":splines.NaturalCubicSpline,"clamped1":splines.FirstClampedCubicSpline,"clamped2":splines.SecondClampedCubicSpline}
    if type(nodes) is int:
        nodes = np.linspace(x.min(),x.max(),nodes)
    else:
        assert hasattr(nodes,iter), "Nodes are not iterable"
        ## use the nodes passed in
    n = len(nodes)
    m = len(y)
    basis_fns = []
    for i in range(n):
        z = np.zeros(n)
        z[i] = 1.0
        basis_fns.append(spline_dict[spline_type](nodes,z))
    A,c = _svd_comp(x,y,basis_fns)
    return LinearCombination(basis_fns,c)

def _svd_comp(x,y,basis_fns):
    """Compute linear least squares fit with given basis functions 
    for data x,y.
    """
    n = len(basis_fns)
    m = len(y)
    A = np.zeros((m,n))
    for i in range(m):
        for j in range(n):
            A[i,j] = basis_fns[j](x[i])
    (u,s,vt) = np.linalg.svd(A)
    Sinv = np.zeros((n,m))
    s[ s<1.0e-10 ] = 0.0
    s[ s>=1.0e-10 ] = 1.0/s[ s>=1.0e-10]
    Sinv[:n,:n] = np.diag(s)
    c = np.dot(Sinv,u.T)
    c = np.dot(vt.T,c)
    c = np.dot(c,y)
    return A,c

def svd_fit(x,y,basis="poly",num_fns=4,nodes=4,plotit=False):
    """Wrapper function for linear least squares fitting with many basis sets
    Returns 
    linCom (LinearCombination instance): object with attributes basis_fns and coeffs
    """
    assert basis in ["poly","bessel","legendre","spline_natural","spline_clamped1","spline_clamped2"], "unsupported basis function"
    basis_dict = {"poly":poly_fit,"spline":spline_fit}
    if "spline" in basis:
        linCom = basis_dict["spline"](x,y,nodes=nodes,spline_type=basis.replace("spline_",""))
    else:
        linCom = basis_dict[basis](x,y,num_fns=num_fns)
    if plotit:
        xnew = np.linspace(x.min(),x.max(),1000)
        ynew = linCom(xnew)
        fig,[ax1,ax2] = plt.subplots(2,sharex=True)
        ax1.plot(x,y,"o")
        ax1.plot(xnew,ynew)
        for i in range(len(linCom.basis_fns)):
            ax2.plot(xnew,linCom.basis_fns[i](xnew))
    return linCom

def get_chisq(x,y,linCom,weights=None):
    """Return chi squared for fit"""
    if weights == None:
        weights = np.ones_like(x)
    return np.sum(np.array([(wi*(linCom(xi)-yi))**2 for xi,yi,wi in zip(x,y,weights)]))

def get_reducedchisq(x,y,linCom,weights=None):
    """Return reduced chi squared for fit"""
    if weights == None:
        weights = np.ones_like(x)
    return 1.0/(len(x)-len(linCom.coeffs))*get_chisq(x,y,linCom,weights=weights)

"""Plotting helpers"""
def streams(ax,xx,yy,u,v,base_map=False):
    """Plot streamlines of vector field u,v on x,y grid"""
    x = np.linspace(xx.min(), xx.max(), 50)
    y = np.linspace(yy.min(), yy.max(), 50)
    xi, yi = np.meshgrid(x,y)
    #then, interpolate your data onto this grid:
    px = xx.flatten()
    py = yy.flatten()
    pu = u.flatten()
    pv = v.flatten()

    gu = griddata(zip(px,py), pu, (xi,yi))
    gv = griddata(zip(px,py), pv, (xi,yi))

    #now, you can use x, y, gu, gv and gspeed in streamplot:
    if base_map:
        xx,yy = ax(xx,yy)
        xi,yi = ax(xi,yi)

#    ax.plot(xx,yy,'-k',alpha=0.3)
#    ax.plot(xx.T,yy.T,'-k',alpha=0.3)
#    ax.plot(xi,yi,'-b',alpha=0.1)
#    ax.plot(xi.T,yi.T,'-b',alpha=0.1)
    c = ax.streamplot(x,y,gu,gv, density=.75,linewidth=2, color="k",zorder=2)

def rect_pol_flux(R,Z,FR,FZ):
    dR = R[0,1]-R[0,0]
    Rrev = R[:,::-1]
    Frev = FZ[:,::-1]
    pol_flux = cumtrapz(Rrev*Frev,dx=dR,axis=1,initial=0)
    pol_flux = pol_flux[:,::-1]
    return pol_flux

def polar_pol_flux(R,Theta,FR,FTheta):
    dR = np.sqrt( (R[0,2]*np.sin(Theta[0,2])-R[0,1]*np.sin(Theta[0,1]))**2 -
            (R[0,2]*np.cos(Theta[0,2])-R[0,1]*np.cos(Theta[0,1]))**2 )
    Rrev = R[:,::-1]
    Frev = FTheta[:,::-1]
    Rrev[~np.isfinite(Rrev)]=0
    Frev[~np.isfinite(Frev)]=0
    pol_flux = cumtrapz(Rrev*Frev,dx=dR,axis=1,initial=0)
    pol_flux = pol_flux[:,::-1]
    return pol_flux*np.sin(Theta)


def regular_grid(xx,yy,*args,**kwargs):
    nx = kwargs.pop("nx",200)
    ny = kwargs.pop("ny",200)
    xi = kwargs.pop("xi",None)
    yi = kwargs.pop("yi",None)
    method = kwargs.pop("method","linear")
    """ interpolate irregularly gridded data onto regular grid."""
    if xi is not None and yi is not None:
        pass
    else:
        x = np.linspace(xx.min(), xx.max(), nx)
        y = np.linspace(yy.min(), yy.max(), ny)
        xi, yi = np.meshgrid(x,y,indexing="ij")

    #then, interpolate your data onto this grid:
    points = np.vstack((xx.flatten(),yy.flatten())).T
    zs = []
    for z in args:
        zi = griddata(points,z.flatten(),(xi,yi),method=method)
        zs.append(zi)
    
    return (xi,yi) + tuple(zs)
