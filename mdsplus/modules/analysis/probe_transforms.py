import numpy as np

###########
## Machine Coordinate System Conventions
## 
## if standing on mezzanine looking southward
##                ^ x
##                |
##     (S pole) o |---> z (N pole)
##              y
##
## theta: angle from z axis (co-latitude)
## phi: angle from x axis in x-y (equatorial) plane (longitude or toroidal angle)
##
## Note: Port naming scheme uses latitude (angle from equator)
##
###########


def get_xz(r0,theta0,L,dtheta,probe_pos=0):
    ## r0,theta0 is location of pivot point for probe (theta0 in degrees)
    ## dtheta is angle of rotation away from radial chord (degrees)
    ## L is length from pivot to tip of probe
    ## probe_pos is relative position array for probe
    x0,z0 = r0*np.sin(np.pi/180.0*theta0),r0*np.cos(np.pi/180.0*theta0)
    delta, beta = np.meshgrid(L-probe_pos,np.deg2rad(theta0+dtheta))
    x = x0 - delta*np.sin(beta)    
    z = z0 - delta*np.cos(beta)    
    return x,z

def get_normvecs(r0,theta0,L,dtheta,probe_pos=0):
    x0,z0 = r0*np.sin(np.pi/180.0*theta0),r0*np.cos(np.pi/180.0*theta0)
    x,z = get_xz(r0,theta0,L,dtheta,probe_pos=probe_pos)
    dx = x  - r0*np.sin(np.deg2rad(theta0))
    dz = z - r0*np.cos(np.deg2rad(theta0))
    return np.dstack((-dx,np.zeros_like(dx),dz))/(np.sqrt(dx**2 + dz**2)[:,:,np.newaxis])
