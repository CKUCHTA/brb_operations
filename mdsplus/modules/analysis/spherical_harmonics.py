from scipy.special import lpmv
import numpy as np
from scipy.misc import factorial

def Ylm(theta, phi, L=0, m=0, deriv=0):
    x = np.cos(theta)

    if deriv == 0:
        norm = np.sqrt((2 * L + 1) / 2 * factorial(L - m) / factorial(L + m)) / np.sqrt(np.pi)

        if m == 0:
            norm /= np.sqrt(2.0)

        plm = norm*lpmv(m, L, x)


        if m == 0:
            return plm
        else:
            return plm*np.sin(m*phi), plm*np.cos(m*phi)
    elif deriv == 1:
        """ 
        useful link: http://mathworld.wolfram.com/AssociatedLegendrePolynomial.html
        """
        norm = np.sqrt((2 * L + 1) / 2 * factorial(L - m) / factorial(L + m)) / np.sqrt(np.pi)

        if m == 0:
            norm /= np.sqrt(2.0)
        if L > 0:
            tmp1 = (L+1)*(L+m)*lpmv(m, L-1, x)
            tmp2 = -L*(L-m+1)*lpmv(m, L+1, x)
            return -np.sin(theta)*norm * (tmp1 + tmp2) / (1-x**2) / (2*L+1)
            #tmp1 = L * x * lpmv(m, L, x)
            #tmp2 = (L + m) * lpmv(m, L - 1, x)
            #return norm * (tmp1 - tmp2) / np.sqrt(1.0 - x**2)
        else:
            return np.zeros_like(theta)
    else:
        return None

if __name__ == "__main__":

    import matplotlib.pyplot as plt
    phi = 0.0
    theta = np.linspace(0, np.pi)

    y = Ylm(theta, phi, L=1)
    plt.plot(theta, y)
    plt.show()
