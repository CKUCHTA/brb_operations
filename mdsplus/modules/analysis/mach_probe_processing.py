from matplotlib import rcParams
import mdsplustools.core as mpt
import numpy as np
import matplotlib.pyplot as plt
# import plottingtools.core as ptools
rcParams['figure.autolayout'] = True

"""Note for later.  For large lists, import iterools and use iterools.izip
to use an iterator"""


def find_time_point(t, t0):
    """

    Parameters
    ----------
    t : ndarray
        time array
    t0 : float
        value to find closest to in t

    Returns
    -------
    i : int
        index of closest value in t to t0
    """
    return np.argmin(np.abs(t - t0))


def calculate_calibration(time, w_current, w_current_sd, e_current,
                          e_current_sd, t_range, plotit=False):
    """
    Calculates the mach probe offset calibration.

    Parameters
    ----------
    time : ndarray
        time array
    w_current : ndarray
        currents for the west calibration direction.  Shape is 2 x n.
    w_current_sd : ndarray
        std dev for west currents. Shape is 2 x n
    e_current : ndarray
        currents for the east calibration direction. Shape is 2 x n.
    e_current_sd: ndarray
        std dev for east currents.  Shape is 2 x n.
    t_range: tuple
        length of 2 specifying the two time end points to average calibration.
    plotit : bool
        generates plot if plotit is True

    Returns
    -------
    cal : float
        mach number calibration offset
    cal_sd : float
        mach number std dev calibration offset
    """

    t1, t2 = t_range
    west_flow, west_flow_sd = calculate_flow(w_current, w_current_sd)
    east_flow, east_flow_sd = calculate_flow(e_current, e_current_sd)

    calibration = (west_flow + east_flow) / 2.0
    calibration_sd = 0.5 * np.sqrt(west_flow_sd ** 2 + east_flow_sd ** 2)
    i1 = find_time_point(time, t1)
    i2 = find_time_point(time, t2)

    temp = calibration_sd[i1:i2]
    i = np.where(temp > 0.0)
    weights = np.zeros(len(temp))
    weights[i] = 1. / temp[i] ** 2

    numerator = 0.0
    denominator = 0.0
    # for idx, w in enumerate(weights):
    for w, c in zip(weights, calibration[i1:i2]):
        # numerator += w * calibration[idx + i1]
        numerator += w * c
        denominator += w
    cal = numerator / denominator
    cal_sd = np.sqrt(1. / denominator)

    if plotit:
        fig, ax = plt.subplots()
        ax.plot(time, west_flow)
        ax.plot(time, east_flow)
        ax.plot(time, calibration)
        low, high = ax.get_ylim()
        ax.plot([time[i1]] * 2, [low, high], '--k')
        ax.plot([time[i2]] * 2, [low, high], '--k')
        plt.show()

    return cal, cal_sd


def average_time_trace(data_list, data_sd_list):
    """

    Calculates weighted averages for the time traces (and standard deviations)
    stored in data_list and data_sd_list. Weights are 1/(standard deviation).

    Parameters
    ----------
    data_list : list
        list containing 1D ndarray time traces
    data_sd_list : list
        list containing 1D ndarray stadanrd deviation time traces

    Returns
    -------
    data : ndarray
        weighted averaged time trace from the time traces stored in data_list
    data_sd :
        standard deviation calculated from the weighted average

    """
    weights = [np.zeros_like(x) for x in data_list]
    for idx, sd in enumerate(data_sd_list):
        i = np.where(sd > 0.0)
        weights[idx][i] = 1. / sd[i]**2

    numerator = 0.0
    denominator = 0.0
    # for idx, w in enumerate(weights):
    for w, d in zip(weights, data_list):
        # numerator += w * data_list[idx]
        numerator += w * d
        denominator += w
    data = numerator / denominator
    data_sd = np.sqrt(1. / denominator)

    return data, data_sd


def average_isat_traces(current_list, current_sd_list):
    # type: (np.ndarray, np.ndarray) -> (np.ndarray, np.ndarray)
    # Build weight vectors
    """
    Computes a weighted average / std dev mach probe current for each probe
    face.

    Parameters
    ----------
    current_list : list
        Each element is a 2 x n ndarray containing mach probe currents for
        each face.
    current_sd_list: list
        Each element is a 2 x n ndarray containing mach probe std dev currents
        for each face.

    Returns
    -------
    current : ndarray
        2 x n ndarray (same shape as elements in current_list)
        Contains weighted averaged currents for each probe face.
    current_sd : ndarray
        2 x n ndarray
        Contains weighted average std dev currents for each probe face.
    """
    weights = [np.zeros_like(x) for x in current_sd_list]
    for idx, sd in enumerate(current_sd_list):
        i = np.where(np.logical_and(sd[0, :] > 0.0, sd[1, :] > 0.0))
        weights[idx][0, i] = 1. / sd[0, i] ** 2
        weights[idx][1, i] = 1. / sd[1, i] ** 2

    # Calculate weighted mean
    numerator = 0.0
    denominator = 0.0
    for w, cur in zip(weights, current_list):
        # numerator += w * current_list[idx]
        numerator += w * cur
        denominator += w

    current = numerator / denominator
    current_sd = np.sqrt(1. / denominator)

    return current, current_sd


def retrieve_data(shot, mach_probe):
    """
    Retrieves mach probe data from mpdx_proc MDSplus tree for shot number = shot.
    Parameters
    ----------
    shot : int
        shot number to retrieve data for
    mach_probe : str
        mach probe tag --> mp<number>

    Returns
    -------
    time : ndarray
        time array
    cur : ndarray
        2d array with currents for each mach probe face
    cur_sd: ndarray
        2d array with std dev currents for each mach probe face
    """
    tags = ["i_{0:s}".format(mach_probe), "i_{0:s}_sd".format(mach_probe)]
    data = mpt.getMDSProcData(shot, tag_list=tags)
    time = data[tags[0]][0]
    cur = data[tags[0]][1]
    cur_sd = data[tags[1]][1]

    return time, cur, cur_sd


def process_mach_probe(cur, cur_sd, cal=None, cal_sd=None, thres=0.005):
    """
    Calculates mach number time trace (with given calibration)

    Parameters
    ----------
    cur : ndarray
        2 x n shape array with currents for both mach probe faces
    cur_sd : ndarray
        2 x n shape array with std dev currents for both mach probe faces
    cal : float
        mach number offset calibration; if None, no calibration is applied
    cal_sd : float
        st dev mach number offset calibration; if None, no calibration is applied
    thres: float
        threshold current in A for calculating flow

    Returns
    -------
    flow : ndarray
        mach number time trace
    flow_sd: ndarray
        std dev mach number number time trace

    Notes
    -----
    Minus sign is to get the direction right.
    If probe direction is 'West', then a plasma flowing west will
    have the west facing probe downstream.  The upstream face (east)
    will have a higher current.
    """
    flow, flow_sd = calculate_flow(cur, cur_sd, thres=thres)

    if cal is not None and cal_sd is not None:
        # apply flow calibration here
        flow, flow_sd = apply_calibration(flow, flow_sd, cal, cal_sd)
    return -flow, flow_sd


def remove_offset(cur, cur_sd, position='end'):
    """
    Removes digitizer offset

    Parameters
    ----------
    cur : ndarray
        2 x n shape array with currents for both mach probe faces
    cur_sd : ndarray
        2 x n shape array with std dev currents for both mach probe faces
    position : str
        'end' --> subtracts the mean from the last 500 points
        'start' --> subtracts the mean from the first 500 points
        anything else defaults to 'end'

    Returns
    -------
    cur : ndarray
        2 x n shape array with currents for both mach probe faces with offset
        correction
    cur_sd : ndarray
        2 x n shape array with std dev currents for both mach probe faces with
        offset correction

    Notes
    -----
    Assume 5 kHz digitization, remove average from the last 0.1 seconds
    f shot.  Average over the first or last 500 points.
    """
    if position == 'start':
        cur[0, :] -= np.mean(cur[0, 0:500])
        cur[1, :] -= np.mean(cur[1, 0:500])

        cur_sd[0, :] = np.sqrt(cur_sd[0, :] ** 2 + np.std(cur[0, 0:500]) ** 2)
        cur_sd[1, :] = np.sqrt(cur_sd[1, :] ** 2 + np.std(cur[1, 0:500]) ** 2)
    else:
        cur[0, :] -= np.mean(cur[0, -500:])
        cur[1, :] -= np.mean(cur[1, -500:])

        cur_sd[0, :] = np.sqrt(cur_sd[0, :] ** 2 + np.std(cur[0, -500:]) ** 2)
        cur_sd[1, :] = np.sqrt(cur_sd[1, :] ** 2 + np.std(cur[1, -500:]) ** 2)

    return cur, cur_sd


def apply_calibration(flow, flow_sd, cal, cal_sd):
    """
    Applies mach probe offset calibration to flow.  Propagates error through.

    Parameters
    ----------
    flow : ndarray
        mach number time trace
    flow_sd: ndarray
        std dev mach number time trace
    cal : float
        mach number offset calibration
    cal_sd : float
        std dev mach number offset calibration

    Returns
    -------
    flow : ndarray
        mach number time trace with calibration applied
    flow_sd: ndarray
        std dev mach number time trace with calibration applied
    """
    flow -= cal

    flow_sd = np.sqrt(cal_sd ** 2 + flow_sd ** 2)

    return flow, flow_sd


def calculate_flow(cur, cur_sd, thres=.005):
    """
    Helper function for calculating mach number time trace

    Parameters
    ----------
    cur : ndarray
        2 x n shape array containing currents for both mach probe faces
    cur_sd : ndarray
        2 x n shape array containing std dev currents for both mach probe faces
    thres : threshold current for calculating flow.  Flow is will be set to
        zero if current is below threshold.

    Returns
    -------
    flow : ndarray
        mach number time trace
    flow_sd : ndarray
        std dev mach number time trace

    Notes
    -----
    Does not apply minus sign for direction.  This is done in process_mach_probe!
    """
    flow = np.zeros_like(cur[0, :])
    flow_sd = np.zeros_like(cur[0, :])
    ind = np.where(np.logical_and(cur[0, :] > thres, cur[1, :] > thres))

    flow[ind] = 0.45 * np.log(cur[0, ind] / cur[1, ind])

    flow_sd[ind] = np.sqrt((0.45 * cur_sd[0, ind] / cur[0, ind]) ** 2 +
                           (0.45 * cur_sd[1, ind] / cur[1, ind]) ** 2)
    return flow, flow_sd


def combine_shots(shots, mach_probe, plotit=False):
    """
    Retrieves shot data and removes digitizer offsets.  Each array is stored
    as an element in a list

    Parameters
    ----------
    shots : list
        list of shot numbers (int)
    mach_probe : str
        mach probe string "mp<number>"
    plotit : bool
        plots the shots for comparison if True

    Returns
    -------
        t : ndarray
            time array of length n
        data_list : list
            list of mach probe current data, each element is a 2 x n shape array
        data_list_sd : list
            list of mach probe std dev current data, each element is a 2 x n shape array
    """
    t = None
    data_list = []
    data_sd_list = []
    if plotit:
        fig, ax = plt.subplots(2)

    for shot in shots:
        t, data, data_sd = retrieve_data(shot, mach_probe)
        data, data_sd = remove_offset(data, data_sd)
        data_list += [data]
        data_sd_list += [data_sd]
        if plotit:
            ax[0].plot(t, data[0, :], label=shot)
            ax[1].plot(t, data[1, :])

    if plotit:
        ax[0].legend()
        plt.show()

    return t, data_list, data_sd_list


# if __name__ == "__main__":
#     tableau20 = ptools.tableau20_colors()
#     # shot_number = 17141
#     time = None
#     """Calculate calibration here.  First you need to average east and west
#      shots.  After that, you can calculate the offset flow."""
#     west_shots = [17020, 17021]
#     east_shots = [17022, 17023]

    # time, west_current_list, west_current_sd_list = combine_shots(west_shots, "mp1")
    # _, east_current_list, east_current_sd_list = combine_shots(east_shots, "mp1")

    # west_current, west_current_sd = average_isat_traces(west_current_list, west_current_sd_list)
    # east_current, east_current_sd = average_isat_traces(east_current_list, east_current_sd_list)

    # cal, cal_sd = calculate_calibration(time, west_current, west_current_sd,
    #                                     east_current, east_current_sd, (12.2, 12.3), plotit=True)

    # print "offset: {0:4.3f} +/- {1:5.4f}".format(cal, cal_sd)

    # shots = [17018, 17019]
    # flow_list = []
    # flow_sd_list = []
    # for shot_number in shots:
    #     time, current, current_sd = retrieve_data(shot_number, "mp1")
    #     current = remove_offset(current)
    #     flow, flow_sd = process_mach_probe(current, current_sd, cal=cal, cal_sd=cal_sd)
    #     flow_list += [flow]
    #     flow_sd_list += [flow_sd]
    #     # plt.plot(time, flow, label="{0:d}".format(shot_number))
    # flow, flow_sd = average_time_trace(flow_list, flow_sd_list)
    # plt.fill_between(time, flow + flow_sd, flow - flow_sd, color='b')
    # plt.plot(time, flow, 'r', label="AVG")

    # # plt.legend()
    # plt.show()
    # # flow, flow_sd = process_mach_probe(current_list, current_sd_list)
    # # current, current_sd = average_isat_traces(current_list, current_sd_list)
    # # plt.plot(time, current[0, :], 'm')
