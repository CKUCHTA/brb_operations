import matplotlib.pyplot as plt
import numpy as np
import modules.analysis.basis_functions as bf
from matplotlib.patches import Wedge
import modules.analysis.t2s2_flow as t2s2
from scipy.stats import norm
import modules.analysis.velocity_fitting as vf


"""Build synthetic velocity measurments here"""
alpha_arr = np.linspace(-20, 20, 6)
L = np.arange(1.6, .06, -0.32) / 1.538

theta_pivot = [75, 90+40]

r, theta, ur, utheta, uphi, beta_arr = t2s2.build_measurements(alpha_arr, L, theta_pivot, epsilon=0.1)
noise = norm(scale=0.03)
ur *= 1.0 + noise.rvs(len(ur))
utheta *= 1.0 + noise.rvs(len(utheta))
uphi *= 1.0 + noise.rvs(len(uphi))

""" Build spline basis functions here"""
vmeas = -np.sin(np.deg2rad(beta_arr))*ur + np.cos(np.deg2rad(beta_arr)) * utheta
nmeas = len(vmeas)
nknots = 8
xdata = np.linspace(0, 1.0, nknots)
ydata = np.eye(nknots)

b = bf.BasisMatrix('natural_cubic_spline', xdata=xdata, ydata=ydata)

Apol = vf.build_pol_fitting_matrix([b, b], [2, 4], r, theta, beta_arr)
Aphi = vf.build_tor_fitting_matrix([b, b, b], [1, 2, 4], r, theta)


cpol, _, _, _ = np.linalg.lstsq(Apol, vmeas)
cphi, _, _, _ = np.linalg.lstsq(Aphi, uphi)

cpol = np.reshape(cpol, (-1, nknots - 2))
cphi = np.reshape(cphi, (-1, nknots-2))

r = np.linspace(0, 1, 200)
theta = np.linspace(0.01, np.pi-.01, 200)
RR, ZZ, ur, utheta, psi = vf.calculate_pol_flow([b, b], [2, 4], cpol, r, theta)
upol = np.sqrt(ur**2 + utheta**2)

_, _, uphi = vf.calculate_tor_flow([b, b, b], [1, 2, 4], cphi, np.linspace(0, 1, 200), np.linspace(0.01, np.pi-0.01, 200))

fig, ax = plt.subplots()
ax.axis('equal')
ax.axis('off')
ax.set_xlim(-1.2, 1.2)
ax.set_ylim(-1.2, 1.2)
w1 = Wedge((0, 0), 1.0, -90, 90, fill=False, edgecolor='k', linewidth=2)
w2 = Wedge((0, 0), 1.0, 90, 270, fill=False, edgecolor='k', linewidth=2)
ax.add_patch(w1)
ax.add_patch(w2)
vals = np.arange(-1.0, 1.0+.005, 0.005)
psi_vals = np.linspace(-.025, .025, 8)
cf = ax.contourf(RR, ZZ, uphi, vals, cmap='seismic')
# ax.contour(RR, ZZ, uphi, np.linspace(-0.5, 0.5, 8), colors='k')
ax.contourf(-RR, ZZ, upol,  vals, cmap='seismic')
ax.contour(-RR, ZZ, psi, psi_vals, colors='k')
plt.colorbar(cf, ticks=np.arange(-1, 1.25, .25))
plt.show()

print(cphi)
