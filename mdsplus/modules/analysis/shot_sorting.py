import numpy as np
import pandas as pio


def sort_shots(fname):

    data = pio.read_csv(fname)

    shots = np.array(data['Shot Number'])
    radius = np.array(data['Radius'])

    #sort the radii and moves shots accordingly
    ind = np.argsort(radius)
    shots = shots[ind]
    radius = radius[ind]

    new_radii = [radius[0],]
    new_shots = []
    temp = [shots[0]]
    for idx, r in enumerate(radius[1:]):
        if r not in new_radii:
            new_shots.append(temp)
            new_radii.append(r)
            temp = [shots[idx+1]]
        else:
            temp.append(shots[idx+1])
    new_shots.append(temp)
    # print new_shots
    return new_radii, new_shots

if __name__ == "__main__":
    pass
