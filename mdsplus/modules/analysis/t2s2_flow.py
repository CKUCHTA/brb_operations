import matplotlib.pyplot as plt
from modules.analysi.spherical_harmonics import Ylm
import numpy as np
from scipy.integrate import cumtrapz
from matplotlib.patches import Wedge


def f(x):
    return x ** 2 * np.sin(np.pi * x)


def f_div_r2(x):
    return np.sin(np.pi * x)


def f_div_r(x):
    return x * np.sin(np.pi * x)


def fprime(x):
    return np.pi * x ** 2 * np.cos(np.pi * x) + 2 * x * np.sin(np.pi * x)


def fprime_div_r(x):
    return np.pi * x * np.cos(np.pi * x) + 2 * np.sin(np.pi * x)


def calc_t2s2(r, theta, plotit=True, epsilon=0.1):
    L = 2
    utheta = fprime_div_r(r) * Ylm(theta, 0.0, L=L, deriv=1)
    ur = L * (L + 1) * f_div_r2(r) * Ylm(theta, 0.0, L=L)
    uphi = -f_div_r(r) * Ylm(theta, 0.0, L=L, deriv=1)

    utheta *= epsilon
    ur *= epsilon

    R = r * np.sin(theta)
    Z = r * np.cos(theta)

    return ur, utheta, uphi


def build_measurements(alpha_arr, L, theta_pivot, epsilon=0.1):
    # alpha_arr = np.linspace(-20, 20, 6)
    # L = np.arange(1.6, .06, -0.32) / 1.538

    # theta_pivot = [75, 90 + 40]
    R_pivot = 1.0

    z_arr = []
    rho_arr = []
    beta_arr = []
    for theta_piv in theta_pivot:
        z_piv = R_pivot * np.cos(np.deg2rad(theta_piv))
        rho_piv = R_pivot * np.sin(np.deg2rad(theta_piv))

        for alpha in alpha_arr:
            dz = L * np.cos(np.deg2rad(alpha + theta_piv))
            drho = L * np.sin(np.deg2rad(alpha + theta_piv))

            z_arr.extend((z_piv - dz).tolist())
            rho_arr.extend((rho_piv - drho).tolist())
            beta_arr.extend((alpha * np.ones(len(L))).tolist())

    z_arr = np.array(z_arr)
    rho_arr = np.array(rho_arr)
    beta_arr = np.array(beta_arr)
    ii = np.where(rho_arr > 0.0)
    z_arr = z_arr[ii]
    rho_arr = rho_arr[ii]
    beta_arr = beta_arr[ii]
    ii = np.where(rho_arr > 0.0)
    z_arr = z_arr[ii]
    rho_arr = rho_arr[ii]
    beta_arr = beta_arr[ii]

    r = np.sqrt(z_arr ** 2 + rho_arr ** 2)
    theta = np.arctan2(rho_arr, z_arr)

    ur, utheta, uphi = calc_t2s2(r, theta)
    return r, theta, ur, utheta, uphi, beta_arr

# if __name__ == "__main__":
#     r = np.linspace(0.0, 1.0, 1000)
#     theta = np.linspace(0.01, np.pi - .01, 1000)
#     r, theta, ur, utheta, psi = calc_t2s2(r, theta)
#
#     R = r * np.sin(theta)
#     Z = r * np.cos(theta)
#     fig, ax = plt.subplots()
#     ax.axis('equal')
#     #cf = ax.contourf(R, Z, utheta)
#     cf = ax.contourf(R, Z, np.sqrt(utheta**2 + ur**2))
#     ax.contour(R, Z, psi, colors='k')
#     plt.colorbar(cf)
#
#     w = Wedge((0, 0), 1.0, -90, 90, fill=False, edgecolor='k', linewidth=2)
#     ax.add_patch(w)
#     ax.set_xlim(0, 1)
#     ax.set_ylim(-1, 1)
#     plt.show()
# r = np.linspace(0, 1.0, 1000)
# theta = np.linspace(0.0, np.pi, 500)

# #Ylm(theta, 0.0, l=2, m=0, deriv=1)
# sp = Ylm(theta, 0.0, l=2, m=0)
# sa = f(r)
# psi = sa[:, np.newaxis] * sp[np.newaxis, :]
# psi_prime = fprime(r)[:, np.newaxis] * Ylm(theta, 0.0, l=2, m=0, deriv=1)[np.newaxis, :]
# print psi.shape

# theta, r = np.meshgrid(theta, r)
# print theta.shape
# print r.shape
# R= r * np.sin(theta)
# Z = r * np.cos(theta)
# print R[0:3, 0:3]
# l=2
# ur = l * (l+1) / r**2 * psi
# utheta = psi_prime / r

# R = np.linspace(0.0, 1.0, 250)
# Z = np.linspace(-1.0, 1.0, 500)

# RR, ZZ = np.meshgrid(R, Z)
# #ZZ, RR = np.meshgrid(Z, R)
# r = RR**2 + ZZ**2
# r[r > 1.0] = np.nan
# #theta = np.arctan(RR/ZZ) + np.pi/2
# theta = np.arctan2(RR, ZZ)
# # fig, ax = plt.subplots()
# # ax.axis('equal')
# # plt.plot(r*np.sin(theta), r*np.cos(theta), 'o')
# # plt.show()


# sp = Ylm(theta, 0.0, l=2, m=0)
# sa = f(r)
# psi = sa * sp
# psi_prime = fprime(r)* Ylm(theta, 0.0, l=2, m=0, deriv=1)

# #pol_psi = cumtrapz(uz * R, R[0, :], initial=0.0, axis=1)
# # #pol_psi = -1.0 * np.sin(theta) * cumtrapz(theta*r, r[:, 0], initial=0.0, axis=0)
# l = 2
# ur = l * (l+1) / r**2 * psi
# utheta = psi_prime / r
# urho = ur * np.cos(theta) + utheta*np.sin(theta)
# uz = ur*np.sin(theta) - utheta*np.cos(theta)
# uz[:, 0] = 0.0
# pol_psi = cumtrapz(uz * RR, RR[0, :], initial=0.0, axis=1)
# print RR[0:5, 0:5]
# # pol_psi = cumtrapz(uz * R, R[0, :], initial=0.0, axis=1)
# fig, ax = plt.subplots()
# ax.axis('equal')
# #plt.contourf(R, Z, psi/r**2)
# #plt.contourf(R, Z, np.sqrt(ur**2 + utheta**2))
# #pol_psi[r > 1.0] = np.nan
# plt.contourf(RR, ZZ, pol_psi)
# #plt.contourf(R, Z, ur)
# #plt.contourf(R, Z, utheta)
# #plt.contourf(R, Z, urho)
# #plt.contourf(R, Z, uz)
# print RR.shape, ZZ.shape, pol_psi.shape
# #plt.contourf(RR, ZZ, pol_psi)
# plt.colorbar()
# #ax.plot(x.flatten(), y.flatten(), 'o')
# #ax.set_xlim(-1.1, 1.1)
# #ax.set_ylim(0, 1.1)
# plt.show()
