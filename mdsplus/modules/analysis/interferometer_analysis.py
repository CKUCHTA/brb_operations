from matplotlib import rcParams
rcParams['font.size']=14
rcParams['ps.useafm'] = True  #totally forgot what this does
rcParams['pdf.use14corefonts'] = True  #I think I have this for latex to work properly in A.I.
#rcParams['text.usetex'] = True   #this works if you don't display the plot!
rcParams['figure.autolayout']=True  #good for subplots and saving figures
import matplotlib.pyplot as plt
import numpy as np
import sys
import sqltools.mysqltools as sqltools
import MDSplus as mds
import modules.analysis.helpers as h


def get_raw_mm(shot):
    try:
        shot =int(shot)
    except ValueError as e:
        print("You did not enter a valid shot number.")
        print(e)
        raise

    


    try:
        tree = mds.Tree("mm_wave_raw",shot)
    except mds.TreeException as e:
        print("Unable to open tree for shot {0:d}".format(shot))
        print(e)
        raise
    

    phase = tree.getNode("phase").getData().data()
    return phase

def clean_mm_trace(phase):

    i = np.min( np.where( phase < 255))
    j = np.min( np.where( phase[i::] >=255)) + (i-1)

    return phase[i:j]

def remove_offset(phase):
    #1 MHz sampling
    #50 ms is .05/1.e-6 = 5e4
    offset = np.mean(phase[0:5e4])
    return phase-offset

def apply_calibration(phase):

    #Dave has something about flipping the sign of mm wave
    #I haven't seen this behavior, but I'm sure its hiding in one of the shots
    ne = 4.58e17 * phase
    return ne

def get_interferometer_density(shot):
    phase = get_raw_mm(shot)
    phase = clean_mm_trace(phase)
    phase = remove_offset(phase)
    time = np.linspace(0,1,len(phase)) * (len(phase)-1) / 1.0e6
    ne = apply_calibration(phase)
    return (time,ne)

def adjust_plasma_turn_on(time,ne):
    ne = h.smooth(ne,10000)
    dt = np.gradient(time,edge_order=2)
    dne_dt = np.gradient(ne,dt,edge_order=2)
    i = np.argmax(dne_dt)
    j = np.min(np.where(dne_dt > .5*dne_dt[i]))
    plt.figure()
    plt.plot(time,dne_dt)
    ax = plt.gca()
    (ylow,yhigh) = ax.get_ylim() 
    plt.plot([time[i]]*2,[ylow,yhigh],'--k')
    plt.plot([time[j]]*2,[ylow,yhigh],'--r')

if __name__=="__main__":

    shot = 14827
    #phase = get_raw_mm(shot)
    #phase = clean_mm_trace(phase)
    #time = np.linspace(0,1,len(phase)) * (len(phase)-1) / 1.0e6
    #phase = remove_offset(phase)
    #ne = apply_calibration(phase)
    (time, ne) = get_interferometer_density(shot)
    adjust_plasma_turn_on(time,ne)
    plt.figure()
    plt.plot(time,ne)

    plt.show()




