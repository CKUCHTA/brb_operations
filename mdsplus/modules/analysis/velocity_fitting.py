import numpy as np
import analysis.spherical_harmonics as sh
import analysis.basis_functions as bf

def build_pol_fitting_matrix(b, L, r, theta, beta):
    assert len(b) == len(L), "b and L must be the same length!"
    assert len(r) == len(theta), "r and theta must be the same length!"
    assert len(r) == len(beta), "r and theta must be the same length!"
    nmeas = len(r)

    f = []
    f_prime = []
    for bb in b:
        f += [bb(r)[:, 1:-1]]
        f_prime += [bb(r, deriv=1)[:, 1:-1]]

    a = None
    b = None

    for j, LL in enumerate(L):
        temp_a = np.zeros_like(f[j])
        temp_b = np.zeros_like(f[j])

        for i in range(nmeas):
            sh_lm = sh.Ylm(theta[i], 0.0, L=LL, m=0)
            sh_lm_prime = sh.Ylm(theta[i], 0.0, L=LL, m=0, deriv=1)
            temp_a[i, :] = -np.sin(np.deg2rad(beta[i])) * LL * (LL + 1) * sh_lm / r[i] ** 2
            temp_b[i, :] = np.cos(np.deg2rad(beta[i])) * sh_lm_prime / r[i]
        if a is None:
            a = temp_a * f[j]
            b = temp_b * f_prime[j]
        else:
            a = np.hstack((a, temp_a * f[j]))
            b = np.hstack((b, temp_b * f_prime[j]))
    return a + b


def build_tor_fitting_matrix(b, L, r, theta):
    assert len(b) == len(L), "b and L must be the same length!"
    nmeas = len(r)

    f = []
    for bb in b:
        # f += [bb(r)[:, 1:-1]]
        f += [bb(r)[:, 1:]]

    a = None
    for j, LL in enumerate(L):
        temp_a = np.zeros_like(f[j])

        for i in range(nmeas):
            sh_lm_prime = sh.Ylm(theta[i], 0.0, L=LL, m=0, deriv=1)
            temp_a[i, :] = sh_lm_prime / r[i]

        if a is None:
            a = -temp_a * f[j]
        else:
            a = np.hstack((a, -temp_a * f[j]))

    return a


def calculate_pol_flow(b, L, coeff, r, theta):
    f = []
    f_prime = []
    for bb in b:
        f += [bb(r)[:, 1:-1]]
        f_prime += [bb(r, deriv=1)[:, 1:-1]]

    ur = 0.0
    utheta = 0.0
    psi = 0.0
    for idx, LL in enumerate(L):
        dsdr = np.dot(b[idx](r, deriv=1)[:, 1:-1], coeff[idx, :])
        s = np.dot(b[idx](r)[:, 1:-1], coeff[idx, :])
        sh_lm = sh.Ylm(theta, 0.0, L=LL, m=0)
        sh_lm_prime = sh.Ylm(theta, 0.0, L=LL, m=0, deriv=1)
        utheta += dsdr[np.newaxis, :] * sh_lm_prime[:, np.newaxis] / r[np.newaxis, :]
        ur += LL * (LL + 1) * s[np.newaxis, :] * sh_lm[:, np.newaxis] / r[np.newaxis, :] ** 2
        psi += -s[np.newaxis, :] * np.sin(theta[:, np.newaxis]) * sh_lm_prime[:, np.newaxis]
    newr, newtheta = np.meshgrid(r, theta)

    rho = newr * np.sin(newtheta)
    z = newr * np.cos(newtheta)
    return rho, z, ur, utheta, psi


def calculate_tor_flow(b, L, coeff, r, theta):
    # assume coeff has been reshaped to (number of L, number of knots)
    # assume r and theta are 1D arrays!

    f = []
    for bb in b:
        # f += [bb(r)[:, 1:-1]]
        f += [bb(r)[:, 1:]]

    uphi = 0.0
    for idx, LL in enumerate(L):
        # t = np.dot(b[idx](r)[:, 1:-1], coeff[idx, :])
        t = np.dot(b[idx](r)[:, 1:], coeff[idx, :])
        sh_lm_prime = sh.Ylm(theta, 0.0, L=LL, m=0, deriv=1)
        uphi += -t[np.newaxis, :] * sh_lm_prime[:, np.newaxis] / r[np.newaxis, :]

    rho = r[np.newaxis, :] * np.sin(theta[:, np.newaxis])
    z = r[np.newaxis, :] * np.cos(theta[:, np.newaxis])

    return rho, z, uphi


def calculate_toroidal_flow(r, theta, L, splines, xmin, amp0, printit=False):
    m = None
    n = None
    if r.ndim > 1:
        m, n = r.shape
        r = r.copy().flatten()
        theta = theta.copy().flatten()

    j = np.where(r < xmin)
    i = np.where(r >= xmin)
    y_lm_prime = sh.Ylm(theta, 0.0, L=L, deriv=1)

    t = splines(r)
    vphi = np.zeros_like(r)
    vphi[i] = -t[i] * y_lm_prime[i] / r[i]
    vphi[j] = -y_lm_prime[j] * amp0 * r[j] / xmin ** 2

    if m is not None and n is not None:
        vphi = vphi.reshape((m, n))
    return vphi


def calculate_poloidal_flow(r, theta, L, splines, xmin, amp0, printit=False):
    m = None
    n = None
    if r.ndim > 1:
        m, n = r.shape
        r = r.copy().flatten()
        theta = theta.copy().flatten()

    j = np.where(r < xmin)
    i = np.where(r >= xmin)
    y_lm = sh.Ylm(theta, 0.0, L=L)
    y_lm_prime = sh.Ylm(theta, 0.0, L=L, deriv=1)

    s = splines(r)
    s[j] = amp0 * r[j] ** 2 / xmin ** 2
    s_prime = splines(r, deriv=1)

    v_r = np.zeros_like(r)
    v_theta = np.zeros_like(r)

    v_r[i] = L * (L + 1) * s[i] * y_lm[i] / r[i] ** 2
    v_r[j] = L * (L + 1) * y_lm * amp0 / xmin ** 2

    v_theta[i] = s_prime[i] * y_lm_prime[i] / r[i]
    v_theta[j] = y_lm_prime * 2.0 * amp0 / xmin ** 2

    psi = -s * np.sin(theta) * y_lm_prime

    if m is not None and n is not None:
        v_r = v_r.reshape((m, n))
        v_theta = v_theta.reshape((m, n))
        psi = psi.reshape((m, n))
    return v_r, v_theta, psi


def chi_squared_poloidal(amplitudes, knots, r, theta, beta_arr, v, L, verr):
    # step 1 is we need splines for each L
    # currently assuming the same number of knots for each L
    amp = np.reshape(amplitudes, (len(L), -1))
    xk = np.reshape(knots, (len(L), -1))
    vcalc = 0.0
    for idx, LL in enumerate(L):
        splines = bf.NewClampedCubicSpline(xk[idx, :], amp[idx, :], yp=2.0 * amp[idx, 0] / xk[idx, 0])
        v_r, v_theta, psi = calculate_poloidal_flow(r, theta, LL, splines, xk[idx, 0], amp[idx, 0])
        vcalc += -np.sin(np.deg2rad(beta_arr)) * v_r + np.cos(np.deg2rad(beta_arr)) * v_theta

    return np.sum((v - vcalc) ** 2 / verr ** 2)


def chi_squared_toroidal(amplitudes, knots, r, theta, beta_arr, v, L, verr):
    # step 1 is we need splines for each L
    # currently assuming the same number of knots for each L
    amp = np.reshape(amplitudes, (len(L), -1))
    xk = np.reshape(knots, (len(L), -1))
    vcalc = 0.0
    for idx, LL in enumerate(L):
        splines = bf.NewClampedCubicSpline(xk[idx, :], amp[idx, :], yp=2.0 * amp[idx, 0] / xk[idx, 0])
        vphi = calculate_toroidal_flow(r, theta, LL, splines, xk[idx, 0], amp[idx, 0])

        vcalc += vphi

    return np.sum((v - vcalc) ** 2 / verr ** 2)
