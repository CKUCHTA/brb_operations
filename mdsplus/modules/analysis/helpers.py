import numpy as np
import scipy.signal as sg


def smooth(inputArr,pts,axis=0):
    shape_in = inputArr.shape
    window = np.ones(pts)/float(pts)
    start = inputArr.shape[axis]
    
    if len(shape_in) == 1:
        tmp = np.concatenate((inputArr[::-1],inputArr,inputArr[::-1]))
        output = sg.fftconvolve(tmp,window,'same')[start:2*start]
    elif len(shape_in) == 2:
        if axis == 0:
            window = window[:,np.newaxis]
            print(window)
            tmp = np.concatenate((inputArr[::-1,:],inputArr,inputArr[::-1,:]),axis=0)
            output = sg.fftconvolve(tmp,window,'same')[start:2*start,:]
        elif axis == 1:
            window = window[np.newaxis,:]
            print(window)
            tmp = np.concatenate((inputArr[:,::-1],inputArr,inputArr[:,::-1]),axis=1)
            output = sg.fftconvolve(tmp,window,'same')[:,start:2*start]
        else:
            raise ValueError("axis value greater than input array dimensions")

    elif len(shape_in) == 3:
        if axis == 0:
            window = window[:,np.newaxis,np.newaxis]
            tmp = np.concatenate((inputArr[::-1,:,:],inputArr,inputArr[::-1,:,:]),axis=0)
            output = sg.fftconvolve(tmp,window,'same')[start:2*start,:,:]
        elif axis == 1:
            window = window[np.newaxis,:,np.newaxis]
            tmp = np.concatenate((inputArr[:,::-1,:],inputArr,inputArr[:,::-1,:]),axis=1)
            output = sg.fftconvolve(tmp,window,'same')[:,start:2*start,:]
        elif axis == 2:
            window = window[np.newaxis,np.newaxis,:]
            tmp = np.concatenate((inputArr[:,:,::-1],inputArr,inputArr[:,:,::-1]),axis=2)
            output = sg.fftconvolve(tmp,window,'same')[:,:,start:2*start]
        else:
            raise ValueError("axis value greater than input array dimensions")
        
    return output

def rebin(array,pts,axis=0):
    window = array.shape[axis]//pts
    starts = np.arange(pts)*window
    end = pts*window
    tmp = np.zeros((pts,array.shape[1]))
    for i in range(window):
        indices = np.s_[i:end:window,]
        tmp += array[indices]/float(window)
    return tmp 

def bin_data(data,npts=100,bin_time=False,time=None):
    """Average data into bins of size npts
    
    Args:
        data (numpy 1d array): values stored in 1d numpy array
        npts (int): integer for number of values to be averaged into each bin

    Kwargs:
        bin_time (boolean): True if you want to get the new binned time array
        time (numpy 1d array): time values for data 

    Returns:
        new_data (1d numpy array): new data array binned from data
        data_sd (1d numpy array): standard deviations for each bin
        new_time (1d numpy array): new binned time array (only returns if 
                                   bin_time=True and time is given)

    """ 
    n = len(data)
    m = n / npts 
    new_data = np.zeros(m) 
    data_sd = np.zeros(m) 
    for i in range(m):
        if (i+1)*npts >= n:
            new_data[i] = np.mean(data[i*npts:])
            data_sd[i] = np.std(data[i*npts:])
        else:
            new_data[i] = np.mean(data[i*npts:(i+1)*npts])
            data_sd[i] = np.std(data[i*npts:(i+1)*npts])

    if bin_time and time is not None:
        new_time = np.zeros(m) 
        for i in range(m): 
            if (i+1)*npts >= n:
                new_time[i] = np.mean(time[i*npts:])
            else:
                new_time[i] = np.mean(time[i*npts:(i+1)*npts])
        return new_data, data_sd, new_time
    else:
        return new_data, data_sd



def new_bin_size(time,newfreq):
    """Returns the number of points for binning data to a lower frequency (newfreq)

    Args:
        time (1d numpy array): time array
        newfreq (int): new frequency (must be smaller than time's digitization frequency)

    Returns:
        the number of points to bin data to the new frequency
    """
    freq = int(1./(time[1]-time[0]))
    return int(freq / newfreq)

def bin_ndarray(ndarray, new_shape, operation='sum'):
    """
    Bins an ndarray in all axes based on the target shape, by summing or
        averaging.
    Number of output dimensions must match number of input dimensions.
    Example
    -------
    >>>> m = np.arange(0,100,1).reshape((10,10))
    >>>> n = bin_ndarray(m, new_shape=(5,5), operation='sum')
    >>>> print(n)
    [[ 22  30  38  46  54]
     [102 110 118 126 134]
     [182 190 198 206 214]
     [262 270 278 286 294]
     [342 350 358 366 374]]
    """
    if not operation.lower() in ['sum', 'mean', 'average', 'avg']:
        raise ValueError("Operation {} not supported.".format(operation))
    if ndarray.ndim != len(new_shape):
        raise ValueError("Shape mismatch: {} -> {}".format(ndarray.shape,
                                                           new_shape))
    compression_pairs = [(d, c//d) for d, c in zip(new_shape,
                                                   ndarray.shape)]
    flattened = [l for p in compression_pairs for l in p]
    ndarray = ndarray.reshape(flattened)
    for i in range(len(new_shape)):
        if operation.lower() == "sum":
            ndarray = ndarray.sum(-1*(i+1))
        elif operation.lower() in ["mean", "average", "avg"]:
            ndarray = ndarray.mean(-1*(i+1))
    return ndarray
