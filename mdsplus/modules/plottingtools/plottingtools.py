# import analysis.helpers as h
import analysis.datahelpers as h
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.colors as colors
import numpy as np
import copy

#;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
#;;;;  Useful Plotting Colors  ;;;;
#;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),  
          (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),  
          (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),  
          (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),  
          (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]

for i in range( len(tableau20) ):
    r, g, b = tableau20[i]
    tableau20[i] = (r/255., g/255., b/255.)


def truncate_colormap(cmap,minval=0.0,maxval=1.0,n=100):
    """truncate colormap cmap between minval and maxval."""
    new_cmap = colors.LinearSegmentedColormap.from_list('trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval,b=maxval),cmap(np.linspace(minval,maxval,n)))
    return new_cmap

def scale_patches(patches,scale):
    """scale patches by desired factor."""
    t = mpl.transforms.Affine2D().scale(scale)
    for p in patches:
        p.set_transform(p.get_transform()+t)
    return patches

def get_mirror_patches(patches,axis=0,scale=1):
    """Get mirror image patches across desired axis.
    axis = 0 means reflect across x axis, axis = 1
    means reflect across y axis. Optional scale
    argument can be used as well."""
    x_ref = np.array([[1,0,0],[0,-1,0],[0,0,1]])
    y_ref = np.array([[-1,0,0],[0,1,0],[0,0,1]])
    if axis == 0:
        matrix = x_ref
    else:
        matrix = y_ref
    t = mpl.transforms.Affine2D(matrix=matrix).scale(scale)
    mirror_patches = []
    for p in patches:
        m_patch = copy.copy(p)
        m_patch.set_transform(m_patch.get_transform()+t)
        mirror_patches.append(m_patch)
    return mirror_patches

def transpose_patches(patches):
    """Transpose patches (reflect across line y=x)."""
    transpose = np.array([[0,1,0],[1,0,0],[0,0,1]])
    t = mpl.transforms.Affine2D(matrix=transpose)
    mirror_patches = []
    for p in patches:
        p.set_transform(p.get_transform()+t)
    #return patches


def makePlot(fig,axes,minor_ticks=True,transparent=False,filename=None,fileformat="pdf"):
    """Modify plot properties to look better.

    Args:
        fig (plt.Figure instance)
        axes (list of axes)

    """
    for ax in axes:
        [s.set_linewidth(1.0) for s in ax.spines.values()]
        if minor_ticks:
            ax.minorticks_on()
        else:
            ax.minorticks_off()
        ax.tick_params('both',length=6,width=1.0, which='major',labelsize="14")#,pad=10)
        ax.tick_params('both',length=4,width=.5, which='minor')
        #lines,labels = ax.get_legend_handles_labels()
        #if labels != []:
        #    leg = ax.legend(loc="best")
        #    leg.draggable()
        ax.set_ylabel(ax.get_ylabel(),fontsize="16")
        ax.set_xlabel(ax.get_xlabel(),fontsize="16")
    axSpines=[ax.spines['left'],ax.spines['right'],ax.spines['bottom'],ax.spines['top']]
    for k,spine in ax.spines.items():
        spine.set_zorder(2000)
    ticklines = ax.get_yticklines()+ax.get_xticklines()
    for tick in ticklines:
        tick.set_zorder(2000)
    fig.canvas.draw()

    if filename != None:
        plt.savefig(filename+"."+fileformat,format=fileformat,transparent=transparent,dpi=120, bbox_inches='tight',)
    else:
        #plt.tight_layout(w_pad=.5)
        pass
        #plt.show()

def makePlot2(fig,axes,minor_ticks=True,transparent=True,filename=None,fileformat="pdf"):
    """Modify plot properties to look better.

    Args:
        fig (plt.Figure instance)
        axes (list of axes)

    """
    for ax in axes:
        if ax.get_label().lower() != "cax":
            [s.set_linewidth(.3) for s in ax.spines.values()]
            if minor_ticks:
                ax.minorticks_on()
            else:
                ax.minorticks_off()
            ax.tick_params('both',length=3,width=.3, which='major',labelsize="10")#,pad=10)
            ax.tick_params('both',length=1.5,width=.15, which='minor')
            lines,labels = ax.get_legend_handles_labels()
            if labels != []:
                leg = ax.legend(loc="best")
                leg.draggable()
            ax.set_ylabel(ax.get_ylabel(),fontsize="12")
            ax.set_xlabel(ax.get_xlabel(),fontsize="12")
    plt.tight_layout()
    if filename != None:
        plt.savefig(filename+"."+fileformat,format=fileformat,transparent=transparent,dpi=600, bbox_inches='tight',)

def makeMovie(fig,ax1,ax,xdata,ydata,ydata_err=[],filename=[],plotline=[],bottoms=[],tops=[],verts=[], frames=100):
    """ Make .mp4 out of figures."""
    skip = len(ydata[:,0])/frames
    [extraLine,] = ax1.plot([0,0],[-1,3],'k',lw=3)
    def init():
        plotline.set_data([],[])
        bottoms.set_data([],[])
        tops.set_data([],[])
        verts.set_segments([])
        extraLine.set_data([],[])
        return plotline,bottoms,tops,verts,extraLine

    def animate(i):
        x = xdata
        y = ydata[skip*i,:]
        yerr = ydata_err[skip*i,:]
        yerr_bot = y-yerr
        yerr_top = y+yerr
        plotline.set_data(x,y)
        plotline.set_markersize(8)
        bottoms.set_data(x,yerr_bot)
        bottoms.set_linewidth(2)
        tops.set_data(x,yerr_top)
        tops.set_linewidth(2)
        tmp_segments = [None]*len(x)
        for k,x0 in enumerate(x):
            tmp_segments[k] = [[x0,yerr_bot[k]],[x0,yerr_top[k]]]
        verts.set_segments(tmp_segments)
        ax.set_title('t = '+str((i*skip)/1000.0*2.5),fontsize=20)
        verts.set_linewidth(2)
        extraLine.set_data([i*2.5/float(frames),i*2.5/float(frames)],[-1,3])
        return plotline,bottoms,tops,verts,extraLine
    
    print("Animating...")
    anim = animation.FuncAnimation(fig, animate, init_func = init, frames = frames, interval=20, blit=True)
    anim.save(filename+'.mp4',fps=10)

    return

