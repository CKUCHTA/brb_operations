import matplotlib.pyplot as plt


def tableau20_colors():
    tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
                 (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
                 (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
                 (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
                 (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]
    for i in range(len(tableau20)):
        r, g, b = tableau20[i]
        tableau20[i] = (r / 255., g / 255., b / 255.)

    return tableau20


def combine_legend(ax1, ax2, frameon=True, fontsize=18, loc=None):
    lines1, labels1 = ax1.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()

    leg = None
    if loc:
        leg = ax1.legend(lines1 + lines2, labels1 + labels2, fontsize=fontsize,
                         frameon=frameon, loc=loc)
    else:
        leg = ax1.legend(lines1 + lines2, labels1 + labels2, fontsize=fontsize,
                         frameon=frameon)
    return leg


def add_thick_box(ax, labelsize=18, minor=True):

    ax.tick_params('both', length=8, width=2, which='major', pad=10,
                   labelsize=labelsize)
    if minor:
        ax.minorticks_on()
        ax.tick_params('both', length=8, width=1, which='minor')
    for spine in ax.spines.values():
        spine.set_linewidth(2)
    return ax


def add_labels(ax, xlabel, ylabel, fontsize=18):
    ax.set_xlabel(xlabel, fontsize=fontsize)
    ax.set_ylabel(ylabel, fontsize=fontsize)
    return ax
