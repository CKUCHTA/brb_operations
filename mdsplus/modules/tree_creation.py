import logging


NODE_NAME = "name_of_node" # Global constant if the tag suffix is the same as the node name.

# Tree node data types
N = "numeric"
S = "signal"
T = "text"
ST = "structure"
ACT = "action"

class Node:
    """
    Object for holding info needed for a node in an MDSplus tree.
    
    Parameters
    ----------
    name : str
        Name of the node in the tree.
    data_type : str
        The MDSplus datatype of the node.
    tag : str, NODE_NAME, or None, default=None
        The tag of the node. If the tag is `None` then there will be no tag
        associated in MDSplus. If `NODE_NAME` then use `name` attribute as tag.
    subtree_prefix : str, default=None
        Prefix to use for all nodes beneath current node. If `None` then no additional prefix.
    """
    def __init__(self, name, data_type, tag=None, subtree_prefix=None):
        self.name = name
        self.data_type = data_type
        if tag == NODE_NAME:
            self.tag = name
        else:
            self.tag = tag
        self.subtree_prefix = subtree_prefix

def recursive_tree_build(tree, curr_node, curr_node_structure, tag_prefix='', recursion_level=0):
    """
    Build the tree by recursively calling the structure and building each node.

    Parameters
    ----------
    tree : mds.Tree
        MDSplus tree that we are building.
    curr_node : mds.TreeNode
        Node who's child nodes we are building.
    curr_node_structure
        Dictionary or set that contains all child nodes of where the tree is currently pointing.
    tag_prefix : str, default=''
        The string to prefix all tags in this subtree by.
    recursion_level : int, default=0
        How many times this function has been recursively called.
    """
    log_prefix = "|   " * recursion_level
    # If the structure is a set instead of a dict then we make it look like a dict to simplify future code.
    if isinstance(curr_node_structure, set):
        temp_dict = {}

        for node in curr_node_structure:
            temp_dict[node] = None

        curr_node_structure = temp_dict

    # Iterate through each child node info, child node structure pair.
    for child_node_obj in curr_node_structure.keys():
        sub_struct = curr_node_structure[child_node_obj]
        # Set the tree to the current node. We do this each time because the tree might change positions during recursion.
        tree.setDefault(curr_node)

        # Create the child node.
        child_name = child_node_obj.name
        child_data_type = child_node_obj.data_type
        child_tag = child_node_obj.tag
        logging.debug(log_prefix + "Adding node '{}'.".format(child_name))
        tree.addNode(child_name, child_data_type)
        child_node = tree.getNode(child_name)

        # Add the tag of the child node.
        if child_tag is not None:
            logging.debug(log_prefix + "Adding tag '{}'.".format(tag_prefix + child_tag))
            child_node.addTag(tag_prefix + child_tag)
        
        # For info logging, an additional space is needed.
        logging.info(" " + log_prefix + "Added node '{}'.".format(child_node.getNodeName()))

        # If there is no sub structure then we don't need to do anything else!
        if sub_struct is None:
            logging.debug(log_prefix + "No subtree of '{node_name}' found.".format(node_name = child_name))
            continue
        else:
            # Create the sub-tree of the child node.
            logging.debug(log_prefix + "Subtree of '{node_name}' found.".format(node_name = child_name))
            
            if child_node_obj.subtree_prefix is not None:
                new_prefix = tag_prefix + child_node_obj.subtree_prefix
            else:
                new_prefix = tag_prefix

            recursive_tree_build(tree, child_node, sub_struct, new_prefix, recursion_level + 1)
