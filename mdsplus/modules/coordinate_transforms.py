import numpy as np
from numpy import cos, sin


def position_to_machine_cartesian(port_r, port_phi, port_theta, alpha, beta, gamma, x_port, y_port, z_port, clocking):
    """
    Take the port position, tilt angles, and probe position in relation to the port and change it to machine cartesian.

    Parameters
    ----------
    port_r : float (m)
        Radial port position.
    port_phi : float (radians)
        Longitude of port.
    port_theta : float (radians)
        Latitude of port.
    alpha, beta, gamma : float (radians)
        1st, 2nd, and 3rd Euler angle of port.
    x_port : float (m)
        Longitudanally directed position of probe in relation to port. (North when clocking=0)
    y_port : float (m)
        Latitudinally directed position of probe in relation to port. (East when clocking=0)
    z_port : float (m)
        Inward position of probe in relation to port. (Down)
    clocking : float (radians)
        Counter-clockwise angle of the probe measured from when the x direction is pointed north.

    Returns
    -------
    np.array[float]
        `[x, y, z]`
    """
    port_cartesian = np.array((
        port_r * cos(port_theta) * cos(port_phi),
        port_r * cos(port_theta) * sin(port_phi),
        port_r * sin(port_theta)
    ))

    probe_coords = np.array((x_port, y_port, z_port))
    probe_cartesian = np.dot(get_transformation_matrix(port_phi, port_theta, alpha, beta, gamma, clocking), probe_coords) + port_cartesian
    return probe_cartesian

def direction_to_machine_cartesian(port_phi, port_theta, alpha, beta, gamma, x_direction, y_direction, z_direction, clocking):
    """
    Take the port position, tilt angles, direction, and clocking and change it to machine cartesian.
    The clocking is defined such that the probe x direction is pointed to the north.

    Parameters
    ----------
    port_phi : float (radians)
        Longitude of port.
    port_theta : float (radians)
        Latitude of port.
    alpha, beta, gamma : float (radians)
        1st, 2nd, and 3rd Euler angle of port.
    x_port : float (m)
        Longitudanally directed position of probe in relation to port. (North)
    y_port : float (m)
        Latitudinally directed position of probe in relation to port. (East)
    z_port : float (m)
        Inward position of probe in relation to port. (Down)
    clocking : float (radians)
        Counter-clockwise angle of the probe measured from when the x direction is pointed north.

    Returns
    -------
    np.array[float]
    """
    direction_coords = np.array((x_direction, y_direction, z_direction))
    direction_cart = np.dot(get_transformation_matrix(port_phi, port_theta, alpha, beta, gamma, clocking), direction_coords)
    return direction_cart

def position_cartesian_to_cylindrical(cartesian_position):
    """
    Change a position in machine cartesian coordinates (x, y, z) 
    to machine cylindrical coordinates (r, phi, z).

    Parameters
    ----------
    cartesian_position : np.array[float]
        Position in machine cartesian coords.

    Returns
    -------
    cylindrical_coords : np.array[float]
        Position in machine cylindrical coords where phi is measured in radians.
    """
    return np.array((
        (cartesian_position[0]**2 + cartesian_position[1]**2)**0.5,
        np.arctan2(cartesian_position[1], cartesian_position[0]),
        cartesian_position[2]
    ))

def position_cylindrical_to_cartesian(cylindrical_position):
    """
    Change a position in machine cylindrical coordinates (r, phi, z)
    to machine cartesian coordinates (x, y, z).

    Parameters
    ----------
    cylindrical_position : np.array[float]
        Position in machine cylindrical coords where phi is measured in radians.

    Returns
    -------
    cartesian_coords : np.array[float]
        Position in machine cartesian coords.
    """
    return np.array((
        cylindrical_position[0] * np.cos(cylindrical_position[1]),
        cylindrical_position[0] * np.sin(cylindrical_position[1]),
        cylindrical_position[2]
    ))

def direction_cartesian_to_cylindrical(cartesian_direction, cartesian_position):
    """
    Change a cartesian direction to cylindrical direction. This requires the location of the direction vector.
    
    Parameters
    ----------
    cartesian_direction : np.array[float]
        `[x, y, z]` Direction of vector in cartesian coordinates.
    cartesian_position : np.array[float]
        `[x, y, z]` Position of where the direction is measured from.

    Returns
    -------
    np.array[float]
        `[r, phi, z]` direction of vector in cylindrical coordinates.
    """
    cylindrical_position = position_cartesian_to_cylindrical(cartesian_position)
    phi_position = cylindrical_position[1]

    rotation_matrix = np.array((
        (np.cos(phi_position),  np.sin(phi_position),   0),
        (-np.sin(phi_position), np.cos(phi_position),   0),
        (0,                     0,                      1)
    ))
    return np.dot(rotation_matrix, cartesian_direction)

def get_transformation_matrix(port_phi, port_theta, alpha, beta, gamma, clocking):
    """
    Get the matrix that transforms local probe coordinates into machine cartesian coordinates.

    Parameters
    ----------
    port_phi : float (radians)
        Longitude of port.
    port_theta : float (radians)
        Latitude of port.
    alpha, beta, gamma : float (radians)
        1st, 2nd, and 3rd Euler angle of port.
    clocking : float (radians)
        Counter-clockwise angle of the probe measured from when the x direction is pointed north.

    Returns
    -------
    transformation_matrix : np.array[float]
        3 x 3 matrix to multiply an (x, y, z) vector by.
    """
    clocking_rotation = np.array((
        (   cos(-clocking), -sin(-clocking),    0),
        (   sin(-clocking), cos(-clocking),     0),
        (   0,              0,                  1)
    ))

    rotation_x = np.array((
        (   1,          0,          0           ),
        (   0,          cos(beta),  -sin(beta)  ),
        (   0,          sin(beta),  cos(beta)   )
    ))
    rotation_y = np.array((
        (   cos(alpha), 0,          -sin(alpha) ),
        (   0,          1,          0           ),
        (   sin(alpha), 0,          cos(alpha)  )
    ))
    rotation_z = np.array((
        (   cos(gamma), -sin(gamma),0           ),
        (   sin(gamma), cos(gamma), 0           ),
        (   0,          0,          1           )
    ))

    rotation_matrix = np.matmul(np.matmul(rotation_x, rotation_y), rotation_z)

    ned_to_cartesian = np.array((
        (   -sin(port_theta) * cos(port_phi),   -sin(port_phi),     -cos(port_theta) * cos(port_phi)),
        (   -sin(port_theta) * sin(port_phi),   cos(port_phi),      -cos(port_theta) * sin(port_phi)),
        (   cos(port_theta),                    0,                  -sin(port_theta))
    ))

    return np.matmul(np.matmul(ned_to_cartesian, rotation_matrix), clocking_rotation)
