import MDSplus as mds
import mds_builders as mdsbuild
import sys

def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    act = "action"

    # hook_bdot2 tree structure
    # Number of Probes: 4x2=8 (doubled axial probes)
    #     Top
    #         data_mach
#               dbr, dbphi, dbz
#               r, phi, z
#         data_loc
#             db1 (r, phi, z), db2, db3a, db3b
#         port
#             clock
#             rport
#             lat
#             long
#             alpha
#             beta
#             gamma
#         Probes
#             PB_01
#                 db1, db2, db3a, db3b
#                 dbr, dbphi, dbz
#                 r, phi, z
#         PCBs
#             PCB_01
#                 db1
#                   raw, gain, r, phi, z
#                 db2..db3b
#         ...
#         
    # 	
    # create node, type, tag
    topSts = [("data_mach",st), ("data_local",st), ("port",st)]
    datamachNodes = [("dbr",s,"dbr"), ("dbphi",s,"dbphi"), ("dbz",s,"dbz"), ("r",n,"r"), ("phi",n,"phi"), ("z",n,"z")]
    datalocNodes = [("db1",s,"db1"), ("db2",s,"db2"), ("db3a",s,"db3a"), ("db3b",s,"db3b")]
    posNodes = [("r",n,"r"), ("phi",n,"phi"), ("z",n,"z")]
    portNodes = [("insert",n,"insert"), ("clock",n,"clock"), ("rport",n,"rport"), ("lat",n,"lat"), ("long",n,"long"), ("alpha",n,"alpha"), ("beta",n,"beta"), ("gamma",n,"gamma")]
    
    pcbNodes = [("db1",s,"db1"), ("db2",s,"db2"), ("db3a",s,"db3a"), ("db3b",s,"db3b")]
    axNodes = [("raw",s,None), ("gain",n,None), ("r",n,None), ("phi",n,None), ("z",n,None)]
    pbNodes = [("dbr",s,"dbr"), ("dbphi",s,"dbphi"), ("dbz",s,"dbz"), ("r",n,"r"), ("phi",n,"phi"), ("z",n,"z"), ("db1",s,"db1"), ("db2",s,"db2"), ("db3",s,"db3")]
    nameTag = "hook_bdot2"
    
    tr = mds.Tree(nameTag, shot=-1, mode="New")
    top = tr.getNode("\\top")
    tr.edit()
    
    # create top level structures and nodes
    for (tnm, tuse) in topSts:
        tr.addNode(tnm, tuse)
#         print(tnm)
        
        # add nodes
        stNd = tr.getNode(tnm)
        tr.setDefault(stNd)
        if tnm=="data_mach":
            inNodes = datamachNodes
        elif tnm=="data_local":
            inNodes = datalocNodes
        elif tnm=="port":
            inNodes = portNodes
        for (ndnm, nduse, ndtag) in inNodes:
            tempNd = tr.addNode(ndnm, nduse)
#             print("{0}_{1}".format(nameTag, ndtag))
            tempNd.addTag("{0}_{1}".format(nameTag, ndtag))
        
        if tnm=="data_local":
            for (ndnm, nduse, ndtag) in inNodes:
                posNd = tr.getNode(ndnm)
                tr.setDefault(posNd)
                for (nd, use, tag) in posNodes:
                    tempNd = tr.addNode(nd, use)
                    tempNd.addTag("{0}_{1}_{2}".format(nameTag, ndnm, tag))
                tr.setDefault(stNd)

        tr.setDefault(top)

            
    # add individual PCB nodes
    numpcbs = 8;
    pcbRange = range(1,numpcbs+1)
    pcbNames = ["pcb{0:02d}".format(i) for i in pcbRange]
    stNd = tr.addNode("pcbs",st)
    tr.setDefault(stNd)
    for pcbnm in pcbNames:
        tr.addNode(pcbnm, st)
        pcbNd = tr.getNode(pcbnm)
        tr.setDefault(pcbNd)
        for (pcbNdnm, pcbNduse, pcbNdtag) in pcbNodes:
            tempNd = tr.addNode(pcbNdnm, pcbNduse)
            if pcbNdtag is not None:
                tempNd.addTag("{0}_{1}_{2}".format(nameTag, pcbnm, pcbNdtag))
        # add db_axis nodes
        for (dbNdnm,__,__) in pcbNodes:
            dbNd = pcbNd.getNode(dbNdnm)
            tr.setDefault(dbNd)
            for (axNdnm, axNduse, axNdtag) in axNodes:
                tempNd = tr.addNode(axNdnm, axNduse)
        tr.setDefault(stNd)

    tr.setDefault(top)

    # add "probe" nodes (combination of pcb probes to effectively double signals)
    numpbs = 16;
    pbRange = range(1, numpbs+1)
    pbNames = ["pb{0:02d}".format(i) for i in pbRange]
    stNd = tr.addNode("probes",st)
    tr.setDefault(stNd)
    for pbnm in pbNames:
        tr.addNode(pbnm, st)
        pbNd = tr.getNode(pbnm)
        tr.setDefault(pbNd)
        for (pbNdnm, pbNduse, pbNdtag) in pbNodes:
            tempNd = tr.addNode(pbNdnm, pbNduse)
            tempNd.addTag("{0}_{1}_{2}".format(nameTag, pbnm, pbNdtag))
        tr.setDefault(stNd)
    
    tr.setDefault(top)

    
    # mdsbuild.make_tree_write_once(tr)
    # unlocked_nodes = (...)
    # mdsbuild.unlock_nodes(mdsbuild.get_node_refs(top,*unlocked_nodes))
    tr.write()
    mdsbuild.traverse_tree(top)
    tr.cleanDatafile()
    tr.quit()
    return

def populate_tdi():
    hkTree = mds.Tree("hook_bdot2",-1)
    top = hkTree.getNode("\\top")

    # populate raw data nodes under PCB## structures
    dbs = ["db1", "db2", "db3a", "db3b"]
    pcbstNd = top.getNode("pcbs")
    hkTree.setDefault(pcbstNd)
    for ch in pcbstNd.getChildren():
        chnm = ch.getNodeName().lower()
        for dbnm in dbs:
            rawnd, gainnd = mdsbuild.get_node_refs(ch, dbnm + ".raw", dbnm + ".gain")
            dbNd = ch.getNode(dbnm)
            dbsig = mds.Data.compile("BUILD_SIGNAL($1*$2, , DIM_OF($2))", gainnd, rawnd)
            mdsbuild.put_wrapper(dbNd, dbsig, units="T/s")
    hkTree.setDefault(top)

    # populate data_local db## arrays
    dbrefs = ["db1", "db2", "db3a", "db3b"]
    datarefs = ["data_local:" + i for i in dbrefs]
    dbNdrefs = mdsbuild.get_node_refs(top, *datarefs)

    numpcbs = 8
    pcbnums = range(1, numpcbs+1)
    
    for index, dbnm in enumerate(dbrefs):
        dbNd = dbNdrefs[index]
        bnodes = mdsbuild.get_node_refs(top, *tuple("pcbs.pcb{0:02d}.{1}".format(i,dbnm) for i in pcbnums))
        data_str = "[" + ",".join(["DATA(${0:d})".format(i) for i in pcbnums]) + "]"
        # use dimension of first probe only
        dim_str = "DIM_OF($1)"

        barr = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str, dim_str), *bnodes)
        mdsbuild.put_wrapper(dbNd, barr, units="T/s")

        # add position data to data_local.db## nodes
        posrefs = ["r", "phi", "z"]
        for posnm in posrefs:
            posNd = mdsbuild.get_node_refs(top, "data_local:{0}:{1}".format(dbnm,posnm))
            posnodes = mdsbuild.get_node_refs(top, *tuple("pcbs.pcb{0:02d}.{1}.{2}".format(i,dbnm,posnm) for i in pcbnums))
            posdata = mds.Data.compile("{0}".format(data_str), *posnodes)
            if posnm=="phi":
                un = "deg"
            else:
                un = "m"
            mdsbuild.put_wrapper(posNd, posdata, units=un)


    # populate data_mach db, pos arrays
    dbs = ["dbr", "dbphi","dbz", "r", "phi", "z"]
    datas = ["data_mach:" + i for i in dbs]
    dbNdrefs = mdsbuild.get_node_refs(top, *datas)

    numpbs = 16
    pbnums = range(1, numpbs+1)

    for index, dbnm in enumerate(dbs):
        dbNd = dbNdrefs[index]
        nodes = mdsbuild.get_node_refs(top, *tuple("probes.pb{0:02d}.{1}".format(i,dbnm) for i in pbnums))
        data_str = "[" + ",".join(["DATA(${0:d})".format(i) for i in pbnums]) + "]"
        dim_str = "DIM_OF($1)"

        if "db" in dbnm:
            arr = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str, dim_str), *nodes)
            un = "T/s"
        else:
            arr = mds.Data.compile("{0}".format(data_str), *nodes)
            if dbnm=="phi":
                un = "deg"
            else:
                un = "m"
        
        mdsbuild.put_wrapper(dbNd, arr, units=un)    
        

    return

if __name__ == "__main__":
    build_structure()
    populate_tdi()
    print("hook_bdot2 tree created!")
