import MDSplus as mds
import modules.mds_builders as mdsbuild

def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    bdot_nodes = [("bdot_loc",n,"bdot_loc"),("bdot_1",s,"bdot_1"),("bdot_2",s,"bdot_2"),("bdot_3",s,"bdot_3"),("timing",s,"timing")]
    double_nodes = [("spiral_1_v",s,"spiral_1_v"),("spiral_1_i",s,"spiral_1_i"),("spiral_2_v",s,"spiral_2_v"),("spiral_2_i",s,"spiral_2_i"),
            ("double_3_v",s,"double_3_v"),("double_3_i",s,"double_3_i"),("double_4_v",s,"double_4_v"),("double_4_i",s,"double_4_i"),("double_locs",n,"double_locs")]

    myTree = mds.Tree(tree="spiral_probe",shot=-1,mode="New")
    top = myTree.getNode("\\top")
    myTree.edit()
    ## create top level magnetics data nodes
    for (tn,use,tag) in bdot_nodes+double_nodes:
        print(tn)
        node = myTree.addNode(tn,use)
        if tag is not None:
            node.addTag("spiral_probe_"+tag)

    myTree.write()
    myTree.cleanDatafile()
    myTree.quit()
    return



def populate_tdi():
    return

if __name__ == "__main__":
    build_structure()
#    populate_tdi()
    print("spiral_probe tree created!")
