import MDSplus as mds
import modules.mds_builders as mdsbuild



def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"

    #nodes = ["wavelength", "calibration", "spectrum", "time", "int_time",
    #        "chord_pos", "int_bins", "int_wave", "timing"]

    nodes = [("wavelength", n), ("rad_cal", n), ("spectrum", n), ("time", n), ("int_time", n),
            ("chord_pos", n), ("radiance", n)]
    myTree = mds.Tree(tree="spec1", shot=-1, mode="New")
    myTree.edit()

    for node, use  in nodes:
        myTree.addNode(node, use)
    myTree.addNode("notes", t)

    time_node = myTree.getNode("time")
    time_node.addNode("timing", s)
    time_node.addNode("n_spectra", n)
    rad = ['rad_656', 'rad_486', 'rad_667', 'rad_706', 'rad_727', 'rad_810', 'rad_750']

    radiance_node = myTree.getNode("radiance")
    for radiance in rad:
        # sub node of radiance node
        radiance_node.addNode(radiance, s)
        rad_node = radiance_node.getNode(radiance)

        # sub node of rad_node
        rad_node.addNode("indices_bin", n)

    myTree.write()

# def populate_tdi():
#     spec_tree = mds.Tree("spec1", -1)
#     top = spec_tree.getNode("\\top")


if __name__ == "__main__":
    build_structure()


