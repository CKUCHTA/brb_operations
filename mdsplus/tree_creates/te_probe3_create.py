import MDSplus as mds
import modules.mds_builders
from modules.tree_creation import *
import sys
import logging


def build_structure():
    """
    Build the full tree node structure with tags. Equations are not filled.
    """
    # teprobe1 tree structure
    # Top
        # avg_index (ST) Indeces when nothing is happening but data is being recorded.
            # start (N) From config.
            # end (N) From config.
        # act_area (N) [m^2] From config.
        # data (ST)
            # vprobe [V] (S) BUILD_SIGNAL(tip01.vprobe, ...)
            # iprobe [V] (S) BUILD_SIGNAL(tip01.iprobe, ...)
            # working (S) BUILD_SIGNAL(tip01.working, ...)
        # port (ST)
            # clock [deg] (N) From MySQL thru post run code.
            # rport [m] (N) From MySQL thru post run code.
            # lat [deg] (N) From MySQL thru post run code.
            # long [deg] (N) From MySQL thru post run code.
            # alpha [deg] (N) From MySQL thru post run code.
            # beta [deg] (N) From MySQL thru post run code.
            # gamma [deg] (N) From MySQL thru post run code.
            # insert [m] (N) From MySQL thru post run code.
        # pos (ST) Position of middle of Te probe.
            # r [m] (N) From MySQL thru post run code
            # phi [rad] (N) From MySQL thru post run code
            # z [m] (N) From MySQL thru post run code
        # bdot (ST)
            # ax1 [T/s] (S) gain * (raw_cw - raw_ccw)
                # gain [T/s/V] (N) From config.
                # raw_cw [V] (S) Channel from config.
                # raw_ccw [V] (S) Channel from config.
            # ax2 [T/s] (S)
                # ...
            # ax3 [T/s] (S)
                # ...
            # axr [T/s] (S) From post run code.
            # axphi [T/s] (S) From post run code.
            # axz [T/s] (S) From post run code.
        # tip01, tip02, ..., tip16 (ST)
            # signal (ST)
                # v [V] (S) (raw - avg)
                    # raw [V] (S) Channel from config.
                    # avg [V] (N) mean( raw[avg_index.start:avg_index.end] )
                # i [A] (S) signal.v / rdiggnd
            # noise (ST)
                # v [V] (S) noise.default
                    # raw [V] (S) Channel from config.
                    # avg [V] (N) mean( raw[avg_index.start:avg_index.end] )
                # i [A] (S) noise.v / rdiggnd
            # vgroupnoise (ST) (raw - avg - adjnoise) * factor
                # raw [V] (S) Channel from config.
                # avg [V] (N)
                # factor (N) From config.
                # adjnoise [V] (S) (raw - avg)
                    # raw [V] (S) Channel from config.
                    # avg [V] (N)
            # vbias [V] (N) (avg - offset) / div
                # raw [V] (S) Channel from config.
                # offset [V] (N) From config.
                # div (N) From config.
                # avg [V] (N)
            # rdiggnd [ohm] (N) Resistance from measured voltage to ground. From config.
            # rdiv [ohm] (N) Full resistance of divider + digitizer. From config.
            # rsense [ohm] (N) From config.
            # rwire [ohm] (N) From config.
            # relarea (N) From config.
            # working (N) From config.
            # side (N) This is a boolean deciding whether the tip is on side 0 or 1. From config.
            # vprobe [V] (S) (rdiv + rwire + rdiv * rwire / rsense) * (signal.i - noise.i) - rsense * noise.i + vbias
            # iprobe [V] (S) (signal.i - noise.i) * (rdiv + rsense) / (rsense * relarea)

    tree_name = "te_probe3"
    node_tag_prefix = "te3" # This is the prefix that goes in front of every tag in this tree.
    
    # Create the tree for editing.
    logging.info("Creating top of tree...")
    tree = mds.Tree(tree_name, shot=-1, mode="New")
    top = tree.getNode("\\top")
    tree.edit()

    # This is a tree of dictionaries that represent the tree. Sometimes we can get away with sets (just curly braces without 'key: value' pairs and only have {'key1', 'key2', ...}). If it is forced to be a dictionary then each element must be a 'key: value' pair and thus sometimes the value is None.
    # The key contains each node.
    tree_structure = {
        Node("avg_index", ST): {
            Node("start", N, "avg_start"),
            Node("end", N, "avg_end")
        },

        Node("act_area", N, NODE_NAME): None,

        Node("data", ST): {
            Node("vprobe", S, NODE_NAME),
            Node("iprobe", S, NODE_NAME),
            Node("working", S, NODE_NAME)
        },

        Node("port", ST): {
            Node("clock", N, NODE_NAME),
            Node("rport", N, NODE_NAME),
            Node("lat", N, NODE_NAME),
            Node("long", N, NODE_NAME),
            Node("alpha", N, NODE_NAME),
            Node("beta", N, NODE_NAME),
            Node("gamma", N, NODE_NAME),
            Node("insert", N, NODE_NAME)
        },

        Node("pos", ST): {
            Node("r", N, NODE_NAME),
            Node("phi", N, NODE_NAME),
            Node("z", N, NODE_NAME)
        },

        Node("bdot", ST): {
            Node("ax1", S, "bdot_ax1"): {
                Node("gain", N),
                Node("raw_cw", S),
                Node("raw_ccw", S)
            },
            Node("ax2", S, "bdot_ax2"): {
                Node("gain", N),
                Node("raw_cw", S),
                Node("raw_ccw", S)
            },
            Node("ax3", S, "bdot_ax3"): {
                Node("gain", N),
                Node("raw_cw", S),
                Node("raw_ccw", S)
            },
            Node("axr", S, "bdot_axr"): None,
            Node("axphi", S, "bdot_axphi"): None,
            Node("axz", S, "bdot_axz"): None
        }
    }

    # Each tip has it's own structure that is repeated for each tip.
    # Tips are prefixed as 01, ..., 16.
    tip_tree_structure = {
        Node("signal", ST, subtree_prefix='s'): {
            Node("v", S, NODE_NAME): {
                Node("raw", S),
                Node("avg", S),
            },
            Node("i", S, NODE_NAME): None,
        },
        Node("noise", ST, subtree_prefix='n'): {
            Node("v", S, NODE_NAME): {
                Node("raw", S),
                Node("avg", S),
            },
            Node("i", S, NODE_NAME): None,
        },
        Node("vgroupnoise", S, "vgrp"): {
            Node("raw", S, "vgrp_raw"): None,
            Node("avg", N, "vgrp_avg"): None,
            Node("factor", N, "vgrp_fac"): None,
            Node("adjnoise", S, "vgrp_adj"): {
                Node("raw", S),
                Node("avg", N)
            }
        },
        Node("vbias", N, "vbias"): {
            Node("raw", S, "vbias_raw"),
            Node("offset", N, "vbias_off"),
            Node("div", N, "vbias_div"),
            Node("avg", N, "vbias_avg")
        },
        Node("rdiggnd", N, NODE_NAME): None,
        Node("rdiv", N, NODE_NAME): None,
        Node("rsense", N, NODE_NAME): None,
        Node("rwire", N, NODE_NAME): None,
        Node("relarea", N, NODE_NAME): None,
        Node("working", N, NODE_NAME): None,
        Node("side", N, NODE_NAME): None,
        Node("vprobe", S, NODE_NAME): None,
        Node("iprobe", S, NODE_NAME): None
    }
            
    # Create non-tip structure.
    logging.info("Creating non-tip tree structure...")
    recursive_tree_build(tree, top, tree_structure, tag_prefix=node_tag_prefix + "_")

    # Create tip structure.
    logging.info("Creating tip tree structure...")
    for tip_index in range(1, 17):
        index_str = str(tip_index).zfill(2) # Pad the index with zeros on the left so that they are nicely ordered in the tree.
        # Create the tip node info.
        tip_name = 'tip{index}'.format(index=index_str)
        tip_node_type = ST
        tip_tag_prefix = '{node_prefix}_{tip_tag}_'.format(node_prefix=node_tag_prefix, tip_tag=index_str)

        # Add the tip to the top of the tree.
        tree.setDefault(top)
        logging.debug("Adding tip node " + tip_name)
        tree.addNode(tip_name, tip_node_type)
        tip_node = tree.getNode(tip_name)
        logging.info("Added tip node " + tip_node.getNodeName())
        
        recursive_tree_build(tree, tip_node, tip_tree_structure, tag_prefix=tip_tag_prefix)

    logging.info("Writing and printing tree...")
    tree.write()
    modules.mds_builders.traverse_tree(top)
    tree.cleanDatafile()
    tree.quit()

def populate_tree():
    """
    Run the top level populator that calls populators for first subtrees.
    """
    tree = mds.Tree("te_probe3", -1)
    top_node = tree.getNode("\\top")

    # Populate 'data'.
    tip_numbers = list(range(1, 17))
    tip_names = ['tip' + str(index).zfill(2) for index in tip_numbers]
    populate_data(top_node, tip_numbers, tip_names)

    # Populate 'bdot'.
    top_node = tree.getNode("\\top")
    populate_bdot(top_node)

    # Populate each tip.
    logging.debug('Populating tip nodes.')
    avg_index_node = top_node.getNode("avg_index")
    for tip_name in tip_names:
        tip_node = top_node.getNode(tip_name)
        populate_tip(tip_node, avg_index_node)

def populate_data(top_node, tip_numbers, tip_names):
    """
    Populate the functions for the nodes in 'data'.

    Parameters
    ----------
    top_node : mds.TreeNode
        Node at the top of the te_probe tree.
    tip_numbers : iter[int]
        An iterable that contains the numbers for each tip.
    tip_names : iter[str]
        An iterable that contains the names for each tip.
    """
    logging.debug("Populating 'data' children nodes.")
    # The data string that combines all tips: '[Data($1), Data($2), ..., Data($16)]'
    combine_tip_data_str = "[" + ", ".join(["DATA(${0:d})".format(i) for i in tip_numbers]) + "]"
    
    # The node suffix looks like 'side' or 'vsense.div' so that the full name looks like 'tip01.side'.
    tip_node_names = lambda node_suffix: ['{name}.{suffix}'.format(name=tip_name, suffix=node_suffix) for tip_name in tip_names]

    # Get the reference to each tip signal.
    vprobe_nodes = modules.mds_builders.get_node_refs(top_node, *tip_node_names('vprobe'))
    iprobe_nodes = modules.mds_builders.get_node_refs(top_node, *tip_node_names('iprobe'))
    working_nodes = modules.mds_builders.get_node_refs(top_node, *tip_node_names('working'))

    # Compile the commands.
    # Dimension of first signal.
    dimension_str = 'DIM_OF($1)'
    logging.debug("Compiling 'data' commands.")
    vprobe_command = mds.Data.compile('BUILD_SIGNAL({combine_tips}, *, {dimension})'.format(combine_tips=combine_tip_data_str, dimension=dimension_str), *vprobe_nodes)
    iprobe_command = mds.Data.compile('BUILD_SIGNAL({combine_tips}, *, {dimension})'.format(combine_tips=combine_tip_data_str, dimension=dimension_str), *iprobe_nodes)
    working_command = mds.Data.compile('BUILD_SIGNAL({combine_tips}, *, {dimension})'.format(combine_tips=combine_tip_data_str, dimension=dimension_str), *working_nodes)
    
    # Get the nodes where these commands go.
    data_node = top_node.getNode("data")
    logging.debug("Getting nodes to insert commands into.")
    vprobe_combined_node = data_node.getNode("vprobe")
    iprobe_combined_node = data_node.getNode("iprobe")
    working_combined_node = data_node.getNode("working")

    # Put the commands into the nodes.
    logging.debug("Inserting commands.")
    modules.mds_builders.put_wrapper(vprobe_combined_node, vprobe_command, units='V')
    modules.mds_builders.put_wrapper(iprobe_combined_node, iprobe_command, units='A')
    modules.mds_builders.put_wrapper(working_combined_node, working_command)
    logging.info("Populated 'data' children nodes.")

def populate_bdot(top_node):
    bdot_node = top_node.getNode("bdot")
    
    for numbered_axis in ["ax1", "ax2", "ax3"]:
        axis_node = bdot_node.getNode(numbered_axis)

        gain_node = axis_node.getNode("gain")
        raw_clockwise_node = axis_node.getNode("raw_cw")
        raw_counterclockwise_node = axis_node.getNode("raw_ccw")

        axis_command = mds.Data.compile("BUILD_SIGNAL($1 * ($2 - $3),, DIM_OF($2))", gain_node, raw_clockwise_node, raw_counterclockwise_node)
        modules.mds_builders.put_wrapper(axis_node, axis_command, units="T/s")

def populate_tip(tip_node, avg_index_node):
    """
    Populate the functions for the nodes in a single tip.

    Parameters
    ----------
    tip_node : mds.TreeNode
        Top node for specific probe tip.
    avg_index_node : mds.TreeNode
        Parent node of indeces to average over.
    """
    # TODO: Fix this stuff to use new method.
    logging.debug("Populating {tip_name} nodes.".format(tip_name=tip_node.getNodeName()))
    # Child nodes of this tip.
    logging.debug("Getting nodes to insert commands into and nodes used in commands.")
    signal_node = tip_node.getNode("signal")
    noise_node = tip_node.getNode("noise")
    vgroupnoise_node = tip_node.getNode("vgroupnoise")
    vbias_node = tip_node.getNode("vbias")
    rdiggnd_node = tip_node.getNode("rdiggnd")
    rdiv_node = tip_node.getNode("rdiv")
    rsense_node = tip_node.getNode("rsense")
    rwire_node = tip_node.getNode("rwire")
    relarea_node = tip_node.getNode("relarea")
    working_node = tip_node.getNode("working")
    side_node = tip_node.getNode("side")
    vprobe_node = tip_node.getNode("vprobe")
    iprobe_node = tip_node.getNode("iprobe")

    # Populate nodes.
    logging.debug("Inserting commands.")
    populate_signal_noise(signal_node, rdiggnd_node, avg_index_node, is_signal=True)
    populate_signal_noise(noise_node, rdiggnd_node, avg_index_node, is_signal=True)
    populate_vgroupnoise(vgroupnoise_node, avg_index_node)
    populate_vbias(vbias_node, avg_index_node)
    populate_vprobe(vprobe_node, signal_node.getNode("i"), noise_node.getNode("i"), 
        vbias_node, rdiv_node, rwire_node, rsense_node)
    populate_iprobe(iprobe_node, signal_node.getNode("i"), noise_node.getNode("i"),
        rdiv_node, rsense_node, relarea_node)
    logging.info("Populated {tip_name} children nodes.".format(tip_name=tip_node.getNodeName()))
    
def populate_signal_noise(signal_noise_node, rdiggnd_node, avg_index_node, is_signal):
    """
    Populate the functions for the nodes in 'signal' or 'noise'.

    Parameters
    ----------
    signal_noise_node : mds.TreeNode
        Either the signal or noise node in a tip.
    rdiggnd_node : mds.TreeNode
        Resistance between digitizer and ground node.
    avg_index_node : mds.TreeNode
        Parent node of indeces to average over.
    is_signal : bool
        Whether we are populating the signal or noise sub-tree.
    """
    if is_signal:
        logging.debug("Populating 'signal'.")
    else:
        logging.debug("Populating 'noise'.")

    # Child nodes of 'signal' or 'noise'.
    v_node = signal_noise_node.getNode('v')
    i_node = signal_noise_node.getNode('i')

    # Child nodes of 'v'.
    raw_node = v_node.getNode('raw')
    avg_node = v_node.getNode('avg')

    # Insert the averaging equation into the avg node.
    modules.mds_builders.put_wrapper(avg_node, average_equation(raw_node, avg_index_node), units='V')

    # Insert `raw - avg` into the v node.
    v_equation = mds.Data.compile("BUILD_SIGNAL($1 - $2,, DIM_OF($1))", raw_node, avg_node)
    modules.mds_builders.put_wrapper(v_node, v_equation, units='V')

    # Insert `v / rdiggnd` into the i node.
    i_equation = mds.Data.compile("BUILD_SIGNAL($1 / $2,, DIM_OF($1))", v_node, rdiggnd_node)
    modules.mds_builders.put_wrapper(i_node, i_equation, units='A')

    if is_signal:
        logging.debug("Populated 'signal'.")
    else:
        logging.debug("Populated 'noise'.")

def populate_vgroupnoise(vgroupnoise_node, avg_index_node):
    """
    Populate the functions for the nodes in 'vgroupnoise'.

    Parameters
    ----------
    vgroupnoise_node : mds.TreeNode
        'vgroupnoise' node for some tip.
    avg_index_node : mds.TreeNode
        Parent node of indeces to average over.
    """
    logging.debug("Populating 'vgroupnoise'.")
    # Child nodes of 'vgroupnoise'.
    raw_node = vgroupnoise_node.getNode('raw')
    avg_node = vgroupnoise_node.getNode('avg')
    adjnoise_node = vgroupnoise_node.getNode("adjnoise")
    factor_node = vgroupnoise_node.getNode('factor')

    # Insert '(raw - avg - noise) * factor' into 'vgroupnoise'.
    vgroupnoise_equation = mds.Data.compile("BUILD_SIGNAL(($1 - $2 - $3) * $4,, DIM_OF($1))", raw_node, avg_node, adjnoise_node, factor_node)
    modules.mds_builders.put_wrapper(vgroupnoise_node, vgroupnoise_equation, units='V')

    # Insert the averaging equation that averages part of the raw signal.
    modules.mds_builders.put_wrapper(avg_node, average_equation(raw_node, avg_index_node), units='V')

    populate_adjnoise(adjnoise_node, avg_index_node)
    logging.debug("Populated 'vgroupnoise'.")

def populate_adjnoise(adjnoise_node, avg_index_node):
    """
    Populate the functions for the nodes in 'adjnoise'.
    """
    logging.debug("Populating 'adjnoise'.")
    raw_node = adjnoise_node.getNode('raw')
    avg_node = adjnoise_node.getNode('avg')

    # Populate the 'adjnoise' node with 'raw - avg'.
    adjnoise_equation = mds.Data.compile("BUILD_SIGNAL($1 - $2,, DIM_OF($1))", raw_node, avg_node)
    modules.mds_builders.put_wrapper(adjnoise_node, adjnoise_equation, units='V')
    
    # Populate the 'avg' node.
    modules.mds_builders.put_wrapper(avg_node, average_equation(raw_node, avg_index_node), units='V')
    logging.debug("Populated 'adjnoise'.")

def populate_vbias(vbias_node, avg_index_node):
    """
    Populate the functions for the nodes in 'vbias'.
    """
    logging.debug("Populating 'vbias'.")
    # Child nodes of 'vbias'.
    raw_node = vbias_node.getNode('raw')
    offset_node = vbias_node.getNode('offset')
    div_node = vbias_node.getNode('div')
    avg_node = vbias_node.getNode('avg')

    # Insert '(avg - offset) / div' into 'vbias'.
    vbias_equation = mds.Data.compile("($1 - $2) / $3", avg_node, offset_node, div_node)
    modules.mds_builders.put_wrapper(vbias_node, vbias_equation, units='V')

    # Insert into 'avg' node.
    modules.mds_builders.put_wrapper(avg_node, average_equation(raw_node, avg_index_node), units='V')
    logging.debug("Populated 'vbias'.")

def populate_vprobe(vprobe_node, signal_i_node, noise_i_node, vbias_node, rdiv_node, rwire_node, rsense_node):
    """
    Populate the function for the 'vprobe' node.
    """
    logging.debug("Populating 'vprobe'.")
    # 1. signal_i_node, 2. noise_i_node, 3. vbias_node, 4. rdiv_node, 5. rwire_node, 6. rsense_node
    vprobe_equation = mds.Data.compile(
        "BUILD_SIGNAL(($4 + $5 + $4 * $5 / $6) * ($1 - $2) - $6 * $2 + $3,, DIM_OF($1))",
        signal_i_node, noise_i_node, vbias_node, rdiv_node, rwire_node, rsense_node
    )
    modules.mds_builders.put_wrapper(vprobe_node, vprobe_equation, units='V')
    logging.debug("Populated 'vprobe'.")

def populate_iprobe(iprobe_node, signal_i_node, noise_i_node, rdiv_node, rsense_node, relarea_node):
    """
    Populate the function for the 'iprobe' node.
    """
    logging.debug("Populating 'iprobe'.")
    # 1. signal_i_node, 2. noise_i_node, 3. rdiv_node, 4. rsense_node, 5. relarea_node
    iprobe_equation = mds.Data.compile(
        "BUILD_SIGNAL(($1 - $2) * ($3 + $4) / ($4 * $5),, DIM_OF($1))",
        signal_i_node, noise_i_node, rdiv_node, rsense_node, relarea_node
    )
    modules.mds_builders.put_wrapper(iprobe_node, iprobe_equation, units='A')
    logging.debug("Populated 'iprobe'.")

def average_equation(signal_node, avg_index_node):
    """
    Create the compiled function for averaging a signal.
    """
    logging.debug("Making averaging equation.")
    start_node = avg_index_node.getNode("start")
    end_node = avg_index_node.getNode("end")
    return mds.Data.compile("MEAN(DATA($1)[$2 : $3])", signal_node, start_node, end_node)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, filename='./create_logs/te_probe3_create_log.txt', filemode='w')

    build_structure()
    populate_tree()
    logging.info("te_probe3 tree created!")
