import MDSplus as mds
import modules.mds_builders as mdsbuild




def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"

    magnetrons = ["magnetron_{0:d}".format(i) for i in range(1,6)]
    mag_nodes = [("forward", s), ("reflected", s), ("transmitted", s)]
    power_nodes = [("v_raw", s),
                 ("offset", n),
                 ("v_cor", s),
                 ("A", n),
                 ("B", n),
                 ("C", n),
                 ("atten", n),
                 ("det_pow", s)]
    myTree = mds.Tree(tree="magnetrons", shot=-1, mode="New")
    myTree.edit()
    top = myTree.getNode("\\top")
    myTree.addNode("run_proc", "action")
    for mag_node_name in magnetrons:
        myTree.addNode(mag_node_name, st)
        mag_node_ref = myTree.getNode(mag_node_name)
        myTree.setDefault(mag_node_ref)
        for (node_name, use) in mag_nodes:
            myTree.addNode(node_name, use)
            myTree.setDefault(myTree.getNode(node_name))
            if node_name in ["forward", "reflected"]:
                for (sub_name, sub_use) in power_nodes:
                    #node_str_name = "{0:s}:{1:s}".format(node_name, sub_name)
                    #node_tag_name = "{0:s}_{1:s}".format(node_name, sub_name) #this might be too long!
                    myTree.addNode(sub_name, sub_use)
                    #n = myTree.getNode(node_str_name)
                    #n.addTag(node_tag_name)
            myTree.setDefault(mag_node_ref) 
        myTree.setDefault(top)
    myTree.write()
    myTree.quit()


def populate_tdi():
    magnetron_tree = mds.Tree("magnetrons", 29214)
    top = magnetron_tree.getNode("\\top")
    magnetrons = ["magnetron_{0:d}".format(i) for i in range(1,6)]
    sub_trees = ["forward", "reflected"] #, ("transmitted", s)]

    power_nodes = [
                 "v_raw",
                 "offset",
                 "v_cor",
                 "A",
                 "B",
                 "C",
                 "atten",
                 "det_pow",
                ]

    for mag in magnetrons:
        mag_node = magnetron_tree.getNode(mag)
        magnetron_tree.setDefault(mag_node)
        for sub_node in sub_trees:
            # Want to reference power nodes relative to the sub trees forward and reflected
            print(sub_node, mag)
            sub_node_ref = magnetron_tree.getNode(sub_node)
            magnetron_tree.setDefault(sub_node_ref)

            # all of this code is more or less fine, just need to grab the right nodes for each power sub tree
            v_raw, offset, v_cor, A, B, C, atten, det_power = mdsbuild.get_node_refs(
                    sub_node_ref, *power_nodes)

            v_raw_sd, offset_sd, v_cor_sd, A_sd, B_sd, C_sd, atten_sd, det_power_sd = mdsbuild.get_error_refs(
                    v_raw, offset, v_cor, A, B, C, atten, det_power)

            v_cor_sig = mds.Data.compile("BUILD_SIGNAL($1-$2,*,DIM_OF($1))", v_raw, offset)
            error_string = "BUILD_SIGNAL(SQRT($2^2),*,DIM_OF($1))"
            v_cor_sig_sd = mds.Data.compile(error_string, v_raw_sd, offset_sd)


            # I am assuming that the calibration is from V to mW (maybe that should change?)
            det_power_sig = mds.Data.compile("BUILD_SIGNAL(($2*$1^2+$3*$1+$4)/1000.0,*,DIM_OF($1))",
                    v_cor, A, B, C)
            """
            1: vcor, 2:vcor_sd
            3: A, 4: A_sd
            5: B, 6: B_sd
            7: C_sd
            """
            error_string = ("BUILD_SIGNAL(0.001*SQRT("
                            "($1 * $4)^2 + "
                            "(2*$3*$1*$2)^2 + "
                            "($6*$1)^2 * (($6/$5)^2 + ($2/$1)^2) + "
                            "$7^2"
                            "),*,DIM_OF($1))"
                            )
            det_power_sig_sd = mds.Data.compile(error_string, v_cor, v_cor_sd, A, A_sd, B, B_sd, C_sd)

            F_sig = mds.Data.compile("BUILD_SIGNAL($1*10^($2/10.0),*,DIM_OF($1))", det_power, atten) 
            """
            1: det_power, 2: det_power_sd
            3: atten, 4: atten_sd
            """
            error_string = ("BUILD_SIGNAL(SQRT("
                            "10^($3/5)*$2^2 + "
                            "($1 * 10^(($3-10.0)/10.0) * LOG(10))^2 * $4^2"
                            "),*,DIM_OF($1))"
                            )
            F_sig_sd = mds.Data.compile(error_string, det_power, det_power_sd, atten, atten_sd)

            mdsbuild.put_wrapper(v_cor, v_cor_sig, err=v_cor_sig_sd, units="V")
            mdsbuild.put_wrapper(det_power, det_power_sig, err=det_power_sig_sd, units="W")
            mdsbuild.put_wrapper(sub_node_ref, F_sig, err=F_sig_sd, units="W")
            magnetron_tree.setDefault(mag_node)


        magnetron_tree.setDefault(mag_node)
        t, r, f = mdsbuild.get_node_refs(mag_node, "transmitted", "reflected", "forward")
        t_sd, r_sd, f_sd = mdsbuild.get_error_refs(t,r,f)

        t_sig = mds.Data.compile("BUILD_SIGNAL($1-$2,*,DIM_OF($1))", f, r)
        error_string = "BUILD_SIGNAL(SQRT($1^2 + $2^2),*,DIM_OF($1))"
        t_sig_err = mds.Data.compile(error_string, f_sd, r_sd)

        mdsbuild.put_wrapper(t, t_sig, err=t_sig_err, units="W")
        magnetron_tree.setDefault(top)


if __name__ == "__main__":
    build_structure()
    populate_tdi()
    #print("magnetron tree created!")
