import MDSplus as mds
import modules.mds_builders as mdsbuild
import sys

def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    act = "action"

    # speed_bdot1 tree structure
    # Top
	#  data
    #  	 dbr, dbphi, dbz
    #    db1a, db1b, db2, db3
	#  pos
    #    r
    #    phi
    #    z
    #  Port
    #   rport
    #   lat
    #   long
    #   alpha
    #   beta
    #   gamma
    #   clock
    #   insert
    #  Pb_01
    #    dbr, dbphi, dbz
    #    r, phi, z
    #    db1a
    #      raw, gain
    #    db1b
    #      ..
    #  Pb_02
    #  ...
	
    # create node, type, tag
    topSts = [("data",st), ("pos",st), ("port",st)]
    dataNodes = [("dbr",s,"dbr"), ("dbphi",s,"dbphi"), ("dbz",s,"dbz"), ("db2",s,"db2"), ("db1a",s,"db1a"), ("db1b",s,"db1b"), ("db3",s,"db3")]
    posNodes = [("r",n,"r"), ("phi",n,"phi"), ("z",n,"z")]
    portNodes = [("insert",n,"insert"), ("clock",n,"clock"), ("rport",n,"rport"), ("lat",n,"lat"), ("long",n,"long"), ("alpha",n,"alpha"), ("beta",n,"beta"), ("gamma",n,"gamma")]
    
    pbNodes = [("dbr",s,"dbr"), ("dbphi",s,"dbphi"), ("dbz",s,"dbz"), ("r",n,"r"), ("phi",n,"phi"), ("z",n,"z"), ("db2",s,"db2"), ("db1a",s,"db1a"), ("db1b",s,"db1b"), ("db3",s,"db3") ]
    axNodes = [("raw",s,None), ("gain",n,None)]
    nameTag = "speed_bdot1"
    
    tr = mds.Tree("speed_bdot1", shot=-1, mode="New")
    top = tr.getNode("\\top")
    tr.edit()
    
    # create top level structures and nodes
    for (tnm, tuse) in topSts:
        tr.addNode(tnm, tuse)
        
        # add nodes
        stNd = tr.getNode(tnm)
        tr.setDefault(stNd)
        if tnm=="data":
            inNodes = dataNodes
        elif tnm=="pos":
            inNodes = posNodes
        elif tnm=="port":
            inNodes = portNodes
        for (ndnm, nduse, ndtag) in inNodes:
            tempNd = tr.addNode(ndnm, nduse)
            tempNd.addTag("{0}_{1}".format(nameTag, ndtag))
        tr.setDefault(top)
            
    # add individual probe nodes
    numpbs = 16;
    pbRange = range(1,numpbs+1)
    pbNames = ["pb{0:02d}".format(i) for i in pbRange]
    for pbnm in pbNames:
        tr.addNode(pbnm, st)
        pbNd = tr.getNode(pbnm)
        tr.setDefault(pbNd)
        for (pbNdnm, pbNduse, pbNdtag) in pbNodes:
            tempNd = tr.addNode(pbNdnm, pbNduse)
            if pbNdtag is not None:
                tempNd.addTag("{0}_{1}_{2}".format(nameTag, pbnm, pbNdtag))
        # add db_axis nodes
        for (dbNdnm,__,__) in pbNodes[-4:]:
            dbNd = pbNd.getNode(dbNdnm)
            tr.setDefault(dbNd)
            for (axNdnm, axNduse, axNdtag) in axNodes:
                tempNd = tr.addNode(axNdnm, axNduse)
        tr.setDefault(top)
    
    # mdsbuild.make_tree_write_once(tr)
    # unlocked_nodes = (...)
    # mdsbuild.unlock_nodes(mdsbuild.get_node_refs(top,*unlocked_nodes))
    tr.write()
    mdsbuild.traverse_tree(top)
    tr.cleanDatafile()
    tr.quit()
    return

def populate_tdi():
    spdTree = mds.Tree("speed_bdot1",-1)
    top = spdTree.getNode("\\top")
    
    # populate array nodes under data structure
    datarefs = ["db2", "db1a", "db1b", "db3", "dbr", "dbphi", "dbz"]
    datarefs = ["data:" + i for i in datarefs]
    db2,db1a,db1b,db3,dbr,dbphi,dbz = mdsbuild.get_node_refs(top, *datarefs)
    
    numpbs = 16;
    pbnums = range(1,numpbs+1)
    
    b1nodes = mdsbuild.get_node_refs(top, *tuple("pb{0:02d}.db2".format(i) for i in pbnums))
    b2anodes = mdsbuild.get_node_refs(top, *tuple("pb{0:02d}.db1a".format(i) for i in pbnums))
    b2bnodes = mdsbuild.get_node_refs(top, *tuple("pb{0:02d}.db1b".format(i) for i in pbnums))
    b3nodes = mdsbuild.get_node_refs(top, *tuple("pb{0:02d}.db3".format(i) for i in pbnums))
    brnodes = mdsbuild.get_node_refs(top, *tuple("pb{0:02d}.dbr".format(i) for i in pbnums))
    bphinodes = mdsbuild.get_node_refs(top, *tuple("pb{0:02d}.dbphi".format(i) for i in pbnums))
    bznodes = mdsbuild.get_node_refs(top, *tuple("pb{0:02d}.dbz".format(i) for i in pbnums))
    
    data_str = "[" + ",".join(["DATA(${0:d})".format(i) for i in pbnums]) + "]"
    # use only dimension of first probe for data
    # dim_str = "IF_ERROR(" + ",".join(["DATA(DIM_OF(${0:d}))".format(i) for i in pbnums]) + ")"
    dim_str = "DIM_OF($1)"
    
    b1arr = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str, dim_str), *b1nodes)
    b2aarr = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str, dim_str), *b2anodes)
    b2barr = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str, dim_str), *b2bnodes)
    b3arr = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str, dim_str), *b3nodes)
    brarr = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str, dim_str), *brnodes)
    bphiarr = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str, dim_str), *bphinodes)
    bzarr = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str, dim_str), *bznodes)
    
    mdsbuild.put_wrapper(db2, b1arr, units="T/s")
    mdsbuild.put_wrapper(db1a, b2aarr, units="T/s")
    mdsbuild.put_wrapper(db1b, b2barr, units="T/s")
    mdsbuild.put_wrapper(db3, b3arr, units="T/s")
    mdsbuild.put_wrapper(dbr, brarr, units="T/s")
    mdsbuild.put_wrapper(dbphi, bphiarr, units="T/s")
    mdsbuild.put_wrapper(dbz, bzarr, units="T/s")



    # populate nodes under pos structure
    posrefs = ["r", "phi", "z"]
    posrefs = ["pos." + i for i in posrefs]
    r,phi,z = mdsbuild.get_node_refs(top,*posrefs)
    
    rnodes = mdsbuild.get_node_refs(top,*tuple("pb{0:02d}.r".format(i) for i in pbnums))
    phinodes = mdsbuild.get_node_refs(top,*tuple("pb{0:02d}.phi".format(i) for i in pbnums))
    znodes = mdsbuild.get_node_refs(top,*tuple("pb{0:02d}.z".format(i) for i in pbnums))
    
    rdata = mds.Data.compile("{0}".format(data_str), *rnodes)
    phidata = mds.Data.compile("{0}".format(data_str), *phinodes)
    zdata = mds.Data.compile("{0}".format(data_str), *znodes)
    
    mdsbuild.put_wrapper(r, rdata, units="m")
    mdsbuild.put_wrapper(phi, phidata, units="deg")
    mdsbuild.put_wrapper(z, zdata, units="m")
    
    
    
    # populate raw data nodes under probe structures
    dbs = ["db2", "db1a", "db1b", "db3"]
    for ch in top.getChildren():
        chnm = ch.getNodeName().lower()
        if "pb" in chnm:
            for dbnm in dbs:
                # get node references
                rawnd, gainnd = mdsbuild.get_node_refs(ch, dbnm + ".raw", dbnm + ".gain")
                dbNd = ch.getNode(dbnm)
                # compile TDI signals and put in nodes
                dbsig = mds.Data.compile("BUILD_SIGNAL($1*$2, , DIM_OF($2))", gainnd, rawnd)
                mdsbuild.put_wrapper(dbNd, dbsig, units="T/s")
    
        

    return

if __name__ == "__main__":
    build_structure()
    populate_tdi()
    print("speed_bdot1 tree created!")
