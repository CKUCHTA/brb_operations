import MDSplus as mds
import modules.mds_builders as mdsbuild
import numpy as np

def build_structure():
    ############# tree structure ###############
    # top
    #   r_pivot
    #   theta_pivot
    #   phi_pivot
    #   r0
    #   delta_theta
    #   oddAtoB_vec
    #   evenAtoB_vec
    #   v_locs 3x8
    #   nt_locs 3x4
    #   run_proc
    #   load_config
    #   gamma_e
    #   gamma_i
    #   Ti
    #   Z
    #   mu_i
    #   mp_1
    #       loc
    #       mach_offset
    #       mach = 0.45*(ln(Ia/Ib) - ma_offset) (flow from a to b)
    #       cs(m/s)
    #       ne = (i_a + i_b)/2 * 1.0/(0.6*qe*cs*area)
    #       velocity(m/s) = mach*cs
    #       i_a/i_b = (raw - raw_offset)/resistor
    #           raw
    #           raw_offset
    #           resistor
    #           vbatt
    #           area
    #   mp_2
    #   mp_3
    #   mp_4
    #   mp_5
    #   mp_6
    #   mp_7
    #   mp_8
    #   tp_1
    #       loc
    #       te = dV/ln(2) (eV)
    #       cs = 9790.0*sqrt((gamma_e*Z*te + gamma_i*ti)/mu_i)
    #       ne = Isat/eAcs
    #       isat
    #           raw
    #           raw_offset
    #           r1
    #           area
    #           vbatt
    #       deltav (v+ - vf) = (raw-raw_offset)*(r2+r3)/r2
    #           raw
    #           raw_offset
    #           r2
    #           r3
    #           area
    #       vfloat = (raw-raw_offset)*(r4+r5)/r4
    #           raw
    #           raw_offset
    #           r4
    #           r5
    #           area
    #   tp_2
    #   tp_3
    #   tp_4
    #   xtip
    #       area
    #       v
    #           raw
    #           raw_offset
    #           gain
    #       i
    #           raw
    #           raw_offset
    #           gain
    #
    ##################################################
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    act = "action"

    top_nodes = [("r_pivot",n),("theta_pivot",n),("phi_pivot",n),("r0",n),("delta_theta",n),
            ("oddAtoB_vec",n),("evenAtoB_vec",n),("v_locs",n),("nt_locs",n),("gamma_e",n),
            ("gamma_i",n),("Z",n),("ti",n),("mu_i",n),("load_config",act),("run_proc",act)]

    mp_nodes = [("loc",n),("mach_offset",n),("mach",s),("cs",s),("ne",s),("velocity",s),("i_a",s),("i_b",s)]
    side_nodes = [("raw",s),("raw_offset",n),("resistor",n),("vbatt",n),("area",n),("calfac",n)]

    tp_nodes = [("loc",n),("te",s),("cs",s),("ne",s),("isat",s),("deltav",s),("vfloat",s)]
    isat_nodes = [("raw",s),("r1",n),("raw_offset",n),("area",n),("vbatt",n),("calfac",n)]
    deltav_nodes = [("raw",s),("r2",n),("r3",n),("raw_offset",n)]
    vfloat_nodes = [("raw",s),("r4",n),("r5",n),("raw_offset",n),("area",n)]

    xtip_nodes = [("v",s),("i",s),("area",n),("loc",n),("te",s),("ne",s),("cs",s),("vfloat",s)]
    sigv_nodes = [("raw",s),("raw_offset",n),("gain",n)]
    sigi_nodes = [("raw",s),("raw_offset",n),("rsweep",n)]

    myTree = mds.Tree(tree="quad_mach_nt",shot=-1,mode="New")
    top = myTree.getNode("\\top")
    top.addTag("qmnt")
    myTree.edit()
    # create top level data nodes
    for (name,use) in top_nodes:
        tempnode = myTree.addNode(name,use)
        tempnode.addTag("qmnt_{0}".format(name))
        if "locs" in name:
            tempnode.setWriteOnce(True)

    # loop through individual 2-D mach probes
    mach_nodes = ["mp_{0:d}".format(i) for i in range(1,9)]
    for mach_id in mach_nodes:
        mp_node = myTree.addNode(mach_id,st)
        mp_node.addTag("qmnt_{0}".format(mach_id))
        ## add nodes to this probe
        myTree.setDefault(mp_node)
        for (name,use) in mp_nodes:
            tempnode = myTree.addNode(name,use)
            tempnode.addTag("qmnt_{0}_{1}".format(mach_id,name))
            if name not in ["mach_offset","cs"]:
                tempnode.setWriteOnce(True)
            if name in ["i_a","i_b"]:
                myTree.setDefault(tempnode)
                for (subname,use) in side_nodes:
                    subnode = myTree.addNode(subname,use)
                    subnode.addTag("qmnt_{0}_{1}_{2}".format(mach_id,name,subname[0:7]))
            myTree.setDefault(mp_node)
        myTree.setDefault(top)

    # loop through triple probe nodes
    for tp_id in ["tp_{0:d}".format(i) for i in range(1,5)]:
        tp_node = myTree.addNode(tp_id,st)
        tp_node.addTag("qmnt_{0}".format(tp_id))
        ## add nodes to this probe
        myTree.setDefault(tp_node)
        for (name,use) in tp_nodes:
            tempnode = myTree.addNode(name,use)
            tempnode.addTag("qmnt_{0}_{1}".format(tp_id,name[0:7]))
            if name != "cs":
                tempnode.setWriteOnce(True)
        isat_node = myTree.getNode("isat")
        deltav_node = myTree.getNode("deltav")
        vfloat_node = myTree.getNode("vfloat")
        myTree.setDefault(isat_node)
        for (name,use) in isat_nodes:
            tempnode = myTree.addNode(name,use)
            tempnode.addTag("qmnt_{0}_isat_{1}".format(tp_id,name[0:7]))
        myTree.setDefault(deltav_node)
        for (name,use) in deltav_nodes:
            tempnode = myTree.addNode(name,use)
            tempnode.addTag("qmnt_{0}_deltav_{1}".format(tp_id,name[0:7]))
        myTree.setDefault(vfloat_node)
        for (name,use) in vfloat_nodes:
            tempnode = myTree.addNode(name,use)
            tempnode.addTag("qmnt_{0}_vfloat_{1}".format(tp_id,name[0:7]))
        myTree.setDefault(top)

    # add extra tip nodes
    for xnode in ["xtip","xtip_t5"]:
        xtip_node = myTree.addNode(xnode,st)
        print(xtip_node)
        myTree.setDefault(xtip_node)
        for (name,use) in xtip_nodes:
            tempnode = myTree.addNode(name,use)
            tempnode.addTag("qmnt_{0}_{1}".format(xnode,name))
            if name == "i":
                myTree.setDefault(tempnode)
                for (subname,subuse) in sigi_nodes:
                    subnode = myTree.addNode(subname,subuse)
                    subnode.addTag("qmnt_{0}_{1}_{2}".format(xnode,name,subname[0:7]))
                myTree.setDefault(xtip_node)
            elif name == "v":
                myTree.setDefault(tempnode)
                for (subname,subuse) in sigv_nodes:
                    subnode = myTree.addNode(subname,subuse)
                    subnode.addTag("qmnt_{0}_{1}_{2}".format(xnode,name,subname[0:7]))
                myTree.setDefault(xtip_node)
            else:
                myTree.setDefault(xtip_node)
        myTree.setDefault(top)

    myTree.setDefault(top)
    myTree.write()
    myTree.cleanDatafile()
    myTree.quit()
    return

def populate_tdi():
    myTree = mds.Tree("quad_mach_nt",-1)
    myTree.edit()
    top = myTree.getNode("\\top")
    # gather top level node references and error references
    r0,delta_theta,rpivot,thetapivot = mdsbuild.get_node_refs(top,"r0","delta_theta","r_pivot","theta_pivot")
    r0_sd,delta_theta_sd,rpivot_sd,thetapivot_sd = mdsbuild.get_error_refs(r0,delta_theta,rpivot,thetapivot)
    gam_e,gam_i,mu_i,ti,z,eatobvec,oatobvec = mdsbuild.get_node_refs(top,"gamma_e","gamma_i","mu_i","ti","z","evenatob_vec","oddatob_vec")
    nt_locs,v_locs = mdsbuild.get_node_refs(top,"nt_locs","v_locs")
    ti_sd,eatobvec_sd,oatobvec_sd,nt_locs_sd,v_locs_sd,z_sd = mdsbuild.get_error_refs(ti,eatobvec,oatobvec,nt_locs,v_locs,z)

    # build v_locs tdi expression
    mplocnodes = mdsbuild.get_node_refs(top,*tuple("mp_{0:d}.loc".format(i) for i in range(1,9)))
    data_str ="["+",".join(["DATA(${0:d})".format(i) for i in range(1,9)])+"]"
    err_str ="["+",".join(["DATA(ERROR_OF(${0:d}))".format(i) for i in range(1,9)])+"]"
    dim_str = "IF_ERROR(" + ",".join(["DATA(DIM_OF(${0:d}))".format(i) for i in range(1,9)])+")"
    locs_dat = mds.Data.compile("{0}".format(data_str),*mplocnodes)
    locs_sd = mds.Data.compile("{0}".format(err_str),*mplocnodes)
    mdsbuild.put_wrapper(top.getNode("v_locs"),locs_dat,err=locs_sd,units="m")

    # build nt_locs tdi expression
    tplocnodes = mdsbuild.get_node_refs(top,*tuple(["xtip.loc"]+["tp_{0:d}.loc".format(i) for i in range(1,5)]))
    data_str ="["+",".join(["DATA(${0:d})".format(i) for i in range(1,6)])+"]"
    err_str ="["+",".join(["DATA(ERROR_OF(${0:d}))".format(i) for i in range(1,6)])+"]"
    dim_str = "IF_ERROR(" + ",".join(["DATA(DIM_OF(${0:d}))".format(i) for i in range(1,6)])+")"
    locs_dat = mds.Data.compile("{0}".format(data_str),*tplocnodes)
    locs_sd = mds.Data.compile("{0}".format(err_str),*tplocnodes)
    mdsbuild.put_wrapper(top.getNode("nt_locs"),locs_dat,err=locs_sd,units="m")

    # loop through all mach probes and store tdi expressions
    mps = ["mp_{0:d}".format(i) for i in range(1,9)]
    mp_dist = .01456 + .01*np.array([1.5,2.0,5.5,6.0,9.5,10.0,13.5,14.0])
    for idx,(mp,mpd) in enumerate(zip(mps,mp_dist)):
        mp_node = myTree.getNode(mp)
        myTree.setDefault(mp_node)
        for side in ["i_a","i_b"]:
            side_node = myTree.getNode(side)
            raw, raw_offset, resistor, calfac = mdsbuild.get_node_refs(side_node,"raw","raw_offset","resistor","calfac")
            raw_sd, raw_offset_sd, resistor_sd, calfac_sd = mdsbuild.get_error_refs(raw,raw_offset,resistor,calfac)
            cur_sig = mds.Data.compile("$4*($1-$2)/$3",raw,raw_offset,resistor,calfac)
            cur_sd_exp = mds.Data.compile("SQRT(($6*$4/$3)^2 + ($6*($1-$2)*$5/$3^2)^2 + ($7*($1-$2)/$3)^2)",raw,raw_offset,resistor,raw_offset_sd,resistor_sd,calfac,calfac_sd)
            cur_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",cur_sd_exp,raw)
            mdsbuild.put_wrapper(side_node,cur_sig,err=cur_sd,units="A")
            myTree.setDefault(mp_node)
        velocity,cs,mach,mach_offset,i_a,i_b,loc = mdsbuild.get_node_refs(mp_node,"velocity","cs","mach","mach_offset","i_a","i_b","loc")
        velocity_sd,cs_sd,mach_sd,mach_offset_sd,i_a_sd,i_b_sd,loc_sd = mdsbuild.get_error_refs(velocity,cs,mach,mach_offset,i_a,i_b,loc)
        ne, area_a, area_b = mdsbuild.get_node_refs(mp_node,"ne","i_a:area","i_b:area")
        ne_sd, area_a_sd, area_b_sd = mdsbuild.get_error_refs(ne,area_a,area_b)
        mach_sig = mds.Data.compile("BUILD_SIGNAL(CONDITIONAL(0.45*(LOG($1/$2)-$3),0.0,$1>0.001 && $2>0.001),*,DIM_OF($1))",i_a,i_b,mach_offset)
        mach_sd_exp = mds.Data.compile("CONDITIONAL(0.45*SQRT(0.02**2+($2/$1)^2+($4/$3)^2+($5)^2),0.0,$1>0.001 && $3>0.001)",i_a,i_a_sd,i_b,i_b_sd,mach_offset_sd)
        mach_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",mach_sd_exp,mach)
        mdsbuild.put_wrapper(mach,mach_sig,err=mach_sd)
        vel_sig = mds.Data.compile("BUILD_SIGNAL($1*$2,*,DIM_OF($2))",cs,mach)
        vel_sd_exp = mds.Data.compile("SQRT(($1*$4)^2 + ($3*$2)^2)",cs,cs_sd,mach,mach_sd)
        vel_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",vel_sd_exp,mach)
        mdsbuild.put_wrapper(velocity,vel_sig,err=vel_sd,units="m/s")
        ne_sig = mds.Data.compile("BUILD_SIGNAL(CONDITIONAL(($1/$3 + $2/$4) / (2*0.6*$QE*$5),0.0,$1>0.001 && $2>0.001),*,DIM_OF($1))",i_a,i_b,area_a,area_b,cs)
        ne_sd_exp = mds.Data.compile("SQRT(($10/(2*0.6*$QE*$5^2)*($1/$3 + $2/$4))^2 + (1.0/(2*0.6*$QE*$5)^2)*(($6/$3)^2 + ($7/$4)^2 + ($8/$3^2)^2 + ($9/$4^2)^2))",i_a,i_b,area_a,area_b,cs,i_a_sd,i_b_sd,area_a_sd,area_b_sd,cs_sd)
        ne_sd = mds.Data.compile("BUILD_SIGNAL(CONDITIONAL($1,0.0,$2>0.001 && $3>0.001),*,DIM_OF($2))",ne_sd_exp,i_a,i_b)
        mdsbuild.put_wrapper(ne,ne_sig,err=ne_sd,units="/m^3")
        ## build position TDI expressions for each mach probe
        xpos = mds.Data.compile("$1*SIN($2) - ($1 - $3 - {0:f})*SIN($2+$4)".format(mpd),rpivot,thetapivot,r0,delta_theta)
        zpos = mds.Data.compile("$1*COS($2) - ($1 - $3 - {0:f})*COS($2+$4)".format(mpd),rpivot,thetapivot,r0,delta_theta)
        pos = mds.Data.compile("FS_FLOAT([$1,0,$2])",xpos,zpos)
        xpos_sd = mds.Data.compile("SQRT( ((SIN($3)+SIN($3+$7))*$2)^2 + (($1*COS($3)-($1-$5-{0:f})*COS($3+$7))*$4)^2 +\
                (SIN($3+$7)*$6)^2 + (($1-$5-{0:f})*COS($3+$7)*$8)^2 )".format(mpd),
                rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
        zpos_sd = mds.Data.compile("SQRT( ((COS($3)-COS($3+$7))*$2)^2 + ((-$1*SIN($3)+($1-$5-{0:f})*SIN($3+$7))*$4)^2 +\
                (COS($3+$7)*$6)^2 + (($1-$5-{0:f})*SIN($3+$7)*$8)^2 )".format(mpd),
                rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
        pos_sd = mds.Data.compile("FS_FLOAT([$1,0,$2])",xpos_sd,zpos_sd)
        mdsbuild.put_wrapper(loc,pos,err=pos_sd,units="m")

        myTree.setDefault(top)


    # loop through triple probe nodes to put tdi expressions
    tps = ["tp_{0:d}".format(i) for i in range(1,5)]
    tp_dist = .01456 + .01*np.array([0.0,4.0,8.0,12.0])
    for idx,(tp,tpd) in enumerate(zip(tps,tp_dist)):
        tp_node = myTree.getNode(tp)
        cs,ne,te,loc,deltav,isat,vfloat = mdsbuild.get_node_refs(tp_node,"cs","ne","te","loc","deltav","isat","vfloat")
        cs_sd,ne_sd,te_sd,loc_sd,deltav_sd,isat_sd,vfloat_sd = mdsbuild.get_error_refs(cs,ne,te,loc,deltav,isat,vfloat)
        # build te_sig # throw extra 10% error on Te
        te_sig = mds.Data.compile("CONDITIONAL($1/LOG(2.0),0.0,$2>0.001 && $1>0.0)",deltav,isat)
        te_sd = mds.Data.compile("BUILD_SIGNAL(CONDITIONAL(SQRT(($2/LOG(2.0))^2 + (0.15*$1/LOG(2.0))^2),0.0,$3>0.001 && $1>0.0),*,DIM_OF($1))",deltav,deltav_sd,isat)
        mdsbuild.put_wrapper(te,te_sig,err=te_sd,units="eV")
        ## build loc tdi expressions for each triple probe
        xpos = mds.Data.compile("$1*SIN($2) - ($1 - $3 - {0:f})*SIN($2+$4)".format(tpd),rpivot,thetapivot,r0,delta_theta)
        zpos = mds.Data.compile("$1*COS($2) - ($1 - $3 - {0:f})*COS($2+$4)".format(tpd),rpivot,thetapivot,r0,delta_theta)
        pos = mds.Data.compile("FS_FLOAT([$1,0,$2])",xpos,zpos)
        xpos_sd = mds.Data.compile("SQRT( ((SIN($3)+SIN($3+$7))*$2)^2 + (($1*COS($3)-($1-$5-{0:f})*COS($3+$7))*$4)^2 +\
                (SIN($3+$7)*$6)^2 + (($1-$5-{0:f})*COS($3+$7)*$8)^2 )".format(tpd),
                rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
        zpos_sd = mds.Data.compile("SQRT( ((COS($3)-COS($3+$7))*$2)^2 + ((-$1*SIN($3)+($1-$5-{0:f})*SIN($3+$7))*$4)^2 +\
                (COS($3+$7)*$6)^2 + (($1-$5-{0:f})*SIN($3+$7)*$8)^2 )".format(tpd),
                rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
        pos_sd = mds.Data.compile("FS_FLOAT([$1,0,$2])",xpos_sd,zpos_sd)
        mdsbuild.put_wrapper(loc,pos,err=pos_sd,units="m")
        # build cs tdi expression
        cs_sig = mds.Data.compile("BUILD_SIGNAL(CONDITIONAL(9790.0*SQRT(($1*$2*$3 + $4*$5)/$6),0.0,$7>0.001),*,DIM_OF($3))",gam_e,z,te,gam_i,ti,mu_i,isat)
        cs_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((9790.0^2/(4*($1*$2*$3 + $4*$5)/$6)) * (($1*$3*$7/$6)^2 +\
                ($1*$2*$8/$6)^2 + ($4*$9/$6)^2)),*,DIM_OF($3))",gam_e,z,te,gam_i,ti,mu_i,z_sd,te_sd,ti_sd)
        mdsbuild.put_wrapper(cs,cs_sig,err=cs_sd,units="m/s")
        # build ne tdi expression
        area = mdsbuild.get_node_refs(isat,"area")
        area_sd = mdsbuild.get_error_refs(area)
        ne_sig = mds.Data.compile("BUILD_SIGNAL(CONDITIONAL($1/(.6*$QE*$2*$3),0.0,$3>0.0),*,DIM_OF($1))",isat,area,cs)
        ne_sd = mds.Data.compile("BUILD_SIGNAL(CONDITIONAL(SQRT(($4/(.6*$QE*$2*$3))^2 + ($1*$5/(.6*$QE*$2^2*$3))^2 +\
                ($1*$6/(.6*$QE*$2*$3^2))^2),0.0,$3>0.0),*,DIM_OF($1))",isat,area,cs,isat_sd,area_sd,cs_sd)
        mdsbuild.put_wrapper(ne,ne_sig,err=ne_sd,units="/m^3")
        # build isat tdi expression
        raw, raw_offset, r1, calfac = mdsbuild.get_node_refs(isat,"raw","raw_offset","r1","calfac")
        raw_sd, raw_offset_sd, r1_sd,calfac_sd = mdsbuild.get_error_refs(raw,raw_offset,r1,calfac)
        isat_sig = mds.Data.compile("$4*($1-$2)/$3",raw,raw_offset,r1,calfac)
        isat_sd_exp = mds.Data.compile("SQRT(($6*$4/$3)^2 + ($6*($1-$2)*$5/$3^2)^2 + (($1-$2)/$3*$7)^2)",raw,raw_offset,r1,raw_offset_sd,r1_sd,calfac,calfac_sd)
        isat_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",isat_sd_exp,raw)
        mdsbuild.put_wrapper(isat,isat_sig,err=isat_sd,units="A")
        # build deltav tdi expression
        raw, raw_offset, r2, r3 = mdsbuild.get_node_refs(deltav,"raw","raw_offset","r2","r3")
        raw_sd, raw_offset_sd, r2_sd, r3_sd = mdsbuild.get_error_refs(raw,raw_offset,r2,r3)
        print(raw,r2,r3)
        deltav_sig = mds.Data.compile("BUILD_SIGNAL(($1-$2)*($3+$4)/$4,*,DIM_OF($1))",raw,raw_offset,r2,r3)
        deltav_sd_exp = mds.Data.compile("SQRT(($5*($3+$4)/$4)^2 + (($1-$2)*$3*$7/$4^2)^2 +\
                (($1-$2)*$6/$4)^2)",raw,raw_offset,r2,r3,raw_offset_sd,r2_sd,r3_sd)
        deltav_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",deltav_sd_exp,raw)
        mdsbuild.put_wrapper(deltav,deltav_sig,err=deltav_sd,units="V")
        # build vfloat tdi expression
        raw, raw_offset, r4, r5 = mdsbuild.get_node_refs(vfloat,"raw","raw_offset","r4","r5")
        raw_sd, raw_offset_sd, r4_sd, r5_sd = mdsbuild.get_error_refs(raw,raw_offset,r4,r5)
        vfloat_sig = mds.Data.compile("BUILD_SIGNAL(($1-$2)*($3+$4)/$4,*,DIM_OF($1))",raw,raw_offset,r4,r5)
        vfloat_sd_exp = mds.Data.compile("SQRT(($5*($3+$4)/$4)^2 + (($1-$2)*$3*$7/$4^2)^2 +\
                (($1-$2)*$6/$4)^2)",raw,raw_offset,r4,r5,raw_offset_sd,r4_sd,r5_sd)
        vfloat_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",vfloat_sd_exp,raw)
        mdsbuild.put_wrapper(vfloat,vfloat_sig,err=vfloat_sd,units="V")

    # do xtip loop
    for xnode in ["xtip","xtip_t5"]:
        print(xnode)
        xtip,xtip_i,xtip_v,xtip_loc = mdsbuild.get_node_refs(top,xnode,xnode+":i",xnode+":v",xnode+":loc")
        # build loc tdi expressions for xtip
        if xnode == "xtip":
            xpos = mds.Data.compile("$1*SIN($2) - ($1 - $3)*SIN($2+$4)",rpivot,thetapivot,r0,delta_theta)
            zpos = mds.Data.compile("$1*COS($2) - ($1 - $3)*COS($2+$4)",rpivot,thetapivot,r0,delta_theta)
            pos = mds.Data.compile("FS_FLOAT([$1,0,$2])",xpos,zpos)
            xpos_sd = mds.Data.compile("SQRT( ((SIN($3)+SIN($3+$7))*$2)^2 + (($1*COS($3)-($1-$5)*COS($3+$7))*$4)^2 +\
                    (SIN($3+$7)*$6)^2 + (($1-$5)*COS($3+$7)*$8)^2 )",rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
            zpos_sd = mds.Data.compile("SQRT( ((COS($3)-COS($3+$7))*$2)^2 + ((-$1*SIN($3)+($1-$5)*SIN($3+$7))*$4)^2 +\
                    (COS($3+$7)*$6)^2 + (($1-$5)*SIN($3+$7)*$8)^2 )",rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
            pos_sd = mds.Data.compile("FS_FLOAT([$1,0,$2])",xpos_sd,zpos_sd)
            mdsbuild.put_wrapper(xtip_loc,pos,err=pos_sd,units="m")
        else:
            xpos = mds.Data.compile("$1*SIN($2) - ($1 - $3 - 0.05456)*SIN($2+$4)",rpivot,thetapivot,r0,delta_theta)
            zpos = mds.Data.compile("$1*COS($2) - ($1 - $3 - 0.05456)*COS($2+$4)",rpivot,thetapivot,r0,delta_theta)
            pos = mds.Data.compile("FS_FLOAT([$1,0,$2])",xpos,zpos)
            xpos_sd = mds.Data.compile("SQRT( ((SIN($3)+SIN($3+$7))*$2)^2 + (($1*COS($3)-($1-$5 - 0.05456)*COS($3+$7))*$4)^2 +\
                    (SIN($3+$7)*$6)^2 + (($1-$5 - 0.05456)*COS($3+$7)*$8)^2 )",rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
            zpos_sd = mds.Data.compile("SQRT( ((COS($3)-COS($3+$7))*$2)^2 + ((-$1*SIN($3)+($1-$5 - 0.05456)*SIN($3+$7))*$4)^2 +\
                    (COS($3+$7)*$6)^2 + (($1-$5 - 0.05456)*SIN($3+$7)*$8)^2 )",rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
            pos_sd = mds.Data.compile("FS_FLOAT([$1,0,$2])",xpos_sd,zpos_sd)
            mdsbuild.put_wrapper(xtip_loc,pos,err=pos_sd,units="m")
        # do current tdi expression
        raw, raw_offset, rsweep = mdsbuild.get_node_refs(xtip_i,"raw","raw_offset","rsweep")
        raw_sd, raw_offset_sd, rsweep_sd,xtip_i_sd = mdsbuild.get_error_refs(raw,raw_offset,resistor,xtip_i)
        cur_sig = mds.Data.compile("BUILD_SIGNAL(($1-$2)/$3,*,DIM_OF($1))",raw,raw_offset,rsweep)
        cur_sd_exp = mds.Data.compile("SQRT(($3/$4)^2 + (($1-$2)*$5/4^2)^2)",raw,raw_offset,raw_offset_sd,rsweep,rsweep_sd)
        cur_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",cur_sd_exp,raw)
        mdsbuild.put_wrapper(xtip_i,cur_sig,err=cur_sd,units="A")
        # now do voltage
        raw, raw_offset, gain = mdsbuild.get_node_refs(xtip_v,"raw","raw_offset","gain")
        raw_sd, raw_offset_sd, gain_sd = mdsbuild.get_error_refs(raw,raw_offset,resistor)
        vol_sig = mds.Data.compile("BUILD_SIGNAL(($1-$2)*$3 + $4*$5,*,DIM_OF($1))",raw,raw_offset,gain,xtip_i,rsweep)
        vol_sd_exp = mds.Data.compile("SQRT(($3*$4)^2 + (($1-$2)*$5)^2 +\
                ($6*$9)^2 + ($7*$8)^2)",raw,raw_offset,raw_offset_sd,gain,gain_sd,xtip_i,xtip_i_sd,rsweep,rsweep_sd)
        vol_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",vol_sd_exp,raw)
        mdsbuild.put_wrapper(xtip_v,vol_sig,err=vol_sd,units="V")


    myTree.write()
    myTree.quit()



    return

if __name__ == "__main__":
    build_structure()
    populate_tdi()
    print("quad_mach_nt tree created!")
