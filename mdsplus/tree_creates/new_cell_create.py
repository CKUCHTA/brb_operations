import MDSplus as mds
import modules.mds_builders as mdsbuild
import sys

def build_structure(cellnum):
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    act = "action"
 
    # trex_cell_7 tree structure
    # numcards
    # chanpercard
    # numsamples
    # oversampfact
    # samplefreq
    # card_1
    #   ch_01
    #   ch_02
    #   ...
    #   ch_32
    # card_2
    # ...

    nodes = [("numcards",n,"numcards"),("chanpercard",n,"chanpercard"),
            ("numsamples",n,"numsamples"),("oversampfact",n,"oversampfact"),
            ("samplefreq",n,"samplefreq")]
    cards = [("card_{0:01d}".format(i+1),"structure",None) for i in range(5)]
    channels = [("ch_{0:02d}".format(i+1),"signal",None) for i in range(32)]

    trex_tree = mds.Tree(tree="trex_cell_{0}".format(cellnum),shot=-1,mode="New")
    trex_tree.cleanDatafile()
    trex_tree.edit()
    top = trex_tree.getNode("\\top")
    ## create nodes and assign tag names
    for name,usage,tag in nodes+cards:
        print(name)
        node = trex_tree.addNode(name,usage)
        if tag is not None:
            node.addTag("c{0}_{1}".format(cellnum,tag))
        if "card_" in name:
            trex_tree.setDefault(node)
            for name1,usage1,tag1 in channels:
                tag1 = "c{0}_{1}_{2}".format(cellnum,name[-1],name1[-2:])
                node1 = trex_tree.addNode(name1,usage1)
                node1.addTag(tag1)
            trex_tree.setDefault(top)

    mdsbuild.make_tree_write_once(trex_tree)
    trex_tree.write()
    mds.Tree.setCurrent(trex_tree.name,0)
    mdsbuild.traverse_tree(top)
    trex_tree.quit()
    return

def populate_tdi():
    # No TDI expressions in this raw tree
    return

if __name__ == "__main__":
    cellnum = int(sys.argv[1])
    build_structure(cellnum)
    populate_tdi()
    print("trex_cell_{0} tree created!".format(cellnum))
