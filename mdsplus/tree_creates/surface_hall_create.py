import MDSplus as mds
import modules.mds_builders as mdsbuild

def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    top_nodes = [("br",s,"surface_hall_br"),("btheta",s,"surface_hall_btheta"),("bphi",s,"surface_hall_bphi"),
            ("locs",n,"surface_hall_locs"),("note",t,"surface_hall_note"),("tree_details",t,"surface_hall_details")]
    hp_nodes = [("loc",n),("br",s),("btheta",s),("bphi",s),("ru",n),
            ("r_axis",st),("theta_axis",st),("phi_axis",st),("ps_voltage",s),("ps_current",s),("cable_res",n)]
    ax_nodes = [("chip_model",t),("loc",n),("gain",n),("offset",n),
            ("b",s),("bperm",n),("raw",s)]
    ps_nodes = [("gain",n),("offset",n),("raw",s)]

    hp_tree = mds.Tree(tree="surface_hall",shot=-1,mode="New")
    hp_tree.cleanDatafile()
    hp_range = range(1,8)
    hp_tree.edit()
    top = hp_tree.getNode("\\top")
    ## create top level magnetics data nodes
    for tn,usage,tag in top_nodes:
        tempnode = hp_tree.addNode(tn,usage)
        tempnode.addTag(tag)
    hp_tree.addNode("load_config","action")
    ## loop through individual 3-axis hall probe units
    for n in ["hp_{0:03d}".format(i) for i in hp_range]:
        hp_tree.addNode(n,"structure")
        hp_n = hp_tree.getNode(n)
        ## add nodes to this puck
        hp_tree.setDefault(hp_n)
        for (node_name,use) in hp_nodes:
            hp_tree.addNode(node_name,use)
        for (ps_node,use) in ps_nodes:
            hp_tree.addNode("ps_current:{0}".format(ps_node),use)
            hp_tree.addNode("ps_voltage:{0}".format(ps_node),use)
        ## add nodes to each axis child for this puck
        for ax_n in hp_n.getChildren():
            hp_tree.setDefault(ax_n)
            for (node_name,use) in ax_nodes:
                hp_tree.addNode(node_name,use)
        ## reset default node to top for next puck
        hp_tree.setDefault(top)

    mdsbuild.make_tree_write_once(hp_tree)
    unlocked_nodes = ("note",)+tuple("hp_{0:03d}:ru".format(i) for i in hp_range)+tuple("hp_{0:03d}:cable_res".format(i) for i in hp_range)+\
        tuple("hp_{0:03d}:{1}:{2}".format(i,j,k) for i in hp_range for j in ["r_axis","theta_axis","phi_axis"] for k in ["bperm","gain","offset"])
    unlocked_nodes += tuple("hp_{0:03d}:{1}:{2}".format(i,j,k) for i in hp_range for j in ["ps_voltage","ps_current"] for k in ["gain","offset"])
    mdsbuild.unlock_nodes(mdsbuild.get_node_refs(top,*unlocked_nodes))
    hp_tree.write()
    mds.Tree.setCurrent(hp_tree.name,0)
    mdsbuild.traverse_tree(top)
    hp_tree.quit()
    return

def populate_tdi():
    hp_tree = mds.Tree("surface_hall",-1)
    hp_range = range(1,8)
    top = hp_tree.getNode("\\top")
    # Populate top level 2D B arrays and locs array and tree details
    brnodes = mdsbuild.get_node_refs(top,*tuple("hp_{0:03d}.br".format(i) for i in hp_range))
    bthetanodes = mdsbuild.get_node_refs(top,*tuple("hp_{0:03d}.btheta".format(i) for i in hp_range))
    bphinodes = mdsbuild.get_node_refs(top,*tuple("hp_{0:03d}.bphi".format(i) for i in hp_range))
    locnodes = mdsbuild.get_node_refs(top,*tuple("hp_{0:03d}.loc".format(i) for i in hp_range))
    data_str ="["+",".join(["DATA(${0:d})".format(i) for i in hp_range])+"]"
    err_str ="["+",".join(["DATA(ERROR_OF(${0:d}))".format(i) for i in hp_range])+"]"
    dim_str = "IF_ERROR(" + ",".join(["DATA(DIM_OF(${0:d}))".format(i) for i in hp_range])+")"
    br_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str,dim_str),*brnodes)
    br_sd_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(err_str,dim_str),*brnodes)
    bt_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str,dim_str),*bthetanodes)
    bt_sd_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(err_str,dim_str),*bthetanodes)
    bp_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str,dim_str),*bphinodes)
    bp_sd_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(err_str,dim_str),*bphinodes)
    locs_dat = mds.Data.compile("{0}".format(data_str),*locnodes)
    locs_dat_sd = mds.Data.compile("{0}".format(err_str),*locnodes)
    mdsbuild.put_wrapper(top.getNode("br"),br_arr_sig,err=br_sd_arr_sig,units="T")
    mdsbuild.put_wrapper(top.getNode("btheta"),bt_arr_sig,err=bt_sd_arr_sig,units="T")
    mdsbuild.put_wrapper(top.getNode("bphi"),bp_arr_sig,err=bp_sd_arr_sig,units="T")
    mdsbuild.put_wrapper(top.getNode("locs"),locs_dat,err=locs_dat_sd)
    tree_det = "All units are SI, locs are [r,theta,phi] arrays in units of [m,rad,rad]. hp_xxx level and higher locations are centroids of that probes individual axes.\
            hp_xxx level and higher br, btheta, bphi values have permanent bfields subtracted off - only measuring external coils and plasma currents"
    top.getNode("tree_details").putData(tree_det)
    
    for child in top.getChildren():
        ## get relevant node references and build TDI signals with subtraction of B perm from permanent fields
        ru,br,btheta,bphi,br_perm,btheta_perm,bphi_perm,br_top,btheta_top,bphi_top = mdsbuild.get_node_refs(child,"ru","r_axis:b","theta_axis:b","phi_axis:b",
                "r_axis:bperm","theta_axis:bperm","phi_axis:bperm","br","btheta","bphi")
        ru_err,br_err,btheta_err,bphi_err,br_perm_err,btheta_perm_err,bphi_perm_err = mdsbuild.get_error_refs(ru,br,btheta,bphi,br_perm,btheta_perm,bphi_perm)
        br_sig = mds.Data.compile("BUILD_SIGNAL($1[0,0]*($2-$5) + $1[1,0]*($3-$6) + $1[2,0]*($4-$7),*,DIM_OF($2))",ru,br,btheta,bphi,br_perm,btheta_perm,bphi_perm)
        btheta_sig = mds.Data.compile("BUILD_SIGNAL($1[0,1]*($2-$5) + $1[1,1]*($3-$6) + $1[2,1]*($4-$7),*,DIM_OF($3))",ru,br,btheta,bphi,br_perm,btheta_perm,bphi_perm)
        bphi_sig = mds.Data.compile("BUILD_SIGNAL($1[0,2]*($2-$5) + $1[1,2]*($3-$6) + $1[2,2]*($4-$7),*,DIM_OF($4))",ru,br,btheta,bphi,br_perm,btheta_perm,bphi_perm)
        ## Build error in b signals with subtraction of B perm from permanent fields
        brerr_exp = mds.Data.compile("SQRT(($1[0,0]*$4)**2 + ($1[0,0]*$10)**2 + ($2[0,0]*($3-$9))**2 + ($1[1,0]*$6)**2 + ($1[1,0]*$12)**2 + \
                ($2[1,0]*($5-$11))**2 + ($1[2,0]*$8)**2 + ($1[2,0]*$14)**2 + ($2[2,0]*($7-$13))**2)",
                ru,ru_err,br,br_err,btheta,btheta_err,bphi,bphi_err,br_perm,br_perm_err,btheta_perm,btheta_perm_err,bphi_perm,bphi_perm_err)
        bthetaerr_exp = mds.Data.compile("SQRT(($1[0,1]*$4)**2 + ($1[0,1]*$10)**2 + ($2[0,1]*($3-$9))**2 + ($1[1,1]*$6)**2 + ($1[1,1]*$12)**2 + \
                ($2[1,1]*($5-$11))**2 + ($1[2,1]*$8)**2 + ($1[2,1]*$14)**2 + ($2[2,1]*($7-$13))**2)",
                ru,ru_err,br,br_err,btheta,btheta_err,bphi,bphi_err,br_perm,br_perm_err,btheta_perm,btheta_perm_err,bphi_perm,bphi_perm_err)
        bphierr_exp = mds.Data.compile("SQRT(($1[0,2]*$4)**2 + ($1[0,2]*$10)**2 + ($2[0,2]*($3-$9))**2 + ($1[1,2]*$6)**2 + ($1[1,2]*$12)**2 + \
                ($2[1,2]*($5-$11))**2 + ($1[2,2]*$8)**2 + ($1[2,2]*$14)**2 + ($2[2,2]*($7-$13))**2)",
                ru,ru_err,br,br_err,btheta,btheta_err,bphi,bphi_err,br_perm,br_perm_err,btheta_perm,btheta_perm_err,bphi_perm,bphi_perm_err)
        br_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",brerr_exp,br)
        btheta_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",bthetaerr_exp,btheta)
        bphi_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",bphierr_exp,bphi)
        
        mdsbuild.put_wrapper(br_top,br_sig,err=br_sd,units="T")
        mdsbuild.put_wrapper(btheta_top,btheta_sig,err=btheta_sd,units="T")
        mdsbuild.put_wrapper(bphi_top,bphi_sig,err=bphi_sd,units="T")

        ## loop through each axis child in each puck
        for ax in child.getChildren():
            gain, offset, b, raw = mdsbuild.get_node_refs(ax,"gain","offset","b","raw")
            b_sig = mds.Data.compile("BUILD_SIGNAL($1*($2-$3),*,DIM_OF($2))",gain,raw,offset)
            b_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((($2-$3)*(ERROR_OF($1)))^2 + ($1*(ERROR_OF($3)))^2),*,DIM_OF($2))",gain,raw,offset)
            mdsbuild.put_wrapper(b,b_sig,err=b_sd,units="T")

        current, gain, offset, raw = mdsbuild.get_node_refs(child,"ps_current","ps_current:gain","ps_current:offset","ps_current:raw")
        curr_sig = mds.Data.compile("BUILD_SIGNAL($1*($2-$3),*,DIM_OF($2))",gain,raw,offset)
        curr_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((($2-$3)*(ERROR_OF($1)))^2 + ($1*(ERROR_OF($3)))^2),*,DIM_OF($2))",gain,raw,offset)
        mdsbuild.put_wrapper(current,curr_sig,err=curr_sd,units="A")

        voltage, gain, offset, raw, res = mdsbuild.get_node_refs(child,"ps_voltage","ps_voltage:gain","ps_voltage:offset","ps_voltage:raw",
                "cable_res")
        vol_sig = mds.Data.compile("BUILD_SIGNAL($1*($2-$3)-IF_ERROR(DATA($4*$5),0.0),*,DIM_OF($2))",gain,raw,offset,res,current)
        vol_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((($2-$3)*(ERROR_OF($1)))^2 + ($1*(ERROR_OF($3)))^2 \
                + IF_ERROR(DATA((ERROR_OF($4)*$5)^2 + ($4*ERROR_OF($5))^2),0.0)),*,DIM_OF($2))",gain,raw,offset,res,current)
        mdsbuild.put_wrapper(voltage,vol_sig,err=vol_sd,units="V")

    return


if __name__ == "__main__":
    build_structure()
    populate_tdi()
    print("surface_hall tree created!")
