import MDSplus as mds
import modules.mds_builders as mdsbuild
import logging

def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    top_nodes = [("r_pivot",n),("theta_pivot",n),("phi_pivot",n),("r0",n),("delta_theta",n),
            ("bx",s),("by",s),("bz",s),("locs",n),("note",t),("ps_voltage",s)]
    hp_nodes = [("pos",n),("bx",s),("by",s),("bz",s),("rfu",n),
            ("axis_1",st),("axis_2",st),("axis_3",st)]
    ax_nodes = [("chip_model",t),("gain",n),("offset",n),
            ("b",s),("raw",s)]

    myTree = mds.Tree(tree="linear_hall2",shot=-1,mode="New")
    myTree.edit()
    pucknodes = ["hp_{0:02d}".format(i) for i in range(1,16)]
    ## create top level magnetics data nodes
    for (tn,use) in top_nodes:
        myTree.addNode(tn,use)
    myTree.addNode("load_config","action")
    myTree.addNode("run_proc","action")
    ## loop through individual 3-axis hall probe units
    for n in pucknodes:
        myTree.addNode(n,"structure")
        hp_n = myTree.getNode(n)
        hp_n.setWriteOnce(True)
        ## add nodes to this puck
        myTree.setDefault(hp_n)
        for (node_name,use) in hp_nodes:
            myTree.addNode(node_name,use)
            myTree.getNode(node_name).setWriteOnce(True)
        ## add nodes to each axis child for this puck
        for ax_n in hp_n.getChildren():
            myTree.setDefault(ax_n)
            for (node_name,use) in ax_nodes:
                myTree.addNode(node_name,use)
                myTree.getNode(node_name).setWriteOnce(True)
        ## reset default node to top for next puck
        myTree.setDefault(myTree.getNode(r"\top"))

    myTree.write()
    myTree.quit()
    return

def populate_tdi():
    hp_tree = mds.Tree("linear_hall2",-1)
    top = hp_tree.getNode("\\top")
    r0,delta_theta,rpivot,thetapivot = mdsbuild.get_node_refs(top,"r0","delta_theta","r_pivot","theta_pivot")
    r0_sd,delta_theta_sd,rpivot_sd,thetapivot_sd = mdsbuild.get_error_refs(r0,delta_theta,rpivot,thetapivot)
    for idx,child in enumerate(top.getChildren()):
        ## get relevant node references
        ## 1 = "theta board", 2 = "phi board", 3 = "r board"
        rfu, b1, b2, b3 = mdsbuild.get_node_refs(child,"rfu","axis_1:b","axis_2:b","axis_3:b")
        ## Build b signal TDI expressions
        bxsig = mds.Data.compile("BUILD_SIGNAL($1[0,0]*$2 + $1[1,0]*$3 + $1[2,0]*$4,*,DIM_OF($2))",rfu,b1,b2,b3)
        bysig = mds.Data.compile("BUILD_SIGNAL($1[0,1]*$2 + $1[1,1]*$3 + $1[2,1]*$4,*,DIM_OF($3))",rfu,b1,b2,b3)
        bzsig = mds.Data.compile("BUILD_SIGNAL($1[0,2]*$2 + $1[1,2]*$3 + $1[2,2]*$4,*,DIM_OF($4))",rfu,b1,b2,b3)
        ## Build b error signal TDI expressions
        rfu_sd,b1_sd,b2_sd,b3_sd = mdsbuild.get_error_refs(rfu,b1,b2,b3)
        bxsd_exp = mds.Data.compile("SQRT(($1[0,0]*$4)^2 + ($2[0,0]*$3)^2 + ($1[1,0]*$6)^2 + ($2[1,0]*$5)^2 + ($1[2,0]*$8)^2 + ($2[2,0]*$7)^2)",
                rfu,rfu_sd,b1,b1_sd,b2,b2_sd,b3,b3_sd)
        bysd_exp = mds.Data.compile("SQRT(($1[0,1]*$4)^2 + ($2[0,1]*$3)^2 + ($1[1,1]*$6)^2 + ($2[1,1]*$5)^2 + ($1[2,1]*$8)^2 + ($2[2,1]*$7)^2)",
                rfu,rfu_sd,b1,b1_sd,b2,b2_sd,b3,b3_sd)
        bzsd_exp = mds.Data.compile("SQRT(($1[0,2]*$4)^2 + ($2[0,2]*$3)^2 + ($1[1,2]*$6)^2 + ($2[1,2]*$5)^2 + ($1[2,2]*$8)^2 + ($2[2,2]*$7)^2)",
                rfu,rfu_sd,b1,b1_sd,b2,b2_sd,b3,b3_sd)
        bxsig_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",bxsd_exp,b1)
        bysig_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",bysd_exp,b2)
        bzsig_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",bzsd_exp,b3)
        ## Put compiled signals in proper nodes
        mdsbuild.put_wrapper(child.getNode("bx"),bxsig,err=bxsig_sd,units="T")
        mdsbuild.put_wrapper(child.getNode("by"),bysig,err=bysig_sd,units="T")
        mdsbuild.put_wrapper(child.getNode("bz"),bzsig,err=bzsig_sd,units="T")

        ## build position TDI expressions for each probe
        xpos = mds.Data.compile("$1*SIN($2) - ($1 - $3 - {0:d}*.015)*SIN($2+$4)".format(idx),rpivot,thetapivot,r0,delta_theta)
        zpos = mds.Data.compile("$1*COS($2) - ($1 - $3 - {0:d}*.015)*COS($2+$4)".format(idx),rpivot,thetapivot,r0,delta_theta)
        pos = mds.Data.compile("FS_FLOAT([$1,0,$2])",xpos,zpos)
        xpos_sd = mds.Data.compile("SQRT( ((SIN($3)+SIN($3+$7))*$2)^2 + (($1*COS($3)-($1-$5-{0:d}*.015)*COS($3+$7))*$4)^2 +\
                (SIN($3+$7)*$6)^2 + (($1-$5-{0:d}*.015)*COS($3+$7)*$8)^2 )".format(idx),
                rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
        zpos_sd = mds.Data.compile("SQRT( ((COS($3)-COS($3+$7))*$2)^2 + ((-$1*SIN($3)+($1-$5-{0:d}*.015)*SIN($3+$7))*$4)^2 +\
                (COS($3+$7)*$6)^2 + (($1-$5-{0:d}*.015)*SIN($3+$7)*$8)^2 )".format(idx),
                rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
        pos_sd = mds.Data.compile("FS_FLOAT([$1,0,$2])",xpos_sd,zpos_sd)
        mdsbuild.put_wrapper(child.getNode("pos"),pos,err=pos_sd,units="m")

        ## loop through each axis child in each puck
        for ax in child.getChildren():
            gain,offset,raw = mdsbuild.get_node_refs(ax,"gain","offset","raw")
            bsig = mds.Data.compile("BUILD_SIGNAL($1*($2-$3),*,DIM_OF($2))",gain,raw,offset)
            bsig_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((($2-$3)*(ERROR_OF($1)))^2 + ($1*(ERROR_OF($3)))^2),*,DIM_OF($2))",gain,raw,offset)
            mdsbuild.put_wrapper(ax.getNode("b"),bsig,err=bsig_sd,units="T")
    return

if __name__ == "__main__":
    build_structure()
    populate_tdi()
    logging.info("linear_hall2 tree created!")
