import MDSplus as mds
import modules.mds_builders as mdsbuild
import logging

def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    anodes = [("anode_{0:02d}".format(i),st) for i in range(1,21)] + [("bullet_01",st),("bullet_02",st),("ring_01",st),("ring_02",st)]
    anode_nodes = [("pos",n),("current",s),("area",n),("status",n)]
    cur_nodes = [("gain",n),("raw",s),("offset",n),("binned",s)]
    myTree = mds.Tree(tree="anodes",shot=-1,mode="New")
    top = myTree.getNode("\\top")
    myTree.edit()
    ## create top level anode tree structure nodes
    for (node_name,use) in anodes:
        node = myTree.addNode(node_name,use)
        myTree.setDefault(node)
        for (sub_name,sub_use) in anode_nodes:
            subnode = myTree.addNode(sub_name,sub_use)
            subnode.addTag("{0}_{1}".format(node_name,sub_name))
            if "cur" in sub_name:
                for (cur_node_name,use) in cur_nodes:
                    cur_subnode = myTree.addNode(sub_name+":{0}".format(cur_node_name),use)
                    cur_subnode.addTag("{0}_current_{1}".format(node_name,cur_node_name))
        myTree.setDefault(top)
    node = myTree.addNode("current_tot","signal")
    node.addTag("anode_current_tot")
    node = myTree.addNode("current_tot:binned","signal")
    node.addTag("anode_current_tot_binned")
    node = myTree.addNode("area_tot","numeric")
    node.addTag("anode_area_tot")
    myTree.addNode("run_proc","action")
    myTree.addNode("sql_load","action")
    node = myTree.addNode("note","text")
    node.addTag("anode_note")
    myTree.write()
    myTree.quit()
    return

def populate_tdi():
    anode_tree = mds.Tree("anodes",-1)
    top = anode_tree.getNode("\\top")
    anode_range = range(1,25)
    anodes = ["anode_{0:02d}".format(i) for i in range(1,21)] + ["bullet_01","bullet_02","ring_01","ring_02"]
    for anode in anodes:
        i,iraw,igain,ioffset,i_bin = mdsbuild.get_node_refs(anode_tree.getNode(anode),
                "current","current:raw","current:gain","current:offset","current:binned")
        ## build current node signals and errors
        isig = mds.Data.compile("BUILD_SIGNAL($1*($2-$3),*,DIM_OF($2))",igain,iraw,ioffset)
        isig_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((ERROR_OF($1)*($2-$3))^2 + (ERROR_OF($3)*$1)^2),*,DIM_OF($2))",igain,iraw,ioffset)
        mdsbuild.put_wrapper(i,isig,err=isig_sd,units="A")

    dim_string = "IF_ERROR(" + ",".join(["DATA(DIM_OF(${0:d}))".format(i) for i in anode_range])+")"
    sum_string = "+".join(["IF_ERROR(DATA(${0:d}),0)".format(i) for i in anode_range])
    err_string = "+".join(["(IF_ERROR(DATA(ERROR_OF(${0:d})),0))^2".format(i) for i in anode_range])
    inodes = mdsbuild.get_node_refs(top,*tuple("{0}:current".format(an) for an in anodes))
    itotsig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(sum_string,dim_string),*inodes)
    itot_sd = mds.Data.compile("BUILD_SIGNAL(SQRT({0}),*,{1})".format(err_string,dim_string),*inodes)
    inodes_bin = mdsbuild.get_node_refs(top,*tuple("{0}:current:binned".format(an) for an in anodes))
    itotbinsig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(sum_string,dim_string),*inodes_bin)
    itotbin_sd = mds.Data.compile("BUILD_SIGNAL(SQRT({0}),*,{1})".format(err_string,dim_string),*inodes_bin)
    areanodes = mdsbuild.get_node_refs(top,*tuple("{0}:area".format(an) for an in anodes))
    areatot = mds.Data.compile(sum_string,*areanodes)
    areatot_sd = mds.Data.compile("SQRT({0})".format(err_string),*areanodes)
    mdsbuild.put_wrapper(anode_tree.getNode("current_tot"),itotsig,err=itot_sd,units="A")
    mdsbuild.put_wrapper(anode_tree.getNode("current_tot:binned"),itotbinsig,err=itotbin_sd,units="A")
    mdsbuild.put_wrapper(anode_tree.getNode("area_tot"),areatot,err=areatot_sd,units="m^2")
    return

if __name__ == "__main__":
    build_structure()
    populate_tdi()
    logging.info("anodes tree created!")
