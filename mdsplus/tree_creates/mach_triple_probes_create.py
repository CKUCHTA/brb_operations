import MDSplus as mds
import modules.mds_builders as mdsbuild
import sys
"""
TO DO

Add a mu node

Add probe location nodes

write populate_tdi()

velocity needs to be handed differently becuase of where I put the Cs node.  Maybe I should move it???
"""
n_probes = 10

def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"


    top_nodes = [("probe_{0:n}".format(probe_number+1), st) for probe_number in range(n_probes)]
    probe_nodes = [("mp", st), ("tp", st), ("r", n), ('theta', n), ('phi', n)]
    mp_nodes = [("mach", s), ("mach_offset", n), ("velocity", n), ("A", st), ("B", st), ("AtoBvec", n)]
    tp_nodes = [("cs", s), ("deltaV", s), ("ne", s), ("Te", s), ("vfloat", s), ("isat", s)]
    mp_sub_nodes = [("mp1", st), ("mp2", st)]
    mp_face_nodes = [("raw", s), ("raw_offset", n), ("R", n), ("current", s)]
    deltaV_nodes = [("R1", n), ("R2", n), ("raw", s), ("raw_offset", n)]
    vfloat_nodes = [("R1", n), ("R2", n), ("raw", s), ("raw_offset", n)]
    isat_nodes = [("R", n), ("raw", s), ("raw_offset", s), ("area", n)]

    myTree = mds.Tree(tree="mach_triple", shot=-1, mode='New')
    myTree.edit()

    for (top, use) in top_nodes:
        myTree.addNode(top, use)
    myTree.addNode("load_sql", "action")
    myTree.addNode("run_proc", "action")
    myTree.addNode("mu", "numeric")

    # Let's populate each probe now
    for i in range(n_probes):
        probe_node = myTree.getNode(top_nodes[i][0])
        print(probe_node)
        print(type(probe_node))
        # set the default to reference nodes relative to this
        myTree.setDefault(probe_node)
        for node, use in probe_nodes:
            myTree.addNode(node, use)
        #myTree.addNode("mp", st)
        #myTree.addNode("tp", st)

        # populate mp nodes
        mp_node = myTree.getNode("mp")
        myTree.setDefault(mp_node)

        for (node, use) in mp_sub_nodes:
            myTree.addNode(node, use)
            myTree.setDefault(myTree.getNode(node)) 

        ######
            for (node, use) in mp_nodes:
                myTree.addNode(node, use)

            # populdate face nodes
            A_node = myTree.getNode("A")
            B_node = myTree.getNode("B")

            myTree.setDefault(A_node)
            for (node, use) in mp_face_nodes:
                myTree.addNode(node, use)

            myTree.setDefault(B_node)
            for (node, use) in mp_face_nodes:
                myTree.addNode(node, use)
            myTree.setDefault(mp_node)
        myTree.setDefault(probe_node)
        ######

        # populate tp nodes
        tp_node = myTree.getNode("tp")
        myTree.setDefault(tp_node)

        for (node, use) in tp_nodes:
            myTree.addNode(node, use)

        deltaV_node = myTree.getNode("deltaV")
        vfloat_node = myTree.getNode("vfloat")
        isat_node = myTree.getNode("isat")

        # populate delta V nodes
        myTree.setDefault(deltaV_node)
        for (node, use) in deltaV_nodes:
            myTree.addNode(node, use)

        # populate vfloat nodes
        myTree.setDefault(vfloat_node)
        for (node, use) in vfloat_nodes:
            myTree.addNode(node, use)

        # populate isat nodes
        myTree.setDefault(isat_node)
        for (node, use) in isat_nodes:
            myTree.addNode(node, use)

        myTree.setDefault(myTree.getNode(r"\top"))
    myTree.write()
    myTree.quit()

def populate_tdi(shot_number=None):
    if shot_number is None:
        shot_number = -1
    tree = mds.Tree("mach_triple", shot_number)
    top = tree.getNode("\\top")
    mu_node = mdsbuild.get_node_refs(top, "mu")[0]
    probes = ["probe_{0:d}".format(x+1) for x in range(n_probes)]
    for probe_name in probes:
        probe_node = tree.getNode(probe_name)

        tree.setDefault(probe_node)

        mp_node = tree.getNode("mp")
        tp_node = tree.getNode("tp")
        tree.setDefault(mp_node)
        mp_1_node = tree.getNode("mp1")
        mp_2_node = tree.getNode("mp2")
        # populate_mach_tdi(mp_node)
        populate_mach_tdi(mp_1_node)
        populate_mach_tdi(mp_2_node)

        tree.setDefault(tp_node)
        populate_tp_tdi(tp_node, mu_node)
        
        mp_nodes = [mp_1_node, mp_2_node]
        cs = mdsbuild.get_node_refs(tp_node, "cs")
        for mp in mp_nodes:
            mach, vel = mdsbuild.get_node_refs(mp, "mach", "velocity")

            vel_sig = mds.Data.compile("BUILD_SIGNAL($1*$2,*,DIM_OF($1))", mach, cs)
            vel_err_string = "BUILD_SIGNAL(0*$1,*,DIM_OF($1))"
            vel_err_sig = mds.Data.compile(vel_err_string, mach)
            mdsbuild.put_wrapper(vel, vel_sig, err=vel_err_sig, units="m/s") 

        tree.setDefault(top)


def populate_mach_tdi(mach_subnode):
    mach_nodes = ["mach", "mach_offset", "A", "B"] 
    mach, mach_offset, A, B = mdsbuild.get_node_refs(mach_subnode, *mach_nodes)
    mach_err, mach_offset_err = mdsbuild.get_error_refs(mach, mach_offset)

    face_node_names = ['current', 'R', 'raw', 'raw_offset']
    A_cur, A_R, A_raw, A_raw_offset = mdsbuild.get_node_refs(A, *face_node_names)
    B_cur, B_R, B_raw, B_raw_offset = mdsbuild.get_node_refs(B, *face_node_names)

    A_cur_err, A_R_err, A_raw_offset_err = mdsbuild.get_error_refs(A_cur, A_R, A_raw_offset)
    B_cur_err, B_R_err, B_raw_offset_err = mdsbuild.get_error_refs(B_cur, B_R, B_raw_offset)

    # Currents
    A_cur_sig = mds.Data.compile("BUILD_SIGNAL(($1-$2)/$3,*,DIM_OF($1))", A_raw, A_raw_offset, A_R)
    B_cur_sig = mds.Data.compile("BUILD_SIGNAL(($1-$2)/$3,*,DIM_OF($1))", B_raw, B_raw_offset, B_R)

    A_error_string = "BUILD_SIGNAL(0.05*$1,*,DIM_OF($1))"
    B_error_string = "BUILD_SIGNAL(0*$1,*,DIM_OF($1))"
    A_cur_err_sig = mds.Data.compile(A_error_string, A_raw)
    B_cur_err_sig = mds.Data.compile(B_error_string, B_raw)

    # Mach Number
    mach_sig = mds.Data.compile("BUILD_SIGNAL(0.45*LOG($1/$2)-$3,*,DIM_OF($1))", A_cur, B_cur, mach_offset)
    mach_error_string = "BUILD_SIGNAL(0*$1,*,DIM_OF($1))"
    mach_err_sig = mds.Data.compile(mach_error_string, A_cur)


    # ToDo
    # Write Error Bar Propagation code

    mdsbuild.put_wrapper(A_cur, A_cur_sig, err=A_cur_err_sig, units="A")
    mdsbuild.put_wrapper(B_cur, B_cur_sig, err=B_cur_err_sig, units="A")
    mdsbuild.put_wrapper(mach, mach_sig, err=mach_err_sig, units="1")
    mdsbuild.put_wrapper(mach_offset, mds.Float32(0.0), err=mds.Float32(0.0), units="1")


def populate_tp_tdi(tp_subnode, mu_node):
    tp_nodes = ["cs", "deltav", "isat", "ne", "te", "vfloat"]
    v_sub_nodes = ["r1", "r2", "raw", "raw_offset"]
    isat_sub_nodes = ["r", "raw", "raw_offset", "area"]

    cs, delta_v, isat, ne, te, vfloat = mdsbuild.get_node_refs(tp_subnode, *tp_nodes)
    cs_err, delta_v_err, isat_err, ne_err, te_err, vfloat_err = mdsbuild.get_error_refs(
            cs, delta_v, isat, ne, te, vfloat)

    d_r1, d_r2, d_raw, d_raw_offset = mdsbuild.get_node_refs(delta_v, *v_sub_nodes)
    f_r1, f_r2, f_raw, f_raw_offset = mdsbuild.get_node_refs(vfloat, *v_sub_nodes)
    r, raw, raw_offset, area = mdsbuild.get_node_refs(isat, *isat_sub_nodes)
    
    vfloat_sig = mds.Data.compile("BUILD_SIGNAL((1.0+($3)/($4))*($1-$2),*,DIM_OF($1))", 
            f_raw, f_raw_offset, f_r1, f_r2)
    vfloat_err_string = "BUILD_SIGNAL(0*$1,*,DIM_OF($1))"
    vfloat_err_sig = mds.Data.compile(vfloat_err_string, f_raw)

    delta_v_sig = mds.Data.compile("BUILD_SIGNAL((1.0+($3)/($4))*($1-$2),*,DIM_OF($1))", 
            d_raw, d_raw_offset, d_r1, d_r2)
    delta_v_err_string = "BUILD_SIGNAL(0*$1,*,DIM_OF($1))"
    delta_v_err_sig = mds.Data.compile(delta_v_err_string, d_raw)

    isat_sig = mds.Data.compile("BUILD_SIGNAL(($1-$2)/($3),*,DIM_OF($1))", raw, raw_offset, r)
    isat_err_string = "BUILD_SIGNAL(0*$1,*,DIM_OF($1))"
    isat_err_sig = mds.Data.compile(isat_err_string, raw)

    te_sig = mds.Data.compile("BUILD_SIGNAL(($1)/LOG(2),*,DIM_OF($1))", delta_v)
    te_err_sig = mds.Data.compile("BUILD_SIGNAL($1/LOG(2),*,DIM_OF($1))", delta_v_err)

    cs_sig = mds.Data.compile("BUILD_SIGNAL(9780*SQRT(ABS($1)/$2),*,DIM_OF($1))", te, mu_node)
    cs_err_sig = mds.Data.compile("BUILD_SIGNAL(0.0*$1,*,DIM_OF($1))", te_err)
    
    ne_sig = mds.Data.compile("BUILD_SIGNAL($1/(0.6*$EV*EXTEND(1000..1000000,,DATA($2))*DATA($3)),*,DIM_OF($1))", isat, cs, area)
    ne_err_sig = mds.Data.compile("BUILD_SIGNAL(0.0*$1,*,DIM_OF($1))", isat)

    mdsbuild.put_wrapper(vfloat, vfloat_sig, err=vfloat_err_sig, units="V")
    mdsbuild.put_wrapper(delta_v, delta_v_sig, err=delta_v_err_sig, units="V")
    mdsbuild.put_wrapper(isat, isat_sig, err=isat_err_sig, units="A")
    mdsbuild.put_wrapper(te, te_sig, err=te_err_sig, units="eV")
    mdsbuild.put_wrapper(cs, cs_sig, err=cs_err_sig, units="m/s")
    mdsbuild.put_wrapper(ne, ne_sig, err=ne_err_sig, units="m^-3")


if __name__ == "__main__":
    #build_structure()
    if len(sys.argv) > 1:
        shot_num = int(sys.argv[1])
    else:
        shot_num = None
    print(shot_num)
    populate_tdi(shot_number=shot_num)
    #print("mach_triple created!")



