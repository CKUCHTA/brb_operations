import MDSplus as mds
#import modules.mds_builders as mdsbuild


def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"

    magnetrons = [("magnetron_{0:02d}".format(i), st) for i in range(1,6)]
    magnetron_nodes = [
                       ("v_raw_T", s),
                       ("cal_T", s),
                       ("atten_T", s),
                       ("power_T", s),
                       ("det_power_T", s),
                       ("v_raw_R", s),
                       ("cal_R", s),
                       ("atten_R", s),
                       ("power_R", s),
                       ("det_power_R", s)
                       ]
    proc_nodes = magnetrons + [("pow_total", s), ]
    proc_magnetron_nodes = [
                            ("power_T", s),
                            ("power_R", s),
                            ("power_F", s)
                            ]
    #myTree = mds.Tree(tree="magnetrons", shot=-1, mode="New")
    #myTree.edit()

    """Create top level magnetron and proc structure nodes"""
    for (node_name, use) in magnetrons + [("proc", st),]:
        print(node_name, use)
        #myTree.addNode(node_name, use)
        if "magnetron" in node_name:
            for (sub_name, sub_use) in magnetron_nodes:
                #myTree.addNode("{0:s}:{1:s}".format(node_name, sub_name), sub_use_
                print("{0:s}:{1:s}".format(node_name, sub_name), sub_use)

    #myTree.addNode("run_proc", "action")
    #myTree.addNode("sql_load", "action")
    #myTree.setDefault(myTree.getNode("\\top.proc"))
    print("\n\n\n")
    """Loop through proc nodes"""
    for (node_name, use) in proc_nodes:
        #myTree.addNode(node_name, use)
        print(node_name)
        if "magnetron" in node_name:
            for (sub_name, sub_use) in proc_magnetron_nodes:
                n_name = "{0:s}:{1:s}".format(node_name, sub_name)
                print(n_name)
                #myTree.addNode(n, sub_use)
                #n = myTree.getNode(n_name)
                #n.addTag("{0:s}_{1:s}".format(node_name, sub_name))
        #myTree.write()
        #myTree.quit()
"""I will do this later..."""
def populate_tdi():
    #magnetron_tree = mds.Tree("magnetrons", -1)
    #top = magnetron_tree.getNode("\\top")
    magnetrons = ["magnetron_{0:02d}".format(i) for i in range(1,6)]
    T_nodes = [
               "v_raw_T",
               "cal_T",
               "atten_T",
               "power_T",
               "det_power_T"
               ]
    R_nodes = ["v_raw_R",
               "cal_R",
               "atten_R",
               "power_R",
               "det_power_R"
               ]
    det_power_string = "BUILD_SIGNAL($1[0]*$2^2+$1[1]*$2+$1[2],*,DIM_OF($2))"
    det_power_err_string = ("BUILD_SIGNAL(SQRT(($1*ERROR_OF($2)[0])^2+"
               "(2*$2[0]*$1*ERROR_OF($1))^2+"
               "($1*ERROR_OF($2)[1])^2+"
               "($2[1]*ERROR_OF($1))^2+"
               "ERROR_OF($2)[2]^2),*,DIM_OF($1))")
    power_string = "BUILD_SIGNAL($1*10^($2/10.0),*,DIM_OF($1))"
    power_err_string = ("BUILD_SIGNAL(SQRT((10^($1/10.0)*ERROR_OF($2))^2+"
                "($2*10^(($1-10.0)/10.0)*LOG(10.0)*ERROR_OF($1))^2),"
                "*,DIM_OF($2))")

    print(det_power_string,"\n")
    print(det_power_err_string,"\n")
    print(power_string,"\n")
    print(power_err_string,"\n")

    #for mag in magnetrons:
    #    node = magnetron_tree.getNode(mag)
    #    v, cal, atten, power, det = mdsbuild.get_node_refs(node, *T_nodes)
    #    """Do compile statements for T here""" 
    #    det_power_sig = mds.Data.compile(det_power_string, cal, v)
    #    det_power_sig_sd = mds.Data.compile(det_power_err_string, v, cal)
    #    mdsbuild.put_wrapper(det, det_power_sig, err=det_power_sig_sd, units="W")

    #    power_sig = mds.Data.compile(power_string, det, atten)
    #    power_sig_sd = mds.Data.compile(power_err_string, atten, det)
    #    mdsbuild.put_wrapper(power, power_sig, err=power_sig_sd, units="W")

    #    v, cal, atten, power, det = mdsbuild.get_node_refs(node, *R_nodes)
    #    """Do compile statements for R here""" 
    #    det_power_sig = mds.Data.compile(det_power_string, cal, v)
    #    det_power_sig_sd = mds.Data.compile(det_power_err_string, v, cal)
    #    mdsbuild.put_wrapper(det, det_power_sig, err=det_power_sig_sd, units="W")

    #    power_sig = mds.Data.compile(power_string, det, atten)
    #    power_sig_sd = mds.Data.compile(power_err_string, atten, det)
    #    mdsbuild.put_wrapper(power, power_sig, err=power_sig_sd, units="W")


if __name__ == "__main__":
    #build_structure()
    populate_tdi()
    print("magnetron tree created!")

