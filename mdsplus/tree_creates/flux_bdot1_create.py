import MDSplus as mds
import modules.mds_builders
from modules.tree_creation import *
import logging


NUM_STICKS = 8
NUM_PROBES_PER_STICK = 16

DB_PROBE_NUMBER_ASSIGNMENTS = {
    1: [14, 15], # 'r'
    2: [12, 13], # 'phi'
    3: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 16] # 'z'
}

def build_structure():
    """
    Build the full tree node structure with tags. Equations are not filled.
    """
    # flux_bdot1 tree structure:
    # Top
        # data (ST)
            # db1, db2, db3 [T/s] (S) BUILD_SIGNAL(sticks.stick_.db_, ...) All stick dB/dt measurements combined into a single signal.
                # working (S) BUILD_SIGNAL(sticks.stick_.db_.working, ...) Working status of each probe.
                # orientation (ST) The orientations of the probes.
                    # stick_coords (ST) The orientations in stick coordinates.
                        # x, y, z [m] (S) BUILD_SIGNAL(sticks.stick_.db_.orientation.stick_coords._, ...)
                    # port_coords (ST) The orientations in port coordinates.
                        # x, y, z [m] (S) BUILD_SIGNAL(sticks.stick_.db_.orientation.port_coords._, ...)
                    # mach_coords (ST) The orientations in machine coordinates
                        # r, phi, z [m or rad] (S) BUILD_SIGNAL(sticks.stick_.db_.orientation.mach_coords._, ...)
                # position (ST) The positions of the probes.
                    # stick_coords (ST) The positions in stick coordinates.
                        # x, y, z [m] (S) BUILD_SIGNAL(sticks.stick_.db_.position.stick_coords._, ...)
                    # port_coords (ST) The positions in port coordinates.
                        # x, y, z [m] (S) BUILD_SIGNAL(sticks.stick_.db_.position.port_coords._, ...)
                    # mach_coords (ST) The positions in machine coordinates
                        # r, phi, z [m or rad] (S) BUILD_SIGNAL(sticks.stick_.db_.position.mach_coords._, ...)
        # port (ST)
            # clock [deg] (N) From MySQL thru post run code.
            # rport [m] (N) From MySQL thru post run code.
            # lat [deg] (N) From MySQL thru post run code.
            # long [deg] (N) From MySQL thru post run code.
            # alpha [deg] (N) From MySQL thru post run code.
            # beta [deg] (N) From MySQL thru post run code.
            # gamma [deg] (N) From MySQL thru post run code.
            # insert [m] (N) From MySQL thru post run code.
        # flange (ST) The flange on the manifold's position and orientation.
            # position (ST)
                # port_coords (ST) Position of flange in port coordinates when insert = 0.
                    # x, y, z (N) From config.
            # orientation (ST)
                # port_coords (ST) Orientation of flange in port coordinates when clocking = 0.
                    # x, y, z (N) From config.
        # sticks (ST)
            # orientation (ST) The unit normal vector for the direction the sticks are pointing from the manifold to the stick tip.
                # port_coords (ST) The orientation in port coordinates:
                        # x=North, y=East, z=Down
                    # x, y, z (N) From config.
            # stick1, stick2, ..., stick8 (ST)
                # probes (ST) All the coils on a single stick.
                    # pb01, pb02, ..., pb16 [T/s] (S) gain * (raw - avg) Each coil on a stick.
                        # raw [V] (S) From config. Raw digitizer channel.
                        # gain [T / (s V)] (N) From config. Gain of signal to convert `V` to `T/s`.
                        # avg [V] (N) mean( raw ). Mean of initial points to subtract off as baseline.
                        # working [N] From config. Whether the probe is working.
                        # orientation (ST) The unit normal vector of the probe.
                            # stick_coords (ST) The orientation in stick coordinates:
                                    # x=along stick from manifold, y=perpendicular to x and z, z=along manifold from flange
                                # x, y, z (N) From config.
                            # port_coords (ST) The orientation in port coordinates:
                                    # x=North, y=East, z=Down
                                # x, y, z (N) From MySQL thru post run code.
                            # mach_coords (ST) The orientation in machine coordinates:
                                # r, phi, z (N) From MySQL thru post run code.
                        # position (ST) The position of the probe.
                            # stick_coords (ST) The position in stick coordinates.
                                # x, y, z [m] (N) From config.
                            # port_coords (ST) The position in port coordinates.
                                # x, y, z [m] (N) From MySQL thru post run code.
                            # mach_coords (ST) The position in machine coordinates.
                                # r, phi, z [m or rad] (N) From MySQL thru post run code.
                # data (ST) The probes on the stick separated into the 3 directions.
                    # db1, db2, db3 [T/s] (S) BUILD_SIGNAL(probes.pb__, ...)
                            # db1 = 'r' direction, db2 = 'phi' direction, and db3 = 'z' direction from Paul's coordinate system.
                        # working [S] BUILD_SIGNAL(probes.pb__.working, ...) Whether each probe is working.
                        # orientation (ST) The orientations of the probes.
                            # stick_coords (ST) The orientations in stick coordinates.
                                # x, y, z [m] (S) BUILD_SIGNAL(probes.pb__.orientation.stick_coords._, ...)
                            # port_coords (ST) The orientations in port coordinates.
                                # x, y, z [m] (S) BUILD_SIGNAL(probes.pb__.orientation.port_coords._, ...)
                            # mach_coords (ST) The orientations in machine coordinates
                                # r, phi, z [m or rad] (S) BUILD_SIGNAL(probes.pb__.orientation.mach_coords._, ...)
                        # position (ST) The positions of the probes.
                            # stick_coords (ST) The positions in stick coordinates.
                                # x, y, z [m] (S) BUILD_SIGNAL(probes.pb__.position.stick_coords._, ...)
                            # port_coords (ST) The positions in port coordinates.
                                # x, y, z [m] (S) BUILD_SIGNAL(probes.pb__.position.port_coords._, ...)
                            # mach_coords (ST) The positions in machine coordinates
                                # r, phi, z [m or rad] (S) BUILD_SIGNAL(probes.pb__.position.mach_coords._, ...)

    
    tree_name = "flux_bdot1"
    node_tag_prefix = "flux_bdot1"

    # Create the tree for editing.
    logging.info("Creating top of tree...")
    tree = mds.Tree(tree_name, shot=-1, mode="New")
    top = tree.getNode("\\top")
    tree.edit()

    top_tree_structure = {
        Node("data", ST): {
            Node("db1", S, NODE_NAME),
            Node("db2", S, NODE_NAME),
            Node("db3", S, NODE_NAME),
        },

        Node("port", ST): {
            Node("clock", N, NODE_NAME),
            Node("rport", N, NODE_NAME),
            Node("lat", N, NODE_NAME),
            Node("long", N, NODE_NAME),
            Node("alpha", N, NODE_NAME),
            Node("beta", N, NODE_NAME),
            Node("gamma", N, NODE_NAME),
            Node("insert", N, NODE_NAME)
        },

        Node("flange", ST): {
            Node("position", ST): {
                Node("port_coords", ST): {
                    Node("x", N),
                    Node("y", N),
                    Node("z", N),
                }
            },
            Node("orientation", ST): {
                Node("port_coords", ST): {
                    Node("x", N),
                    Node("y", N),
                    Node("z", N),
                }
            }
        },

        Node("sticks", ST): {
            Node("stick1", ST): None,
            Node("stick2", ST): None,
            Node("stick3", ST): None,
            Node("stick4", ST): None,
            Node("stick5", ST): None,
            Node("stick6", ST): None,
            Node("stick7", ST): None,
            Node("stick8", ST): None,

            Node("orientation", ST): {
                Node("port_coords", ST): {
                    Node("x", N),
                    Node("y", N),
                    Node("z", N),
                },
            },
        },
    }

    stick_tree_structure = {
        Node("probes", ST) : {
            Node("pb{}".format(str(num).zfill(2)), S) for num in range(1, NUM_PROBES_PER_STICK + 1)
        },

        Node("data", ST) : {
            Node("db{}".format(direction), S) for direction in range(1, 4)
        },
    }

    probe_tree_structure = {
        Node("raw", S): None,
        Node("gain", N): None,
        Node("avg", N): None,
        Node("working", N): None,
        Node("orientation", ST): {
            Node("stick_coords", ST): {
                Node("x", N),
                Node("y", N),
                Node("z", N),
            },
            Node("port_coords", ST): {
                Node("x", N),
                Node("y", N),
                Node("z", N),
            },
            Node("mach_coords", ST): {
                Node("r", N),
                Node("phi", N),
                Node("z", N),
            },
        },
        Node("position", ST): {
            Node("stick_coords", ST): {
                Node("x", N),
                Node("y", N),
                Node("z", N),
            },
            Node("port_coords", ST): {
                Node("x", N),
                Node("y", N),
                Node("z", N),
            },
            Node("mach_coords", ST): {
                Node("r", N),
                Node("phi", N),
                Node("z", N),
            },
        },
    }

    db_tree_structure = {
        Node("working", S): None,
        Node("orientation", ST): {
            Node("stick_coords", ST): {
                Node("x", S),
                Node("y", S),
                Node("z", S),
            },
            Node("port_coords", ST): {
                Node("x", S),
                Node("y", S),
                Node("z", S),
            },
            Node("mach_coords", ST): {
                Node("r", S),
                Node("phi", S),
                Node("z", S),
            },
        },
        Node("position", ST): {
            Node("stick_coords", ST): {
                Node("x", S),
                Node("y", S),
                Node("z", S),
            },
            Node("port_coords", ST): {
                Node("x", S),
                Node("y", S),
                Node("z", S),
            },
            Node("mach_coords", ST): {
                Node("r", S),
                Node("phi", S),
                Node("z", S),
            },
        },
    }

    logging.debug("Creating `top` tree structure...")
    recursive_tree_build(tree, top, top_tree_structure, tag_prefix=node_tag_prefix + "_")

    logging.debug("Creating `data.db` tree structure...")
    for direction in range(1, 4):
        tree.setDefault(top)
        data_db_node = tree.getNode("data.db{}".format(direction))
        tree.setDefault(data_db_node)

        recursive_tree_build(tree, data_db_node, db_tree_structure, node_tag_prefix + "_", recursion_level=1)

    logging.debug("Creating `sticks` tree structure...")
    for stick_number in range(1, NUM_STICKS + 1):
        tree.setDefault(top)
        stick_node = tree.getNode("sticks.stick{}".format(stick_number))
        tree.setDefault(stick_node)

        recursive_tree_build(tree, stick_node, stick_tree_structure, node_tag_prefix + "_", recursion_level=1)
        
        logging.debug("Creating `sticks.stick{}.probes` tree structure".format(stick_number))
        for probe_number in range(1, NUM_PROBES_PER_STICK + 1):
            tree.setDefault(stick_node)
            probe_node = tree.getNode("probes.pb{}".format(str(probe_number).zfill(2)))
            tree.setDefault(probe_node)

            recursive_tree_build(tree, probe_node, probe_tree_structure, node_tag_prefix + "_", recursion_level=2)

        logging.debug("Creating `sticks.stick{}.data` tree structure".format(stick_number))
        for direction in range(1, 4):
            tree.setDefault(stick_node)
            data_db_node = tree.getNode("data.db{}".format(direction))
            tree.setDefault(data_db_node)

            recursive_tree_build(tree, data_db_node, db_tree_structure, node_tag_prefix + "_", recursion_level=2)

    logging.info("Writing and printing tree...")
    tree.write()
    modules.mds_builders.traverse_tree(top)
    tree.cleanDatafile()
    tree.quit()

def populate_tree():
    """
    Run the top level populator that calls populators for first subtrees.
    """
    logging.debug("Populating top of tree.")
    tree = mds.Tree("flux_bdot1", -1)
    top_node = tree.getNode("\\top")

    # Get all the next level nodes.
    data_node = top_node.getNode("data")
    sticks_node = top_node.getNode("sticks")

    stick_names = ['stick{}'.format(stick_number) for stick_number in range(1, NUM_STICKS + 1)]
    stick_nodes = modules.mds_builders.get_node_refs(sticks_node, *stick_names)

    populate_data(data_node, stick_nodes)
    populate_sticks(stick_nodes)
    logging.info("Populated top of tree.")

def populate_data(data_node, stick_nodes):
    logging.debug("Populating 'data' children nodes.")
    combine_stick_data_str = "[" + ", ".join(["DATA(${})".format(i + 1) for i in range(NUM_STICKS)]) + "]"
    dimension_str = "DIM_OF($1)"

    for db_name in ['db{}'.format(direction) for direction in range(1, 4)]:
        logging.debug("Populating 'data.{}' children nodes.".format(db_name))
        db_node = data_node.getNode(db_name)
        working_node = db_node.getNode("working")
        orientation_node = db_node.getNode("orientation")
        position_node = db_node.getNode("position")

        stick_db_nodes = []
        stick_orientation_nodes = []
        stick_position_nodes = []
        for stick_node in stick_nodes:
            stick_db_nodes.append(stick_node.getNode("data.{}".format(db_name)))
            stick_orientation_nodes.append(stick_node.getNode("data.{}.orientation".format(db_name)))
            stick_position_nodes.append(stick_node.getNode("data.{}.position".format(db_name)))

        logging.debug("Compiling 'data.{}' commands.".format(db_name))
        db_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
            combine_stick_data_str, dimension_str
        ), *stick_db_nodes)
        working_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
            combine_stick_data_str, dimension_str
        ), *[stick_db_node.getNode('working') for stick_db_node in stick_db_nodes])

        logging.debug("Inserting commands.")
        modules.mds_builders.put_wrapper(db_node, db_command, units="T / s")
        modules.mds_builders.put_wrapper(working_node, working_command)

        populate_orientation_or_position(orientation_node, stick_orientation_nodes, is_orientation=True)
        populate_orientation_or_position(position_node, stick_position_nodes, is_orientation=False)
    logging.info("Populated 'data' children nodes.")

def populate_sticks(stick_nodes):
    logging.debug("Populating 'sticks' children nodes.")
    for stick_number, stick_node in enumerate(stick_nodes, start=1):
        logging.debug("Populating 'sticks.stick{}' children nodes.".format(stick_number))
        stick_data_node = stick_node.getNode("data")
        stick_probes_node = stick_node.getNode("probes")

        for direction, db_name in enumerate(['db{}'.format(direction) for direction in range(1, 4)], start=1):
            logging.debug("Populating 'sticks.stick{}.data.{}' children nodes.".format(stick_number, db_name))
            stick_db_node = stick_data_node.getNode(db_name)
            stick_working_node = stick_db_node.getNode('working')
            stick_orientation_node = stick_db_node.getNode('orientation')
            stick_position_node = stick_db_node.getNode('position')
            probe_nodes = [
                stick_probes_node.getNode("pb{}".format(str(probe_number).zfill(2))) 
                for probe_number in DB_PROBE_NUMBER_ASSIGNMENTS[direction]
            ]

            probe_working_nodes = []
            probe_orientation_nodes = []
            probe_position_nodes = []
            for probe_node in probe_nodes:
                probe_working_nodes.append(probe_node.getNode("working"))
                probe_orientation_nodes.append(probe_node.getNode("orientation"))
                probe_position_nodes.append(probe_node.getNode("position"))

            combine_probe_data_str = "[" + ", ".join(["DATA(${})".format(i + 1) for i in range(len(DB_PROBE_NUMBER_ASSIGNMENTS[direction]))]) + "]"
            dimension_str = "DIM_OF($1)"

            logging.debug("Compiling 'sticks.stick{}.data.{}' commands.".format(stick_number, db_name))
            stick_db_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
                combine_probe_data_str, dimension_str
            ), *probe_nodes)
            stick_working_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
                combine_probe_data_str, dimension_str
            ), *probe_working_nodes)

            logging.debug("Inserting commands.")
            modules.mds_builders.put_wrapper(stick_db_node, stick_db_command, units="T / s")
            modules.mds_builders.put_wrapper(stick_working_node, stick_working_command)

            populate_orientation_or_position(stick_orientation_node, probe_orientation_nodes, is_orientation=True)
            populate_orientation_or_position(stick_position_node, probe_position_nodes, is_orientation=False)
            logging.info("Populated 'sticks.stick{}.data.{}' children nodes.".format(stick_number, db_name))
        logging.info("Populated 'sticks.stick{}' children nodes.".format(stick_number))
    
        populate_probes(stick_probes_node)
    logging.info("Populated 'sticks' children nodes.")

def populate_orientation_or_position(combined_node, separate_nodes, is_orientation):
    logging.debug("Populating orientations or positions.")
    for coordinate_system in ['stick', 'port', 'mach']:
        logging.debug("Populating the '{}' coordinate system.".format(coordinate_system))
        coordinate_system_name = "{}_coords".format(coordinate_system)
        coordinate_system_combined_node = combined_node.getNode(coordinate_system_name)
        coordinate_system_separate_nodes = [separate_node.getNode(coordinate_system_name) for separate_node in separate_nodes]

        combine_data_str = "[" + ", ".join(["DATA(${})".format(i + 1) for i in range(len(separate_nodes))]) + "]"
        dimension_str = "DIM_OF($1)"

        directions = ['x', 'y', 'z'] if coordinate_system != 'mach' else ['r', 'phi', 'z']

        if is_orientation:
            direction_units = [None, None, None]
        elif coordinate_system == 'mach':
            direction_units = ['m', 'rad', 'm']
        else:
            direction_units = ['m', 'm', 'm']

        for direction, unit in zip(directions, direction_units):
            logging.debug("Populating the '{}' direction in the '{}' coordinate system.".format(direction, coordinate_system))
            direction_combined_node = coordinate_system_combined_node.getNode(direction)
            direction_separate_nodes = [separate_node.getNode(direction) for separate_node in coordinate_system_separate_nodes]

            logging.debug("Compiling command.")
            combine_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
                combine_data_str, dimension_str
            ), *direction_separate_nodes)

            logging.debug("Inserting command.")
            modules.mds_builders.put_wrapper(direction_combined_node, combine_command, units=unit)
            logging.info("Populated the '{}' direction in the '{}' coordinate system.".format(direction, coordinate_system))
        logging.info("Populated the '{}' coordinate system.".format(coordinate_system))
    logging.info("Populated orientations or positions.")

def populate_probes(probes_node):
    logging.debug("Populating 'probes' children nodes.")
    for probe_number in range(1, NUM_PROBES_PER_STICK + 1):
        probe_name = "pb{}".format(str(probe_number).zfill(2))
        logging.debug("Populating probe {}.".format(probe_number))
        probe_node = probes_node.getNode(probe_name)
        raw_node = probe_node.getNode('raw')
        gain_node = probe_node.getNode('gain')
        avg_node = probe_node.getNode('avg')

        logging.debug("Compiling commands.")
        dimension_str = "DIM_OF($1)"
        probe_command = mds.Data.compile('BUILD_SIGNAL($2 * ($1 - $3), *, {})'.format(dimension_str),
            raw_node, gain_node, avg_node
        )
        avg_command = mds.Data.compile('MEAN( $1 )', raw_node)

        logging.debug("Inserting commands.")
        modules.mds_builders.put_wrapper(probe_node, probe_command, units='T / s')
        modules.mds_builders.put_wrapper(avg_node, avg_command, units='V')
        logging.info("Populated probe {}.".format(probe_number))
    logging.info("Populated 'probes' children nodes.")


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, filename='./create_logs/flux_bdot1_create_log.txt', filemode='w')

    build_structure()
    populate_tree()
    logging.info("flux_bdot1 tree created!")


