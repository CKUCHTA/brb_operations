import MDSplus as mds
import modules.mds_builders as mdsbuild
import logging

def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    cathode_range = range(1,13)
    cathodes = [("cathode_{0:02d}".format(i),st) for i in cathode_range]
    cath_nodes = [("pos",n),("voltage",s),("current",s),("power",s),("area",n),("status",n)]
    iv_nodes = [("gain",n),("raw",s),("offset",n),("binned",s)]

    myTree = mds.Tree(tree="cathodes",shot=-1,mode="New")
    top = myTree.getNode("\\top")
    myTree.edit()
    ## create top level cathode and proc structure nodes
    for (node_name,use) in cathodes:
        myTree.addNode(node_name,use)
        myTree.setDefault(myTree.getNode(node_name))
        for (sub_name,sub_use) in cath_nodes:
            sub_node = myTree.addNode(sub_name,sub_use)
            sub_node.addTag("{0}_{1}".format(node_name,sub_name))
            if "vol" in sub_name or "cur" in sub_name:
                for (iv_node_name,use) in iv_nodes:
                    subsub_node = myTree.addNode(sub_name+":{0}".format(iv_node_name),use)
                    subsub_node.addTag("{0}_{1}_{2}".format(node_name,sub_name[0:3],iv_node_name))
            elif "power" in sub_name:
                subsub_node = myTree.addNode(sub_name+":binned",s)
                subsub_node.addTag("{0}_{1}_binned".format(node_name,sub_name[0:3]))
        myTree.setDefault(top)
    node = myTree.addNode("current_tot","signal")
    node.addTag("cathode_current_tot")
    node = myTree.addNode("power_tot","signal")
    node.addTag("cathode_power_tot")
    node = myTree.addNode("power_tot:binned","signal")
    node.addTag("cathode_power_tot_bin")
    node = myTree.addNode("current_tot:binned","signal")
    node.addTag("cathode_current_tot_bin")
    node = myTree.addNode("area_tot","numeric")
    node.addTag("cathode_area_tot")
    myTree.addNode("run_proc","action")
    myTree.addNode("sql_load","action")
    myTree.addNode("note","text")
    myTree.write()
    myTree.quit()
    return

def populate_tdi():
    cath_tree = mds.Tree("cathodes",-1)
    top = cath_tree.getNode("\\top")
    cathode_range = range(1,13)
    cathodes = ["cathode_{0:02d}".format(i) for i in cathode_range]
    for cath in cathodes:
        i,iraw,igain,ioffset,v,vraw,vgain,voffset,p,i_bin,v_bin,p_bin = mdsbuild.get_node_refs(cath_tree.getNode(cath),
                "current","current:raw","current:gain","current:offset","voltage","voltage:raw","voltage:gain","voltage:offset",
                "power","current:binned","voltage:binned","power:binned")
        ## build i and v node signals and errors
        isig = mds.Data.compile("BUILD_SIGNAL($1*($2-$3),*,DIM_OF($2))",igain,iraw,ioffset)
        isig_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((ERROR_OF($1)*($2-$3))^2 + (ERROR_OF($3)*$1)^2),*,DIM_OF($2))",igain,iraw,ioffset)
        vsig = mds.Data.compile("BUILD_SIGNAL($1*($2-$3),*,DIM_OF($2))",vgain,vraw,voffset)
        vsig_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((ERROR_OF($1)*($2-$3))^2 + (ERROR_OF($3)*$1)^2),*,DIM_OF($2))",vgain,vraw,voffset)
        psig = mds.Data.compile("BUILD_SIGNAL($1*$2,*,DIM_OF($1))",i,v)
        psig_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((ERROR_OF($1)*$2)^2 + (ERROR_OF($2)*$1)^2),*,DIM_OF($1))",i,v)
        psig_bin = mds.Data.compile("BUILD_SIGNAL($1*$2,*,DIM_OF($1))",i_bin,v_bin)
        psig_bin_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((ERROR_OF($1)*$2)^2 + (ERROR_OF($2)*$1)^2),*,DIM_OF($1))",i_bin,v_bin)
        mdsbuild.put_wrapper(i,isig,err=isig_sd,units="A")
        mdsbuild.put_wrapper(v,vsig,err=vsig_sd,units="V")
        mdsbuild.put_wrapper(p,psig,err=psig_sd,units="W")
        mdsbuild.put_wrapper(p_bin,psig_bin,err=psig_bin_sd,units="W")

    dim_string = "IF_ERROR(" + ",".join(["DATA(DIM_OF(${0:d}))".format(i) for i in cathode_range])+")"
    sum_string = "+".join(["IF_ERROR(DATA(${0:d}),0)".format(i) for i in cathode_range])
    err_string = "+".join(["(IF_ERROR(DATA(ERROR_OF(${0:d})),0))^2".format(i) for i in cathode_range])
    inodes = mdsbuild.get_node_refs(top,*tuple("cathode_{0:02d}:current".format(idx) for idx in cathode_range))
    itotsig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(sum_string,dim_string),*inodes)
    itot_sd = mds.Data.compile("BUILD_SIGNAL(SQRT({0}),*,{1})".format(err_string,dim_string),*inodes)
    pnodes= mdsbuild.get_node_refs(top,*tuple("cathode_{0:02d}:power".format(idx) for idx in cathode_range))
    ptotsig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(sum_string,dim_string),*pnodes)
    ptot_sd = mds.Data.compile("BUILD_SIGNAL(SQRT({0}),*,{1})".format(err_string,dim_string),*pnodes)
    inodes_bin = mdsbuild.get_node_refs(top,*tuple("cathode_{0:02d}:current:binned".format(idx) for idx in cathode_range))
    itotbinsig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(sum_string,dim_string),*inodes_bin)
    itotbin_sd = mds.Data.compile("BUILD_SIGNAL(SQRT({0}),*,{1})".format(err_string,dim_string),*inodes_bin)
    pnodes_bin = mdsbuild.get_node_refs(top,*tuple("cathode_{0:02d}:power:binned".format(idx) for idx in cathode_range))
    ptotbinsig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(sum_string,dim_string),*pnodes_bin)
    ptotbin_sd = mds.Data.compile("BUILD_SIGNAL(SQRT({0}),*,{1})".format(err_string,dim_string),*pnodes_bin)
    areanodes = mdsbuild.get_node_refs(top,*tuple("cathode_{0:02d}:area".format(idx) for idx in cathode_range))
    areatot = mds.Data.compile(sum_string,*areanodes)
    areatot_sd = mds.Data.compile("SQRT({0})".format(err_string),*areanodes)
    mdsbuild.put_wrapper(cath_tree.getNode("current_tot"),itotsig,err=itot_sd,units="A")
    mdsbuild.put_wrapper(cath_tree.getNode("power_tot"),ptotsig,err=ptot_sd,units="W")
    mdsbuild.put_wrapper(cath_tree.getNode("current_tot:binned"),itotbinsig,err=itotbin_sd,units="A")
    mdsbuild.put_wrapper(cath_tree.getNode("power_tot:binned"),ptotbinsig,err=ptotbin_sd,units="W")
    mdsbuild.put_wrapper(cath_tree.getNode("area_tot"),areatot,err=areatot_sd,units="m^2")

    return

if __name__ == "__main__":
    build_structure()
    populate_tdi()
    logging.info("cathodes tree created!")
