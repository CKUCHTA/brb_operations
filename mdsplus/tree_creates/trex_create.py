import MDSplus as mds
import modules.mds_builders as mdsbuild

def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    act = "action"
 
    # trex tree structure
    #irog_1
    #   gain
    #   offset
    #   raw
    #irog_2
    #   gain
    #   offset
    #   raw
    #irog_3
    #   gain
    #   offset
    #   raw
    #irog_4
    #   gain
    #   offset
    #   raw
    #vloop_1
    #   gain
    #   offset
    #   raw
    #vloop_2
    #   gain
    #   offset
    #   raw
    #vloop_3
    #   gain
    #   offset
    #   raw
    #vloop_4
    #   gain
    #   offset
    #   raw
    #vcap
    #   gain
    #   offset
    #   raw
    # probes
    #   

    top_nodes = [("irog_1",s,"trex_irog_1"),("irog_2",s,"trex_irog_2"),
            ("irog_3",s,"trex_irog_3"),("irog_4",s,"trex_irog_4"),
            ("vloop_1",s,"trex_vloop_1"),("vloop_2",s,"trex_vloop_2"),
            ("vloop_3",s,"trex_vloop_3"),("vloop_4",s,"trex_vloop_4"),
            ("vcap",s,"trex_v_cap"),("probes",st,None)]
    v_nodes = [("gain",n),("offset",n),("raw",s)]
    irog_nodes = v_nodes

    trex_tree = mds.Tree(tree="trex",shot=-1,mode="New")
    trex_tree.cleanDatafile()
    trex_tree.edit()
    top = trex_tree.getNode("\\top")
    ## create top level nodes
    for tn,usage,tag in top_nodes:
        print(tn)
        tempnode = trex_tree.addNode(tn,usage)
        tempnode.addTag(tag)
        trex_tree.setDefault(tempnode)
        if tn.startswith("irog"):
            for sn,snuse in irog_nodes:
                subnode = trex_tree.addNode(sn,snuse)
        elif tn.startswith("v"):
            for sn,snuse in v_nodes:
                subnode = trex_tree.addNode(sn,snuse)
        else:
            pass
        trex_tree.setDefault(top)

    # mdsbuild.make_tree_write_once(trex_tree)
    unlocked_nodes = ("irog_1","irog_2","irog_3","irog_4")
    # mdsbuild.unlock_nodes(mdsbuild.get_node_refs(top,*unlocked_nodes))
    trex_tree.write()
    mds.Tree.setCurrent(trex_tree.name,0)
    mdsbuild.traverse_tree(top)
    trex_tree.quit()
    return

def populate_tdi():
    trex_tree = mds.Tree("trex",-1)
    top = trex_tree.getNode("\\top")
    irogs = ["irog_{0:01d}".format(i) for i in range(1,5)]
    vnodes = ["vloop_1","vloop_2","vloop_3","vloop_4","vcap"]
    irog_specs = {1:{"gain":3.3880e3,"offset":.000,"raw":"RAW.A373_DIRECT:CH_55"},
            2:{"gain":3.6213e3,"offset":.000,"raw":"RAW.A373_DIRECT:CH_57"},
            3:{"gain":3.5805e3,"offset":.000,"raw":"RAW.A373_DIRECT:CH_59"},
            4:{"gain":3.5456e3,"offset":.000,"raw":"RAW.A373_DIRECT:CH_61"}}

    v_specs = {1:{"gain":2037.709,"offset":.000,"raw":"RAW.A373_DIRECT:CH_56"},
            2:{"gain":2053.002,"offset":.000,"raw":"RAW.A373_DIRECT:CH_58"},
            3:{"gain":2063.563,"offset":.000,"raw":"RAW.A373_DIRECT:CH_60"},
            4:{"gain":1981.235,"offset":.000,"raw":"RAW.A373_DIRECT:CH_62"},
            5:{"gain":1.10837334E3,"offset":.000,"raw":"RAW.A373_DIRECT:CH_63"}}

    for i,irogn in enumerate(irogs):
        irog_node = trex_tree.getNode(irogn)
        gain, offset, raw = mdsbuild.get_node_refs(irog_node,"gain","offset","raw")
        isig = mds.Data.compile("BUILD_SIGNAL($1*($2-$3),*,DIM_OF($2))",gain,raw,offset)
        isig_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((ERROR_OF($1)*($2-$3))^2 + (ERROR_OF($3)*$1)^2),*,DIM_OF($2))",gain,raw,offset)
        gain_val,off_val,raw_val = irog_specs[i+1]["gain"],irog_specs[i+1]["offset"],irog_specs[i+1]["raw"]
        mdsbuild.put_wrapper(irog_node,isig,err=isig_sd,units="A/s")
        mdsbuild.put_wrapper(gain,mds.Float32(gain_val),err=mds.Float32(gain_val*.01),units="A/Vs")
        mdsbuild.put_wrapper(offset,mds.Float32(off_val),err=mds.Float32(off_val*.1),units="V")
        mdsbuild.put_wrapper(raw,mds.TreePath(raw_val),err=None)

    for i,vn in enumerate(vnodes):
        vnode = trex_tree.getNode(vn)
        gain, offset, raw = mdsbuild.get_node_refs(vnode,"gain","offset","raw")
        vsig = mds.Data.compile("BUILD_SIGNAL($1*($2-$3),*,DIM_OF($2))",gain,raw,offset)
        vsig_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((ERROR_OF($1)*($2-$3))^2 + (ERROR_OF($3)*$1)^2),*,DIM_OF($2))",gain,raw,offset)
        gain_val,off_val,raw_val = v_specs[i+1]["gain"],v_specs[i+1]["offset"],v_specs[i+1]["raw"]
        mdsbuild.put_wrapper(vnode,vsig,err=vsig_sd,units="V")
        mdsbuild.put_wrapper(gain,mds.Float32(gain_val),err=mds.Float32(gain_val*.01),units="V/V")
        mdsbuild.put_wrapper(offset,mds.Float32(off_val),err=mds.Float32(off_val*.1),units="V")
        mdsbuild.put_wrapper(raw,mds.TreePath(raw_val),err=None)

    return

if __name__ == "__main__":
    build_structure()
    populate_tdi()
    print("trex tree created!")
