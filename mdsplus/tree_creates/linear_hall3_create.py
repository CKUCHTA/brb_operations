import MDSplus as mds
import modules.mds_builders as mdsbuild
import logging

def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    top_nodes = [("r_pivot",n,"linear_hall3_r_pivot"),("theta_pivot",n,"linear_hall3_theta_pivot"),
            ("phi_pivot",n,"linear_hall3_phi_pivot"),("r0",n,"linear_hall3_r0"),("delta_theta",n,"linear_hall3_delta_theta"),
            ("bx",s,"linear_hall3_bx"),("by",s,"linear_hall3_by"),("bz",s,"linear_hall3_bz"),("locs",n,"linear_hall3_locs"),
            ("note",t,"linear_hall3_note"),("ps_voltage",s,None)]
    hp_nodes = [("loc",n,"loc"),("bx",s,"bx"),("by",s,"by"),("bz",s,"bz"),("rfu",n,"rfu"),
            ("axis_1",st,None),("axis_2",st,None),("axis_3",st,None)]
    ax_nodes = [("chip_model",t),("gain",n),("offset",n),
            ("b",s),("raw",s)]
    hp_range = range(1,16)

    myTree = mds.Tree(tree="linear_hall3",shot=-1,mode="New")
    top = myTree.getNode("\\top")
    myTree.edit()
    ## create top level magnetics data nodes
    for (tn,use,tag) in top_nodes:
        node = myTree.addNode(tn,use)
        if tag is not None:
            node.addTag(tag)
    myTree.addNode("load_config","action")
    myTree.addNode("run_proc","action")
    temp_node = myTree.addNode("bx:binned","signal")
    temp_node.addTag("linear_hall3_bx_binned")
    temp_node = myTree.addNode("by:binned","signal")
    temp_node.addTag("linear_hall3_by_binned")
    temp_node = myTree.addNode("bz:binned","signal")
    temp_node.addTag("linear_hall3_bz_binned")

    ## loop through individual 3-axis hall probe units
    pucknodes = ["hp_{0:02d}".format(i) for i in hp_range]
    for pn in pucknodes:
        myTree.addNode(pn,"structure")
        hp_n = myTree.getNode(pn)
        ## add nodes to this puck
        myTree.setDefault(hp_n)
        for (node_name,use,tag) in hp_nodes:
            temp_node = myTree.addNode(node_name,use)
            if tag is not None:
                temp_node.addTag("linear_hall3_{0}_{1}".format(pn,tag))
        ## add nodes to each axis child for this puck
        for ax_n in hp_n.getChildren():
            myTree.setDefault(ax_n)
            for (node_name,use) in ax_nodes:
                myTree.addNode(node_name,use)
        ## reset default node to top for next puck
        myTree.setDefault(top)

    mdsbuild.make_tree_write_once(myTree)
    unlocked_nodes = ("note","r_pivot","theta_pivot","phi_pivot","r0","delta_theta","bx:binned","by:binned","bz:binned")+ \
            tuple("hp_{0:02d}:rfu".format(i) for i in hp_range) + \
            tuple("hp_{0:02d}.{1}:{2}".format(i,j,k) for i in hp_range for j in ["axis_1","axis_2","axis_3"] for k in ["gain","offset"])
    mdsbuild.unlock_nodes(mdsbuild.get_node_refs(top,*unlocked_nodes))
    myTree.write()
    myTree.cleanDatafile()
    myTree.quit()
    return



def populate_tdi():
    hp_tree = mds.Tree("linear_hall3",-1)
    top = hp_tree.getNode("\\top")
    r0,delta_theta,rpivot,thetapivot = mdsbuild.get_node_refs(top,"r0","delta_theta","r_pivot","theta_pivot")
    r0_sd,delta_theta_sd,rpivot_sd,thetapivot_sd = mdsbuild.get_error_refs(r0,delta_theta,rpivot,thetapivot)

    bxnodes = mdsbuild.get_node_refs(top,*tuple("hp_{0:02d}.bx".format(i) for i in range(1,16)))
    bynodes = mdsbuild.get_node_refs(top,*tuple("hp_{0:02d}.by".format(i) for i in range(1,16)))
    bznodes = mdsbuild.get_node_refs(top,*tuple("hp_{0:02d}.bz".format(i) for i in range(1,16)))
    locnodes = mdsbuild.get_node_refs(top,*tuple("hp_{0:02d}.loc".format(i) for i in range(1,16)))

    data_str ="["+",".join(["DATA(${0:d})".format(i) for i in range(1,16)])+"]"
    err_str ="["+",".join(["DATA(ERROR_OF(${0:d}))".format(i) for i in range(1,16)])+"]"
    dim_str = "IF_ERROR(" + ",".join(["DATA(DIM_OF(${0:d}))".format(i) for i in range(1,16)])+")"

    bx_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str,dim_str),*bxnodes)
    bx_sd_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(err_str,dim_str),*bxnodes)
    by_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str,dim_str),*bynodes)
    by_sd_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(err_str,dim_str),*bynodes)
    bz_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(data_str,dim_str),*bznodes)
    bz_sd_arr_sig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(err_str,dim_str),*bznodes)
    locs_dat = mds.Data.compile("{0}".format(data_str),*locnodes)
    locs_sd = mds.Data.compile("{0}".format(err_str),*locnodes)

    mdsbuild.put_wrapper(top.getNode("bx"),bx_arr_sig,err=bx_sd_arr_sig,units="T")
    mdsbuild.put_wrapper(top.getNode("by"),by_arr_sig,err=by_sd_arr_sig,units="T")
    mdsbuild.put_wrapper(top.getNode("bz"),bz_arr_sig,err=bz_sd_arr_sig,units="T")
    mdsbuild.put_wrapper(top.getNode("locs"),locs_dat,err=locs_sd,units="m")
    for idx,child in enumerate(top.getChildren()):
        ## get relevant node references
        ## 1 = "theta board", 2 = "phi board", 3 = "r board"
        rfu, b1, b2, b3 = mdsbuild.get_node_refs(child,"rfu","axis_1:b","axis_2:b","axis_3:b")
        ## Build b signal TDI expressions
        bxsig = mds.Data.compile("BUILD_SIGNAL($1[0,0]*$2 + $1[1,0]*$3 + $1[2,0]*$4,*,DIM_OF($2))",rfu,b1,b2,b3)
        bysig = mds.Data.compile("BUILD_SIGNAL($1[0,1]*$2 + $1[1,1]*$3 + $1[2,1]*$4,*,DIM_OF($3))",rfu,b1,b2,b3)
        bzsig = mds.Data.compile("BUILD_SIGNAL($1[0,2]*$2 + $1[1,2]*$3 + $1[2,2]*$4,*,DIM_OF($4))",rfu,b1,b2,b3)
        ## Build b error signal TDI expressions
        rfu_sd,b1_sd,b2_sd,b3_sd = mdsbuild.get_error_refs(rfu,b1,b2,b3)
        bxsd_exp = mds.Data.compile("SQRT(($1[0,0]*$4)^2 + ($2[0,0]*$3)^2 + ($1[1,0]*$6)^2 + ($2[1,0]*$5)^2 + ($1[2,0]*$8)^2 + ($2[2,0]*$7)^2)",
                rfu,rfu_sd,b1,b1_sd,b2,b2_sd,b3,b3_sd)
        bysd_exp = mds.Data.compile("SQRT(($1[0,1]*$4)^2 + ($2[0,1]*$3)^2 + ($1[1,1]*$6)^2 + ($2[1,1]*$5)^2 + ($1[2,1]*$8)^2 + ($2[2,1]*$7)^2)",
                rfu,rfu_sd,b1,b1_sd,b2,b2_sd,b3,b3_sd)
        bzsd_exp = mds.Data.compile("SQRT(($1[0,2]*$4)^2 + ($2[0,2]*$3)^2 + ($1[1,2]*$6)^2 + ($2[1,2]*$5)^2 + ($1[2,2]*$8)^2 + ($2[2,2]*$7)^2)",
                rfu,rfu_sd,b1,b1_sd,b2,b2_sd,b3,b3_sd)
        bxsig_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",bxsd_exp,b1)
        bysig_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",bysd_exp,b2)
        bzsig_sd = mds.Data.compile("BUILD_SIGNAL($1,*,DIM_OF($2))",bzsd_exp,b3)
        ## Put compiled signals in proper nodes
        mdsbuild.put_wrapper(child.getNode("bx"),bxsig,err=bxsig_sd,units="T")
        mdsbuild.put_wrapper(child.getNode("by"),bysig,err=bysig_sd,units="T")
        mdsbuild.put_wrapper(child.getNode("bz"),bzsig,err=bzsig_sd,units="T")

        ## build position TDI expressions for each probe
        xloc = mds.Data.compile("$1*SIN($2) - ($1 - $3 - {0:d}*.03)*SIN($2+$4)".format(idx),rpivot,thetapivot,r0,delta_theta)
        zloc = mds.Data.compile("$1*COS($2) - ($1 - $3 - {0:d}*.03)*COS($2+$4)".format(idx),rpivot,thetapivot,r0,delta_theta)
        loc = mds.Data.compile("FS_FLOAT([$1,0,$2])",xloc,zloc)
        xloc_sd = mds.Data.compile("SQRT( ((SIN($3)+SIN($3+$7))*$2)^2 + (($1*COS($3)-($1-$5-{0:d}*.03)*COS($3+$7))*$4)^2 +\
                (SIN($3+$7)*$6)^2 + (($1-$5-{0:d}*.03)*COS($3+$7)*$8)^2 )".format(idx),
                rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
        zloc_sd = mds.Data.compile("SQRT( ((COS($3)-COS($3+$7))*$2)^2 + ((-$1*SIN($3)+($1-$5-{0:d}*.03)*SIN($3+$7))*$4)^2 +\
                (COS($3+$7)*$6)^2 + (($1-$5-{0:d}*.03)*SIN($3+$7)*$8)^2 )".format(idx),
                rpivot,rpivot_sd,thetapivot,thetapivot_sd,r0,r0_sd,delta_theta,delta_theta_sd)
        loc_sd = mds.Data.compile("FS_FLOAT([$1,0,$2])",xloc_sd,zloc_sd)
        mdsbuild.put_wrapper(child.getNode("loc"),loc,err=loc_sd,units="m")

        ## loop through each axis child in each puck
        for ax in child.getChildren():
            gain,offset,raw = mdsbuild.get_node_refs(ax,"gain","offset","raw")
            bsig = mds.Data.compile("BUILD_SIGNAL($1*($2-$3),*,DIM_OF($2))",gain,raw,offset)
            bsig_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((($2-$3)*(ERROR_OF($1)))^2 + ($1*(ERROR_OF($3)))^2),*,DIM_OF($2))",gain,raw,offset)
            mdsbuild.put_wrapper(ax.getNode("b"),bsig,err=bsig_sd,units="T")
    return

if __name__ == "__main__":
    build_structure()
    populate_tdi()
    logging.info("linear_hall3 tree created!")
