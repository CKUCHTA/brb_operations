import MDSplus as mds
import modules.mds_builders
import sys
import logging


NODE_NAME = -1 # Global constant if the tag suffix is the same as the node name.

class Node:
    # Object for holding info needed for a node in an MDSplus tree.
    def __init__(self, name, data_type, tag=None):
        self.name = name
        self.data_type = data_type
        if tag == NODE_NAME:
            self.tag = name
        else:
            self.tag = tag


def build_structure():
    """
    Build the full tree node structure with tags. Equations are not filled.
    """
    # Tree node data types
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    act = "action"

    # teprobe2 tree structure
    # Top
        # avg_index (st) Indeces when nothing is happening in the but data is being recorded.
            # start (n) From config.
            # end (n) From config.
        # act_area (n) [m^2] From config.
        # data (st)
            # vprobe [V] (s) BUILD_SIGNAL(tip01.vprobe, ...)
            # iprobe [V] (s) BUILD_SIGNAL(tip01.iprobe, ...)
            # working (s) BUILD_SIGNAL(tip01.working, ...)
        # port (st)
            # clock [deg] (n) From MySQL thru post run code.
            # rport [deg] (n) From MySQL thru post run code.
            # lat [deg] (n) From MySQL thru post run code.
            # long [deg] (n) From MySQL thru post run code.
            # alpha [deg] (n) From MySQL thru post run code.
            # beta [deg] (n) From MySQL thru post run code.
            # gamma [deg] (n) From MySQL thru post run code.
            # insert [m] (n) From MySQL thru post run code. QUESTION: Is this actually from SQL?
        # pos (st) Position of middle of Te probe.
            # r [m] (n) From MySQL thru post run code
            # phi [deg] (n) From MySQL thru post run code
            # z [m] (n) From MySQL thru post run code
        # bdot (st)
            # ax1 [T/s] (s) gain * (raw_cw - raw_ccw)
                # gain [T/s/V] (n) From config.
                # raw_cw [V] (s) Channel from config.
                # raw_ccw [V] (s) Channel from config.
            # ax2 [T/s] (s)
                # ...
            # ax3 [T/s] (s)
                # ...
            # axr [T/s] (s) From post run code.
            # axphi [T/s] (s) From post run code.
            # axz [T/s] (s) From post run code.
        # tip01 (st)
            # vmeas [V] (s) (raw - avg - adjnoise)
                # raw [V] (s) Channel from config.
                # avg [V] (n) mean( vmeas.raw[avg_index.start:avg_index.end] )
                # adjnoise [V] (s) (raw - avg)
                    # raw [V] (s) Channel from config.
                    # avg [V] (n)
            # vgroupnoise (st) (raw - avg - adjnoise) * factor
                # raw [V] (s) Channel from config.
                # avg [V] (n)
                # factor (n) From config.
                # adjnoise [V] (s) (raw - avg)
                    # raw [V] (s) Channel from config.
                    # avg [V] (n)
            # vsense [V] (s) vsense.baselined / div
                # div (n) From config.
                # baselined [V] (s) vmeas - vgroupnoise
            # vbias [V] (n) (avg - offset) / div
                # raw [V] (s) Channel from config.
                # offset [V] (n) From config. QUESTION: How is this measured? TODO: Analyze drift in bias over some previous shots. Also add rails to SQL database.
                # div (n) From config.
                # avg [V] (n)
            # rsense [ohm] (n) From config.
            # rwire [ohm] (n) From config.
            # relarea (n) From config.
            # working (n) From config.
            # side (n) This is a boolean deciding whether the tip is on side 0 or 1. From config.
            # vprobe [V] (s) vbias + vsense * (rsense + rwire) / rsense
            # iprobe [V] (s) vsense * relarea / rsense
        # tip02 (st)
            # ...
        # ...

    tree_name = "te_probe2"
    node_tag_prefix = "te2" # This is the prefix that goes in front of every tag in this tree.
    
    # Create the tree for editing.
    logging.info("Creating top of tree...")
    tree = mds.Tree(tree_name, shot=-1, mode="New")
    top = tree.getNode("\\top")
    tree.edit()

    # This is a tree of dictionaries that represent the tree. Sometimes we can get away with sets (just curly braces without 'key: value' pairs and only have {'key1', 'key2', ...}). If it is forced to be a dictionary then each element must be a 'key: value' pair and thus sometimes the value is None.
    # The key contains each node.
    tree_structure = {
        Node("avg_index", st): {
            Node("start", n, "avg_start"),
            Node("end", n, "avg_end")
        },

        Node("act_area", n, NODE_NAME): None,

        Node("data", st): {
            Node("vprobe", s, NODE_NAME),
            Node("iprobe", s, NODE_NAME),
            Node("working", s, NODE_NAME)
        },

        Node("port", st): {
            Node("clock", n, NODE_NAME),
            Node("rport", n, NODE_NAME),
            Node("lat", n, NODE_NAME),
            Node("long", n, NODE_NAME),
            Node("alpha", n, NODE_NAME),
            Node("beta", n, NODE_NAME),
            Node("gamma", n, NODE_NAME),
            Node("insert", n, NODE_NAME)
        },

        Node("pos", st): {
            Node("r", n, NODE_NAME),
            Node("phi", n, NODE_NAME),
            Node("z", n, NODE_NAME)
        },

        Node("bdot", st): {
            Node("ax1", s, "bdot_ax1"): {
                Node("gain", n),
                Node("raw_cw", s),
                Node("raw_ccw", s)
            },
            Node("ax2", s, "bdot_ax2"): {
                Node("gain", n),
                Node("raw_cw", s),
                Node("raw_ccw", s)
            },
            Node("ax3", s, "bdot_ax3"): {
                Node("gain", n),
                Node("raw_cw", s),
                Node("raw_ccw", s)
            },
            Node("axr", s, "bdot_axr"): None,
            Node("axphi", s, "bdot_axphi"): None,
            Node("axz", s, "bdot_axz"): None
        }
    }

    # Each tip has it's own structure that is repeated for each tip.
    # Tips are prefixed as 01, ..., 16.
    tip_tree_structure = {
        Node("vmeas", s, "v"): {
            Node("raw", s, "v_raw"): None,
            Node("avg", n, "v_avg"): None,
            Node("adjnoise", s, "v_adj"): {
                Node("raw", s),
                Node("avg", n)
            }
        },
        Node("vgroupnoise", s, "vgrp"): {
            Node("raw", s, "vgrp_raw"): None,
            Node("avg", n, "vgrp_avg"): None,
            Node("factor", n, "vgrp_fac"): None,
            Node("adjnoise", s, "vgrp_adj"): {
                Node("raw", s),
                Node("avg", n)
            }
        },
        Node("vsense", s, "vsen"): {
            Node("div", n, "vsen_div"),
            Node("baselined", s, "vsen_base")
        },
        Node("vbias", n, "vbias"): {
            Node("raw", s, "vbias_raw"),
            Node("offset", n, "vbias_off"),
            Node("div", n, "vbias_div"),
            Node("avg", n, "vbias_avg")
        },
        Node("rsense", n, NODE_NAME): None,
        Node("rwire", n, NODE_NAME): None,
        Node("relarea", n, NODE_NAME): None,
        Node("working", n, NODE_NAME): None,
        Node("side", n, NODE_NAME): None,
        Node("vprobe", s, NODE_NAME): None,
        Node("iprobe", s, NODE_NAME): None
    }
            
    # Create non-tip structure.
    logging.info("Creating non-tip tree structure...")
    recursive_tree_build(tree, top, tree_structure, tag_prefix=node_tag_prefix + "_")

    # Create tip structure.
    logging.info("Creating tip tree structure...")
    for tip_index in range(1, 17):
        index_str = str(tip_index).zfill(2) # Pad the index with zeros on the left so that they are nicely ordered in the tree.
        # Create the tip node info.
        tip_name = 'tip{index}'.format(index=index_str)
        tip_node_type = st
        tip_tag_prefix = '{node_prefix}_{tip_tag}_'.format(node_prefix=node_tag_prefix, tip_tag=index_str)

        # Add the tip to the top of the tree.
        tree.setDefault(top)
        logging.debug("adding tip node " + tip_name)
        tree.addNode(tip_name, tip_node_type)
        tip_node = tree.getNode(tip_name)
        logging.info("Added tip node " + tip_node.getNodeName())
        
        recursive_tree_build(tree, tip_node, tip_tree_structure, tag_prefix=tip_tag_prefix)

    logging.info("Writing and printing tree...")
    tree.write()
    modules.mds_builders.traverse_tree(top)
    tree.cleanDatafile()
    tree.quit()

def recursive_tree_build(tree, curr_node, curr_node_structure, tag_prefix=''):
    """
    Build the tree by recursively calling the structure and building each node.

    params:
    tree                    = MDSplus tree that we are building.
    curr_node               = Node who's child nodes we are building.
    curr_node_structure     = Dictionary or set that contains all child nodes of where the tree is currently pointing.
    tag_prefix              = The string to prefix all tags in this subtree by.
    """
    # If the structure is a set instead of a dict then we make it look like a dict to simplify future code.
    if isinstance(curr_node_structure, set):
        temp_dict = {}

        for node in curr_node_structure:
            temp_dict[node] = None

        curr_node_structure = temp_dict

    # Iterate through each child node info, child node structure pair.
    for child_node_obj in curr_node_structure.keys():
        sub_struct = curr_node_structure[child_node_obj]
        # Set the tree to the current node. We do this each time because the tree might change positions during recursion.
        tree.setDefault(curr_node)

        # Create the child node.
        child_name = child_node_obj.name
        child_data_type = child_node_obj.data_type
        child_tag = child_node_obj.tag
        logging.debug("Adding node " + child_name)
        tree.addNode(child_name, child_data_type)
        child_node = tree.getNode(child_name)

        # Add the tag of the child node.
        if child_tag is not None:
            logging.debug("Adding tag " + tag_prefix + child_tag)
            child_node.addTag(tag_prefix + child_tag)
        
        logging.info("Added node " + child_node.getNodeName())

        # If there is no sub structure then we don't need to do anything else!
        if sub_struct is None:
            logging.debug("No subtree of {node_name} found.".format(node_name = child_name))
            continue
        else:
            # Create the sub-tree of the child node.
            logging.debug("Subtree of {node_name} found.".format(node_name = child_name))
            recursive_tree_build(tree, child_node, sub_struct, tag_prefix)

def populate_tree():
    """
    Populate the equations used for each node.
    """
    tree = mds.Tree("te_probe2", -1)
    top_node = tree.getNode("\\top")

    # Populate 'data'.
    tip_numbers = list(range(1, 17))
    tip_names = ['tip' + str(index).zfill(2) for index in tip_numbers]
    populate_data(top_node, tip_numbers, tip_names)

    # Populate 'bdot'.
    top_node = tree.getNode("\\top")
    populate_bdot(top_node)

    # Populate each tip.
    logging.debug('Populating tip nodes.')
    avg_index_node = top_node.getNode("avg_index")
    for tip_name in tip_names:
        tip_node = top_node.getNode(tip_name)
        populate_tip(tip_node, avg_index_node)

def populate_data(top_node, tip_numbers, tip_names):
    """
    Populate the functions for the nodes in 'data'.

    params:
    top_node    = Node at the top of the te_probe tree.
    tip_numbers = An iterable that contains the numbers for each tip.
    tip_names   = An iterable that contains the names for each tip.
    """
    logging.debug("Populating 'data' children nodes.")
    # The data string that combines all tips: '[Data($1), Data($2), ..., Data($17)]'
    combine_tip_data_str = "[" + ", ".join(["DATA(${0:d})".format(i) for i in tip_numbers]) + "]"
    
    # The node suffix looks like 'side' or 'vsense.div' so that the full name looks like 'tip01.side'.
    tip_node_names = lambda node_suffix: ['{name}.{suffix}'.format(name=tip_name, suffix=node_suffix) for tip_name in tip_names]

    # Get the reference to each tip signal.
    vprobe_nodes = modules.mds_builders.get_node_refs(top_node, *tip_node_names('vprobe'))
    iprobe_nodes = modules.mds_builders.get_node_refs(top_node, *tip_node_names('iprobe'))
    working_nodes = modules.mds_builders.get_node_refs(top_node, *tip_node_names('working'))

    # Compile the commands.
    # Dimension of first signal.
    dimension_str = 'DIM_OF($1)'
    logging.debug("Compiling 'data' commands.")
    vprobe_command = mds.Data.compile('BUILD_SIGNAL({combine_tips}, *, {dimension})'.format(combine_tips=combine_tip_data_str, dimension=dimension_str), *vprobe_nodes)
    iprobe_command = mds.Data.compile('BUILD_SIGNAL({combine_tips}, *, {dimension})'.format(combine_tips=combine_tip_data_str, dimension=dimension_str), *iprobe_nodes)
    working_command = mds.Data.compile('BUILD_SIGNAL({combine_tips}, *, {dimension})'.format(combine_tips=combine_tip_data_str, dimension=dimension_str), *working_nodes)
    
    # Get the nodes where these commands go.
    data_node = top_node.getNode("data")
    logging.debug("Getting nodes to insert commands into.")
    vprobe_combined_node = data_node.getNode("vprobe")
    iprobe_combined_node = data_node.getNode("iprobe")
    working_combined_node = data_node.getNode("working")

    # Put the commands into the nodes.
    logging.debug("Inserting commands.")
    modules.mds_builders.put_wrapper(vprobe_combined_node, vprobe_command, units='V')
    modules.mds_builders.put_wrapper(iprobe_combined_node, iprobe_command, units='V')
    modules.mds_builders.put_wrapper(working_combined_node, working_command)
    logging.info("Populated 'data' children nodes.")

def populate_bdot(top_node):
    bdot_node = top_node.getNode("bdot")
    
    for numbered_axis in ["ax1", "ax2", "ax3"]:
        axis_node = bdot_node.getNode(numbered_axis)

        gain_node = axis_node.getNode("gain")
        raw_clockwise_node = axis_node.getNode("raw_cw")
        raw_counterclockwise_node = axis_node.getNode("raw_ccw")

        axis_command = mds.Data.compile("BUILD_SIGNAL($1 * ($2 - $3),, DIM_OF($2))", gain_node, raw_clockwise_node, raw_counterclockwise_node)
        modules.mds_builders.put_wrapper(axis_node, axis_command, units="T/s")

    # TODO: Add method for calculating the cylindrical coordinate bdot values.

def populate_tip(tip_node, avg_index_node):
    """
    Populate the functions for the nodes in a single tip.

    params:
    tip_node        = Top node for specific probe tip.
    avg_index_node  = Parent node of indeces to average over.
    """
    logging.debug("Populating {tip_name} nodes.".format(tip_name=tip_node.getNodeName()))
    # Child nodes of this tip.
    logging.debug("Getting nodes to insert commands into and nodes used in commands.")
    vmeas_node = tip_node.getNode("vmeas")
    vgroupnoise_node = tip_node.getNode("vgroupnoise")
    vsense_node = tip_node.getNode("vsense")
    vbias_node = tip_node.getNode("vbias")
    rsense_node = tip_node.getNode("rsense")
    rwire_node = tip_node.getNode("rwire")
    relarea_node = tip_node.getNode("relarea")
    working_node = tip_node.getNode("working")
    side_node = tip_node.getNode("side")
    vprobe_node = tip_node.getNode("vprobe")
    iprobe_node = tip_node.getNode("iprobe")

    # Populate nodes.
    logging.debug("Inserting commands.")
    populate_vmeas(vmeas_node, avg_index_node)
    populate_vgroupnoise(vgroupnoise_node, avg_index_node)
    populate_vsense(vsense_node, vmeas_node, vgroupnoise_node)
    populate_vbias(vbias_node, avg_index_node)
    populate_vprobe(vprobe_node, vbias_node, vsense_node, rsense_node, rwire_node)
    populate_iprobe(iprobe_node, vsense_node, relarea_node, rsense_node)
    logging.info("Populated {tip_name} children nodes.".format(tip_name=tip_node.getNodeName()))
    
def populate_vmeas(vmeas_node, avg_index_node):
    """
    Populate the functions for the nodes in 'vmeas'.

    params:
    vmeas_node      = 'vmeas' node for some tip.
    avg_index_node  = Parent node of indeces to average over.
    """
    logging.debug("Populating 'vmeas'.")
    # Child nodes of 'vmeas'.
    raw_node = vmeas_node.getNode('raw')
    avg_node = vmeas_node.getNode('avg')
    adjnoise_node = vmeas_node.getNode("adjnoise")

    # Insert 'raw - avg - noise' into 'vmeas'.
    vmeas_equation = mds.Data.compile("BUILD_SIGNAL($1 - $2 - $3,, DIM_OF($1))", raw_node, avg_node, adjnoise_node)
    modules.mds_builders.put_wrapper(vmeas_node, vmeas_equation, units='V')

    # Insert the averaging equation that averages part of the raw signal.
    modules.mds_builders.put_wrapper(avg_node, average_equation(raw_node, avg_index_node), units='V')

    populate_adjnoise(adjnoise_node, avg_index_node)
    logging.debug("Populated 'vmeas'.")

def populate_vgroupnoise(vgroupnoise_node, avg_index_node):
    """
    Populate the functions for the nodes in 'vgroupnoise'.

    params:
    vgroupnoise_node    = 'vgroupnoise' node for some tip.
    avg_index_node      = Parent node of indeces to average over.
    """
    logging.debug("Populating 'vgroupnoise'.")
    # Child nodes of 'vgroupnoise'.
    raw_node = vgroupnoise_node.getNode('raw')
    avg_node = vgroupnoise_node.getNode('avg')
    adjnoise_node = vgroupnoise_node.getNode("adjnoise")
    factor_node = vgroupnoise_node.getNode('factor')

    # Insert '(raw - avg - noise) * factor' into 'vgroupnoise'.
    vgroupnoise_equation = mds.Data.compile("BUILD_SIGNAL(($1 - $2 - $3) * $4,, DIM_OF($1))", raw_node, avg_node, adjnoise_node, factor_node)
    modules.mds_builders.put_wrapper(vgroupnoise_node, vgroupnoise_equation, units='V')

    # Insert the averaging equation that averages part of the raw signal.
    modules.mds_builders.put_wrapper(avg_node, average_equation(raw_node, avg_index_node), units='V')

    populate_adjnoise(adjnoise_node, avg_index_node)
    logging.debug("Populated 'vgroupnoise'.")

def populate_vsense(vsense_node, vmeas_node, vgroupnoise_node):
    """
    Populate the functions for the nodes in 'vsense'.
    """
    logging.debug("Populating 'vsense'.")
    # Child nodes of 'vsense'.
    div_node = vsense_node.getNode('div')
    baselined_node = vsense_node.getNode('baselined')

    # Insert 'baselined / div' into 'vsense'.
    vsense_equation = mds.Data.compile("BUILD_SIGNAL($1 / $2,, DIM_OF($1))", baselined_node, div_node)
    modules.mds_builders.put_wrapper(vsense_node, vsense_equation, units='V')

    # Insert 'vmeas - vgroupnoise' into 'baselined'.
    baselined_equation = mds.Data.compile("BUILD_SIGNAL($1 - $2,, DIM_OF($1))", vmeas_node, vgroupnoise_node)
    modules.mds_builders.put_wrapper(baselined_node, baselined_equation, units='V')
    logging.debug("Populated 'vsense'.")

def populate_vbias(vbias_node, avg_index_node):
    """
    Populate the functions for the nodes in 'vbias'.
    """
    logging.debug("Populating 'vbias'.")
    # Child nodes of 'vbias'.
    raw_node = vbias_node.getNode('raw')
    offset_node = vbias_node.getNode('offset')
    div_node = vbias_node.getNode('div')
    avg_node = vbias_node.getNode('avg')

    # Insert '(avg - offset) / div' into 'vbias'.
    vbias_equation = mds.Data.compile("($1 - $2) / $3", avg_node, offset_node, div_node)
    modules.mds_builders.put_wrapper(vbias_node, vbias_equation, units='V')

    # Insert into 'avg' node.
    modules.mds_builders.put_wrapper(avg_node, average_equation(raw_node, avg_index_node), units='V')
    logging.debug("Populated 'vbias'.")

def populate_vprobe(vprobe_node, vbias_node, vsense_node, rsense_node, rwire_node):
    """
    Populate the function for the 'vprobe' node.
    """
    logging.debug("Populating 'vprobe'.")
    # Insert 'vbias + vsense * (rsense + rwire) / rsense' into 'vprobe'.
    vprobe_equation = mds.Data.compile("BUILD_SIGNAL($1 + $2 * ($3 + $4) / $3,, DIM_OF($1))", vbias_node, vsense_node, rsense_node, rwire_node)
    modules.mds_builders.put_wrapper(vprobe_node, vprobe_equation, units='V')
    logging.debug("Populated 'vprobe'.")

def populate_iprobe(iprobe_node, vsense_node, relarea_node, rsense_node):
    """
    Populate the function for the 'iprobe' node.
    """
    logging.debug("Populating 'iprobe'.")
    # Insert '(vsense * relarea) / rsense' into 'iprobe'.
    iprobe_equation = mds.Data.compile("BUILD_SIGNAL(($1 * $2) / $3,, DIM_OF($1))", vsense_node, relarea_node, rsense_node)
    modules.mds_builders.put_wrapper(iprobe_node, iprobe_equation, units='A')
    logging.debug("Populated 'iprobe'.")

def populate_adjnoise(adjnoise_node, avg_index_node):
    """
    Populate the functions for the nodes in 'adjnoise'.
    """
    logging.debug("Populating 'adjnoise'.")
    raw_node = adjnoise_node.getNode('raw')
    avg_node = adjnoise_node.getNode('avg')

    # Populate the 'adjnoise' node with 'raw - avg'.
    adjnoise_equation = mds.Data.compile("BUILD_SIGNAL($1 - $2,, DIM_OF($1))", raw_node, avg_node)
    modules.mds_builders.put_wrapper(adjnoise_node, adjnoise_equation, units='V')

    # Populate the 'avg' node.
    modules.mds_builders.put_wrapper(avg_node, average_equation(raw_node, avg_index_node), units='V')
    logging.debug("Populated 'adjnoise'.")

def average_equation(signal_node, avg_index_node):
    """
    Create the compiled function for averaging a signal.
    """
    logging.debug("Making averaging equation.")
    start_node = avg_index_node.getNode("start")
    end_node = avg_index_node.getNode("end")
    return mds.Data.compile("MEAN(DATA($1)[$2 : $3])", signal_node, start_node, end_node)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, filename='./create_logs/te_probe2_create_log.txt', filemode='w')

    build_structure()
    populate_tree()
    logging.info("te_probe2 tree created!")
