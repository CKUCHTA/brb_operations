import MDSplus as mds
import modules.mds_builders as mdsbuild

def build_structure():
    n = "numeric"
    s = "signal"
    t = "text"
    st = "structure"
    act = "action"
 
    # plasma gun nodes
    # i_arc_tot
    # p_arc_tot
    # i_bias_tot
    # p_bias_tot
    # gun#
    #   loc (r,theta) ref to center of gun
    #   arc_ps_id = pfn#
    #   bias_ps_id = pfn#
    #   i_arc
    #   v_arc
    #   p_arc
    #   v_bias
    #   i_bias
    #   p_bias

    top_nodes = [("i_arc_tot",s,"guns_i_arc_tot"),("p_arc_tot",s,"guns_p_arc_tot"),
            ("i_bias_tot",s,"guns_i_bias_tot"),("p_bias_tot",s,"guns_p_bias_tot"),
            ("locs",n,"guns_locs"),("load_config",act,None),("note",t,"guns_note")]
    gun_nodes = [("loc",n),("arc_ps_id",t),("bias_ps_id",t),("i_arc",s),("v_arc",s),
            ("p_arc",s),("i_bias",s),("v_bias",s),("p_bias",s)]
    sig_nodes = [("gain",n),("offset",n),("raw",s)]

    gun_tree = mds.Tree(tree="plasma_guns",shot=-1,mode="New")
    gun_tree.cleanDatafile()
    gun_range = range(1,20)
    gun_tree.edit()
    top = gun_tree.getNode("\\top")
    ## create top level nodes
    for tn,usage,tag in top_nodes:
        print(tn)
        tempnode = gun_tree.addNode(tn,usage)
        tempnode.addTag(tag)
    ## loop through individual plasma guns
    for n in ["gun_{0:02d}".format(i) for i in gun_range]:
        gun_tree.addNode(n,"structure")
        gun_n = gun_tree.getNode(n)
        ## add nodes to this gun
        gun_tree.setDefault(gun_n)
        for (node_name,use) in gun_nodes:
            tempnode = gun_tree.addNode(node_name,use)
            tempnode.addTag("{0}_{1}".format(n,node_name))
            ## add nodes to each axis child for this puck
            if node_name not in ["loc", "arc_ps_id", "bias_ps_id","p_arc","p_bias"]:
                for snode,use in sig_nodes:
                    tempnode.addNode(snode,use)
            ## reset default node to top for next puck
        gun_tree.setDefault(top)

    mdsbuild.make_tree_write_once(gun_tree)
    unlocked_nodes = ("note",)
    mdsbuild.unlock_nodes(mdsbuild.get_node_refs(top,*unlocked_nodes))
    gun_tree.write()
    mds.Tree.setCurrent(gun_tree.name,0)
    mdsbuild.traverse_tree(top)
    gun_tree.quit()
    return

def populate_tdi():
    gun_tree = mds.Tree("plasma_guns",-1)
    top = gun_tree.getNode("\\top")
    gun_range = range(1,20)
    guns = ["gun_{0:02d}".format(i) for i in gun_range]

    dim_string = "IF_ERROR(" + ",".join(["DATA(DIM_OF(${0:d}))".format(i) for i in gun_range])+")"
    sum_string = "+".join(["IF_ERROR(DATA(${0:d}),0)".format(i) for i in gun_range])
    err_string = "+".join(["(IF_ERROR(DATA(ERROR_OF(${0:d})),0))^2".format(i) for i in gun_range])
    data_string ="["+",".join(["DATA(${0:d})".format(i) for i in gun_range])+"]"

    iarcnodes = mdsbuild.get_node_refs(top,*tuple("gun_{0:02d}:i_arc".format(idx) for idx in gun_range))
    iarctotsig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(sum_string,dim_string),*iarcnodes)
    iarctot_sd = mds.Data.compile("BUILD_SIGNAL(SQRT({0}),*,{1})".format(err_string,dim_string),*iarcnodes)
    mdsbuild.put_wrapper(gun_tree.getNode("i_arc_tot"),iarctotsig,err=iarctot_sd,units="A")

    ibiasnodes = mdsbuild.get_node_refs(top,*tuple("gun_{0:02d}:i_bias".format(idx) for idx in gun_range))
    ibiastotsig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(sum_string,dim_string),*ibiasnodes)
    ibiastot_sd = mds.Data.compile("BUILD_SIGNAL(SQRT({0}),*,{1})".format(err_string,dim_string),*ibiasnodes)
    mdsbuild.put_wrapper(gun_tree.getNode("i_bias_tot"),ibiastotsig,err=ibiastot_sd,units="A")

    parcnodes = mdsbuild.get_node_refs(top,*tuple("gun_{0:02d}:p_arc".format(idx) for idx in gun_range))
    parctotsig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(sum_string,dim_string),*parcnodes)
    parctot_sd = mds.Data.compile("BUILD_SIGNAL(SQRT({0}),*,{1})".format(err_string,dim_string),*parcnodes)
    mdsbuild.put_wrapper(gun_tree.getNode("p_arc_tot"),parctotsig,err=parctot_sd,units="W")

    pbiasnodes = mdsbuild.get_node_refs(top,*tuple("gun_{0:02d}:p_bias".format(idx) for idx in gun_range))
    pbiastotsig = mds.Data.compile("BUILD_SIGNAL({0},*,{1})".format(sum_string,dim_string),*pbiasnodes)
    pbiastot_sd = mds.Data.compile("BUILD_SIGNAL(SQRT({0}),*,{1})".format(err_string,dim_string),*pbiasnodes)
    mdsbuild.put_wrapper(gun_tree.getNode("p_bias_tot"),pbiastotsig,err=pbiastot_sd,units="W")

    locnodes = mdsbuild.get_node_refs(top,*tuple("gun_{0:02d}:loc".format(i) for i in gun_range))
    locs_dat = mds.Data.compile("{0}".format(data_string),*locnodes)
    locs_sd = mds.Data.compile("{0}".format(err_string),*locnodes)
    mdsbuild.put_wrapper(top.getNode("locs"),locs_dat,err=locs_sd)

    for gun in guns:
        gun_node = gun_tree.getNode(gun)
        i_arc, v_arc, i_bias, v_bias = mdsbuild.get_node_refs(gun_node,"i_arc","v_arc","i_bias","v_bias")
        p_arcsig = mds.Data.compile("BUILD_SIGNAL($1*$2,*,DIM_OF($1))",i_arc,v_arc)
        p_arcsig_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((ERROR_OF($1)*$2)^2 + (ERROR_OF($2)*$1)^2),*,DIM_OF($1))",i_arc,v_arc)
        p_biassig = mds.Data.compile("BUILD_SIGNAL($1*$2,*,DIM_OF($1))",i_bias,v_bias)
        p_biassig_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((ERROR_OF($1)*$2)^2 + (ERROR_OF($2)*$1)^2),*,DIM_OF($1))",i_bias,v_bias)
        mdsbuild.put_wrapper(gun_node.getNode("p_arc"),p_arcsig,err=p_arcsig_sd,units="W")
        mdsbuild.put_wrapper(gun_node.getNode("p_bias"),p_biassig,err=p_biassig_sd,units="W")

        for sig,units in [("i_arc","A"),("v_arc","V"),("i_bias","A"),("v_bias","V")]:
            sig_node = gun_node.getNode(sig)
            gain, offset, raw = mdsbuild.get_node_refs(sig_node,"gain","offset","raw")
            sig = mds.Data.compile("BUILD_SIGNAL($1*($2-$3),*,DIM_OF($2))",gain,raw,offset)
            sig_sd = mds.Data.compile("BUILD_SIGNAL(SQRT((ERROR_OF($1)*($2-$3))^2 + (ERROR_OF($3)*$1)^2),*,DIM_OF($2))",gain,raw,offset)
            mdsbuild.put_wrapper(sig_node,sig,err=sig_sd,units=units)

    return

if __name__ == "__main__":
    build_structure()
    populate_tdi()
    print("plasma_guns tree created!")
