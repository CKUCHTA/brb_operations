import MDSplus as mds
import modules.mds_builders
from modules.tree_creation import *
import logging


def build_structure():
    """
    Build the full tree node structure with tags. Equations are not filled.
    """
    # paprobe1 tree structure
    # Top
        # avg_index (ST) Indeces when nothing is happening in the but data is being recorded.
            # start (N) From config.
            # end (N) From config.
        # act_area (ST) 
            # core [m^2] (N) From config.
            # shell [m^2] (N) From config.
        # data (ST)
            # vcore [V] (S) BUILD_SIGNAL(tip01.core.vprobe, ...)
            # vshell [V] (S) BUILD_SIGNAL(tip01.shell.vprobe, ...)
            # icore [V] (S) BUILD_SIGNAL(tip01.core.iprobe, ...)
            # ishell [V] (S) BUILD_SIGNAL(tip01.shell.iprobe, ...)
            # working (S) BUILD_SIGNAL(tip01.working, ...)
            # direction [m] (S) BUILD_SIGNAL(tip01.direction.machine, ...)
            # relareacore [m^2] (S) BUILD_SIGNAL(tip01.core.relarea, ...)
            # relareashell [m^2] (S) BUILD_SIGNAL(tip01.shell.relarea, ...)
            # TODO: Should tip retraction + area + shell area be stored.
        # port (ST)
            # clock [deg] (N) From MySQL. Clocking is zero when x axis pointed toward North pole.
            # rport [m] (N) From MySQL.
            # lat [deg] (N) From MySQL.
            # long [deg] (N) From MySQL.
            # alpha [deg] (N) From MySQL.
            # beta [deg] (N) From MySQL.
            # gamma [deg] (N) From MySQL.
            # insert [m] (N) From MySQL.
        # pos (ST) Position of middle of Te probe.
            # r [m] (N) From MySQL thru post run code
            # phi [deg] (N) From MySQL thru post run code
            # z [m] (N) From MySQL thru post run code
        # bdot (ST)
            # ax1 [T/s] (S) gain * (raw_cw - factor * raw_ccw)
                # gain [T/s/V] (N) From config.
                # factor (N) From config.
                # raw_cw [V] (S) Channel from config.
                # raw_ccw [V] (S) Channel from config.
            # ax2 [T/s] (S)
                # ...
            # ax3 [T/s] (S)
                # ...
            # axr [T/s] (S) From post run code.
            # axphi [T/s] (S) From post run code.
            # axz [T/s] (S) From post run code.
        # noise_relay (N) IF(avg > limit) 1.0; ELSE 0.0;
            # raw (S) Channel from config.
            # avg (N)
            # limit (N) From config.
        # vbias [V] (N) (avg - offset) / div
            # raw [V] (S) Channel from config.
            # offset [V] (N) From config.
            # div (N) From config.
            # avg [V] (N)
        # tip01 (ST)
            # core (ST)
                # signal (ST)
                    # v [V] (S) IF(noise_relay > 0.5) core.noise.default; ELSE core.signal.default;
                    # i [A] (S) core.signal.v / rdiggnd
                    # default [V] (S) (raw - avg)
                        # raw [V] (S) Channel from config.
                        # avg [V] (N) mean( raw[avg_index.start:avg_index.end] )
                # noise (ST)
                    # v [V] (S) IF(noise_relay > 0.5) core.signal.default; ELSE core.noise.default;
                    # i [A] (S) core.noise.v / rdiggnd
                    # default [V] (S) (raw - avg)
                        # raw [V] (S) Channel from config.
                        # avg [V] (N) mean( raw[avg_index.start:avg_index.end] )
                # rwire [ohm] (N) From config.
                # relarea (N) From config.
                # vprobe [V] (S) (rdiv + core.rwire + rdiv * core.rwire / rsense) * core.signal.i - (core.rwire + rdiv * core.rwire / rsense) * core.noise.i + vbias
                # iprobe [V] (S) (rdiv / rsense + 1) * (core.signal.i - core.noise.i)
            # shell (ST)
                # signal (ST)
                    # v [V] (S) shell.signal.default
                    # i [A] (S) shell.signal.v / rdiggnd
                    # default [V] (S) (raw - avg)
                        # raw [V] (S) Channel from config.
                        # avg [V] (N) mean( raw[avg_index.start:avg_index.end] )
                # noise (ST)
                    # v [V] (S) shell.noise.default
                    # i [A] (S) shell.noise.v / rdiggnd
                    # default [V] (S) (raw - avg)
                        # raw [V] (S) Channel from config.
                        # avg [V] (N) mean( raw[avg_index.start:avg_index.end] )
                # rwire [ohm] (N) From config.
                # relarea (N) From config.
                # vprobe [V] (S) (rdiv + shell.rwire + 3 * rdiv * shell.rwire / rsense) * shell.signal.i + vbias - (rdiv * shell.rwire * core.signal.i + rdiv * shell.rwire * shell.noise.i + rsense * shell.rwire * shell.noise.i + rdiv * shell.rwire * core.noise.i) / rsense
                # iprobe [A] (S) (3 * rdiv * shell.signal.i + rsense * shell.signal.i - rdiv * core.signal.i - rdiv * shell.noise.i - rsense * shell.noise.i - rdiv * core.noise.i) / rsense
            # rdiggnd [ohm] (N) Resistance from measured voltage to ground. From config.
            # rdiv [ohm] (N) Full resistance of divider + digitizer. From config.
            # rsense [ohm] (N) From config.
            # working (N) From config.
            # direction (ST)
                # probe (ST)
                    # theta [deg] (N) From config.
                    # phi [deg] (N) From config.
                # machine [m] (S) BUILD_SIGNAL(r, phi, z)
                    # r [m] (N) From post run code.
                    # phi [m] (N) From post run code.
                    # z [m] (N) From post run code.
        # tip02 (ST)
            # ...
        # ...

    tree_name = "pa_probe1"
    node_tag_prefix = "pa1" # This is the prefix that goes in front of every tag in this tree.
    
    # Create the tree for editing.
    logging.info("Creating top of tree...")
    tree = mds.Tree(tree_name, shot=-1, mode="New")
    top = tree.getNode("\\top")
    tree.edit()

    # This is a tree of dictionaries that represent the tree. Sometimes we can get away with sets (just curly braces without 'key: value' pairs and only have {'key1', 'key2', ...}). If it is forced to be a dictionary then each element must be a 'key: value' pair and thus sometimes the value is None.
    # The key contains each node.
    tree_structure = {
        Node("avg_index", ST): {
            Node("start", N, "avg_start"),
            Node("end", N, "avg_end")
        },

        Node("act_area", ST): {
            Node("core", N, "area_core"): None,
            Node("shell", N, "area_shell"): None,
        },

        Node("data", ST): {
            Node("vcore", S, NODE_NAME),
            Node("icore", S, NODE_NAME),
            Node("vshell", S, NODE_NAME),
            Node("ishell", S, NODE_NAME),
            Node("working", S, NODE_NAME),
            Node("direction", S, NODE_NAME),
            Node("relareacore", S, NODE_NAME),
            Node("relareashell", S, NODE_NAME),
        },

        Node("port", ST): {
            Node("clock", N, NODE_NAME),
            Node("rport", N, NODE_NAME),
            Node("lat", N, NODE_NAME),
            Node("long", N, NODE_NAME),
            Node("alpha", N, NODE_NAME),
            Node("beta", N, NODE_NAME),
            Node("gamma", N, NODE_NAME),
            Node("insert", N, NODE_NAME)
        },

        Node("pos", ST): {
            Node("r", N, NODE_NAME),
            Node("phi", N, NODE_NAME),
            Node("z", N, NODE_NAME)
        },

        Node("bdot", ST): {
            Node("ax1", S, "bdot_ax1"): {
                Node("gain", N),
                Node("factor", N),
                Node("raw_cw", S),
                Node("raw_ccw", S)
            },
            Node("ax2", S, "bdot_ax2"): {
                Node("gain", N),
                Node("factor", N),
                Node("raw_cw", S),
                Node("raw_ccw", S)
            },
            Node("ax3", S, "bdot_ax3"): {
                Node("gain", N),
                Node("factor", N),
                Node("raw_cw", S),
                Node("raw_ccw", S)
            },
            Node("axr", S, "bdot_axr"): None,
            Node("axphi", S, "bdot_axphi"): None,
            Node("axz", S, "bdot_axz"): None
        },
        Node("noise_relay", N, NODE_NAME): {
            Node("raw", S, None),
            Node("avg", N, None),
            Node("limit", N, None)
        },
        Node("vbias", N, NODE_NAME): {
            Node("raw", S, "vbias_raw"),
            Node("offset", N, "vbias_off"),
            Node("div", N, "vbias_div"),
            Node("avg", N, "vbias_avg")
        }
    }

    # Each tip has it's own structure that is repeated for each tip.
    # Tips are prefixed as 01, ..., 12.
    # The shell and core are prefixed by 's' and 'c' respectively.
    tip_tree_structure = {
        Node("core", ST, subtree_prefix='c'): {
            Node("signal", ST, subtree_prefix='s'): {
                Node("v", S, NODE_NAME): None,
                Node("i", S, NODE_NAME): None,
                Node("default", S, "def"): {
                    Node("raw", S, NODE_NAME),
                    Node("avg", N, NODE_NAME)
                }
            },
            Node("noise", ST, subtree_prefix='n'): {
                Node("v", S, NODE_NAME): None,
                Node("i", S, NODE_NAME): None,
                Node("default", S, "def"): {
                    Node("raw", S, NODE_NAME),
                    Node("avg", N, NODE_NAME)
                }
            },
            Node("rwire", N, NODE_NAME): None,
            Node("relarea", N, NODE_NAME): None,
            Node("vprobe", S, NODE_NAME): None,
            Node("iprobe", S, NODE_NAME): None
        },
        Node("shell", ST, subtree_prefix='s'): {
            Node("signal", ST, subtree_prefix='s'): {
                Node("v", S, NODE_NAME): None,
                Node("i", S, NODE_NAME): None,
                Node("default", S, "def"): {
                    Node("raw", S, NODE_NAME),
                    Node("avg", N, NODE_NAME)
                }
            },
            Node("noise", ST, subtree_prefix='n'): {
                Node("v", S, NODE_NAME): None,
                Node("i", S, NODE_NAME): None,
                Node("default", S, "def"): {
                    Node("raw", S, NODE_NAME),
                    Node("avg", N, NODE_NAME)
                }
            },
            Node("rwire", N, NODE_NAME): None,
            Node("relarea", N, NODE_NAME): None,
            Node("vprobe", S, NODE_NAME): None,
            Node("iprobe", S, NODE_NAME): None
        },
        Node("rdiggnd", N, NODE_NAME): None,
        Node("rdiv", N, NODE_NAME): None,
        Node("rsense", N, NODE_NAME): None,
        Node("working", N, NODE_NAME): None,
        Node("direction", ST): {
            Node("probe", ST): {
                Node("theta", N, NODE_NAME),
                Node("phi", N, NODE_NAME)
            },
            Node("machine", S, "dir_mach"): {
                Node("r", N, "r_mach"),
                Node("phi", N, "phi_mach"),
                Node("z", N, "z_mach")
            }
        }
    }
            
    # Create non-tip structure.
    logging.info("Creating non-tip tree structure...")
    recursive_tree_build(tree, top, tree_structure, tag_prefix=node_tag_prefix + "_")

    # Create tip structure.
    logging.info("Creating tip tree structure...")
    for tip_index in range(1, 13):
        index_str = str(tip_index).zfill(2) # Pad the index with zeros on the left so that they are nicely ordered in the tree.
        # Create the tip node info.
        tip_name = 'tip{index}'.format(index=index_str)
        tip_node_type = ST
        tip_tag_prefix = '{node_prefix}_{tip_tag}_'.format(node_prefix=node_tag_prefix, tip_tag=index_str)

        # Add the tip to the top of the tree.
        tree.setDefault(top)
        logging.debug("Adding tip node " + tip_name)
        tree.addNode(tip_name, tip_node_type)
        tip_node = tree.getNode(tip_name)
        logging.info("Added tip node " + tip_node.getNodeName())
        
        recursive_tree_build(tree, tip_node, tip_tree_structure, tag_prefix=tip_tag_prefix)

    logging.info("Writing and printing tree...")
    tree.write()
    modules.mds_builders.traverse_tree(top)
    tree.cleanDatafile()
    tree.quit()

def recursive_tree_build(tree, curr_node, curr_node_structure, tag_prefix=''):
    """
    Build the tree by recursively calling the structure and building each node.

    params:
    tree                    = MDSplus tree that we are building.
    curr_node               = Node who's child nodes we are building.
    curr_node_structure     = Dictionary or set that contains all child nodes of where the tree is currently pointing.
    tag_prefix              = The string to prefix all tags in this subtree by.
    """
    # If the structure is a set instead of a dict then we make it look like a dict to simplify future code.
    if isinstance(curr_node_structure, set):
        temp_dict = {}

        for node in curr_node_structure:
            temp_dict[node] = None

        curr_node_structure = temp_dict

    # Iterate through each child node info, child node structure pair.
    for child_node_obj in curr_node_structure.keys():
        sub_struct = curr_node_structure[child_node_obj]
        # Set the tree to the current node. We do this each time because the tree might change positions during recursion.
        tree.setDefault(curr_node)

        # Create the child node.
        child_name = child_node_obj.name
        child_data_type = child_node_obj.data_type
        child_tag = child_node_obj.tag
        logging.debug("Adding node " + child_name)
        tree.addNode(child_name, child_data_type)
        child_node = tree.getNode(child_name)

        # Add the tag of the child node.
        if child_tag is not None:
            logging.debug("Adding tag " + tag_prefix + child_tag)
            child_node.addTag(tag_prefix + child_tag)
        
        logging.info("Added node " + child_node.getNodeName())

        # If there is no sub structure then we don't need to do anything else!
        if sub_struct is None:
            logging.debug("No subtree of {node_name} found.".format(node_name = child_name))
            continue
        else:
            # Create the sub-tree of the child node.
            logging.debug("Subtree of {node_name} found.".format(node_name = child_name))

            # If there is a prefix to add on then use it.
            if child_node_obj.subtree_prefix is not None:
                recursive_tree_build(tree, child_node, sub_struct, tag_prefix + child_node_obj.subtree_prefix)
            else:
                recursive_tree_build(tree, child_node, sub_struct, tag_prefix)

def populate_tree():
    """
    Run the top level populator that calls populators for first subtrees.
    """
    tree = mds.Tree("pa_probe1", -1)
    top_node = tree.getNode("\\top")

    # Populate 'data'.
    tip_numbers = list(range(1, 13))
    tip_names = ['tip' + str(index).zfill(2) for index in tip_numbers]
    populate_data(top_node, tip_numbers, tip_names)

    # Populate 'bdot'.
    top_node = tree.getNode("\\top")
    populate_bdot(top_node)

    # Populate 'noise_relay'.
    top_node = tree.getNode("\\top")
    avg_index_node = top_node.getNode("avg_index")
    populate_noise_relay(top_node, avg_index_node)

    # Populate 'vbias'.
    top_node = tree.getNode("\\top")
    populate_vbias(top_node, avg_index_node)

    # Populate each tip.
    logging.debug('Populating tip nodes.')
    top_node = tree.getNode("\\top")
    vbias_node = top_node.getNode("vbias")
    noise_relay_node = tree.getNode("noise_relay")
    for tip_name in tip_names:
        tip_node = top_node.getNode(tip_name)
        populate_tip(tip_node, vbias_node, noise_relay_node, avg_index_node)

def populate_data(top_node, tip_numbers, tip_names):
    """
    Populate the functions for the nodes in 'data'.

    params:
    top_node    = Node at the top of the te_probe tree.
    tip_numbers = An iterable that contains the numbers for each tip.
    tip_names   = An iterable that contains the names for each tip.
    """
    logging.debug("Populating 'data' children nodes.")
    # The data string that combines all tips: '[Data($1), Data($2), ..., Data($12)]'
    combine_tip_data_str = "[" + ", ".join(["DATA(${0:d})".format(i) for i in tip_numbers]) + "]"
    
    # The node suffix looks like 'side' or 'vsense.div' so that the full name looks like 'tip01.side'.
    tip_node_names = lambda node_suffix: ['{name}.{suffix}'.format(name=tip_name, suffix=node_suffix) for tip_name in tip_names]

    # Extract the name of the node in the tip tree, the name of the node in the data tree, and the units of the node.
    # If the data_name is None then assume it's the same as the node suffix.
    for node_suffix, data_name, units in [('core.vprobe', 'vcore', 'V'), ('shell.vprobe', 'vshell', 'V'), ('core.iprobe', 'icore', 'A'), ('shell.iprobe', 'ishell', 'A'), 
        ('working', None, None), ('direction.machine', 'direction', 'm'), ('core.relarea', 'relareacore', None), ('shell.relarea', 'relareashell', None)]:
        if data_name is None:
            data_name = node_suffix

        data_node = top_node.getNode("data")
        # Get the reference to each tip signal.
        individual_nodes = modules.mds_builders.get_node_refs(top_node, *tip_node_names(node_suffix))

        # Compile the commands.
        # Dimension of first signal.
        dimension_str = 'DIM_OF($1)'
        logging.debug("Compiling command for '{}' in 'data'.".format(data_name))
        command = mds.Data.compile('BUILD_SIGNAL({combine_tips}, *, {dimension})'.format(combine_tips=combine_tip_data_str, dimension=dimension_str), *individual_nodes)
    
        # Get the nodes where these commands go.
        logging.debug("Getting '{}' node to insert command into.".format(data_name))
        combined_node = data_node.getNode(data_name)

        # Put the commands into the nodes.
        logging.debug("Inserting command.")
        if units is not None:
            modules.mds_builders.put_wrapper(combined_node, command, units=units)
        else:
            modules.mds_builders.put_wrapper(combined_node, command)
    logging.info("Populated 'data' children nodes.")

def populate_bdot(top_node):
    logging.debug("Populating 'bdot' children nodes.")
    bdot_node = top_node.getNode("bdot")
    
    for numbered_axis in ["ax1", "ax2", "ax3"]:
        axis_node = bdot_node.getNode(numbered_axis)

        gain_node = axis_node.getNode("gain")
        factor_node = axis_node.getNode("factor")
        raw_clockwise_node = axis_node.getNode("raw_cw")
        raw_counterclockwise_node = axis_node.getNode("raw_ccw")

        axis_command = mds.Data.compile("BUILD_SIGNAL($1 * ($3 - $2 * $4),, DIM_OF($3))", gain_node, factor_node, raw_clockwise_node, raw_counterclockwise_node)
        modules.mds_builders.put_wrapper(axis_node, axis_command, units="T/s")

def populate_noise_relay(top_node, avg_index_node):
    logging.debug("Populating 'noise_relay' children nodes.")
    noise_relay_node = top_node.getNode("noise_relay")
    raw_node = noise_relay_node.getNode("raw")
    avg_node = noise_relay_node.getNode("avg")
    limit_node = noise_relay_node.getNode("limit")

    # Put the averaging code into the avg node.
    modules.mds_builders.put_wrapper(avg_node, average_equation(raw_node, avg_index_node))
    # Put the boolean statement into the noise relay node.
    # TODO: Make sure that in actual measurements on the digitizer that 'limit' is a reasonable value for checking the relay status.
    relay_command = mds.Data.compile("IF($1 > $2) 1.0; ELSE 0.0;", avg_node, limit_node)
    modules.mds_builders.put_wrapper(noise_relay_node, relay_command)

def populate_vbias(top_node, avg_index_node):
    logging.debug("Populating 'vbias' children nodes.")
    # Get nodes.
    vbias_node = top_node.getNode("vbias")
    raw_node = vbias_node.getNode("raw")
    avg_node = vbias_node.getNode("avg")
    offset_node = vbias_node.getNode("offset")
    div_node = vbias_node.getNode("div")
    
    # Get and write commands.
    vbias_command = mds.Data.compile("($1 - $2) / $3", avg_node, offset_node, div_node)
    modules.mds_builders.put_wrapper(vbias_node, vbias_command, units='V')
    modules.mds_builders.put_wrapper(avg_node, average_equation(raw_node, avg_index_node), units='V')

def populate_tip(tip_node, vbias_node, noise_relay_node, avg_index_node):
    """
    Populate the functions for the nodes in a single tip.
    
    params:
    tip_nodes           = Top node for specific probe tip.
    vbias_node          = Bias voltage node.
    noise_relay_node    = Node holding status of tip switching relay.
    avg_index_node      = Parent node of indeces to average over.
    """
    logging.debug("Populating {tip_name} nodes.".format(tip_name=tip_node.getNodeName()))
    # Child nodes of this tip.
    logging.debug("Getting nodes to insert commands into and nodes used in commands.")
    core_node = tip_node.getNode("core")
    shell_node = tip_node.getNode("shell")

    rdiggnd_node = tip_node.getNode("rdiggnd")
    rdiv_node = tip_node.getNode("rdiv")
    rsense_node = tip_node.getNode("rsense")

    direction_node = tip_node.getNode("direction")

    # Populate nodes.
    logging.debug("Inserting commands.")
    populate_core(core_node, rdiggnd_node, rdiv_node, rsense_node, vbias_node, noise_relay_node, avg_index_node)
    populate_shell(shell_node, core_node, rdiggnd_node, rdiv_node, rsense_node, vbias_node, noise_relay_node, avg_index_node)
    populate_direction(direction_node)
    logging.info("Populated {tip_name} children nodes.".format(tip_name=tip_node.getNodeName()))

def populate_core(core_node, rdiggnd_node, rdiv_node, rsense_node, vbias_node, noise_relay_node, avg_index_node):
    """
    Populate the core sub-tree.
    """
    logging.debug("Populating 'core'.")

    # Get nodes.
    signal_node = core_node.getNode('signal')
    signal_default_node = signal_node.getNode('default')
    noise_node = core_node.getNode('noise')
    noise_default_node = noise_node.getNode('default')
    rwire_node = core_node.getNode('rwire')
    vprobe_node = core_node.getNode('vprobe')
    iprobe_node = core_node.getNode('iprobe')

    # Populate sub-trees.
    populate_signal_noise(signal_node, signal_default_node, noise_default_node, rdiggnd_node, noise_relay_node, avg_index_node, is_core=True, is_signal=True)
    populate_signal_noise(noise_node, signal_default_node, noise_default_node, rdiggnd_node, noise_relay_node, avg_index_node, is_core=True, is_signal=False)

    # Create equations.
    # 1. rdiv, 2. rwire, 3. rsense, 4. signal.i, 5. noise.i, 6. vbias
    vprobe_equation = mds.Data.compile("BUILD_SIGNAL(($1 + $2 + $1 * $2 / $3) * $4 - ($2 + $1 * $2 / $3) * $5 + $6,, DIM_OF($4))",
        rdiv_node, rwire_node, rsense_node, signal_node.getNode('i'), noise_node.getNode('i'), vbias_node)
    # 1. rdiv, 2. rsense, 3. signal.i, 4. noise.i
    iprobe_equation = mds.Data.compile("BUILD_SIGNAL(($1 / $2 + 1) * ($3 - $4),, DIM_OF($3))",
        rdiv_node, rsense_node, signal_node.getNode('i'), noise_node.getNode('i'))

    # Insert equations.
    modules.mds_builders.put_wrapper(vprobe_node, vprobe_equation, units='V')
    modules.mds_builders.put_wrapper(iprobe_node, iprobe_equation, units='A')

    logging.debug("Populated 'core'.")

def populate_shell(shell_node, core_node, rdiggnd_node, rdiv_node, rsense_node, vbias_node, noise_relay_node, avg_index_node):
    """
    Populate the shell sub-tree.
    """
    logging.debug("Populating 'shell'.")

    # Get nodes.
    signal_node = shell_node.getNode('signal')
    signal_default_node = signal_node.getNode('default')
    core_signal_node = core_node.getNode('signal')
    noise_node = shell_node.getNode('noise')
    noise_default_node = noise_node.getNode('default')
    core_noise_node = core_node.getNode('noise')
    rwire_node = shell_node.getNode('rwire')
    vprobe_node = shell_node.getNode('vprobe')
    iprobe_node = shell_node.getNode('iprobe')

    # Populate sub-trees.
    populate_signal_noise(signal_node, signal_default_node, noise_default_node, rdiggnd_node, noise_relay_node, avg_index_node, is_core=False, is_signal=True)
    populate_signal_noise(noise_node, signal_default_node, noise_default_node, rdiggnd_node, noise_relay_node, avg_index_node, is_core=False, is_signal=False)

    # Create equations.
    # 1. rdiv, 2. rwire, 3. rsense, 4. shell.signal.i, 5. vbias, 6. core.signal.i, 7. shell.noise.i, 8. core.noise.i
    vprobe_equation = mds.Data.compile("BUILD_SIGNAL(($1 + $2 + 3 * $1 * $2 / $3) * $4 + $5 - ($1 * $2 * $6 + $1 * $2 * $7 + $3 * $2 * $7 + $1 * $2 * $8) / $3,, DIM_OF($4))",
        rdiv_node, rwire_node, rsense_node, signal_node.getNode('i'), vbias_node, core_signal_node.getNode('i'), noise_node.getNode('i'), core_noise_node.getNode('i'))
    # 1. rdiv, 2. shell.signal.i, 3. rsense, 4. core.signal.i, 5. shell.noise.i, 6. core.noise.i
    iprobe_equation = mds.Data.compile("BUILD_SIGNAL((3 * $1 * $2 + $3 * $2 - $1 * $4 - $1 * $5 - $3 * $5 - $1 * $6) / $3,, DIM_OF($2))",
        rdiv_node, signal_node.getNode('i'), rsense_node, core_signal_node.getNode('i'), noise_node.getNode('i'), core_noise_node.getNode('i'))

    # Insert equations.
    modules.mds_builders.put_wrapper(vprobe_node, vprobe_equation, units='V')
    modules.mds_builders.put_wrapper(iprobe_node, iprobe_equation, units='A')

    logging.debug("Populated 'shell'.")

def populate_signal_noise(signal_noise_node, signal_default_node, noise_default_node, rdiggnd_node, noise_relay_node, avg_index_node, is_core, is_signal):
    """
    Populate either the signal or noise nodes for the core/shell sub-trees.
    """
    if is_signal:
        logging.debug("Populating 'signal'.")
    else:
        logging.debug("Populating 'noise'.")

    signal_noise_v_node = signal_noise_node.getNode('v')
    signal_noise_i_node = signal_noise_node.getNode('i')

    # Fill the 'default' sub-tree.
    logging.debug("Populating 'default'.")
    default_node = signal_noise_node.getNode('default')
    raw_node = default_node.getNode('raw')
    avg_node = default_node.getNode('avg')

    modules.mds_builders.put_wrapper(avg_node, average_equation(raw_node, avg_index_node), units='V')
    modules.mds_builders.put_wrapper(default_node, mds.Data.compile("BUILD_SIGNAL($1 - $2,, DIM_OF($1))", raw_node, avg_node), units='V')
    logging.debug("Finished populating 'default'.")

    # Fill the 'v' node.
    if is_core:
        if is_signal:
            v_equation = mds.Data.compile("IF($1 > 0.5) $2; ELSE $3;", noise_relay_node, noise_default_node, signal_default_node)
            modules.mds_builders.put_wrapper(signal_noise_v_node, v_equation, units='V')
        else:
            v_equation = mds.Data.compile("IF($1 > 0.5) $2; ELSE $3;", noise_relay_node, signal_default_node, noise_default_node)
            modules.mds_builders.put_wrapper(signal_noise_v_node, v_equation, units='V')
    else:
        modules.mds_builders.put_wrapper(signal_noise_v_node, mds.Data.compile("$1", default_node), units='V')

    # Fill the 'i' node.
    modules.mds_builders.put_wrapper(signal_noise_i_node, mds.Data.compile("BUILD_SIGNAL($1 / $2,, DIM_OF($1))", signal_noise_v_node, rdiggnd_node))

    if is_signal:
        logging.debug("Finished populating 'signal'.")
    else:
        logging.debug("Finished populating 'core'.")

def populate_direction(direction_node):
    """
    Populate the functions for the nodes in 'direction'.
    """
    logging.debug("Populating 'direction'.")
    machine_node = direction_node.getNode('machine')
    r_node = machine_node.getNode('r')
    phi_node = machine_node.getNode('phi')
    z_node = machine_node.getNode('z')

    machine_equation = mds.Data.compile("BUILD_SIGNAL( [ DATA($1), DATA($2), DATA($3) ],, DIM_OF($1))", r_node, phi_node, z_node)
    modules.mds_builders.put_wrapper(machine_node, machine_equation, units='m')
    logging.debug("Populated 'direction'.")

def average_equation(signal_node, avg_index_node):
    """
    Create the compiled function for averaging a signal.

    params:
    signal_node     = Node to average over.
    avg_index_node  = Node that contains the start and end nodes.

    returns:
    avg_equation    = Compiled equation for averaged node.
    """
    logging.debug("Making averaging equation.")
    start_node = avg_index_node.getNode("start")
    end_node = avg_index_node.getNode("end")
    return mds.Data.compile("MEAN(DATA($1)[$2 : $3])", signal_node, start_node, end_node)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, filename='./create_logs/pa_probe1_create.log', filemode='w')

    build_structure()
    populate_tree()
    logging.info("pa_probe1 tree created!")
