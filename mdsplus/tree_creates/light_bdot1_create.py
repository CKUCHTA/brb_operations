import MDSplus as mds
import modules.mds_builders
from modules.tree_creation import *
import logging


def build_structure():
    """
    Build the full tree node structure with tags. Equations are not filled.
    """
    # light_bdot1 tree structure:
    # Top
        # data_mach (ST)
            # dbr, dbphi, dbz [T/s] (S) BUILD_SIGNAL(pb01.dbr, ...) dB / dt in machine coordinates by combining 3 coils together.
            # r, phi, z [m] (S) BUILD_SIGNAL(pb01.r, ...) Center locations of each coil triplet.
        # data_loc (ST)
            # db1, db2, db3a, db3b [T/s] (S) BUILD_SIGNAL(pcb01.db1, ...) dB / dt for each coil.
                # r, phi, z [m or rad] (S) BUILD_SIGNAL(pcb01.db1.r, ...) Location of each coil.
                # working (S) BUILD_SIGNAL(pcb01.db1.working, ...) Working status of each coil.
                # orientation (ST)
                    # x, y, z (S) BUILD_SIGNAL(pcb01.db1.orientation.x, ...) Orientation of each coil.
        # port (ST)
            # clock [deg] (N) From MySQL thru post run code.
            # rport [m] (N) From MySQL thru post run code.
            # lat [deg] (N) From MySQL thru post run code.
            # long [deg] (N) From MySQL thru post run code.
            # alpha [deg] (N) From MySQL thru post run code.
            # beta [deg] (N) From MySQL thru post run code.
            # gamma [deg] (N) From MySQL thru post run code.
            # insert [m] (N) From MySQL thru post run code.
        # probes (ST)
            # pb01, pb02, ..., pb64 (ST)
                # db1, db2, db3 [T/s] (S) pcbs.pcb__.db_ This scans along the PCBs in triplets so PCB # != probe #.
                # dbr, dbphi, dbz [T/s] (S) From MySQL thru post run code.
                # r, phi, z [m or rad] (S) mean( [pcb__.db_.r, pcb__.db_.r, pcb__.db_.r] )
        # pcbs (ST)
            # pcb01, pcb02, ..., pcb32 (ST)
                # db1, db2, db3a, db3b [T/s] (S) gain * (raw - avg)
                    # raw [V] (S) From config.
                    # gain [T / (s V)] (N) From config.
                    # avg [V] (N) mean( raw )
                    # r, phi, z [m or rad] (N) From MySQL thru post run code.
                    # working [N] From config.
                    # orientation (ST)
                        # x, y, z (N) From config. These are in the port coordinate system (x=North, y=East, z=Down) when clocking is 0.
    
    tree_name = "light_bdot1"
    node_tag_prefix = "light_bdot1"

    # Create the tree for editing.
    logging.info("Creating top of tree...")
    tree = mds.Tree(tree_name, shot=-1, mode="New")
    top = tree.getNode("\\top")
    tree.edit()

    tree_structure = {
        Node("data_mach", ST): {
            Node("dbr", S, NODE_NAME),
            Node("dbphi", S, NODE_NAME),
            Node("dbz", S, NODE_NAME),
            Node("r", S, NODE_NAME),
            Node("phi", S, NODE_NAME),
            Node("z", S, NODE_NAME),
        },

        Node("data_loc", ST): {
            Node("db1", S, NODE_NAME, subtree_prefix="db1"): {
                Node("r", S, NODE_NAME): None,
                Node("phi", S, NODE_NAME): None,
                Node("z", S, NODE_NAME): None,
                Node("working", S, NODE_NAME): None,
                Node("orientation", ST): {
                    Node("x", S),
                    Node("y", S),
                    Node("z", S),
                }
            },
            Node("db2", S, NODE_NAME, subtree_prefix="db2"): {
                Node("r", S, NODE_NAME): None,
                Node("phi", S, NODE_NAME): None,
                Node("z", S, NODE_NAME): None,
                Node("working", S, NODE_NAME): None,
                Node("orientation", ST): {
                    Node("x", S),
                    Node("y", S),
                    Node("z", S),
                }
            },
            Node("db3a", S, NODE_NAME, subtree_prefix="db3a"): {
                Node("r", S, NODE_NAME): None,
                Node("phi", S, NODE_NAME): None,
                Node("z", S, NODE_NAME): None,
                Node("working", S, NODE_NAME): None,
                Node("orientation", ST): {
                    Node("x", S),
                    Node("y", S),
                    Node("z", S),
                }
            },
            Node("db3b", S, NODE_NAME, subtree_prefix="db3b"): {
                Node("r", S, NODE_NAME): None,
                Node("phi", S, NODE_NAME): None,
                Node("z", S, NODE_NAME): None,
                Node("working", S, NODE_NAME): None,
                Node("orientation", ST): {
                    Node("x", S),
                    Node("y", S),
                    Node("z", S),
                }
            },
        },

        Node("port", ST): {
            Node("clock", N, NODE_NAME),
            Node("rport", N, NODE_NAME),
            Node("lat", N, NODE_NAME),
            Node("long", N, NODE_NAME),
            Node("alpha", N, NODE_NAME),
            Node("beta", N, NODE_NAME),
            Node("gamma", N, NODE_NAME),
            Node("insert", N, NODE_NAME)
        },

        Node("probes", ST): {},
        Node("pcbs", ST): {},
    }

    probe_tree_structure = {
        Node("db1", S),
        Node("db2", S),
        Node("db3", S),
        Node("dbr", S),
        Node("dbphi", S),
        Node("dbz", S),
        Node("r", S),
        Node("phi", S),
        Node("z", S),
    }

    pcb_tree_structure = {
        Node("db1", S): {
            Node("raw", S): None,
            Node("gain", N): None,
            Node("avg", N): None,
            Node("r", S): None,
            Node("phi", S): None,
            Node("z", S): None,
            Node("working", N): None,
            Node("orientation", ST): {
                Node("x", N),
                Node("y", N),
                Node("z", N),
            }
        },

        Node("db2", S): {
            Node("raw", S): None,
            Node("gain", N): None,
            Node("avg", N): None,
            Node("r", S): None,
            Node("phi", S): None,
            Node("z", S): None,
            Node("working", N): None,
            Node("orientation", ST): {
                Node("x", N),
                Node("y", N),
                Node("z", N),
            }
        },

        Node("db3a", S): {
            Node("raw", S): None,
            Node("gain", N): None,
            Node("avg", N): None,
            Node("r", S): None,
            Node("phi", S): None,
            Node("z", S): None,
            Node("working", N): None,
            Node("orientation", ST): {
                Node("x", N),
                Node("y", N),
                Node("z", N),
            }
        },

        Node("db3b", S): {
            Node("raw", S): None,
            Node("gain", N): None,
            Node("avg", N): None,
            Node("r", S): None,
            Node("phi", S): None,
            Node("z", S): None,
            Node("working", N): None,
            Node("orientation", ST): {
                Node("x", N),
                Node("y", N),
                Node("z", N),
            }
        },
    }

    logging.debug("Creating tree structure...")
    recursive_tree_build(tree, top, tree_structure, tag_prefix=node_tag_prefix + "_")

    logging.debug("Creating probe tree structure...")
    num_probes = 64
    for probe_number in range(1, num_probes + 1):
        number_str = str(probe_number).zfill(2)
        # Create the probe node info.
        probe_name = "pb{}".format(number_str)
        probe_node_type = ST
        probe_tag_prefix = "{}_{}_".format(node_tag_prefix, probe_name)

        # Add the probe to the `probes` subtree.
        tree.setDefault(top)
        probes_node = tree.getNode("probes")
        tree.setDefault(probes_node)
        logging.debug("Adding probe node " + probe_name)
        tree.addNode(probe_name, probe_node_type)
        probe_node = tree.getNode(probe_name)
        logging.info("Added probe node " + probe_node.getNodeName())

        recursive_tree_build(tree, probe_node, probe_tree_structure, probe_tag_prefix)
    
    logging.debug("Creating PCB tree structure...")
    num_pcbs = 32
    for pcb_number in range(1, num_pcbs + 1):
        number_str = str(pcb_number).zfill(2)
        # Create the pcb node info.
        pcb_name = "pcb{}".format(number_str)
        pcb_node_type = ST
        pcb_tag_prefix = "{}_{}_".format(node_tag_prefix, pcb_name)

        # Add the pcb to the `pcbs` subtree.
        tree.setDefault(top)
        pcbs_node = tree.getNode("pcbs")
        tree.setDefault(pcbs_node)
        logging.debug("Adding pcb node " + pcb_name)
        tree.addNode(pcb_name, pcb_node_type)
        pcb_node = tree.getNode(pcb_name)
        logging.info("Added pcb node " + pcb_node.getNodeName())

        recursive_tree_build(tree, pcb_node, pcb_tree_structure, pcb_tag_prefix)

    logging.info("Writing and printing tree...")
    tree.write()
    modules.mds_builders.traverse_tree(top)
    tree.cleanDatafile()
    tree.quit()

def populate_tree():
    """
    Run the top level populator that calls populators for first subtrees.
    """
    logging.debug("Populating top of tree.")
    tree = mds.Tree("light_bdot1", -1)
    top_node = tree.getNode("\\top")

    # Get all the next level nodes.
    data_mach_node = top_node.getNode("data_mach")
    data_loc_node = top_node.getNode("data_loc")
    probes_node = top_node.getNode("probes")
    pcbs_node = top_node.getNode("pcbs")

    probe_names = ['pb{}'.format(str(i).zfill(2)) for i in range(1, 65)]
    probe_nodes = modules.mds_builders.get_node_refs(probes_node, *probe_names)
    pcb_names = ['pcb{}'.format(str(i).zfill(2)) for i in range(1, 33)]
    pcb_nodes = modules.mds_builders.get_node_refs(pcbs_node, *pcb_names)

    # Populate `data_mach`.
    populate_data_mach(data_mach_node, probe_nodes)

    # Populate `data_loc`.
    populate_data_loc(data_loc_node, pcb_nodes)

    # Populate `probes`.
    populate_probes(probes_node, pcb_nodes)

    # Populate `pcbs`.
    populate_pcbs(pcb_nodes)
    logging.info("Populated top of tree.")

def populate_data_mach(data_mach_node, probe_nodes):
    logging.debug("Populating 'data_mach' children nodes.")
    # The data string that combines all probes: '[Data($1), Data($2), ..., Data($64)]'
    combine_probe_data_str = "[" + ", ".join(["DATA(${0:d})".format(i) for i in range(1, len(probe_nodes) + 1)]) + "]"

    # Compile the commands.
    # Dimension of first signal.
    dimension_str = "DIM_OF($1)"
    logging.debug("Compiling 'data_mach' db commands.")
    dbr_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_probe_data_str, dimension_str
    ), *[probe.getNode('dbr') for probe in probe_nodes])
    dbphi_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_probe_data_str, dimension_str
    ), *[probe.getNode('dbphi') for probe in probe_nodes])
    dbz_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_probe_data_str, dimension_str
    ), *[probe.getNode('dbz') for probe in probe_nodes])
    logging.debug("Compiling 'data_mach' position commands.")
    r_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_probe_data_str, dimension_str
    ), *[probe.getNode('r') for probe in probe_nodes])
    phi_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_probe_data_str, dimension_str
    ), *[probe.getNode('phi') for probe in probe_nodes])
    z_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_probe_data_str, dimension_str
    ), *[probe.getNode('z') for probe in probe_nodes])

    # Get the nodes where these commands go.
    logging.debug("Getting nodes to insert commands into.")
    dbr_node = data_mach_node.getNode("dbr")
    dbphi_node = data_mach_node.getNode("dbphi")
    dbz_node = data_mach_node.getNode("dbz")
    r_node = data_mach_node.getNode("r")
    phi_node = data_mach_node.getNode("phi")
    z_node = data_mach_node.getNode("z")

    # Put the commands into the nodes.
    logging.debug("Inserting commands.")
    modules.mds_builders.put_wrapper(dbr_node, dbr_command, units='T / s')
    modules.mds_builders.put_wrapper(dbphi_node, dbphi_command, units='T / s')
    modules.mds_builders.put_wrapper(dbz_node, dbz_command, units='T / s')
    modules.mds_builders.put_wrapper(r_node, r_command, units='m')
    modules.mds_builders.put_wrapper(phi_node, phi_command, units='rad')
    modules.mds_builders.put_wrapper(z_node, z_command, units='m')
    logging.info("Populated 'data_mach' children nodes.")

def populate_data_loc(data_loc_node, pcb_nodes):
    logging.debug("Populating 'data_loc' children nodes.")
    # The data string that combines all pcbs: '[Data($1), Data($2), ..., Data($64)]'
    combine_pcb_data_str = "[" + ", ".join(["DATA(${0:d})".format(i) for i in range(1, len(pcb_nodes) + 1)]) + "]"

    # Compile the commands.
    # Dimension of first signal.
    dimension_str = "DIM_OF($1)"
    logging.debug("Compiling 'data_loc' db commands.")
    db1_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_pcb_data_str, dimension_str
    ), *[pcb.getNode('db1') for pcb in pcb_nodes])
    db2_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_pcb_data_str, dimension_str
    ), *[pcb.getNode('db2') for pcb in pcb_nodes])
    db3a_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_pcb_data_str, dimension_str
    ), *[pcb.getNode('db3a') for pcb in pcb_nodes])
    db3b_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_pcb_data_str, dimension_str
    ), *[pcb.getNode('db3b') for pcb in pcb_nodes])

    # Get the nodes where those commands go.
    logging.debug("Getting nodes to insert commands into.")
    db1_node = data_loc_node.getNode('db1')
    db2_node = data_loc_node.getNode('db2')
    db3a_node = data_loc_node.getNode('db3a')
    db3b_node = data_loc_node.getNode('db3b')

    # Put the commands into the nodes.
    logging.debug("Inserting commands.")
    modules.mds_builders.put_wrapper(db1_node, db1_command, units='T / s')
    modules.mds_builders.put_wrapper(db2_node, db2_command, units='T / s')
    modules.mds_builders.put_wrapper(db3a_node, db3a_command, units='T / s')
    modules.mds_builders.put_wrapper(db3b_node, db3b_command, units='T / s')

    for coil in ['db1', 'db2', 'db3a', 'db3b']:
        node = data_loc_node.getNode(coil)
        pcb_coil_nodes = [pcb.getNode(coil) for pcb in pcb_nodes]

        populate_combined_db(node, pcb_coil_nodes, combine_pcb_data_str)
    logging.info("Populated 'data_loc' children nodes.")

def populate_combined_db(coil_node, pcb_coil_nodes, combine_pcb_data_str):
    logging.debug("Populating db positions.")

    # Compile the commands.
    dimension_str = "DIM_OF($1)"
    logging.debug("Compiling position commands.")
    r_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_pcb_data_str, dimension_str
    ), *[pcb.getNode('r') for pcb in pcb_coil_nodes])
    phi_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_pcb_data_str, dimension_str
    ), *[pcb.getNode('phi') for pcb in pcb_coil_nodes])
    z_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_pcb_data_str, dimension_str
    ), *[pcb.getNode('z') for pcb in pcb_coil_nodes])
    working_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_pcb_data_str, dimension_str
    ), *[pcb.getNode('working') for pcb in pcb_coil_nodes])

    # Get the nodes where those commands go.
    logging.debug("Getting nodes to insert commands into.")
    r_node = coil_node.getNode('r')
    phi_node = coil_node.getNode('phi')
    z_node = coil_node.getNode('z')
    working_node = coil_node.getNode('working')

    # Put the commands into the nodes.
    logging.debug("Inserting commands.")
    modules.mds_builders.put_wrapper(r_node, r_command, units='m')
    modules.mds_builders.put_wrapper(phi_node, phi_command, units='rad')
    modules.mds_builders.put_wrapper(z_node, z_command, units='m')
    modules.mds_builders.put_wrapper(working_node, working_command)

    populate_combined_orientation(coil_node.getNode('orientation'), pcb_coil_nodes, combine_pcb_data_str)
    logging.info("Populated db positions.")

def populate_combined_orientation(orientation_node, pcb_coil_nodes, combine_pcb_data_str):
    logging.debug("Populating combined orientation node.")

    # Compile the commands.
    dimension_str = "DIM_OF($1)"
    logging.debug("Compiling orientation commands.")
    x_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_pcb_data_str, dimension_str
    ), *[pcb.getNode('orientation').getNode('x') for pcb in pcb_coil_nodes])
    y_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_pcb_data_str, dimension_str
    ), *[pcb.getNode('orientation').getNode('y') for pcb in pcb_coil_nodes])
    z_command = mds.Data.compile('BUILD_SIGNAL({}, *, {})'.format(
        combine_pcb_data_str, dimension_str
    ), *[pcb.getNode('orientation').getNode('z') for pcb in pcb_coil_nodes])

    # Get the nodes where those commands go.
    x_node = orientation_node.getNode('x')
    y_node = orientation_node.getNode('y')
    z_node = orientation_node.getNode('z')

    # Put the commands into the nodes.
    logging.debug("Inserting commands.")
    modules.mds_builders.put_wrapper(x_node, x_command)
    modules.mds_builders.put_wrapper(y_node, y_command)
    modules.mds_builders.put_wrapper(z_node, z_command)
    logging.info("Populated combined orientation.")

def populate_probes(probes_node, pcb_nodes):
    logging.debug("Populating probes.")

    for probe_number in range(1, 65):
        logging.debug("Populating probe #{}.".format(probe_number))
        if probe_number == 32:
            # This is just left of the middle.
            pcb_db1_num = 16
            pcb_db2_num = 16
            pcb_db3_num = 16
            pcb_db3_option = 'a'
        elif probe_number == 33:
            # This is just right of the middle.
            pcb_db1_num = 17
            pcb_db2_num = 17
            pcb_db3_num = 17
            pcb_db3_option = 'a'
        elif (probe_number % 2 == 1 and probe_number < 32):
            pcb_db1_num = probe_number // 2 + 1
            pcb_db2_num = probe_number // 2 + 1
            pcb_db3_num = probe_number // 2 + 1
            pcb_db3_option = 'b'
        elif (probe_number % 2 == 0 and probe_number < 32):
            pcb_db1_num = probe_number // 2
            pcb_db2_num = probe_number // 2 + 1
            pcb_db3_num = probe_number // 2
            pcb_db3_option = 'a'
        elif (probe_number % 2 == 1 and probe_number > 33):
            pcb_db1_num = probe_number // 2 + 1
            pcb_db2_num = probe_number // 2
            pcb_db3_num = probe_number // 2 + 1
            pcb_db3_option = 'a'
        elif (probe_number % 2 == 0 and probe_number > 33):
            pcb_db1_num = probe_number // 2
            pcb_db2_num = probe_number // 2
            pcb_db3_num = probe_number // 2
            pcb_db3_option = 'b'


        logging.debug("Getting pcb and probe nodes.")    
        pcb_db1_node = pcb_nodes[pcb_db1_num - 1].getNode('db1')
        pcb_db2_node = pcb_nodes[pcb_db2_num - 1].getNode('db2')
        pcb_db3_node = pcb_nodes[pcb_db3_num - 1].getNode('db3{}'.format(pcb_db3_option))

        probe_node = probes_node.getNode('pb{}'.format(str(probe_number).zfill(2)))
        probe_db1_node = probe_node.getNode('db1')
        probe_db2_node = probe_node.getNode('db2')
        probe_db3_node = probe_node.getNode('db3')

        probe_r_node = probe_node.getNode('r')
        probe_phi_node = probe_node.getNode('phi')
        probe_z_node = probe_node.getNode('z')

        logging.debug("Compiling commands.")
        probe_db1_command = mds.Data.compile("$1", pcb_db1_node)
        probe_db2_command = mds.Data.compile("$1", pcb_db2_node)
        probe_db3_command = mds.Data.compile("$1", pcb_db3_node)

        probe_r_command = mds.Data.compile("MEAN([{}])".format(
            ", ".join("DATA(${})".format(i) for i in range(1, 4))
        ), *[node.getNode('r') for node in [pcb_db1_node, pcb_db2_node, pcb_db3_node]])
        probe_phi_command = mds.Data.compile("MEAN([{}])".format(
            ", ".join("DATA(${})".format(i) for i in range(1, 4))
        ), *[node.getNode('phi') for node in [pcb_db1_node, pcb_db2_node, pcb_db3_node]])
        probe_z_command = mds.Data.compile("MEAN([{}])".format(
            ", ".join("DATA(${})".format(i) for i in range(1, 4))
        ), *[node.getNode('z') for node in [pcb_db1_node, pcb_db2_node, pcb_db3_node]])

        # Put the commands into the nodes.
        logging.debug("Inserting commands.")
        modules.mds_builders.put_wrapper(probe_db1_node, probe_db1_command, units='')
        modules.mds_builders.put_wrapper(probe_db2_node, probe_db2_command, units='')
        modules.mds_builders.put_wrapper(probe_db3_node, probe_db3_command, units='')

        modules.mds_builders.put_wrapper(probe_r_node, probe_r_command, units='')
        modules.mds_builders.put_wrapper(probe_phi_node, probe_phi_command, units='')
        modules.mds_builders.put_wrapper(probe_z_node, probe_z_command, units='')
        logging.info("Populated probe #{}.".format(probe_number))
    logging.info("Populated probes.")

def populate_pcbs(pcb_nodes):
    logging.debug("Populating pcbs.")
    for i, pcb_node in enumerate(pcb_nodes, start=1):
        logging.debug("Populating pcb #{}.".format(i))
        for coil_str in ['1', '2', '3a', '3b']:
            logging.debug("Populating coil {}.".format(coil_str))
            coil_node = pcb_node.getNode('db{}'.format(coil_str))

            # Get the nodes.
            logging.debug("Getting nodes.")
            raw_node = coil_node.getNode('raw')
            gain_node = coil_node.getNode('gain')
            avg_node = coil_node.getNode('avg')

            # Compile the commands.
            dimension_str = "DIM_OF($1)"
            logging.debug("Compiling coil {} commands.".format(coil_str))
            avg_command = mds.Data.compile('MEAN( $1 )', raw_node)
            coil_command = mds.Data.compile('BUILD_SIGNAL($2 * ($1 - $3),, {})'.format(dimension_str),
                raw_node, gain_node, avg_node
            )

            # Put the commands into the nodes.
            logging.debug("Inserting commands.")
            modules.mds_builders.put_wrapper(avg_node, avg_command, units='V')
            modules.mds_builders.put_wrapper(coil_node, coil_command, units='T / s')
            logging.info("Populated coil {}.".format(coil_str))


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, filename='./create_logs/light_bdot1_create_log.txt', filemode='w')

    build_structure()
    populate_tree()
    logging.info("light_bdot1 tree created!")
