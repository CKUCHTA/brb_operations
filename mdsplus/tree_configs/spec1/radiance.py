#!/opt/anaconda2/bin/python
from __future__ import print_function, division
import numpy as np
import MDSplus as mds
import sys
import matplotlib.pyplot as plt

edges = {'486': (483.0, 490.0),
         '656': (653.57, 660.61),
         '667': (665.80, 671.10),
         '706': (704.71, 710.00),
         '727': (726.0, 731.10),
         '810': (807.50, 816.50),
        }


nodes = {'486': "spectroscopy.spec1.radiance.rad_486",
         '656': "spectroscopy.spec1.radiance.rad_656",
         '667': "spectroscopy.spec1.radiance.rad_667",
         '706': "spectroscopy.spec1.radiance.rad_706",
         '727': "spectroscopy.spec1.radiance.rad_727",
         '810': "spectroscopy.spec1.radiance.rad_810",
        }


def read_raw_data(shot_number):
    tree = mds.Tree("wipal", shot_number)

    wavelength = tree.getNode("spectroscopy.spec1.wavelength").data()
    calibration = tree.getNode("spectroscopy.spec1.rad_cal").data()
    spectrum = tree.getNode("spectroscopy.spec1.spectrum").data()
    int_time = tree.getNode("spectroscopy.spec1.int_time").data()
    # offset = spectrum[0, :]
    # plt.plot(wavelength, offset)
    # plt.plot(wavelength, spectrum[1, :])
    # plt.show()
    # plt.plot(wavelength, calibration)
    # plt.show()
    offset = 0.0
    spectral_radiance = calibration * (spectrum - offset) / int_time

    return wavelength, spectral_radiance


def find_wavelength_bins(wavelength, left, right):

    wleft = np.abs(wavelength - left).argmin()
    wright = np.abs(wavelength - right).argmin() + 1

    return wleft, wright


def calculate_radiance(wavelength, spectral_radiance, plotit=False):
    bins = dict()
    radiance = dict()
    fig = None
    for w, (left, right) in edges.items():
        left_idx, right_idx = find_wavelength_bins(wavelength, left, right)
        bins[w] = (left_idx, right_idx)

        spec_rad = np.trapz(spectral_radiance[:, left_idx:right_idx], 
                            x=wavelength[left_idx:right_idx], axis=1)
        spec_rad -= spec_rad[0]
        radiance[w] = spec_rad 
        if plotit:
            if fig is None:
                fig, ax = plt.subplots()
            ax.plot(spec_rad, 'o', label=w)

    if plotit:
        plt.legend()
        plt.show()
    return bins, radiance


def write_to_tree(shot_number, radiance_dict, bin_dict):
    tree = mds.Tree("wipal", shot_number)

    for w in radiance_dict:
        node = tree.getNode(nodes[w])
        node.putData(mds.Float64Array(radiance_dict[w]))

        ind_node = node.getNode("indices_bin")
        indices = range(bin_dict[w][0], bin_dict[w][1])
        ind_node.putData(mds.Int32Array(indices))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Please enter a shot number")
        sys.exit()

    try:
        shot = int(sys.argv[1])
    except ValueError:
        print("Please enter a valid shot number")
        sys.exit()

    wavelength, spectral_radiance = read_raw_data(shot)
    n, m = spectral_radiance.shape
    #print(n, m)
    #for i in range(n):
    #    plt.plot(wavelength, spectral_radiance[i, :])
    #plt.show()
    bins, radiance = calculate_radiance(wavelength, spectral_radiance, plotit=False)
    write_to_tree(shot, radiance, bins)


