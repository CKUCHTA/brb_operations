#!/opt/anaconda2/bin/python
import MDSplus as mds
import modules.mds_builders as mdsbuild
import numpy as np
from sqltools.core import start_cnx, close_cnx
import pandas.io.sql as pio
from modules.mysql_connection import get_engine
import pandas
import sys


def read_sql(shot_number):
    sql_config_path="/home/WIPALdata/sql_config.ini"
    engine = get_engine(sql_config_path)
    database = engine.url.database
    with engine.connect() as connection:
        sql_query = 'select * from {0}.spec1 where shot_num = {1:d}'.format(database, shot_number)
        result = pandas.read_sql(sql_query, connection)

    return result


def parse_dataframe(frame, shot_number):

    digitizer = frame.loc[shot_number, 'timing_digitizer']
    channel = frame.loc[shot_number, 'timing_ch']
    n_spectra = frame.loc[shot_number, 'number_of_spectra']

    return digitizer, channel, n_spectra


def write_to_mdsplus(shot_number, digitizer, channel, n_spectra):
    tree = mds.Tree("wipal", shot_number)

    node_loc = "spectroscopy.spec1.time.timing"
    timing_node = tree.getNode(node_loc)
    timing_node.putData(mds.TreePath("\\top.raw.{0}.ch_{1:02d}".format(digitizer, channel)))
    
    n_spectra_node = tree.getNode("spectroscopy.spec1.time.n_spectra")
    n_spectra_node.putData(mds.Int32(n_spectra))

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("You must enter a shot number.")
        sys.exit()

    try:
        shot = int(sys.argv[1])
    except ValueError:
        print("You must enter a valid shot number.")
        sys.exit()

    info = read_sql(shot)
    digitizer, channel, n_spectra = parse_dataframe(info, shot)
    write_to_mdsplus(shot, digitizer, channel, n_spectra)



