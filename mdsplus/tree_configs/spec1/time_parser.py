#!/opt/anaconda2/bin/python
import numpy as np
from scipy import signal
import MDSplus as mds
import matplotlib.pyplot as plt
from time import time
import sys


def fake_timing_signal(tstart, tstop, timestep, ton=0.001, toff=0.002, delta=0.001, plotit=False):
    n_points = (tstop - tstart) / timestep + 1

    t = np.linspace(tstart, tstop, n_points)

    period = ton + toff
    duty = ton / period

    data = signal.square(2*np.pi*(t-delta)/period, duty=duty)
    data = 5.0*(data + 1)/2

    if plotit:
        fig, ax = plt.subplots()
        ax.plot(t, data)
        ax.set_ylim(-.1, 5.1)
        ax.set_xlim(tstart, tstop)
        plt.show()

    return t, data 


def parser(time, data, thres=2.5, plotit=False):
    rises = np.where(np.logical_and(data[0:-1] < thres, data[1:] > thres)) 

    t_rises = time[rises]
    print(data[0])
    if data[0] > thres:
        t_rises = np.hstack(([0.0], t_rises))

    if plotit:
        fig, ax = plt.subplots()
        ax.plot(time, data)
        #ax.set_ylim(-.1, 5.1)
        #ax.set_xlim(time[0], time[-1])
        for t in t_rises:
            ax.axvline(t, color='g')
        plt.show()
    return t_rises


def read_raw_timing(shot_number):
    tree = mds.Tree('wipal', shot_number)
    timing_node = tree.getNode("spectroscopy.spec1.time.timing")

    pulse_train = timing_node.data()
    t = timing_node.dim_of().data()

    n_spectra_node = tree.getNode("spectroscopy.spec1.time.n_spectra")
    n_spectra = n_spectra_node.data()

    return t, pulse_train, n_spectra


def write_time(shot_number, intergration_times):
    tree = mds.Tree("wipal", shot_number)
    time_node = tree.getNode("spectroscopy.spec1.time")

    time_node.putData(intergration_times)


if __name__ == "__main__":
    # Testing
    # 1 second time trace at 200 kHz
    t, data = fake_timing_signal(0.0, 2.0, 5e-6)
    t0 = time()
    parser(t, data)
    print(time()-t0)

    # Actual Analysis
    if len(sys.argv) < 2:
        print("Please Enter a Shot Number")
        sys.exit()

    try:
        shot = int(sys.argv[1])
    except ValueError:
        print("Please Enter a Valid Shot Number")
        sys.exit()

    t, timing, n_spectra = read_raw_timing(shot)
    integration_times = parser(t, timing, plotit=False)
    print(integration_times)
    write_time(shot, integration_times[0:n_spectra])


