#!/usr/bin/python3
import numpy as np
import pandas as pio
import MDSplus as mds
import modules.mds_builders as mdsbuild
from modules.mysql_connection import get_engine
import argparse


def main(shot_num):
    wipal_tree = mds.Tree("wipal", shot_num)
    cathode_top = wipal_tree.getNode("discharge.cathodes")
    n_cathodes = 12
    cathodes = ["cathode_{0:02d}".format(x+1) for x in range(n_cathodes)]
    for idx, cathode in enumerate(cathodes):
        cathode_node = cathode_top.getNode(cathode)
        status = sql_log(cathode_node, shot_num, idx+1)
        if status:
            calculate_offsets(cathode_node)


def sql_log(cath_node, shot_num, cathode_number):
    engine = get_engine()
    database = engine.url.database
    with engine.connect() as cnx:
        query = "select * from {0}.shot_log_cathode{1:d} where shot_num = {2:d}".format(database, cathode_number, shot_num)
        result = pio.read_sql(query, cnx)

    if result.status[0] == 'off':
        print("Cathode {0:d} is off.  Let's just move on then! No need to throw an exception!".format(cathode_number))
        return False
    else:
        write_mdsplus_data(cath_node, result)
        return True


def write_mdsplus_data(cath_node, info):
    current, voltage = mdsbuild.get_node_refs(cath_node, "current", "voltage")
    cur_gain, cur_raw = mdsbuild.get_node_refs(current, "gain", "raw")
    vol_gain, vol_raw = mdsbuild.get_node_refs(voltage, "gain", "raw")

    v_gain = info.voltage_cal[0]
    c_gain = info.current_cal[0] 

    mdsbuild.put_wrapper(cur_gain, mds.Float32(c_gain), err=mds.Float32(0.02*c_gain), units="A/V")
    mdsbuild.put_wrapper(vol_gain, mds.Float32(v_gain), err=mds.Float32(0.02*v_gain), units="1")

    vol_path = "\\top.raw.a223_direct.ch_{0:02d}".format(info.vol_chan[0]).upper()
    cur_path = "\\top.raw.a223_direct.ch_{0:02d}".format(info.cur_chan[0]).upper()

    vol_raw.putData(mds.TreePath(vol_path))
    cur_raw.putData(mds.TreePath(cur_path))


def calculate_offsets(cath_node, npts=100):

    current, voltage = mdsbuild.get_node_refs(cath_node, "current", "voltage")
    cur_offset, cur_raw = mdsbuild.get_node_refs(current, "offset", "raw")
    vol_offset, vol_raw = mdsbuild.get_node_refs(voltage, "offset", "raw")

    cur_data = cur_raw.data()
    vol_data = vol_raw.data()

    c_offset = mds.Float32(np.mean(cur_data[0:npts]))
    v_offset = mds.Float32(np.mean(vol_data[0:npts]))

    c_offset_sd = mds.Float32(np.std(cur_data[0:npts]))
    v_offset_sd = mds.Float32(np.std(vol_data[0:npts]))

    mdsbuild.put_wrapper(cur_offset, c_offset, err=c_offset_sd, units="V")
    mdsbuild.put_wrapper(vol_offset, v_offset, err=v_offset_sd, units="V")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Reads sql data and fills in necessary data in the cathode tree.")
    parser.add_argument("shot_num", metavar="Shot Number", type=int, help="Shot number to fill in MDSplus tree")
    args = parser.parse_args()

    main(args.shot_num)


