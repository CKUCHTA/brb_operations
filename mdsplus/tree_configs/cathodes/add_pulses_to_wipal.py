import MDSplus as mds

shots = range(29355,29359)
for shot in shots:
    try:                                                            
        myTree = mds.Tree("cathodes",shot)
    except mds.TreeException:
        try:
            print("creating new cathodes pulse: shot {0}...".format(shot))
            modelTree = mds.Tree("cathodes",-1)
            modelTree.createPulse(shot)
            mds.tree.Tree.setCurrent("cathodes",shot)
            print("success")
#            print("adding to wipal tree...")
#            wipal_tree = mds.Tree("wipal",shot)
#            wipal_tree.edit()
#            wipal_tree.addNode("discharge.cathodes","subtree")
#            wipal_tree.write()
#            wipal_tree.quit()
#            print("success")
        except mds.TreeException as e:
            print("Create cathodes pulse failed {0}".format(shot))
            sys.exit()
    else:
        print("cathodes shot {0} already exists".format(shot))


