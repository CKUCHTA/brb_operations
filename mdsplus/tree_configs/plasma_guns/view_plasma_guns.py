#!opt/anaconda2/bin/python
import MDSplus as mds
from modules.npconfigobj import NPConfigObj
import numpy as np
import matplotlib.pyplot as plt
cfgroot = "/home/WIPALdata/wipal_mdsplus/tree_configs/plasma_guns/configs/"
cfgspec = cfgroot+"plasma_guns_cfgspec.ini"
cfgfile = cfgroot+"plasma_guns.ini"
ps_cfgspec = cfgroot+"ps_ids_cfgspec.ini"
ps_cfgfile = cfgroot+"ps_ids.ini"

cfg = NPConfigObj(cfgfile,configspec=cfgspec)
ps_cfg = NPConfigObj(ps_cfgfile,configspec=ps_cfgspec)


shots = [46882]

for shot in shots:
    wt = mds.Tree("wipal",shot)
    locs = wt.getNode("\\guns_locs").data()
    r,theta = locs[:,0],locs[:,1]
    x,y = r*np.sin(theta), r*np.cos(theta)
    plt.plot(x,y,"o")
#    plt.plot(x[1],y[1],"ro")
    plt.show()

    fig,(ax0,ax1) = plt.subplots(2)
    for gun in ["gun_{0:02d}".format(i) for i in range(1,20)]:
        try:
            vnode = wt.getNode("discharge.plasma_guns.{0}.v_arc".format(gun)).data()
            inode = wt.getNode("discharge.plasma_guns.{0}.i_arc".format(gun)).data()
            print(gun)
            print("vnode mean: ", np.mean(vnode[0:2000]))
            print("inode mean: ", np.mean(inode[0:2000]))
            ax0.plot(vnode)
            ax1.plot(inode)
        except:
            pass
    plt.legend(range(1,20))
    plt.show()


