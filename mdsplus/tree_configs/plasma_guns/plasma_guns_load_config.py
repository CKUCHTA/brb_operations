#!/usr/bin/python3
import sys
import MDSplus as mds
import modules.mds_builders as mdsbuild
from modules.npconfigobj import NPConfigObj
import numpy as np
import logging


# Config file paths.
cfgroot = "/home/WIPALdata/brb_operations/mdsplus/tree_configs/plasma_guns/configs/"
cfgspec = cfgroot+"plasma_guns_cfgspec.ini"
cfgfile = cfgroot+"plasma_guns.ini"
ps_cfgspec = cfgroot+"ps_ids_cfgspec.ini"
ps_cfgfile = cfgroot+"ps_ids.ini"

def main(shot):
    logging.debug("Running 'plasma_guns_load_config.py' for shot #{}.".format(shot))
    logging.debug("Opening trees and nodes.")
    wipal_tree = mds.Tree("wipal",shot)
    top = wipal_tree.getNode("discharge.plasma_guns")
    gun_tree = mds.Tree("plasma_guns",shot)
    gun_map = {"arc_ps_id":(mds.String,None),"bias_ps_id":(mds.String,None),"loc":(mds.Float64Array,None)}
    pscur_map = {"gain":(mds.Float64,"A/V"), "offset":(mds.Float64,"V"), "raw":(mds.TreePath,None)}
    psvol_map = {"gain":(mds.Float64,"V/V"), "offset":(mds.Float64,"V"), "raw":(mds.TreePath,None)}

    logging.debug("Opening config files.")
    cfg = NPConfigObj(cfgfile,configspec=cfgspec)
    ps_cfg = NPConfigObj(ps_cfgfile,configspec=ps_cfgspec)

    # mdsbuild.unlock_tree(gun_tree) # Comment out this line when not reediting trees.
    for gun in cfg.sections:
        logging.debug("Making updates for gun {}.".format(gun))
        for name, (mdscls, units) in gun_map.items():
            node = top.getNode("{0}:{1}".format(gun,name))
            data = mdscls(cfg[gun][name])
            logging.debug("Inserting value ({}) for {} into {}.".format(data, name, gun))

            err = None
            try:
                err = mdscls(cfg[gun][name + "_sd"])
            except KeyError:
                pass

            try:
                mdsbuild.put_wrapper(node, data, err=err, units=units)
            except mds.TreeNOOVERWRITE:
                logging.error("Plasma gun tree can't be overwritten.")
                pass

        arc_key = cfg[gun]["arc_ps_id"]
        bias_key = cfg[gun]["bias_ps_id"]

        if arc_key != "":
            cfg_id = ps_cfg[arc_key]
            for sig in ["i_arc","v_arc"]:
                sig_node = top.getNode("{0}:{1}".format(gun,sig))
                if "i_" in sig:
                    sig_map = pscur_map
                    ps_section = "current"
                else:
                    sig_map = psvol_map
                    ps_section = "voltage"

                for name,(mdscls,units) in sig_map.items():
                    data = mdscls(cfg_id[ps_section][name])

                    err = None
                    try:
                        err = mdscls(cfg_id[ps_section][name+"_sd"])
                    except KeyError:
                        pass

                    try:
                        mdsbuild.put_wrapper(sig_node.getNode(name),data,err=err,units=units)
                    except mds.TreeNOOVERWRITE:
                        logging.error("Plasma gun tree can't be overwritten.")
                        pass
                    pass

        if bias_key != "":
            cfg_id = ps_cfg[bias_key]
            for sig in ["i_bias","v_bias"]:
                sig_node = top.getNode("{0}:{1}".format(gun,sig))
                if "i_" in sig:
                    sig_map = pscur_map
                    ps_section = "current"
                else:
                    sig_map = psvol_map
                    ps_section = "voltage"

                for name,(mdscls,units) in sig_map.items():
                    data = mdscls(cfg_id[ps_section][name])
                    err = None
                    try:
                        err = mdscls(cfg_id[ps_section][name+"_sd"])
                    except KeyError:
                        pass
                    try:
                        mdsbuild.put_wrapper(sig_node.getNode(name),data,err=err,units=units)
                    except mds.TreeNOOVERWRITE:
                        pass
                    pass

    print("plasma_guns_load_config success!")

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, filename='/home/WIPALdata/brb_operations/mdsplus/tree_configs/plasma_guns/plasma_guns_load_config.log', filemode='w')
    shot = int(sys.argv[1])
    main(shot)

