#!opt/anaconda2/bin/python
import MDSplus as mds
from modules.npconfigobj import NPConfigObj
import numpy as np
import matplotlib.pyplot as plt
cfgroot = "/home/WIPALdata/wipal_mdsplus/tree_configs/plasma_guns/configs/"
cfgspec = cfgroot+"plasma_guns_cfgspec.ini"
cfgfile = cfgroot+"plasma_guns.ini"
ps_cfgspec = cfgroot+"ps_ids_cfgspec.ini"
ps_cfgfile = cfgroot+"ps_ids.ini"

cfg = NPConfigObj(cfgfile,configspec=cfgspec)
ps_cfg = NPConfigObj(ps_cfgfile,configspec=ps_cfgspec)

check_locs = False
if check_locs:
    plt.ion()
    fig,ax = plt.subplots()
    ax.set_xlim(-.2,.2)
    ax.set_ylim(-.2,.2)
    ax.set_aspect(1)
    for gun in cfg.sections:
        print(gun)
        r,theta = cfg[gun]["loc"]
        theta -= np.deg2rad(6)
        cfg[gun]["loc"] = np.array([r,theta])
#        x,y = r*np.sin(theta), r*np.cos(theta)
#        ax.plot(x,y,"o")
#        plt.pause(1)

find_offsets = True
shots = range(33147,33149)
if find_offsets:
    for gun in cfg.sections:
        print(gun)
#        if gun != "gun_01":
#            continue
        arc_ps_id = cfg[gun]["arc_ps_id"]
        bias_ps_id = cfg[gun]["bias_ps_id"]
        if arc_ps_id != "":
            for sig in ps_cfg[arc_ps_id].sections:
                if sig == "current":
                    sig_str = "i"
                else:
                    sig_str = "v"
                print(sig_str)
                offset = ps_cfg[arc_ps_id][sig]["offset"]
                raw = ps_cfg[arc_ps_id][sig]["raw"]
                print(raw)
                raw_mean = np.zeros(len(shots))
                raw_std = np.zeros(len(shots))
                for i,shot in enumerate(shots):
                    print(shot)
                    wt = mds.Tree("wipal",shot)
                    raw_dat = wt.getNode("discharge.plasma_guns.{0}.{1}_arc:raw".format(gun,sig_str)).data()[0:400]
                    raw_mean[i] = np.mean(raw_dat)
                    raw_std[i] = np.std(raw_dat)
                new_off = np.mean(raw_mean)
                new_off_sd = np.sqrt(np.sum(raw_std**2))
                ps_cfg[arc_ps_id][sig]["offset"] = new_off
                ps_cfg[arc_ps_id][sig]["offset_sd"] = new_off_sd


        if bias_ps_id != "":
            for sig in ps_cfg[bias_ps_id].sections:
                if sig == "current":
                    sig_str = "i"
                else:
                    sig_str = "v"
                offset = ps_cfg[bias_ps_id][sig]["offset"]
                raw = ps_cfg[bias_ps_id][sig]["raw"]
                raw_mean = np.zeros(len(shots))
                raw_std = np.zeros(len(shots))
                for i,shot in enumerate(shots):
                    print(shot)
                    wt = mds.Tree("wipal",shot)
                    raw_dat = wt.getNode("discharge.plasma_guns.{0}.{1}_bias:raw".format(gun,sig_str)).data()[0:400]
                    raw_mean[i] = np.mean(raw_dat)
                    raw_std[i] = np.std(raw_dat)
                new_off = np.mean(raw_mean)
                new_off_sd = np.sqrt(np.sum(raw_std**2))
                ps_cfg[bias_ps_id][sig]["offset"] = new_off
                ps_cfg[bias_ps_id][sig]["offset_sd"] = new_off_sd


ps_cfg.mode = "edit"
ps_cfg.write()

                



