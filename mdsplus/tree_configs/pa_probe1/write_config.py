"""
Write/edit the config file used for the pa_probe1 MDSplus tree.
"""
import os.path
import logging
# Log to a file rewriting the file each time.
path_to_pa_probe1 = os.path.dirname(__file__)
logging.basicConfig(filename=os.path.join(path_to_pa_probe1, "pa_write_config.log"), filemode="w", level=logging.DEBUG)
import json
from collections import OrderedDict

# JSON data structure

# [
    # { (Start of data_to_write)
        # "start_shot": start_shot,
        # "act_area": {
            # "core": (area, "m^2"),
            # "shell": (area, "m^2")
        # },
        # "avg_index": {
            # "start": start_index,
            # "end": end_index
        # },
        # "bdot": {
            # "ax1": {
                # "gain": gain_factor,
                # "factor": factor,
                # "raw_cw": mds_channel
                # "raw_ccw": mds_channel
            # },
            # "ax2": {
                # "gain": gain_factor,
                # "factor": factor,
                # "raw_cw": mds_channel
                # "raw_ccw": mds_channel
            # },
            # "ax3": {
                # "gain": gain_factor,
                # "factor": factor,
                # "raw_cw": mds_channel
                # "raw_ccw": mds_channel
            # }
        # },
        # "noise_relay": {
            # "raw": mds_channel
        # },
        # "vbias": {
            # TODO: Add noise channel measurement and subtraction factor.
            # "raw": (mds_channel, "V"),
            # "offset": (offset_value, "V"),
            # "div": div_value
        # },
        # "tip01": {
            # "core": {
                # "signal": {
                    # "default": {
                        # "raw": (mds_channel, "V")
                    # }
                # },
                # "noise": {
                    # "default": {
                        # "raw": (mds_channel, "V")
                    # }
                # },
                # "rwire": (wire_resistance, "ohm"),
                # "relarea": (relative_area, "m^2")
            # },
            # "shell": {
                # "signal": {
                    # "default": {
                        # "raw": (mds_channel, "V")
                    # }
                # },
                # "noise": {
                    # "default": {
                        # "raw": (mds_channel, "V")
                    # }
                # },
                # "rwire": (wire_resistance, "ohm"),
                # "relarea": (relative_area, "m^2")
            # },
            # "rdiggnd": (digitizer_ground_resistance, "ohm"),
            # "rdiv": (divider_resistance, "ohm"),
            # "rsense": (sense_resistance, "ohm"),
            # "working": working_boolean,
            # "direction": {
                # "probe": {
                    # "theta": theta_value,
                    # "phi": phi_value
                # }
            # }
        # },
        # "tip02": {
            # ...
        # }
        # ...
    # } (End of data_to_write)
    # ...
# ]

def write_json(config_file_path, data_to_write):
    """
    Write some data to the config file. If the start_shot of the new
    data is the same as the start_shot of a pre-existing json section
    then edit the previous section instead of making a new one.

    params:
    config_file_path    = The path to the config file we should be writing in.
    data_to_write       = A dict that contains the data to write for one shot set.
    """
    # Check if config file already exists. If so, read it in. Otherwise create an empty list as the config.
    if os.path.exists(config_file_path):
        # Check if the start_shot of the new data is the same as a start shot already in the config.
        logging.debug("Reading old config file at '{path}'.".format(path=config_file_path))
        with open(config_file_path, 'r') as config_file:
            config = json.load(config_file)
    else:
        logging.warning("Could not find old config file at '{path}'. Creating new file.".format(path=config_file_path))
        config = []

    # Reorder the config so that when rewriting the config, all data is ordered.
    ordered_config = []
    logging.debug("Ordering all blocks in config.")
    for unordered_dict in config:
        ordered_config.append(order_dict(unordered_dict))
    config = ordered_config
    logging.debug("Finished ordering all blocks in config.")

    # Write a backup of the current config file to config_file_path.bak.
    logging.debug("Writing backup config to '{path}'.".format(path=config_file_path + ".bak"))
    with open(config_file_path + ".bak", 'w') as config_file_backup:
        json.dump(config, config_file_backup, indent=4) # Tab in the lines to make it pretty.
    
    # Choose the write mode whether an old block should be edited or a new block added.
    logging.debug("Checking if new data has same start shot as pre-existing data.")
    for block_index, data_block in enumerate(config):
        if data_to_write["start_shot"] == data_block["start_shot"]:
            logging.info("Found old config block with start_shot {shot_num} (index {i}). Will edit this block.".format(shot_num=data_to_write["start_shot"], i=block_index))

            logging.debug("Searching for differences between blocks...")
            search_for_differences(config[block_index], data_to_write)
            # Edit the block by replacing the old block with the new block.
            config[block_index] = data_to_write

            break
    else:
        logging.info("Did not find pre-existing start_shot {shot_num}. Will add new block.".format(shot_num=data_to_write["start_shot"]))
        config.append(data_to_write)

    logging.debug("Writing to config file.")
    with open(config_file_path, 'w') as config_file:
        json.dump(config, config_file, indent=4) # Tab in the lines to make it pretty.
    logging.info("Succesfully wrote new config data to '{path}'.".format(path=config_file_path))


def search_for_differences(dictionary_old : dict, dictionary_new : dict, log_prefix : str="Found difference:"):
    """
    Search through two dictionaries and log all differences between them.
    """
    dict_old_keys = dictionary_old.keys()
    dict_new_keys = dictionary_new.keys()
    if len(dict_old_keys & dict_new_keys) != len(dict_old_keys | dict_new_keys):
        # If the number of keys between ANDing and ORing the sets is different
        # then there must be at least one key in one that is not in the other.
        logging.debug(f"{log_prefix} Old dictionary had keys {dict_old_keys - dict_new_keys} that weren't in the new dictionary.")
        for key in dict_old_keys - dict_new_keys:
            logging.debug(f"{log_prefix} Old dictionary key '{key}': `{dict_old_keys[key]}`")
        logging.debug(f"{log_prefix} New dictionary had keys {dict_new_keys - dict_old_keys} that weren't in the old dictionary.")
        for key in dict_new_keys - dict_old_keys:
            logging.debug(f"{log_prefix} New dictionary key '{key}': `{dict_new_keys[key]}`")

    for key in dict_old_keys & dict_new_keys:
        if isinstance(dictionary_old[key], dict) & isinstance(dictionary_new[key], dict):
            # Call this function again with an updated prefex.
            search_for_differences(dictionary_old[key], dictionary_new[key], log_prefix=f"{log_prefix} '{key}':")
            continue

        if isinstance(dictionary_old[key], tuple):
            old_test_value = list(dictionary_old[key])
        else:
            old_test_value = dictionary_old[key]
        if isinstance(dictionary_new[key], tuple):
            new_test_value = list(dictionary_new[key])
        else:
            new_test_value = dictionary_new[key]
        
        if old_test_value != new_test_value:
            logging.debug(f"{log_prefix} Difference in '{key}': Old dictionary had value `{old_test_value}`, New dictionary had value `{new_test_value}`")


def order_dict(unordered_dict):
    """
    Change a dictionary object into an ordered dictionary object.
    """
    ordered_dict = OrderedDict()
    for key in sorted(list(unordered_dict.keys())):
        if isinstance(unordered_dict[key], dict):
            ordered_dict[key] = order_dict(unordered_dict[key])
        else:
            ordered_dict[key] = unordered_dict[key]

    return ordered_dict


if __name__ == "__main__":
    # Change this dictionary to add/edit the config.
    # If a 'None' value is set either as the element or as the first element of the tuple then nothing is written into the MDSplus table.
    # TODO: Figure out what the factors and gain factors should be for the bdot signal.
    # TODO: Figure out what the core and shell areas should be by comparing them against what the Te probe says.
    
    # 22000 is R1, 82 is R2, 500 is TREX cell internal resistance.
    # rdiggnd = (1 / 82 + 1 / 500)**-1
    rdiggnd = (1 / 680 + 1 / 1.6e6)**-1
    rdiv = rdiggnd + 56000
    # rsense = 1000
    rsense = 680

    digitizer_1 = 325
    digitizer_2 = 324

    trex_tag = lambda cell, card, channel: "\\c{cell}_{card}_{channel}".format(cell=cell, card=card, channel=str(channel).zfill(2))
    acq_tag = lambda acq_digitizer_number, channel: f"\\ta{str(acq_digitizer_number).zfill(3)}_{str(channel).zfill(2)}"
    noise_relay_channel = "\\wipal::top.raw.a373_direct:ch_51"
    data_to_write = { 
        "start_shot": 66615,
        "act_area": {
            "core": (1, "m^2"),
            "shell": (1, "m^2")
        },
        "avg_index": {
            "start": 100,
            "end": 1000
        },
        "bdot": {
            "ax1": { # Probe x direction
                "gain": 1,
                "factor": 1,
                "raw_cw": (acq_tag(digitizer_1, 20), "V"),
                "raw_ccw": (acq_tag(digitizer_1, 19), "V")
            },
            "ax2": { # Probe y direction
                "gain": 1,
                "factor": 0,
                "raw_cw": (acq_tag(digitizer_1, 18), "V"),
                "raw_ccw": (acq_tag(digitizer_1, 17), "V")
            },
            "ax3": { # Probe z direction
                "gain": 1,
                "factor": 1,
                "raw_cw": (acq_tag(digitizer_1, 22), "V"),
                "raw_ccw": (acq_tag(digitizer_1, 21), "V")
            }
        },
        "noise_relay": {
            "raw": noise_relay_channel,
            "limit": 0.2
        },
        "vbias": {
            "raw": (acq_tag(digitizer_1, 32), "V"),
            "offset": (-0.00174219, "V"),
            "div": -0.00193682
        },
        "tip01": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 4), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 3), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 2), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 1), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (45, "deg"),
                    "phi": (45, "deg")
                }
            }
        },
        "tip02": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 8), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 7), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 6), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 5), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (90, "deg"),
                    "phi": (45, "deg")
                }
            }
        },
        "tip03": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 12), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 11), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 10), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 9), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (135, "deg"),
                    "phi": (45, "deg")
                }
            }
        },
        "tip04": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 16), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 15), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 14), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_1, 13), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (45, "deg"),
                    "phi": (135, "deg")
                }
            }
        },
        "tip05": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 4), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 3), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 2), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 1), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (90, "deg"),
                    "phi": (135, "deg")
                }
            }
        },
        "tip06": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 8), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 7), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 6), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 5), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (135, "deg"),
                    "phi": (135, "deg")
                }
            }
        },
        "tip07": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 12), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 11), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 10), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 9), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (45, "deg"),
                    "phi": (225, "deg")
                }
            }
        },
        "tip08": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 16), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 15), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 14), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 13), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (90, "deg"),
                    "phi": (225, "deg")
                }
            }
        },
        "tip09": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 20), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 19), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 18), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 17), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (135, "deg"),
                    "phi": (225, "deg")
                }
            }
        },
        "tip10": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 24), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 23), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 22), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 21), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (45, "deg"),
                    "phi": (315, "deg")
                }
            }
        },
        "tip11": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 28), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 27), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 26), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 25), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (90, "deg"),
                    "phi": (315, "deg")
                }
            }
        },
        "tip12": {
            "core": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 32), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 31), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "shell": {
                "signal": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 30), "V")
                    }
                },
                "noise": {
                    "default": {
                        "raw": (acq_tag(digitizer_2, 29), "V")
                    }
                },
                "rwire": (0, "ohm"),
                "relarea": (1, "m^2")
            },
            "rdiggnd": (rdiggnd, "ohm"),
            "rdiv": (rdiv, "ohm"),
            "rsense": (rsense, "ohm"),
            "working": 1,
            "direction": {
                "probe": {
                    "theta": (135, "deg"),
                    "phi": (315, "deg")
                }
            }
        }
    }
    logging.debug("Reordering data to write for ease of reading.")
    ordered_data_to_write = order_dict(data_to_write)

    config_file_path = os.path.join(path_to_pa_probe1, 'configs', 'pa_probe1.json')

    write_json(config_file_path, ordered_data_to_write)
