#!/usr/bin/python3
import MDSplus as mds
import modules.mds_builders as mdsbuild
import numpy as np
import sys
import os
from modules.mysql_connection import get_engine
import pandas
import logging
import pandas.io.sql as pio
from modules.npconfigobj import NPConfigObj
cfgroot = "/home/WIPALdata/brb_operations/mdsplus/tree_configs/linear_hall3/configs/"
cfgspec = cfgroot+"linear_hall3_cfgspec.ini"
cfgfile = cfgroot+"linear_hall3.ini"

def rebin(data, pts=100, axis=0, data_sd=None):
    """
    Rebin the data into a new shape and pad with NaN.

    params:
    data        = The data to reshape.
    pts         = The size of the axis to change.
    axis        = The axis to rebin.
    data_sd     = The standard deviation of the data.
    """
    inshape = data.shape
    # Get the number of points of one axis of the data.
    axpts = inshape[axis]
    # Find how much we need to pad.
    m, pad = divmod(axpts, pts)

    # If we need to pad the data, do so. Otherwise ignore.
    if pad != 0:
        m += 1
        pad = m * pts % axpts
        # Set the widths to pad each axis.
        pad_width = tuple([(0, pad) if i == axis else (0, 0) for i in range(len(inshape))])
        # Pad the data with NaN values.
        output = np.pad(data, pad_width, "constant", constant_values=np.nan)
        if data_sd is not None:
            # Pad the standard deviation as well.
            output_sd = np.pad(data_sd, pad_width, "constant", constant_values=np.nan)
    else:
        output = data
        if data_sd is not None:
            output_sd = data_sd

    # Construct the shape of the new data.
    outshape = list(inshape)
    outshape[axis] = pts
    outshape.insert(axis+1,m)
    outshape = tuple(outshape)
    output = output.reshape(outshape)

    # Return the data where the last axis is averaged over.
    if data_sd is not None:
        output_sd = output_sd.reshape(outshape)
        return np.nanmean(output,axis=axis+1),np.sqrt(np.nansum(output_sd**2,axis=axis+1))
    else:
        return np.nanmean(output,axis=axis+1),np.nanstd(output,axis=axis+1)

def sql_log(top_node, shot):
    """
    Get MySQL table for linear hall array 3 and load the values into the MDSplus tree. The values are:
    R_PIVOT, THETA_PIVOT, PHI_PIVOT, R0, DELTA_THETA, PS_VOLTAGE
    """
    sql_config_path="/home/WIPALdata/sql_config.ini"
    engine = get_engine(sql_config_path)
    database = engine.url.database
    # Connect and query the database.
    with engine.connect() as connection:
        sql_query = 'select * from {0}.linear_hall3 where shot_num = {1:d}'.format(database, shot)
        try:
            df = pandas.read_sql(sql_query, connection)
        except Exception:
            logging.exception("Failure executing query '{query}'.".format(query=sql_query))
            raise

    # If the status of the linear hall 3 is off then raise an exception.
    if df.status[0] == "off":
        raise Exception("MySQL table {0}.linear_hall3 status is 'off' for shot {1}".format(database, shot))
    
    # Get the references to the nodes.
    r_pivot, theta_pivot, phi_pivot, r0, delta_theta,ps_voltage = mdsbuild.get_node_refs(top_node,
            "r_pivot","theta_pivot","phi_pivot","r0","delta_theta","ps_voltage")

    # Put the MySQL data into the MDSplus tree.
    # R_PIVOT
    rpiv, rpiv_sd = mds.Float64(df.r_pivot[0]), mds.Float64(0.003)
    mdsbuild.put_wrapper(r_pivot, rpiv, err=rpiv_sd, units="m")
    # THETA_PIVOT
    tpiv, tpiv_sd = mds.Float64(np.deg2rad(df.theta_pivot[0])), mds.Float64(0)
    mdsbuild.put_wrapper(theta_pivot, tpiv, err=tpiv_sd, units="rad")
    # PHI_PIVOT
    ppiv, ppiv_sd = mds.Float64(np.deg2rad(df.phi_pivot[0])), mds.Float64(0)
    mdsbuild.put_wrapper(phi_pivot, ppiv, err=ppiv_sd, units="rad")
    # R0
    r0dat, r0dat_sd = mds.Float64(df.r0[0]), mds.Float64(0.002)
    mdsbuild.put_wrapper(r0, r0dat, err=r0dat_sd, units="m")
    # DELTA_THETA
    dtheta, dtheta_sd = mds.Float64(np.deg2rad(df.delta_theta[0])), mds.Float64(np.deg2rad(.2))
    mdsbuild.put_wrapper(delta_theta, dtheta, err=dtheta_sd, units="rad")
    #PS_VOLTAGE
    try:
        ps_voltage.putData(mds.TreePath("\\TOP.RAW.{0}".format(df.ps_raw_node[0].upper())))
    except mds.TreeNOOVERWRITE:
        logging.warning("Cannot overwrite tree while trying to put PS_VOLTAGE for linear_hall3...")
        pass


def process_data(top_node,shot):
    cfg = NPConfigObj(cfgfile,configspec=cfgspec,val_on_init=True)
    logging.info("Calculating transformation matrix RFU...")
    for probe in cfg.sections:
        # Read in "unskew" matrix U and error for each set of 3 axis probe units
        u = cfg[probe]["unskew"]
        u_sd = cfg[probe]["unskew_sd"]
        # Create "flip" matrix f to flip axis 3 to RH coordinate system
        f = np.array([[1,0,0],[0,1,0],[0,0,-1]],dtype="float64")
        # Build rotation matrix r and error to transform to machine frame
        dtheta = top_node.getNode("delta_theta").getData()
        theta_pivot = top_node.getNode("theta_pivot").getData()
        beta = dtheta.data() + theta_pivot.data()
        beta_sd = np.sqrt(dtheta.getError().data()**2 + theta_pivot.getError().data()**2)
        r = np.array([[np.cos(beta),0,np.sin(beta)],[0,1,0],[-np.sin(beta),0,np.cos(beta)]],dtype="float64")
        ####### rotation correction for Jan and Doug's stuff
        alpha = -39.69*np.pi/180
        r1 = np.array([[np.cos(alpha),-np.sin(alpha),0],[np.sin(alpha),np.cos(alpha),0],[0,0,1]],dtype="float64")
        r = np.dot(r1,r)
        #################################################
        r_sd = np.sqrt(np.array([[np.sin(beta)**2,0,np.cos(beta)**2],[0,0,0],[np.cos(beta)**2,0,np.sin(beta)**2]],dtype="float64"))*beta_sd
        # Build matrix RFU with associated error to store in MDSplus
        rfu = r.dot(f.dot(u))
        rfu_sd = mds.Float64Array((r.dot(f.dot(u_sd.dot(f.T)))).dot(r.T) + r_sd)
        rfu = mds.Float64Array(rfu)
        mdsbuild.put_wrapper(top_node.getNode("{0}:rfu".format(probe)),rfu,err=rfu_sd)

    # # Start processing to populate binned bx,by,bz
    # pts = 1000
    # logging.info("Binning top level data down to {0} points...".format(pts))
    # bx = top_node.getNode("\\linear_hall3_bx").getData()
    # by = top_node.getNode("\\linear_hall3_by").getData()
    # bz = top_node.getNode("\\linear_hall3_bz").getData()
    # bx_arr,bx_sd_arr = rebin(bx.data(),pts=pts,axis=1,data_sd=bx.getError().data())
    # by_arr,by_sd_arr = rebin(by.data(),pts=pts,axis=1,data_sd=by.getError().data())
    # bz_arr,bz_sd_arr = rebin(bz.data(),pts=pts,axis=1,data_sd=bz.getError().data())
    # time, _ = rebin(bx.dim_of().data(), pts=pts)
    # logging.info("binning complete")

    # logging.info("uploading to tree...")
    # bxsig = mds.Signal(bx_arr,None,time)
    # bxsig_sd = mds.Signal(bx_sd_arr,None,time)
    # bysig = mds.Signal(by_arr,None,time)
    # bysig_sd = mds.Signal(by_sd_arr,None,time)
    # bzsig = mds.Signal(bz_arr,None,time)
    # bzsig_sd = mds.Signal(bz_sd_arr,None,time)

    # mdsbuild.put_wrapper(top_node.getNode("\\linear_hall3_bx_binned"),bxsig,err=bxsig_sd,units="T")
    # mdsbuild.put_wrapper(top_node.getNode("\\linear_hall3_by_binned"),bysig,err=bysig_sd,units="T")
    # mdsbuild.put_wrapper(top_node.getNode("\\linear_hall3_bz_binned"),bzsig,err=bzsig_sd,units="T")
    logging.info("linear_hall3_run_proc shot {0:d} success!".format(shot))


def main(shot):
    wipal_tree = mds.Tree("wipal",shot)
    hall_top = wipal_tree.getNode("magnetics.linear_hall3")
    # mdsbuild.unlock_tree(mds.Tree("linear_hall3",shot))
    try:
        sql_log(hall_top, shot)
    except:
        logging.warning("Not processing linear_hall3 data for shot {0:d}, set top node to off".format(shot))
        hall_top.setOn(False)
        #raise
    else:
        process_data(hall_top, shot)
        ht = mds.Tree("linear_hall3",shot)
        ht.cleanDatafile()
        # mdsbuild.make_tree_write_once(mds.Tree("linear_hall3",shot))
        # try:
            # process_data(hall_top,shot)
        # except:
            # logging.error("error processing shot {0}".format(shot))

if __name__ == "__main__":
    shot = int(sys.argv[1])

    # Log to a file rewriting the file each time.
    log_file_path = os.path.join(os.path.realpath(os.path.dirname(__file__)), "linear_hall3_run_proc.log")
    logging.basicConfig(filename=log_file_path, filemode="w", level=logging.DEBUG)

    main(shot)

