import numpy as np
import MDSplus as mds
import matplotlib.pyplot as plt
import cProfile

def rebin(data,pts=100,axis=0,data_sd=None):
    inshape = data.shape
    axpts = inshape[axis]
    m,pad = divmod(axpts,pts)
    if pad != 0:
        m += 1
        pad = m*pts % axpts
        pad_width = tuple([(0,pad) if i == axis else (0,0) for i in range(len(inshape))])
        output = np.pad(data,pad_width,"constant",constant_values = np.nan)
        if data_sd is not None:
            output_sd = np.pad(data_sd,pad_width,"constant",constant_values = np.nan)
    else:
        output = data
        if data_sd is not None:
            output_sd = data_sd
    print(m,pad)
    outshape = list(inshape)
    outshape[axis] = pts
    outshape.insert(axis+1,m)
    outshape = tuple(outshape)
    output = output.reshape(outshape)
    if data_sd is not None:
        output_sd = output_sd.reshape(outshape)
        return np.nanmean(output,axis=axis+1),np.sqrt(np.nansum(output_sd**2,axis=axis+1))
    else:
        return np.nanmean(output,axis=axis+1),np.nanstd(output,axis=axis+1)


#npts = 5000932
#npts = 5000000
##npts = 50005
#funcs = np.zeros((15,npts))
#t = np.linspace(0,2*np.pi,npts)
#for i in range(15):
#    funcs[i,:] = i + np.sin((i+1)*t + 2*np.random.rand(1)*np.pi)+.1*np.random.rand(npts)
#
#print("starting binning")
#time,time_sd = rebin(t,pts=1000)
#f_bin,f_bin_sd = rebin(funcs,pts=1000,axis=1,data_sd=.01*funcs)
#print("done binning")
#
#plt.plot(t,funcs.T)
#plt.plot(time,f_bin.T)
#plt.show()

shot = 25584

def wrapper(func,*args,**kwargs):
    def wrapped():
        return func(*args,**kwargs)
    return wrapped

def test1(shot):
    t = mds.Tree("wipal",shot)
    node = t.getNode("\\linear_hall3_bx").getData()
    bx,bx_sd = rebin(node.data(),pts=4000,axis=1,data_sd=node.getError().data())
    node = t.getNode("\\linear_hall3_by").getData()
    by,by_sd = rebin(node.data(),pts=4000,axis=1,data_sd=node.getError().data())
    node = t.getNode("\\linear_hall3_bz").getData()
    bz,bz_sd = rebin(node.data(),pts=4000,axis=1,data_sd=node.getError().data())

def test2(shot):
    bx,bx_sd = np.zeros((15,400000)),np.zeros((15,400000))
    by,by_sd = np.zeros((15,400000)),np.zeros((15,400000))
    bz,bz_sd = np.zeros((15,400000)),np.zeros((15,400000))
    t = mds.Tree("wipal",shot)
    for i,tag in enumerate(["hp_{0:02d}".format(j) for j in range(1,16)]):
        node = t.getNode("\\linear_hall3_{0}_bx".format(tag)).getData()
        bx[i,:] = node.data()
        bx_sd[i,:] = node.getError().data()
        node = t.getNode("\\linear_hall3_{0}_by".format(tag)).getData()
        by[i,:] = node.data()
        by_sd[i,:] = node.getError().data()
        node = t.getNode("\\linear_hall3_{0}_bz".format(tag)).getData()
        bz[i,:] = node.data()
        bz_sd[i,:] = node.getError().data()
    bx,bx_sd = rebin(bx,pts=4000,axis=1,data_sd=bx_sd)
    by,by_sd = rebin(by,pts=4000,axis=1,data_sd=by_sd)
    bz,bz_sd = rebin(bz,pts=4000,axis=1,data_sd=bz_sd)

test1 = wrapper(test1,shot)
test2 = wrapper(test2,shot)

cProfile.run("test1()")
cProfile.run("test2()")
