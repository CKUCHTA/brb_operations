from modules.npconfigobj import NPConfigObj
import MDSplus as mds
import modules.mds_builders as mdsbuild
import numpy as np
import copy

cfgroot = "/home/WIPALdata/wipal_mdsplus/tree_configs/linear_hall3/configs/"
cfgspec = cfgroot+"linear_hall3_cfgspec.ini"
cfgfile = cfgroot+"linear_hall3.ini"
cfg = NPConfigObj(cfgfile,configspec=cfgspec,val_on_init=True,mode="edit")
cfgbak = NPConfigObj(cfgfile+".bak",configspec=cfgspec,val_on_init=True)
offsets = np.load("new_offsets3.npy")
offsets_sd = 0.2*np.abs(offsets)
# unskewdat = np.load("unskew11unskew13.npy")
# unskew_sddat = .2*np.abs(unskewdat)
# unskew = np.eye(3)
# unskew_sd = np.zeros_like(unskew)
print(offsets)

for i, probe in enumerate(cfg.sections):
    cfg[probe]["axis_1"]["offset"] = offsets[i,0]
    cfg[probe]["axis_1"]["offset_sd"] = offsets_sd[i,0]
    cfg[probe]["axis_2"]["offset"] = cfgbak[probe]["axis_2"]["offset"]
    cfg[probe]["axis_2"]["offset_sd"] = cfgbak[probe]["axis_2"]["offset_sd"]
    cfg[probe]["axis_3"]["offset"] = offsets[i,2]
    cfg[probe]["axis_3"]["offset_sd"] = offsets_sd[i,2]
    # unskew_tmp = copy.copy(unskew)
    # unskew_sd_tmp = copy.copy(unskew_sd)
    # unskew_tmp[1,0] = unskewdat[i,0]
    # unskew_tmp[1,2] = unskewdat[i,1]
    # unskew_sd_tmp[1,0] = unskew_sddat[i,0]
    # unskew_sd_tmp[1,2] = unskew_sddat[i,1]
    print(i)
    # print unskew_tmp
    # print unskew_sd_tmp
    # cfg[probe]["unskew"] = unskew_tmp
    # cfg[probe]["unskew_sd"] = unskew_sd_tmp

cfg.write()

#shots = range(26087,26103)
#for shot in shots:
#    t = mds.Tree("linear_hall3",shot)
#    for i,probe in enumerate(cfg.sections):
#        print probe
#        for ax in ["axis_{0:d}".format(i) for i in range(1,4)]:
#            print ax
#            off = cfg[probe][ax]["offset"]
#            off_sd = cfg[probe][ax]["offset_sd"]
#            mdsbuild.put_wrapper(t.getNode("{0}.{1}:offset".format(probe,ax)),mds.Float64(off),err=mds.Float64(off_sd),units="V")
#
