#!/usr/bin/python3
import sys
import MDSplus as mds
import modules.mds_builders as mdsbuild
from modules.npconfigobj import NPConfigObj
import numpy as np
cfgroot = "/home/WIPALdata/brb_operations/mdsplus/tree_configs/linear_hall3/configs/"
cfgspec = cfgroot+"linear_hall3_cfgspec.ini"
cfgfile = cfgroot+"linear_hall3.ini"

def main(shot):
    """
    Write the config values to the MDSplus tree.
    """
    wipal_tree = mds.Tree("wipal", shot)
    top = wipal_tree.getNode("magnetics.linear_hall3")
    axis_map = {"chip_model":(mds.String,None),"gain":(mds.Float64,"T/V"),"offset":(mds.Float64,"V"),
            "raw":(mds.TreePath,None)}

    cfg = NPConfigObj(cfgfile,configspec=cfgspec,val_on_init=True)
    for idx, probe in enumerate(cfg.sections):
        print(probe)
        for axis in ["axis_1","axis_2","axis_3"]:
            print(axis)
            for name, (mdscls,units) in axis_map.items():
                print(name)
                node = top.getNode("{0}.{1}:{2}".format(probe,axis,name))
                data = mdscls(cfg[probe][axis][name])
                err = None
                try:
                    err = mdscls(cfg[probe][axis][name+"_sd"])
                except KeyError:
                    pass
                try:
                    mdsbuild.put_wrapper(node,data,err=err,units=units)
                    print("Wrote {0} node for shot {1}".format(node.getFullPath(),shot))
                except mds.TreeNOOVERWRITE:
                    pass

    print("linear_hall3_load_config shot {0:d} success!".format(shot))

if __name__ == "__main__":
    shot = int(sys.argv[1])
    main(shot)

