import MDSplus as mds
from subprocess import call
import time
from linear_hall3_load_config import main


#shots = range(26161,26195) # Bvac_checking_HH file
#shots = range(26031,26047) # dipole_alignment4.py file
#shots = range(26087,26103) # dipole_alignment5.py file
#shots = range(26788,26833) # flux_surfaces[9,10].py files
#shots = range(26315,26378) # flux_surfaces[7,7new].py files
#shots = range(26786,38550) # after nonlinear minimization fit for offsets
#shots = range(26087,26786) # after nonlinear minimization fit for offsets

shots = range(33787,44727)

for shot in shots:
    print(shot)
    try:
        main(shot)
    except:
        print("Failed to process shot: {0}".format(shot))
