import MDSplus as mds
from subprocess import call
import time

#shots = range(26161,26195) # Bvac_checking_HH file
#shots = range(26031,26047) # dipole_alignment4.py file
#shots = range(26087,26103) # dipole_alignment5.py file
#shots = range(26788,26833) # flux_surfaces[9,10].py files
#shots = range(26315,26378) # flux_surfaces[7,7new].py files
#shots = range(30647,31006) # new shots
for shot in range(26031,31006):
    print(shot)
    t = mds.Tree("linear_hall3",shot)
    t.cleanDatafile()
