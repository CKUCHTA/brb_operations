import MDSplus as mds
from subprocess import call
import time
from quad_mach_nt_run_proc import main

#shots = range(25767,26990)
shots = range(26990,27990)
for shot in shots:
    print(shot)
    try:
        main(shot)
    except:
        print("Failed to process shot: {0}".format(shot))
        continue
