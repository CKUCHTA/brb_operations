#!/opt/anaconda2/bin/python
import sys
import os
import glob
import MDSplus as mds
import modules.mds_builders as mdsbuild
from modules.npconfigobj import NPConfigObj
import numpy as np

def choose_config(cfg_root,treename,shot):
    # get all config files 
    cfglist = glob.glob(os.path.join(cfg_root,'{0}_[0-9]*.ini'.format(treename)))
    cfg_files = [c.split("/")[-1] for c in cfglist]
    cfg_ret = None
    for cfg in cfg_files:
        shotrange = cfg.split(".")[0].split("_")[-1]
        start,end = shotrange.split("-")
        start = int(start)
        if end == "present":
            if start <= shot:
                cfg_ret = cfg
            else:
                continue
        else:
            end = int(end)
            if start <= shot <= end:
                cfg_ret = cfg
            else:
                continue
    if cfg_ret is None:
        raise ValueError("invalid shot number for quad_mach_nt: {0:d}".format(shot)) 
    else:
        return os.path.join(cfg_root,cfg_ret)

def main(shot,overwrite=False):
    # Get configuration file paths and load config file object
    treename = "quad_mach_nt"
    cfgroot = "/home/WIPALdata/wipal_mdsplus/tree_configs/{0}/configs".format(treename)
    cfgspec = os.path.join(cfgroot,"{0}_cfgspec.ini".format(treename))
    cfgfile = choose_config(cfgroot,treename,shot)
    print("Loading config for shot {0:d} ...".format(shot))
    cfg = NPConfigObj(cfgfile,configspec=cfgspec,val_on_init=True)
    print("config file: " + cfg.filename)

    # Open tree for loading data
    qmnt_tree = mds.Tree("quad_mach_nt",shot)
    top = qmnt_tree.getNode("\\top")
    if overwrite:
        mdsbuild.set_write_once(top,is_write_once=False)

    # Get shortcut names for sections in config object
    mps = cfg["mps"]
    tps = cfg["tps"]
    xtip = cfg["xtip"]
    xtip_t5 = cfg["xtip_t5"]

    # Build MDSplus (types,units) maps for each node that gets written 
    mp_map = {"raw":(mds.TreePath,None),"raw_offset":(mds.Float64,"V"),
            "resistor":(mds.Float64,"Ohm"),"vbatt":(mds.Float64,"V"),
            "area":(mds.Float64,"m^2"),"calfac":(mds.Float64,None)}

    deltav_map = {"raw":(mds.TreePath,None),"raw_offset":(mds.Float64,"V"),
            "r2":(mds.Float64,"Ohm"),"r3":(mds.Float64,"Ohm")}

    isat_map = {"raw":(mds.TreePath,None),"raw_offset":(mds.Float64,"V"),
            "r1":(mds.Float64,"Ohm"),"area":(mds.Float64,"m^2"),"vbatt":(mds.Float64,"V"),
            "calfac":(mds.Float64,None)}

    vfloat_map = {"raw":(mds.TreePath,None),"raw_offset":(mds.Float64,"V"),
            "r4":(mds.Float64,"Ohm"),"r5":(mds.Float64,"Ohm"),"area":(mds.Float64,"m^2")}

    tp_map = {"deltav":deltav_map,"isat":isat_map,"vfloat":vfloat_map}


    xtip_map = {"i":{"raw":(mds.TreePath,None),"raw_offset":(mds.Float64,"V"),"rsweep":(mds.Float64,"Ohm"),}, 
        "v":{"raw":(mds.TreePath,None),"raw_offset":(mds.Float64,"V"),"gain":(mds.Float64,"V/V"),},
        "area":(mds.Float64,"m^2")}

    xtip_t5_map = {"i":{"raw":(mds.TreePath,None),"raw_offset":(mds.Float64,"V"),"rsweep":(mds.Float64,"Ohm"),}, 
        "v":{"raw":(mds.TreePath,None),"raw_offset":(mds.Float64,"V"),"gain":(mds.Float64,"V/V"),},
        "area":(mds.Float64,"m^2")}

    # Load Mach Probe sections 
    for mp in mps.sections:
        for side in mps[mp].sections:
            for name,(mdscls,units) in mp_map.items():
                node = top.getNode("{0}.{1}:{2}".format(mp,side,name))
                data = mdscls(mps[mp][side][name])
                err = None
                try:
                    err = mdscls(mps[mp][side][name+"_sd"])
                except KeyError:
                    pass
                try:
                    mdsbuild.put_wrapper(node,data,err=err,units=units)
                    #print("Wrote {0} node for shot {1}".format(node.getFullPath(),shot) )
                except mds.TreeNOOVERWRITE:
                    pass
        # load mach offset for each mach probe
        node = top.getNode("{0}:mach_offset".format(mp))
        data,err = mds.Float64(mps[mp]["mach_offset"]), mds.Float64(mps[mp]["mach_offset_sd"])
        mdsbuild.put_wrapper(node,data,err=err,units=None)

    # Load Triple Probe sections
    for tp in tps.sections:
        for meas in ["deltav","isat","vfloat"]:
            for name,(mdscls,units) in tp_map[meas].items():
                node = top.getNode("{0}.{1}:{2}".format(tp,meas,name))
                data = mdscls(tps[tp][meas][name])
                err = None
                try:
                    err = mdscls(tps[tp][meas][name+"_sd"])
                except KeyError:
                    pass
                try:
                    mdsbuild.put_wrapper(node,data,err=err,units=units)
                    #print("Wrote {0} node for shot {1}".format(node.getFullPath(),shot) )
                except mds.TreeNOOVERWRITE:
                    pass

    # Load extra tip section
    for sig in xtip.sections:
        for name,(mdscls,units) in xtip_map[sig].items():
            node = top.getNode("xtip.{0}:{1}".format(sig,name))
            data = mdscls(xtip[sig][name])
            err = None
            try:
                err = mdscls(xtip[sig][name+"_sd"])
            except KeyError:
                pass
            try:
                mdsbuild.put_wrapper(node,data,err=err,units=units)
                #print("Wrote {0} node for shot {1}".format(node.getFullPath(),shot) )
            except mds.TreeNOOVERWRITE:
                pass
        # load xtip area
        node = top.getNode("xtip:area")
        data,err = mds.Float64(xtip["area"]), mds.Float64(xtip["area_sd"])
        mdsbuild.put_wrapper(node,data,err=err,units="m^2")

    # Load extra xtip_t5 section
    for sig in xtip_t5.sections:
        for name,(mdscls,units) in xtip_t5_map[sig].items():
            node = top.getNode("xtip_t5.{0}:{1}".format(sig,name))
            data = mdscls(xtip_t5[sig][name])
            err = None
            try:
                err = mdscls(xtip_t5[sig][name+"_sd"])
            except KeyError:
                pass
            try:
                mdsbuild.put_wrapper(node,data,err=err,units=units)
                #print("Wrote {0} node for shot {1}".format(node.getFullPath(),shot) )
            except mds.TreeNOOVERWRITE:
                pass
        # load xtip_t5 area
        node = top.getNode("xtip_t5:area")
        data,err = mds.Float64(xtip_t5["area"]), mds.Float64(xtip_t5["area_sd"])
        mdsbuild.put_wrapper(node,data,err=err,units="m^2")

    qmnt_tree.cleanDatafile()
    print("Loading config complete!")

if __name__ == "__main__":
    shot = int(sys.argv[1])
    main(shot)

