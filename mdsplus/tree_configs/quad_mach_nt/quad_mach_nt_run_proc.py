#!/opt/anaconda2/bin/python
import MDSplus as mds
import modules.mds_builders as mdsbuild
import numpy as np
import sys
from sqltools.core import start_cnx, close_cnx
from modules.mysql_connection import get_engine
import pandas
from analysis.datahelpers import smooth
from scipy.interpolate import interp1d
import pandas.io.sql as pio
import matplotlib.pyplot as plt

def sql_log(top_node,shot):
    # get MySQL table for quad_mach_nt tree
    # load following top level values into quad_mach_nt tree
    # R_PIVOT, THETA_PIVOT, PHI_PIVOT, R0, DELTA_THETA, ODDATOB_VEC, EVENATOB_VEC
    mass_dict = {"Ar":40.0,"He":4.0,"H2":1.0,"D":2.0}
    cnx = start_cnx(filename='/home/WIPALdata/sql_config.ini')
    sql_config_path = '/home/WIPALdata/sql_config.ini'
    engine = get_engine(sql_config_path)
    database = engine.url.database
    with engine.connect() as connection:
        df_sql_query = 'select * from {0}.quad_mach_nt where shot_num = {1:d}'.format(database, shot)
        dfb_sql_query = 'select * from {0}.quad_mach_basic where shot_num = {1:d}'.format(database, shot)
        try:
            df = pandas.read_sql(df_sql_query, connection)
            dfb = pandas.read_sql(dfb_sql_query, connection)
        except:
            raise ValueError("We weren't able to connect to one of the sql databases")
        else:
            if df.status[0] == "off":
                raise IOError("MySQL table {0}.quad_mach_nt status is 'off' for shot {1}".format(database, shot))
            else:
                gas = dfb.shot_gastype[0]
                mu_i = mass_dict[gas]
                r_pivot, theta_pivot, phi_pivot, r0, delta_theta,oddatob_vec,evenatob_vec = mdsbuild.get_node_refs(top_node,
                        "r_pivot","theta_pivot","phi_pivot","r0","delta_theta","oddatob_vec","evenatob_vec")
                # R_PIVOT
                rpiv,rpiv_sd = mds.Float64(df.r_pivot[0]), mds.Float64(0.003)
                mdsbuild.put_wrapper(r_pivot,rpiv,err=rpiv_sd,units="m")
                # THETA_PIVOT
                tpiv, tpiv_sd = mds.Float64(np.deg2rad(df.theta_pivot[0])), mds.Float64(0)
                mdsbuild.put_wrapper(theta_pivot,tpiv,err=tpiv_sd,units="rad")
                # PHI_PIVOT
                ppiv, ppiv_sd = mds.Float64(np.deg2rad(df.phi_pivot[0])), mds.Float64(0)
                mdsbuild.put_wrapper(phi_pivot,ppiv,err=ppiv_sd,units="rad")
                # R0
                r0dat,r0dat_sd = mds.Float64(df.r0[0]), mds.Float64(0.002)
                mdsbuild.put_wrapper(r0,r0dat,err=r0dat_sd,units="m")
                # DELTA_THETA
                dtheta, dtheta_sd = mds.Float64(np.deg2rad(df.delta_theta[0])), mds.Float64(np.deg2rad(.2))
                mdsbuild.put_wrapper(delta_theta,dtheta,err=dtheta_sd,units="rad")

                # helper functions for getting velocity unit vectors for mach probe faces
                # r_hat x oddatob_vec = evenatob_vec
                def odd_rot(dt):
                    c = np.cos(dt)
                    s = np.sin(dt)
                    return np.array([[c,0,s],[0,1,0],[-s,0,c]])

                def even_rot(tpiv,dt):
                    c1 = np.cos(tpiv)
                    s1 = np.sin(tpiv)
                    c2 = np.cos(tpiv+dt)
                    s2 = np.sin(tpiv+dt)
                    return np.array([[s2*s1, -c2, c1*s2],[c1,0,-s1],[s1*c2, s2, c1*c2]])

                def get_nvecs(tpiv,dt,m1_atob_dir):
                    if m1_atob_dir == "south":
                        nvec = odd_rot(tpiv).dot(np.array([1,0,0]))
                    elif m1_atob_dir == "north":
                        nvec = odd_rot(tpiv).dot(np.array([-1,0,0]))
                    elif m1_atob_dir == "east":
                        nvec = np.array([0,1,0])
                    elif m1_atob_dir == "west":
                        nvec =np.array([0,-1,0])
                    else:
                        raise ValueError("unsupported direction orientation")

                    odd_nvecs = odd_rot(dt).dot(nvec)
                    even_nvecs = even_rot(tpiv,dt).dot(nvec)
                    return odd_nvecs, even_nvecs

                # get flow unit vectors and put into mdsplus tree
                oddatob,evenatob = get_nvecs(np.deg2rad(df.theta_pivot[0]),np.deg2rad(df.delta_theta[0]),df.mp1_atob[0].lower())
                mdsbuild.put_wrapper(oddatob_vec,mds.Float64Array(oddatob),err=mds.Float64Array([0,0,0]))
                mdsbuild.put_wrapper(evenatob_vec,mds.Float64Array(evenatob),err=mds.Float64Array([0,0,0]))

                # load ti,mu,z,gamma_e,gamma_i for now, some of these should be read from mysql...
                mdsbuild.put_wrapper(top_node.getNode("gamma_e"),mds.Float64(1.0))
                mdsbuild.put_wrapper(top_node.getNode("gamma_i"),mds.Float64(1.0))
                mdsbuild.put_wrapper(top_node.getNode("mu_i"),mds.Float64(mu_i))
                if mu_i == 4:
                    mdsbuild.put_wrapper(top_node.getNode("ti"),mds.Float64(0.5),err=mds.Float64(0.25),units="eV")
                elif mu_i == 40:
                    mdsbuild.put_wrapper(top_node.getNode("ti"),mds.Float64(1.5),err=mds.Float64(0.25),units="eV")
                else:
                    mdsbuild.put_wrapper(top_node.getNode("ti"),mds.Float64(0.0),err=mds.Float64(0.0),units="eV")
                mdsbuild.put_wrapper(top_node.getNode("z"),mds.Float64(1.0),err=mds.Float64(0.1))


def process_data(top_node,shot):
    # Remaining work to be done in this function...
    #   handling when tp_2 is replaced by xtip_t5 (and adjusting wij)
    #   langmuir sweep probe fitting
    #   interpolation of sweep parameters onto triple probe time base

    # Variables that can be changed to affect processing output
    wp = 1 # inverse distance weighting power
    smooth_pts = 200 # number of points for smoothing window for cs data

    # acquire relative position vectors for interpolation of cs from 
    # sweep and triple probes to mach probe locations
    cs_loc_vec = top_node.getNode("nt_locs").data()
    mp_loc_vec = top_node.getNode("v_locs").data()
    for i,probe in enumerate(["xtip","tp_1","tp_2","tp_3","tp_4"]):
        ploc = top_node.getNode("{0}:loc".format(probe)).data()
        cs_loc_vec[i,:] = ploc
        if i == 1:
            time = top_node.getNode("{0}:cs".format(probe)).dim_of().data()
    cs_dvec = np.sqrt(np.sum((cs_loc_vec - cs_loc_vec[0,:])**2,axis=-1))
    mp_dvec = np.sqrt(np.sum((mp_loc_vec - cs_loc_vec[0,:])**2,axis=-1))


    # build cs matrix by smoothing cs from triple probes
    for i,probe in enumerate(["tp_1","tp_2","tp_3","tp_4"]):
        csnode = top_node.getNode("{0}:cs".format(probe))
        cstmp, cstmp_sd = csnode.data(), csnode.getError().data()
        if i == 0:
            time = csnode.dim_of().data()
            cs_mat = np.zeros((5,len(time)))
            cs_mat_sd = np.zeros_like(cs_mat)

        cs_mat[i+1,:] = smooth(cstmp,smooth_pts)
        cs_mat_sd[i+1,:] = cstmp_sd

    # build weight matrix (8x5) for inverse distance weighting 
    # interpolation onto 8 mach probe locations from 5 cs measurement locations
    wij = 1.0 / np.abs(mp_dvec[:,np.newaxis] - cs_dvec[np.newaxis,:])**wp

    # run langmuir sweep analysis on xtips and store te,ne,cs
    # interpolate swept probe cs from xtip and xtip_t5 (if in use)
    # onto triple probe time base and put into cs matrix
    if 26990 < shot < 27726:
        proc_tree = mds.Tree("mpdx_proc",shot)
        ## do processing for xtip, xtip5 
        # for shot > 26990 \\te_lp9, \\dens_lp9, \\vf_lp9 are for xtip
        # for shot > 26990 \\te_lp6, \\dens_lp6, \\vf_lp6 are for xtip_t5
        gamma_e, gamma_i, mu_i, ti, z = mdsbuild.get_node_refs(top_node,"gamma_e","gamma_i","mu_i","ti","z")
        for idx, lp, xtip in zip([0,2],["lp9","lp6"],["xtip","xtip_t5"]):
            lptenode = proc_tree.getNode("\\te_{0}".format(lp))
            lptime, lpte = lptenode.dim_of().data(), lptenode.data()
            lpne, lpvf = proc_tree.getNode("\\dens_{0}".format(lp)).data(), proc_tree.getNode("\\vf_{0}".format(lp)).data()
            toolow = lpne < 1E16
            lpte[toolow] = 0.0
            lpne[toolow] = 0.0
            lpvf[toolow] = 0.0
            teterp = interp1d(lptime,lpte,bounds_error=False,fill_value=0.0)(time)
            teterp[np.isnan(teterp)] = 0.0
            neterp = interp1d(lptime,lpne,bounds_error=False,fill_value=0.0)(time)
            vfterp = interp1d(lptime,lpvf,bounds_error=False,fill_value=0.0)(time)
            csterp = 9790*np.sqrt((z.data()*gamma_e.data()*teterp + gamma_i.data()*ti.data()) / mu_i.data())
            csterp[teterp < 0.1] = 0.0
            tmpcs = smooth(csterp,smooth_pts)
            cs_mat[idx,:] = tmpcs 
            cs_mat_sd[idx,:] = 0.15*tmpcs

            tenode, nenode, vfnode, csnode = mdsbuild.get_node_refs(top_node.getNode(xtip),"te","ne","vfloat","cs")
            tesig = mds.Signal(teterp,None,time)
            tesig_sd = mds.Signal(0.1*teterp,None,time)
            nesig = mds.Signal(neterp,None,time)
            nesig_sd = mds.Signal(0.1*neterp,None,time)
            vfsig = mds.Signal(vfterp,None,time)
            vfsig_sd = mds.Signal(0.05*vfterp,None,time)
            cssig = mds.Signal(tmpcs,None,time)
            cssig_sd = mds.Signal(0.1*tmpcs,None,time)
            mdsbuild.put_wrapper(tenode,tesig,err=tesig_sd,units="eV")
            mdsbuild.put_wrapper(nenode,nesig,err=nesig_sd,units="m^-3")
            mdsbuild.put_wrapper(vfnode,vfsig,err=vfsig_sd,units="V")
            mdsbuild.put_wrapper(csnode,cssig,err=cssig_sd,units="m/s")
    else:
        # set wij for xtips to zero because these probes weren't on for shots outside this range
        wij[:,0] = 0.0
        wij[:,2] = 0.0


#    # For each mach probe load the cs interpolation into the proper node
#    fig,axes = plt.subplots(2,4)
#    fig2,ax2 = plt.subplots()
#    ax2.plot(time,cs_mat.T/1000)
#    ax2.legend(range(5))
    for i in range(0,8):
        mpcs_node = top_node.getNode("mp_{0}:cs".format(i+1))
        cstmp = np.dot(cs_mat.T,wij[i,:]) / np.sum(wij[i,:])
        cstmp_sd = np.sqrt(np.dot((cs_mat_sd.T)**2,wij[i,:]**2) / np.sum(wij[i,:])**2)
        cs_sig = mds.Signal(cstmp,None,time)
#        axes.ravel()[i].plot(time,cstmp/1000)
        cs_sd_sig = mds.Signal(cstmp_sd,None,time)
        mdsbuild.put_wrapper(mpcs_node,cs_sig,err=cs_sd_sig,units="m/s")
#    plt.show()


def main(shot):
    wipal_tree = mds.Tree("wipal",shot)
    qmnt_top = wipal_tree.getNode("kinetics.quad_mach_nt")
    try:
        sql_log(qmnt_top,shot)
    except IOError:
        print("Not processing quad_mach_nt data for shot {0:d}, set top node to off".format(shot))
        qmnt_top.setOn(False)
    else:
        process_data(qmnt_top, shot)
        qmnt_tree = mds.Tree("quad_mach_nt",shot)
        qmnt_tree.cleanDatafile()

if __name__ == "__main__":
    shot = int(sys.argv[1])
    main(shot)

