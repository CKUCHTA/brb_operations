import MDSplus as mds
from subprocess import call
import time
from quad_mach_nt_load_config import main

shots = range(26990,27990)
#shots = range(25705,26990)
for shot in shots:
    print(shot)
    try:
        main(shot)
    except:
        print("Failed to load config for shot: {0}".format(shot))
        continue
