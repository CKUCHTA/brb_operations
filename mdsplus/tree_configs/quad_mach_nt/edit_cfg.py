#!/opt/anaconda2/bin/python
import sys
import MDSplus as mds
import modules.mds_builders as mdsbuild
from modules.npconfigobj import NPConfigObj
import numpy as np
import matplotlib.pyplot as plt

def apply_raw_offsets(shots,t0=0.0,t1=0.25,plotit=True,cfg=None,write=False,reset=False):
    mps = cfg["mps"]
    tps = cfg["tps"]
    for shot in shots:
        wt = mds.Tree("wipal",shot)
        top = wt.getNode("\\qmnt")
        for j,mp in enumerate(["mp_{0:d}".format(jj) for jj in range(1,9)]):
            raw_a = top.getNode("{0}:i_a:raw".format(mp)).data()
            mps[mp]["i_a"]["raw_offset"] = np.mean(raw_a)
            mps[mp]["i_a"]["raw_offset_sd"] = np.std(raw_a)
            raw_b = top.getNode("{0}:i_b:raw".format(mp)).data()
            mps[mp]["i_b"]["raw_offset"] = np.mean(raw_b)
            mps[mp]["i_b"]["raw_offset_sd"] = np.std(raw_b)

        for j,tp in enumerate(["tp_{0:d}".format(jj) for jj in range(1,5)]):
            deltav_raw = top.getNode("{0}:deltav:raw".format(tp)).data()
            tps[tp]["deltav"]["raw_offset"] = np.mean(deltav_raw)
            tps[tp]["deltav"]["raw_offset_sd"] = np.std(deltav_raw)
            isat_raw = top.getNode("{0}:isat:raw".format(tp)).data()
            tps[tp]["isat"]["raw_offset"] = np.mean(isat_raw)
            tps[tp]["isat"]["raw_offset_sd"] = np.std(isat_raw)
            vfloat_raw = top.getNode("{0}:vfloat:raw".format(tp)).data()
            tps[tp]["vfloat"]["raw_offset"] = np.mean(vfloat_raw)
            tps[tp]["vfloat"]["raw_offset_sd"] = np.std(vfloat_raw)

    if write:
        cfg.mode = "edit"
        cfg.write()

def apply_tripleprobe_calfacs(shots,good_probes,t0=1.0,t1=1.5,plotit=True,cfg=None,write=False,reset=False):
    if plotit:
        alps = np.linspace(0.1,1,len(shots))
        fig_tpisats,tpaxes = plt.subplots(1,4,figsize=(16,9),sharex=True)
        fig_interf,ax2 = plt.subplots()
        fig_tp_calfacs,ax3 = plt.subplots()
        fig_tpisats.suptitle("Triple Probe Density Scaled Isat Values")
        ax2.set_title("Interferometer Density")

    for j,shot in enumerate(shots):
        print(shot)
        wt = mds.Tree("wipal",shot)
        top = wt.getNode("kinetics.quad_mach_nt")
        mpdx_proc = mds.Tree("mpdx_proc",shot)

        # get interferometer density and get scale factors for the shot
        # this eliminates shot to shot density variations in the calibration
        # process
        dens_node = mpdx_proc.getNode("\\dens_interf")
        nt,dens = dens_node.dim_of().data(),dens_node.data()/1E18
        ti,tj = np.abs(nt-t0).argmin(), np.abs(nt-t1).argmin()
        scale = np.nanmean(dens[ti:tj])
        if plotit:
            ax2.plot(nt[0::10],dens[0::10],"b",alpha=alps[j])

        # compare triple probe data to interferometer data and scale accordingly
        tps = ["tp_{0}".format(i) for i in range(1,5)]
        for i,tp in enumerate(tps):
            tpne_node = top.getNode("{0}:ne".format(tp))
            cal = top.getNode("{0}:isat:calfac".format(tp)).data()
            time, tpne, tpne_sd = tpne_node.dim_of().data(), tpne_node.data() / (1E18*cal), tpne_node.getError().data() / 1E18
            if j == 0 and i == 0:
                tpne_arr = np.empty((len(shots),4,len(time)))
                tpne_calfacs = np.empty((len(shots),4))
            tpne_arr[j,i,:] = tpne
            ti,tj = np.abs(time-t0).argmin(), np.abs(time-t1).argmin()
            tpne_calfacs[j,i] = scale / np.nanmean(tpne[ti:tj])
            print(j,i,tpne_calfacs[j,i])
            if plotit:
                ax = tpaxes.ravel()[i]
                ax.plot(time[0::1000],tpne[0::1000] * tpne_calfacs[j,i],"r",alpha=alps[j])
                ax.plot(nt[0::10],dens[0::10],"b",alpha=alps[j])
                if j == 0:
                    ax.legend(["TP $n_e$","$n_e$ interf"],loc="upper right")
                    ax.set_title("Probe "+str(tp[-1]))
                    ax.set_ylim(bottom=0)
                    ax.set_ylabel("$I_{sat}$ (mA)")

    # find calfacs for triple probes by averaging over shots
    tp_calfacs = np.nanmean(tpne_calfacs,axis=0)
    tp_calfacs_sd = np.nanstd(tpne_calfacs,axis=0)
    print("TP calfac values: ",  tp_calfacs)
    print("TP calfac_sd values: ",  tp_calfacs_sd)

    if plotit:
        ax3.set_title("Calfacs for triple probes")
        ax3.plot(np.arange(1,5),tp_calfacs,"o")
        plt.show()

    if cfg is not None:
        tps = cfg["tps"]
        if reset:
            # Reset calfacs
            print("Resetting triple probe calfacs...")
            for i in range(1,5):
                tps["tp_{0}".format(i)]["isat"]["calfac"] = 1.0
                tps["tp_{0}".format(i)]["isat"]["calfac_sd"] = 0.0

        else:
            # Load new calfacs
            print("Loading new calfacs for triple probes...")
            for i in range(1,5):
                if i-1 in good_probes:
                    tps["tp_{0}".format(i)]["isat"]["calfac"] = tp_calfacs[i-1]
                    tps["tp_{0}".format(i)]["isat"]["calfac_sd"] = tp_calfacs_sd[i-1]
        if write:
            print("Updating config file...")
            cfg.mode = "edit"
            cfg.write()
            print("Updating config file complete!")
    else:
        return tp_calfacs, tp_calfacs_sd

def apply_mach_calfacs(shots,good_aprobes,good_bprobes,t0=1.0,t1=1.5,plotit=True,cfg=None,write=False,reset=False):
    if plotit:
        alps = np.linspace(0.1,1,len(shots))
        fig_isats,axes = plt.subplots(2,4,figsize=(16,9),sharex=True)
        fig_interf,ax2 = plt.subplots()
        fig_shotavgs,ax3 = plt.subplots()
        fig_calfacs,ax4 = plt.subplots()
        fig_isats.suptitle("Mach Probe Density Scaled Isat Values")
        ax2.set_title("Interferometer Density")

    for j,shot in enumerate(shots):
        print(shot)
        wt = mds.Tree("wipal",shot)
        top = wt.getNode("kinetics.quad_mach_nt")
        mpdx_proc = mds.Tree("mpdx_proc",shot)

        # get interferometer density and get scale factors for the shot
        # this eliminates shot to shot density variations in the calibration
        # process
        dens_node = mpdx_proc.getNode("\\dens_interf")
        nt,dens = dens_node.dim_of().data(),dens_node.data()/1E18
        ti,tj = np.abs(nt-t0).argmin(), np.abs(nt-t1).argmin()
        scale = np.nanmean(dens[ti:tj])
        if plotit:
            ax2.plot(nt[0::10],dens[0::10],"b",alpha=alps[j])

        # get scaled mach probe data
        mps = ["mp_{0}".format(i) for i in range(1,9)]
        for i,mp in enumerate(mps):
            mpia = top.getNode("{0}:i_a".format(mp))
            acal = top.getNode("{0}:i_a:calfac".format(mp)).data()
            mpib = top.getNode("{0}:i_b".format(mp))
            bcal = top.getNode("{0}:i_b:calfac".format(mp)).data()
            time, ia, ib = mpia.dim_of().data(), mpia.data() / acal, mpib.data() / bcal
            ia_sd, ib_sd = mpia.getError().data(), mpib.getError().data()
            if j == 0 and i == 0:
                ia_arr = np.empty((len(shots),8,len(time)))
                ib_arr = np.empty_like(ia_arr)
            ia_arr[j,i,] = ia*1000/scale
            ib_arr[j,i,] = ib*1000/scale
            if plotit:
                ax = axes.ravel()[i]
                ax.plot(time[0::1000],ia[0::1000]*1000/scale,"b",alpha=alps[j])
                ax.plot(time[0::1000],ib[0::1000]*1000/scale,"orange",alpha=alps[j])
                if j == 0:
                    ax.legend(["$I_A$","$I_B$"],loc="upper right")
                    ax.set_title("Probe "+str(mp[-1]))
                    ax.set_ylim(bottom=0)
                    ax.set_ylabel("$I_{sat}$ (mA)")

    # get average signal for each probe face over all shots (only ones used in calibration first) 
    avg_asigs = np.mean(ia_arr[:,good_aprobes,:],axis=0)
    avg_bsigs = np.mean(ib_arr[:,good_bprobes,:],axis=0)
    avg_tot_sig = np.mean(np.vstack((avg_asigs,avg_bsigs)),axis=0)
    # find average isat value from time window
    ti,tj = np.abs(time-t0).argmin(), np.abs(time-t1).argmin()
    norm_isat = np.mean(avg_tot_sig[ti:tj])
    a_calfacs = norm_isat / np.mean(np.mean(ia_arr[:,:,ti:tj],axis=-1),axis=0)
    b_calfacs = norm_isat / np.mean(np.mean(ib_arr[:,:,ti:tj],axis=-1),axis=0)
    a_calfacs_sd = np.std(np.mean(ia_arr[:,:,ti:tj],axis=-1),axis=0) / norm_isat
    b_calfacs_sd = np.std(np.mean(ib_arr[:,:,ti:tj],axis=-1),axis=0) / norm_isat
    print("Mean Isat value: {0:1.4f} mA".format(norm_isat))
    print("I_A calfac values: ",  a_calfacs)
    print("I_B calfac values: ",  b_calfacs)
    print("I_A calfac_sd values: ",  a_calfacs_sd)
    print("I_B calfac_sd values: ",  b_calfacs_sd)

    if plotit:
        ax3.set_title("Shot averaged density normalized signals and mean")
        ax3.plot(time,avg_asigs.T,"b",lw=2)
        ax3.plot(time,avg_bsigs.T,"orange",lw=2)
        ax3.plot(time,avg_tot_sig,"k--",lw=3)

        ax4.set_title("Calfacs for Asides and Bsides")
        ax4.plot(np.arange(1,9),a_calfacs,"o")
        ax4.plot(np.arange(1,9),b_calfacs,"o")

        plt.show()

    if cfg is not None:
        mps = cfg["mps"]
        if reset:
            # Reset calfacs
            print("Resetting mach probe calfacs...")
            for i in range(1,9):
                mps["mp_{0}".format(i)]["i_a"]["calfac"] = 1.0
                mps["mp_{0}".format(i)]["i_a"]["calfac_sd"] = 0.0
                mps["mp_{0}".format(i)]["i_b"]["calfac"] = 1.0
                mps["mp_{0}".format(i)]["i_b"]["calfac_sd"] = 0.0
        else:
            # Load new calfacs
            print("Loading new calfacs for mach probes...")
            for i in range(1,9):
                mps["mp_{0}".format(i)]["i_a"]["calfac"] = a_calfacs[i-1]
                mps["mp_{0}".format(i)]["i_a"]["calfac_sd"] = a_calfacs_sd[i-1]
                mps["mp_{0}".format(i)]["i_b"]["calfac"] = b_calfacs[i-1]
                mps["mp_{0}".format(i)]["i_b"]["calfac_sd"] = b_calfacs_sd[i-1]

        if write:
            print("Updating config file...")
            cfg.mode = "edit"
            cfg.write()
            print("Updating config file complete!")
    else:
        return a_calfacs, b_calfacs, a_calfacs_sd, b_calfacs_sd

def apply_mach_offsets(eshots,wshots,t0=1.0,t1=1.5,isat_thresh=0.003,plotit=True,cfg=None,write=False,reset=False):
    Awisat = np.zeros((len(wshots),8,500000))
    Aeisat = np.zeros((len(eshots),8,500000))
    Bwisat = np.zeros_like(Awisat)
    Beisat = np.zeros_like(Aeisat)

    for i,shot in enumerate(eshots):
        print("East shot: ", shot)
        wt = mds.Tree("wipal",shot)
        # get interferometer density and get scale factors for the shot
        # this eliminates shot to shot density variations in the calibration
        # process
        mpdx_proc = mds.Tree("mpdx_proc",shot)
        dens_node = mpdx_proc.getNode("\\dens_interf")
        nt,dens = dens_node.dim_of().data(),dens_node.data()/1E18
        ti,tj = np.abs(nt-t0).argmin(), np.abs(nt-t1).argmin()
        scale = np.nanmean(dens[ti:tj])
        for j,mp in enumerate(["mp_{0:d}".format(jj) for jj in range(1,9)]):
            ai = wt.getNode("\\qmnt_{0}_i_a".format(mp))
            te = ai.dim_of().data()
            Aeisat[i,j,:] = ai.data() / scale
            Beisat[i,j,:] = wt.getNode("\\qmnt_{0}_i_b".format(mp)).data() / scale

    for i,shot in enumerate(wshots):
        print("West shot: ", shot)
        wt = mds.Tree("wipal",shot)
        # get interferometer density and get scale factors for the shot
        # this eliminates shot to shot density variations in the calibration
        # process
        mpdx_proc = mds.Tree("mpdx_proc",shot)
        dens_node = mpdx_proc.getNode("\\dens_interf")
        nt,dens = dens_node.dim_of().data(),dens_node.data()/1E18
        ti,tj = np.abs(nt-t0).argmin(), np.abs(nt-t1).argmin()
        scale = np.nanmean(dens[ti:tj])
        for j,mp in enumerate(["mp_{0:d}".format(jj) for jj in range(1,9)]):
            ai = wt.getNode("\\qmnt_{0}_i_a".format(mp))
            tw = ai.dim_of().data()
            Awisat[i,j,:] = ai.data() / scale
            Bwisat[i,j,:] = wt.getNode("\\qmnt_{0}_i_b".format(mp)).data() / scale


    Aeisat = Aeisat - np.mean(Aeisat[:,:,0:50000],axis=-1)[:,:,np.newaxis]
    Awisat = Awisat - np.mean(Awisat[:,:,0:50000],axis=-1)[:,:,np.newaxis]
    Beisat = Beisat - np.mean(Beisat[:,:,0:50000],axis=-1)[:,:,np.newaxis]
    Bwisat = Bwisat - np.mean(Bwisat[:,:,0:50000],axis=-1)[:,:,np.newaxis]
    Aeisat = np.nanmean(Aeisat,axis=0)
    Awisat = np.nanmean(Awisat,axis=0)
    Beisat = np.nanmean(Beisat,axis=0)
    Bwisat = np.nanmean(Bwisat,axis=0)
    Ma_offset = (np.log(Aeisat/Beisat) + np.log(Awisat/Bwisat))/2.0
    Ma_offset[np.abs(Aeisat)<isat_thresh] = 0.0
    idx0,idx1 = np.abs(te-t0).argmin(), np.abs(te-t1).argmin()
    ## offsets is vector with dim 8 containing ln(Aa/Ab) which should be subtracted from ln(Ia/Ib) to get mach #
    offsets = np.nanmean(Ma_offset[:,idx0:idx1],axis=-1)
    offsets_sd = np.nanstd(Ma_offset[:,idx0:idx1],axis=-1)
    print("Mach probe offsets: ", offsets)
    print("Mach probe offsets_sd: ", offsets_sd)

    if plotit:
        fig,axes = plt.subplots(2,4,sharex=True,sharey=True,figsize=(10,8))
        for j,ax in enumerate(axes.flatten()):
            ax.plot(te,Ma_offset[j,:],"r")
            ax.axhline(offsets[j],color="k",ls="--",lw=1)
        plt.show()

    if cfg is not None:
        mps = cfg["mps"]
        if reset:
            # Reset Mach Probe offsets
            print("Resetting mach probe offsets to 0...")
            for i in range(1,9):
                mps["mp_{0}".format(i)]["mach_offset"] = 0.0#offsets[i-1]
                mps["mp_{0}".format(i)]["mach_offset_sd"] = 0.0#offsets_sd[i-1]
        else:
            # Set new Mach Probe offsets
            print("Loading new mach probe offsets")
            for i in range(1,9):
                mps["mp_{0}".format(i)]["mach_offset"] = offsets[i-1]
                mps["mp_{0}".format(i)]["mach_offset_sd"] = offsets_sd[i-1]
        if write:
            print("Updating config file...")
            cfg.mode = "edit"
            cfg.write()
            print("Updating config file complete!")

    else:
        return offsets, offsets_sd


if __name__ == "__main__":
    # Get configuration file paths
    treename = "quad_mach_nt"
    cfgroot = "/home/WIPALdata/wipal_mdsplus/tree_configs/{0}/configs".format(treename)
    cfgspec = "{0}/{1}_cfgspec.ini".format(cfgroot,treename)

######### Calibration section for shots 26990 - 27077 #########################
#    #### Get config file information
#    cfgfile = "{0}/{1}_26990-27077.ini".format(cfgroot,treename)
#    cfg = NPConfigObj(cfgfile,configspec=cfgspec,val_on_init=True)
#    print(cfg.filename)
#
#    #### Find and set raw offsets 
#
#    #### Find and set calfacs for mach probes
#    shots = np.arange(27010,27023)
#    bad = [27016,27018,27019,27021]
#    shots = [s for s in shots if s not in bad]
#    good_aprobes = np.array([1,4,5,6,7])
#    good_bprobes = np.array([0,1,4,5,7])
#    apply_mach_calfacs(shots,good_aprobes,good_bprobes,t0=1.1,t1=1.6,plotit=True,cfg=cfg,write=True)
#
#    #### Find and set mach offsets
#    eshots = [27041,27042,27043]
#    wshots = [27044,27045,27046]
#    apply_mach_offsets(eshots,wshots,t0=1.0,t1=1.9,cfg=cfg,reset=True,write=True)
#################################################################################

######## Calibration section for shots 27078 - 27245 #########################
    #### Get config file information
    cfgfile = "{0}/{1}_27078-27245.ini".format(cfgroot,treename)
    cfg = NPConfigObj(cfgfile,configspec=cfgspec,val_on_init=True)
    print("Config file: ", cfg.filename)

    #### Find and set raw offsets 

    #### Find and set calfacs for mach probes
    shots = np.arange(27097,27100)
    bad = []
    shots = [s for s in shots if s not in bad]
    good_aprobes = np.array([1,4,5,6,7])
    good_bprobes = np.array([0,1,4,5,7])
    apply_mach_calfacs(shots,good_aprobes,good_bprobes,t0=1.1,t1=1.6,plotit=True,cfg=cfg,write=True)

    #### Find and set calfacs for triple probes
    shots = np.arange(27110,27114)
    bad = []
    shots = [s for s in shots if s not in bad]
    good_probes = np.array([0,2,3])
    apply_tripleprobe_calfacs(shots,good_probes,t0=1.1,t1=1.6,plotit=True,cfg=cfg,write=True)

    #### Find and set mach offsets
    eshots = [27112,27113]
    wshots = [27110,27111]
    apply_mach_offsets(eshots,wshots,t0=1.0,t1=1.9,cfg=cfg,reset=True,write=True)
################################################################################

######### Calibration section for shots 27639 - 27727 #########################
#    #### Get config file information
#    cfgfile = "{0}/{1}_27639-27727.ini".format(cfgroot,treename)
#    print(cfgfile)
#    cfg = NPConfigObj(cfgfile,configspec=cfgspec,val_on_init=True)
#
#    #### Find and set raw offsets 
#    #apply_raw_offsets...
#
#    #### Find and set calfacs for mach probes
#    shots = np.arange(27676,27679).tolist()# + [27692,27726,27727]
#    bad = []
#    shots = [s for s in shots if s not in bad]
#    good_aprobes = np.array([1,4,5,6,7])
#    good_bprobes = np.array([0,1,4,5,7])
#    apply_mach_calfacs(shots,good_aprobes,good_bprobes,t0=0.9,t1=1.3,plotit=True,cfg=cfg,write=True)
#
#    #### Find and set mach offsets
#    eshots = np.arange(27660,27665)
#    wshots = [27657,27658,27659]
#    apply_mach_offsets(eshots,wshots,t0=0.8,t1=1.0,plotit=True,cfg=cfg,write=True)
#################################################################################


######### Calibration section for shots 27728 - present #########################
#    #### Get config file information
#    cfgfile = "{0}/{1}_27728-present.ini".format(cfgroot,treename)
#    print(cfgfile)
#    cfg = NPConfigObj(cfgfile,configspec=cfgspec,val_on_init=True)
#
#    #### Find and set raw offsets 
#    #apply_raw_offsets...
#
#    #### Find and set calfacs for mach probes
#    shots = np.arange(27828,27831)
#    bad = []
#    shots = [s for s in shots if s not in bad]
#    good_aprobes = np.array([1,4,5,6,7])
#    good_bprobes = np.array([0,1,4,5,7])
#    apply_mach_calfacs(shots,good_aprobes,good_bprobes,t0=0.8,t1=1.0,plotit=True,cfg=cfg,write=True)
#
#    #### Find and set mach offsets
#    eshots = [27796,27798,27802]
#    wshots = [27799,27800,27801]
#    apply_mach_offsets(eshots,wshots,t0=0.8,t1=1.0,plotit=True,cfg=cfg,write=True)
#################################################################################
