"""
Write/edit the config file used for the flux_bdot1 MDSplus tree.
"""
import logging
import json
import os.path
from collections import OrderedDict
import numpy as np
from tree_creates.flux_bdot1_create import NUM_STICKS, NUM_PROBES_PER_STICK

# JSON data structure

# [
    # { (Start of data_to_write)
        # "start_shot": start_shot,
        # "flange": { # The coordinates below are the position of the flange in port coordinates.
            # "position": {
                # "port_coords": {
                    # "x": x_coord,
                    # "y": y_coord,
                    # "z": z_coord,
                # }
            # },
            # "orientation": {
                # "port_coords": {
                    # "x": x_coord,
                    # "y": y_coord,
                    # "z": z_coord,
                # }
            # }
        # },
        # "sticks": {
            # "orientation": {
                # "port_coords": {
                    # "x": x_coord,
                    # "y": y_coord,
                    # "z": z_coord,
                # },
            # },
            # "stick1": {
                # "probes": {
                    # "pb01": {
                        # "raw": (channel, "V"),
                        # "gain": (gain, "T / (s V)"),
                        # "working": working_status,
                        # "orientation": {
                            # "stick_coords": {
                                # "x": x_coord,
                                # "y": y_coord,
                                # "z": z_coord,
                            # },
                        # },
                        # "position": {
                            # "stick_coords": {
                                # "x": (x_coord, "m"),
                                # "y": (y_coord, "m"),
                                # "z": (z_coord, "m"),
                            # },
                        # },
                    # },
                    # ...
                # },
            # }, 
            # ...
        # },
    # }
# ]

def write_json(config_file_path, data_to_write):
    """
    Write some data to the config file. If the start_shot of the new
    data is the same as the start_shot of a pre-existing json section
    then edit the previous section instead of making a new one.

    Parameters
    ----------
    config_file_path : str
        The path to the config file we should be writing in.
    data_to_write : dict
        A dict that contains the data to write for one shot set.
    """
    # Check if config file already exists. If so, read it in. Otherwise create an empty list as the config.
    if os.path.exists(config_file_path):
        # Check if the start_shot of the new data is the same as a start shot already in the config.
        logging.debug("Reading old config file at '{path}'.".format(path=config_file_path))
        with open(config_file_path, 'r') as config_file:
            config = json.load(config_file)
    else:
        logging.warning("Could not find old config file at '{path}'. Creating new file.".format(path=config_file_path))
        config = []

    # Reorder the config so that when rewriting the config, all data is ordered.
    ordered_config = []
    logging.debug("Ordering all blocks in config.")
    for unordered_dict in config:
        ordered_config.append(order_dict(unordered_dict))
    config = ordered_config
    logging.debug("Finished ordering all blocks in config.")

    # Write a backup of the current config file to config_file_path.bak.
    logging.debug("Writing backup config to '{path}'.".format(path=config_file_path + ".bak"))
    with open(config_file_path + ".bak", 'w') as config_file_backup:
        json.dump(config, config_file_backup, indent=4) # Tab in the lines to make it pretty.
    
    # Choose the write mode whether an old block should be edited or a new block added.
    logging.debug("Checking if new data has same start shot as pre-existing data.")
    for block_index, data_block in enumerate(config):
        if data_to_write["start_shot"] == data_block["start_shot"]:
            logging.info("Found old config block with start_shot {shot_num} (index {i}). Will edit this block.".format(shot_num=data_to_write["start_shot"], i=block_index))
            # Edit the block by replacing the old block with the new block.
            config[block_index] = data_to_write

            break
    else:
        logging.info("Did not find pre-existing start_shot {shot_num}. Will add new block.".format(shot_num=data_to_write["start_shot"]))
        config.append(data_to_write)

    logging.debug("Writing to config file.")
    with open(config_file_path, 'w') as config_file:
        json.dump(config, config_file, indent=4) # Tab in the lines to make it pretty.
    logging.info("Succesfully wrote new config data to '{path}'.".format(path=config_file_path))

def order_dict(unordered_dict):
    """
    Change a dictionary object into an ordered dictionary object.
    """
    ordered_dict = OrderedDict()
    for key in sorted(list(unordered_dict.keys())):
        if isinstance(unordered_dict[key], dict):
            ordered_dict[key] = order_dict(unordered_dict[key])
        else:
            ordered_dict[key] = unordered_dict[key]

    return ordered_dict


if __name__ == "__main__":
    # Log to a file rewriting the file each time.
    log_file_path = os.path.join(os.path.realpath(os.path.dirname(__file__)), "flux_bdot1_write_config.log")
    logging.basicConfig(filename=log_file_path, filemode="w", level=logging.DEBUG)

    # Mary's orientations and positions. These are for probes from left to right on the PCB.
    mary_probe_orientation_1 = np.array([
        -0.008850000000000, -0.215350000000000, 0.001250000000000
    ]) # Probe pointed in the db2 direction.
    mary_probe_on_pcb_position_1 = np.array([
        0.329745797984147, 0.104577829842807, 0.027538206843061
    ])
    mary_probe_orientation_2 = np.array([
        0.011800000000000, -0.008850000000000, -0.473750000000000
    ]) # Probe pointed in the db3 direction. This is never used.
    mary_probe_on_pcb_position_2 = np.array([
        0.809494913475074, 0.101291846547378, 0.026451242148188
    ])
    mary_probe_orientation_3 = np.array([
        -0.159300000000000, 0.014750000000000, -0.004375000000000
    ]) # Probe pointed in the db1 direction.
    mary_probe_on_pcb_position_3 = np.array([
        1.333907863140271, 0.099344381572869, 0.028218802444273
    ])
    mary_probe_orientation_4 = np.array([
        0.011800000000000, -0.008850000000000, -0.473750000000000
    ]) # Another probe pointed in the db3 direction. This is the probe that is actually used.
    # TODO: Figure out the correct X coordinate for this probe.
    mary_probe_on_pcb_position_4 = np.array([
        1.809494913475074, 0.101291846547378, 0.026451242148188
    ])

    # Convert the normal areas from inch**2 to m**2.
    mary_probe_orientation_1 *= 0.0254**2
    mary_probe_orientation_3 *= 0.0254**2
    mary_probe_orientation_4 *= 0.0254**2
    # Convert the positions from inch to m.
    mary_probe_on_pcb_position_1 *= 0.0254
    mary_probe_on_pcb_position_3 *= 0.0254
    mary_probe_on_pcb_position_4 *= 0.0254

    # Divide the orientation by a factor of 2. Not sure why but maybe something to do with Mary's calculation.
    mary_probe_orientation_1 /= 2
    mary_probe_orientation_3 /= 2
    mary_probe_orientation_4 /= 2

    # Change the position origin so that y = 0 is the center of the probe in the short direction on the face and z = 0 is the center of the probe through the silicon.
    origin_correction = -0.0254 * np.array([0, 0.2 / 2, 0.059 / 2])
    mary_probe_on_pcb_position_1 += origin_correction
    mary_probe_on_pcb_position_3 += origin_correction
    mary_probe_on_pcb_position_4 += origin_correction

    mary_probe_normalized_area_1 = np.linalg.norm(mary_probe_orientation_1)
    mary_probe_normalized_area_3 = np.linalg.norm(mary_probe_orientation_3)
    mary_probe_normalized_area_4 = np.linalg.norm(mary_probe_orientation_4)

    resistor_1 = 510 # Ohms
    resistor_2 = 82 # Ohms
    resistor_wire = 30 # Ohms
    resistor_divider_gain = (resistor_1 + resistor_2 + resistor_wire) / resistor_1
    # Get the gain for each coil.
    db1_gain = resistor_divider_gain / mary_probe_normalized_area_3
    db2_gain = resistor_divider_gain / mary_probe_normalized_area_1
    db3_gain = resistor_divider_gain / mary_probe_normalized_area_4

    # Convert the probes from Mary's coordinate system to ours.
    # Our coordinate system is: x=along stick from manifold, y=perpendicular to x and z, z=along manifold from flange
    db1_orientation = mary_probe_orientation_3 / mary_probe_normalized_area_3
    db2_orientation = mary_probe_orientation_1 / mary_probe_normalized_area_1
    db3_orientation = mary_probe_orientation_4 / mary_probe_normalized_area_4
    mary_to_stick_coords_index_transform = np.array([0, 1, 2])
    mary_to_stick_coords_reflection_transform = np.array([1, 1, 1])

    for orientation in [db1_orientation, db2_orientation, db3_orientation]:
        orientation = mary_to_stick_coords_reflection_transform * orientation[mary_to_stick_coords_index_transform]

    db1_on_pcb_position = mary_to_stick_coords_reflection_transform * mary_probe_on_pcb_position_3[mary_to_stick_coords_index_transform]
    db2_on_pcb_position = mary_to_stick_coords_reflection_transform * mary_probe_on_pcb_position_1[mary_to_stick_coords_index_transform]
    db3_on_pcb_position = mary_to_stick_coords_reflection_transform * mary_probe_on_pcb_position_4[mary_to_stick_coords_index_transform]

    db3_to_db3_x_separation = 0.051
    manifold_axis_to_pcb_start_x_separation = 0.031
    stick_to_stick_z_separation = 0.12
    manifold_flange_to_stick_8_z_separation = 1.005 - 0.84

    # Get the locations of the probes from closest to furthest from the manifold.
    db1_locations = np.zeros([NUM_STICKS, 2, 3])
    db2_locations = np.zeros([NUM_STICKS, 2, 3])
    db3_locations = np.zeros([NUM_STICKS, 12, 3])
    for stick_index, stick_number in enumerate(range(1, NUM_STICKS + 1)):
        # Stick 1 is the stick furthest from the flange.
        stick_z_position = (NUM_STICKS - stick_number) * stick_to_stick_z_separation + manifold_flange_to_stick_8_z_separation
        # We assume the PCBs are centered in the sticks.
        first_pcb_origin_location = np.array([manifold_axis_to_pcb_start_x_separation, 0, stick_z_position])

        db1_pcb_numbers = np.array([3, 8])
        db2_pcb_numbers = np.array([4, 8])
        db3_pcb_numbers = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])

        db1_locations[stick_index] = (db1_on_pcb_position + first_pcb_origin_location)[np.newaxis, :] + db1_pcb_numbers[:, np.newaxis] * np.array([db3_to_db3_x_separation, 0, 0])[np.newaxis, :]
        db2_locations[stick_index] = (db2_on_pcb_position + first_pcb_origin_location)[np.newaxis, :] + db2_pcb_numbers[:, np.newaxis] * np.array([db3_to_db3_x_separation, 0, 0])[np.newaxis, :]
        db3_locations[stick_index] = (db3_on_pcb_position + first_pcb_origin_location)[np.newaxis, :] + db3_pcb_numbers[:, np.newaxis] * np.array([db3_to_db3_x_separation, 0, 0])[np.newaxis, :]

    db1_to_probe_number_transform = np.array([14, 15])
    db2_to_probe_number_transform = np.array([12, 13])
    db3_to_probe_number_transform = np.array([16, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
    
    def probe_number_to_orientation_dict(probe_number):
        """Get the orientation for a specific probe number on any stick."""
        if probe_number in db1_to_probe_number_transform:
            orientation = db1_orientation
        elif probe_number in db2_to_probe_number_transform:
            orientation = db2_orientation
        elif probe_number in db3_to_probe_number_transform:
            orientation = db3_orientation

        return {direction: coordinate for direction, coordinate in zip(['x', 'y', 'z'], orientation)}
    
    def probe_and_stick_number_to_position_dict(probe_number, stick_number):
        """Get the position for a specific probe number and stick number."""
        if probe_number in db1_to_probe_number_transform:
            position = db1_locations[stick_number - 1, np.where(db1_to_probe_number_transform == probe_number)[0][0]]
        elif probe_number in db2_to_probe_number_transform:
            position = db2_locations[stick_number - 1, np.where(db2_to_probe_number_transform == probe_number)[0][0]]
        elif probe_number in db3_to_probe_number_transform:
            position = db3_locations[stick_number - 1, np.where(db3_to_probe_number_transform == probe_number)[0][0]]

        return {direction: coordinate for direction, coordinate in zip(['x', 'y', 'z'], position)}

    # From `facZ1DArray.mat`
    db3_relative_gains = np.array(
        [
            [1.04696858, 1.03442966, 0.98984241, 1.03290232, 1.05704814, 1.06249413, 1.05314075, 1.05331401, 1.07810642, 1.08494314, 1.1252573 , 1.07119646],
            [1.03915745, 1.00594186, 1.30440572, 0.98677317, 1.01816246, 1.00950681, 1.02870529, 1.01028394, 1.03327913, 1.03624553, 1.06772798, 0.99236664],
            [0.99793413, 0.99591886, 0.9871877 , 0.96796343, 1.00868589, 0.9897961 , 0.96678404, 0.91552791, 0.92655506, 0.96185499, 0.99647735, 0.95235076],
            [1.02329032, 0.88213495, 0.99011218, 0.99548735, 0.98138551, 0.97355166, 0.92337243, 2.34241294, 0.88426815, 1.2031925 , 0.9512863 , 0.95889164],
            [0.99007036, 0.9942353 , 0.98019389, 1.00355852, 0.98942508, 0.98009253, 0.94318426, 0.92151516, 0.92251042, 0.92816648, 0.91398716, 0.94497991],
            [0.97155474, 0.9461144 , 0.97245801, 0.98587826, 0.98568281, 0.97127533, 0.98720824, 0.97563952, 0.97752563, 0.93831155, 1.4744793 , 0.94393492],
            [0.92927385, 1.40926039, 0.92817724, 0.97858474, 0.93349125, 0.96372761, 0.97418775, 0.96653972, 0.9684484 , 0.94805048, 0.95089231, 0.95713533],
            [1.00045509, 0.94779434, 0.97982166, 0.93332367, 2.12538415, 0.91642835, 0.97426137, 1.30171757, 0.98029842, 1.00957771, 1.03693665, 0.95754323]
        ]
    )
    db3_relative_gains = db3_relative_gains[:, ::-1]

    def gain_factor(probe_number, stick_number):
        """Get the gain for a specific probe number and stick number."""
        if probe_number in db1_to_probe_number_transform:
            gain = db1_gain
        elif probe_number in db2_to_probe_number_transform:
            gain = db2_gain
        else:
            gain = db3_gain * db3_relative_gains[stick_number - 1, np.where(db3_to_probe_number_transform == probe_number)[0][0]]

        return gain


    # Change this dictionary to add/edit the config.
    # If a 'None' value is set either as the element or as the first element of the tuple then nothing is written into the MDSplus table.
    data_to_write = {
        "start_shot": 62499,
        "flange": { # The coordinates below are the position and orientation of the flange in port coordinates when `insert` and `clocking` is 0.
            "position": {
                "port_coords": {
                    # Distance from flange to insertion axis.
                    "x": -0.249,
                    "y": 0,
                    "z": 1.390 - 1.005 + 6 * 0.0254, # Last term is because of extra 6 inch garage.
                }
            },
            "orientation": {
                "port_coords": {
                    # Orientation of the manifold off the flange.
                    "x": 0,
                    "y": 0,
                    "z": 1,
                }
            },
        },
        "sticks": {
            "orientation": {
                "port_coords": {
                    "x": 1,
                    "y": 0,
                    "z": 0,
                },
            },
            "stick1": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta115_01", "V"),
                        "gain": (gain_factor(1, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 1),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta115_02", "V"),
                        "gain": (gain_factor(2, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 1),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta115_03", "V"),
                        "gain": (gain_factor(3, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 1),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta115_04", "V"),
                        "gain": (gain_factor(4, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 1),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta115_05", "V"),
                        "gain": (gain_factor(5, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 1),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta115_06", "V"),
                        "gain": (gain_factor(6, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 1),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta115_07", "V"),
                        "gain": (gain_factor(7, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 1),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta115_08", "V"),
                        "gain": (gain_factor(8, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 1),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta115_09", "V"),
                        "gain": (gain_factor(9, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 1),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta115_10", "V"),
                        "gain": (gain_factor(10, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 1),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta115_11", "V"),
                        "gain": (gain_factor(11, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 1),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta115_12", "V"),
                        "gain": (gain_factor(12, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 1),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta115_13", "V"),
                        "gain": (gain_factor(13, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 1),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta115_14", "V"),
                        "gain": (gain_factor(14, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 1),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta115_15", "V"),
                        "gain": (gain_factor(15, 1), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 1),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta115_16", "V"),
                        "gain": (gain_factor(16, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 1),
                        },
                    },
                },
            }, 
            "stick2": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta115_17", "V"),
                        "gain": (gain_factor(1, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 2),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta115_18", "V"),
                        "gain": (gain_factor(2, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 2),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta115_19", "V"),
                        "gain": (gain_factor(3, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 2),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta115_20", "V"),
                        "gain": (gain_factor(4, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 2),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta115_21", "V"),
                        "gain": (gain_factor(5, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 2),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta115_22", "V"),
                        "gain": (gain_factor(6, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 2),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta115_23", "V"),
                        "gain": (gain_factor(7, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 2),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta115_24", "V"),
                        "gain": (gain_factor(8, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 2),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta115_25", "V"),
                        "gain": (gain_factor(9, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 2),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta115_26", "V"),
                        "gain": (gain_factor(10, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 2),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta115_27", "V"),
                        "gain": (gain_factor(11, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 2),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta115_28", "V"),
                        "gain": (gain_factor(12, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 2),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta115_29", "V"),
                        "gain": (gain_factor(13, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 2),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta115_30", "V"),
                        "gain": (gain_factor(14, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 2),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta115_31", "V"),
                        "gain": (gain_factor(15, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 2),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta115_32", "V"),
                        "gain": (gain_factor(16, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 2),
                        },
                    },
                },
            }, 
            "stick3": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta113_01", "V"),
                        "gain": (gain_factor(1, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 3),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta113_02", "V"),
                        "gain": (gain_factor(2, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 3),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta113_03", "V"),
                        "gain": (gain_factor(3, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 3),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta113_04", "V"),
                        "gain": (gain_factor(4, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 3),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta113_05", "V"),
                        "gain": (gain_factor(5, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 3),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta113_06", "V"),
                        "gain": (gain_factor(6, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 3),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta113_07", "V"),
                        "gain": (gain_factor(7, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 3),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta113_08", "V"),
                        "gain": (gain_factor(8, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 3),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta113_09", "V"),
                        "gain": (gain_factor(9, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 3),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta113_10", "V"),
                        "gain": (gain_factor(10, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 3),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta113_11", "V"),
                        "gain": (gain_factor(11, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 3),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta113_12", "V"),
                        "gain": (gain_factor(12, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 3),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta113_13", "V"),
                        "gain": (gain_factor(13, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 3),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta113_14", "V"),
                        "gain": (gain_factor(14, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 3),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta113_15", "V"),
                        "gain": (gain_factor(15, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 3),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta113_16", "V"),
                        "gain": (gain_factor(16, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 3),
                        },
                    },
                },
            }, 
            "stick4": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta113_17", "V"),
                        "gain": (gain_factor(1, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 4),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta113_18", "V"),
                        "gain": (gain_factor(2, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 4),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta113_19", "V"),
                        "gain": (gain_factor(3, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 4),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta113_20", "V"),
                        "gain": (gain_factor(4, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 4),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta113_21", "V"),
                        "gain": (gain_factor(5, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 4),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta113_22", "V"),
                        "gain": (gain_factor(6, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 4),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta113_23", "V"),
                        "gain": (gain_factor(7, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 4),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta113_24", "V"),
                        "gain": (gain_factor(8, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 4),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta113_25", "V"),
                        "gain": (gain_factor(9, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 4),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta113_26", "V"),
                        "gain": (gain_factor(10, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 4),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta113_27", "V"),
                        "gain": (gain_factor(11, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 4),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta113_28", "V"),
                        "gain": (gain_factor(12, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 4),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta113_29", "V"),
                        "gain": (gain_factor(13, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 4),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta113_30", "V"),
                        "gain": (gain_factor(14, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 4),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta113_31", "V"),
                        "gain": (gain_factor(15, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 4),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta113_32", "V"),
                        "gain": (gain_factor(16, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 4),
                        },
                    },
                },
            }, 
            "stick5": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta114_01", "V"),
                        "gain": (gain_factor(1, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 5),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta114_02", "V"),
                        "gain": (gain_factor(2, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 5),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta114_03", "V"),
                        "gain": (gain_factor(3, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 5),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta114_04", "V"),
                        "gain": (gain_factor(4, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 5),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta114_05", "V"),
                        "gain": (gain_factor(5, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 5),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta114_06", "V"),
                        "gain": (gain_factor(6, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 5),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta114_07", "V"),
                        "gain": (gain_factor(7, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 5),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta114_08", "V"),
                        "gain": (gain_factor(8, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 5),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta114_09", "V"),
                        "gain": (gain_factor(9, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 5),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta114_10", "V"),
                        "gain": (gain_factor(10, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 5),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta114_11", "V"),
                        "gain": (gain_factor(11, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 5),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta114_12", "V"),
                        "gain": (gain_factor(12, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 5),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta114_13", "V"),
                        "gain": (gain_factor(13, 5), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 5),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta114_14", "V"),
                        "gain": (gain_factor(14, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 5),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta114_15", "V"),
                        "gain": (gain_factor(15, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 5),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta114_16", "V"),
                        "gain": (gain_factor(16, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 5),
                        },
                    },
                },
            }, 
            "stick6": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta114_17", "V"),
                        "gain": (gain_factor(1, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 6),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta114_18", "V"),
                        "gain": (gain_factor(2, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 6),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta114_19", "V"),
                        "gain": (gain_factor(3, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 6),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta114_20", "V"),
                        "gain": (gain_factor(4, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 6),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta114_21", "V"),
                        "gain": (gain_factor(5, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 6),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta114_22", "V"),
                        "gain": (gain_factor(6, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 6),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta114_23", "V"),
                        "gain": (gain_factor(7, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 6),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta114_24", "V"),
                        "gain": (gain_factor(8, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 6),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta114_25", "V"),
                        "gain": (gain_factor(9, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 6),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta114_26", "V"),
                        "gain": (gain_factor(10, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 6),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta114_27", "V"),
                        "gain": (gain_factor(11, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 6),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta114_28", "V"),
                        "gain": (gain_factor(12, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 6),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta114_29", "V"),
                        "gain": (gain_factor(13, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 6),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta114_30", "V"),
                        "gain": (gain_factor(14, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 6),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta114_31", "V"),
                        "gain": (gain_factor(15, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 6),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta114_32", "V"),
                        "gain": (gain_factor(16, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 6),
                        },
                    },
                },
            }, 
            "stick7": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta322_01", "V"),
                        "gain": (gain_factor(1, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 7),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta322_12", "V"),
                        "gain": (gain_factor(2, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 7),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta322_03", "V"),
                        "gain": (gain_factor(3, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 7),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta322_04", "V"),
                        "gain": (gain_factor(4, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 7),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta322_05", "V"),
                        "gain": (gain_factor(5, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 7),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta322_02", "V"),
                        "gain": (gain_factor(6, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 7),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta322_15", "V"),
                        "gain": (gain_factor(7, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 7),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta322_08", "V"),
                        "gain": (gain_factor(8, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 7),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta322_09", "V"),
                        "gain": (gain_factor(9, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 7),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta322_10", "V"),
                        "gain": (gain_factor(10, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 7),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta322_07", "V"),
                        "gain": (gain_factor(11, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 7),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta322_06", "V"),
                        "gain": (gain_factor(12, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 7),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta322_13", "V"),
                        "gain": (gain_factor(13, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 7),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta322_14", "V"),
                        "gain": (gain_factor(14, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 7),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta322_11", "V"),
                        "gain": (gain_factor(15, 7), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 7),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta322_16", "V"),
                        "gain": (gain_factor(16, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 7),
                        },
                    },
                },
            }, 
            "stick8": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta322_26", "V"),
                        "gain": (gain_factor(1, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 8),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta322_25", "V"),
                        "gain": (-gain_factor(2, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 8),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta322_24", "V"),
                        "gain": (gain_factor(3, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 8),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta322_23", "V"),
                        "gain": (gain_factor(4, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 8),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta322_22", "V"),
                        "gain": (gain_factor(5, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 8),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta322_21", "V"),
                        "gain": (gain_factor(6, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 8),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta322_20", "V"),
                        "gain": (gain_factor(7, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 8),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta322_19", "V"),
                        "gain": (gain_factor(8, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 8),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta322_18", "V"),
                        "gain": (gain_factor(9, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 8),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta322_17", "V"),
                        "gain": (gain_factor(10, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 8),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta322_32", "V"),
                        "gain": (gain_factor(11, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 8),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta322_30", "V"),
                        "gain": (gain_factor(12, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 8),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta322_31", "V"),
                        "gain": (gain_factor(13, 8), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 8),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta322_29", "V"),
                        "gain": (gain_factor(14, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 8),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta322_28", "V"),
                        "gain": (gain_factor(15, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 8),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta322_27", "V"),
                        "gain": (gain_factor(16, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 8),
                        },
                    },
                },
            }, 
        },
    }

    logging.debug("Reordering data to write for ease of reading.")
    ordered_data_to_write = order_dict(data_to_write)
    config_file_path = os.path.join(os.path.realpath(os.path.dirname(__file__)), "configs/flux_bdot1.json")

    write_json(config_file_path, ordered_data_to_write)
