"""
These are functions needed to edit the config object after we take a shot.
They don't actually edit the config file and instead add/remove information
that is needed when writing to the tree.
"""
import logging
import numpy as np
import modules.coordinate_transforms as coords
from modules.mysql_connection import get_engine
import pandas
import MDSplus as mds
from tree_creates.flux_bdot1_create import NUM_STICKS, NUM_PROBES_PER_STICK

def read_sql(shot_number, sql_config_path="/home/WIPALdata/sql_config.ini"):
    """
    Read the probe position from the database for the queried shot.

    Parameters
    ----------
    shot_number : int
        The number of the shot we want the probe position for.
    sql_config : str, default='/home/WIPALdata/sql_config.ini'
        Path to config file containing sql info for database.

    Returns
    -------
    query_result : QueryResult
        Object containing the returned values from the database.
    """
    # TODO: Add a table to the MySQL database for the flux probe that is similar to the `flux_bdot1` table.
    logging.debug("Reading from MySQL database te probe info for shot {num}.".format(num=shot_number))
    engine = get_engine(sql_config_path)
    database = engine.url.database
    with engine.connect() as connection:
        sql_query = "SELECT insertion, clocking, latitude, longitude, alpha, beta, gamma, port_radius FROM {database}.flux_bdot1 where shot_num={num}".format(database=database, num=shot_number)

        logging.debug("Executing query '{query}'.".format(query=sql_query))
        try:
            result = pandas.read_sql(sql_query, connection)
        except Exception:
            logging.exception("Failure executing query '{query}'.".format(query=sql_query))
            raise
        else:
            logging.info("Executed query successfully.")

    if result.size == 0:
        logging.exception("Could not find values for shot {}.".format(shot_number))
        raise ValueError("Missing row in MySQL '{table}' table for shot {num}.".format(table='flux_bdot1', num=shot_number))

    class QueryResult:
        # Change the query result into an object for easy access.
        def __init__(self, query_result):
            # Set the value of each variable to that of the result.
            self.insertion, self.clocking_deg, self.latitude_deg, self.longitude_deg, self.alpha_deg, self.beta_deg, self.gamma_deg, self.port_radius = query_result.iloc[0]

            # Change all variables to radians.
            self.clocking = np.deg2rad(self.clocking_deg)
            self.latitude = np.deg2rad(self.latitude_deg)
            self.longitude = np.deg2rad(self.longitude_deg)
            self.alpha = np.deg2rad(self.alpha_deg)
            self.beta = np.deg2rad(self.beta_deg)
            self.gamma = np.deg2rad(self.gamma_deg)

    # Extract the values from the call.
    query_result = QueryResult(result)
    logging.debug("Got values from row:\n{}".format(result))
    return query_result

def stick_to_port_coordinates_position(
        stick_coords_position_dict, flange_port_coords_position_dict, 
        flange_port_coords_orientation_dict, sticks_port_coords_orientation_dict,
        insertion
    ):
    """
    Convert a position in stick coordinates to port coordinates.
    
    Parameters
    ----------
    stick_coords_position_dict : dict[str: float]
        Dictionary for a position in stick coordinates of the form `{'x': x_value, 'y': y_value, 'z': z_value}`.
    flange_port_coords_position_dict, flange_port_coords_orientation_dict : dict[str: float]
        Dictionaries of the same form as above but for the flange position and orientation in port coordinates.
    sticks_port_coords_orientation_dict : dict[str: float]
        Dictionary of the same form as above but for the orientation of the sticks.
    insertion : float
        Insertion distance of probe from MySQL query.

    Returns
    -------
    dict
    """
    stick_position_array = np.array([stick_coords_position_dict[direction] for direction in ['x', 'y', 'z']])
    flange_port_position_array = np.array([flange_port_coords_position_dict[direction] for direction in ['x', 'y', 'z']])
    flange_port_orientation_array = np.array([flange_port_coords_orientation_dict[direction] for direction in ['x', 'y', 'z']])
    sticks_port_orientation_array = np.array([sticks_port_coords_orientation_dict[direction] for direction in ['x', 'y', 'z']])

    port_position_array = flange_port_position_array + np.array([0, 0, insertion]) + stick_position_array[2] * flange_port_orientation_array + \
        stick_position_array[0] * sticks_port_orientation_array + stick_position_array[1] * -np.cross(sticks_port_orientation_array, flange_port_orientation_array)
    
    return {direction: port_position_array[direction_index] for direction_index, direction in enumerate(['x', 'y', 'z'])}

def stick_to_port_coordinates_orientation(stick_coords_orientation_dict, flange_port_coords_orientation_dict, sticks_port_coords_orientation_dict):
    """
    Convert an orientation stick coordinates to port coordinates.
    
    Parameters
    ----------
    stick_coords_orientation_dict : dict[str: float]
        Dictionary for an orientation in stick coordinates of the form `{'x': x_value, 'y': y_value, 'z': z_value}`.
    flange_port_coords_orientation_dict : dict[str: float]
        Dictionary of the same form as above but for the flange orientation in port coordinates.
    sticks_port_coords_orientation_dict : dict[str: float]
        Dictionary of the same form as above but for the orientation of the sticks.
    """
    stick_orientation_array = np.array([stick_coords_orientation_dict[direction] for direction in ['x', 'y', 'z']])
    flange_port_orientation_array = np.array([flange_port_coords_orientation_dict[direction] for direction in ['x', 'y', 'z']])
    sticks_port_orientation_array = np.array([sticks_port_coords_orientation_dict[direction] for direction in ['x', 'y', 'z']])

    port_orientation_array = stick_orientation_array[2] * flange_port_orientation_array + \
        stick_orientation_array[0] * sticks_port_orientation_array + stick_orientation_array[1] * -np.cross(sticks_port_orientation_array, flange_port_orientation_array)
    
    return {direction: port_orientation_array[direction_index] for direction_index, direction in enumerate(['x', 'y', 'z'])}

def edit_config(config_object, shot_number):
    """
    Edit the config object before inserting data into the MDSplus tree.
    
    Parameters
    ----------
    config_object : dict[str, dict[...]]
        Object read from config file.
    shot_number : int
        The shot number we are evaluating.

    Returns
    -------
    config_object : dict[str, dict[...]]
    """
    logging.debug("Editing config object for shot {}.".format(shot_number))
    logging.debug("Removing 'start_shot' from config data since no need to insert into tree.")
    config_object.pop('start_shot')

    # Try to connect to the MySQL database.
    try:
        query = read_sql(shot_number)
    except ValueError as e:
        logging.info("Can't get probe positions for shot {} since no row in database. Error was:\n{}".format(shot_number, e))
        raise
    except Exception as e:
        logging.info("Caught error trying to read from MySQL. Can't get probe position information. Error was:\n{}".format(e))
        raise

    logging.debug("Adding port information.")
    config_object['port'] = {
        'clock': query.clocking_deg,
        'rport': query.port_radius,
        'lat': query.latitude_deg,
        'long': query.longitude_deg,
        'alpha': query.alpha_deg,
        'beta': query.beta_deg,
        'gamma': query.gamma_deg,
        'insert': query.insertion
    }

    logging.debug("Adding probe positions and orientations.")
    flange_port_position_dict = config_object['flange']['position']['port_coords']
    flange_port_orientation_dict = config_object['flange']['orientation']['port_coords']
    sticks_port_orientation_dict = config_object['sticks']['orientation']['port_coords']
    for stick_number in range(1, NUM_STICKS + 1):
        stick_key = 'stick{}'.format(stick_number)
        stick_dict = config_object['sticks'][stick_key]
        logging.debug("Adding probe positions and orientations for stick #{}.".format(stick_number))
        for probe_number in range(1, NUM_PROBES_PER_STICK + 1):
            logging.debug("Adding probe positions and orientations for stick #{}, probe #{}.".format(stick_number, probe_number))
            probe_key = 'pb{}'.format(str(probe_number).zfill(2))
            probe_dict = stick_dict['probes'][probe_key]

            # Get the probe position in port and machine coordinates.
            probe_stick_position_dict = probe_dict['position']['stick_coords']
            probe_port_position_dict = stick_to_port_coordinates_position(
                probe_stick_position_dict, flange_port_position_dict, flange_port_orientation_dict, sticks_port_orientation_dict, query.insertion
            )
            probe_machine_cartesion_position_array = coords.position_to_machine_cartesian(
                query.port_radius, query.longitude, query.latitude, query.alpha, query.beta, query.gamma, 
                probe_port_position_dict['x'], probe_port_position_dict['y'], probe_port_position_dict['z'], query.clocking
            )
            probe_machine_cylindrical_array = coords.position_cartesian_to_cylindrical(probe_machine_cartesion_position_array)
            probe_machine_position_dict = {
                direction: probe_machine_cylindrical_array[direction_index] for direction_index, direction in enumerate(['r', 'phi', 'z'])
            }
            config_object['sticks'][stick_key]['probes'][probe_key]['position']['port_coords'] = probe_port_position_dict
            config_object['sticks'][stick_key]['probes'][probe_key]['position']['mach_coords'] = probe_machine_position_dict

            # Get the probe orientation in port and machine coordinates.
            probe_stick_orientation_dict = probe_dict['orientation']['stick_coords']
            probe_port_orientation_dict = stick_to_port_coordinates_orientation(probe_stick_orientation_dict, flange_port_orientation_dict, sticks_port_orientation_dict)
            probe_machine_cartesion_orientation_array = coords.direction_to_machine_cartesian(
                query.longitude, query.latitude, query.alpha, query.beta, query.gamma, 
                probe_port_orientation_dict['x'], probe_port_orientation_dict['y'], probe_port_orientation_dict['z'], query.clocking
            )
            probe_machine_cylindrical_orientation_array = coords.direction_cartesian_to_cylindrical(probe_machine_cartesion_orientation_array, probe_machine_cartesion_position_array)
            probe_machine_orientation_dict = {
                direction: probe_machine_cylindrical_orientation_array[direction_index] for direction_index, direction in enumerate(['r', 'phi', 'z'])
            }
            config_object['sticks'][stick_key]['probes'][probe_key]['orientation']['port_coords'] = probe_port_orientation_dict
            config_object['sticks'][stick_key]['probes'][probe_key]['orientation']['mach_coords'] = probe_machine_orientation_dict

    logging.debug("Done editing config object.")
    return config_object

