#!/opt/anaconda2/bin/python
import numpy as np
import pandas as pio
import sqltools.core as sql
import MDSplus as mds
import modules.mds_builders as mdsbuild
from modules.mysql_connection import get_engine
import pandas
import argparse

"""
To Do:
    Probe Location Code
"""
mu = {
     'ar': 40.0,
     'he': 4.0,
     'h': 1.0,
     'h2': 1.0,
     'd': 2.0,
     'ne': 20.0,
     'none': 1.0,
      }

def main(shot_num):
    wipal_tree = mds.Tree("wipal", shot_num)
    mp_tp_top = wipal_tree.getNode("kinetics.mach_triple")
    mu_node = mp_tp_top.getNode("mu")
    nprobes = 5
    probes = ["probe_{0:d}".format(x+1) for x in range(nprobes)]

    sql_config_path="/home/WIPALdata/sql_config.ini"
    engine = get_engine(sql_config_path)
    database = engine.url.database
    with engine.connect() as connection:
        sql_query = "select shot_num, shot_gastype from {0}.shot_log_basic where shot_num = {1:d}".format(database, shot_num)
        df_basic = pandas.read_sql(sql_query, connection)

    gastype = df_basic.shot_gastype[0]
    gas_mu = mu[gastype.lower()]
    print(gastype, gas_mu)
    mdsbuild.put_wrapper(mu_node, mds.Float32(gas_mu), err=mds.Float32(0.0), units="1") 

    for idx, probe in enumerate(probes):
        probe_node = mp_tp_top.getNode(probe)
        sql_log(probe_node, shot_num, idx+1)
        calculate_offsets(probe_node, npts=100)

def sql_log(probe_node, shot_num, probe_number):
    sql_config_path="/home/WIPALdata/sql_config.ini"
    engine = get_engine(sql_config_path)
    database = engine.url.database
    with engine.connect() as connection:
        sql_query = "select * from {0}.mach_triple{1:d} where shot_num = {2:d}".format(database, probe_number, shot_num)
        try:
            df = pandas.read_sql(sql_query, connection)
        except:
            raise Exception("Really shouldn't try to catch all exceptions!")
        else:
            if df.status[0] == 'off':
                print("probe is off.  Ethan throws an exception here??  How does this not crash everything?")
            else:
                mp, tp, r, theta, phi = mdsbuild.get_node_refs(probe_node, "mp", "tp", "r", "theta", "phi")

                # handle tp
                delta_v, isat, vfloat = mdsbuild.get_node_refs(tp, "deltav", "isat", "vfloat")

                d_r1, d_r2, d_raw = mdsbuild.get_node_refs(delta_v, "r1", "r2", "raw")
                f_r1, f_r2, f_raw = mdsbuild.get_node_refs(vfloat, "r1", "r2", "raw")
                area, r, isat_raw = mdsbuild.get_node_refs(isat, "area", "r", "raw")

                # raw node pointers for tp
                raw_node_string = "\\TOP.RAW.{0}.CH_{1:02d}".format(df.digitizer[0], df.delta_v_ch[0]).upper()
                d_raw.putData(mds.TreePath(raw_node_string))

                raw_node_string = "\\TOP.RAW.{0}.CH_{1:02d}".format(df.digitizer[0], df.vf_ch[0]).upper()
                f_raw.putData(mds.TreePath(raw_node_string))

                raw_node_string = "\\TOP.RAW.{0}.CH_{1:02d}".format(df.digitizer[0], df.isat_ch[0]).upper()
                isat_raw.putData(mds.TreePath(raw_node_string))

                # delta v stuff
                mdsbuild.put_wrapper(d_r1, mds.Float32(df.delta_v_R1[0]), err=mds.Float32(0.0), units="Ohms")
                mdsbuild.put_wrapper(d_r2, mds.Float32(df.delta_v_R2[0]), err=mds.Float32(0.0), units="Ohms")

                # vfloat stuff
                mdsbuild.put_wrapper(f_r1, mds.Float32(df.vf_R1[0]), err=mds.Float32(0.0), units="")
                mdsbuild.put_wrapper(f_r2, mds.Float32(df.vf_R2[0]), err=mds.Float32(0.0), units="")

                # isat stuff
                mdsbuild.put_wrapper(area, mds.Float32(df.isat_area[0]), err=mds.Float32(0.0), units="m^2")
                mdsbuild.put_wrapper(r, mds.Float32(df.isat_R[0]), err=mds.Float32(0.0), units="Ohms")

                mp1, mp2 = mdsbuild.get_node_refs(mp, "mp1", "mp2")
                # handle mp1
                A, B, AtoB_vec, mach_offset = mdsbuild.get_node_refs(mp1, "A", "B", "AtoBvec", "mach_offset")

                A_R, A_raw = mdsbuild.get_node_refs(A, "r", "raw")
                B_R, B_raw = mdsbuild.get_node_refs(B, "r", "raw")

                raw_node_string = "\\TOP.RAW.{0}.CH_{1:02d}".format(df.digitizer[0], df.mp1_A_ch[0]).upper()
                A_raw.putData(mds.TreePath(raw_node_string))

                raw_node_string = "\\TOP.RAW.{0}.CH_{1:02d}".format(df.digitizer[0], df.mp1_B_ch[0]).upper()
                B_raw.putData(mds.TreePath(raw_node_string))

                mdsbuild.put_wrapper(A_R, mds.Float32(df.mp1_A_R[0]), err=mds.Float32(0.0), units="Ohms")
                mdsbuild.put_wrapper(B_R, mds.Float32(df.mp1_B_R[0]), err=mds.Float32(0.0), units="Ohms")
                mdsbuild.put_wrapper(mach_offset, mds.Float32(0.0), err=mds.Float32(0.0), units="1")

                # handle mp2
                A, B, AtoB_vec, mach_offset = mdsbuild.get_node_refs(mp2, "A", "B", "AtoBvec", "mach_offset")
                A_R, A_raw = mdsbuild.get_node_refs(A, "r", "raw")
                B_R, B_raw = mdsbuild.get_node_refs(B, "r", "raw")

                raw_node_string = "\\TOP.RAW.{0}.CH_{1:02d}".format(df.digitizer[0], df.mp2_A_ch[0]).upper()
                A_raw.putData(mds.TreePath(raw_node_string))

                raw_node_string = "\\TOP.RAW.{0}.CH_{1:02d}".format(df.digitizer[0], df.mp2_B_ch[0]).upper()
                B_raw.putData(mds.TreePath(raw_node_string))

                mdsbuild.put_wrapper(A_R, mds.Float32(df.mp2_A_R[0]), err=mds.Float32(0.0), units="Ohms")
                mdsbuild.put_wrapper(B_R, mds.Float32(df.mp2_B_R[0]), err=mds.Float32(0.0), units="Ohms")
                mdsbuild.put_wrapper(mach_offset, mds.Float32(0.0), err=mds.Float32(0.0), units="1")

                #mdsbuild.put_wrapper(, mds.Float32(), err=mds.Float32(0.0), units="")


def calculate_offsets(probe_node, npts=100):

    mp, tp = mdsbuild.get_node_refs(probe_node, "mp", "tp")
    mp1, mp2 = mdsbuild.get_node_refs(mp, "mp1", "mp2")
    mps = ["mp1", "mp2"] 
    faces = ["A", "B"]
    for mach in mps:
        probe = mdsbuild.get_node_refs(mp, mach)
        for face in faces:
            face_node = mdsbuild.get_node_refs(probe, face)
            raw, raw_offset = mdsbuild.get_node_refs(face_node, "raw", "raw_offset")
            print(face_node)
            print(raw)
            try:
                data = raw.data()
                offset = mds.Float32(np.mean(data[0:npts]))
                offset_sd = mds.Float32(np.std(data[0:npts]))
                mdsbuild.put_wrapper(raw_offset, offset, err=offset_sd, units="V")
            except mds.TreeNODATA as e:
                print("No data for {0}".format(raw))

    tp_nodes = ["deltav", "isat", "vfloat"]
    for node in tp_nodes:
        tp_node = mdsbuild.get_node_refs(tp, node)
        raw, raw_offset = mdsbuild.get_node_refs(tp_node, "raw", "raw_offset")
        try:
            data = raw.data()
            offset = mds.Float32(np.mean(data[0:npts]))
            offset_sd = mds.Float32(np.std(data[0:npts]))
            mdsbuild.put_wrapper(raw_offset, offset, err=offset_sd, units="V")
        except mds.TreeNODATA as e:
            print("No data for {0}".format(raw))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Reads sql data and fills in necessary data in the mach_triple tree.")
    parser.add_argument("shot_num", metavar="Shot Number", type=int, help="Shot number to fill in MDSplus tree")
    args = parser.parse_args()

    main(args.shot_num)
