import MDSplus as mds
import matplotlib.pyplot as plt
from modules.mysql_connection import get_engine
import pandas
import pandas.io.sql as pio
import pprint
import modules.mds_builders as mdsbuild
import numpy as np
import sys

def offsub(sig,sig_side="front",sig_frac=.1):
    if sig_side.lower() in ["front", "beginning"]:
        return sig - np.mean(sig[0:int(len(sig)*sig_frac)])
    elif sig_side.lower() in ["back","end"]:
        return sig - np.mean(sig[(len(sig)*(1-sig_frac)):])
    else:
        raise ValueError("invalid option for keyword arg sig_side, must be from [front, beginning, back, end]")

def sql_log(top_node,shot):
    # get MySQL table for spiral_probe
    # load following top level values into spiral_probe tree
    # R_PIVOT, THETA_PIVOT, PHI_PIVOT, R0, DELTA_THETA, PS_VOLTAGE
    sql_config_path = '/home/WIPALdata/sql_config.ini'
    engine = get_engine(sql_config_path)
    database = engine.url.database
    with engine.connect() as connection:
        sql_query = 'select * from {0}.spiral_probe where shot_num = {1:d}'.format(database, shot)

        try:
            df = pandas.read_sql(sql_query, connection)
        except Exception:
            raise Exception("Failed to read MySQL table {0}.spiral_probe for shot {1}".format(database, shot))
        else:
            if df.status[0] == "off":
                raise Exception("MySQL table {0}.spiral_probe status is 'off' for shot {1}".format(database, shot))
            else:
                r_pivot, theta_pivot, phi_pivot, r0, delta_theta,bdot_1_dir = mdsbuild.get_node_refs(top_node,
                        "r_pivot","theta_pivot","phi_pivot","r0","delta_theta","bdot_1_dir")
                # R_PIVOT
                rpiv,rpiv_sd = mds.Float64(df.r_pivot[0]), mds.Float64(0.003)
                mdsbuild.put_wrapper(r_pivot,rpiv,err=rpiv_sd,units="m")
                # THETA_PIVOT
                tpiv, tpiv_sd = mds.Float64(np.deg2rad(df.theta_pivot[0])), mds.Float64(0)
                mdsbuild.put_wrapper(theta_pivot,tpiv,err=tpiv_sd,units="rad")
                # PHI_PIVOT
                ppiv, ppiv_sd = mds.Float64(np.deg2rad(df.phi_pivot[0])), mds.Float64(0)
                mdsbuild.put_wrapper(phi_pivot,ppiv,err=ppiv_sd,units="rad")
                # R0
                r0dat,r0dat_sd = mds.Float64(df.r0[0]), mds.Float64(0.002)
                mdsbuild.put_wrapper(r0,r0dat,err=r0dat_sd,units="m")
                # DELTA_THETA
                dtheta, dtheta_sd = mds.Float64(np.deg2rad(df.delta_theta[0])), mds.Float64(np.deg2rad(.2))
                mdsbuild.put_wrapper(delta_theta,dtheta,err=dtheta_sd,units="rad")
                # Bdot_1_dir
                bdot1dir = mds.String(df.bdot_1_dir[0])
                mdsbuild.put_wrapper(bdot_1_dir,bdot1dir)
                print("upload of mysql info complete shot {0}".format(shot))

def process_data(top_node,shot):
    wipal_tree = mds.Tree("wipal",shot)
    data_node = wipal_tree.getNode("raw.trex_cell_1.data")
    oversamp = wipal_tree.getNode("raw.trex_cell_1.oversampfact")
    wipal_tree.quit()
    print(oversamp)
    time = data_node.dim_of().data()
    data = data_node.data().T

    b1_chs = (0,1)
    b2_chs = (2,3)
    b3_chs = (4,5)
    s1v_ch = 6
    s1i_ch = 7
    s2v_ch = 8
    s2i_ch = 9
    d3v_ch = 10
    d3i_ch = 11
    d4v_ch = 12
    d4i_ch = 13
    tp_ch = 15
    thresh = 0.02

    # Build rotation matrix r and error to transform to machine frame
    dtheta = top_node.getNode("delta_theta").getData()
    theta_pivot = top_node.getNode("theta_pivot").getData()
    beta = dtheta.data() + theta_pivot.data()
    beta_sd = np.sqrt(dtheta.getError().data()**2 + theta_pivot.getError().data()**2)
    r = np.array([[np.cos(beta),0,np.sin(beta)],[0,1,0],[-np.sin(beta),0,np.cos(beta)]],dtype="float64")
    r_sd = np.sqrt(np.array([[np.sin(beta)**2,0,np.cos(beta)**2],[0,0,0],[np.cos(beta)**2,0,np.sin(beta)**2]],dtype="float64"))*beta_sd
    # Build matrix R with associated error to store in MDSplus
    bdot_1_dir = top_node.getNode("bdot_1_dir").data().lower()
    if bdot_1_dir == "north":
        ct,st = np.cos(np.pi),np.sin(np.pi)
    elif bdot_1_dir == "south":
        ct,st = np.cos(0),np.sin(0)
    elif bdot_1_dir == "east":
        ct,st = np.cos(-np.pi/2),np.sin(-np.pi/2)
    elif bdot_1_dir == "west":
        ct,st = np.cos(np.pi/2),np.sin(np.pi/2)

    spin = np.array([[ct,-st,0],[st,ct,0],[0,0,1]])
    rspin = r.dot(spin)
    rspin_sd = r_sd
    print(r)
    print(spin)
    print(rspin)

    bdot_1 = offsub(data[b1_chs[0]] - data[b1_chs[1]])
    bdot_2 = offsub(data[b2_chs[0]] - data[b2_chs[1]])
    bdot_3 = offsub(data[b3_chs[0]] - 1.022*data[b3_chs[1]])
    bdot_trans = rspin.dot(np.vstack((bdot_1,bdot_2,bdot_3)))
    bdot_1 = mds.Signal(mds.Float32Array(bdot_1),None,time)
    bdot_2 = mds.Signal(mds.Float32Array(bdot_2),None,time)
    bdot_3 = mds.Signal(mds.Float32Array(bdot_3),None,time)
    bdot_x = mds.Signal(mds.Float32Array(bdot_trans[0,:]),None,time)
    bdot_y = mds.Signal(mds.Float32Array(bdot_trans[1,:]),None,time)
    bdot_z = mds.Signal(mds.Float32Array(bdot_trans[2,:]),None,time)

    spiral_1_v = mds.Signal(mds.Float32Array(offsub(200*data[s1v_ch,:])),None,time)
    spiral_1_i = mds.Signal(mds.Float32Array(offsub(data[s1i_ch,:]/20.0)),None,time)
    spiral_2_v = mds.Signal(mds.Float32Array(offsub(200*data[s2v_ch,:])),None,time)
    spiral_2_i = mds.Signal(mds.Float32Array(offsub(data[s2i_ch,:]/1.12)),None,time)
    double_3_v = mds.Signal(mds.Float32Array(offsub(200*data[d3v_ch,:])),None,time)
    double_3_i = mds.Signal(mds.Float32Array(offsub(data[d3i_ch,:]/20.0)),None,time)
    double_4_v = mds.Signal(mds.Float32Array(offsub(200*data[d4v_ch,:])),None,time)
    double_4_i = mds.Signal(mds.Float32Array(offsub(data[d4i_ch,:]/10.0)),None,time)
    timing_pulse= mds.Signal(mds.Float32Array(offsub(data[tp_ch,:])),None,time)

    mdsbuild.put_wrapper(top_node.getNode("bdot_1"),bdot_1,units="V")
    mdsbuild.put_wrapper(top_node.getNode("bdot_2"),bdot_2,units="V")
    mdsbuild.put_wrapper(top_node.getNode("bdot_3"),bdot_3,units="V")
    mdsbuild.put_wrapper(top_node.getNode("bdot_x"),bdot_x,units="V")
    mdsbuild.put_wrapper(top_node.getNode("bdot_y"),bdot_y,units="V")
    mdsbuild.put_wrapper(top_node.getNode("bdot_z"),bdot_z,units="V")
    mdsbuild.put_wrapper(top_node.getNode("spiral_1_v"),spiral_1_v,units="V")
    mdsbuild.put_wrapper(top_node.getNode("spiral_1_i"),spiral_1_i,units="A")
    mdsbuild.put_wrapper(top_node.getNode("spiral_2_v"),spiral_2_v,units="V")
    mdsbuild.put_wrapper(top_node.getNode("spiral_2_i"),spiral_2_i,units="A")
    mdsbuild.put_wrapper(top_node.getNode("double_3_v"),double_3_v,units="V")
    mdsbuild.put_wrapper(top_node.getNode("double_3_i"),double_3_i,units="A")
    mdsbuild.put_wrapper(top_node.getNode("double_4_v"),double_4_v,units="V")
    mdsbuild.put_wrapper(top_node.getNode("double_4_i"),double_4_i,units="A")
    mdsbuild.put_wrapper(top_node.getNode("timing"),timing_pulse,units="V")
    print("upload to spiral_probe for shot {0} complete!".format(shot))

def main(shot):
    spiral_tree = mds.Tree("spiral_probe",shot)
    sp_top = spiral_tree.getNode("\\top")
    try:
        sql_log(sp_top,shot)
    except:
        raise
        print("Not processing spiral_probe data for shot {0:d}, set top node to off".format(shot))
        sp_top.setOn(False)
    else:
        try:
            process_data(sp_top,shot)
            spiral_tree.cleanDatafile()
            spiral_tree.quit()
        except:
            raise
            print("error processing shot {0}".format(shot))

if __name__ == "__main__":
    shot = int(sys.argv[1])
    main(shot)
