import MDSplus as mds
import matplotlib.pyplot as plt
import modules.mds_builders as mdsbuild
from analysis.datahelpers import smooth
from scipy.integrate import cumtrapz
from scipy.optimize import fmin
import numpy as np
import sys

shot = int(sys.argv[1])
wipal_tree = mds.Tree("wipal",shot)
data_node = wipal_tree.getNode("raw.trex_cell_1.data")
data = data_node.data().T
time = data_node.dim_of().data()
n = len(time)

def offsub(sig,sig_side="front",sig_frac=.1):
    if sig_side.lower() in ["front", "beginning"]:
        return sig - np.mean(sig[0:int(len(sig)*sig_frac)])
    elif sig_side.lower() in ["back","end"]:
        return sig - np.mean(sig[(len(sig)*(1-sig_frac)):])
    else:
        raise ValueError("invalid option for keyword arg sig_side, must be from [front, beginning, back, end]")

def func(a,sig1,sig2):
    return np.mean(sig1 - a[0]*sig2)**2

fig,axes = plt.subplots(4,4,sharex=True,sharey=True)
for i,ax in enumerate(axes.flatten()):
#    print i
##    if i > 2:
##        break
#    sig1 = data[i*2,:]
#    sig2 = data[i*2+1,:]
##    a_vec = np.linspace(.7,1.5,1000)
##    means = np.zeros(len(a_vec))
##    for j,a in enumerate(a_vec):
##        means[j] = np.mean(sig1-a*sig2)
##    ax.plot(a_vec,means)
#    xopt,fopt,_,_,_ = fmin(func,[1.0],args=(sig1,sig2),ftol=0.1,full_output=1)
#    sig = sig1-xopt*sig2
#    print xopt
##    ax.plot(time,sig1)
##    ax.plot(time,-sig2)
##    ax.plot(time,sig1-sig2)
##    ax.plot(time,cumtrapz(sig,initial=0.0))
#    ax.plot(time,sig1)
#    ax.plot(time,sig2)
#    ax.set_title(i)
    sig1 = data[i,:]-0*data[12,:]
    if i == 5:
        ax.plot(sig1-smooth(sig1,10000))
    else:
        ax.plot(sig1)
    ax.set_title(i)

plt.show()



