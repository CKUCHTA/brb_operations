import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft,fftfreq,fftshift
from scipy.signal import spectrogram, welch
from analysis.datahelpers import *
import MDSplus as mds
import sys
import plottingtools.plottingtools as ptools


#def fft_helper(sig, fs=1.0,window=None,fold=True):
#    n = len(sig)
#    sig_fft = fftshift(fft(sig))
#    sig_freqs = fftshift(fftfreq(n,d=1.0/fs))
#    if fold:
#        return sig_fft[n/2:], sig_freqs[n/2:]
#    else:
#        return sig_fft, sig_freqs

def fft_helper(sig, fs=1.0,window=None,bins=2,fold=True):
    n = len(sig)
    d = n / bins
    print(d)
    sig_fft = 0
    for i in range(bins):
        sig_fft += np.abs(fftshift(fft(sig[i*d:(i+1)*d]-np.mean(sig[i*d:(i+1)*d]))))
    sig_freqs = fftshift(fftfreq(d,d=1.0/fs))
    if fold:
        return sig_fft[d/2:], sig_freqs[d/2:]
    else:
        return sig_fft, sig_freqs

shot = int(sys.argv[1])
Sn = 0
Sbdot = 0
Shall = 0
Shall1 = 0
for shot in range(32130,32140):
    print(shot)
    tree = mds.Tree("wipal",shot)
    sigbdot = smooth(tree.getNode("\\spiral_probe_bdot_2").data(),4)
    time20M = tree.getNode("\\spiral_probe_bdot_2").dim_of().data()
    sign = smooth(tree.getNode("\\spiral_probe_spiral_2_i").data(),4)
    sighall = tree.getNode("magnetics.linear_hall3.hp_09.by").data()
    time400k = tree.getNode("magnetics.linear_hall3.hp_09.by").dim_of().data()
    fs20M = 20000000
    fs400k = 400000
    t020,t120 = np.abs(time20M-.0085).argmin(), np.abs(time20M-.0145).argmin()
    t0400,t1400 = np.abs(time400k-.0085).argmin(), np.abs(time400k-.0145).argmin()
    hallfreqs,hallfft = welch(sighall[t0400:t1400]-np.mean(sighall[t0400:t1400]),fs=fs400k, nperseg=512,scaling="density")
#    bdotfreqs,bdotfft = welch(sigbdot[t020:t120]-np.mean(sigbdot[t020:t120]),fs=fs20M,nperseg=16384,scaling="density")
#    nfreqs,nfft = welch(sign[t020:t120]-np.mean(sign[t020:t120]),fs=fs20M,nperseg=16384,scaling="density")
    bdotfreqs,bdotfft = welch(sigbdot[t020:t120]-np.mean(sigbdot[t020:t120]),fs=fs20M,nperseg=32768,scaling="density")
    nfreqs,nfft = welch(sign[t020:t120]-np.mean(sign[t020:t120]),fs=fs20M,nperseg=32768,scaling="density")
    Shall += .1*np.abs(hallfft)/np.max(np.abs(hallfft))
    temp = bdotfft/(2*np.pi*bdotfreqs)
    temp_max = np.max(temp[np.isfinite(temp)])
    print(temp_max)
    Sbdot += .1*temp/temp_max
    Sn += .1*np.abs(nfft)/np.max(np.abs(nfft))
#    plt.plot(hallfreqs,Shall,"r")
#    plt.plot(bdotfreqs,Sbdot,"g")
#    plt.plot(bdotfreqs,Sn,"b")
#    plt.show()
    #f20,t,S = spectrogram(sig[-np.mean(sig),fs=fs,nperseg=256,detrend="constant")
    #plt.plot(f,np.mean(S[:,t0:t1],axis=1))
#    Sxx += 0.1*np.mean(S[:,t0:t1],axis=1)
fig,ax = plt.subplots()
ax.loglog(hallfreqs,Shall,"r",lw=1.5)
ax.loglog(bdotfreqs,Sbdot,"g",lw=1.5)
ax.loglog(bdotfreqs,Sn,"b",lw=1.5)
ax.set_xlim(1E3,1E6)
ax.set_ylim(1E-7,1E0)
ax.set_ylabel("PSD (Arb)")
ax.set_xlabel("Frequency (Hz)")
ax.axvspan(3000,6000,fc="y",alpha=.4)
ax.axvspan(17E3,24E3,fc="y",alpha=.4)
ax.axvspan(120E3,200E3,fc="y",alpha=.4)
leg = plt.legend(["Hall Probe (BW 100kHz)","Bdot Probe","Isat Probe"])
leg.draggable()
ptools.makePlot(fig,[ax])
plt.show()

