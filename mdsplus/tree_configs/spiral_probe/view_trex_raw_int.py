import MDSplus as mds
import matplotlib.pyplot as plt
import modules.mds_builders as mdsbuild
from scipy.integrate import cumtrapz
import numpy as np
import sys

shot = int(sys.argv[1])
wipal_tree = mds.Tree("wipal",shot)
data_node = wipal_tree.getNode("raw.trex_cell_1.data")
data = data_node.data().T
time = data_node.dim_of().data()
print(data.shape)

def offsub(sig,sig_side="front",sig_frac=.1):
    if sig_side.lower() in ["front", "beginning"]:
        return sig - np.mean(sig[0:int(len(sig)*sig_frac)])
    elif sig_side.lower() in ["back","end"]:
        return sig - np.mean(sig[(len(sig)*(1-sig_frac)):])
    else:
        raise ValueError("invalid option for keyword arg sig_side, must be from [front, beginning, back, end]")

fig,axes = plt.subplots(4,4)
print(axes.shape)
for i,ax in enumerate(axes.flatten()):
    ax.plot(time,cumtrapz(offsub(data[i,:]),initial=0.0))
    ax.set_title(i)

plt.show()



