import MDSplus as mds
import matplotlib.pyplot as plt
import modules.mds_builders as mdsbuild
from scipy.integrate import cumtrapz
import numpy as np
import sys

shot = int(sys.argv[1])
wipal_tree = mds.Tree("wipal",shot)
bdot1 = wipal_tree.getNode("\\spiral_probe_bdot_x")
bdot2 = wipal_tree.getNode("\\spiral_probe_bdot_y")
bdot3 = wipal_tree.getNode("\\spiral_probe_bdot_z")
time = bdot1.dim_of().data()
bdot1 = bdot1.data()
bdot2 = bdot2.data()
bdot3 = bdot3.data()

fig,axes = plt.subplots(3)
axes[0].plot(time,cumtrapz(bdot1,initial=0.0))
axes[1].plot(time,cumtrapz(bdot2,initial=0.0))
axes[2].plot(time,cumtrapz(bdot3,initial=0.0))


plt.show()



