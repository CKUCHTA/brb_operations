import MDSplus as mds
import modules.mds_builders as mdsbuild


n = "numeric"
s = "signal"
t = "text"
st = "structure"

shots = list(range(31738,31851))+[-1]
for shot in shots:
    print(shot)
    sp_tree = mds.Tree("spiral_probe",shot)
    top = sp_tree.getNode("\\top")
    sp_tree.edit()
    nodes_to_add = [("bdot_1_dir",t,"bdot_1_dir")]
    #nodes_to_add = [("bdot_x",s,"bdot_x"),("bdot_y",s,"bdot_y"),("bdot_z",s,"bdot_z"),("delta_theta",n,"delta_theta"),
#            ("r_pivot",n,"r_pivot"),("theta_pivot",n,"theta_pivot"),("phi_pivot",n,"phi_pivot")]
    ## create top level magnetics data nodes
    for (tn,use,tag) in nodes_to_add:
        print(tn)
        node = sp_tree.addNode(tn,use)
        if tag is not None:
            node.addTag("spiral_probe_"+tag)

    sp_tree.write()
    sp_tree.cleanDatafile()
    sp_tree.quit()
