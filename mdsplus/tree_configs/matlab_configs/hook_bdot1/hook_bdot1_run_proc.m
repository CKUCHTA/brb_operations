function hook_bdot1_run_proc(shots)
% takes shot number or array of CONTINUOUS shot numbers and uploads raw data to
% MDSplus


%% make diary log file

funcfpath = fileparts(mfilename('fullpath'));
logfile = fullfile(funcfpath, 'putlog.txt');
logid = fopen(logfile, 'wt');



%% shot info

% login credentials
mdsip = 'localhost';
sql_dbase = 'skywalker';
sql_user = 'public';
sql_pswd = 'public';


try
    conn = database(sql_dbase, sql_user, sql_pswd);
    sqlgood = 1;
    % radial location and clocking of shaft from sql database
    sqlquery = sprintf('select shot_num,sh_theta,sh_r from TREX_data where shot_num between %d and %d',shots(1),shots(end));
    parameters = select(conn, sqlquery);
    close(conn);
catch
    fprintf(logid,'Error connecting to SQL database. Continuing to populate raw data nodes\n');
    sqlgood = 0;
end


try
    mdsconnect(mdsip);
catch
    fprintf(logid,'Error connecting to MDSplus\n');
    return
end



%% grab config file data

% config = './config/hook_bdot1_config.ini';
config = fullfile(funcfpath, 'config', 'hook_bdot1_config.ini');
I = INI('File',config);
I.read();


%% upload data to MDSplus

nshots = length(shots);
for nni = 1:nshots
    
    shotnum = shots(nni);
    fprintf(1, 'Shot %d of %d\n', nni, nshots);
    
    fprintf(logid, 'Uploading hook probe data: Shot %d\n', shotnum);
    mdsopen('wipal',shotnum);
    
    %% db nodes
    axnames = {'db1', 'db2a', 'db2b', 'db3'};
    numPbs = 15;
    for pbnum1 = 1:numPbs
        
        pb = sprintf('pb%02d', pbnum1);
        fprintf(logid, '    Adding probe %02d raw data\n', pbnum1);
        
        for axn = 1:4
            
            ax = axnames{axn};
            fail = []; % fail strings
            
            % gain data
            gainNdName = sprintf('magnetics.hook_bdot1.%s.%s.gain', pb, ax);
            gainData = I.(pb).(ax).gain;
            gainUnit = I.(pb).(ax).gainunit;
            gainStr = 'build_with_units($1,$2)';
            gainout = mdsput(gainNdName, gainStr, gainData, gainUnit);
            if ~mod(gainout,2)
                failold = fail;
                fail = [failold ', ' gainNdName];
            end
            
            % raw data
            rawNdName = sprintf('magnetics.hook_bdot1.%s.%s.raw', pb, ax);
            %         rawStr = 'build_signal($1,*,dim_of($1))';
            rawData = I.(pb).(ax).raw;
            rawout = mdsput(rawNdName, rawData);
            if ~mod(rawout,2)
                failold = fail;
                fail = [failold ', ' rawNdName];
            end
        end
        
        if ~isempty(fail)
            fprintf(logid,'        Failure on nodes: %s\n',fail);
        end 
    end
    
    %% Fix bad channels
    %   db2a(3) --> db2b(3)
    %   db1(2), db1(7) --> ave(db1(1,3)), ave(db1(6,8))
    %   db3(5) --> ave(db3(4,6))
    fprintf(logid,'    Adding probe fixes\n');
    
    fail = []; % fail strings
    
    % db1 signals
    badNd = '\hook_bdot1_pb02_db1';
    rep1 = '\hook_bdot1_pb01_db1';
    rep2 = '\hook_bdot1_pb03_db1';
    repsig = sprintf('BUILD_SIGNAL(BUILD_WITH_UNITS((%1$s+%2$s)/2,UNITS(%1$s)),,DIM_OF(%1$s))',rep1,rep2);
    fixput = mdsput(badNd, repsig);
    if ~mod(fixput,2)
        failold = fail;
        fail = [failold ', ' badNd];
    end
    
    badNd = '\hook_bdot1_pb07_db1';
    rep1 = '\hook_bdot1_pb06_db1';
    rep2 = '\hook_bdot1_pb08_db1';
    repsig = sprintf('BUILD_SIGNAL(BUILD_WITH_UNITS((%1$s+%2$s)/2,UNITS(%1$s)),,DIM_OF(%1$s))',rep1,rep2);
    fixput = mdsput(badNd, repsig, rep1, rep2);
    if ~mod(fixput,2)
        failold = fail;
        fail = [failold ', ' badNd];
    end
    
    % db2a signal
    badNd = '\hook_bdot1_pb03_db2a';
    rep1 = '\hook_bdot1_pb03_db2b';
%     repsig = sprintf('BUILD_SIGNAL(BUILD_WITH_UNITS(%1$s,UNITS(%1$s)),,DIM_OF(%1$s))',rep1);
%     fixput = mdsput(badNd, repsig, rep1);
    fixput = mdsput(badNd, rep1);
    if ~mod(fixput,2)
        failold = fail;
        fail = [failold ', ' badNd];
    end
    
    % db3 signal
    badNd = '\hook_bdot1_pb05_db3';
    rep1 = '\hook_bdot1_pb04_db3';
    rep2 = '\hook_bdot1_pb06_db3';
    repsig = sprintf('BUILD_SIGNAL(BUILD_WITH_UNITS((%1$s+%2$s)/2,UNITS(%1$s)),,DIM_OF(%1$s))',rep1,rep2);
    fixput = mdsput(badNd, repsig, rep1, rep2);
    if ~mod(fixput,2)
        failold = fail;
        fail = [failold ', ' badNd];
    end
    
    if ~isempty(fail)
        fprintf(logid,'        Failure on nodes: %s\n',fail);
    end
    
    
    
    if sqlgood
        %% port configuration data nodes
        fprintf(logid,'    Adding port configuration data\n');
        
        % port info
        rport = I.port.rport;
        lat = I.port.lat;
        long = I.port.long;
        alpha = I.port.alpha;
        beta = I.port.beta;
        gamma = I.port.gamma;
        pii = find(parameters.shot_num==shotnum);
        probeRot = round((parameters.sh_theta(pii)), 2); % from motor control VI
        r_shaft = round((parameters.sh_r(pii)), 2)*1e-2; % from motor control VI
        insert = rport*cosd(alpha) - r_shaft; % insertion depth (will come from motor control VI)
        
        ptNames = {'rport', 'lat', 'long', 'alpha', 'beta', 'gamma', 'clock', 'insert'};
        ptUnits = {'m', 'deg', 'deg', 'deg', 'deg', 'deg', 'deg', 'm'};
        matDats = {rport, lat, long, alpha, beta, gamma, probeRot, insert};
        for porti = 1:length(ptNames)
            
            nms = ptNames{porti};
            un = ptUnits{porti};
            
            ndName = ['\hook_bdot1_' nms];
            putStr = sprintf( 'BUILD_WITH_UNITS($1,"%s")', un);
            matData = matDats{porti};
            
            out = mdsput(ndName, putStr, matData);
            fail = []; % fail strings
            if ~mod(out,2)
                failold = fail;
                fail = [failold ', ' ndName];
            end
        end
        
        if ~isempty(fail)
            fprintf(logid,'        Failure on nodes: %s\n',fail);
        end
        
        
        %% probe coordinates (adapted from HookCoordTrans)
        fprintf(logid,'    Adding probe coordinate data\n');
        
        % local Z location for each probe
        tipAlumina = 83.8;
        probeSpace = 2.17*2.54;
        tip1 = tipAlumina - 1 - 2.65; % middle of triplet 1
        probeZ = tip1 - probeSpace*(0:numPbs-1);
        probeZ = probeZ*1e-2; % cm to m        
        
        % rotation matrix for probe to port(defined as CCW rotation)
        Ry90 = [cosd(-90) 0 -sind(-90); 0 1 0; sind(-90) 0 cosd(-90)];
        RxRot = [1 0 0; 0 cosd(probeRot) -sind(probeRot); 0 sind(probeRot) cosd(probeRot)];
        R1 = Ry90*RxRot;
        
        % rotation matrix of port
        Rx = [1 0 0; 0 cosd(beta) -sind(beta); 0 sind(beta) cosd(beta)];
        Ry = [cosd(alpha) 0 -sind(alpha); 0 1 0; sind(alpha) 0 cosd(alpha)];
        Rz = [cosd(gamma) -sind(gamma) 0; sind(gamma) cosd(gamma) 0; 0 0 1];
        R2 = Rx*Ry*Rz;
        
        % NED transformation
        R3 = [-sind(lat)*cosd(long) -sind(long) -cosd(lat)*cosd(long); ...
            -sind(lat)*sind(long) cosd(long) -cosd(lat)*sind(long); ...
            cosd(lat) 0 -sind(lat)];
        
        % combined matrices
        M1 = R3*R2;
        M2 = M1*R1;
        
        % M2*[0 0 probeZ]'+M1*[0 0 insert]'+vport
        x = M2(1,3).*probeZ + M1(1,3)*insert + rport*cosd(lat)*cosd(long);
        y = M2(2,3).*probeZ + M1(2,3)*insert + rport*cosd(lat)*sind(long);
        z = M2(3,3).*probeZ + M1(3,3).*insert + rport*sind(lat);
        
        r = sqrt(x.^2 + y.^2); % cylindrical r coordinate
        phi = atan2d(y,x); % toroidal coordinate (degrees)
        
        for pbnum2 = 1:numPbs
            crds = {'r','phi','z'};
            crdUnits = {'m','deg','m'};
            crdDat = {r(pbnum2), phi(pbnum2), z(pbnum2)};
            fail = []; % fail strings
            for crdi = 1:3
                ndName = sprintf('\\hook_bdot1_pb%02d_%s',pbnum2,crds{crdi});
                ndput = sprintf('BUILD_WITH_UNITS($1,"%s")',crdUnits{crdi});
                ndout = mdsput(ndName, ndput, crdDat{crdi});
                if ~mod(ndout,2)
                    failold = fail;
                    fail = [failold ', ' ndName];
                end
            end
            if ~isempty(fail)
                fprintf(logid,'        Failure on nodes: %s\n',fail);
            end
        end
        
        
        
        %% probe signal transformations (adapted from HookCoordTrans)
        fprintf(logid,'    Adding transformed probe signals\n');
        
        % cart to polar transformation matrix based on probe position
        for pbnum3 = 1:numPbs
            Rpol = [ cosd(phi(pbnum3)) sind(phi(pbnum3)) 0; ...
                    -sind(phi(pbnum3)) cosd(phi(pbnum3)) 0; ...
                    0 0 1];
            M3 = Rpol*M2;
            % equation: M3*[db1 db2 db3]'
            
            rndName = sprintf('\\hook_bdot1_pb%02d_dbr',pbnum3);
            phindName = sprintf('\\hook_bdot1_pb%02d_dbphi',pbnum3);
            zndName = sprintf('\\hook_bdot1_pb%02d_dbz',pbnum3);
            dirndNames = {rndName, phindName, zndName};
            
            db1 = sprintf('\\hook_bdot1_pb%02d_db1',pbnum3);
            db2a = sprintf('\\hook_bdot1_pb%02d_db2a',pbnum3);
            db2b = sprintf('\\hook_bdot1_pb%02d_db2b',pbnum3);
            db3 = sprintf('\\hook_bdot1_pb%02d_db3',pbnum3);
            
%             sig = 'BUILD_SIGNAL(BUILD_WITH_UNITS($5*DATA($1)+$6*(DATA($2)+DATA($3))/2+$7*DATA($4),"T/s"),,DIM_OF($1))';
            sig = sprintf('BUILD_SIGNAL(BUILD_WITH_UNITS($1*%1$s+$2*(%2$s+%3$s)/2+$3*%4$s,"T/s"),,DIM_OF(%1$s))',db1,db2a,db2b,db3);
            
            fail = []; % fail strings
            for dirii = 1:3
                dirndName = dirndNames{dirii};
                dirout = mdsput(dirndName, sig, M3(dirii,1), M3(dirii,2), M3(dirii,3));
                if ~mod(dirout,2)
                    failold = fail;
                    fail = [failold ', ' dirndName];
                end
            end
            if ~isempty(fail)
                fprintf(logid,'        Failure on nodes: %s\n',fail);
            end
        end
    end
    
    mdsclose;
    
end



%% clean up

mdsdisconnect;
fclose(logid);




























