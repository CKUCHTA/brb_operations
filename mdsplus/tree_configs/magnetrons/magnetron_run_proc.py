#!/opt/anaconda2/bin/python
import numpy as np
import pandas as pio
import sqltools.core as sql
import MDSplus as mds
import modules.mds_builders as mdsbuild
from modules.mysql_connection import get_engine
import pandas
import argparse

"""
I suggest that I try to fill in one magnetron at a time and throw an error per magnetron that is off.  Right now, straight copying from 
Ethan will lead to errors for all magnetrons even if some are on.
"""
def main(shot_num):
    wipal_tree = mds.Tree("wipal", shot_num)
    mag_top = wipal_tree.getNode("discharge.magnetrons")
    magnetrons = ['magnetron_{0:d}'.format(x) for x in range(1,3)]
    for mag in range(1,3):
        mag_node = mag_top.getNode(magnetrons[mag-1])
        sql_log(mag_node, mag, shot_num)
        calculate_magnetron_offsets(mag_node)

def sql_log(top_node, magnetron, shot_num):
    cnx = sql.start_cnx(filename="/home/WIPALdata/sql_config.ini")
    sql_config_path = "/home/WIPALdata/sql_config.ini"
    engine = get_engine(sql_config_path)
    database = engine.url.database
    with engine.connect() as connection:
        sql_query = 'select * from {0}.magnetron{2:d} where shot_num = {1:d}'.format(database, shot_num, magnetron)

        try:
            df = pandas.read_sql(sql_query, connection)
        except:
            raise Exception("Failed to read MySQL table {0}.magnetron{2:d} for shot {1}".format(database, shot_num, magnetron))
        else:
            if df.status[0] == "off":
                #raise Exception("MySQL table {0}.magnetron{2:d} status is 'off' for shot {1}".format(database, shot_num, magnetron))
                print("MySQL table {0}.magnetron{2:d} status is 'off' for shot {1}".format(database, shot_num, magnetron))
            else:
                node = top_node.getNode("forward")
                A, B, C, atten, raw = mdsbuild.get_node_refs(node, "A", "B", "C", "atten", "v_raw")
                raw_node_string = "\\TOP.RAW.{0}:ch_{1:02d}".format(df.F_digitizer[0], df.F_ch[0]).upper()

                mdsbuild.put_wrapper(A, mds.Float32(df.A_F[0]), err=mds.Float32(df.A_F_sd[0]), units="mW/V^2")
                mdsbuild.put_wrapper(B, mds.Float32(df.B_F[0]), err=mds.Float32(df.B_F_sd[0]), units="mW/V")
                mdsbuild.put_wrapper(C, mds.Float32(df.C_F[0]), err=mds.Float32(df.C_F_sd[0]), units="mW")
                mdsbuild.put_wrapper(atten, mds.Float32(df.atten_F[0]), err=mds.Float32(df.atten_F_sd[0]), units="dB")
                raw.putData(mds.TreePath(raw_node_string))

                node = top_node.getNode("reflected")
                A, B, C, atten, raw = mdsbuild.get_node_refs(node, "A", "B", "C", "atten", "v_raw")
                raw_node_string = "\\TOP.RAW.{0}:ch_{1:02d}".format(df.R_digitizer[0], df.R_ch[0]).upper()

                mdsbuild.put_wrapper(A, mds.Float32(df.A_R[0]), err=mds.Float32(df.A_R_sd[0]), units="mW/V^2")
                mdsbuild.put_wrapper(B, mds.Float32(df.B_R[0]), err=mds.Float32(df.B_R_sd[0]), units="mW/V")
                mdsbuild.put_wrapper(C, mds.Float32(df.C_R[0]), err=mds.Float32(df.C_R_sd[0]), units="mW")
                mdsbuild.put_wrapper(atten, mds.Float32(df.atten_R[0]), err=mds.Float32(df.atten_R_sd[0]), units="dB")
                raw.putData(mds.TreePath(raw_node_string))

def calculate_magnetron_offsets(top_node, npts=100):
    node_names = ["forward", "reflected"]
    for name in node_names:
        node = top_node.getNode(name)

        raw_node, offset_node = mdsbuild.get_node_refs(node, "v_raw", "offset")
        raw_data = raw_node.data()

        offset = np.mean(raw_data[0:npts])
        offset_sd = np.std(raw_data[0:npts])

        mdsbuild.put_wrapper(offset_node, mds.Float32(offset), err=mds.Float32(offset_sd), units="V")



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Reads sql data and fills in necessary data in the magnetron MDSplus tree.")
    parser.add_argument("shot_num", metavar="Shot Number", type=int, help="Shot number to fill in MDSplus tree")
    args = parser.parse_args()

    main(args.shot_num)
