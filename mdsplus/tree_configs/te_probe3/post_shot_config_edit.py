"""
These are functions needed to edit the config object after we take a shot.
They don't actually edit the config file and instead add/remove information
that is needed when writing to the tree.
"""
import logging
import numpy as np
import modules.coordinate_transforms as coords
from modules.mysql_connection import get_engine
import pandas
import MDSplus as mds


def read_sql(shot_number, sql_config_path="/home/WIPALdata/sql_config.ini"):
    """
    Read the probe position from the database for the queried shot.

    Parameters
    ----------
    shot_number : int
        The number of the shot we want the probe position for.
    sql_config : str, default='/home/WIPALdata/sql_config.ini'
        Path to config file containing sql info for database.

    Returns
    -------
    query_result : QueryResult
        Object containing the returned values from the database.
    """
    logging.debug("Reading from MySQL database te probe info for shot {num}.".format(num=shot_number))
    engine = get_engine(sql_config_path)
    database = engine.url.database
    with engine.connect() as connection:
        sql_query = "SELECT insertion, clocking, latitude, longitude, alpha, beta, gamma, port_radius FROM {database}.te_probe3 where shot_num={num}".format(database=database, num=shot_number)

        logging.debug("Executing query '{query}'.".format(query=sql_query))
        try:
            result = pandas.read_sql(sql_query, connection)
        except Exception:
            logging.exception("Failure executing query '{query}'.".format(query=sql_query))
            raise
        else:
            logging.info("Executed query successfully.")

    if result.size == 0:
        logging.exception("Could not find values for shot {}.".format(shot_number))
        raise ValueError("Missing row in MySQL '{table}' table for shot {num}.".format(table='te_probe3', num=shot_number))

    class QueryResult:
        # Change the query result into an object for easy access.
        def __init__(self, query_result):
            # Set the value of each variable to that of the result.
            self.insertion, self.clocking_deg, self.latitude_deg, self.longitude_deg, self.alpha_deg, self.beta_deg, self.gamma_deg, self.port_radius = query_result.iloc[0]

            # Change all variables to radians.
            self.clocking = np.deg2rad(self.clocking_deg)
            self.latitude = np.deg2rad(self.latitude_deg)
            self.longitude = np.deg2rad(self.longitude_deg)
            self.alpha = np.deg2rad(self.alpha_deg)
            self.beta = np.deg2rad(self.beta_deg)
            self.gamma = np.deg2rad(self.gamma_deg)

    # Extract the values from the call.
    query_result = QueryResult(result)
    logging.debug("Got values from row:\n{}".format(result))
    return query_result

def get_cartesian_bdot_nodes(te_top_node):
    """
    Get x, y, and z bdot nodes.
    """
    bdot_node = te_top_node.getNode("bdot")
    return (bdot_node.getNode("ax1"), bdot_node.getNode("ax2"), bdot_node.getNode("ax3"))

def get_bdot_equations(mysql_query, probe_cartesian_position, bdot_axial_nodes):
    """
    Get the equations for calculating bdotr, bdotphi, and bdotz.
    
    Parameters
    ----------
    mysql_query : QueryResult
        Query object containing mysql query values.
    probe_cartesian_position : np.array[float]
        Position of probe in machine cartesian coordinates.
    bdot_axial_nodes : iter[mds.TreeNode]
        Iterable of nodes in the probe directions (x, y, z).

    Returns
    -------
    compiled_equations : list[mds.Data]
        List of equations compiled by mds for the [r, phi, z] directions.
    """
    # Start with the bdot directions in probe coordinates.
    bdot_x = np.array((1, 0, 0))
    bdot_y = np.array((0, 1, 0))
    bdot_z = np.array((0, 0, 1))

    # Change this direction into machine cartesian coordinates.
    bdot_x = coords.direction_to_machine_cartesian(mysql_query.longitude, mysql_query.latitude, 
        mysql_query.alpha, mysql_query.beta, mysql_query.gamma,
        bdot_x[0], bdot_x[1], bdot_x[2], mysql_query.clocking
    )
    bdot_y = coords.direction_to_machine_cartesian(mysql_query.longitude, mysql_query.latitude, 
        mysql_query.alpha, mysql_query.beta, mysql_query.gamma,
        bdot_y[0], bdot_y[1], bdot_y[2], mysql_query.clocking
    )
    bdot_z = coords.direction_to_machine_cartesian(mysql_query.longitude, mysql_query.latitude, 
        mysql_query.alpha, mysql_query.beta, mysql_query.gamma,
        bdot_z[0], bdot_z[1], bdot_z[2], mysql_query.clocking
    )

    # Change each machine cartesian into cylindrical. These now contain the contribution of bdot_i in the (r, phi, z) directions.
    bdot_x = coords.direction_cartesian_to_cylindrical(bdot_x, probe_cartesian_position)
    bdot_y = coords.direction_cartesian_to_cylindrical(bdot_y, probe_cartesian_position)
    bdot_z = coords.direction_cartesian_to_cylindrical(bdot_z, probe_cartesian_position)

    # Build the equations.
    raw_equations = ['BUILD_SIGNAL($1 * {} + $2 * {} + $3 * {},, DIM_OF($1))'.format(bdot_x[i], bdot_y[i], bdot_z[i]) for i in range(3)]
    compiled_equations = [mds.Data.compile(equation, *bdot_axial_nodes) for equation in raw_equations]

    logging.debug("Bdot cylindrical equations:\n" + "\n".join(str(equation) for equation in compiled_equations))
    return compiled_equations

def get_probe_cartesian_position(mysql_query):
    """
    Get the position of the probe in cartesian coordinates.
    """
    position =  coords.position_to_machine_cartesian(
        mysql_query.port_radius, mysql_query.longitude, mysql_query.latitude, 
        mysql_query.alpha, mysql_query.beta, mysql_query.gamma,
        0, 0, mysql_query.insertion, mysql_query.clocking
    )
    logging.debug("Probe cartesian position: {}".format(position))
    return position

def edit_config(config_object, shot_number, te_top_node):
    """
    Edit the config object before inserting data into the MDSplus tree.
    
    Parameters
    ----------
    config_object : dict[str, dict[...]]
        Object read from config file.
    shot_number : int
        The shot number we are evaluating.
    te_top_node : mds.TreeNode
        Top of the te probe tree.

    Returns
    -------
    config_object : dict[str, dict[...]]
    """
    logging.debug("Editing config object for shot {}.".format(shot_number))
    logging.debug("Removing 'start_shot' from config data since no need to insert into tree.")
    config_object.pop('start_shot')

    # Try to connect to the MySQL database.
    try:
        query = read_sql(shot_number)
    except ValueError as e:
        logging.info("Can't get probe positions for shot {} since no row in database. Error was:\n{}".format(shot_number, e))
        raise
    except Exception as e:
        logging.info("Caught error trying to read from MySQL. Can't get probe position information. Error was:\n{}".format(e))
        raise

    logging.debug("Adding probe position information.")
    config_object['port'] = {}
    config_object['port']['clock'] = query.clocking_deg

    probe_cartesian_position = get_probe_cartesian_position(query)
    probe_cylindrical_position = coords.position_cartesian_to_cylindrical(probe_cartesian_position)
    config_object['pos'] = {}
    config_object['pos']['r'] = probe_cylindrical_position[0]
    config_object['pos']['phi'] = probe_cylindrical_position[1]
    config_object['pos']['z'] = probe_cylindrical_position[2]

    logging.debug("Adding bdot equations.")
    equations = get_bdot_equations(query, probe_cartesian_position, get_cartesian_bdot_nodes(te_top_node))
    config_object["bdot"]["axr"] = equations[0]
    config_object["bdot"]["axphi"] = equations[1]
    config_object["bdot"]["axz"] = equations[2]

    logging.debug("Done editing config object.")
    return config_object
