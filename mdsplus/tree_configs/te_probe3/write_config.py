"""
Write/edit the config file used for the te_probe3 MDSplus tree.
"""
import logging
import json
import os.path
from collections import OrderedDict

# JSON data structure

# [
    # { (Start of data_to_write)
        # "start_shot": start_shot,
        # "act_area": (area, "m"),
        # "avg_index": {
            # "start": start_index,
            # "end": end_index
        # },
        # "bdot": {
            # "ax1": {
                # "gain": gain_factor,
                # "raw_cw": mds_channel
                # "raw_ccw": mds_channel
            # },
            # "ax2": {
                # "gain": gain_factor,
                # "raw_cw": mds_channel
                # "raw_ccw": mds_channel
            # },
            # "ax3": {
                # "gain": gain_factor,
                # "raw_cw": mds_channel
                # "raw_ccw": mds_channel
            # }
        # }
        # "port": {
            # "alpha": (alpha_value, "deg"),
            # "beta": (beta_value, "deg"),
            # "gamma": (gamma_value, "deg"),
            # "lat": (latitude_value, "deg"),
            # "long": (longitude_value, "deg"),
            # "rport": (port_radius_value, "m")
        # }
        # "tip01": {
            # "signal": {
                # "v": {
                    # "raw": mds_channel
                # }
            # },
            # "noise": {
                # "v": {
                    # "raw": mds_channel
                # }
            # },
            # "vgroupnoise": {
                # "raw": mds_channel,
                # "factor": factor_value,
                # "adjnoise": {
                    # "raw": mds_channel
                # }
            # },
            # "vbias": {
                # "raw": mds_channel,
                # "offset": offset_voltage,
                # "div": divider_factor
            # },
            # "rdiggnd": ((1/R_2 + 1/R_digitizer)**(-1), "ohm"),
            # "rdiv": ((R_1 + (1/R_2 + 1/R_digitizer)**(-1)), "ohm"),
            # "rsense": (sense_resistance, "ohm"),
            # "rwire": (wire_resistance, "ohm"),
            # "relarea": relative_area,
            # "working": working_boolean,
            # "side": side_boolean
        # }
        # "tip02": {
            # ...
        # }
        # ...
    # } (End of data_to_write)
    # ...
# ]

def write_json(config_file_path, data_to_write):
    """
    Write some data to the config file. If the start_shot of the new
    data is the same as the start_shot of a pre-existing json section
    then edit the previous section instead of making a new one.

    Parameters
    ----------
    config_file_path : str
        The path to the config file we should be writing in.
    data_to_write : dict
        A dict that contains the data to write for one shot set.
    """
    # Check if config file already exists. If so, read it in. Otherwise create an empty list as the config.
    if os.path.exists(config_file_path):
        # Check if the start_shot of the new data is the same as a start shot already in the config.
        logging.debug("Reading old config file at '{path}'.".format(path=config_file_path))
        with open(config_file_path, 'r') as config_file:
            config = json.load(config_file)
    else:
        logging.warning("Could not find old config file at '{path}'. Creating new file.".format(path=config_file_path))
        config = []

    # Reorder the config so that when rewriting the config, all data is ordered.
    ordered_config = []
    logging.debug("Ordering all blocks in config.")
    for unordered_dict in config:
        ordered_config.append(order_dict(unordered_dict))
    config = ordered_config
    logging.debug("Finished ordering all blocks in config.")

    # Write a backup of the current config file to config_file_path.bak.
    logging.debug("Writing backup config to '{path}'.".format(path=config_file_path + ".bak"))
    with open(config_file_path + ".bak", 'w') as config_file_backup:
        json.dump(config, config_file_backup, indent=4) # Tab in the lines to make it pretty.
    
    # Choose the write mode whether an old block should be edited or a new block added.
    logging.debug("Checking if new data has same start shot as pre-existing data.")
    for block_index, data_block in enumerate(config):
        if data_to_write["start_shot"] == data_block["start_shot"]:
            logging.info("Found old config block with start_shot {shot_num} (index {i}). Will edit this block.".format(shot_num=data_to_write["start_shot"], i=block_index))
            # Edit the block by replacing the old block with the new block.
            config[block_index] = data_to_write

            break
    else:
        logging.info("Did not find pre-existing start_shot {shot_num}. Will add new block.".format(shot_num=data_to_write["start_shot"]))
        config.append(data_to_write)

    logging.debug("Writing to config file.")
    with open(config_file_path, 'w') as config_file:
        json.dump(config, config_file, indent=4) # Tab in the lines to make it pretty.
    logging.info("Succesfully wrote new config data to '{path}'.".format(path=config_file_path))

def order_dict(unordered_dict):
    """
    Change a dictionary object into an ordered dictionary object.
    """
    ordered_dict = OrderedDict()
    for key in sorted(list(unordered_dict.keys())):
        if isinstance(unordered_dict[key], dict):
            ordered_dict[key] = order_dict(unordered_dict[key])
        else:
            ordered_dict[key] = unordered_dict[key]

    return ordered_dict


if __name__ == "__main__":
    # Log to a file rewriting the file each time.
    log_filepath = os.path.join(os.path.realpath(os.path.dirname(__file__)), "te3_write_config.log")
    logging.basicConfig(filename=log_filepath, filemode="w", level=logging.DEBUG)

    # Change this dictionary to add/edit the config.
    # If a 'None' value is set either as the element or as the first element of the tuple then nothing is written into the MDSplus table.
    # TODO: Figure out what the gain factors should be for the bdot signal.
    # TODO: Add port coordinates.
    R_digitizer = 500 # ohm
    R_1_big = 56000 # ohm
    R_1_small = 22000 # ohm
    R_2 = 510 # ohm
    # These are the optimized values. As probe 4 does not work we substitute in the unoptimized values.
    R_diggnd = [249.95049774169922, 249.95049774169922, 249.95049774169922, (1/R_2 + 1/R_digitizer)**(-1), 249.95049774169922, 249.95049774169922, 249.95049774169922, 249.95049774169922, 249.95049774169922, 249.95049774169922, 249.95049774169922, 249.95049774169922, 249.95049774169922, 249.95049774169922, 249.95049774169922, 249.95049774169922]
    R_div = [22434.383129275153, 22090.384907069034, 22473.8151563702, (R_1_small + (1/R_2 + 1/R_digitizer)**(-1)), 22468.181213712818, 22137.59875078857, 22216.86270385702, 22220.969613719026, 55876.25725685392, 56734.505032102854, 56099.60731200911, 56375.16028410401, 56481.180329506446, 56487.10267990941, 55895.458020625294, 55700.72269555352]
    R_sense = [128.38, 131.9080059814453, 132.98599700927736, 136.5, 132.49599700927735, 130.14400299072267, 131.02599700927735, 131.51599700927736, 364.3639880371094, 393.0780059814453, 380.0439880371094, 372.7919940185547, 377.98601196289064, 384.0619940185547, 368.77398803710935, 389.6480059814453]
    R_wire = [27.289608681311254, 30.0, 0.0, 0.0, 15.472878378913148, 11.742308531355544, 11.853840901485734, 13.7569971291964, 12.3388293504034, 21.74716023975639, 27.259849261645893, 19.79103050654612, 30.0, 0.0, 30.0, 0.0]
    A_rel = [0.2507635752835547, 0.26478090915177754, 0.29112571922366404, 0.25, 0.26134030313349965, 0.26728114051442003, 0.26220449839931503, 0.2507025043834627, 0.25501925152369564, 0.24796661283146312, 0.23546346000800825, 0.2348158028884792, 0.25302378674682324, 0.24709190432416994, 0.23203518690089278, 0.23056965675075197]

    data_to_write = {
        "act_area": [
            1, 
            "m**2"
        ], 
        "avg_index": {
            "end": 1000, 
            "start": 0
        }, 
        "bdot": {
            "ax1": {
                "gain": 1, 
                "raw_ccw": "\\c4_3_02", 
                "raw_cw": "\\c4_3_01"
            }, 
            "ax2": {
                "gain": 1, 
                "raw_ccw": "\\c4_3_04", 
                "raw_cw": "\\c4_3_03"
            }, 
            "ax3": {
                "gain": 1, 
                "raw_ccw": "\\c4_3_06", 
                "raw_cw": "\\c4_3_05"
            }
        },
        "start_shot": 64917, 
        "tip01": {
            "signal": {
                "v": {
                    "raw": "\\c4_2_01"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_2_02"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.001546440218332616, 
                "offset": -0.0023631, 
                "raw": "\\c1_1_09"
            }, 
            "rdiggnd": (R_diggnd[0], "ohm"),
            "rdiv": (R_div[0], "ohm"),
            "rsense": (R_sense[0], "ohm"),
            "rwire": (R_wire[0], "ohm"),
            "relarea": A_rel[0],
            "working": 1,
            "side": 0
        }, 
        "tip02": {
            "signal": {
                "v": {
                    "raw": "\\c4_2_03"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_2_04"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.0015096757382578553, 
                "offset": 0.0077426, 
                "raw": "\\c1_1_10"
            }, 
            "rdiggnd": (R_diggnd[1], "ohm"),
            "rdiv": (R_div[1], "ohm"),
            "rsense": (R_sense[1], "ohm"),
            "rwire": (R_wire[1], "ohm"),
            "relarea": A_rel[1],
            "working": 0,
            "side": 1,
        }, 
        "tip03": {
            "signal": {
                "v": {
                    "raw": "\\c4_2_06"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_2_05"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.0015070038758632682, 
                "offset": 6.5583e-05, 
                "raw": "\\c1_1_11"
            }, 
            "rdiggnd": (R_diggnd[2], "ohm"),
            "rdiv": (R_div[2], "ohm"),
            "rsense": (R_sense[2], "ohm"),
            "rwire": (R_wire[2], "ohm"),
            "relarea": A_rel[2],
            "working": 1,
            "side": 0,
        }, 
        "tip04": {
            "signal": {
                "v": {
                    "raw": "\\c4_2_07"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_2_08"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.001336306169124245, 
                "offset": 0.0074439, 
                "raw": "\\c1_1_12"
            }, 
            "rdiggnd": (R_diggnd[3], "ohm"),
            "rdiv": (R_div[3], "ohm"),
            "rsense": (R_sense[3], "ohm"),
            "rwire": (R_wire[3], "ohm"),
            "relarea": A_rel[3],
            "working": 1,
            "side": 1,
        }, 
        "tip05": {
            "signal": {
                "v": {
                    "raw": "\\c4_2_09"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_2_10"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.0015024550867344783, 
                "offset": -0.0059191, 
                "raw": "\\c1_1_13"
            }, 
            "rdiggnd": (R_diggnd[4], "ohm"),
            "rdiv": (R_div[4], "ohm"),
            "rsense": (R_sense[4], "ohm"),
            "rwire": (R_wire[4], "ohm"),
            "relarea": A_rel[4],
            "working": 1,
            "side": 0, 
        }, 
        "tip06": {
            "signal": {
                "v": {
                    "raw": "\\c4_2_11"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_2_12"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.0015066240231803147, 
                "offset": 0.0042913, 
                "raw": "\\c1_1_14"
            },
            "rdiggnd": (R_diggnd[5], "ohm"),
            "rdiv": (R_div[5], "ohm"),
            "rsense": (R_sense[5], "ohm"),
            "rwire": (R_wire[5], "ohm"),
            "relarea": A_rel[5],
            "working": 1,
            "side": 1,
        }, 
        "tip07": {
            "signal": {
                "v": {
                    "raw": "\\c4_2_13"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_2_14"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.0015103933185032848, 
                "offset": 0.0043818, 
                "raw": "\\c1_1_15"
            }, 
            "rdiggnd": (R_diggnd[6], "ohm"),
            "rdiv": (R_div[6], "ohm"),
            "rsense": (R_sense[6], "ohm"),
            "rwire": (R_wire[6], "ohm"),
            "relarea": A_rel[6],
            "working": 1,
            "side": 0,
        }, 
        "tip08": {
            "signal": {
                "v": {
                    "raw": "\\c4_2_15"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_2_16"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.0015004536621647554, 
                "offset": -0.0021195, 
                "raw": "\\c1_1_16"
            }, 
            "rdiggnd": (R_diggnd[7], "ohm"),
            "rdiv": (R_div[7], "ohm"),
            "rsense": (R_sense[7], "ohm"),
            "rwire": (R_wire[7], "ohm"),
            "relarea": A_rel[7],
            "working": 1,
            "side": 1,
        }, 
        "tip09": {
            "signal": {
                "v": {
                    "raw": "\\c4_1_01"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_1_02"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.0014987272807931505, 
                "offset": 0.00085847, 
                "raw": "\\c1_2_09"
            }, 
            "rdiggnd": (R_diggnd[8], "ohm"),
            "rdiv": (R_div[8], "ohm"),
            "rsense": (R_sense[8], "ohm"),
            "rwire": (R_wire[8], "ohm"),
            "relarea": A_rel[8],
            "working": 1,
            "side": 0, 
        }, 
        "tip10": {
            "signal": {
                "v": {
                    "raw": "\\c4_1_03"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_1_04"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.001513484389165268, 
                "offset": -0.0034514, 
                "raw": "\\c1_2_10"
            }, 
            "rdiggnd": (R_diggnd[9], "ohm"),
            "rdiv": (R_div[9], "ohm"),
            "rsense": (R_sense[9], "ohm"),
            "rwire": (R_wire[9], "ohm"),
            "relarea": A_rel[9],
            "working": 0,
            "side": 1, 
        }, 
        "tip11": {
            "signal": {
                "v": {
                    "raw": "\\c4_1_06"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_1_05"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.001503698346082189, 
                "offset": -0.0040018, 
                "raw": "\\c1_2_11"
            }, 
            "rdiggnd": (R_diggnd[10], "ohm"),
            "rdiv": (R_div[10], "ohm"),
            "rsense": (R_sense[10], "ohm"),
            "rwire": (R_wire[10], "ohm"),
            "relarea": A_rel[10],
            "working": 1,
            "side": 0, 
        }, 
        "tip12": {
            "signal": {
                "v": {
                    "raw": "\\c4_1_07"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_1_08"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.0015062847471648334, 
                "offset": -0.0041457, 
                "raw": "\\c1_2_12"
            }, 
            "rdiggnd": (R_diggnd[11], "ohm"),
            "rdiv": (R_div[11], "ohm"),
            "rsense": (R_sense[11], "ohm"),
            "rwire": (R_wire[11], "ohm"),
            "relarea": A_rel[11],
            "working": 1,
            "side": 1, 
        }, 
        "tip13": {
            "signal": {
                "v": {
                    "raw": "\\c4_1_09"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_1_10"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.0014990997905757592, 
                "offset": -0.0043879, 
                "raw": "\\c1_2_13"
            }, 
            "rdiggnd": (R_diggnd[12], "ohm"),
            "rdiv": (R_div[12], "ohm"),
            "rsense": (R_sense[12], "ohm"),
            "rwire": (R_wire[12], "ohm"),
            "relarea": A_rel[12],
            "working": 1,
            "side": 0, 
        }, 
        "tip14": {
            "signal": {
                "v": {
                    "raw": "\\c4_1_11"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_1_12"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.001504352543213279, 
                "offset": 0.00075331, 
                "raw": "\\c1_2_14"
            }, 
            "rdiggnd": (R_diggnd[13], "ohm"),
            "rdiv": (R_div[13], "ohm"),
            "rsense": (R_sense[13], "ohm"),
            "rwire": (R_wire[13], "ohm"),
            "relarea": A_rel[13],
            "working": 0,
            "side": 1, 
        }, 
        "tip15": {
            "signal": {
                "v": {
                    "raw": "\\c4_1_13"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_1_14"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.0015043688375411032, 
                "offset": -0.0016667, 
                "raw": "\\c1_2_15"
            }, 
            "rdiggnd": (R_diggnd[14], "ohm"),
            "rdiv": (R_div[14], "ohm"),
            "rsense": (R_sense[14], "ohm"),
            "rwire": (R_wire[14], "ohm"),
            "relarea": A_rel[14],
            "working": 1,
            "side": 0, 
        }, 
        "tip16": {
            "signal": {
                "v": {
                    "raw": "\\c4_1_15"
                }
            },
            "noise": {
                "v": {
                    "raw": "\\c4_1_16"
                }
            },
            "vgroupnoise": {
                "adjnoise": {
                    "raw": "\\c4_1_01"
                }, 
                "factor": 0, 
                "raw": "\\c4_1_01"
            },
            "vbias": {
                "div": 0.0015122845901829743, 
                "offset": -0.0064643, 
                "raw": "\\c1_2_16"
            }, 
            "rdiggnd": (R_diggnd[15], "ohm"),
            "rdiv": (R_div[15], "ohm"),
            "rsense": (R_sense[15], "ohm"),
            "rwire": (R_wire[15], "ohm"),
            "relarea": A_rel[15],
            "working": 1,
            "side": 1, 
        }
    }
    logging.debug("Reordering data to write for ease of reading.")
    ordered_data_to_write = order_dict(data_to_write)
    config_file_path = os.path.join(os.path.realpath(os.path.dirname(__file__)), "configs/te_probe3.json")
    write_json(config_file_path, ordered_data_to_write)
