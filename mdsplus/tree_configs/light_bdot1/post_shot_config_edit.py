"""
These are functions needed to edit the config object after we take a shot.
They don't actually edit the config file and instead add/remove information
that is needed when writing to the tree.
"""
import logging
import numpy as np
import modules.coordinate_transforms as coords
from modules.mysql_connection import get_engine
import pandas
import MDSplus as mds

def read_sql(shot_number, sql_config_path="/home/WIPALdata/sql_config.ini"):
    """
    Read the probe position from the database for the queried shot.

    Parameters
    ----------
    shot_number : int
        The number of the shot we want the probe position for.
    sql_config : str, default='/home/WIPALdata/sql_config.ini'
        Path to config file containing sql info for database.

    Returns
    -------
    query_result : QueryResult
        Object containing the returned values from the database.
    """
    logging.debug("Reading from MySQL database te probe info for shot {num}.".format(num=shot_number))
    engine = get_engine(sql_config_path)
    database = engine.url.database
    with engine.connect() as connection:
        sql_query = "SELECT insertion, clocking, latitude, longitude, alpha, beta, gamma, port_radius FROM {database}.light_bdot1 where shot_num={num}".format(database=database, num=shot_number)

        logging.debug("Executing query '{query}'.".format(query=sql_query))
        try:
            result = pandas.read_sql(sql_query, connection)
        except Exception:
            logging.exception("Failure executing query '{query}'.".format(query=sql_query))
            raise
        else:
            logging.info("Executed query successfully.")

    if result.size == 0:
        logging.exception("Could not find values for shot {}.".format(shot_number))
        raise ValueError("Missing row in MySQL '{table}' table for shot {num}.".format(table='light_bdot1', num=shot_number))

    class QueryResult:
        # Change the query result into an object for easy access.
        def __init__(self, query_result):
            # Set the value of each variable to that of the result.
            self.insertion, self.clocking_deg, self.latitude_deg, self.longitude_deg, self.alpha_deg, self.beta_deg, self.gamma_deg, self.port_radius = query_result.iloc[0]

            # Change all variables to radians.
            self.clocking = np.deg2rad(self.clocking_deg)
            self.latitude = np.deg2rad(self.latitude_deg)
            self.longitude = np.deg2rad(self.longitude_deg)
            self.alpha = np.deg2rad(self.alpha_deg)
            self.beta = np.deg2rad(self.beta_deg)
            self.gamma = np.deg2rad(self.gamma_deg)

    # Extract the values from the call.
    query_result = QueryResult(result)
    logging.debug("Got values from row:\n{}".format(result))
    return query_result

def get_pcb_coil_locations(mysql_query):
    """
    Get the position of each coil on each pcb in cartesian coordinates.

    Parameters
    ----------
    mysql_query : QueryResult
        Result from the MySQL call.

    Returns
    -------
    position_dict : dict[str : dict[str: (float, str)]]
        Dictionary of pcb locations.
    """
    # Get the positions of every coil on the lightsaber probe.
    positive_x_coil_positions = np.linspace(0.0137, 0.7747 + 0.0345, num=4 * 16)
    all_coil_x_positions = np.concatenate((-np.flip(positive_x_coil_positions), positive_x_coil_positions))
    # x decreases as probe number increases so multiply by a negative sign.
    all_coil_x_positions *= -1

    coil_names = ['db3a', 'db1', 'db3b', 'db2']
    # Index along each pcb the coil is.
    pcb_coil_indeces = [0, 1, 2, 3]

    position_dict = {}
    # Iterate along pcbs from most negative x to most positive x.
    for pcb_number in range(1, 33):
        pcb_name = "pcb{}".format(str(pcb_number).zfill(2))
        position_dict[pcb_name] = {}

        # Iterate through each coil and where along it's pcb it is.
        for coil_name, pcb_coil_index in zip(coil_names, pcb_coil_indeces):
            if pcb_number <= 16:
                full_coil_index = 4 * (pcb_number - 1) + (3 - pcb_coil_index)
            else:
                full_coil_index = 4 * (pcb_number - 1) + pcb_coil_index
            
            position_dict[pcb_name][coil_name] = {}

            coil_cartesian_position = coords.position_to_machine_cartesian(
                mysql_query.port_radius, mysql_query.longitude, mysql_query.latitude, 
                mysql_query.alpha, mysql_query.beta, mysql_query.gamma,
                all_coil_x_positions[full_coil_index], 0, mysql_query.insertion, mysql_query.clocking
            )
            coil_cylindrical_position = coords.position_cartesian_to_cylindrical(coil_cartesian_position)

            for direction, unit, position in zip(['r', 'phi', 'z'], ['m', 'rad', 'm'], coil_cylindrical_position):
                position_dict[pcb_name][coil_name][direction] = (position, unit)

    return position_dict

def get_probe_bdot_equations(mysql_query, coil_position_dict, coil_orientation_dict, pcbs_node):
    """
    Get the equations for calculating `dbr`, `dbphi`, and `dbz` for all combined pcb sets.
    
    Parameters
    ----------
    mysql_query : QueryResult
        Result from the MySQL call.
    coil_position_dict : dict[str: dict[str: (float, str)]]
        Dictionary of pcb locations.
    coil_orientation_dict : dict[str: dict[str: dict[str: dict[str: float]]]]
        "pcbs" subdictionary from the config.
    pcbs_node : mds.TreeNode
        `pcbs` node from tree.

    Returns
    -------
    all_compiled_equations : dict[str : dict[str : (mds.Data, str)]]
        Dictionary of equations compiled by mds for the bdot directions.
    """
    logging.debug("Getting probe bdot equations.")
    all_compiled_equations = {}
    for probe_number in range(1, 65):
        logging.debug("Creating equations for probe #{}.".format(probe_number))
        probe_str = "pb{}".format(str(probe_number).zfill(2))
        all_compiled_equations[probe_str] = {}

        # Get which pcbs are combined for this probe.
        # Left of the middle.
        if probe_number == 32:
            pcb_db1_num = 16
            pcb_db2_num = 16
            pcb_db3_num = 16
            pcb_db3_option = 'a'
        elif (probe_number % 2 == 1 and probe_number < 32):
            pcb_db1_num = probe_number // 2 + 1
            pcb_db2_num = probe_number // 2 + 1
            pcb_db3_num = probe_number // 2 + 1
            pcb_db3_option = 'b'
        elif (probe_number % 2 == 0 and probe_number < 32):
            pcb_db1_num = probe_number // 2
            pcb_db2_num = probe_number // 2 + 1
            pcb_db3_num = probe_number // 2
            pcb_db3_option = 'a'
        # Right of the middle.
        elif probe_number == 33:
            pcb_db1_num = 17
            pcb_db2_num = 17
            pcb_db3_num = 17
            pcb_db3_option = 'a'
        elif (probe_number % 2 == 1 and probe_number > 33):
            pcb_db1_num = probe_number // 2 + 1
            pcb_db2_num = probe_number // 2
            pcb_db3_num = probe_number // 2 + 1
            pcb_db3_option = 'a'
        elif (probe_number % 2 == 0 and probe_number > 33):
            pcb_db1_num = probe_number // 2
            pcb_db2_num = probe_number // 2
            pcb_db3_num = probe_number // 2
            pcb_db3_option = 'b'

        pcb_db1_str = "pcb{}".format(str(pcb_db1_num).zfill(2))
        pcb_db2_str = "pcb{}".format(str(pcb_db2_num).zfill(2))
        pcb_db3_str = "pcb{}".format(str(pcb_db3_num).zfill(2))

        logging.debug("For probe #{}:\t1: {}, 2: {}, 3{}: {}".format(probe_number, pcb_db1_str, pcb_db2_str, pcb_db3_option, pcb_db3_str))

        # Get the positions of each pcb.
        db1_r_position = coil_position_dict[pcb_db1_str]['db1']['r'][0]
        db1_phi_position = coil_position_dict[pcb_db1_str]['db1']['phi'][0]
        db1_z_position = coil_position_dict[pcb_db1_str]['db1']['z'][0]
        db2_r_position = coil_position_dict[pcb_db2_str]['db2']['r'][0]
        db2_phi_position = coil_position_dict[pcb_db2_str]['db2']['phi'][0]
        db2_z_position = coil_position_dict[pcb_db2_str]['db2']['z'][0]
        db3_r_position = coil_position_dict[pcb_db3_str]['db3' + pcb_db3_option]['r'][0]
        db3_phi_position = coil_position_dict[pcb_db3_str]['db3' + pcb_db3_option]['phi'][0]
        db3_z_position = coil_position_dict[pcb_db3_str]['db3' + pcb_db3_option]['z'][0]

        # Average the pcb positions.
        avg_r_position = np.mean([db1_r_position, db2_r_position, db3_r_position])
        avg_phi_position = np.mean([db1_phi_position, db2_phi_position, db3_phi_position])
        avg_z_position = np.mean([db1_z_position, db2_z_position, db3_z_position])
        avg_cartesian_position = coords.position_cylindrical_to_cartesian(
            np.array([avg_r_position, avg_phi_position, avg_z_position])
        )
        logging.debug("Average pcb cartesian position: {}".format(avg_cartesian_position))

        # Get the orientation of each probe in port coordinates.
        db1_x_orientation = coil_orientation_dict[pcb_db1_str]['db1']['orientation']['x']
        db1_y_orientation = coil_orientation_dict[pcb_db1_str]['db1']['orientation']['y']
        db1_z_orientation = coil_orientation_dict[pcb_db1_str]['db1']['orientation']['z']
        db2_x_orientation = coil_orientation_dict[pcb_db2_str]['db2']['orientation']['x']
        db2_y_orientation = coil_orientation_dict[pcb_db2_str]['db2']['orientation']['y']
        db2_z_orientation = coil_orientation_dict[pcb_db2_str]['db2']['orientation']['z']
        db3_x_orientation = coil_orientation_dict[pcb_db3_str]['db3' + pcb_db3_option]['orientation']['x']
        db3_y_orientation = coil_orientation_dict[pcb_db3_str]['db3' + pcb_db3_option]['orientation']['y']
        db3_z_orientation = coil_orientation_dict[pcb_db3_str]['db3' + pcb_db3_option]['orientation']['z']
        # Change the orientation to cartesian coordinates.
        db1_x_orientation, db1_y_orientation, db1_z_orientation = coords.direction_to_machine_cartesian(
            mysql_query.longitude, mysql_query.latitude, mysql_query.alpha, mysql_query.beta, mysql_query.gamma,
            db1_x_orientation, db1_y_orientation, db1_z_orientation, mysql_query.clocking
        )
        db2_x_orientation, db2_y_orientation, db2_z_orientation = coords.direction_to_machine_cartesian(
            mysql_query.longitude, mysql_query.latitude, mysql_query.alpha, mysql_query.beta, mysql_query.gamma,
            db2_x_orientation, db2_y_orientation, db2_z_orientation, mysql_query.clocking
        )
        db3_x_orientation, db3_y_orientation, db3_z_orientation = coords.direction_to_machine_cartesian(
            mysql_query.longitude, mysql_query.latitude, mysql_query.alpha, mysql_query.beta, mysql_query.gamma,
            db3_x_orientation, db3_y_orientation, db3_z_orientation, mysql_query.clocking
        )

        # Each probe acts like a dot product and measures the magnitude of Bdot in a direction defined by the orientation.
        # We can extract out what each x, y, and z component is of the original field by inverting the matrix where each row is an orientation.
        orientation_matrix = np.array([
            [db1_x_orientation, db1_y_orientation, db1_z_orientation],
            [db2_x_orientation, db2_y_orientation, db2_z_orientation],
            [db3_x_orientation, db3_y_orientation, db3_z_orientation]
        ])
        # Each row of the inverse is the components of the corresponding pcbs in cartesian coordinates.
        orientation_inverse = np.linalg.inv(orientation_matrix)
        # Now change the inverse so that it converts to cylindrical coordinates.
        orientation_inverse = coords.direction_cartesian_to_cylindrical(orientation_inverse, avg_cartesian_position)

        # Now we can create the equations needed to take the pcb coil values and convert them to cylindrical.
        raw_equations = [
            'BUILD_SIGNAL(BUILD_WITH_UNITS($1 * {} + $2 * {} + $3 * {}, "T / s"),, DIM_OF($1))'.format(*orientation_inverse[0]),
            'BUILD_SIGNAL(BUILD_WITH_UNITS($1 * {} + $2 * {} + $3 * {}, "T / s"),, DIM_OF($1))'.format(*orientation_inverse[1]),
            'BUILD_SIGNAL(BUILD_WITH_UNITS($1 * {} + $2 * {} + $3 * {}, "T / s"),, DIM_OF($1))'.format(*orientation_inverse[2])
        ]

        # Get the actual node references from the tree.
        db1_node = pcbs_node.getNode(pcb_db1_str).getNode('db1')
        db2_node = pcbs_node.getNode(pcb_db2_str).getNode('db2')
        db3_node = pcbs_node.getNode(pcb_db3_str).getNode('db3' + pcb_db3_option)

        compiled_equations = [
            mds.Data.compile(equation, db1_node, db2_node, db3_node) for equation in raw_equations
        ]
        for direction, equation in zip(['dbr', 'dbphi', 'dbz'], compiled_equations):
            # Attach the units for the equations when putting it into the dictionary.
            all_compiled_equations[probe_str][direction] = equation

        logging.debug("Bdot cylindrical equations for probe #{}:\n".format(probe_number) + "\n".join(str(equation) for equation in compiled_equations))
    logging.info("Finished creating all equations for probes.")
    return all_compiled_equations

def edit_config(config_object, shot_number, light_bdot1_top_node):
    """
    Edit the config object before inserting data into the MDSplus tree.
    
    Parameters
    ----------
    config_object : dict[str, dict[...]]
        Object read from config file.
    shot_number : int
        The shot number we are evaluating.
    light_bdot1_top_node : mds.TreeNode
        Top of the light_bdot1 tree.

    Returns
    -------
    config_object : dict[str, dict[...]]
    """
    logging.debug("Editing config object for shot {}.".format(shot_number))
    logging.debug("Removing 'start_shot' from config data since no need to insert into tree.")
    config_object.pop('start_shot')

    # Try to connect to the MySQL database.
    try:
        query = read_sql(shot_number)
    except ValueError as e:
        logging.info("Can't get probe positions for shot {} since no row in database. Error was:\n{}".format(shot_number, e))
        raise
    except Exception as e:
        logging.info("Caught error trying to read from MySQL. Can't get probe position information. Error was:\n{}".format(e))
        raise

    logging.debug("Adding port information.")
    config_object['port'] = {
        'clock': query.clocking_deg,
        'rport': query.port_radius,
        'lat': query.latitude_deg,
        'long': query.longitude_deg,
        'alpha': query.alpha_deg,
        'beta': query.beta_deg,
        'gamma': query.gamma_deg,
        'insert': query.insertion
    }

    logging.debug("Adding coil locations")
    coil_positions = get_pcb_coil_locations(query)
    for pcb_key in coil_positions.keys():
        for coil_key in coil_positions[pcb_key].keys():
            for direction_key in coil_positions[pcb_key][coil_key].keys():
                config_object['pcbs'][pcb_key][coil_key][direction_key] = coil_positions[pcb_key][coil_key][direction_key]

    logging.debug("Adding probe equations.")
    probe_equations = get_probe_bdot_equations(query, coil_positions, config_object['pcbs'], light_bdot1_top_node.getNode('pcbs'))
    config_object['probes'] = probe_equations

    logging.debug("Done editing config object.")
    return config_object
