"""
Write/edit the config file used for the light_bdot1 MDSplus tree.
"""
import logging
import json
import os.path
from collections import OrderedDict

# JSON data structure

# [
    # { (Start of data_to_write)
        # "start_shot": start_shot,
        # "pcbs": {
            # "pcb01": {
                # "db1": {
                    # "gain": (gain, "T / (s V)"),
                    # "raw": (chan, "V"),
                    # "working": working_status,
                    # "orientation": {
                        # The orientations are in the x=North, y=East, z=Down port coordinate system when clocking is 0.
                        # "x": 1_component, # This is how much of pcb01.db1 is in the pcbs 1 direction.
                        # "y": 2_component,
                        # "z": 3_component,
                    # }
                # },
                # ...
            # },
            # ...
        # },
    # }
# ]

def write_json(config_file_path, data_to_write):
    """
    Write some data to the config file. If the start_shot of the new
    data is the same as the start_shot of a pre-existing json section
    then edit the previous section instead of making a new one.

    Parameters
    ----------
    config_file_path : str
        The path to the config file we should be writing in.
    data_to_write : dict
        A dict that contains the data to write for one shot set.
    """
    # Check if config file already exists. If so, read it in. Otherwise create an empty list as the config.
    if os.path.exists(config_file_path):
        # Check if the start_shot of the new data is the same as a start shot already in the config.
        logging.debug("Reading old config file at '{path}'.".format(path=config_file_path))
        with open(config_file_path, 'r') as config_file:
            config = json.load(config_file)
    else:
        logging.warning("Could not find old config file at '{path}'. Creating new file.".format(path=config_file_path))
        config = []

    # Reorder the config so that when rewriting the config, all data is ordered.
    ordered_config = []
    logging.debug("Ordering all blocks in config.")
    for unordered_dict in config:
        ordered_config.append(order_dict(unordered_dict))
    config = ordered_config
    logging.debug("Finished ordering all blocks in config.")

    # Write a backup of the current config file to config_file_path.bak.
    logging.debug("Writing backup config to '{path}'.".format(path=config_file_path + ".bak"))
    with open(config_file_path + ".bak", 'w') as config_file_backup:
        json.dump(config, config_file_backup, indent=4) # Tab in the lines to make it pretty.
    
    # Choose the write mode whether an old block should be edited or a new block added.
    logging.debug("Checking if new data has same start shot as pre-existing data.")
    for block_index, data_block in enumerate(config):
        if data_to_write["start_shot"] == data_block["start_shot"]:
            logging.info("Found old config block with start_shot {shot_num} (index {i}). Will edit this block.".format(shot_num=data_to_write["start_shot"], i=block_index))
            # Edit the block by replacing the old block with the new block.
            config[block_index] = data_to_write

            break
    else:
        logging.info("Did not find pre-existing start_shot {shot_num}. Will add new block.".format(shot_num=data_to_write["start_shot"]))
        config.append(data_to_write)

    logging.debug("Writing to config file.")
    with open(config_file_path, 'w') as config_file:
        json.dump(config, config_file, indent=4) # Tab in the lines to make it pretty.
    logging.info("Succesfully wrote new config data to '{path}'.".format(path=config_file_path))

def order_dict(unordered_dict):
    """
    Change a dictionary object into an ordered dictionary object.
    """
    ordered_dict = OrderedDict()
    for key in sorted(list(unordered_dict.keys())):
        if isinstance(unordered_dict[key], dict):
            ordered_dict[key] = order_dict(unordered_dict[key])
        else:
            ordered_dict[key] = unordered_dict[key]

    return ordered_dict


if __name__ == "__main__":
    # Log to a file rewriting the file each time.
    log_file_path = os.path.join(os.path.realpath(os.path.dirname(__file__)), "light_bdot1_write_config.log")
    logging.basicConfig(filename=log_file_path, filemode="w", level=logging.DEBUG)

    gains = {'3a': 10892.80009510781, '1': 9675.85673633917, '3b': 10724.294961508762, '2': 8767.497617193932}
    db1_gain = gains['1']
    db2_gain = gains['2']
    db3a_gain = gains['3a']
    db3b_gain = gains['3b']

    # The orientations are in the x=North, y=East, z=Down port coordinate system when clocking is 0.
    orientations_positive_x = {
        # The coils need to be flipped compared to Mary's orientations as we know dB_z < 0.
        '3a': {'x': -0.9948709924806819, 'y': -0.09070268745374756, 'z': -0.0447742203636293}, 
        '1': {'x': 0.03547510374061758, 'y': -0.08366467033387577, 'z': 0.9958623097358973}, 
        '3b': {'x': 0.995124971069634, 'y': -0.09690377136456345, 'z': 0.018328967482767557}, 
        '2': {'x': 0.02408480863975193, 'y': 0.9976353449693043, 'z': 0.06437111511201171}
    }
    db1_orientation_positive_x = orientations_positive_x['1']
    db2_orientation_positive_x = orientations_positive_x['2']
    db3a_orientation_positive_x = orientations_positive_x['3a']
    db3b_orientation_positive_x = orientations_positive_x['3b']
    # For negative x pcbs, we rotate 180 deg around the z axis and swap the connections so z -> -z.
    db1_orientation_negative_x = {'x': db1_orientation_positive_x["x"], 'y': db1_orientation_positive_x["y"], 'z': -db1_orientation_positive_x["z"]}
    db2_orientation_negative_x = {'x': db2_orientation_positive_x["x"], 'y': db2_orientation_positive_x["y"], 'z': -db2_orientation_positive_x["z"]}
    db3a_orientation_negative_x = {'x': db3a_orientation_positive_x["x"], 'y': db3a_orientation_positive_x["y"], 'z': -db3a_orientation_positive_x["z"]}
    # For some reason the 3b coils are only rotated. Thus 'x' and 'y' are reversed.
    db3b_orientation_negative_x = {'x': -db3b_orientation_positive_x["x"], 'y': -db3b_orientation_positive_x["y"], 'z': db3b_orientation_positive_x["z"]}

    # Change this dictionary to add/edit the config.
    # If a 'None' value is set either as the element or as the first element of the tuple then nothing is written into the MDSplus table.
    data_to_write = {
        "start_shot": 58842,
        "pcbs": {
            "pcb01": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_17", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_17", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_01", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_negative_x
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_01", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x
                },
            },
            "pcb02": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_18", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_18", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_02", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_02", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb03": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_19", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_19", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_03", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_03", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb04": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_20", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_20", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_04", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_04", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb05": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_21", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_21", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_05", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_05", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb06": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_22", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_22", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_06", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_06", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb07": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_23", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_23", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_07", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (1.3 * db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_07", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb08": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_24", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_24", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_08", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_08", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb09": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_25", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_25", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_09", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_09", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb10": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_26", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_26", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_10", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_10", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb11": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_27", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_27", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_11", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_11", "V"),
                    "working": 0,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb12": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_28", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_28", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_12", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_12", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb13": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_29", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_29", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_13", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_13", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb14": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_30", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_30", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_14", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_14", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb15": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_31", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_31", "V"),
                    "working": 1,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_15", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_15", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb16": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta324_32", "V"),
                    "working": 1,
                    "orientation": db1_orientation_negative_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta323_32", "V"),
                    "working": 0,
                    "orientation": db2_orientation_negative_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta323_16", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_negative_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta324_16", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_negative_x,
                },
            },
            "pcb17": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_32", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_32", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_16", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_16", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb18": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_31", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_31", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_15", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_15", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb19": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_30", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_30", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_14", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_14", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb20": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_29", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_29", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_13", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_13", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb21": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_28", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_28", "V"),
                    "working": 0,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_12", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_12", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb22": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_27", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_27", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_11", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_11", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb23": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_26", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_26", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_10", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_10", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb24": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_23", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_25", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_09", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_09", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb25": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_24", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_24", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_08", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_08", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb26": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_25", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_23", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_07", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_07", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb27": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_22", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_22", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_06", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_06", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb28": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_21", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_21", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_05", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_05", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb29": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_20", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_20", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_04", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_04", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb30": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_19", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_19", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_03", "V"),
                    "working": 0,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_03", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb31": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_18", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_18", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_02", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_02", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
            "pcb32": {
                "db1": {
                    "gain": (db1_gain, "T / (s V)"),
                    "raw": ("\\ta326_17", "V"),
                    "working": 1,
                    "orientation": db1_orientation_positive_x,
                },
                "db2": {
                    "gain": (db2_gain, "T / (s V)"),
                    "raw": ("\\ta325_17", "V"),
                    "working": 1,
                    "orientation": db2_orientation_positive_x,
                },
                "db3a": {
                    "gain": (db3a_gain, "T / (s V)"),
                    "raw": ("\\ta325_01", "V"),
                    "working": 1,
                    "orientation": db3a_orientation_positive_x,
                },
                "db3b": {
                    "gain": (db3b_gain, "T / (s V)"),
                    "raw": ("\\ta326_01", "V"),
                    "working": 1,
                    "orientation": db3b_orientation_positive_x,
                },
            },
        },
    }

    logging.debug("Reordering data to write for ease of reading.")
    ordered_data_to_write = order_dict(data_to_write)
    config_file_path = os.path.join(os.path.realpath(os.path.dirname(__file__)), "configs/light_bdot1.json")

    write_json(config_file_path, ordered_data_to_write)
