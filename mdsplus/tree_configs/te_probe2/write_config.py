"""
Write/edit the config file used for the te_probe2 MDSplus tree.
"""
import logging
import json
import os.path
from collections import OrderedDict

# JSON data structure

# [
    # { (Start of data_to_write)
        # "start_shot": start_shot,
        # "act_area": (area, "m"),
        # "avg_index": {
            # "start": start_index,
            # "end": end_index
        # },
        # "bdot": {
            # "ax1": {
                # "gain": gain_factor,
                # "raw_cw": mds_channel
                # "raw_ccw": mds_channel
            # },
            # "ax2": {
                # "gain": gain_factor,
                # "raw_cw": mds_channel
                # "raw_ccw": mds_channel
            # },
            # "ax3": {
                # "gain": gain_factor,
                # "raw_cw": mds_channel
                # "raw_ccw": mds_channel
            # }
        # }
        # "port": {
            # "alpha": (alpha_value, "deg"),
            # "beta": (beta_value, "deg"),
            # "gamma": (gamma_value, "deg"),
            # "lat": (latitude_value, "deg"),
            # "long": (longitude_value, "deg"),
            # "rport": (port_radius_value, "m")
        # }
        # "tip01": {
            # "vmeas": {
                # "raw": mds_channel,
                # "adjnoise": {
                    # "raw": mds_channel
                # }
            # },
            # "vgroupnoise": {
                # "raw": mds_channel,
                # "factor": factor_value,
                # "adjnoise": {
                    # "raw": mds_channel
                # }
            # },
            # "vsense": {"div": divider_factor}, Let the divider be in the order R_1 -> R_2 -> ground.
            # Then div = (1/R_2 + 1/R_digitizer)^(-1) / (R_1 + (1/R_2 + 1/R_digitizer)^(-1)).
            # "vbias": {
                # "raw": mds_channel,
                # "offset": offset_voltage
                # "div": divider_factor
            # }
            # "rsense": (sense_resistance, "ohm"),
            # "rwire": (wire_resistance, "ohm")
            # "relarea": relative_area,
            # "working": working_boolean,
            # "side": side_boolean
        # }
        # "tip02": {
            # ...
        # }
        # ...
    # } (End of data_to_write)
    # ...
# ]

def write_json(config_file_path, data_to_write):
    """
    Write some data to the config file. If the start_shot of the new
    data is the same as the start_shot of a pre-existing json section
    then edit the previous section instead of making a new one.

    params:
    config_file_path    = The path to the config file we should be writing in.
    data_to_write       = A dict that contains the data to write for one shot set.
    """
    # Check if config file already exists. If so, read it in. Otherwise create an empty list as the config.
    if os.path.exists(config_file_path):
        # Check if the start_shot of the new data is the same as a start shot already in the config.
        logging.debug("Reading old config file at '{path}'.".format(path=config_file_path))
        with open(config_file_path, 'r') as config_file:
            config = json.load(config_file)
    else:
        logging.warning("Could not find old config file at '{path}'. Creating new file.".format(path=config_file_path))
        config = []

    # Reorder the config so that when rewriting the config, all data is ordered.
    ordered_config = []
    logging.debug("Ordering all blocks in config.")
    for unordered_dict in config:
        ordered_config.append(order_dict(unordered_dict))
    config = ordered_config
    logging.debug("Finished ordering all blocks in config.")

    # Write a backup of the current config file to config_file_path.bak.
    logging.debug("Writing backup config to '{path}'.".format(path=config_file_path + ".bak"))
    with open(config_file_path + ".bak", 'w') as config_file_backup:
        json.dump(config, config_file_backup, indent=4) # Tab in the lines to make it pretty.
    
    # Choose the write mode whether an old block should be edited or a new block added.
    logging.debug("Checking if new data has same start shot as pre-existing data.")
    for block_index, data_block in enumerate(config):
        if data_to_write["start_shot"] == data_block["start_shot"]:
            logging.info("Found old config block with start_shot {shot_num} (index {i}). Will edit this block.".format(shot_num=data_to_write["start_shot"], i=block_index))
            # Edit the block by replacing the old block with the new block.
            config[block_index] = data_to_write

            break
    else:
        logging.info("Did not find pre-existing start_shot {shot_num}. Will add new block.".format(shot_num=data_to_write["start_shot"]))
        config.append(data_to_write)

    logging.debug("Writing to config file.")
    with open(config_file_path, 'w') as config_file:
        json.dump(config, config_file, indent=4) # Tab in the lines to make it pretty.
    logging.info("Succesfully wrote new config data to '{path}'.".format(path=config_file_path))

def order_dict(unordered_dict):
    """
    Change a dictionary object into an ordered dictionary object.
    """
    ordered_dict = OrderedDict()
    for key in sorted(list(unordered_dict.keys())):
        if isinstance(unordered_dict[key], dict):
            ordered_dict[key] = order_dict(unordered_dict[key])
        else:
            ordered_dict[key] = unordered_dict[key]

    return ordered_dict


if __name__ == "__main__":
    # Change this dictionary to add/edit the config.
    # If a 'None' value is set either as the element or as the first element of the tuple then nothing is written into the MDSplus table.
    # TODO: Figure out what the gain factors should be for the bdot signal.
    # TODO: Add port coordinates.
    bias_divider = 2e3 / (2e3 + 470e3)
    sense_resistor = 470
    bdot_divider = 0.5
    measurement_divider = 680 / (680 + 56e3)
    acq_tag = lambda acq_digitizer_number, channel: f"\\ta{str(acq_digitizer_number).zfill(3)}_{str(channel).zfill(2)}"
    vbias_offsets = [
        0.002049311057356356, -0.0013491699848070845, 
        0.0006915255978550617, 0.0006813225953992075, 
        0.0020629267662322427, 0.0007567144923081694, 
        -0.0031299731764857907, -0.004902739664775896, 
        0.0007891703985922946, -0.0024743225755778403, 
        -0.0018625071537202618, -3.95013196625723e-05, 
        0.0019621728210526307, -0.0005432773842625045, 
        -0.0020521340582085137, -0.00487641530978336
    ]

    data_to_write = {
        "act_area": [
            1,
            "m"
        ], 
        "avg_index": {
            "end": 1000, 
            "start": 100
        }, 
        "bdot": {
            "ax1": {
                "gain": 1 / bdot_divider, 
                "raw_ccw": acq_tag(114, 18), 
                "raw_cw": acq_tag(114, 21)
            }, 
            "ax2": {
                "gain": 1 / bdot_divider, 
                "raw_ccw": acq_tag(114, 20), 
                "raw_cw": acq_tag(114, 19)
            }, 
            "ax3": {
                "gain": 1 / bdot_divider, 
                "raw_ccw": acq_tag(114, 22), 
                "raw_cw": acq_tag(114, 17)
            }
        }, 
        "start_shot": 67931, 
        "tip01": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 0, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[1 - 1], 
                "raw": acq_tag(322, 1)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 1)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip02": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 1, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[2 - 1], 
                "raw": acq_tag(322, 2)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 2)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 0
        }, 
        "tip03": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 0, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[3 - 1], 
                "raw": acq_tag(322, 3)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 3)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 0
        }, 
        "tip04": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 1, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[4 - 1], 
                "raw": acq_tag(322, 4)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 4)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip05": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 0, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[5 - 1], 
                "raw": acq_tag(322, 5)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 5)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip06": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 1, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[6 - 1], 
                "raw": acq_tag(322, 6)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 6)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip07": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 0, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[7 - 1], 
                "raw": acq_tag(322, 7)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 7)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip08": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 1, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[8 - 1], 
                "raw": acq_tag(322, 8)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 8)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip09": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 0, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[9 - 1], 
                "raw": acq_tag(322, 9)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 9)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip10": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 1, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[10 - 1], 
                "raw": acq_tag(322, 10)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 10)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip11": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 0, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[11 - 1], 
                "raw": acq_tag(322, 11)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 11)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip12": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 1, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[12 - 1], 
                "raw": acq_tag(322, 12)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 12)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip13": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 0, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[13 - 1], 
                "raw": acq_tag(322, 13)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 13)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip14": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 1, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[14 - 1], 
                "raw": acq_tag(322, 14)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 14)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip15": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 0, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[15 - 1], 
                "raw": acq_tag(322, 15)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 15)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }, 
        "tip16": {
            "relarea": 1, 
            "rsense": [
                sense_resistor, 
                "ohm"
            ], 
            "rwire": [
                0, 
                "ohm"
            ], 
            "side": 1, 
            "vbias": {
                "div": bias_divider, 
                "offset": vbias_offsets[16 - 1], 
                "raw": acq_tag(322, 16)
            }, 
            "vgroupnoise": {
                "adjnoise": {
                    "raw": None
                }, 
                "factor": 0, 
                "raw": None
            }, 
            "vmeas": {
                "adjnoise": {
                    "raw": None
                }, 
                "raw": acq_tag(322, 33 - 16)
            }, 
            "vsense": {
                "div": measurement_divider
            }, 
            "working": 1
        }
    }
    logging.debug("Reordering data to write for ease of reading.")
    ordered_data_to_write = order_dict(data_to_write)

    config_file_path = "./configs/te_probe2.json"

    # Log to a file rewriting the file each time.
    logging.basicConfig(filename="te_write_config.log", filemode="w", level=logging.DEBUG)
    write_json(config_file_path, ordered_data_to_write)
