"""
Write/edit the config file used for the flux_bdot2 MDSplus tree.
"""
import logging
import json
import os.path
from collections import OrderedDict
import numpy as np
from tree_creates.flux_bdot2_create import NUM_STICKS, NUM_PROBES_PER_STICK, DB_PROBE_NUMBER_ASSIGNMENTS

# JSON data structure

# [
    # { (Start of data_to_write)
        # "start_shot": start_shot,
        # "flange": { # The coordinates below are the position of the flange in port coordinates.
            # "position": {
                # "port_coords": {
                    # "x": x_coord,
                    # "y": y_coord,
                    # "z": z_coord,
                # }
            # },
            # "orientation": {
                # "port_coords": {
                    # "x": x_coord,
                    # "y": y_coord,
                    # "z": z_coord,
                # }
            # }
        # },
        # "sticks": {
            # "orientation": {
                # "port_coords": {
                    # "x": x_coord,
                    # "y": y_coord,
                    # "z": z_coord,
                # },
            # },
            # "stick1": {
                # "probes": {
                    # "pb01": {
                        # "raw": (channel, "V"),
                        # "gain": (gain, "T / (s V)"),
                        # "working": working_status,
                        # "orientation": {
                            # "stick_coords": {
                                # "x": x_coord,
                                # "y": y_coord,
                                # "z": z_coord,
                            # },
                        # },
                        # "position": {
                            # "stick_coords": {
                                # "x": (x_coord, "m"),
                                # "y": (y_coord, "m"),
                                # "z": (z_coord, "m"),
                            # },
                        # },
                    # },
                    # ...
                # },
            # }, 
            # ...
        # },
    # }
# ]

def write_json(config_file_path, data_to_write):
    """
    Write some data to the config file. If the start_shot of the new
    data is the same as the start_shot of a pre-existing json section
    then edit the previous section instead of making a new one.

    Parameters
    ----------
    config_file_path : str
        The path to the config file we should be writing in.
    data_to_write : dict
        A dict that contains the data to write for one shot set.
    """
    # Check if config file already exists. If so, read it in. Otherwise create an empty list as the config.
    if os.path.exists(config_file_path):
        # Check if the start_shot of the new data is the same as a start shot already in the config.
        logging.debug("Reading old config file at '{path}'.".format(path=config_file_path))
        with open(config_file_path, 'r') as config_file:
            config = json.load(config_file)
    else:
        logging.warning("Could not find old config file at '{path}'. Creating new file.".format(path=config_file_path))
        config = []

    # Reorder the config so that when rewriting the config, all data is ordered.
    ordered_config = []
    logging.debug("Ordering all blocks in config.")
    for unordered_dict in config:
        ordered_config.append(order_dict(unordered_dict))
    config = ordered_config
    logging.debug("Finished ordering all blocks in config.")

    # Write a backup of the current config file to config_file_path.bak.
    logging.debug("Writing backup config to '{path}'.".format(path=config_file_path + ".bak"))
    with open(config_file_path + ".bak", 'w') as config_file_backup:
        json.dump(config, config_file_backup, indent=4) # Tab in the lines to make it pretty.
    
    # Choose the write mode whether an old block should be edited or a new block added.
    logging.debug("Checking if new data has same start shot as pre-existing data.")
    for block_index, data_block in enumerate(config):
        if data_to_write["start_shot"] == data_block["start_shot"]:
            logging.info("Found old config block with start_shot {shot_num} (index {i}). Will edit this block.".format(shot_num=data_to_write["start_shot"], i=block_index))
            # Edit the block by replacing the old block with the new block.
            config[block_index] = data_to_write

            break
    else:
        logging.info("Did not find pre-existing start_shot {shot_num}. Will add new block.".format(shot_num=data_to_write["start_shot"]))
        config.append(data_to_write)

    logging.debug("Writing to config file.")
    with open(config_file_path, 'w') as config_file:
        json.dump(config, config_file, indent=4) # Tab in the lines to make it pretty.
    logging.info("Succesfully wrote new config data to '{path}'.".format(path=config_file_path))

def order_dict(unordered_dict):
    """
    Change a dictionary object into an ordered dictionary object.
    """
    ordered_dict = OrderedDict()
    for key in sorted(list(unordered_dict.keys())):
        if isinstance(unordered_dict[key], dict):
            ordered_dict[key] = order_dict(unordered_dict[key])
        else:
            ordered_dict[key] = unordered_dict[key]

    return ordered_dict


if __name__ == "__main__":
    # Log to a file rewriting the file each time.
    log_file_path = os.path.join(os.path.realpath(os.path.dirname(__file__)), "flux_bdot2_write_config.log")
    logging.basicConfig(filename=log_file_path, filemode="w", level=logging.DEBUG)

    # Mary's orientations and positions. These are for probes from left to right on the PCB.
    mary_probe_orientation_1 = np.array([
        -0.008850000000000, -0.215350000000000, 0.001250000000000
    ]) # Probe pointed in the db2 direction.
    mary_probe_on_pcb_position_1 = np.array([
        0.329745797984147, 0.104577829842807, 0.027538206843061
    ])
    mary_probe_orientation_2 = np.array([
        0.011800000000000, -0.008850000000000, -0.473750000000000
    ]) # Probe pointed in the db3 direction. This is never used.
    mary_probe_on_pcb_position_2 = np.array([
        0.809494913475074, 0.101291846547378, 0.026451242148188
    ])
    mary_probe_orientation_3 = np.array([
        -0.159300000000000, 0.014750000000000, -0.004375000000000
    ]) # Probe pointed in the db1 direction.
    mary_probe_on_pcb_position_3 = np.array([
        1.333907863140271, 0.099344381572869, 0.028218802444273
    ])
    mary_probe_orientation_4 = np.array([
        0.011800000000000, -0.008850000000000, -0.473750000000000
    ]) # Another probe pointed in the db3 direction. This is the probe that is actually used.
    # TODO: Figure out the correct X coordinate for this probe.
    mary_probe_on_pcb_position_4 = np.array([
        1.809494913475074, 0.101291846547378, 0.026451242148188
    ])

    # Convert the normal areas from inch**2 to m**2.
    mary_probe_orientation_1 *= 0.0254**2
    mary_probe_orientation_2 *= 0.0254**2
    mary_probe_orientation_3 *= 0.0254**2
    mary_probe_orientation_4 *= 0.0254**2
    # Convert the positions from inch to m.
    mary_probe_on_pcb_position_1 *= 0.0254
    mary_probe_on_pcb_position_2 *= 0.0254
    mary_probe_on_pcb_position_3 *= 0.0254
    mary_probe_on_pcb_position_4 *= 0.0254

    # Divide the orientation by a factor of 2. Not sure why but maybe something to do with Mary's calculation.
    mary_probe_orientation_1 /= 2
    mary_probe_orientation_2 /= 2
    mary_probe_orientation_3 /= 2
    mary_probe_orientation_4 /= 2

    # Change the position origin so that y = 0 is the center of the probe in the short direction on the face and z = 0 is the center of the probe through the silicon.
    origin_correction = -0.0254 * np.array([0, 0.2 / 2, 0.059 / 2])
    mary_probe_on_pcb_position_1 += origin_correction
    mary_probe_on_pcb_position_2 += origin_correction
    mary_probe_on_pcb_position_3 += origin_correction
    mary_probe_on_pcb_position_4 += origin_correction

    mary_probe_normalized_area_1 = np.linalg.norm(mary_probe_orientation_1)
    mary_probe_normalized_area_2 = np.linalg.norm(mary_probe_orientation_2)
    mary_probe_normalized_area_3 = np.linalg.norm(mary_probe_orientation_3)
    mary_probe_normalized_area_4 = np.linalg.norm(mary_probe_orientation_4)

    resistor_1 = 510 # Ohms
    resistor_2 = 82 # Ohms
    resistor_wire = 30 # Ohms
    resistor_divider_gain = (resistor_1 + resistor_2 + resistor_wire) / resistor_1
    # Get the gain for each coil.
    db1_gain = resistor_divider_gain / mary_probe_normalized_area_3
    db2_gain = resistor_divider_gain / mary_probe_normalized_area_1
    db3_gain = resistor_divider_gain / mary_probe_normalized_area_2

    # Convert the probes from Mary's coordinate system to ours.
    # Our coordinate system is: x=along stick from manifold, y=perpendicular to x and z, z=along manifold from flange
    db1_orientation = mary_probe_orientation_3 / mary_probe_normalized_area_3
    db2_orientation = mary_probe_orientation_1 / mary_probe_normalized_area_1
    db3_orientation = mary_probe_orientation_2 / mary_probe_normalized_area_2
    mary_to_stick_coords_index_transform = np.array([0, 1, 2])
    mary_to_stick_coords_reflection_transform = np.array([1, 1, 1])

    for orientation in [db1_orientation, db2_orientation, db3_orientation]:
        orientation = mary_to_stick_coords_reflection_transform * orientation[mary_to_stick_coords_index_transform]

    db1_on_pcb_position = mary_to_stick_coords_reflection_transform * mary_probe_on_pcb_position_3[mary_to_stick_coords_index_transform]
    db2_on_pcb_position = mary_to_stick_coords_reflection_transform * mary_probe_on_pcb_position_1[mary_to_stick_coords_index_transform]
    db3_on_pcb_position = mary_to_stick_coords_reflection_transform * mary_probe_on_pcb_position_2[mary_to_stick_coords_index_transform]

    db3_to_db3_x_separation = 0.051
    manifold_axis_to_pcb_start_x_separation = 0.031
    stick_to_stick_z_separation = 0.12
    manifold_flange_to_stick_8_z_separation = 1.005 - 0.84

    # Get the locations of the probes from closest to furthest from the manifold.
    db1_locations = np.zeros([NUM_STICKS, len(DB_PROBE_NUMBER_ASSIGNMENTS[1]), 3])
    db2_locations = np.zeros([NUM_STICKS, len(DB_PROBE_NUMBER_ASSIGNMENTS[2]), 3])
    db3_locations = np.zeros([NUM_STICKS, len(DB_PROBE_NUMBER_ASSIGNMENTS[3]), 3])
    for stick_index, stick_number in enumerate(range(1, NUM_STICKS + 1)):
        # Stick 1 is the stick furthest from the flange.
        stick_z_position = (NUM_STICKS - stick_number) * stick_to_stick_z_separation + manifold_flange_to_stick_8_z_separation
        # We assume the PCBs are centered in the sticks.
        first_pcb_origin_location = np.array([manifold_axis_to_pcb_start_x_separation, 0, stick_z_position])

        # TODO: Figure out if this is the correct PCB #.
        db1_pcb_numbers = np.arange(3, 13)
        db2_pcb_numbers = np.arange(3, 13)
        db3_pcb_numbers = np.arange(1, 13)

        db1_locations[stick_index] = (db1_on_pcb_position + first_pcb_origin_location)[np.newaxis, :] + db1_pcb_numbers[:, np.newaxis] * np.array([db3_to_db3_x_separation, 0, 0])[np.newaxis, :]
        db2_locations[stick_index] = (db2_on_pcb_position + first_pcb_origin_location)[np.newaxis, :] + db2_pcb_numbers[:, np.newaxis] * np.array([db3_to_db3_x_separation, 0, 0])[np.newaxis, :]
        db3_locations[stick_index] = (db3_on_pcb_position + first_pcb_origin_location)[np.newaxis, :] + db3_pcb_numbers[:, np.newaxis] * np.array([db3_to_db3_x_separation, 0, 0])[np.newaxis, :]

    db1_to_probe_number_transform = DB_PROBE_NUMBER_ASSIGNMENTS[1]
    db2_to_probe_number_transform = DB_PROBE_NUMBER_ASSIGNMENTS[2]
    db3_to_probe_number_transform = DB_PROBE_NUMBER_ASSIGNMENTS[3]
    
    def probe_number_to_orientation_dict(probe_number):
        """Get the orientation for a specific probe number on any stick."""
        if probe_number in db1_to_probe_number_transform:
            orientation = db1_orientation
        elif probe_number in db2_to_probe_number_transform:
            orientation = db2_orientation
        elif probe_number in db3_to_probe_number_transform:
            orientation = db3_orientation

        return {direction: coordinate for direction, coordinate in zip(['x', 'y', 'z'], orientation)}
    
    def probe_and_stick_number_to_position_dict(probe_number, stick_number):
        """Get the position for a specific probe number and stick number."""
        if probe_number in db1_to_probe_number_transform:
            position = db1_locations[stick_number - 1, np.where(db1_to_probe_number_transform == probe_number)[0][0]]
        elif probe_number in db2_to_probe_number_transform:
            position = db2_locations[stick_number - 1, np.where(db2_to_probe_number_transform == probe_number)[0][0]]
        elif probe_number in db3_to_probe_number_transform:
            position = db3_locations[stick_number - 1, np.where(db3_to_probe_number_transform == probe_number)[0][0]]

        return {direction: coordinate for direction, coordinate in zip(['x', 'y', 'z'], position)}

    db3_relative_gains = np.ones((NUM_STICKS, len(db3_to_probe_number_transform)))

    def gain_factor(probe_number, stick_number):
        """Get the gain for a specific probe number and stick number."""
        if probe_number in db1_to_probe_number_transform:
            gain = db1_gain
        elif probe_number in db2_to_probe_number_transform:
            gain = db2_gain
        else:
            gain = db3_gain * db3_relative_gains[stick_number - 1, np.where(db3_to_probe_number_transform == probe_number)[0][0]]

        return gain
    
    # Change this dictionary to add/edit the config.
    # If a 'None' value is set either as the element or as the first element of the tuple then nothing is written into the MDSplus table.
    data_to_write = {
        "start_shot": 62429,
        "flange": { # The coordinates below are the position and orientation of the flange in port coordinates when `insert` and `clocking` is 0.
            "position": {
                "port_coords": {
                    # Distance from flange to insertion axis.
                    # TODO: Determine these distances for when the dipole is in.
                    "x": -0.249,
                    "y": 0,
                    "z": 1.390 - 1.005 + 6 * 0.0254, # Last term is because of extra 6 inch garage.
                }
            },
            "orientation": {
                "port_coords": {
                    # Orientation of the manifold off the flange.
                    "x": 0,
                    "y": 1,
                    "z": 0,
                }
            },
        },
        "sticks": {
            "orientation": {
                "port_coords": {
                    "x": -1,
                    "y": 0,
                    "z": 0,
                },
            },
            "stick1": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta323_01", "V"),
                        "gain": (gain_factor(1, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 1),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta323_02", "V"),
                        "gain": (gain_factor(2, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 1),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta323_03", "V"),
                        "gain": (gain_factor(3, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 1),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta323_04", "V"),
                        "gain": (gain_factor(4, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 1),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta323_05", "V"),
                        "gain": (gain_factor(5, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 1),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta323_06", "V"),
                        "gain": (gain_factor(6, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 1),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta323_07", "V"),
                        "gain": (gain_factor(7, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 1),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta323_08", "V"),
                        "gain": (gain_factor(8, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 1),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta323_09", "V"),
                        "gain": (gain_factor(9, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 1),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta323_10", "V"),
                        "gain": (gain_factor(10, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 1),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta323_11", "V"),
                        "gain": (gain_factor(11, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 1),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta323_12", "V"),
                        "gain": (gain_factor(12, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 1),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta323_13", "V"),
                        "gain": (gain_factor(13, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 1),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta323_14", "V"),
                        "gain": (gain_factor(14, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 1),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta323_15", "V"),
                        "gain": (gain_factor(15, 1), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 1),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta323_16", "V"),
                        "gain": (gain_factor(16, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 1),
                        },
                    },
                    "pb17": {
                        "raw": ("\\ta323_17", "V"),
                        "gain": (gain_factor(17, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(17),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(17, 1),
                        },
                    },
                    "pb18": {
                        "raw": ("\\ta323_18", "V"),
                        "gain": (gain_factor(18, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(18),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(18, 1),
                        },
                    },
                    "pb19": {
                        "raw": ("\\ta323_19", "V"),
                        "gain": (gain_factor(19, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(19),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(19, 1),
                        },
                    },
                    "pb20": {
                        "raw": ("\\ta323_20", "V"),
                        "gain": (gain_factor(20, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(20),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(20, 1),
                        },
                    },
                    "pb21": {
                        "raw": ("\\ta323_21", "V"),
                        "gain": (gain_factor(21, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(21),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(21, 1),
                        },
                    },
                    "pb22": {
                        "raw": ("\\ta323_22", "V"),
                        "gain": (gain_factor(22, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(22),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(22, 1),
                        },
                    },
                    "pb23": {
                        "raw": ("\\ta323_23", "V"),
                        "gain": (gain_factor(23, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(23),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(23, 1),
                        },
                    },
                    "pb24": {
                        "raw": ("\\ta323_24", "V"),
                        "gain": (gain_factor(24, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(24),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(24, 1),
                        },
                    },
                    "pb25": {
                        "raw": ("\\ta323_25", "V"),
                        "gain": (gain_factor(25, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(25),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(25, 1),
                        },
                    },
                    "pb26": {
                        "raw": ("\\ta323_26", "V"),
                        "gain": (gain_factor(26, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(26),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(26, 1),
                        },
                    },
                    "pb27": {
                        "raw": ("\\ta323_27", "V"),
                        "gain": (gain_factor(27, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(27),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(27, 1),
                        },
                    },
                    "pb28": {
                        "raw": ("\\ta323_28", "V"),
                        "gain": (gain_factor(28, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(28),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(28, 1),
                        },
                    },
                    "pb29": {
                        "raw": ("\\ta323_29", "V"),
                        "gain": (gain_factor(29, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(29),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(29, 1),
                        },
                    },
                    "pb30": {
                        "raw": ("\\ta323_30", "V"),
                        "gain": (gain_factor(30, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(30),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(30, 1),
                        },
                    },
                    "pb31": {
                        "raw": ("\\ta323_31", "V"),
                        "gain": (gain_factor(31, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(31),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(31, 1),
                        },
                    },
                    "pb32": {
                        "raw": ("\\ta323_32", "V"),
                        "gain": (gain_factor(32, 1), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(32),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(32, 1),
                        },
                    },
                },
            }, 
            "stick2": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta324_01", "V"),
                        "gain": (gain_factor(1, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 2),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta324_02", "V"),
                        "gain": (gain_factor(2, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 2),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta324_03", "V"),
                        "gain": (gain_factor(3, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 2),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta324_04", "V"),
                        "gain": (gain_factor(4, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 2),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta324_05", "V"),
                        "gain": (gain_factor(5, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 2),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta324_06", "V"),
                        "gain": (gain_factor(6, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 2),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta324_07", "V"),
                        "gain": (gain_factor(7, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 2),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta324_08", "V"),
                        "gain": (gain_factor(8, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 2),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta324_09", "V"),
                        "gain": (gain_factor(9, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 2),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta324_10", "V"),
                        "gain": (gain_factor(10, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 2),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta324_11", "V"),
                        "gain": (gain_factor(11, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 2),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta324_12", "V"),
                        "gain": (gain_factor(12, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 2),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta324_13", "V"),
                        "gain": (gain_factor(13, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 2),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta324_14", "V"),
                        "gain": (gain_factor(14, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 2),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta324_15", "V"),
                        "gain": (gain_factor(15, 2), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 2),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta324_16", "V"),
                        "gain": (gain_factor(16, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 2),
                        },
                    },
                    "pb17": {
                        "raw": ("\\ta324_17", "V"),
                        "gain": (gain_factor(17, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(17),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(17, 2),
                        },
                    },
                    "pb18": {
                        "raw": ("\\ta324_18", "V"),
                        "gain": (gain_factor(18, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(18),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(18, 2),
                        },
                    },
                    "pb19": {
                        "raw": ("\\ta324_19", "V"),
                        "gain": (gain_factor(19, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(19),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(19, 2),
                        },
                    },
                    "pb20": {
                        "raw": ("\\ta324_20", "V"),
                        "gain": (gain_factor(20, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(20),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(20, 2),
                        },
                    },
                    "pb21": {
                        "raw": ("\\ta324_21", "V"),
                        "gain": (gain_factor(21, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(21),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(21, 2),
                        },
                    },
                    "pb22": {
                        "raw": ("\\ta324_22", "V"),
                        "gain": (gain_factor(22, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(22),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(22, 2),
                        },
                    },
                    "pb23": {
                        "raw": ("\\ta324_23", "V"),
                        "gain": (gain_factor(23, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(23),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(23, 2),
                        },
                    },
                    "pb24": {
                        "raw": ("\\ta324_24", "V"),
                        "gain": (gain_factor(24, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(24),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(24, 2),
                        },
                    },
                    "pb25": {
                        "raw": ("\\ta324_25", "V"),
                        "gain": (gain_factor(25, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(25),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(25, 2),
                        },
                    },
                    "pb26": {
                        "raw": ("\\ta324_26", "V"),
                        "gain": (gain_factor(26, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(26),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(26, 2),
                        },
                    },
                    "pb27": {
                        "raw": ("\\ta324_27", "V"),
                        "gain": (gain_factor(27, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(27),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(27, 2),
                        },
                    },
                    "pb28": {
                        "raw": ("\\ta324_28", "V"),
                        "gain": (gain_factor(28, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(28),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(28, 2),
                        },
                    },
                    "pb29": {
                        "raw": ("\\ta324_29", "V"),
                        "gain": (gain_factor(29, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(29),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(29, 2),
                        },
                    },
                    "pb30": {
                        "raw": ("\\ta324_30", "V"),
                        "gain": (gain_factor(30, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(30),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(30, 2),
                        },
                    },
                    "pb31": {
                        "raw": ("\\ta324_31", "V"),
                        "gain": (gain_factor(31, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(31),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(31, 2),
                        },
                    },
                    "pb32": {
                        "raw": ("\\ta324_32", "V"),
                        "gain": (gain_factor(32, 2), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(32),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(32, 2),
                        },
                    },
                },
            }, 
            "stick3": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta325_01", "V"),
                        "gain": (gain_factor(1, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 3),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta325_02", "V"),
                        "gain": (gain_factor(2, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 3),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta325_03", "V"),
                        "gain": (gain_factor(3, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 3),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta325_04", "V"),
                        "gain": (gain_factor(4, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 3),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta325_05", "V"),
                        "gain": (gain_factor(5, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 3),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta325_06", "V"),
                        "gain": (gain_factor(6, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 3),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta325_07", "V"),
                        "gain": (gain_factor(7, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 3),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta325_08", "V"),
                        "gain": (gain_factor(8, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 3),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta325_09", "V"),
                        "gain": (gain_factor(9, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 3),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta325_10", "V"),
                        "gain": (gain_factor(10, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 3),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta325_11", "V"),
                        "gain": (gain_factor(11, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 3),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta325_12", "V"),
                        "gain": (gain_factor(12, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 3),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta325_13", "V"),
                        "gain": (gain_factor(13, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 3),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta325_14", "V"),
                        "gain": (gain_factor(14, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 3),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta325_15", "V"),
                        "gain": (gain_factor(15, 3), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 3),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta325_16", "V"),
                        "gain": (gain_factor(16, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 3),
                        },
                    },
                    "pb17": {
                        "raw": ("\\ta325_17", "V"),
                        "gain": (gain_factor(17, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(17),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(17, 3),
                        },
                    },
                    "pb18": {
                        "raw": ("\\ta325_18", "V"),
                        "gain": (gain_factor(18, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(18),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(18, 3),
                        },
                    },
                    "pb19": {
                        "raw": ("\\ta325_19", "V"),
                        "gain": (gain_factor(19, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(19),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(19, 3),
                        },
                    },
                    "pb20": {
                        "raw": ("\\ta325_20", "V"),
                        "gain": (gain_factor(20, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(20),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(20, 3),
                        },
                    },
                    "pb21": {
                        "raw": ("\\ta325_21", "V"),
                        "gain": (gain_factor(21, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(21),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(21, 3),
                        },
                    },
                    "pb22": {
                        "raw": ("\\ta325_22", "V"),
                        "gain": (gain_factor(22, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(22),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(22, 3),
                        },
                    },
                    "pb23": {
                        "raw": ("\\ta325_23", "V"),
                        "gain": (gain_factor(23, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(23),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(23, 3),
                        },
                    },
                    "pb24": {
                        "raw": ("\\ta325_24", "V"),
                        "gain": (gain_factor(24, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(24),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(24, 3),
                        },
                    },
                    "pb25": {
                        "raw": ("\\ta325_25", "V"),
                        "gain": (gain_factor(25, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(25),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(25, 3),
                        },
                    },
                    "pb26": {
                        "raw": ("\\ta325_26", "V"),
                        "gain": (gain_factor(26, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(26),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(26, 3),
                        },
                    },
                    "pb27": {
                        "raw": ("\\ta325_27", "V"),
                        "gain": (gain_factor(27, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(27),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(27, 3),
                        },
                    },
                    "pb28": {
                        "raw": ("\\ta325_28", "V"),
                        "gain": (gain_factor(28, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(28),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(28, 3),
                        },
                    },
                    "pb29": {
                        "raw": ("\\ta325_29", "V"),
                        "gain": (gain_factor(29, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(29),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(29, 3),
                        },
                    },
                    "pb30": {
                        "raw": ("\\ta325_30", "V"),
                        "gain": (gain_factor(30, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(30),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(30, 3),
                        },
                    },
                    "pb31": {
                        "raw": ("\\ta325_31", "V"),
                        "gain": (gain_factor(31, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(31),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(31, 3),
                        },
                    },
                    "pb32": {
                        "raw": ("\\ta325_32", "V"),
                        "gain": (gain_factor(32, 3), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(32),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(32, 3),
                        },
                    },
                },
            }, 
            "stick4": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta326_01", "V"),
                        "gain": (gain_factor(1, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 4),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta326_02", "V"),
                        "gain": (gain_factor(2, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 4),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta326_03", "V"),
                        "gain": (gain_factor(3, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 4),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta326_04", "V"),
                        "gain": (gain_factor(4, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 4),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta326_05", "V"),
                        "gain": (gain_factor(5, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 4),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta326_06", "V"),
                        "gain": (gain_factor(6, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 4),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta326_07", "V"),
                        "gain": (gain_factor(7, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 4),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta326_08", "V"),
                        "gain": (gain_factor(8, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 4),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta326_09", "V"),
                        "gain": (gain_factor(9, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 4),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta326_10", "V"),
                        "gain": (gain_factor(10, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 4),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta326_11", "V"),
                        "gain": (gain_factor(11, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 4),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta326_12", "V"),
                        "gain": (gain_factor(12, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 4),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta326_13", "V"),
                        "gain": (gain_factor(13, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 4),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta326_14", "V"),
                        "gain": (gain_factor(14, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 4),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta326_15", "V"),
                        "gain": (gain_factor(15, 4), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 4),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta326_16", "V"),
                        "gain": (gain_factor(16, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 4),
                        },
                    },
                    "pb17": {
                        "raw": ("\\ta326_17", "V"),
                        "gain": (gain_factor(17, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(17),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(17, 4),
                        },
                    },
                    "pb18": {
                        "raw": ("\\ta326_18", "V"),
                        "gain": (gain_factor(18, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(18),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(18, 4),
                        },
                    },
                    "pb19": {
                        "raw": ("\\ta326_19", "V"),
                        "gain": (gain_factor(19, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(19),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(19, 4),
                        },
                    },
                    "pb20": {
                        "raw": ("\\ta326_20", "V"),
                        "gain": (gain_factor(20, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(20),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(20, 4),
                        },
                    },
                    "pb21": {
                        "raw": ("\\ta326_21", "V"),
                        "gain": (gain_factor(21, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(21),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(21, 4),
                        },
                    },
                    "pb22": {
                        "raw": ("\\ta326_22", "V"),
                        "gain": (gain_factor(22, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(22),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(22, 4),
                        },
                    },
                    "pb23": {
                        "raw": ("\\ta326_23", "V"),
                        "gain": (gain_factor(23, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(23),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(23, 4),
                        },
                    },
                    "pb24": {
                        "raw": ("\\ta326_24", "V"),
                        "gain": (gain_factor(24, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(24),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(24, 4),
                        },
                    },
                    "pb25": {
                        "raw": ("\\ta326_25", "V"),
                        "gain": (gain_factor(25, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(25),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(25, 4),
                        },
                    },
                    "pb26": {
                        "raw": ("\\ta326_26", "V"),
                        "gain": (gain_factor(26, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(26),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(26, 4),
                        },
                    },
                    "pb27": {
                        "raw": ("\\ta326_27", "V"),
                        "gain": (gain_factor(27, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(27),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(27, 4),
                        },
                    },
                    "pb28": {
                        "raw": ("\\ta326_28", "V"),
                        "gain": (gain_factor(28, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(28),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(28, 4),
                        },
                    },
                    "pb29": {
                        "raw": ("\\ta326_29", "V"),
                        "gain": (gain_factor(29, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(29),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(29, 4),
                        },
                    },
                    "pb30": {
                        "raw": ("\\ta326_30", "V"),
                        "gain": (gain_factor(30, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(30),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(30, 4),
                        },
                    },
                    "pb31": {
                        "raw": ("\\ta326_31", "V"),
                        "gain": (gain_factor(31, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(31),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(31, 4),
                        },
                    },
                    "pb32": {
                        "raw": ("\\ta326_32", "V"),
                        "gain": (gain_factor(32, 4), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(32),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(32, 4),
                        },
                    },
                },
            }, 
            "stick5": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta116_01", "V"),
                        "gain": (gain_factor(1, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 5),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta116_02", "V"),
                        "gain": (gain_factor(2, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 5),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta116_03", "V"),
                        "gain": (gain_factor(3, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 5),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta116_04", "V"),
                        "gain": (gain_factor(4, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 5),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta116_05", "V"),
                        "gain": (gain_factor(5, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 5),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta116_06", "V"),
                        "gain": (gain_factor(6, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 5),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta116_30", "V"),
                        "gain": (gain_factor(7, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 5),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta116_08", "V"),
                        "gain": (gain_factor(8, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 5),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta116_09", "V"),
                        "gain": (gain_factor(9, 5), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 5),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta116_10", "V"),
                        "gain": (gain_factor(10, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 5),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta116_11", "V"),
                        "gain": (gain_factor(11, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 5),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta116_07", "V"),
                        "gain": (gain_factor(12, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 5),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta116_13", "V"),
                        "gain": (gain_factor(13, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 5),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta116_14", "V"),
                        "gain": (gain_factor(14, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 5),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta116_18", "V"),
                        "gain": (gain_factor(15, 5), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 5),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta116_32", "V"),
                        "gain": (gain_factor(16, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 5),
                        },
                    },
                    "pb17": {
                        "raw": ("\\ta116_17", "V"),
                        "gain": (gain_factor(17, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(17),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(17, 5),
                        },
                    },
                    "pb18": {
                        "raw": ("\\ta116_12", "V"),
                        "gain": (gain_factor(18, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(18),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(18, 5),
                        },
                    },
                    "pb19": {
                        "raw": ("\\ta116_19", "V"),
                        "gain": (gain_factor(19, 5), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(19),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(19, 5),
                        },
                    },
                    "pb20": {
                        "raw": ("\\ta116_20", "V"),
                        "gain": (gain_factor(20, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(20),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(20, 5),
                        },
                    },
                    "pb21": {
                        "raw": ("\\ta116_21", "V"),
                        "gain": (gain_factor(21, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(21),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(21, 5),
                        },
                    },
                    "pb22": {
                        "raw": ("\\ta116_16", "V"),
                        "gain": (gain_factor(22, 5), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(22),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(22, 5),
                        },
                    },
                    "pb23": {
                        "raw": ("\\ta116_23", "V"),
                        "gain": (gain_factor(23, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(23),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(23, 5),
                        },
                    },
                    "pb24": {
                        "raw": ("\\ta116_28", "V"),
                        "gain": (gain_factor(24, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(24),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(24, 5),
                        },
                    },
                    "pb25": {
                        "raw": ("\\ta116_26", "V"),
                        "gain": (gain_factor(25, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(25),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(25, 5),
                        },
                    },
                    "pb26": {
                        "raw": ("\\ta116_18", "V"),
                        "gain": (gain_factor(26, 5), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(26),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(26, 5),
                        },
                    },
                    "pb27": {
                        "raw": ("\\ta116_22", "V"),
                        "gain": (gain_factor(27, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(27),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(27, 5),
                        },
                    },
                    "pb28": {
                        "raw": ("\\ta116_27", "V"),
                        "gain": (gain_factor(28, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(28),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(28, 5),
                        },
                    },
                    "pb29": {
                        "raw": ("\\ta116_31", "V"),
                        "gain": (gain_factor(29, 5), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(29),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(29, 5),
                        },
                    },
                    "pb30": {
                        "raw": ("\\ta116_25", "V"),
                        "gain": (gain_factor(30, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(30),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(30, 5),
                        },
                    },
                    "pb31": {
                        "raw": ("\\ta116_29", "V"),
                        "gain": (gain_factor(31, 5), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(31),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(31, 5),
                        },
                    },
                    "pb32": {
                        "raw": ("\\ta116_24", "V"),
                        "gain": (gain_factor(32, 5), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(32),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(32, 5),
                        },
                    },
                },
            }, 
            "stick6": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta128_01", "V"),
                        "gain": (gain_factor(1, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 6),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta128_02", "V"),
                        "gain": (gain_factor(2, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 6),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta128_03", "V"),
                        "gain": (gain_factor(3, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 6),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta128_04", "V"),
                        "gain": (gain_factor(4, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 6),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta128_05", "V"),
                        "gain": (gain_factor(5, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 6),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta128_06", "V"),
                        "gain": (gain_factor(6, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 6),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta128_07", "V"),
                        "gain": (gain_factor(7, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 6),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta128_08", "V"),
                        "gain": (gain_factor(8, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 6),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta128_09", "V"),
                        "gain": (gain_factor(9, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 6),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta128_10", "V"),
                        "gain": (gain_factor(10, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 6),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta128_11", "V"),
                        "gain": (gain_factor(11, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 6),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta128_12", "V"),
                        "gain": (gain_factor(12, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 6),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta128_13", "V"),
                        "gain": (gain_factor(13, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 6),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta128_14", "V"),
                        "gain": (gain_factor(14, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 6),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta128_15", "V"),
                        "gain": (gain_factor(15, 6), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 6),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta128_16", "V"),
                        "gain": (gain_factor(16, 6), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 6),
                        },
                    },
                    "pb17": {
                        "raw": ("\\ta128_17", "V"),
                        "gain": (gain_factor(17, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(17),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(17, 6),
                        },
                    },
                    "pb18": {
                        "raw": ("\\ta128_18", "V"),
                        "gain": (gain_factor(18, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(18),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(18, 6),
                        },
                    },
                    "pb19": {
                        "raw": ("\\ta128_19", "V"),
                        "gain": (gain_factor(19, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(19),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(19, 6),
                        },
                    },
                    "pb20": {
                        "raw": ("\\ta128_20", "V"),
                        "gain": (gain_factor(20, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(20),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(20, 6),
                        },
                    },
                    "pb21": {
                        "raw": ("\\ta128_21", "V"),
                        "gain": (gain_factor(21, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(21),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(21, 6),
                        },
                    },
                    "pb22": {
                        "raw": ("\\ta128_22", "V"),
                        "gain": (gain_factor(22, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(22),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(22, 6),
                        },
                    },
                    "pb23": {
                        "raw": ("\\ta128_23", "V"),
                        "gain": (gain_factor(23, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(23),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(23, 6),
                        },
                    },
                    "pb24": {
                        "raw": ("\\ta128_24", "V"),
                        "gain": (gain_factor(24, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(24),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(24, 6),
                        },
                    },
                    "pb25": {
                        "raw": ("\\ta128_25", "V"),
                        "gain": (gain_factor(25, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(25),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(25, 6),
                        },
                    },
                    "pb26": {
                        "raw": ("\\ta128_26", "V"),
                        "gain": (gain_factor(26, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(26),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(26, 6),
                        },
                    },
                    "pb27": {
                        "raw": ("\\ta128_27", "V"),
                        "gain": (gain_factor(27, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(27),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(27, 6),
                        },
                    },
                    "pb28": {
                        "raw": ("\\ta128_28", "V"),
                        "gain": (gain_factor(28, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(28),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(28, 6),
                        },
                    },
                    "pb29": {
                        "raw": ("\\ta128_29", "V"),
                        "gain": (gain_factor(29, 6), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(29),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(29, 6),
                        },
                    },
                    "pb30": {
                        "raw": ("\\ta128_30", "V"),
                        "gain": (gain_factor(30, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(30),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(30, 6),
                        },
                    },
                    "pb31": {
                        "raw": ("\\ta128_31", "V"),
                        "gain": (gain_factor(31, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(31),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(31, 6),
                        },
                    },
                    "pb32": {
                        "raw": ("\\ta128_32", "V"),
                        "gain": (gain_factor(32, 6), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(32),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(32, 6),
                        },
                    },
                },
            }, 
            "stick7": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta320_01", "V"),
                        "gain": (gain_factor(1, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 7),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta320_02", "V"),
                        "gain": (gain_factor(2, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 7),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta320_03", "V"),
                        "gain": (gain_factor(3, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 7),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta320_04", "V"),
                        "gain": (gain_factor(4, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 7),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta320_05", "V"),
                        "gain": (gain_factor(5, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 7),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta320_06", "V"),
                        "gain": (gain_factor(6, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 7),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta320_21", "V"),
                        "gain": (gain_factor(7, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 7),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta320_08", "V"),
                        "gain": (gain_factor(8, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 7),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta320_09", "V"),
                        "gain": (gain_factor(9, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 7),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta320_10", "V"),
                        "gain": (gain_factor(10, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 7),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta320_11", "V"),
                        "gain": (gain_factor(11, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 7),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta320_12", "V"),
                        "gain": (gain_factor(12, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 7),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta320_13", "V"),
                        "gain": (gain_factor(13, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 7),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta320_14", "V"),
                        "gain": (gain_factor(14, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 7),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta320_15", "V"),
                        "gain": (gain_factor(15, 7), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 7),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta320_16", "V"),
                        "gain": (gain_factor(16, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 7),
                        },
                    },
                    "pb17": {
                        "raw": ("\\ta320_17", "V"),
                        "gain": (gain_factor(17, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(17),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(17, 7),
                        },
                    },
                    "pb18": {
                        "raw": ("\\ta320_18", "V"),
                        "gain": (gain_factor(18, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(18),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(18, 7),
                        },
                    },
                    "pb19": {
                        "raw": ("\\ta320_19", "V"),
                        "gain": (gain_factor(19, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(19),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(19, 7),
                        },
                    },
                    "pb20": {
                        "raw": ("\\ta320_20", "V"),
                        "gain": (gain_factor(20, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(20),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(20, 7),
                        },
                    },
                    "pb21": {
                        "raw": ("\\ta320_7", "V"),
                        "gain": (gain_factor(21, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(21),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(21, 7),
                        },
                    },
                    "pb22": {
                        "raw": ("\\ta320_22", "V"),
                        "gain": (gain_factor(22, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(22),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(22, 7),
                        },
                    },
                    "pb23": {
                        "raw": ("\\ta320_23", "V"),
                        "gain": (gain_factor(23, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(23),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(23, 7),
                        },
                    },
                    "pb24": {
                        "raw": ("\\ta320_24", "V"),
                        "gain": (gain_factor(24, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(24),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(24, 7),
                        },
                    },
                    "pb25": {
                        "raw": ("\\ta320_25", "V"),
                        "gain": (gain_factor(25, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(25),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(25, 7),
                        },
                    },
                    "pb26": {
                        "raw": ("\\ta320_26", "V"),
                        "gain": (gain_factor(26, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(26),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(26, 7),
                        },
                    },
                    "pb27": {
                        "raw": ("\\ta320_27", "V"),
                        "gain": (gain_factor(27, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(27),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(27, 7),
                        },
                    },
                    "pb28": {
                        "raw": ("\\ta320_28", "V"),
                        "gain": (gain_factor(28, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(28),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(28, 7),
                        },
                    },
                    "pb29": {
                        "raw": ("\\ta320_29", "V"),
                        "gain": (gain_factor(29, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(29),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(29, 7),
                        },
                    },
                    "pb30": {
                        "raw": ("\\ta320_30", "V"),
                        "gain": (gain_factor(30, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(30),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(30, 7),
                        },
                    },
                    "pb31": {
                        "raw": ("\\ta320_31", "V"),
                        "gain": (gain_factor(31, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(31),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(31, 7),
                        },
                    },
                    "pb32": {
                        "raw": ("\\ta320_32", "V"),
                        "gain": (gain_factor(32, 7), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(32),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(32, 7),
                        },
                    },
                },
            }, 
            "stick8": {
                "probes": {
                    "pb01": {
                        "raw": ("\\ta129_01", "V"),
                        "gain": (gain_factor(1, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(1),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(1, 8),
                        },
                    },
                    "pb02": {
                        "raw": ("\\ta129_02", "V"),
                        "gain": (gain_factor(2, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(2),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(2, 8),
                        },
                    },
                    "pb03": {
                        "raw": ("\\ta129_03", "V"),
                        "gain": (gain_factor(3, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(3),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(3, 8),
                        },
                    },
                    "pb04": {
                        "raw": ("\\ta129_04", "V"),
                        "gain": (gain_factor(4, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(4),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(4, 8),
                        },
                    },
                    "pb05": {
                        "raw": ("\\ta129_05", "V"),
                        "gain": (gain_factor(5, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(5),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(5, 8),
                        },
                    },
                    "pb06": {
                        "raw": ("\\ta129_06", "V"),
                        "gain": (gain_factor(6, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(6),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(6, 8),
                        },
                    },
                    "pb07": {
                        "raw": ("\\ta129_07", "V"),
                        "gain": (gain_factor(7, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(7),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(7, 8),
                        },
                    },
                    "pb08": {
                        "raw": ("\\ta129_08", "V"),
                        "gain": (gain_factor(8, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(8),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(8, 8),
                        },
                    },
                    "pb09": {
                        "raw": ("\\ta129_09", "V"),
                        "gain": (gain_factor(9, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(9),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(9, 8),
                        },
                    },
                    "pb10": {
                        "raw": ("\\ta129_10", "V"),
                        "gain": (gain_factor(10, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(10),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(10, 8),
                        },
                    },
                    "pb11": {
                        "raw": ("\\ta129_11", "V"),
                        "gain": (gain_factor(11, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(11),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(11, 8),
                        },
                    },
                    "pb12": {
                        "raw": ("\\ta129_12", "V"),
                        "gain": (gain_factor(12, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(12),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(12, 8),
                        },
                    },
                    "pb13": {
                        "raw": ("\\ta129_13", "V"),
                        "gain": (gain_factor(13, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(13),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(13, 8),
                        },
                    },
                    "pb14": {
                        "raw": ("\\ta129_29", "V"),
                        "gain": (gain_factor(14, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(14),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(14, 8),
                        },
                    },
                    "pb15": {
                        "raw": ("\\ta129_15", "V"),
                        "gain": (gain_factor(15, 8), "T / (s V)"),
                        "working": False,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(15),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(15, 8),
                        },
                    },
                    "pb16": {
                        "raw": ("\\ta129_16", "V"),
                        "gain": (gain_factor(16, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(16),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(16, 8),
                        },
                    },
                    "pb17": {
                        "raw": ("\\ta129_17", "V"),
                        "gain": (gain_factor(17, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(17),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(17, 8),
                        },
                    },
                    "pb18": {
                        "raw": ("\\ta129_18", "V"),
                        "gain": (gain_factor(18, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(18),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(18, 8),
                        },
                    },
                    "pb19": {
                        "raw": ("\\ta129_19", "V"),
                        "gain": (gain_factor(19, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(19),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(19, 8),
                        },
                    },
                    "pb20": {
                        "raw": ("\\ta129_20", "V"),
                        "gain": (gain_factor(20, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(20),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(20, 8),
                        },
                    },
                    "pb21": {
                        "raw": ("\\ta129_21", "V"),
                        "gain": (gain_factor(21, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(21),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(21, 8),
                        },
                    },
                    "pb22": {
                        "raw": ("\\ta129_22", "V"),
                        "gain": (gain_factor(22, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(22),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(22, 8),
                        },
                    },
                    "pb23": {
                        "raw": ("\\ta129_23", "V"),
                        "gain": (gain_factor(23, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(23),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(23, 8),
                        },
                    },
                    "pb24": {
                        "raw": ("\\ta129_24", "V"),
                        "gain": (gain_factor(24, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(24),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(24, 8),
                        },
                    },
                    "pb25": {
                        "raw": ("\\ta129_25", "V"),
                        "gain": (gain_factor(25, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(25),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(25, 8),
                        },
                    },
                    "pb26": {
                        "raw": ("\\ta129_26", "V"),
                        "gain": (gain_factor(26, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(26),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(26, 8),
                        },
                    },
                    "pb27": {
                        "raw": ("\\ta129_27", "V"),
                        "gain": (gain_factor(27, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(27),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(27, 8),
                        },
                    },
                    "pb28": {
                        "raw": ("\\ta129_28", "V"),
                        "gain": (gain_factor(28, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(28),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(28, 8),
                        },
                    },
                    "pb29": {
                        "raw": ("\\ta129_14", "V"),
                        "gain": (gain_factor(29, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": -1 * probe_number_to_orientation_dict(29),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(29, 8),
                        },
                    },
                    "pb30": {
                        "raw": ("\\ta129_30", "V"),
                        "gain": (gain_factor(30, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(30),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(30, 8),
                        },
                    },
                    "pb31": {
                        "raw": ("\\ta129_31", "V"),
                        "gain": (gain_factor(31, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(31),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(31, 8),
                        },
                    },
                    "pb32": {
                        "raw": ("\\ta129_32", "V"),
                        "gain": (gain_factor(32, 8), "T / (s V)"),
                        "working": True,
                        "orientation": {
                            "stick_coords": probe_number_to_orientation_dict(32),
                        },
                        "position": {
                            "stick_coords": probe_and_stick_number_to_position_dict(32, 8),
                        },
                    },
                },
            }, 
        },
    }

    logging.debug("Reordering data to write for ease of reading.")
    ordered_data_to_write = order_dict(data_to_write)
    config_file_path = os.path.join(os.path.realpath(os.path.dirname(__file__)), "configs/flux_bdot2.json")

    write_json(config_file_path, ordered_data_to_write)
