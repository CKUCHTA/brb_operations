"""
Populates MDSplus tree for flux_bdot2 with all necessary data from MySQL and the config file.
"""
import logging
import warnings
import os.path
import json
import modules.mds_builders
import sys
import MDSplus as mds
import MDSplus.mdsExceptions as mds_exceptions
from tree_configs.flux_bdot1.post_shot_config_edit import edit_config


def get_shot_config(shot_number, probe_config_path=os.path.join(os.path.realpath(os.path.dirname(__file__)), "configs/flux_bdot2.json")):
    """
    Get the data block for this shot by using the block with the maximum start_shot that is less than the queried shot.

    Parameters
    ----------
    shot_number : int
        The number of the shot we want the config of.
    probe_config_path : str, default='./configs/flux_bdot2.json'
        The path to the probe config file.

    Returns
    -------
    data_block : dict
        The data block from the config file that should be used for this shot.
    """
    try:
        with open(probe_config_path, 'r') as config_file:
            logging.debug("Succesfully opened '{path}'.".format(path=probe_config_path))
            config = json.load(config_file)
    except FileNotFoundError:
        logging.error("Could not find flux_bdot2 config file at '{path}'.".format(path=probe_config_path))
        raise

    # Loop through the data blocks and find the block that has the latest start_shot before the queried shot.
    curr_start_shot = -1
    curr_block_index = -1
    for block_index, data_block in enumerate(config):
        block_start_shot = data_block["start_shot"] 
        if block_start_shot <= shot_number and block_start_shot > curr_start_shot:
            curr_start_shot = block_start_shot
            curr_block_index = block_index

    # If we didn't find any valid data blocks for this shot then raise an error.
    if curr_start_shot == -1:
        logging.exception("Couldn't find any data blocks in flux_bdot2 config that are valid for shot {num}.".format(num=shot_number))
        raise ValueError("No valid config data block in '{path}' for shot {num}.".format(path=probe_config_path, num=shot_number))
    # Otherwise return the data_block for this shot.
    else:
        logging.info("Found valid data block with starting shot {start} (block {index}).".format(start=curr_start_shot, index=curr_block_index))
        return config[curr_block_index]

def insert_data(tree, probe_top_node, data):
    """
    Put data into the flux_bdot2 tree ignoring 'None' values.

    Parameters
    ----------
    tree : mds.Tree
        Tree to insert into.
    probe_top_node : mds.TreeNode
        Top of the probe tree.
    data : dict[str, dict[...]]
        Data to insert into the tree. The data should be nested dictionaries where each key is the name of a node and sub dictionaries contain the nodes children. Each value is either a single value or a list of length 2 where the first is a float and the second are the units of the data as a string.
    """
    def recursive_insert(tree, curr_node, curr_data, position_str=''):
        """
        Insert data in this sub-dictionary into the tree treating the curr_node as the top of the curr_data.
        """
        for node_name, node_data in curr_data.items():
            # Set the tree to the current node. We do this each iteration because the tree might change position during recursion.
            tree.setDefault(curr_node)

            # Try to get the node if it exists. If not then skip the insert and warn the user.
            curr_node_name = curr_node.getNodeName()
            curr_position = position_str + '.' + curr_node_name
            logging.debug(f"Trying to get node '{node_name}' of node '{curr_position}'.")
            try:
                node = tree.getNode(node_name)
            except mds_exceptions.TreeNNF as e:
                warning_str = f"Could not find node '{node_name}' of node '{curr_position}'. Skipping inserts for this node and sub-nodes."
                logging.warning(warning_str)
                warnings.warn(warning_str)
                continue
            except:
                logging.exception(f"An unhandled exception occured while getting node '{node_name}' of node '{curr_position}'. Exception was:")
                raise
            
            # If the data is a dictionary then recursively insert the dictionary with a new node.
            if isinstance(node_data, dict):
                logging.debug(f"Inserting '{node_name}' sub-dict for node '{curr_position}'.")
                recursive_insert(tree, node, node_data, position_str=curr_position)
            # If the data is a list then the first value is the data and the second is the units.
            elif isinstance(node_data, list) or isinstance(node_data, tuple):
                value, units = node_data
                if value is None:
                    logging.debug(f"Not inserting into '{node_name}' for node '{curr_position}' since data value is 'None' (units are '{units}').")
                else:
                    logging.debug(f"Inserting into '{node_name}', a value of '{value}' '{units}' for node '{curr_position}'.")
                    modules.mds_builders.put_wrapper(node, modules.mds_builders.to_mds_datatype(node_data[0]), units=node_data[1])
            # Otherwise just insert the data.
            else:
                if node_data is None:
                    logging.debug(f"Not inserting into '{node_name}' for node '{curr_position}' since data value is 'None' (no units).")
                else:
                    logging.debug(f"Inserting into '{node_name}', a value of '{node_data}' (no units) for node '{curr_position}'.")
                    if isinstance(node_data, mds.compound.Signal):
                        # Don't change compiled signals into a different type of data.
                        modules.mds_builders.put_wrapper(node, node_data)
                    else:
                        modules.mds_builders.put_wrapper(node, modules.mds_builders.to_mds_datatype(node_data))
        logging.info(f"Inserted sub-tree of '{curr_position}'.")

    # Recursively insert the data starting from the top.
    logging.debug("Starting to insert data.")
    recursive_insert(tree, probe_top_node, data)
    logging.debug("Finished inserting data.")

def main(shot_number):
    """
    Get data from MySQL and the flux_bdot2 config and write that data into the MDSplus tree.

    Parameters
    ----------
    shot_number : int
    """
    logging.info("Running post-run procedure for shot {num}.".format(num=shot_number))

    # Open the wipal tree and get the top of the flux_bdot2 tree.
    logging.debug("Opening MDSplus wipal tree.")
    wipal_tree = mds.Tree("wipal", shot_number)
    flux_bdot2_top_node = wipal_tree.getNode("magnetics.flux_bdot2")

    # Get the config file for the shot.
    shot_config = get_shot_config(shot_number)
    try:
        shot_config = edit_config(shot_config, shot_number)
    except:
        logging.exception("******* Error occurred while editing config. Could not update config. *******")

    # Insert the data into the tree.
    logging.debug("Inserting config data into MDSplus.")
    insert_data(wipal_tree, flux_bdot2_top_node, shot_config)

    logging.info("Finished running post-run procedure for shot {num}.".format(num=shot_number))


if __name__=="__main__":
    # Log to a file rewriting the file each time.
    log_file_path = os.path.join(os.path.realpath(os.path.dirname(__file__)), "flux_bdot2_run_proc.log")
    logging.basicConfig(filename=log_file_path, filemode="w", level=logging.DEBUG)

    shot_number = int(sys.argv[1])
    main(shot_number)

