#!/usr/bin/python3
import numpy as np
import pandas as pio
import MDSplus as mds
import modules.mds_builders as mdsbuild
from modules.mysql_connection import get_engine
import argparse

"""
TODO: Need to add an I offset node (not sure what we should name it)
      After that, I can fix the calculate offsets code
"""

def main(shot_num):
    wipal_tree = mds.Tree("wipal", shot_num)
    anode_top = wipal_tree.getNode("discharge.anodes")
    n_anodes = 20
    anodes = ["anode_{0:02d}".format(x+1) for x in range(n_anodes)]
    for idx, anode in enumerate(anodes):
        anode_node = anode_top.getNode(anode)
        status = sql_log(anode_node, shot_num, idx+1)
        if status:
            calculate_offsets(anode_node)
    special_anodes = ['ring_anode', 'bullet_anode']
    i = range(2)
    for anode in special_anodes:
        for idx in i:
            s = anode.replace("anode", "")
            anode_node = anode_top.getNode("{0}{1:02d}".format(s, idx+1))
            status = special_sql_log(anode_node, shot_num, anode, idx+1)
            if status:
                calculate_offsets(anode_node)

def special_sql_log(anode_node, shot_num, anode_type, anode_number):
    engine = get_engine()
    database = engine.url.database
    with engine.connect() as cnx:
        query = "select * from {0}.{1}{2} where shot_num = {3:d}".format(database, anode_type, anode_number, shot_num)
        result = pio.read_sql(query, cnx)

    if result.status[0] == 'off':
        print("Anode {0} {1} is off.".format(anode_type, anode_number))
        return False
    else:
        write_mdsplus_data(anode_node, result)
        return True

def sql_log(anode_node, shot_num, anode_number):
    engine = get_engine()
    database = engine.url.database
    with engine.connect() as cnx:
        query = "select * from {0}.shot_log_anode{1:d} where shot_num = {2:d}".format(database, anode_number, shot_num)
        result = pio.read_sql(query, cnx)

    if result.status[0] == 'off':
        print("Anode {0:d} is off.  Let's just move on then! No need to throw an exception!".format(anode_number))
        return False
    else:
        write_mdsplus_data(anode_node, result)
        return True

def write_mdsplus_data(anode_node, mysql_result):
    current_top = anode_node.getNode("current")
    raw, gain = mdsbuild.get_node_refs(current_top, "raw", "gain")

    c_gain = mysql_result.current_cal[0]

    mdsbuild.put_wrapper(gain, mds.Float32(c_gain), err=mds.Float32(0.02*c_gain), units="A/V")

    cur_path = "\\top.raw.a223_direct.ch_{0:02d}".format(mysql_result.cur_chan[0]).upper()

    raw.putData(mds.TreePath(cur_path))


def calculate_offsets(anode_node, npts=100):

    current_top = anode_node.getNode("current")
    current, current_offset = mdsbuild.get_node_refs(current_top, "raw", "offset")
    print(current)
    print(current.value_of())
    cur_data = current.data()

    c_offset = mds.Float32(np.mean(cur_data[0:npts]))

    c_offset_sd = mds.Float32(np.std(cur_data[0:npts]))

    mdsbuild.put_wrapper(current_offset, c_offset, err=c_offset_sd, units="V")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Reads sql data and fills in necessary data in the anode tree.")
    parser.add_argument("shot_num", metavar="Shot Number", type=int, help="Shot number to fill in MDSplus tree")
    args = parser.parse_args()

    main(args.shot_num)


