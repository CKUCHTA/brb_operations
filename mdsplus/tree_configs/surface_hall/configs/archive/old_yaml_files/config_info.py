#!/opt/anaconda2/bin/python
# Module for reading/writing yaml configuration files
# for surface_hall MDSplus tree
from ruamel import yaml
import os

config_path = "/home/WIPALdata/wipal_mdsplus/tree_configs/surface_hall/configs"

def write_config(hp_dict,idx,path="./"):
    with open(os.path.join(path,"hp_{0:03d}.yaml".format(idx)),"w") as file:
        file.write("---\n")
        file.write("# Surface Hall Array probe_{0:03d} configuration file\n".format(idx))
        file.write(yaml.dump(hp_dict))
        file.write("...")

def read_config(idx,path="./"):
    with open(os.path.join(path,"hp_{0:03d}.yaml".format(idx)),"r") as file:
        d = yaml.load(file,Loader=yaml.Loader)
    return d

if __name__ == "__main__":
    create = False
    if create:
        for idx in range(1,8):
            ### Hall puck #idx config/calibration data for individual axes and unit
            hp_r = {"chip_model":"MLX91205-AAL","gain":1.0/280.0,"gain_sd":1.0E-5,
                    "offset":0.000,"offset_sd":0.001,"loc":[1.524,0.0,0.0],
                    "loc_sd":[0.001,0.001,0.001],"raw":"\\TOP.RAW.A372_DIRECT:CH_65",
                    "bperm":0.0,"bperm_sd":0.0}
            hp_t = {"chip_model":"MLX91205-AAL","gain":1.0/280.0,"gain_sd":1.0E-5,
                    "offset":0.000,"offset_sd":0.001,"loc":[1.524,0.0,0.0],
                    "loc_sd":[0.001,0.001,0.001],"raw":"\\TOP.RAW.A372_DIRECT:CH_66",
                    "bperm":0.0,"bperm_sd":0.0}
            hp_p = {"chip_model":"MLX91205-AAL","gain":1.0/280.0,"gain_sd":1.0E-5,
                    "offset":0.000,"offset_sd":0.001,"loc":[1.524,0.0,0.0],
                    "loc_sd":[0.001,0.001,0.001],"raw":"\\TOP.RAW.A372_DIRECT:CH_67",
                    "bperm":0.0,"bperm_sd":0.0}
            ps_vol = {"gain":1.0,"gain_sd":0.0,"offset":0.000,"offset_sd":0.000,"raw":"\\TOP.RAW.A372_DIRECT:CH_80"}
            ps_cur = {"gain":1.0,"gain_sd":0.0,"offset":0.000,"offset_sd":0.000,"raw":""}
            hp_dict = {"unskew":[[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]],
                    "unskew_sd":[[1E-6,1E-6,1E-6],[1E-6,1E-6,1E-6],[1E-6,1E-6,1E-6]],
                    "rotate":[[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]],
                    "rotate_sd":[[1E-6,1E-6,1E-6],[1E-6,1E-6,1E-6],[1E-6,1E-6,1E-6]],
                    "cable_res":2.0, "cable_res_sd":0.1,"ps_voltage":ps_vol,"ps_current":ps_cur,
                    "r_axis":hp_r,"theta_axis":hp_t,"phi_axis":hp_p}
            write_config(hp_dict,idx,config_path)

