import MDSplus as mds
import numpy as np
import matplotlib.pyplot as plt

shot1s = [485,497,509,521,533,545,557]
boffs = np.zeros((7,3))
boffs_sd = np.zeros((7,3))
for j,shot1 in enumerate(shot1s):
    ps = np.zeros(12)
    br = np.zeros(12)
    bt = np.zeros(12)
    bp = np.zeros(12)
    shots = range(shot1, shot1+12)
    for i,shot in enumerate(shots):
        print(shot)
        t = mds.Tree("a370_direct",shot)
#        plt.plot(t.getNode("ch_11").data())
#        plt.plot(t.getNode("ch_12").data())
#        plt.plot(t.getNode("ch_13").data())
#        plt.show()
        ps[i] = np.mean(t.getNode("ch_01").data())# - dtq_offsets[0]
        br[i] = np.mean(t.getNode("ch_11").data())# - dtq_offsets[1]
        bt[i] = np.mean(t.getNode("ch_12").data())# - dtq_offsets[2]
        bp[i] = np.mean(t.getNode("ch_13").data())# - dtq_offsets[3]

    ## brdirs = brd,bru,brn,brs,bre,brw for probe 1
    if j == 0:
        brdirs = np.array([(br[0]+br[2])/2, (br[1]+br[3])/2, (br[4]+br[6])/2, (br[5]+br[7])/2, (br[8]+br[9])/2, (br[10]+br[11])/2])
        btdirs = np.array([(bt[9]+bt[10])/2, (bt[8]+bt[11])/2, (bt[0]+bt[3])/2, (bt[1]+bt[2])/2, (bt[4]+bt[7])/2, (bt[5]+bt[6])/2])
        bpdirs = np.array([(bp[4]+bp[5])/2, (bp[6]+bp[7])/2, (bp[9]+bp[11])/2, (bp[8]+bp[10])/2, (bp[0]+bp[1])/2, (bp[2]+bp[3])/2])

    # brdirs = brd,bru,brn,brs,bre,brw for all other probes
    else:
        brdirs = np.array([(br[0]+br[1])/2, (br[2]+br[3])/2, (br[4]+br[5])/2, (br[6]+br[7])/2, (br[8]+br[9])/2, (br[10]+br[11])/2])
        btdirs = np.array([(bt[8]+bt[11])/2, (bt[9]+bt[10])/2, (bt[0]+bt[3])/2, (bt[1]+bt[2])/2, (bt[5]+bt[6])/2, (bt[4]+bt[7])/2])
        bpdirs = np.array([(bp[5]+bp[7])/2, (bp[4]+bp[6])/2, (bp[8]+bp[10])/2, (bp[9]+bp[11])/2, (bp[0]+bp[2])/2, (bp[1]+bp[3])/2])
    dirs = ["D","U","N","S","E","W"]
    bravg = np.mean((brdirs[0::2]+brdirs[1::2])/2)
    bravg_sd= np.std((brdirs[0::2]+brdirs[1::2])/2)
    btavg = np.mean((btdirs[0::2]+btdirs[1::2])/2)
    btavg_sd= np.std((btdirs[0::2]+btdirs[1::2])/2)
    bpavg = np.mean((bpdirs[0::2]+bpdirs[1::2])/2)
    bpavg_sd= np.std((bpdirs[0::2]+bpdirs[1::2])/2)
    boffs[j,0] = bravg
    boffs[j,1] = btavg
    boffs[j,2] = bpavg
    boffs_sd[j,0] = bravg_sd
    boffs_sd[j,1] = btavg_sd
    boffs_sd[j,2] = bpavg_sd
    plt.plot(brdirs,"ro")
    #plt.plot([0.5,2.5,4.5],bravg,"rD")
    plt.plot(btdirs,"go")
    #plt.plot([0.5,2.5,4.5],btavg,"gD")
    plt.plot(bpdirs,"bo")
    #plt.plot([0.5,2.5,4.5],bpavg,"bD")
    plt.xticks(range(6),dirs)
    plt.xlim(-1,6)
    plt.title("Probe {0}".format(j+1))
    plt.show()

#np.save("boffs",boffs)
#np.save("boffs_sd",boffs_sd)
