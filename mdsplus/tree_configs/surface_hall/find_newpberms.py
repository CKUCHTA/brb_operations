import numpy as np
import matplotlib.pyplot as plt
import pickle

pucks = {}
for i in range(1,8):
    with open("puck_offsets/puck_offsets_{0}.p".format(i),"rb") as f:
        pucks["puck_{0}".format(i)] = pickle.load(f)

shots = pucks["puck_1"]["shots"]
br = np.zeros((len(shots),7))
bt = np.zeros((len(shots),7))
bp = np.zeros((len(shots),7))
for i in range(1,8):
    br[:,i-1] = pucks["puck_{0}".format(i)]["br_mean"]
    bt[:,i-1] = pucks["puck_{0}".format(i)]["bt_mean"]
    bp[:,i-1] = pucks["puck_{0}".format(i)]["bp_mean"]

idxl = np.where(shots<31224)[0]
idxg = np.where(shots>=31486)[0]
newdBRperm = np.mean(br[idxg,:],axis=0) - np.mean(br[idxl,:],axis=0)
newdBTperm = np.mean(bt[idxg,:],axis=0) - np.mean(bt[idxl,:],axis=0)
newdBPperm = np.mean(bp[idxg,:],axis=0) - np.mean(bp[idxl,:],axis=0)
print(newdBRperm.shape)
print(newdBRperm)
print(newdBTperm)
print(newdBPperm)

#np.save("newdBperms",np.vstack((newdBRperm,newdBTperm,newdBPperm)).T)

fig,axes = plt.subplots(1,7)
for i,ax in enumerate(axes.flatten()):
    ax.plot(shots,br[:,i],"ro")
    ax.plot(shots,bt[:,i],"go")
    ax.plot(shots,bp[:,i],"bo")

plt.show()


