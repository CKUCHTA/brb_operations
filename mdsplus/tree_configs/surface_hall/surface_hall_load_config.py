#!/opt/anaconda2/bin/python
import sys
import MDSplus as mds
import modules.mds_builders as mdsbuild
from modules.npconfigobj import NPConfigObj
import numpy as np

def main(shot):
    cfgroot = "/home/WIPALdata/wipal_mdsplus/tree_configs/surface_hall/configs/"
    cfgspec = cfgroot+"surface_hall_cfgspec.ini"
    cfgfile = cfgroot+"surface_hall.ini"

    wipal_tree = mds.Tree("wipal",shot)
    top = wipal_tree.getNode("magnetics.surface_hall")
    hpn_map = {"cable_res":(mds.Float64,"Ohm"),"loc":(mds.Float64Array,None)}
    pscur_map = {"gain":(mds.Float64,"A/V"), "offset":(mds.Float64,"V"), "raw":(mds.TreePath,None)}
    psvol_map = {"gain":(mds.Float64,"V/V"), "offset":(mds.Float64,"V"), "raw":(mds.TreePath,None)}
    axis_map = {"chip_model":(mds.String,None),"gain":(mds.Float64,"T/V"),"offset":(mds.Float64,"V"),
            "raw":(mds.TreePath,None),"bperm":(mds.Float64,"T"),"loc":(mds.Float64Array,None)}

    cfg = NPConfigObj(cfgfile,configspec=cfgspec)
    for probe in cfg.sections:
        print(probe)
        for name,(mdscls,units) in hpn_map.items():
            node = top.getNode("{0}:{1}".format(probe,name))
            data = mdscls(cfg[probe][name])
            err = None
            try:
                err = mdscls(cfg[probe][name+"_sd"])
            except KeyError:
                pass
            try:
                mdsbuild.put_wrapper(node,data,err=err,units=units)
            except mds.TreeNOOVERWRITE:
                pass

        for name,(mdscls,units) in pscur_map.items():
            node = top.getNode("{0}:ps_current:{1}".format(probe,name))
            data = mdscls(cfg[probe]["ps_current"][name])
            err = None
            try:
                err = mdscls(cfg[probe]["ps_current"][name+"_sd"])
            except KeyError:
                pass
            try:
                mdsbuild.put_wrapper(node,data,err=err,units=units)
            except mds.TreeNOOVERWRITE:
                pass

        for name,(mdscls,units) in psvol_map.items():
            node = top.getNode("{0}:ps_voltage:{1}".format(probe,name))
            data = mdscls(cfg[probe]["ps_voltage"][name])
            err = None
            try:
                err = mdscls(cfg[probe]["ps_voltage"][name+"_sd"])
            except KeyError:
                pass
            try:
                mdsbuild.put_wrapper(node,data,err=err,units=units)
            except mds.TreeNOOVERWRITE:
                pass

        for axis in ["r_axis","theta_axis","phi_axis"]:
            for name,(mdscls,units) in axis_map.items():
                node = top.getNode("{0}:{1}:{2}".format(probe,axis,name))
                data = mdscls(cfg[probe][axis][name])
                err = None
                try:
                    err = mdscls(cfg[probe][axis][name+"_sd"])
                except KeyError:
                    pass
                try:
                    mdsbuild.put_wrapper(node,data,err=err,units=units)
                except mds.TreeNOOVERWRITE:
                    pass

        # Build matrix RU with associated error to store in MDSplus
        u = cfg[probe]["unskew"]
        u_sd = cfg[probe]["unskew_sd"]
        r = cfg[probe]["rotate"]
        r_sd = cfg[probe]["rotate_sd"]
        ru = mds.Float64Array(r.dot(u))
        ru_sd = mds.Float64Array(r.dot(u_sd.dot(r.T)) + r_sd)
        mdsbuild.put_wrapper(top.getNode("{0}:ru".format(probe)),ru,err=ru_sd)
    print("surface_hall_load_config success!")

if __name__ == "__main__":
    shot = int(sys.argv[1])
    main(shot)

