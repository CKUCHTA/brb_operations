import matplotlib.pyplot as plt
import numpy as np
import MDSplus as mds
import sys
from analysis.datahelpers import smooth

shot = int(sys.argv[1])
t = mds.Tree("wipal",shot)
Br = t.getNode("\\surface_hall_br")
time = Br.getData().dim_of().data()
Br = Br.data()
Btheta = t.getNode("\\surface_hall_btheta").data()
Bphi = t.getNode("\\surface_hall_bphi").data()
locs = t.getNode("\\surface_hall_locs").data()

fig,axes = plt.subplots(2,4,figsize=(16,9))
for i,ax in enumerate(axes.flatten()):
    if i == 7:
        break
    br = smooth(10000*Br[i,:],30)
    bt = smooth(10000*Btheta[i,:],30)
    bp = smooth(10000*Bphi[i,:],30)
    ax.plot(time,br,"r")
    ax.plot(time,bt,"g")
    ax.plot(time,bp,"b")
    if i == 0:
        plt.legend(["Br","Btheta","Bphi"])
    ax.set_title("Theta={0:d},    Phi={1:d}".format(int(np.rad2deg(locs[i,1])),int(np.rad2deg(locs[i,2]))))

plt.show()
