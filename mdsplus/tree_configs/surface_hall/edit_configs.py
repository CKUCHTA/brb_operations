#!/opt/anaconda2/bin/python
# Module for reading/writing yaml configuration files
# for surface_hall MDSplus tree
import numpy as np
import matplotlib.pyplot as plt
from modules.npconfigobj import NPConfigObj
cfgroot = "/home/WIPALdata/wipal_mdsplus/tree_configs/surface_hall/configs/"
cfgspec = cfgroot+"surface_hall_cfgspec.ini"
cfgfile = cfgroot+"surface_hall.ini"

def compute_centroid(rloc,tloc,ploc,rloc_sd,tloc_sd,ploc_sd):
    locstack = np.vstack((rloc,tloc,ploc))
    rvec,tvec,pvec = locstack[:,0],locstack[:,1],locstack[:,2]
    ctvec,stvec,cpvec,spvec = np.cos(tvec),np.sin(tvec),np.cos(pvec),np.sin(pvec)
    locstack_sd = np.vstack((rloc_sd,tloc_sd,ploc_sd))
    rvec_sd,tvec_sd,pvec_sd = locstack_sd[:,0],locstack_sd[:,1],locstack_sd[:,2]

    # begin to calculate centroid using xbar, ybar, zbar temp variables
    xbar = 1.0/3.0*(rvec[0]*stvec[0]*cpvec[0] + rvec[1]*stvec[1]*cpvec[1] + rvec[2]*stvec[2]*cpvec[2])
    xbar_sd = 1.0/3.0*np.sqrt((stvec[0]*cpvec[0]*rvec_sd[0])**2 + (rvec[0]*ctvec[0]*cpvec[0]*tvec_sd[0])**2 + \
        (rvec[0]*stvec[0]*spvec[0]*pvec_sd[0])**2 + (stvec[1]*cpvec[1]*rvec_sd[1])**2 + \
        (rvec[1]*ctvec[1]*cpvec[1]*tvec_sd[1])**2 + (rvec[1]*stvec[1]*spvec[1]*pvec_sd[1])**2 + \
        (stvec[2]*cpvec[2]*rvec_sd[2])**2 + (rvec[2]*ctvec[2]*cpvec[2]*tvec_sd[2])**2 + \
        (rvec[2]*stvec[2]*spvec[2]*pvec_sd[2])**2)
    ybar = 1.0/3.0*(rvec[0]*stvec[0]*spvec[0] + rvec[1]*stvec[1]*spvec[1] + rvec[2]*stvec[2]*spvec[2])
    ybar_sd = 1.0/3.0*np.sqrt((stvec[0]*spvec[0]*rvec_sd[0])**2 + (rvec[0]*ctvec[0]*spvec[0]*tvec_sd[0])**2 + \
        (rvec[0]*stvec[0]*cpvec[0]*pvec_sd[0])**2 + (stvec[1]*spvec[1]*rvec_sd[1])**2 + \
        (rvec[1]*ctvec[1]*spvec[1]*tvec_sd[1])**2 + (rvec[1]*stvec[1]*cpvec[1]*pvec_sd[1])**2 + \
        (stvec[2]*spvec[2]*rvec_sd[2])**2 + (rvec[2]*ctvec[2]*spvec[2]*tvec_sd[2])**2 + \
        (rvec[2]*stvec[2]*cpvec[2]*pvec_sd[2])**2)
    zbar = 1.0/3.0*(rvec[0]*ctvec[0] + rvec[1]*ctvec[1] + rvec[2]*ctvec[2])
    zbar_sd = 1.0/3.0*np.sqrt((ctvec[0]*rvec_sd[0])**2 + (rvec[0]*stvec[0]*tvec_sd[0])**2 + \
        (ctvec[1]*rvec_sd[1])**2 + (rvec[1]*stvec[1]*tvec_sd[1])**2 + \
        (ctvec[2]*rvec_sd[2])**2 + (rvec[2]*stvec[2]*tvec_sd[2])**2)
    rbar = np.sqrt(xbar**2+ybar**2+zbar**2)
    rbar_sd = 1.0/rbar*np.sqrt((xbar*xbar_sd)**2 + (ybar*ybar_sd)**2 + (zbar*zbar_sd)**2)
    tbar = np.arctan2(np.sqrt(xbar**2+ybar**2),zbar)
    tbar_sd = 1.0/rbar**2*np.sqrt((zbar**2*((xbar*xbar_sd)**2 + (ybar*ybar_sd)**2))/(xbar**2 + ybar**2) + \
            (xbar**2+ybar**2)*zbar_sd**2)
    pbar = np.mod(np.arctan2(ybar,xbar)+2*np.pi,2*np.pi)
    pbar_sd = 1.0/(xbar**2+ybar**2)*np.sqrt((ybar*xbar_sd)**2 + (xbar*ybar_sd)**2)
    return np.array([rbar,tbar,pbar]), np.array([rbar_sd,tbar_sd,pbar_sd])


cfg = NPConfigObj(cfgfile,configspec=cfgspec)
for probe in cfg.sections:
    print(probe)
    loc = cfg[probe]["loc"]
    loc_sd = cfg[probe]["loc_sd"]
    rloc = cfg[probe]["r_axis"]["loc"]
    rloc_sd = cfg[probe]["r_axis"]["loc_sd"]
    tloc = cfg[probe]["theta_axis"]["loc"]
    tloc_sd = cfg[probe]["theta_axis"]["loc_sd"]
    ploc = cfg[probe]["phi_axis"]["loc"]
    ploc_sd = cfg[probe]["phi_axis"]["loc_sd"]
    cent, cent_sd = compute_centroid(rloc,tloc,ploc,rloc_sd,tloc_sd,ploc_sd)
    print((loc-cent)/loc*100, (loc_sd-cent_sd)/loc_sd*100)

#### Plotting stuff #######
    rloc*=180.0/np.pi
    rloc_sd*=180.0/np.pi
    tloc*=180.0/np.pi
    tloc_sd*=180.0/np.pi
    ploc*=180.0/np.pi
    ploc_sd*=180.0/np.pi
    loc*=180.0/np.pi
    loc_sd*=180.0/np.pi
    plt.errorbar(rloc[1],rloc[2],xerr=rloc_sd[1],yerr=rloc_sd[2],fmt="o")
    plt.errorbar(tloc[1],tloc[2],xerr=tloc_sd[1],yerr=tloc_sd[2],fmt="o")
    plt.errorbar(ploc[1],ploc[2],xerr=ploc_sd[1],yerr=ploc_sd[2],fmt="o")
    plt.errorbar(loc[1],loc[2],xerr=loc_sd[1],yerr=loc_sd[2],fmt="o")
plt.xlim(0,180)
plt.ylim(0,360)
plt.show()
