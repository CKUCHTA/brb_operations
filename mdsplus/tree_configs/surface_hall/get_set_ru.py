import MDSplus as mds
import numpy as np
import matplotlib.pyplot as plt
from modules.npconfigobj import NPConfigObj
from fields.grids import PointsListGrid
from fields.systems import BRB
import sys
cfgroot = "/home/WIPALdata/wipal_mdsplus/tree_configs/surface_hall/configs/"
cfgspec = cfgroot+"surface_hall_cfgspec.ini"
cfgfile = cfgroot+"surface_hall.ini"

write = False
if write:
    mode = "edit"
else:
    mode = "r"

cfg = NPConfigObj(cfgfile,configspec=cfgspec,mode=mode)
#hhshot = int(sys.argv[1])
#vacshot = int(sys.argv[2])
hhshot = 31158
vacshot = 31157
hht = mds.Tree("wipal",hhshot)
vact = mds.Tree("wipal",vacshot)
# get r,theta,phi locs and convert to R,phi,Z
locs = hht.getNode("\\surface_hall_locs").data()
#print(locs)
theta = locs[:,1]
rpts,zpts = locs[:,0]*np.sin(theta), locs[:,0]*np.cos(theta)
#print(rpts, zpts)
# do theoretical calculations for puck locations
RZgrid = PointsListGrid(rpts,zpts)
brb = BRB()
brb.trex.currents = [-56.8,-56.8]
brb.ltrx.currents = [0,0]
brb.vessel_mags.currents = [0,0,0]
brb.grid = RZgrid
BR = np.diag(brb.BR*10000)
BZ = np.diag(brb.BZ*10000)
Br,Btheta = BZ*np.cos(theta) + BR*np.sin(theta), -BZ*np.sin(theta) + BR*np.cos(theta)
#print("Br: ", Br)
#print("Btheta: ", Btheta)
brtp_theory = np.vstack((Br,Btheta,np.zeros(7))).T
brtp_meas = np.vstack((np.zeros(7),np.zeros(7),np.zeros(7))).T

for i, probe in enumerate(cfg.sections):
    print(probe)
    print(np.mean(hht.getNode("\\top.magnetics.surface_hall.{0}:ps_voltage".format(probe)).data()))
    br = np.mean(hht.getNode("\\top.magnetics.surface_hall.{0}:br".format(probe)).data()) - np.mean(vact.getNode("\\top.magnetics.surface_hall.{0}:br".format(probe)).data())
    bt = np.mean(hht.getNode("\\top.magnetics.surface_hall.{0}:btheta".format(probe)).data()) - np.mean(vact.getNode("\\top.magnetics.surface_hall.{0}:btheta".format(probe)).data())
    bp = np.mean(hht.getNode("\\top.magnetics.surface_hall.{0}:bphi".format(probe)).data()) - np.mean(vact.getNode("\\top.magnetics.surface_hall.{0}:bphi".format(probe)).data())
    brtp_meas[i,0] = 10000*br
    brtp_meas[i,1] = 10000*bt
    brtp_meas[i,2] = 10000*bp

write_gains = False
gains = 1/280.0 * np.sqrt(np.sum(brtp_theory**2,axis=1))/np.sqrt(np.sum(brtp_meas**2,axis=1))
#probes 5,7 probably broken
gains[4] = 1.0/280.0
gains[6] = 1.0/280.0

if write_gains and write:
    for i,probe in enumerate(cfg.sections):
        for axis in ["r_axis","theta_axis","phi_axis"]:
            cfg[probe][axis]["gain"] = gains[i]

write_ru = False
print(brtp_theory)
print(brtp_meas)
if write_ru and write:
#if write_ru and write:
    for i,probe in enumerate(cfg.sections):
        print(probe)
        b = brtp_theory[i,:]
        a = brtp_meas[i,:]
        print(a,b)
        axb = np.cross(a,b)/(np.sqrt(np.sum(b**2))*np.sqrt(np.sum(a**2)))
        print(axb)
        c = a.dot(b)/(np.sqrt(np.sum(b**2))*np.sqrt(np.sum(a**2)))
        vx = np.array([[0,-axb[2],axb[1]],[axb[2],0,-axb[0]],[-axb[1],axb[0],0]])
        print(c)
        R = np.eye(3) + vx + vx**2*(1.0/(1.0+c))
        if (i == 4) or (i == 6):
            break
        else:
            cfg[probe]["rotate"] = R

if write:
    cfg.write()

