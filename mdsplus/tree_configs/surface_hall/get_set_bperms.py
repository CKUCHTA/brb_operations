import MDSplus as mds
import numpy as np
import matplotlib.pyplot as plt
from modules.npconfigobj import NPConfigObj
import sys
cfgroot = "/home/WIPALdata/wipal_mdsplus/tree_configs/surface_hall/configs/"
cfgspec = cfgroot+"surface_hall_cfgspec.ini"
cfgfile = cfgroot+"surface_hall.ini"

write = True
if write:
    mode = "edit"
else:
    mode = "r"

cfg = NPConfigObj(cfgfile,configspec=cfgspec,mode=mode)
shot = int(sys.argv[1])
t = mds.Tree("wipal",shot)
newdBperms = np.load("newdBperms.npy")
print(newdBperms)
for i,probe in enumerate(cfg.sections):
    print(i, probe)
    print(np.mean(t.getNode("\\top.magnetics.surface_hall.{0}:ps_voltage".format(probe)).data()))
    for j,axis in enumerate(["r_axis","theta_axis","phi_axis"]):
        node = t.getNode("\\top.magnetics.surface_hall.{0}.{1}:b".format(probe,axis))
        bperm = cfg[probe][axis]["bperm"]
        cfg[probe][axis]["bperm"] = bperm + newdBperms[i,j]
#        cfg[probe][axis]["bperm"] = np.mean(node.data())
#        cfg[probe][axis]["bperm_sd"] = np.mean(node.getError().data())

if write:
    cfg.write()

