#!/opt/anaconda2/bin/python
import MDSplus as mds
import modules.mds_builders as mdsbuild
from config_info import read_config, config_path
import numpy as np
import sys
from analysis.datahelpers import rebin
from sqltools.core import start_cnx, close_cnx
import pandas.io.sql as pio
import pprint


def sql_log(top_node,shot):
    # get MySQL table for linear hall array 2
    # load following top level values into linear_hall2 tree
    # R_PIVOT, THETA_PIVOT, PHI_PIVOT, R0, DELTA_THETA, PS_VOLTAGE
    sql_config_path="/home/WIPALdata/sql_config.ini"
    engine = get_engine(sql_config_path)
    database = engine.url.database
    with engine.connect() as connection:
        sql_query = 'select * from {0}.linear_hall2 where shot_num = {1:d}'.format(database, shot)

        try:
            df = pandas.read_sql('select * from {0}.linear_hall2 where shot_num = {1:d}'.format(database, shot),cnx)
        except:
            raise Exception("Failed to read MySQL table {0}.linear_hall2 for shot {1}".format(database, shot))
        else:
            if df.status[0] == "off":
                raise Exception("MySQL table {0}.linear_hall2 status is 'off' for shot {1}".format(database, shot))
            else:
                r_pivot, theta_pivot, phi_pivot, r0, delta_theta,ps_voltage = mdsbuild.get_node_refs(top_node,
                        "r_pivot","theta_pivot","phi_pivot","r0","delta_theta","ps_voltage")
                # R_PIVOT
                rpiv,rpiv_sd = mds.Float32(df.r_pivot[0]), mds.Float32(0.003)
                mdsbuild.put_wrapper(r_pivot,rpiv,err=rpiv_sd,units="m")
                # THETA_PIVOT
                tpiv, tpiv_sd = mds.Float32(np.deg2rad(df.theta_pivot[0])), mds.Float32(0)
                mdsbuild.put_wrapper(theta_pivot,tpiv,err=tpiv_sd,units="rad")
                # PHI_PIVOT
                ppiv, ppiv_sd = mds.Float32(np.deg2rad(df.phi_pivot[0])), mds.Float32(0)
                mdsbuild.put_wrapper(phi_pivot,ppiv,err=ppiv_sd,units="rad")
                # R0
                r0dat,r0dat_sd = mds.Float32(df.r0[0]), mds.Float32(0.002)
                mdsbuild.put_wrapper(r0,r0dat,err=r0dat_sd,units="m")
                # DELTA_THETA
                dtheta, dtheta_sd = mds.Float32(np.deg2rad(df.delta_theta[0])), mds.Float32(np.deg2rad(.2))
                mdsbuild.put_wrapper(delta_theta,dtheta,err=dtheta_sd,units="rad")
                #PS_VOLTAGE
                ps_voltage.putData(mds.TreePath("\\TOP.RAW.{0}".format(df.ps_raw_node[0].upper())))

def process_data(top_node,shot):
    for idx in range(1,16):
        # Read in "unskew" matrix U and error for each set of 3 axis probe units
        d = read_config(idx,config_path)
        u = np.array(d["unskew"],dtype="float32")
        u_sd = np.array(d["unskew_sd"],dtype="float32")
        # Create "flip" matrix f to flip axis 3 to RH coordinate system
        f = np.array([[1,0,0],[0,1,0],[0,0,-1]],dtype="float32")
        # Build rotation matrix r and error to transform to machine frame
        dtheta = top_node.getNode("delta_theta").getData()
        theta_pivot = top_node.getNode("theta_pivot").getData()
        beta = dtheta.data() + theta_pivot.data()
        beta_sd = np.sqrt(dtheta.getError().data()**2 + theta_pivot.getError().data()**2)
        r = np.array([[np.cos(beta),0,np.sin(beta)],[0,1,0],[-np.sin(beta),0,np.cos(beta)]],dtype="float32")
        r_sd = np.sqrt(np.array([[np.sin(beta)**2,0,np.cos(beta)**2],[0,0,0],[np.cos(beta)**2,0,np.sin(beta)**2]],dtype="float32"))*beta_sd
        # Build matrix RFU with associated error to store in MDSplus
        # Add extra rotation if axis 1 points in -x when probe is inserted along N pole 
        #ex_rot = np.array([[1,0,0],[0,1,0],[0,0,1]],dtype="float32")
        #ex_rot = np.array([[-1,0,0],[0,-1,0],[0,0,1]],dtype="float32")
        rfu = r.dot(f.dot(u))
        rfu_sd = (r.dot(f.dot(u_sd.dot(f.T)))).dot(r.T) + r_sd
        rfu = mds.Float32Array(rfu)
        rfu.setError(rfu_sd)
        top_node.getNode("hp_{0:02d}:rfu".format(idx)).putData(rfu)

    # Start processing to populate subsampled bx,by,bz and probe location matrix
    pts = 5000
    locs = np.zeros((3,15),dtype="float32")
    locs_sd = np.zeros((3,15),dtype="float32")
    bx_arr = np.zeros((pts,15),dtype="float32")
    bx_sd_arr = np.zeros((pts,15),dtype="float32")
    by_arr = np.zeros((pts,15),dtype="float32")
    by_sd_arr = np.zeros((pts,15),dtype="float32")
    bz_arr = np.zeros((pts,15),dtype="float32")
    bz_sd_arr = np.zeros((pts,15),dtype="float32")
    for idx in range(15):
        hp = top_node.getNode("hp_{0:02d}".format(idx+1))
        pos = hp.getNode("pos").getData()
        bx = hp.getNode("bx").getData()
        by = hp.getNode("by").getData()
        bz = hp.getNode("bz").getData()
        time = rebin(bz.dim_of().data(),pts)[0]

        locs[:,idx] = pos.data()
        locs_sd[:,idx] = pos.getError().data()
        bx_arr[:,idx] = rebin(bx.data(),pts)[0]
        bx_sd_arr[:,idx] = rebin(bx.getError().data(),pts)[0]
        by_arr[:,idx] = rebin(by.data(),pts)[0]
        by_sd_arr[:,idx] = rebin(by.getError().data(),pts)[0]
        bz_arr[:,idx] = rebin(bz.data(),pts)[0]
        bz_sd_arr[:,idx] = rebin(bz.getError().data(),pts)[0]

    locs = mds.Float32Array(locs)
    locs_sd = mds.Float32Array(locs_sd)
    bxsig = mds.Signal(bx_arr,None,time)
    bxsig_sd = mds.Signal(bx_sd_arr,None,time)
    bysig = mds.Signal(by_arr,None,time)
    bysig_sd = mds.Signal(by_sd_arr,None,time)
    bzsig = mds.Signal(bz_arr,None,time)
    bzsig_sd = mds.Signal(bz_sd_arr,None,time)

    mdsbuild.put_wrapper(top_node.getNode("locs"),locs,err=locs_sd,units="m")
    mdsbuild.put_wrapper(top_node.getNode("bx"),bxsig,err=bxsig_sd,units="T")
    mdsbuild.put_wrapper(top_node.getNode("by"),bysig,err=bysig_sd,units="T")
    mdsbuild.put_wrapper(top_node.getNode("bz"),bzsig,err=bzsig_sd,units="T")
    print("linear_hall2_run_proc shot {0:d} success!".format(shot))


def main(shot):
    wipal_tree = mds.Tree("wipal",shot)
    hall_top = wipal_tree.getNode("magnetics.linear_hall2")
    try:
        sql_log(hall_top,shot)
    except:
        print("Not processing linear_hall2 data for shot {0:d}, set top node to off".format(shot))
        hall_top.setOn(False)
        raise
    else:
        process_data(hall_top,shot)
        ht = mds.Tree("linear_hall2",shot)
        ht.cleanDatafile()

if __name__ == "__main__":
    shot = int(sys.argv[1])
    main(shot)

