#!/opt/anaconda2/bin/python
# Module for reading/writing yaml configuration files
# for linear_hall2 MDSplus tree
from ruamel import yaml
import os

config_path = "/home/WIPALdata/wipal_mdsplus/tree_configs/linear_hall2/26047"

def write_config(hp_dict,idx,path="./"):
    with open(os.path.join(path,"hp_{0:02d}.yaml".format(idx)),"w") as file:
        file.write("---\n")
        file.write("# Linear Hall Array 1 probe {0:02d} configuration file\n".format(idx))
        file.write(yaml.dump(hp_dict))
        file.write("...")

def read_config(idx,path="./"):
    with open(os.path.join(path,"hp_{0:02d}.yaml".format(idx)),"r") as file:
        d = yaml.load(file,Loader=yaml.Loader)
    return d

if __name__ == "__main__":
    create = False
    if create:
        for idx in range(1,16):
            ### Hall puck #idx config/calibration data for individual axes and unit
            hp_1 = {"chip_model":"csa-1vg-so","gain":1.0/278.8,"gain_sd":7.2E-5,
                    "offset":.007,"offset_sd":.001,"raw":"\\TOP.RAW.A371_DIRECT:CH_01"}
            hp_2 = {"chip_model":"csa-1vg-so","gain":1.0/278.8,"gain_sd":7.2E-5,
                    "offset":.007,"offset_sd":.001,"raw":"\\TOP.RAW.A371_DIRECT:CH_01"}
            hp_3 = {"chip_model":"csa-1vg-so","gain":1.0/278.8,"gain_sd":7.2E-5,
                    "offset":.007,"offset_sd":.001,"raw":"\\TOP.RAW.A371_DIRECT:CH_01"}
            hp_dict = {"unskew":[[1,0,0],[0,1,0],[0,0,1]],
                    "unskew_sd":[[1E-3,1E-3,1E-3],[1E-3,1E-3,1E-3],[1E-3,1E-3,1E-3]],
                    "axis_1":hp_1,"axis_2":hp_2,"axis_3":hp_3}
            write_config(hp_dict,idx)

