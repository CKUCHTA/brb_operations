import MDSplus as mds
import numpy as np
import matplotlib.pyplot as plt
import sys

shot = int(sys.argv[1])

t = mds.Tree("wipal",shot)
h = t.getNode("magnetics.linear_hall2")

# Plot x, z coordinates
locs = h.getNode("locs").getData()
locs_sd = locs.getError().data()
print(locs_sd)
locs = locs.data()
z,x = locs[2,:],locs[0,:]
z_sd,x_sd = locs_sd[2,:],locs_sd[0,:]
fig0,ax0 = plt.subplots()
ax0.errorbar(z,x,yerr=x_sd,xerr=z_sd,fmt="o")
ax0.set_xlabel("Z (m)")
ax0.set_ylabel("X (m)")

bx = h.getNode("bx").getData()
bx_sd = bx.getError().data()
time = bx.dim_of().data()
bx = bx.data()

by = h.getNode("by").getData()
by_sd = by.getError().data()
by = by.data()

bz = h.getNode("bz").getData()
bz_sd = bz.getError().data()
bz = bz.data()

b = 10000*np.hstack((bx,by,bz))
b_sd = 10000*np.hstack((bx_sd,by_sd,bz_sd))
#np.save("/home/WIPALdata/b",b)
#np.save("/home/WIPALdata/b_sd",b_sd)
fig1,axes1 = plt.subplots(3,15)
for i,ax in enumerate(axes1.flatten()):
    ax.errorbar(time,b[:,i],yerr=b_sd[:,i],errorevery=25)

plt.show()
