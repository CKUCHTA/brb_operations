import numpy as np
from linear_hall2_run_proc import main

shots = [27018,27019,27046,27106,27116,27117]
for shot in shots:
    try:
        main(shot)
    except:
        print("Failed to process shot {0}".format(shot))
