#!/opt/anaconda2/bin/python
import sys
import MDSplus as mds
from config_info import read_config, config_path
import numpy as np

def main(shot):
    wipal_tree = mds.Tree("wipal",shot)
    hall_top = wipal_tree.getNode("magnetics.linear_hall2")

    child_cls_map = {"chip_model":mds.String,"gain":mds.Float32,
            "offset":mds.Float32, "raw":mds.TreePath}

    units_map = {"chip_model":None,"gain":"T/V", "offset":"V", "raw":None}

    for idx in range(1,16):
        d = read_config(idx,config_path)
        children = [k for k in d.keys() if "axis" in k]
        for child in children:
            c = d[child]
            for node_name in child_cls_map.keys():
                n = hall_top.getNode("hp_{0:02d}.{1}:{2}".format(idx,child,node_name))
                mdscls = child_cls_map[node_name]
                units = units_map[node_name]
                data = mdscls(c[node_name])
                data.setUnits(units)
                try:
                    err = mdscls(c[node_name+"_sd"])
                    err.setUnits(units)
                    data.setError(err)
                except KeyError:
                    pass
                n.putData(data)

if __name__ == "__main__":
    shot = int(sys.argv[1])
    main(shot)

