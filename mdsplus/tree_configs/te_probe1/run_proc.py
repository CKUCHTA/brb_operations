"""
Populates MDSplus tree for te probe1 with all necessary data from MySQL and the config file.
"""
import logging
import os.path
import json
import modules.mds_builders
from modules.mysql_connection import get_engine
import sqlalchemy
import pandas
import sys
import MDSplus as mds
import MDSplus.mdsExceptions as mds_exceptions


def read_sql(shot_number, sql_config_path="/home/WIPALdata/sql_config.ini"):
    """
    Read the probe position from the database for the queried shot.

    Parameters
    ----------
    shot_number : int
        The number of the shot we want the probe position for.
    sql_config : str, default="/home/WIPALdata/sql_config.ini"
        Path to config file containing sql info for database.

    Returns
    -------
    probe_r : float
        The radial position of the probe.
    probe_z : float
        The z position of the probe.
    """
    logging.debug("Reading from MySQL database te probe info for shot {num}.".format(num=shot_number))
    engine = get_engine(sql_config_path)
    database = engine.url.database
    with engine.connect() as connection:
        sql_query = "SELECT te_r, te_z FROM {database}.TREX_data where shot_num={num}".format(database=database, num=shot_number)

        logging.debug("Executing query '{query}'.".format(query=sql_query))
        try:
            result = pandas.read_sql(sql_query, connection)
        except sqlalchemy.exc.SQLAlchemyError:
            logging.exception("Failure executing query '{query}'.".format(query=sql_query))
            raise
        else:
            logging.info("Executed query successfully.")

    if result.size == 0:
        logging.error("Could not find te_r, te_z values for shot {}.".format(shot_number))
        raise ValueError("Missing row in MySQL for shot {}. No te_r and te_z values.".format(shot_number))

    # Extract the r and z values from the call.
    probe_r, probe_z = result.iloc[0]
    logging.debug("Got probe radius {r} and z {z}.".format(r=probe_r, z=probe_z))
    return probe_r, probe_z

def get_shot_config(shot_number, probe_config_path=os.path.join(os.path.realpath(os.path.dirname(__file__)), "configs/te_probe1.json")):
    """
    Get the data block for this shot by using the block with the maximum start_shot that is less than the queried shot.

    Parameters
    ----------
    shot_number : int
        The number of the shot we want the probe position for.
    probe_config_path : str, default='./configs/te_probe1.json'
        The path to the probe config file.

    Returns
    -------
    data_block : dict
        The data block from the config file that should be used for this shot.
    """
    try:
        with open(probe_config_path, 'r') as config_file:
            logging.debug("Succesfully opened '{path}'.".format(path=probe_config_path))
            config = json.load(config_file)
    except IOError: # TODO: Change this to FileNotFoundError once code moved to python3.
        logging.exception("Could not find te_probe1 config file at '{path}'.".format(path=probe_config_path))
        raise

    # Loop through the data blocks and find the block that has the latest start_shot before the queried shot.
    curr_start_shot = -1
    curr_block_index = -1
    for block_index, data_block in enumerate(config):
        block_start_shot = data_block["start_shot"] 
        if block_start_shot <= shot_number and block_start_shot > curr_start_shot:
            curr_start_shot = block_start_shot
            curr_block_index = block_index

    # If we didn't find any valid data blocks for this shot then raise an error.
    if curr_start_shot == -1:
        logging.error("Couldn't find any data blocks in te_probe1 config that are valid for shot {num}.".format(num=shot_number))
        raise ValueError("No valid config data block in '{path}' for shot {num}.".format(path=probe_config_path, num=shot_number))
    # Otherwise return the data_block for this shot.
    else:
        logging.info("Found valid data block with starting shot {start} (block {index}).".format(start=curr_start_shot, index=curr_block_index))
        return config[curr_block_index]
    
def insert_data(tree, te_top_node, data):
    """
    Put data into the te probe tree ignoring 'None' values.

    Parameters
    ----------
    te_top_node : mds.TreeNode
        Top of the te probe tree.
    data : dict[str, dict[...]]
        Data to insert into the tree. The data should be nested dictionaries where each key 
        is the name of a node and sub dictionaries contain the nodes children. Each value is 
        either a single value or a list of length 2 where the first is a float and the second 
        are the units of the data as a string.
    """
    def recursive_insert(tree, curr_node, curr_data):
        """
        Insert data in this sub-dictionary into the tree treating the curr_node as the top of the curr_data.
        """
        for node_name, node_data in curr_data.items():
            # Set the tree to the current node. We do this each iteration because the tree might change position during recursion.
            tree.setDefault(curr_node)

            # Try to get the node if it exists. If not then skip the insert and warn the user.
            # TODO: Add this also to te2 run_proc.py.
            try:
                node = tree.getNode(node_name)
            except mds_exceptions.TreeNNF() as e:
                logging.warning("Could not find node {name} of node {parent}. Skipping inserts for this node and sub-nodes.".format(name=node_name, parent=curr_node.getNodeName()))
                continue
            
            # If the data is a dictionary then recursively insert the dictionary with a new node.
            if isinstance(node_data, dict):
                logging.debug("Inserting {node_name} sub-dict for node {parent_name}.".format(node_name=node_name, parent_name=curr_node.getNodeName()))
                recursive_insert(tree, node, node_data)
            # If the data is a list then the first value is the data and the second is the units.
            elif isinstance(node_data, list) or isinstance(node_data, tuple):
                if node_data[0] is None:
                    logging.debug("Not inserting into {node_name} for node {parent_name} since data value is 'None' (units are {units}).".format(node_name=node_name, parent_name=curr_node.getNodeName(), units=node_data[1]))
                else:
                    logging.debug("Inserting into {node_name}, a value of {val} {units} for node {parent_name}.".format(node_name=node_name, val=node_data[0], units=node_data[1], parent_name=curr_node.getNodeName()))
                    modules.mds_builders.put_wrapper(node, modules.mds_builders.to_mds_datatype(node_data[0]), units=node_data[1])
            # Otherwise just insert the data.
            else:
                if node_data is None:
                    logging.debug("Not inserting into {node_name} for node {parent_name} since data value is 'None' (no units).".format(node_name=node_name, val=node_data, parent_name=curr_node.getNodeName()))
                else:
                    logging.debug("Inserting into {node_name}, a value of {val} (no units) for node {parent_name}.".format(node_name=node_name, val=node_data, parent_name=curr_node.getNodeName()))
                    modules.mds_builders.put_wrapper(node, modules.mds_builders.to_mds_datatype(node_data))
        logging.info("Inserted sub-tree of {curr_name}.".format(curr_name=curr_node.getNodeName()))

    # Remove the 'start_shot' attribute from the data since that is not associated with a node.
    data_to_insert = data.copy()
    logging.debug("Removing 'start_shot' from config data since no need to insert into tree.")
    data_to_insert.pop('start_shot')
    # Recursively insert the data starting from the top.
    logging.debug("Starting to insert data.")
    recursive_insert(tree, te_top_node, data_to_insert)
    logging.debug("Finished inserting data.")

def main(shot_number):
    """
    Get data from MySQL and the Te config and write that data into the MDSplus tree.
    """
    # TODO: Add method for calculating the cylindrical coordinate bdot values.
    logging.info("Running post-run procedure for shot {num}.".format(num=shot_number))

    # Open the wipal tree and get the top of the te tree.
    logging.debug("Opening MDSplus wipal tree.")
    wipal_tree = mds.Tree("wipal", shot_number)
    te_top_node = wipal_tree.getNode("kinetics.te_probe1")

    # Try to get the probe position from MySQL.
    try:
        probe_r, probe_z = read_sql(shot_number)
    except ValueError as e:
        logging.info("Can't get probe positions for shot {} since no row in database so not writing position. Error was:\n{}".format(shot_number, e))
        probe_r = None
        probe_z = None
    except sqlalchemy.exc.SQLAlchemyError as e:
        logging.info("Caught error while trying to read from MySQL. Can't get probe positions so not writing position.", exc_info=True)
        probe_r = None
        probe_z = None

    # Get the config file for the shot.
    shot_config = get_shot_config(shot_number)

    # Add the probe position to the data to write.
    shot_config["pos"] = {"r": (probe_r, "m"), "z": (probe_z, "m")}

    # Insert the data into the tree.
    logging.debug("Inserting config data into MDSplus.")
    insert_data(wipal_tree, te_top_node, shot_config)

    logging.info("Finished running post-run procedure for shot {num}.".format(num=shot_number))


if __name__=="__main__":
    # Log to a file rewriting the file each time.
    logging.basicConfig(filename="/home/WIPALdata/brb_operations/mdsplus/tree_configs/te_probe1/te_run_proc.log", filemode="w", level=logging.DEBUG)
    shot_number = int(sys.argv[1])
    main(shot_number)

