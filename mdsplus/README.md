# mdsplus

This sub-directory contains the codes used for creating trees and correctly giving them data as shots on BRB occur.

## Package dependencies

All packages needed can be found in `brb_operations/mdsplus/conda_environment.yml`. These packages are:

```yaml
python
configobj
cython
h5py
matplotlib
sqlalchemy
mysqlclient
numpy
pandas
pyyaml
scipy
pytables
mdsplus
ipython
```

To install these packages, we will use `mamba` which is a virtual environment manager similar to `conda`. To install `mamba`, follow [the install instructions](https://mamba.readthedocs.io/en/latest/installation.html).

Once installed, we can then install the required packages into a virtual environment. The default environment name is `snoke_env`. To install the environment with this name, run the following:

```shell
mamba env create -f <path>/snoke_conda_environment.yml
```

`<path>/snoke_conda_environment.yml` is the path to the `snoke_conda_environment.yml` file. If you want a different name, use the following:

```shell
mamba env create -f <path>/snoke_conda_environment.yml -n myenv
```

`myenv` is the name of the environment you want to create.

## Setup

In order to run this on snoke make sure that the python install can find all folders in the `brb_operations/mdsplus` directory. To do this, we need to put a `.pth` file into the `site-packages` directory that python uses. To find this directory, run the following:

```shell
conda activate snoke_env
python -m site --user-site
```

`snoke_env` is the name of the conda environment previously created. If you called your environment something else, please change the first line. If the second command (`python -m ...`) did not return anything, run `python -m site` instead after activating your environment. You need to go to the directory that ends in `site-packages`.

Now create a file called `brb_operations_mdsplus_modules.pth` in the directory that ends with `site-packages` from the prior commands. In the new file add a single line that is the path to the `mdsplus` directory in this repository. This pathname will end in `/brb_operations/mdsplus`.

To test that you have done this correctly activate python and try importing the following:

```python
from modules import mds_builders
```

If this does not throw a `ModuleNotFoundError` then you should be all set!

In addition, make sure that the `startup.m` file is in the `userpath` for matlab so that matlab loads it when running.

## Contributing

When adding new probes/measurements, there are a few files that need to be created.

The first is to create the MDSplus tree. These files are located in the `mdsplus/tree_creates/` directory. This file is only run once to initially create the tree (it can be run later if updates are needed but it's best to set the tree correct from the start).

The second is to create a set of files that update the tree after each shot. These can be found in the `mdsplus/tree_configs/`. Each sub-tree of the `wipal` tree has it's own directory. One file writes a config file to be used (usually something like `write_config.py`) and the other reads that file and does some other things before uploading data to the tree (similar to `run_proc.py`).

General tools for making these files can be found in the `modules` directory.
