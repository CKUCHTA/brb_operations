# brb_operations

Operate and analyze data from the BRB. Each directory at this level is for a different functionality and some have their own README's so check them out!

* `analysis`: Remotely access the data for further analysis for users.
* `labview`: Host labview codes used for operating the machine.
* `mdsplus`: Create and populate the MDSplus database to store the data.
* `mysql`: Create and populate the MySQL database.

## Cloning to a machine

On your personal computer, you need all files located in the `analysis` directory. Since it doesn't take up too much space, it is easy enough to clone the entire directory. There are two options for cloning; either `ssh` which requires the use of your public ssh key or `http` which requires you to enter your password each time you want to use `git`. If you want to use the `ssh` option, please jump to the section below titled "Using `ssh` clone".

### Using `ssh` Clone

Since it's a pain to have to login each time a `push` or `pull` happens it's easier to use cloning via `ssh`. Using the `https` method forces you to repeatedly log in so we want to avoid that.

#### Cloning with WiscVPN

Follow the steps below to clone this repository if you are able to use the [WiscVPN](https://it.wisc.edu/services/wiscvpn/):

1. Connect to WiscVPN.
2. Log into [the wisconsin gitlab](https://git.doit.wisc.edu/).
3. Search for the `brb_operations` project and request access.
4. Create an `ssh` public key if you don't already have one.  
The following works for Unix machines (Mac, Linux, etc.). To see if you have a public key, go into your `~/.ssh/` directory. If there is a file called `id_rsa.pub` then you are all set! If not, run `ssh-keygen` in a terminal session, leave the location to be `~/.ssh/id_rsa`, and set no password. This file should now exist.
5. Add the public key to your gitlab account.
    1. Edit your profile by clicking on your profile image on the top right and selecting 'Edit Profile' from the dropdown.
    2. Click on the 'SSH Keys' button on the left sidebar.
    3. Copy the text in your `id_rsa.pub` file into the 'Key' area.
    4. (Optional) Give it a descriptive name.
    5. (Optional) Add an expiration date.
    6. Click 'Add key'.
6. Using a terminal session, navigate to the location just above where you want the `brb_operations` directory. e.g. If I wanted `brb_operations` to be `~/Documents/brb_operations` I'd navigate to `~/Documents`.
7. Enter the command `git clone git@git.doit.wisc.edu:CKUCHTA/brb_operations.git`.  
Mac does not come with the required git tools installed so your computer may prompt you to install those tools. Once installed, rerun the command above.
8. You should now have the `brb_operations` directory installed on your computer!

If you are planning on using the `analysis` directory, please follow [these instructions](analysis/README.md).

#### Cloning with a guest account

If you are trying to connect to the repo using a computer that does not have a connection to the UW VPN you need to do some special stuff. First, check that you have an ssh config file located at `~/.ssh/config`. If not, create a file located in `~/.ssh/` called `config`. Next we will edit that file so that we use a proxy when connecting to GitLab. Edit the `config` file and create two hosts. The first host will connect us to the internal server (ex. `wippl-gateway.physics.wisc.edu`) and the other one will use that host. Below is the text to put in that file where anything contained between `<` and `>` is user dependent. We'll assume you are using the `wippl-gateway.physics.wisc.edu` connection server.

```bash
# The nickname `wippl-gateway` is your choice but it needs to be kept consistent.
Host wippl-gateway
  HostName wippl-gateway.physics.wisc.edu
  User <username-to-connect-to-wippl-gateway>
  Port 22

Host git.doit.wisc.edu
  ProxyCommand ssh wippl-gateway nc %h %p
```

Now that you have updated your `config` file, we need to add your ssh public key to the account of a current UW Madison employee. First, check that the `~/.ssh/id_rsa.pub` exists. If not, run `ssh-keygen` in a terminal session. I would advise NOT adding password protection as it will be more difficult to get data from the remote server. Now, send the `id_rsa.pub` file to a UW Madison employee you are in contact with, Cameron Kuchta at `ckuchta@wisc.edu` can do this. Ask them to add your ssh key to their gitlab account. As long as they have access to the `brb_operations` repository, you should also have access.

Your new clone command is `git clone ssh://git@git.doit.wisc.edu/CKUCHTA/brb_operations.git`.

### Cloning only some files

Often we only want some of these files on a machine. To clone only some files do the following:

```shell
mkdir <repo>
cd <repo>
git init
git sparse-checkout init
git remote add -f origin <url>
```

This creates an empty repository that points to the remote repository. Now we need to set what we want to checkout.

```shell
git sparse-checkout set "<directory/path>"
```

Do this for each directory you want to get from the repository. You can check what you have added with

```shell
git sparse-checkout list
```

These setting are put in the `.git/info/sparse-checkout` file so you can edit the file manually if you'd like to do that instead. Once you have added all the directories you want, use

```shell
git pull origin master
```

to get the files from the repository onto your computer.

## Contributing

When adding new probes, there are a few things to do. These are described in the `mdsplus/README.md` and `analysis/README.md` files.
